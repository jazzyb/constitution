TARGET := book.pdf
MAIN := main.tex
SRC := $(shell find book/* -name '*.tex') meta.sty refs.bib

all: README.md $(TARGET)

$(TARGET): $(MAIN) $(SRC)
	latexmk -pdflua -interaction=nonstopmode $<
	mv $(MAIN:.tex=.pdf) $@

README.md: book/preface.tex LICENSE
	pandoc -t markdown -s $< | \
		sed -e "s/{#section .unnumbered}/The Framers' Republic/" \
		    -e "s/{.flushright}//g" \
		    -e "s/::://g" > $@
	cat LICENSE >> $@

OUTPUT := $(TARGET) $(MAIN:.tex=.bbl) $(MAIN:.tex=.ent) $(MAIN:.tex=.run.xml)

clean:
	latexmk -C
	$(RM) $(OUTPUT)

list:
	# List delegates by number of statements:
	@find $(SRC) -name appendix_a.tex -prune -o \
		-type f -exec grep "^\\\\\\\\\\dd" {} \\; \
		| cut -d\{ -f2 \
		| cut -d\} -f1 \
		| grep -v Chairman \
		| grep -v Committee \
		| sort | uniq -c | sort -nr

.PHONY: clean list
