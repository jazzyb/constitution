\addpart{The Preamble}{\textit{We the People...}}
\cq{We the People of the United States, in Order to form a more perfect Union,
establish Justice, insure domestic Tranquility, provide for the common
defence, promote the general Welfare, and secure the Blessings of Liberty to
ourselves and our Posterity, do ordain and establish this Constitution for the
United States of America.}

\dd{The Chairman}{The Chair recognizes Governor Edmund Randolph of Virginia to
introduce the business of the convention.  Mr.\ Randolph, the floor is yours.}

\dd{Edmund Randolph (VA)}{I had insisted that someone of longer standing in
life and greater political experience should introduce our work, but since
this convention was Virginia's idea --- and you no doubt expect some
explanation from us --- my colleagues have imposed this responsibility on me.

\mn{Deficiencies of the Confederation}
Let me cut straight to the facts of our present crisis:  The experience of the
last six years demonstrates severe defects in the Articles of Confederation
--- defects that threaten to destroy the Union, that result from the
fundamentals of the Articles themselves.  Notice how poorly the Articles
fulfill the purposes for which they were framed:\rf{farrand1}{18}

\begin{enumerate}[wide=\parindent]
\item \emph{The Confederation cannot keep its promises to foreign nations.}
The states have violated all our treaties with Britain, France, and Holland,
and Congress cannot compel their obedience.  This defiance creates frequent
violations of the law of nations in other respects.  Foreign powers have yet
to hold us accountable, but we must not mistake their moderation for a
permanent partiality to our faults or a permanent security against
conflict.\pc{madison}

\item \emph{The federal government cannot defend itself from the encroachments
of the states.}  Factions have opposed federal measures in every state
assembly and session of Congress.  The result has been the delinquency of the
states.  With what power can Congress compel delinquent states to do what is
right?  By the militia --- a force that must be drawn from the states and
that the states may choose not to furnish.\rf{farrand1}{26}

\item \emph{The federal government has no power to secure harmony in the states.}
It cannot save the states from internal sedition.  What laws authorize
Congress to march troops into a state?  By what authority can Congress
determine which of the sides in a rebellion is in the right --- the supporters
or the oppressors of the government --- those who wish to change it or those
who wish to preserve it?\rf{farrand1}{25}

\item \emph{Congress requires powers that are currently impossible under
the present constitution.}  Among these is the power to tax.  Our
foreign debts have become urgent.  Congress has a duty to pay its debts, and
the power of taxation is necessary to ensure they can be paid.  Begging the
states for money has proven inefficient.\pc{madison}  With a sufficient
revenue we could establish great national works:  the improvement of
transportation, agriculture, manufacturing, and a freer intercourse among the
people.\rf{farrand1}{26}

\item \emph{The laws of the states have supremacy over the Articles of
Confederation.}  The state constitutions were formed early in the war by
direct representatives of the people.  The Confederation was formed long after
and ratified by the state legislatures --- not by any special appointment from
the people.  No judge would say the Articles are paramount to the state
constitutions.\rf{farrand1}{26}
\end{enumerate}

How did so many fatal omissions happen?  From a mistaken confidence that the
justice, good faith, honor, and sound policy of the several states would
render superfluous any ordinary sanction or coercion by which the laws secure
the obedience of individuals.\pc{madison}

I have the highest respect for the framers of the Articles.  They did all that
patriots could have done then in the infancy of the science of constitutions.
It is doubtful they could have attained anything better from the jealousy of
the states with regard to their sovereignty.\rf{farrand1}{18-9}  But today we
can perceive the defects inherent in the system.  Our chief danger arises from
the democratic parts of our state constitutions.  The powers of the government
that are exercised by the people swallow up the other branches.  None of the
states have checked this democratic tendency.\rf{farrand1}{26-7}

Men are impatient of restraint.  Nor will they conform to what is necessary to
the good order of society, unless either they are perfect in discernment and
virtue or the government under which they live is efficient.  The fathers of
the American fabric erred on the side of the former principle and chose it for
a foundation.  In the progress of the experiment, the fallacy is discovered,
and our Union must collapse if we cannot supply the latter.\rf{vancleve}{223}

Our only remedy is to frame a new Constitution, based on republican
principles, for a more efficient government.  But two points deserve attention
before beginning this work:  First, the new Constitution should only include
essential principles with simple and precise language.  We do not want to clog
the operations of government by making its provisions, which should be
accommodated to times and events, permanent and unalterable.

\mn{Purpose of the Preamble}
Second, we should start this Constitution with a preamble to explain the
purpose and ends of the new government.  The Massachusetts preamble, for
example, argues that government is formed by a voluntary association of
individuals, who each agree to be governed by certain laws for the common
good, in order to secure the protection and liberty of the whole.  But such
theory, which is proper in the first formation of state governments, is unfit
for our purposes, since we are not working on the natural rights of men not
yet gathered into society, but upon those rights modified by society and
interwoven with what we call the rights of states.

With this in mind I move that the preamble shall declare:  that the present
federal government is insufficient to the general happiness and security, that
the conviction of this fact gave birth to this convention, and that the only
effectual means that can be devised for curing this insufficiency is the
establishment of a consolidated national government with a \emph{supreme}
legislature, executive, and judiciary.\rf{farrand4}{183}}

\dd{The Chairman}{Congress, being aware of defects in the Confederation, has
instructed each of the thirteen states to send delegates to this convention
for the sole and express purpose of revising the Articles of Confederation and
rendering the federal constitution adequate to the exigencies of government
and the preservation of the Union.\rf{farrand3}{13-4}

The format of this convention will be as follows:  As \emph{Chairman}, I will
ask delegates to introduce resolutions for revision and then open the floor to
debate.  Once there are no more comments on the current motion, I will yield
the floor to the \emph{Committee of Detail}, who will take the entire debate
into consideration and, after careful deliberation, recommend a final
resolution.  We will continue this process until all the delegates' proposals
are exhausted and the final list of revisions can be submitted to Congress and
the states.

Since twelve of the states are represented --- only Rhode Island has declined
to appoint any delegates --- let us begin.  The floor is open to debate on
Mr.\ Randolph's proposition for a national government.}

\dd{Charles Pinckney (SC)}{What does `supreme' mean in this
context?\rf{farrand1}{33-4}}

\mn{Federalism vs. Nationalism}
\dd{Gouverneur Morris (PA)}{The distinction between our present federal system
and the \emph{supreme}, national government that Mr.\ Randolph proposes is
that the former is a mere compact resting on the good faith of the parties,
while the latter has a complete and compulsive operation.\rf{farrand1}{34}}

\dd{Elbridge Gerry (MA)}{I have doubts that our instructions from Congress and
our states can authorize a discussion of a system founded on different
principles from the Articles of Confederation.\rf{farrand1}{34}}

\dd{Roger Sherman (CT)}{I admit there are many defects in the current system.
The jurisdictions between the state and national governments are ill-defined,
and Congress lacks sufficient power --- particularly that of raising money,
which would require many more powers besides.  But I am reluctant to change
the existing system too much.  It would be wrong to lose every necessary
improvement by inserting any that the states would reject.\rf{farrand1}{34-5}}

\dd{Edmund Randolph (VA)}{When the salvation of the Republic is at stake, it
would be wrong not to propose anything we believe necessary.  The question is
whether we will adhere to the federal system or recommend a national plan.  As
I have demonstrated, the insufficiency of the former is on full display.  The
present moment is favorable to change and might be the last chance we will
have.\rf{farrand1}{255-6}  The best exercise of our power is to exert it for
the public good.\rf{farrand1}{263}}

\dd{John Lansing (NY)}{Our power is constrained to amendments of a federal
nature.  The act of Congress and the commissions of the several states all
prove that it would be unnecessary and improper to go further.  Do you think
the states will adopt or ratify a scheme that they never authorized us to
propose and so far exceeds what they regard as sufficient?  New York would
have never sent a delegation if she had supposed that our deliberations would
have turned to a national government.\rf{farrand1}{249}}

\dd{Gunning Bedford (DE)}{Mr.\ Randolph tells us --- with a dictatorial air
--- that this is the last chance for a good government.  It will be the last
indeed if his plan goes forth to the people.  The actual language of the
people has been that Congress should collect taxes on imports and coerce the
states to obey when it may be necessary.  This is what the people expect from
us.  All agree on the necessity of a more efficient government.  Why not make
the one they desire?\rf{farrand1}{491-2}}

\dd{William Paterson (NJ)}{Agreed. Instead of the plan suggested by Mr.\
Randolph, we should expand the powers of the present Congress by amending the
Articles of Confederation.  I move that we maintain the suffrage and
constitution of Congress, give Congress the power to raise revenue from taxes
on imports, and establish the power to call forth the force of the
Confederation to compel the obedience of delinquent
states.\rf{farrand1}{242-5}}

\dd{James Madison (VA)}{It would be unwise to give the federal government the
power to coerce or punish delinquent states.\rf{farrand1}{34}  The more I
reflect on the use of force, the more I doubt its practicability, justice, and
efficacy when applied to people collectively and not individually.  A Union of
the states containing such an ingredient would provide for its own
destruction.  The use of force against a state looks more like a declaration
of war than an infliction of punishment.  The attacked party might even
consider it a dissolution of all previous compacts by which it might be
bound.\rf{farrand1}{54}  We must erect a new government that can directly
operate on individual citizens and punish only those whose guilt requires
it.\rf{farrand1}{34}}

\dd{Edmund Randolph (VA)}{There are two methods by which the general
government can attain its ends: coercion or real legislation.  Coercion is
impractical, expensive, and cruel.  It tends to habituate its instruments to
shed the blood and riot in the spoils of their fellow citizens and,
consequently, to train them up for the service of ambition.

We must, therefore, resort to a national legislature over individuals rather
than a Congress over states.  To vest the necessary power in the current
Congress would blend the executive with the legislative.  If the union of
these powers has so far been safe in Congress, it is owing to the impotence of
that body.  Congress moreover is not elected by the people but by the state
legislatures.  They are a mere diplomatic body and are subservient to the
views of the states, who are always encroaching on the authority of the Union.

Provisions for harmony among the states, trade, naturalization --- for
crushing rebellion whenever it may rear its head --- must be made, but these
powers can never be given to a body so inadequate in its representation and
method of appointment.  Only a national government, properly constructed, will
answer the purpose.\rf{farrand1}{256}}
%This is the last moment for establishing one.  After this select experiment,
%the people will yield to despair.

\dd{William Paterson (NJ)}{Is coercion under a federal plan any more
impracticable or unjust than under a national one?  Its efficacy will depend
on the amount of power collected, not on whether the power is drawn from
states or individuals.  And force may be exerted on individuals in a federal
plan just as well, Mr.\ Madison.\rf{farrand1}{251}

The fact is an improved federal government better accords with the powers of
this convention and the sentiments of the people.  I do not speak on my own
behalf but on behalf of those who sent me.  Our purpose is not to frame the
best government possible but such a one as our constituents have authorized us
to prepare and that they will approve.\rf{farrand1}{250}}
%Article XIII declares that no alteration be made without unanimous
%consent.\footnote{From Article XIII: ``...nor shall any alteration at any
%time hereafter be made in any of them, unless such alteration be agreed to in
%a congress of the united states, and be afterwards confirmed by the
%legislatures of every state.''}  This is the nature of all treaties.  What is
%unanimously done, must be unanimously undone.

\dd{Alexander Hamilton (NY)}{Our constituents instructed us to meet ``for the
sole and express purpose'' of amending the Confederation and rendering it
effectual for the purposes of a good government.  You and other proponents of
the federal plan seem to lay great stress on the words `sole' and `express' as
if these words were meant to confine us to a federal government, when their
real import is no more than that the institution of a good government must be
the \emph{sole and express purpose} of our deliberations.\rf{farrand1}{294}}

\dd{James Wilson (PA)}{Mr.\ Paterson, you say you prefer a federal plan
because it accords with the powers of this convention and the sentiments of
the people.  Regarding the power of this convention, I conceive of myself as
authorized to conclude nothing but to be at liberty to propose anything.  I
have a right to agree to any plan or none.

Regarding the sentiments of the people, I find it difficult to know precisely
what they are.  Those of one's social circle are often mistaken for the
general voice.  I cannot persuade myself that the state governments and their
sovereignty are as much the idols of the people as you suppose.  Why should a
national government be unpopular?  Has it less dignity?  Will citizens of
\emph{New Jersey} be degraded by becoming citizens of the \emph{United
States}?  Will they enjoy less liberty or protection?

Where do the people now look for relief from the evils of which they complain?
Is it to the internal reform of their government?  No, sir.  It is from this
\emph{national} convention that they expect relief.  For these reasons I am
sure the people will follow us into a national government.\rf{farrand1}{253}}

\dd{Luther Martin (MD)}{The separation from Great Britain placed the thirteen
states in a state of nature towards each other.  We would have remained in
that state but for the Confederation.  We entered into the Confederation as
independent sovereigns on equal footing, and we meet now to amend it on that
same footing.\rf{farrand1}{324}}

\dd{James Wilson (PA)}{The colonies became independent of Great Britain, but
they did not become independent of each other.  The Declaration of
Independence declares, ``That these \emph{united colonies} are, and of right
ought to be free and independent states.''  The colonies did not declare their
independence \emph{individually} but \emph{unitedly}.\rf{farrand1}{324}}

\dd{Rufus King (MA)}{The states do not possess the peculiar features of
sovereignty that you assert, Mr.\ Martin.  They can make no war or peace or
alliances or treaties.  They are dumb for they cannot speak to foreign powers.
They are deaf for they cannot hear any propositions from the same.  They are
even unarmed for they cannot of themselves raise troops or equip vessels for
war.

If the Union of the states comprises the idea of a confederation, it also
comprises the idea of a consolidation.  A union of states is a union of the
men composing them, from whence a national character results to the whole.  If
the states have retained some portion of their sovereignty, they have
certainly divested themselves of essential portions of it.  If they form a
confederation in some respects, they form a nation in
others.\rf{farrand1}{323-4}}

\dd{William Johnson (CT)}{I understand the proponents of the national plan
intend for the states be left with a considerable though subordinate
jurisdiction.  But they have not shown how this could be secured against the
general sovereignty and jurisdiction of the national government.  If this
could be shown in such a manner as would satisfy the proponents of the federal
plan, that the individuality of the states would not be endangered, then many
of their reservations might be removed.\rf{farrand1}{355}}

\dd{James Wilson (PA)}{Dr.\ Johnson, the national government will be as ready
to preserve the rights of the states as the states are to preserve the rights
of their citizens.  All the members of the national legislature, being
representatives of the people of the states, will have an interest in leaving
the state governments in possession of whatever power the people wish them to
retain.  Therefore, I see no danger whatsoever to the states from a national
government.  On the contrary, I anticipate that in spite of every precaution
the national government will be in perpetual danger of encroachments from the
state governments.\rf{farrand1}{356}}

\dd{James Madison (VA)}{All the examples of other confederations prove a
greater tendency in such systems to anarchy than to tyranny --- to
disobedience of the members than to arrogation of the federal head.  Our own
experience fully illustrates this tendency.

But my opponents will point out that the proposed change in the principles and
form of the Union will vary the tendency, that the national government will
have real and greater powers.  To give full force to this objection, let us
suppose that indefinite powers are given to the national government and the
states reduced to mere dependent corporations.  Why would the national
government, in such a case, take from the states any power that was beneficial
and desirable to the people?

Take Connecticut for example:  All the townships are incorporated and have a
certain limited jurisdiction.  Have the representatives of the state
legislature ever attempted to deprive the townships of any part of their local
authority?  As far as the local authority is convenient to the people, they
are attached to it, and the representatives chosen by them respect these
attachments as much as any other right or interest.  The relation of the
national government to the states is analogous.\rf{farrand1}{356-7}}

\summary{The Committee finds that the Articles of Confederation are deficient
for achieving the ends of the general government, being principally to secure
the liberties, common defense, and general welfare of the American
people.\rf{story1}{444}  In its place this convention will attempt to
institute a national system --- a \emph{constitution} deriving its authority
from the people directly rather than a mere \emph{compact} among sovereign
states.\rf{story1}{446}  The Constitution shall begin with a preamble
emphasizing how it derives its power from the people and that its aim is to
achieve the ends for which the federal system is deficient.}
