\ch{Article III, Section 2}{Jurisdiction of the Supreme Court}
\cl{Clause 1}{Extent of cases and controversies}
\cq{The judicial Power shall extend to all Cases, in Law and Equity, arising
under this Constitution, the Laws of the United States, and Treaties made, or
which shall be made, under their Authority; to all Cases affecting
Ambassadors, other public Ministers and Consuls; to all Cases of admiralty and
maritime Jurisdiction; to Controversies to which the United States shall be a
Party; to Controversies between two or more States; between a State and
Citizens of another State; between Citizens of different States; between
Citizens of the same State claiming Lands under Grants of different States,
and between a State, or the Citizens thereof, and foreign States, Citizens or
Subjects.\footnote{This clause modified by Amendment XI (1798):  ``The
Judicial power of the United States shall not be construed to extend to any
suit in law or equity, commenced or prosecuted against one of the United
States by Citizens of another State, or by Citizens or Subjects of any Foreign
State.''}}

\dd{The Chairman}{The Chair recognizes Edmund Randolph of Virginia.}

\dd{Edmund Randolph (VA)}{I move that the jurisdiction of the Supreme Court
shall extend to all cases arising under the laws passed by
Congress,\rf{farrand2}{186} which respect the collection of revenue,
impeachments, and questions that involve the national peace and
harmony.\rf{farrand1}{232}}

\dd{The Chairman}{The floor is open to debate on the motion.}

\dd{William Johnson (CT)}{The jurisdiction of the Supreme Court should extend
to equity\footnote{In this context \emph{equity} means making an exception to
the law in a specific case, where the general rule of the law does not apply.
That is, if the legislator himself had foreseen the case, he would have
considered it an exception to the law in question. (\cite[p.\ 172]{aristotle};
\cite[p.\ 47]{blackstone1})  In British law \emph{courts of equity} were
limited to matters of property and were established ``to correct and soften
the rigor of the law, when through its generality it bears too hard in
particular cases.'' (\cite[p.\ 67]{blackstone1})  Blackstone goes on to admit
that there is a great deal of overlap between courts of law and courts of
equity.  However, one essential difference between the two is in their
outcomes.  For example, in a case regarding a breach of contract, a court of
law would grant damages for non-performance while a court of equity would
compel the party at fault to execute what they had agreed to do. (\cite[p.\
287-8]{blackstone3})  The distinction between these two types of courts has
since been dissolved in most of the United States.} as well as
law\rf{farrand2}{428} and to this Constitution.\rf{farrand2}{430}}

\dd{James Madison (VA)}{Dr.\ Johnson, do you think it goes too far to extend
jurisdiction of the court to constitutional cases \emph{generally}?  Shouldn't
we limit the Supreme Court to expounding the Constitution in cases solely of a
judicial nature?\rf{farrand2}{430}}

\dd{William Johnson (CT)}{Yes, it was my intention that the jurisdiction is
constructively limited to cases of a judicial nature.\rf{farrand2}{430}}

\dd{George Read (DE)}{We should not vest the powers to hear cases of equity
and law in the same court.\rf{farrand2}{428}}

\dd{George Mason (VA)}{I agree.  If the jurisdiction of the Supreme Court
extends to all cases in law and equity, to what cases will this expression not
extend?  What are we leaving within the jurisdiction of the state courts?
When I consider the nature of such a court, I must conclude that its effect
and operation will be to utterly destroy the state governments --- especially
when this body will itself judge how far its own jurisdiction
operates.\rf{elliot3}{521}}

\summary{\emph{The jurisdiction of the Supreme Court shall extend in law and
equity} to ten types of cases, for which the state courts would be
improper:\rf{story3}{500-2}

\begin{enumerate}[wide=\parindent]
\item \emph{To all cases arising under this Constitution} --- because the
meaning, construction, and operation of a compact should be ascertained by all
the parties, not by an authority derived from only one of them.

\item \emph{To all cases arising under the laws of the United States} ---
because as such laws, constitutionally made, are obligatory on each state, the
measure of obligation and obedience should be decided by a tribunal deriving
authority from both parties.

\item \emph{To all cases made by treaties under their authority} --- because
as treaties are compacts made by, and obligatory on, the whole nation, their
operation should not be affected by local laws or courts of part of the
nation.

\item \emph{To all cases affecting ambassadors, other public ministers, and
consuls} --- because as these are officers of foreign nations, whom the nation
is bound to protect and treat according to the laws of nations, cases
affecting them should be tried by national authority.

\item \emph{To all cases of admiralty and maritime jurisdiction} --- because
the seas are the joint property of nations, whose rights and privileges are
regulated by the law of nations and treaties, such cases naturally belong to
the national jurisdiction.

\item \emph{To controversies to which the United States shall be a party} ---
because in cases in which the whole people are interested, it would not be
just or wise to let any one state decide.

\item \emph{To controversies between two or more states} --- because justice
should not depend on the will of either litigant, and domestic tranquility
requires that the contentions of states should be peaceably terminated by a
common judiciary.

\item \emph{To controversies between a state and citizens of another state or
between citizens of different states} --- because a trial in either states'
jurisdiction would create suspicions of partiality, and true republican
government requires that free and equal citizens should have free, fair, and
equal justice.

\item \emph{To controversies between citizens of the same state, claiming
lands under grants of different states} --- because as the rights of two
states to grant the land are drawn into question, neither state should decide
the controversy.

\item \emph{And to controversies between a state, or its citizens, and foreign
states, citizens, or subjects} --- because as every nation is responsible for
the conduct of its citizens towards other nations, all questions touching the
justice due to foreign nations or people should be ascertained by national
authority.
\end{enumerate}}

\cl{Clause 2}{Original and appellate cases}
\cq{In all Cases affecting Ambassadors, other public Ministers and Consuls,
and those in which a State shall be Party, the supreme Court shall have
original Jurisdiction. In all the other Cases before mentioned, the supreme
Court shall have appellate Jurisdiction, both as to Law and Fact, with such
Exceptions, and under such Regulations as the Congress shall make.}

\dd{The Chairman}{The Chair recognizes James Wilson of Pennsylvania.}

\dd{James Wilson (PA)}{I move that the Supreme Court shall have original
jurisdiction in all cases affecting public ministers or in which a state
should be a party.  In all other cases the court shall have appellate
jurisdiction, both as to law and fact, under such regulations as Congress
shall make.}

\dd{The Chairman}{The floor is open to debate on the motion.}

\dd{Gouverneur Morris (PA)}{In what contexts is the jurisdiction appellate?
Does it extend to common as well as civil law?\rf{farrand2}{431}}

\dd{James Wilson (PA)}{Yes, the same jurisdiction as the federal court of
appeals under the Articles of Confederation.\rf{farrand2}{431}}

\dd{Luther Martin (MD)}{Having appellate jurisdiction ``as to fact'' means
that the trial will be without a jury, which is concerning in criminal cases.
True, the first trial in an inferior court might have a jury.  But if in a
criminal case the prosecution is not satisfied with the verdict of a jury,
they may remove the decision to the Supreme Court where the verdict of the
jury will be of no effect, and the judges are to decide upon the fact as well
as the law, the same as in civil cases.\rf{farrand3}{221}}

\dd{James Madison (VA)}{A jury trial at this level would be impracticable;
although, I do regret it cannot be done.  A satisfactory answer is to leave it
to the discretion of Congress to modify it according to
circumstances.\rf{farrand3}{332}}

\summary{\emph{The Supreme Court shall have original jurisdiction in all cases
affecting ambassadors, public ministers, and consuls.}  Thus, they might be
directly resorted to if the delays of an inferior court might endanger the
tranquility or interests of the government.\rf{story3}{522}
\emph{Additionally, original jurisdiction shall extend to cases involving a
state.} % TODO: But why original jurisdiction for states?

\emph{The Supreme Court shall have appellate jurisdiction in all other cases,
subject to such exceptions and regulations as Congress may prescribe.}  Judges
of equal learning and integrity in inferior or state courts might differ in
their interpretations of statutes or treaties or even the Constitution itself.
Without a revising authority to control these jarring and discordant
judgments and harmonize them into uniformity, the laws, treaties, and
Constitution of the United States would have different constructions,
obligations, and efficacy in different states.  The appellate jurisdiction is
the only adequate remedy for such evils.\rf{story3}{607-8}}

\cl{Clause 3}{Jury trials}
\cq{The Trial of all Crimes, except in Cases of Impeachment, shall be by Jury;
and such Trial shall be held in the State where the said Crimes shall have
been committed; but when not committed within any State, the Trial shall be at
such Place or Places as the Congress may by Law have directed.}

\dd{The Chairman}{The Chair recognizes William Johnson of Connecticut.}

\dd{William Johnson (CT)}{I move that the trial of all crimes --- except
impeachment --- shall be by jury and be held local to where the crime is said
to have been committed.}

\dd{The Chairman}{The floor is open to debate the motion.}

\dd{Charles Pinckney (SC)}{Trial by jury should be guaranteed in civil cases,
too.\rf{farrand2}{628}}

\dd{C. C. Pinckney (SC)}{A provision guaranteeing jury trials in civil cases
would be pregnant with embarrassments.\rf{farrand2}{628}}

\dd{Nathaniel Gorham (MA)}{It is not possible to distinguish between civil
cases in which juries would be proper or improper.  We should leave the matter
to Congress to decide.\rf{farrand2}{587}}

\dd{Elbridge Gerry (MA)}{Juries are necessary to guard against corrupt judges,
even in civil trials.\rf{farrand2}{587}}

\dd{George Mason (VA)}{Mr.\ Gorham is correct that proper cases cannot be
enumerated.  But the statement of a general principle here might be
sufficient.\rf{farrand2}{587}}

\summary{\emph{The trial of all crimes shall be by jury.}  This fundamental
right goes back as far as an article of the Magna Carta, which says, ``No man
shall be arrested nor imprisoned nor deprived of life but by the judgment of
his peers or the law of the land.''\rf{story3}{652}

\emph{Such trial shall be held in the state where the crime shall have been
committed.}  This right secures the accused from being dragged to some distant
state, away from his friends and witnesses and neighborhood, to be subjected
to the verdict of mere strangers, who may feel no common sympathy with him and
who may even be prejudiced against him.\rf{story3}{654}}
