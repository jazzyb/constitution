\ch{Article III, Section 3}{Treason}
\cq{Treason against the United States, shall consist only in levying War
against them, or in adhering to their Enemies, giving them Aid and Comfort. No
Person shall be convicted of Treason unless on the Testimony of two Witnesses
to the same overt Act, or on Confession in open Court.  The Congress shall
have power to declare the Punishment of Treason, but no Attainder of Treason
shall work Corruption of Blood, or Forfeiture except during the Life of the
Person attainted.\footnote{In British common law of the time, a death sentence
created a state called \emph{attainder} because the person sentenced was
\emph{attainted} --- meaning stained or blackened --- i.e. ``he cannot be a
witness in court; neither is he capable of performing the functions of another
man: for, by an anticipation of his punishment, he is already dead in law.''
(\cite[p.\ 245]{blackstone4})  The consequences of attainder were the
\emph{forfeiture} (confiscation) of the attainted's property and their
\emph{corruption of blood}, which meant they could neither inherit property
nor transmit property to any heir. (\cite[p.\ 250]{blackstone4})}}

\dd{The Chairman}{The Chair recognizes John Rutledge of South Carolina.}

\dd{John Rutledge (SC)}{I move that treason against the United States shall
consist only in levying war against the United States, or any of them, or in
supporting the enemies of the United States, or any of them.  Congress shall
have power to declare the punishment of treason.  And no person shall be
convicted of treason unless on the testimony of two
witnesses.\rf{farrand2}{345}}

\dd{The Chairman}{The floor is open to debate on the motion.}

%\dd{James Madison (VA)}{The `and' between ``levying war'' and ``adhering''
%should be an `or.'  Otherwise, both offenses might be necessary to constitute
%treason.\rf{farrand2}{346}}

\dd{John Dickinson (DE)}{What does the testimony of two witnesses mean?  Are
they witnesses to the same act or to two different acts?\rf{farrand2}{346}}

\dd{Benjamin Franklin (PA)}{They should be witnesses to the same overt act.
Prosecutions of treason can become heated, and perjury is too easily used
against the innocent.\rf{farrand2}{348}}

\dd{James Wilson (PA)}{I'm not sure that would be better.  Treason may be done
so as to render proof extremely difficult.\rf{farrand2}{348}}

\dd{Edmund Randolph (VA)}{The motion's definition of treason is too narrow.
The British law includes ``giving enemies aid and comfort,''\footnote{From
statue 25 Edw.\ III.\ c.\ 2:  ``If a man be adherent [supporter] to the king's
enemies in his realm, giving to them aid and comfort in the realm, or
elsewhere.'' (\cite[p.\ 55]{blackstone4})} which has a more extensive
meaning.\rf{farrand2}{345}}

\dd{James Wilson (PA)}{``Giving aid and comfort'' are explanatory words, not
operative, and should be omitted.\rf{farrand2}{346}}

\dd{James Madison (VA)}{Since the motion specifies treason \emph{against the
United States}, the clause implies that the individual states will possess a
concurrent power to define and punish treason against themselves, which might
result in double jeopardy.\rf{farrand2}{346}}

\dd{Gouverneur Morris (PA)}{Then we should specify that Congress shall have
the \emph{sole} power to declare treason.  In a contest between the Union and
a particular state, the people of the latter must, given the current wording
of the clause, be traitors to one or the other authority.\rf{farrand2}{345}}

\dd{William Johnson (CT)}{There can be no treason against an individual state.
The sovereignty in our present Confederation is in the Union, not the states.
It will be even less so under the new system.\rf{farrand2}{347}}

\dd{George Mason (VA)}{No, Dr.\ Johnson, the United States will have a
qualified sovereignty only.  The individual states will retain a part of their
sovereignty.  An act may be treason against a state but not so against the
United States.  For example, when Nathaniel Bacon rebelled against Governor
Berkeley, it was treason against the colonial government of Virginia but not
against the Crown.\footnote{In May of 1676 after a series of escalating,
retaliatory attacks between English settlers and American Indian tribes on the
Virginia frontier, Nathaniel Bacon, an Indian trader and planter, led a
campaign to indiscriminately murder all the tribes in the area.  When William
Berkeley, the colonial governor (and Bacon's cousin by marriage), denounced
and refused to support the attacks, Bacon accused the governor of not doing
enough to secure the safety of the English and led a rebellion to overthrow
him, which resulted in the burning of the capital, Jamestown.  Berkeley's
forces managed to overcome the rebels and regain control shortly after Bacon's
death that October. (\cite{washburn})}\rf{farrand2}{347}}

\dd{William Johnson (CT)}{No line can be drawn between treason against the
United States and an individual state.  An offense against one or the other is
an offense against the same \emph{supreme} sovereign --- the United
States.\rf{farrand2}{347}}

\dd{Roger Sherman (CT)}{But there is a difference, Dr.\ Johnson.  Whether
state or national laws are being resisted forms the line.  Both the state and
national governments should have power to defend their respective
sovereignty.\rf{farrand2}{349}}

\dd{Rufus King (MA)}{The distinction might not be that important.  Congress
may punish capitally under offenses other than ``treason.''\rf{farrand2}{347}
Or the states may call offenses ``high misdemeanors'' if they cannot punish
them as treason.\rf{farrand2}{348}}

\summary{\emph{Treason against the United States shall consist only in levying
war against them, or in supporting their enemies, giving them aid and
comfort.} This clause borrows the wording of the British statute of treason.
It is of great importance that the nature and limits of treason be explicitly
defined in the Constitution.  If it were left to the discretion of Congress or
judges to ascertain, then violent factions might use the accusation to
threaten and punish their innocent enemies with death.\rf{story3}{667-9}

\emph{No person shall be convicted of treason, unless on the testimony of two
witnesses to the same overt act, or on confession in open court.}  A similar
provision also exists in British jurisprudence,\rf{blackstone4}{230} founded
upon the same great policy of protecting men against false testimony and
unguarded confessions to their utter ruin.\rf{story3}{671}

\emph{Congress shall have the power to declare the punishment of treason.}  A
state may punish treason only if it be committed exclusively against itself
--- if indeed any case can exist, which is not at the same time treason
against the United States.\rf{story3}{173}

\emph{But no death penalty resulting from the treason shall prevent the
traitor from transmitting property to their heirs, nor shall their property be
confiscated except during their lifetime.}  This protection prevents the
sentence against the traitor from also becoming an offense against any
innocent posterity.  It is enough for society to take the life of the
offender, as a just punishment of their crime, without taking from their
offspring and relatives that property, which may be the only means of saving
them from poverty and ruin.\rf{story3}{172}}
