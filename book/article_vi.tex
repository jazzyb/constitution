\addpart{Article VI}{National Sovereignty}
\cl{Clause 1}{Prior debts}
\cq{All Debts contracted and Engagements entered into, before the Adoption of
this Constitution, shall be as valid against the United States under this
Constitution, as under the Confederation.}

\dd{The Chairman}{The Chair recognizes Gouverneur Morris of Pennsylvania.}

\dd{Gouverneur Morris (PA)}{I move that Congress shall discharge the debts and
fulfill the engagements of the United States.\rf{farrand2}{377}  As I have
never been a public creditor, I can urge with more propriety the compliance
with public faith.  Holding the new Congress to this obligation will increase
support of the plan.\rf{farrand2}{414}}

\dd{The Chairman}{The floor is open to debate on the motion.}

\dd{Pierce Butler (SC)}{By discharging the debts I hope we do not intend to
compel payment to those \emph{blood-suckers}, who speculated on the distresses
of the soldiers who fought and bled for their country.\rf{farrand2}{392}  Our
veterans received much of their service pay in the form of deprecated state
notes.  Unable to find anyone to accept them on par, many have been forced to
sell them to speculators for shillings on the pound.\rf{richards}{79-81}  We
should make some distinction between these two classes of
people.\rf{farrand2}{392}}

\dd{George Mason (VA)}{I agree with Mr.\ Butler.  The word ``shall'' is an
issue.  If we \emph{compel} Congress to discharge the debts, it will beget
speculations and increase that pestilent practice of stock-jobbing the
securities guaranteed to our veterans.  In this case there is a great
distinction between the original creditors and those who defrauded the
ignorant and distressed.

I am sensible of the difficulty of drawing the line, but we must make some
attempt.  Even fair purchasers at 4 to 1 should not stand on the same footing
with the first holders.  The interest they receive even in paper is equal to
their purchase price.  I want to allow Congress to buy up the securities
without requiring them to buy at face value.\rf{farrand2}{413}}

\dd{Elbridge Gerry (MA)}{Since the public has received the value of the
literal amount, that amount should be paid to somebody.  The frauds
perpetrated on the soldiers should have been foreseen.  Those poor, ignorant
people could not but part with their securities.  There are others who will
part with anything rather than be cheated of the capital of their advances.

If the public faith will admit --- and I am not clear that it would --- then I
would support a revision of the debt that compels restitution to the ignorant
and distressed who have been defrauded.  But as to the ``stock-jobbers,''
there is no reason for such criticism.  They keep up the value of the paper.
Without them there is no market.\rf{farrand2}{413}}

\dd{Oliver Ellsworth (CT)}{Rumor has it that you own a great deal of these
securities, Mr.\ Gerry.  Might that be why you support their redemption at
face value?\rf{farrand3}{171}}

\dd{Elbridge Gerry (MA)}{I do not now, nor have I ever, owned even ten pounds
of such notes, Mr.\ Ellsworth.  I propose only that the public debt should be
made neither better nor worse by the new system but stand precisely on the
same ground as the Articles of Confederation.\rf{farrand3}{240}}

\dd{William Johnson (CT)}{We do not need this provision.  It is superfluous.
The debts are debts of the United States, of the great body of America.
Changing the government cannot change the obligation of the United States,
which is transferred to the new government.\rf{farrand2}{414}}

\dd{James Madison (VA)}{The motion makes the obligation explicit in order to
prevent dishonest interpretations.  After the revolution, some debtors to
British subjects tried to get out of their debts by asserting that the old
government had been dissolved at the end of the war.\rf{farrand2}{377}}

%\dd{Edmund Randolph (VA)}{I agree that we should make this explicit in the
%Constitution.  I move instead that all debts contracted and engagements
%entered into by or under the authority of Congress shall be as valid against
%the United States under this Constitution as under the
%Confederation.\rf{farrand2}{414}}

\summary{\emph{All debts contracted and engagements entered into under the
authority of Congress shall be as valid against the United States under this
Constitution as under the Confederation.}\rf{farrand2}{414}  A change in the
political form of society should have no power to produce a dissolution of any
of its moral obligations.  This doctrine can be deduced from the first
principles of moral duty and the law of nature, applied to the intercourse and
social relations of nations.  This explicit declaration serves as a solemn
recognition of the obligations of the United States to its public creditors,
both foreign and domestic.\rf{story3}{691-2}}

\cl{Clause 2}{National supremacy}
\cq{This Constitution, and the Laws of the United States which shall be made
in Pursuance thereof; and all Treaties made, or which shall be made, under the
Authority of the United States, shall be the supreme Law of the Land; and the
Judges in every State shall be bound thereby, any Thing in the Constitution or
Laws of any State to the Contrary notwithstanding.}

\dd{The Chairman}{The Chair recognizes James Madison of Virginia.}

%\mn{Congressional veto of state laws}
\dd{James Madison (VA)}{Many of my colleagues believe that the judicial
authority of the Supreme Court will keep the states in their proper limits.
My answer is that it is easier to prevent a bill's passage than to declare a
law void after the fact.  If state law aggrieves or encroaches on the rights
of the people, will the average citizen be able to support an appeal against
their state to the Supreme Court?  Would a state that disobeyed the
legislative rights of the Union readily obey a judicial decree in support of
them?  The necessary recurrence to force in the event of such disobedience is
an evil that our new Constitution is intended to prevent.\rf{farrand3}{134-5}

To this end I move that Congress should have authority to veto all state laws
that they judge to be improper.  Experience has shown the constant tendency in
the states to encroach on the federal authority, to violate national treaties,
to infringe on the rights and interests of one another, and to oppress
minority parties within their jurisdictions.  A veto is the mildest power that
we can devise to prevent such mischief and would render the use of force
unnecessary.\rf{farrand1}{164-5}}

\dd{The Chairman}{The floor is open to debate on the motion.}

\dd{Charles Pinckney (SC)}{This universal veto is the cornerstone of an
efficient national government.  If the states are left to themselves, it will
become impossible to defend the national prerogatives, however extensive they
might be on paper.  The colonies benefited from the veto of the crown under
British rule, and the states are more one nation now than the colonies were
then.\rf{farrand1}{164}}

\dd{Roger Sherman (CT)}{I am uncertain about making such a veto universal.
The laws of the national government will already be paramount to the
states.\rf{farrand2}{390}  The power, therefore, assumes a wrong principle:
that an unconstitutional state law could be a valid law.\rf{farrand2}{28}  But
we might be able to define specific cases in which Congress should exercise a
veto.\rf{farrand1}{166}}

\dd{John Dickinson (DE)}{We could not possibly enumerate an explicit list of
cases, Mr.\ Sherman.  Our choice is simple:  We can either subject the states
to the danger of being injured by the power of the national government or
subject the latter to the danger of being injured by the
former.\rf{farrand1}{167}  We must leave discretion to one side or the other.
The way I see it, the power is more safely lodged in the national
government.\rf{farrand1}{166}}

\dd{Elbridge Gerry (MA)}{I am against every unnecessary power, and --- unless
we want Congress to enslave the states --- I cannot see the use of this one.
I have no objection to authorizing vetoes of explicit acts, such as paper
money and similar measures, but a universal veto could extend to the
regulation of the militia --- a matter to which the existence of the states
depends.

The people will never accept the motion as is.  No amateur political theorist
--- and there are enough of that sort about --- has in any pamphlet or
newspaper suggested or even conceived of the idea.  New territories will have
separate views from the old states, and will hesitate to enter the Union under
this limitation.  If they do, they may enter under some foreign influence.  In
such a case, are they to participate in the veto over the laws of other
states?\rf{farrand1}{165-6}}

\dd{James Wilson (PA)}{There is no instance in which the laws say that
individuals should be bound in one case and at liberty to judge whether they
will obey or disobey in another.  The case of the states is analogous.
Federal liberty is to the states what civil liberty is to individuals, and the
states are no more willing to purchase it by the necessary concession of their
political sovereignty than the savage is to purchase civil liberty by the
surrender of his personal sovereignty, which he enjoys in a state of nature.

Among the sentiments expressed during the first Continental Congress was that
Virginia is no more, Pennsylvania is no more, Massachusetts is no more.  We
are now one nation.  We must bury all local interests and distinctions.  This
language continued for some time until the tables began to turn.  No sooner
did the state governments form than they began to display their ambition and
jealousy.  Each endeavored to cut a slice from the common loaf to add to his
own morsel until at length they frittered down the Confederation to its
present impotent condition.

The business of this convention is to correct the vices of the Articles of
Confederation.  One such vice is the want of an effectual control in the whole
over its parts.  What danger is there that the Union will unnecessarily
sacrifice one of its parts?  But reverse the case and leave the whole to the
mercy of each state:  Will not the general interest be continually sacrificed
to the local?\rf{farrand1}{166-7}}

\dd{Gunning Bedford (DE)}{You ask, Mr.\ Wilson, ``What danger is there that
the Union may sacrifice a state?''  Delaware will have an insignificant share
in Congress while Pennsylvania and Virginia may wind up possessing one-third
of the whole.  Is there no difference in their interests, no rivalry of
commerce or manufacture?  Won't the large states crush the small whenever they
stand in the way of their ambitions?  Based on the conduct of their delegates,
it seems as if Pennsylvania and Virginia wish for a system in which they will
have an enormous and monstrous influence.

Let me ask:  How should the proposed veto be exercised?  Are the states to
suspend their laws in the most urgent cases until they can be sent seven or
eight hundred miles and undergo the deliberations of a body who may be
incapable of judging them?  Must Congress sit continually to revise the laws
of the states?  And after all that, if a state ignores the congressional veto,
mustn't the national government still resort to force as the only ultimate
remedy, in this as in any system?\rf{farrand1}{167-8}}

\dd{George Mason (VA)}{I would like to hear answers to these obvious
objections, Mr.\ Madison.\rf{farrand2}{390}}

\dd{James Madison (VA)}{The difficulties Mr.\ Bedford stated are worthy of
attention and ought to be answered before the Committee accepts my motion.
The case of urgent laws would require some national agent in each state to
give temporary assent at least.  This was the practice in the colonies before
the revolution.  I would also be willing to amend the motion to place the veto
power in the Senate alone so that the more numerous and expensive house would
not be obliged to sit constantly.\rf{farrand1}{168}}

\dd{John Lansing (NY)}{There will never be enough leisure for such a task.
One should assume, by the most moderate calculation, that Congress will
receive an act to approve every day.  Will even the Senate be competent
judges?  Can a senator from Georgia judge of the expediency of a law that will
operate in New Hampshire?

The scheme will be more harmful than anything experienced under British
rule.\rf{farrand1}{337}  It is totally novel.  There is no parallel to it
anywhere.\rf{farrand1}{250}}

\dd{John Rutledge (SC)}{The acceptance of this motion will damn the
ratification of the Constitution and \emph{ought} to damn it.  No state would
ever agree to bind themselves hand and foot in this manner.\rf{farrand2}{391}}

%\dd{William Paterson (NJ)}{Instead of Mr.\ Madison's motion, I move that we
%just explicitly state that the Constitution, laws, and treaties of the United
%States are the supreme law of the land, and the judges in every state shall be
%bound by them.\rf{farrand1}{245}}
%
%\dd{George Mason (VA)}{I have concerns with the new motion.  By lumping
%treaties in with the supreme law of the land, we would give the President and
%Senate in many cases an exclusive power of legislation, which might have been
%avoided by requiring the assent of the House.\rf{farrand2}{639}
%
%Moreover, the power annuls the rights of the people.  The bill of rights is a
%part of the constitution of Virginia, but the judges of Virginia would be
%obliged to defer to the laws of the general government.  Consequently, the
%rights of the citizens of Virginia are given up.  Where is the barrier between
%the government and the rights of its citizens, as secured in our own state
%governments?\rf{elliot3}{266}}

\summary{With all due respect to Mr.\ Madison, the Committee rejects the
motion to give Congress a veto over state laws.  As useful as the power might
be, there would be too much difficulty with its implementation and
ratification.

Yet if we are to establish a national government, that government should be
supreme.  To this end the Committee approves the following power, which is
more effective and less liable to objections: \emph{This Constitution, the
laws of the United States made in pursuance thereof, and all treaties shall be
the supreme law of the land, and the judges in every state shall be bound
thereby.}  The propriety of this power is self-evident.  It results from the
very nature of the Constitution itself.\rf{story3}{693}}

\cl{Clause 3}{Oaths of office}
\cq{The Senators and Representatives before mentioned, and the Members of the
several State Legislatures, and all executive and judicial Officers, both of
the United States and of the several States, shall be bound by Oath or
Affirmation, to support this Constitution; but no religious Test shall ever be
required as a Qualification to any Office or public Trust under the United
States.}

\mn{Requirement of oaths}
\dd{The Chairman}{The Chair recognizes Edmund Randolph of Virginia.}

\dd{Edmund Randolph (VA)}{I move that the executive, legislative, and
judicial powers of the state governments should be bound by oath to support
the national Constitution.\rf{farrand1}{203}}

\dd{The Chairman}{The floor is open to debate on the motion.}

\dd{Roger Sherman (CT)}{The motion unnecessarily intrudes into the states'
jurisdictions.\rf{farrand1}{203}}

\dd{Edmund Randolph (VA)}{It is necessary to prevent the competition between
the state and general governments that we have felt under the Confederation.
The state officers are already under oath to the states.  To prevent undue
partiality, we should also bind them to support the national
government.\rf{farrand1}{203}  We are erecting a supreme national authority.
Can we give it too many sinews?\rf{farrand1}{207}}

\dd{Luther Martin (MD)}{The clause is unnecessary.  If new oaths are contrary
to those already taken by the state officers, then the new oaths will be
improper.  If not, then the oaths already taken are
sufficient.\rf{farrand1}{203}}

\dd{Hugh Williamson (NC)}{I do not like the motion either.  We have as much
reason to require a reciprocal oath of fidelity from national officers to the
states.\rf{farrand2}{87}}

\dd{Elbridge Gerry (MA)}{I agree with you in principle, Mr.\
Williamson.\rf{farrand1}{203}  But oaths to the national government could have
one practical benefit:  Under the Confederation the officers of both
governments have considered the states distinct from the federal government
and have always given a preference to the former.  The current motion will fix
that error.\rf{farrand2}{88}  Moreover, consistency requires that the
national officers should swear the same oaths to support the national
government.\rf{farrand2}{87}}

\dd{James Wilson (PA)}{Oaths provide only false security.  A good government
does not need them, and a bad government should not be supported.  I am
concerned that binding the officers of the national government to a version of
that government could prevent them from making amendments.\rf{farrand2}{87}}

\dd{Nathaniel Gorham (MA)}{I also doubt oaths will be of much use, but there
is no inconsistency between swearing an oath and amending the Constitution.
An amendment could never be regarded as a breach of the Constitution or any
oath to support it.\rf{farrand2}{88}}

\dd{Charles Pinckney (SC)}{If the Chair will allow, I would like to make a
related motion while we are on the subject of oaths.}

\dd{The Chairman}{Very well.  If there are no more comments on the current
proposition, then the floor will hear from Mr.\ Pinckney.}

\mn{Religious tests}
\dd{Charles Pinckney (SC)}{I move that no religious test shall ever be
required as a qualification to any office under the authority of the United
States.\rf{farrand2}{468}  The world will expect this from us in the
establishment of a system founded on republican principles and in an age so
liberal and enlightened as the present.\rf{farrand3}{122}}

\dd{The Chairman}{The floor is open to debate on Mr.\ Pinckney's motion.}

\dd{Roger Sherman (CT)}{The precaution is unnecessary.  The prevailing
liberalism across the country is security enough against such
tests.\rf{farrand2}{468}}

\dd{Edmund Randolph (VA)}{The exclusion of religious tests from oaths prevents
officers from being bound to support one mode of worship or to adhere to a
particular religious doctrine.  It protects religious freedom by putting them
all on the same footing.  A man of abilities and character, of any sect
whatsoever, may be admitted to any office or public trust.  How many different
denominations compose America?  And how many different denominations will
be in Congress?  There are so many in the United States --- and being
prevented from establishing any one in prejudice to the rest by this clause
--- that they will forever oppose all attempts to infringe religious
liberty.\rf{farrand3}{310}}

\dd{Luther Martin (MD)}{My liberal colleagues should count me among those so
unfashionable as to think that a belief in the existence of God and in a
future state of rewards and punishments would be some security for the good
conduct of our rulers.  In a \emph{Christian} country we should at least hold
some distinction between Christians of different denominations and downright
atheism or paganism.\rf{farrand3}{227}}

\summary{\emph{All officers and representatives, both of the states and the
national government, shall be bound by oath or affirmation to support this
Constitution.}  The people have a plain right to require some guarantee from
every officer that he will be conscientious in his duty.  If in the ordinary
administration of justice oaths are required of those who try and testify, to
guard against malice, falsehood, and evasion, then surely like guards should
be required in the administration of high public trusts --- especially in such
as may concern the welfare and safety of the whole community.

However, the Committee is aware of denominations --- particularly Quakers ---
who are conscientiously scrupulous of taking oaths.  To prevent any unjust
exclusion from office, the Committee has chosen to permit a solemn affirmation
to be made instead.\rf{story3}{702-3}

\emph{But no religious test shall ever be required to qualify for any office.}
Bigotry is unceasingly vigilant in its schemes to secure itself an exclusive
influence over the human mind, and intolerance is ever ready to arm itself
with all the terrors of the civil power, to exterminate those who doubt its
dogmas or resist its infallibility.  This provision, however, will cut off
forever every pretense of an alliance between church and state in the national
government.\rf{story3}{705}  No sect will ever secure themselves a monopoly of
all the offices of trust and profit under this Constitution.\rf{story3}{709}}
