\newcommand{\prefacetitle}{Preface}
\chapter*{\utopia{\prefacetitle}}
\markboth{\lmr{\textsc{\prefacetitle}}}{\lmr{\textsc{\prefacetitle}}}
\addcontentsline{toc}{chapter}{\prefacetitle}

\begin{quote}
	\texttt{We shall not cease from exploration} \\
	\texttt{And the end of all our exploring} \\
	\texttt{Will be to arrive where we started} \\
	\texttt{And know the place for the first time.}
	\begin{flushright}
		--- T.\ S.\ Eliot, ``Little Gidding''
	\end{flushright}
\end{quote}

Fifteen years ago curiosity motivated me to read the journals of James
Madison.  Those journals contain an incredibly detailed record of the debates
that occurred in Philadelphia over the summer of 1787 --- a series of debates
that would produce the United States Constitution.  I wanted to find out why
the United States government was constructed the way that it was.  I wanted to
understand the original intent of the framers of the Constitution.  I assume
you are reading this book with similar motives.

Since high school, my impression of the drafting of the Constitution had been
that, with the exception of a few necessary compromises, the basic structure
of the United States government had emerged fully formed from the framers'
minds, like Athena from the head of Zeus.  Yet the more I read the primary
sources, the less agreement I found among the framers.  Even the delegates who
signed their names to that document and fought for its ratification scarcely
seemed to agree on any one point.  There is not a single, original intent to
be found among them.

What I discovered instead was a grand conversation, a series of intense
debates concerning the nature of government and society.  From the framers'
many disagreements arose profound questions, like:  What is the purpose of
government?  What qualities should we require in our representatives?  Who
should elect?  How can we incentivize and equip the best citizens to serve,
without inviting corruption and abuse of power?  I wrote this book to share
with you these important questions and the great arguments they produced.

This book presents these debates as a series of dialogues related to each
clause of the original Constitution, in order, from the Preamble to Article
VII and the Endorsement.  I've based most dialogue on the convention journals
of the delegates themselves, primarily Madison, but some also come from
transcripts of the state ratifying conventions, letters, and second-hand
accounts.

This dialogue format required that I take several liberties with the source
materials.  During the historical convention the debate over a particular
provision might have begun on one day and not resumed or concluded until weeks
or months later, and the comments I've included from state ratifying
conventions and letters would, of course, have been entirely disconnected from
the physical debates in Philadelphia.  I have, therefore, edited the framers'
quotes within each section to give the illusion of a single, continuous
debate.  This often meant massaging the dialogue so that the framers respond
to one another, when the original quotes might have occurred at different
times and in different contexts.  On other occasions multiple delegates would
make similar arguments, and so as not to have them repeat each other, I've
consolidated their arguments into one.

I've also added two fictional characters to the debates: \emph{The Chairman}
and \emph{The Committee of Detail}.  While the actual convention did have a
real Chairman, George Washington, and a Committee of Detail, who wrote the
original draft of the Constitution, the fictional versions in this book serve
to give each debate a clear beginning and end.  Each section begins with the
Chairman, who introduces the initial motion for that clause and gives an
overall structure to the debate, and ends with the Committee of Detail
presenting a summary explaining why the clause in question was accepted as it
was.  I've borrowed many of these summaries from the Federalist papers and
early Supreme Court opinions.

Additionally, I've modernized the spelling, punctuation, vocabulary, and
grammar of the eighteenth-century language to ease the comprehension for
twenty-first century readers.  You should, therefore, not consider any line of
dialogue to be an exact, historical \emph{quote} but rather my own
\emph{paraphrase} of that delegate's actual position.  If you do wish to find
the original source for any particular statement, I've cited everything using
superscripts, which you can look up in the back of the book.  If, however, you
find a delegate's argument unintelligible or factually inaccurate, the fault
is all my own.

But wherever I thought it appropriate, I sought to retain the framers'
original words.  This means that some of the language in this book might come
across as insensitive.  For example, I've preserved the use of masculine
singular pronouns, he/him/his, when referring to public officers, where a
modern text might use she or they.  Likewise, I've preserved the terms
``blacks'' and ``negroes'' when the framers discuss slaves.  You'll also read
comments containing negative stereotypes of American Indians, Catholics, and
others.  I believe that updating such language to be more acceptable to modern
ears would be to obscure our history and the framers' actual opinions.

While some of the framers' beliefs and practices were bigoted, even by the
standards of their own time, I find their wisdom in other matters is often
timeless, and their warnings sometimes prescient.  And that, ultimately, is
the reason we should engage with their arguments, even the bad ones.  The
framers' debates are part of a conversation we Americans continue with both
our forebears and our grandchildren.  The articles and amendments of our
present Constitution, far from settling the matter, represent only one
possible set of answers to the questions raised in this grand conversation.
The history of America is the history of each imperfect generation wrestling
with these difficult questions.

\smallskip
May we ever seek more perfect answers together.

\begin{flushright}
Jason M Barnes \\
Mount Pleasant, MI \\
January 15, 2023
\end{flushright}
