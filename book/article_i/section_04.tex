\ch{Article I, Section 4}{Regulation and Assembly of Congress}
\cl{Clause 1}{Regulation of elections}
\cq{The Times, Places and Manner of holding Elections for Senators and
Representatives, shall be prescribed in each State by the Legislature thereof;
but the Congress may at any time by Law make or alter such Regulations, except
as to the Place of Chusing Senators.}

\dd{The Chairman}{The Chair recognizes William Davie of North Carolina.}

\dd{William Davie (NC)}{I move that the state legislatures may prescribe the
times, places, and manners of holding elections for their congressional
representatives, but Congress may have the power to alter such
regulations.\rf{farrand2}{239}  With this power Congress could defend the
people against ambitious designs of the state legislatures.  Take Rhode
Island:  An abandoned faction have seized the reins of government and
frequently refuse any representation in Congress.  If Congress had the power
of making the law of elections operate throughout the United States, that
trifling state would not have been able to withhold its federal
representation.

Government cannot exist without this means of self-preservation.  The
Confederation is the only instance of a government without such means, and it
is a nerveless system, as inadequate to every purpose of government as it is
to the security of the liberties of the American people.  When Congress has
this power over elections, they can, in spite of any faction in any state,
give the people their representation.\rf{farrand3}{344-5}}

\dd{The Chairman}{The floor is open to debate on the motion.}

\dd{Charles Pinckney (SC)}{I support giving the state legislatures the power
to regulate elections, but I object to the clause allowing Congress to alter
them.  We must rely on the states in such cases.\rf{farrand2}{240}}

\dd{Roger Sherman (CT)}{It might be safer to retain the clause regardless of
our confidence in the states.\rf{farrand2}{241}}

%\dd{Nathaniel Gorham (MA)}{It would be as improper to withhold this power from
%Congress as it would be to prevent the British Parliament from regulating the
%circumstances of elections and leaving the business to the counties
%alone.\rf{farrand2}{240}}

\dd{James Madison (VA)}{Since the state legislatures might ignore the common
interest in favor of their local convenience or prejudice, we cannot give them
an uncontrolled right to regulate elections.  The method of an election
influences its results.  Whether the electors vote by ballot secretly or aloud
publicly or should assemble at this place or that, these and many other points
would depend on the state legislatures and might materially affect the
appointments.  We cannot foresee all the ways the states might abuse this
discretionary power.  Whenever the legislatures would have a favorite measure
to carry, they would take care to mold their regulations to favor the
candidates they wished to succeed.\rf{farrand2}{240-1}

Having said all this, we should amend the motion with an exception in the case
of senators.  If Congress can fix the \emph{place} of choosing the senators,
it might compel the state legislatures to elect them in a different location
from their usual sessions, which would produce some inconvenience and is not
necessary for the purpose of the rule.  But it would still be necessary to
give the general government control over the time and manner of choosing the
senators to prevent its own dissolution.\rf{farrand3}{311}}

%\dd{Gouverneur Morris (PA)}{Without some oversight by Congress, a state
%legislature might falsify the result of an election and then make no
%provisions for new ones.\rf{farrand2}{241}}

\summary{\emph{The state legislatures shall exercise the power of regulating
their elections, but Congress shall have the power} in extraordinary
circumstances \emph{to modify those regulations} for their own
self-preservation and to defend the right of the people to elect their
representatives.\rf{story2}{282}

\emph{But Congress shall have no power to change the place of choosing
senators.}  Because the senators are appointed by the state legislatures, it
would be unnecessary and unbecoming for Congress to prescribe the place where
they should meet.\rf{story2}{292}}

\cl{Clause 2}{Annual meeting of Congress}
\cq{The Congress shall assemble at least once in every Year, and such Meeting
shall be on the first Monday in December, unless they shall by Law appoint a
different Day.\footnote{This clause is modified by Amendment XX, Section 2
(1933):  ``The Congress shall assemble at least once in every year, and such
meeting shall begin at noon on the 3d day of January, unless they shall by law
appoint a different day.''}}

\dd{The Chairman}{The Chair recognizes Nathaniel Gorham of Massachusetts.}

\dd{Nathaniel Gorham (MA)}{I move that Congress meet on the first Monday in
December every year.\rf{farrand2}{196}  By fixing an exact date in the
Constitution, we will prevent disputes in Congress and allow the states to
coordinate the times of their elections.\rf{farrand2}{198}}

\dd{The Chairman}{The floor is open to debate on the motion.}

\dd{James Madison (VA)}{I disagree with the motion.  If some pressing need
were to cause Congress to meet and finish their session a short time before
the fixed annual period, it would be extremely inconvenient to reassemble so
quickly and without the least necessity.  The Constitution should only require
that at least one meeting occur every year.  Then Congress can fix the date by
law.  The states should have no difficulty adjusting their elections to
it.\rf{farrand2}{197, 198}}

%\dd{Nathaniel Gorham (MA)}{The charters and constitutions of the New England
%states have long fixed their annual meetings without any
%inconvenience.\rf{farrand2}{198}}

\dd{Gouverneur Morris (PA)}{We should not tie Congress down to a particular
date.  The public business might not even require they meet every
year.\rf{farrand2}{198}}

\dd{Oliver Ellsworth (CT)}{Congress won't know whether the public business
requires their meeting until they assemble.  I don't see why we shouldn't fix
a date.  We're as capable of judging it as Congress is.\rf{farrand2}{198}}

\dd{Rufus King (MA)}{Congress will have no need to meet every year.  One of
the great vices of our present system is that of legislating too much.  The
states have the greatest legal jurisdiction.  The national government's will
be smaller, primarily concerned with commerce and revenue.  Once those issues
are settled, alterations will be rarely needed and easily
made.\rf{farrand2}{198}}

\dd{George Mason (VA)}{Annual congressional meetings are essential to the
preservation of the Constitution.  The great extent of the country will supply
the business.  Even if it doesn't, Congress, besides \emph{legislative}, will
have \emph{inquisitorial} powers.\rf{farrand2}{199}  They must meet frequently
to inspect the conduct of the public offices\rf{farrand2}{206} and, in
particular, the President.\rf{farrand2}{198}}

%\dd{Roger Sherman (CT)}{Not only should we fix the date of meetings, but we
%should require Congress to have frequent meetings.  If the date be changeable,
%disputes and difficulties will arise between the House and the Senate and
%between Congress and the states.  Frequent meetings of Parliament were
%required after the Glorious Revolution as an essential safeguard of
%liberty.\rf{farrand2}{199}}

\dd{Edmund Randolph (VA)}{I am against fixing any day irrevocably.  But since
there's no provision anywhere else in the Constitution for regulating the
periods of meeting, we'll need to fix some precise date until Congress can
provide their own.  We should add the words ``unless a different day shall be
appointed by law'' to give Congress the power to change it.\rf{farrand2}{199}}

\dd{James Madison (VA)}{If we must set a date, we should replace December with
May.  The former would require traveling to and from the capital in the most
inconvenient seasons of the year.  Additionally, Congress may want to adjust
their legislative sessions to those in Europe, which would pass measures in
the winter and of which news wouldn't arrive until the
spring.\rf{farrand2}{199}}

\dd{Oliver Ellsworth (CT)}{Summer sessions will interfere too much with
private business.  Almost all the members of Congress will be connected with
agriculture in some way.\rf{farrand2}{200}}

\dd{Edmund Randolph (VA)}{The initial date will be of no real significance if
Congress can vary it.  December would better suit the current dates of
elections in the state constitutions.  It's important we render our
innovations as convenient as possible.\rf{farrand2}{200}}

\summary{\emph{Congress shall be required to meet at least once every year.}
The people of America consider annual sessions to be a great security to
public liberty.\rf{story2}{293}  \emph{The date set in the Constitution shall
be the first Monday in December} because some initial date is needed,
\emph{but Congress will be allowed to change the date if another is more
convenient.}}
