\ch{Article I, Section 9}{Congressional Restrictions}
\cl{Clause 1}{Foreign slave trade}
\cq{The Migration or Importation of such Persons as any of the States now
existing shall think proper to admit, shall not be prohibited by the Congress
prior to the Year one thousand eight hundred and eight, but a tax or duty may
be imposed on such Importation, not exceeding ten dollars for each Person.}

\dd{The Chairman}{The Chair recognizes John Rutledge of South Carolina.}

\dd{John Rutledge (SC)}{I move that no tax or duty shall be laid by Congress
on the migration or importation of such persons as the several states shall
think proper to admit; nor shall such migration or importation be prohibited.
My colleagues and I believe this provision is a necessary security for
southern interests.\rf{farrand2}{359}}

\dd{The Chairman}{The floor is open to debate on the motion.}

\dd{Luther Martin (MD)}{Allow me to speak more plainly than Mr.\ Rutledge on
this matter:  Our southern colleagues wish to prevent the general government
from banning the slave trade.  Notice the absence of the word ``slaves'' in
the motion.  They mean to avoid an expression that might ring as odious to
American ears, yet they are willing to admit into the system the things that
that expression signifies.\rf{farrand3}{210}

We have strong reasons to reject this motion.  First, as slaves count towards
the proportion of representatives, this motion would further encourage such
traffic.  Second, the privilege of importing slaves is unreasonable from a
defensive stand-point.  Slavery weakens one part of the Union, which the other
parts will be bound to protect.  And finally, such a feature in the
Constitution is inconsistent with the principles of the revolution and
dishonorable to the American character.\rf{farrand2}{364}}

\dd{John Rutledge (SC)}{I cannot see how this motion will encourage the
importation of slaves.  Nor do I fear insurrections.  I would readily exempt
the other states from an obligation to protect the South against them.
Religion and humanity have nothing to do with this question.  Interest alone
is the governing principle with nations.

The true question is whether or not the southern states shall be parties in
the Union.\rf{farrand2}{364}  If you think Georgia and the Carolinas will ever
agree to the plan unless their right to import slaves is secure, then your
expectation is in vain.  The people of those states would never be such fools
as to give up so important an interest.\rf{farrand2}{373} And if the northern
states consult their interest, they will not oppose an increase of slaves,
which would increase the commodities of which they will be the
carriers.\rf{farrand2}{364}}

\dd{Oliver Ellsworth (CT)}{Personally, I oppose the slave trade, but I am for
leaving the clause as it stands.  We should create as few objections to the
plan as possible.  Let every state import what it pleases.  The morality and
wisdom of slavery are considerations belonging to the states themselves.  What
enriches a part, enriches the whole, and the states are the best judges of
their particular interests.  The Confederation has never meddled with this
right, and I do not see any greater necessity for bringing it within the
policy of the new system.\rf{farrand2}{364, 369-70}}

% The following dialogue was added to Ellsworth's above:
%\dd{Roger Sherman (CT)}{Personally, I oppose the slave trade.  However, since
%the states have the right and it does not hinder the public good, we should
%create as few objections to the plan as possible.  It is best to leave the
%matter as we find it.  The abolition of slavery appears to be advancing in the
%Union state by state, and slavery will eventually end on its own.  I must also
%remind my colleagues that we really ought to be finishing our convention and
%submitting a plan to the states.\rf{farrand2}{369-70}}

\dd{George Mason (VA)}{Yes, the states have this right, Mr.\ Ellsworth, but
the same can be said for many other rights now to be given up.  The present
question concerns not the importation of slaves alone but the good of the
whole Union.  Slavery discourages art and industry, for the poor despise
labor when performed by slaves.  It prevents the immigration of whites, who
really enrich and strengthen a country.  It produces the most pernicious
effect on manners, for every slave master is born a petty tyrant.  And it will
inevitably bring the judgment of heaven on our country, for Providence
punishes national sins with national calamities.
%This infernal traffic originated from the avarice of British merchants, and
%the British government constantly checked Virginia's attempts to put a stop to
%it.

Western settlers call for slaves for their new lands.  Maryland and Virginia
have prohibited the trade, and North Carolina has practically done the same.
But all will have been in vain if Georgia and South Carolina are left at
liberty to fill the western country with slaves.  It is essential that we give
the national government the power to prevent the increase of
slavery.\rf{farrand2}{370}}

\dd{Oliver Ellsworth (CT)}{As I have never owned a slave, I cannot speak for
its effect on one's character.  But if you have so many moral issues with the
practice, Col.\ Mason, then you should advocate for freeing the slaves already
in the country --- perhaps beginning with your own.

Let us not interfere with the internal affairs of the states.  As population
increases, poor laborers will be so plentiful as to render slaves useless.
Slavery in time will not be a speck in our country.  Connecticut has made
provisions for abolishing it, and abolition has already taken place in
Massachusetts.\rf{farrand2}{370-1}}

\dd{Charles Pinckney (SC)}{If slavery be wrong, it is justified by the example
of all the world.  You will note its practice in Greece and Rome, as well as
the sanction given by France, England, Holland, and other modern states.  In
all ages, one-half of mankind has been enslaved.\rf{farrand2}{371}}

\dd{John Dickinson (DE)}{Authorizing the slave trade violates every principle
of honor and safety.  If England and France sanction the slave trade, they at
the same time exclude slaves from both their kingdoms.  Greece and Rome both
suffered slave rebellions.  I cannot believe the South will refuse to unite
without the clause, especially since Congress is unlikely to exercise the
power immediately.\rf{farrand2}{372-3}}

\dd{Charles Pinckney (SC)}{South Carolina can never accept the Constitution if
it prohibits the slave trade.  We have watched every proposed extension of
congressional power and guarded against meddling with the importation of
negroes.  If the southern states are left at liberty, they may one day ban the
trade themselves, as Virginia and Maryland have already
done.\rf{farrand2}{364-5}  I would, as a citizen of South Carolina, vote for
it myself.\rf{farrand2}{371}}

\dd{C. C. Pinckney (SC)}{If all the delegates of South Carolina were to sign
this Constitution and use their personal influence, we would fail to obtain
the assent of our constituents without this clause.  South Carolina and
Georgia cannot do without slaves.  As Col.\ Mason is aware, slaves multiply
much faster in Virginia.  She will gain by stopping the importations.  Her
slaves will rise in value, and she has more than she wants.  It would be
unjust to require South Carolina or Georgia to confederate on such unequal
terms.

The slave trade is in the best interest of the whole Union.  The more slaves,
then the more produce to employ the northern shipping trade --- the more
consumption also, which leads to more revenue for the common
treasury.\rf{farrand2}{371}}

\dd{Abraham Baldwin (GA)}{I had assumed that national interests alone were
before the convention, not those of this present local nature.  Georgia is
decided on this point.  Our constituents already suspect that the general
government is a means for the central states to subsume everything into their
vortex, that Georgia's distance will preclude her from an equal advantage, and
that she cannot prudently purchase it by yielding national powers.  From this
you might understand in what light our constituents would view an attempt to
abridge one of her favorite prerogatives.  Just like South Carolina, if left
to herself, Georgia will probably put a stop to the evil in
time.\rf{farrand2}{372}}

\dd{James Wilson (PA)}{If South Carolina and Georgia were really so disposed
to end their slave trades in any reasonable amount of time, they would never
refuse to unite because Congress \emph{might} prohibit the
trade.\rf{farrand2}{372}}

\dd{John Dickinson (DE)}{My colleagues on the other side have argued, rightly,
that the true question is whether the slave trade promotes or impedes the
happiness and interests of the nation.  But this question should be answered
by the national government, not the states particularly
interested.\rf{farrand2}{372}}

\dd{Rufus King (MA)}{If two states will not agree to the Constitution without
this clause, you can be sure that a great and equal opposition will arise from
the other eleven states if this motion is accepted.  We have given Congress
the power to tax all other imports.\footnote{See Article I, Section 8, Clause
1.}  Exempting slaves from the duty is an inequality that cannot fail to
strike at the commercial interests of the North.\rf{farrand2}{373}}

\dd{C. C. Pinckney (SC)}{A rejection of this motion would mean the exclusion
of South Carolina from the Union.\rf{farrand2}{372}  We should, however, make
slaves liable to the same taxes as other imports.  I believe this would be
right and reasonable and would remove one difficulty that this motion has
started.\rf{farrand2}{373}}

%\dd{Roger Sherman (CT)}{We are better off allowing the South to import slaves
%than parting with them.  I am opposed to a tax on slaves as making the matter
%worse --- it would imply they are \emph{property}.  If we give Congress the
%power to prohibit the slave trade, they will exercise it.  It would be their
%duty to exercise the power.\rf{farrand2}{374}}

\dd{Edmund Randolph (VA)}{I support some middle ground like that of Genl.\
Pinckney's suggestion.  I would sooner risk the Constitution than agree to the
motion as it stands.  It will revolt the Quakers, Methodists, and many others
in the states with no slaves.  On the other hand two states might be lost to
the Union without some compromise.\rf{farrand2}{374}}

\dd{Oliver Ellsworth (CT)}{I, too, will accept Genl.\ Pinckney's suggestion.
If we do not agree on this middle and moderate ground, we might lose Georgia
and South Carolina.  The states may fly into a variety of shapes and
directions and most probably into several confederations.  If we cannot unite,
there will be bloodshed.\rf{farrand2}{375}}

\dd{William Livingston (NJ)}{As a compromise, I move that the migration or
importation of such persons as the several states now existing shall think
proper to admit, shall not be prohibited by Congress prior to the year 1800,
but a tax or duty may be imposed on such migration or importation at a rate
not exceeding the average of the duties laid on imports.\rf{farrand2}{400}
The southern states will be allowed for a time to import slaves, which will be
taxed as other imports, and after twelve years Congress may prohibit the
trade.}

\dd{Abraham Baldwin (GA)}{The expression ``average of the duties laid on
imports'' will be too vague.  I propose ``ten dollars''
instead.\rf{farrand2}{417}  This is the common impost for most
articles.\rf{farrand2}{416}}

\dd{C. C. Pinckney (SC)}{South Carolina would prefer the year 1808 over
1800.\rf{farrand2}{415}}

\dd{James Madison (VA)}{Twenty years will produce all the mischief of the
original clause and will dishonor the national character more than saying
nothing about it in the Constitution.\rf{farrand2}{415}}

\dd{Gouverneur Morris (PA)}{The wording should be less ambiguous and read,
``importation of slaves into North Carolina, South Carolina, and Georgia shall
not be prohibited.''  The use of ``slaves'' makes clear the purpose of the
clause, and the reference to the three southern states indicates that this
part of the Constitution is in compliance with them.\rf{farrand2}{415}}

\dd{George Mason (VA)}{I am not against using the term ``slave,'' but we
should avoid explicit names lest we offend the people of those
states.\rf{farrand2}{415}}

\dd{Roger Sherman (CT)}{I prefer the euphemism better than ``slave.''  The old
Congress had declined using the term as it was not pleasing to some
people.\rf{farrand2}{415}  Moreover, a tax on the importation makes the matter
worse.  It implies that men can be property.\rf{farrand2}{416}}

\dd{Rufus King (MA)}{The tax in the second part is the price of the expiration
date in the first part.\rf{farrand2}{416}}

\dd{James Madison (VA)}{It is still wrong to admit in the Constitution the
idea that there can be property in men.  Duties on slaves do not follow since
they are not consumed like merchandise.\rf{farrand2}{417}}

\dd{Nathaniel Gorham (MA)}{Do not think of this as implying that slaves are
property.  Think of it as a means of discouraging the
importation.\rf{farrand2}{416}}

\dd{Roger Sherman (CT)}{On the contrary Mr.\ Gorham, the smallness of the duty
--- a mere ten dollars --- declares revenue, not discouragement, to be the
purpose.\rf{farrand2}{417}}

%\dd{Gouverneur Morris (PA)}{As the clause now stands, it implies that Congress
%can tax freemen who are imported.\rf{farrand2}{417}}
%
%\dd{George Mason (VA)}{The provision as it stands is necessary for the case of
%convicts in order to prevent the introduction of them.\rf{farrand2}{417}}

\summary{\emph{Congress shall not prohibit the importation of slaves prior to
the year 1808.}  This provision lays the foundation for banishing slavery out
of this country, though the period is more distant than some may wish.  As a
partial prohibition, \emph{imported slaves may be taxed up to ten
dollars.}\rf{debate1}{830}  This compromise is perhaps the best that can be
obtained.\footnote{For further information on this compromise see footnote on
p.\ \pageref{slave_trade_navigation_act}.}}

\cl{Clause 2}{Habeas corpus}
\cq{The privilege of the Writ of Habeas Corpus shall not be suspended, unless
when in Cases of Rebellion or Invasion the public Safety may require it.}

\dd{The Chairman}{The Chair recognizes Charles Pinckney of South Carolina.}

\dd{Charles Pinckney (SC)}{I move to secure the privilege and benefit of
habeas corpus,\footnote{The right to petition for a writ of habeas corpus is a
right in British common law that allows a prisoner to petition a court to
determine whether their arrest is lawful and, if not, to release them.  The
right prevents a person from being held in confinement without sufficient
ground. (\cite[p.\ 206]{story3})} and Congress shall not suspend it except on
the most urgent occasions and only then for a limited time not exceeding
twelve months.\rf{farrand2}{341, 438}}

\dd{The Chairman}{The floor is open to debate on the motion.}

\dd{John Rutledge (SC)}{Habeas corpus should be inviolable.  A suspension
could never be necessary throughout all the states at the same
time.\rf{farrand2}{438}}

\dd{Gouverneur Morris (PA)}{Congress should only suspend habeas corpus in
cases of rebellion or invasion.\rf{farrand2}{438}}

\dd{James Wilson (PA)}{Mr.\ Morris, I wonder if a suspension of habeas corpus
would ever be necessary in either case when judges have the discretion whether
to keep in jail or admit on bail.\rf{farrand2}{438}}

\summary{\emph{Congress shall have the power to suspend the writ of habeas
corpus \emph{only} when rebellion, invasion, or the pubic safety may require
it.}}

\cl{Clause 3}{Bills of attainder and ex post facto laws}
\cq{No Bill of Attainder or ex post facto Law shall be passed.}

\dd{The Chairman}{The Chair recognizes Elbridge Gerry of Massachusetts.}

\dd{Elbridge Gerry (MA)}{I move that Congress shall not pass any bills of
attainder\footnote{Bills of attainder are legislative acts that declare legal
guilt and inflict punishment upon a person without the benefit of a fair
trial. (\cite[p.\ 209]{story3})} or ex post facto laws.\footnote{Ex post facto
laws retroactively criminalize or punish an action that was not criminal or
punishable when the action was originally committed. (\cite[p.\
212-3]{story3})} The restriction is more vital in Congress than the states.
Since the number of legislators in Congress are fewer, they are more to be
feared.\rf{farrand2}{375}}

\dd{The Chairman}{The floor is open to debate on the motion.}

\dd{Oliver Ellsworth (CT)}{A prohibition on ex post facto laws is completely
unnecessary.  There is no lawyer --- no layman --- who would not declare that
they are void of themselves.\rf{farrand2}{376}}

\dd{James McHenry (MD)}{Indeed.  To say that Congress shall pass no ex post
facto law is the same as declaring they shall not do a thing contrary to
common sense --- that they shall not criminalize that which is not a
crime.\rf{farrand2}{379}}

\dd{James Wilson (PA)}{The provision would bring undesirable scrutiny on the
Constitution.  The people will think we either were ignorant of the first
principles of legislation or intended to constitute a government that would be
so.\rf{farrand2}{376}}

\dd{Daniel Carroll (MD)}{Gentlemen, experience overrules your concerns.
Regardless of how lawyers or laymen or others might view ex post facto laws,
the state legislatures have passed them, and they have taken
effect.\rf{farrand2}{376}}

\dd{James Wilson (PA)}{But if like prohibitions in the state constitutions
have no effect, then what use will they be in this
Constitution?\rf{farrand2}{376}}

\dd{Hugh Williamson (NC)}{Such a prohibition exists in the constitution of
North Carolina.  While it has been violated, it has done good there --- and
may do good here --- because the judges can use it to overturn those
laws.\rf{farrand2}{376}}

\dd{George Mason (VA)}{The wording of the motion does not make sufficiently
clear that the ex post facto prohibition is limited to cases of a criminal
nature, and no legislature ever did or can altogether avoid them in civil
cases.\rf{farrand2}{617}}

%\dd{Elbridge Gerry (MA)}{I think the prohibition should extend to civil cases
%as well.\rf{farrand2}{617}}

\dd{John Dickinson (DE)}{In British common law ``ex post facto'' only applies
to criminal cases.  But I think we should try to restrain the states from
passing retrospective laws in civil cases, as well.\rf{farrand2}{448-9}}

\dd{Gouverneur Morris (PA)}{I agree with others that the ex post facto
prohibition does not belong, but the precaution against bills of attainder is
essential.\rf{farrand2}{376}}

\summary{\emph{Congress shall pass no bills of attainder or ex post facto
laws.}  This clause is perhaps unnecessary.  Bills of attainder are contrary
to the separation of powers, and ex post facto laws are contrary to
legislative principles.  However, the Committee adds the prohibition for
the sake of clarity.}

\cl{Clause 4}{Direct taxation}
\cq{No capitation, or other direct, Tax shall be laid, unless in Proportion to
the Census or Enumeration herein before directed to be taken.\footnote{This
clause is superseded by Amendment XVI (1913):  ``The Congress shall have power
to lay and collect taxes on incomes, from whatever source derived, without
apportionment among the several States, and without regard to any census or
enumeration.''}}

\dd{The Chairman}{The Chair recognizes Elbridge Gerry of Massachusetts.}

% Does Gerry's motion farrad2, p. 350 + discussion pp. 357-8 fit here?
\dd{Elbridge Gerry (MA)}{I move that no capitation tax\footnote{Capitation
taxes (or \emph{poll taxes}) are fixed sums levied on every liable individual.
(\cite[p.\ 424]{story2})} shall be laid unless in proportion to the census
herein before directed to be taken.\rf{farrand2}{169}}

\dd{The Chairman}{The floor is open to debate on the motion.}

\dd{George Read (DE)}{We should insert the words ``or other direct tax'' after
``capitation taxes.''  Otherwise, Congress might use the power to readjust
past requisitions based on the results of the census.  The addition of direct
taxes will give another cast to the meaning and take away the
pretext.\rf{farrand2}{618}}

\dd{Luther Martin (MD)}{The power to impose direct taxes on the people is a
dangerous and oppressive power in the national government.  The states are
much better judges of the sums that can be collected from their citizens by
direct taxation and of the manner in which it could be raised.  The general
government should only have the power to lay direct taxes in the event of a
state's delinquency or some other case of absolute
necessity.\rf{farrand3}{205}}

\summary{The Committee accepts Mr.\ Gerry's motion with Mr.\ Read's amendment
regarding direct taxes.}

\cl{Clause 5}{Export taxes}
\cq{No Tax or Duty shall be laid on Articles exported from any State.}

\dd{The Chairman}{The Chair recognizes George Mason of Virginia.}

\dd{George Mason (VA)}{We must expressly prohibit Congress from taxing exports
lest they raise price of American goods on the foreign market.  I hope the
North does not mean to deny the South this security.  Our southern
constituents are jealous for the productions of the staple states.  To this
end I move that no tax, duty, or imposition shall be laid by Congress on
articles exported from any state.\rf{farrand2}{305-6}}

\dd{The Chairman}{The floor is open to debate on the motion.}

\dd{Gouverneur Morris (PA)}{This motion is so radically objectionable that it
might sabotage the whole plan.  In some cases it would be unfair to tax
imports without also taxing exports, and exports will often be the easier of
the two to tax.\rf{farrand2}{306}}

\dd{James Madison (VA)}{I, too, disagree with the motion.  Congress may
benefit from taxing exports for which America is unrivaled in foreign markets
--- like tobacco.  The burden of such taxes will admittedly fall heaviest on
the southern states, who are the primary exporters.  But it is only right they
should pay more revenue as they will have the most need of naval protection.
Remember, we are not framing a Constitution for only this present moment.
Time will equalize the states in this matter.\rf{farrand2}{306-7}}

\dd{George Mason (VA)}{Other nations raise tobacco and are as capable of
raising it as Virginia.  The taxation of that article would harm the trade of
Virginia.\rf{farrand2}{363}  This is evident from Virginia's own experiment
with export taxes, which benefited Maryland on the foreign
market.\rf{farrand2}{308}}

\dd{John Mercer (MD)}{Mr.\ Madison, you claim that the South has the most need
of naval protection.  The reverse is the case.  Were it not for promoting the
carrying trade of the northern states,\footnote{See the debate surrounding
navigation acts in Article I, Section 8, Clause 3.} the southern states could
send their produce on foreign ships, where it would not need our protection.

I am against giving Congress the power to tax exports.  It would encourage the
raising of crops for domestic use to the detriment of crops for foreign
export.  Under the Confederation the commercial states have the right to tax
both the imports and exports of their non-commercial neighbors.  It is enough
for them to sacrifice one-half.\rf{farrand2}{307-8}}

\dd{James Wilson (PA)}{The power to tax exports is proper in itself.  Since
the states have demonstrated they cannot exercise the power separately with
propriety, we must vest it in them collectively, through Congress.  If we do
not give Congress an exclusive power over exports, the commercial states will
lay taxes on the foreign-bound commodities of their non-exporting neighbors,
and the latter will see none of the benefits of the revenue.\rf{farrand2}{306}
It would be a grave injustice to New Jersey, Connecticut, and others to leave
them subject to the extortion of their commercial
neighbors.\rf{farrand2}{307}}

\dd{John Langdon (NH)}{Mr.\ Wilson is right.  New Hampshire will be subject to
the taxes of the states exporting her produce.  I cannot allow
it.\rf{farrand2}{359}  We should prohibit states from taxing the produce of
their neighbors altogether.\rf{farrand2}{361}  If the southern states fear
that the North will oppress their trade, a better way of guarding against it
would be to require two-thirds or three-fourths of Congress to consent in some
cases.\rf{farrand2}{359}}

\dd{Elbridge Gerry (MA)}{Congress cannot be trusted with the power to tax
exports.  It might ruin the country if it were exercised partially ---
advantaging one part while depressing another.\rf{farrand2}{307}}

\dd{Gouverneur Morris (PA)}{However we form Congress, it will, if inclined, be
able to ruin the country.  All countries tax their particular exports, as
France does with her wine and brandy.  We may even use the power to retaliate
against economic rivals.  A tax on lumber would fall on the West Indies and
punish their restriction on our trade.  The same is true of live-stock and
flour.

Taxes on exports are a necessary source of revenue.  For a long time the
people of America will not have enough money to pay direct taxes.  Seize and
sell their effects and you will push them into revolts.\rf{farrand2}{307}}

\dd{Oliver Ellsworth (CT)}{There are solid reasons against giving this power
to Congress.  Taxes on exports discourage industry in the same way that taxes
on imports discourage luxury.  It would also be impossible to make fair and
uniform.  There are only a few goods that could be taxed at all:  tobacco,
rice, and indigo.  And a tax on these alone would be unjust.

Congress will have the power to regulate trade between the states, which will
protect them from one another.  Should this not be the case, the attempt of
one state to tax the produce of another passing through its hands would force
a direct exportation and defeat itself.\rf{farrand2}{359-60}}

\dd{Gouverneur Morris (PA)}{The power to regulate trade between Pennsylvania
and New Jersey will never prevent the former from taxing the latter.  Nor will
such a tax force a direct exportation from New Jersey.  The advantages of a
large trading port outweigh the disadvantages of a moderate duty.

Many exports from several states would provide advantageous revenue ---
tobacco, lumber, live-stock, and ginseng among them.  And Congress might use
duties on exports, such as animal skins and other peculiar raw materials, as a
means of encouraging American manufacture.\rf{farrand2}{360}}

\dd{John Dickinson (DE)}{A congressional power to tax exports may be
presently inconvenient, but it would be dangerous to prohibit it for all goods
and forever.  It would be better to exempt certain goods from the
power.\rf{farrand2}{361}}

\dd{Roger Sherman (CT)}{We should prohibit Congress from taxing any exports,
excepting perhaps goods that should not be exported.  An enumeration of
exemptions would be difficult, offensive, and improper.  The complexity of
American business renders an equal tax on exports impracticable.  The states
will never agree to trust Congress and give up all power over trade.  This
will sink the whole plan.

The Constitution already sufficiently empowers Congress:  The power to
regulate trade between states prevents the oppression of the non-commercial
states spoken of by Mr.\ Wilson and Mr.\ Langdon, and the regulation of trade
in general might accommodate the economic retaliation that concerns Mr.\
Morris.\rf{farrand2}{308, 361}}

\dd{James Madison (VA)}{The power to regulate trade between states will do
little to hinder the extortion of the non-commercial states.  A proper
regulation of exports, while presently inexpedient, will hereafter be
necessary and for the same reasons as the regulation of imports:  to generate
revenue, to protect domestic manufacturing, and to procure equitable trade
regulations from other countries.

The commercial states fear a disproportionate burden if Congress taxes
exports.  It was agreed on all hands that our revenue would principally be
drawn from trade, but it is not material how they are collected --- whether
from imports only or from half imports and half exports.  The imports and
exports must be pretty nearly equal in every state and relatively the same
among the different states.\rf{farrand2}{361}}

\dd{George Mason (VA)}{Exports are not the same as imports, Mr.\ Madison.  The
latter are the same throughout the states, but the former are very different.
If I wanted to reduce the state governments to mere corporations, however, I
would also subject exports to a power of general taxation.

It is a fundamental principle that the majority, when interested, will oppress
the minority.  The northern states will have majorities in both the House and
the Senate.  The southern states, therefore, have good ground for their
suspicions.\rf{farrand2}{362-3}}

\dd{James Wilson (PA)}{Pennsylvania exports the produce of Maryland, New
Jersey, Delaware, and --- once the River Delaware is opened --- that of New
York, too.  And yet I must favor the general power over exports against the
particular interest of my state.  We are not \emph{compelling} Congress to tax
exports; we are only authorizing them to do so.  To deny this power is to take
away half the regulation of trade.  A power over exports will be better than a
power over imports for obtaining beneficial commercial
treaties.\rf{farrand2}{362}}

\dd{Elbridge Gerry (MA)}{The power over exports might be used to compel the
states to comply with the will of the general government and to grant it any
new powers that it demands.  We have already given Congress more power than we
know what it will do with.  The general government may use it to oppress the
states as much as Great Britain oppresses Ireland.\rf{farrand2}{362}}

\dd{Rufus King (MA)}{I cannot support this motion for the same reason I did
not support slaves in the rule of representation.  The general government
exists, in part, to defend against foreign invasion and internal sedition.
Should we allow some states to introduce a weakness that will make that
defense more difficult?  We are tying the hands of Congress on two counts:
They cannot halt the importation of slaves, and now it is proposed they cannot
tax exports.  Shall one part of the Union be bound to defend the other and
that other not only increase its danger but also withhold the compensation for
the additional burden?  If slaves are allowed to be imported, shouldn't the
exports produced by their labor supply a revenue that the general government
will need to defend their masters?  We cannot allow unlimited importation of
slaves \emph{and} prohibit the taxation of exports.  I cannot assent to it
under any circumstances and neither will the people of the northern
states.\rf{farrand2}{220}}

% NOTE: George Mason and Elbridge Gerry dialogues on p.413 (August 25) are
% already printed in Article I.8.1

\summary{\emph{Congress shall not tax the exports of any state.}  For some
states the whole of their productions are agricultural, while the resources of
others come from fisheries, freight, or other commerce.  A tax on the export
of any particular state would burden that state with extreme inequality and
might injure or destroy its staple production or common
article.\rf{story2}{469-70}}

\cl{Clause 6}{Port preferences}
\cq{No Preference shall be given by any Regulation of Commerce or Revenue to
the Ports of one State over those of another: nor shall Vessels bound to, or
from, one State, be obliged to enter, clear, or pay Duties in another.}

\dd{The Chairman}{The Chair recognizes Daniel Carroll of Maryland.}

\dd{Daniel Carroll (MD)}{Under the power of regulating trade, my constituents
fear that Congress might favor the ports of particular states by requiring
vessels destined to or from one state to clear their cargo at another --- for
example, forcing a vessel bound for Baltimore to enter and clear in Norfolk
instead.\rf{farrand2}{417}  To this end I move that no regulation of commerce
or taxation shall give preference to the ports of one state over those of
another, nor oblige vessels bound to or from any state to clear or pay duties
in another.\rf{farrand2}{480}  This restriction is a tender point in
Maryland.\rf{farrand2}{481}}

\dd{The Chairman}{The floor is open to debate on the motion.}

\dd{Nathaniel Gorham (MA)}{The precaution is unnecessary and insufficient.
It will encourage smugglers to evade the duties by running up long rivers
through the jurisdiction of different states, landing and selling their cargo
along the way, before finally clearing through a customhouse at their ultimate
destination.\rf{farrand2}{418}  This inconvenience might happen on the River
Delaware, for example, if Congress cannot make a vessel clear their goods
below the jurisdiction of Pennsylvania.\rf{farrand2}{480}}

\dd{Thomas Fitzsimons (PA)}{It would be a greater inconvenience, Mr.\ Gorham,
to require vessels bound to Philadelphia to clear their goods below the
jurisdiction of the state.\rf{farrand2}{480}}

\dd{John Langdon (NH)}{The government would be so fettered by the clause, as
to defeat the good purpose of the plan.  Congress should have the power to
establish precautions against smuggling.\rf{farrand2}{480-1}}

\dd{James McHenry (MD)}{Even with this clause Congress could still prevent the
difficulties mentioned.  Nothing in Mr.\ Carroll's motion would, for example,
prevent obliging vessels to take an officer on board as a security for due
entry.\rf{farrand2}{481}}

\summary{\emph{Regarding the power to regulate trade, Congress shall give no
preference to the ports of one state over those of another.}}

\cl{Clause 7}{Accounting of the treasury}
\cq{No Money shall be drawn from the Treasury, but in Consequence of
Appropriations made by Law; and a regular Statement and Account of the
Receipts and Expenditures of all public Money shall be published from time to
time.}

\dd{The Chairman}{The Chair recognizes George Mason of Virginia.}

\dd{George Mason (VA)}{I move that no money shall be drawn from the treasury
but in consequence of appropriations made by Congress,\rf{farrand2}{509} and
an account of the public expenditures should be published
annually.\rf{farrand2}{618}}

\dd{The Chairman}{The floor is open to debate on the motion.}

\dd{Gouverneur Morris (PA)}{Publishing annually would be impossible in many
cases.\rf{farrand2}{618}}

\dd{Rufus King (MA)}{The term ``expenditures'' might mean every minute cent,
which would be impracticable.  Congress might make a more reasonable account,
but it would be so general as to afford no satisfactory
information.\rf{farrand2}{618}}

\dd{James Madison (VA)}{I would prefer accounts published ``from time to
time'' rather than annually.  This would still urge frequent publications but
would leave the period to the discretion of Congress.  If we require too much,
the difficulty will beget a habit of doing nothing.  The Articles of
Confederation require biannual publications on the subject.  Because a
punctual compliance is often impossible, the practice has ceased
altogether.\rf{farrand2}{618-9}}

\summary{\emph{No money shall be drawn from the treasury but in consequence of
appropriations made by law.}  As all taxes raised from the people are to be
applied to the discharge of expenses, debts, and other engagements, Congress
alone should possess the power to decide how and when any money should be
applied for those purposes.\rf{story3}{213}

\emph{A regular account of receipts of all public money shall be published
from time to time}, that the people may know how their money is expended, for
what purposes, and by what authority.\rf{story3}{214}}

\cl{Clause 8}{Titles of nobility and foreign emoluments}
\cq{No Title of Nobility shall be granted by the United States: And no Person
holding any Office of Profit or Trust under them, shall, without the Consent
of the Congress, accept of any present, Emolument, Office, or Title, of any
kind whatever, from any King, Prince or foreign State.}

\dd{The Chairman}{The Chair recognizes James McHenry of Maryland.}

\dd{James McHenry (MD)}{I move that no titles of nobility shall be granted by
the United States --- the better to preserve our Union from the evils of
aristocracy.\rf{farrand3}{150}}

\dd{The Chairman}{The floor is open to debate on the motion.}

\dd{Edmund Randolph (VA)}{We should also restrain any persons in office from
accepting any present, emolument, title, or office from any foreign prince or
state.  The sixth Article of Confederation already includes such a
restriction.  All men have an inherent right to accept emoluments from anyone.
Our officers will need the explicit regulation to prevent
corruption.\rf{farrand3}{327}}

\dd{Benjamin Franklin (PA)}{Mr.\ Randolph's amendment is necessary to prevent
even the \emph{appearance} of corruption.  I am embarrassed to admit that I
myself have violated such a restriction; though, fortunately the event caused
no scandal.  While serving as a minister to France, I carelessly accepted a
gilded snuff-box from King Louis XVI.  I thought nothing of the gift at the
time, but I believe, that if in that moment, when we were in harmony with the
King of France, anyone in Congress had supposed that he was corrupting one of
our ambassadors, it might have disturbed that confidence and diminished that
mutual friendship, which contributed to carry us through the
war.\rf{farrand3}{327}}

\summary{\emph{No title of nobility shall be granted by the United States.}
Distinctions between citizens, in regard to rank, would soon lay the
foundation of odious claims and privileges and silently subvert the spirit of
independence and personal dignity, which are the best security of republican
government.\rf{story3}{215}

\emph{Nor shall any officer of the United States accept any present,
emolument, title, or office from a foreign state.}  This provision is founded
on a just jealousy of foreign influence of every sort and prevents any officer
of the government from wearing borrowed honors, which would enhance his
supposed importance abroad by a titular dignity at home.\rf{story3}{216}}
