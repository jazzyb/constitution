\ch{Article I, Section 6}{Privileges of Congress}
\cl{Clause 1}{Compensation of legislators}
\cq{The Senators and Representatives shall receive a Compensation for their
Services, to be ascertained by Law, and paid out of the Treasury of the United
States.  They shall in all Cases, except Treason, Felony and Breach of the
Peace, be privileged from Arrest during their Attendance at the Session of
their respective Houses, and in going to and returning from the same; and for
any Speech or Debate in either House, they shall not be questioned in any
other Place.}

\dd{The Chairman}{The Chair recognizes James Madison of Virginia.}

\mn{Congressional salary}
\dd{James Madison (VA)}{I move that the members of Congress receive a fixed,
liberal salary, which should be paid from the national treasury.  Fixed
\emph{in the Constitution} because it would be indecent and dangerous to let
Congress set their own salary.  And paid \emph{from the national treasury} so
Congress does not become dependent on the states for their compensation.  To
avoid issues of inflation or deflation, we could base the salary on the price
of some commodity, like wheat\rf{farrand1}{215-6} or other standard that will
not vary with circumstances.\rf{farrand1}{373}}

\dd{The Chairman}{The floor is open to debate on the motion.}

\dd{Benjamin Franklin (PA)}{I approve of the motion, but I dislike calling
their compensation ``liberal.''  If any qualification is necessary, I would
prefer to substitute it with ``moderate.''  Abuses have a tendency to grow of
themselves when once begun.  Recall the progression of ecclesiastical
benefices from the practice of the apostles to the establishment of the papal
system.\rf{farrand1}{216}}

\dd{Oliver Ellsworth (CT)}{The states should compensate the legislators from
their own treasuries.  Standards of living differ from state to state.  What
would be a reasonable compensation in one state would be very unpopular in
another.\rf{farrand1}{372}}

\dd{George Mason (VA)}{If different states provide different pay, they will
introduce feelings of inequality where members should in all respects be
equal.  The stinginess of the states might even reduce the compensation so low
that the question will not be `who are most fit to serve' but `who are the
most willing.'\rf{farrand1}{216}  The states already pare down the salaries of
their federal delegates in order to keep out the men most capable of executing
the functions of Congress.\rf{farrand1}{372}}

\dd{Pierce Butler (SC)}{The state legislatures should provide the wages.  The
senators, in particular, will live so long away from their respective states
that they will lose sight of their constituents unless dependent on them for
their support.\rf{farrand2}{290}}

\dd{Daniel Carroll (MD)}{That would complete the dependence of both houses on
the states.  They would reward the congressmen who comply with their wishes
and starve the ones who don't.  The new government in this form would be
nothing more than a second edition of the Confederation Congress in two
volumes and, perhaps, with very few amendments.\rf{farrand2}{292}}

\dd{Hugh Williamson (NC)}{The states should fund the salaries.  New western
states, being poor, will pay little into the national treasury.  They will
also have different interests from the original states.  The latter should not
have to pay the expenses of men who will try to thwart their
measures.\rf{farrand1}{372}}

\dd{Edmund Randolph (VA)}{You are consulting too much with the popular
prejudices, Mr.\ Williamson.  Whatever respect might be due to them in lesser
matters or when they form the permanent character of the people, it is neither
incumbent nor honorable to sacrifice right and justice in this instance.  If
the states were to pay the members of Congress, the dependence would corrupt
the whole system.  The entire nation has an interest in the attendance and
services of the legislators.  The national treasury, therefore, is the proper
fund to support them.\rf{farrand1}{372}}

\dd{James Madison (VA)}{Mr.\ Williamson, we cannot leave the members from the
poor states beyond the mountains to the precarious and miserly support of
their constituents.  If the western territories should join the Union, we
should consider them our equals and brethren.  If we should admit their
representatives into the common councils, we should also present terms that
would invite the most capable and respectable characters into the
service.\rf{farrand1}{373}  Consider the situation of the representation of
the smaller states in the Confederation Congress.  The financial expense has
prevented them from maintaining their delegates, and their absences frequently
delay the public business.\rf{farrand1}{319}}

%There is another undesirable consequence of leaving such a burden to the
%states.  During a considerable period of the recent war, the sole
%representative of Delaware was a citizen and resident of Pennsylvania and held
%an office in his own state which was incompatible with an appointment to
%Congress.  During another period the same state was represented by three
%delegates, two of whom were from Pennsylvania and the other from New Jersey.
%These expedients were intended to avoid the burden of supporting delegates of
%their own state.\rf{farrand1}{319-20}}

\dd{Nathaniel Gorham (MA)}{I agree with Mr.\ Madison and others that the state
legislatures should not provide the compensation.  But neither should we fix
the salary in the Constitution.  We could not make it as liberal as necessary
without exciting enmity against the whole plan.  Let Congress provide for
their own wages from time to time just as the state legislatures do.  I have
not seen, nor can I apprehend, an abuse of this power.\rf{farrand1}{372}}

\dd{Rufus King (MA)}{If we let Congress set their own wages, we would excite
greater opposition to the plan than the most unnecessary fixed sum
could.\rf{farrand1}{372}}

\dd{Alexander Hamilton (NY)}{Fixing the salary in the Constitution would
indeed be inconvenient, but I am strenuously against making Congress dependent
on the rewards of the states.  Those who pay are the masters of those who are
paid.\rf{farrand1}{373}

Some gentlemen appear to labor under the notion that the interests of the
general government and the state legislatures are precisely the same.  This
cannot be true.  The views of the governed are often materially different from
those who govern.  The science of policy is the science of human nature.  A
state government will ever be the rival power of the general government.  It
is, therefore, highly problematic that the state legislatures should be the
paymasters of the members of the national government.  All political bodies
love power and will often improperly obtain it.\rf{farrand1}{378-9}}

\dd{Oliver Ellsworth (CT)}{If we are so exceedingly jealous of the state
legislatures, why shouldn't they be as jealous of us?  If I return to my state
and tell them we made such and such regulations for a government because we
dared not trust you with any excessive powers, will they be satisfied?  Will
they adopt your government?  Let it ever be remembered that without their
approval your government is nothing more than a rope of
sand.\rf{farrand1}{379}}

\dd{James Wilson (PA)}{I do not intend to submit the national plan to the
state legislatures for approval.  They and the state officers will certainly
oppose it.  We should carry it to the people of each state to
ratify.\rf{farrand1}{379}

Returning to the issue at hand, I support Congress setting their own stipend
and paying it from the national treasury.\rf{farrand1}{378}}

\dd{James Madison (VA)}{The members of Congress will be too involved to
ascertain their own compensation.  We must not let them to put their hands
into the public purse for the sake of their own pockets.\rf{farrand1}{373-4}}

\dd{Roger Sherman (CT)}{I am less afraid that Congress will make their wages
too high than that they will make them so low that otherwise competent men
cannot serve unless they are also rich.  The best plan would be to fix a
moderate allowance out of the national treasury --- say five dollars per day
--- and let the states supplement it as they see fit.\rf{farrand2}{291}}

\dd{John Dickinson (DE)}{There are objections to every method of payment so
far proposed.  Fortunately, most of you are already convinced that the general
government should be independent of the prejudices and passions of the state
legislatures.  If we leave Congress dependent on the legislatures, then it
would be happy for us if we had never met in this room.

I would like to propose a less objectionable method for your consideration:  I
move that every twelve years Congress should pass an act setting their
wages\rf{farrand2}{292} and that we should require the wages of both houses to
be the same.\rf{farrand2}{293}}

\mn{Compensation of senators}
\dd{Nathaniel Gorham (MA)}{It would be unreasonable to pay the senators the
same as the representatives.  The senators' business will detain them longer
from home, oblige them to move their families, and perhaps require them to sit
constantly in wartime.  Their salaries should certainly be
higher.\rf{farrand2}{293}}

\dd{C. C. Pinckney (SC)}{If the Senate is meant to represent the wealth of the
country, it should consist of persons of wealth.  We should give them no
stipend.  Then, only the wealthy will undertake the
service.\rf{farrand1}{426-7}}

\dd{Edmund Randolph (VA)}{Our country lacks sufficient personal fortunes to
induce gentlemen to attend for nothing.\rf{farrand1}{377}}

\dd{Benjamin Franklin (PA)}{The Constitution should fix no salary for
senators.  The states will probably send a number of the young men in this
convention to the Senate.  If we recommend a lucrative appointment, the people
might charge us with having carved out places for
ourselves.\rf{farrand1}{427}}

\dd{James Madison (VA)}{If the national government does not pay the senators,
then their states will pay and create the improper dependence I have warned
against.\rf{farrand1}{219}}

\dd{Luther Martin (MD)}{If the Senate represents the states' interests, then
the states \emph{should} pay the senators.\rf{farrand2}{292}}

\dd{Daniel Carroll (MD)}{The Senate is meant to represent and manage the
affairs of the whole, not to be the advocates of state interests.  They
should neither be dependent on nor paid by the states.\rf{farrand2}{292}}

\dd{Oliver Ellsworth (CT)}{I still fail to see the error of letting the states
pay their senators.  If the Senate is meant to strengthen the government, it
should have the confidence of the states.  The states will have an interest in
keeping up a representation and will support the members so as to ensure their
attendance.\rf{farrand1}{427}}

\dd{James Madison (VA)}{Mr.\ Ellsworth, your proposal is a departure from
fundamental principles and subverts the six year limit we placed on the
senatorial term.\footnote{See the debate surrounding the Senate term length in
Article I, Section 3, Clause 1, p.\ \pageref{senate:term}.}  It would ensure
the senators held their places indefinitely at the pleasure of the
legislatures.

One great purpose of the Senate is, that being firm, wise, and impartial, it
might hold an even balance among different states.  Your suggestion turns the
Senate into another Confederation Congress --- the mere agents and advocates
of state views --- instead of the impartial umpires and guardians of justice
and the general good.\rf{farrand1}{427-8}}

%\dd{Elbridge Gerry (MA)}{There are difficulties on both sides.  The
%observation of Mr.\ Butler has some weight, but the state legislatures may
%turn out the senators by reducing their salaries.  Such things have
%happened.\rf{farrand2}{291}}

\summary{\emph{Members of Congress shall receive a compensation from the
national treasury and shall be able to set their salary by law.}  Of the many
methods for compensating congressmen for their service, the Committee
considered this method the least questionable.  It removes the virtual
disqualification of poverty from candidates who, though favored by talent,
might not be favored by fortune.  The superior emoluments will also have the
possible effect of making a seat in the national councils more attractive than
those of the states.\rf{story2}{320-1}

\mn{Congressional privileges}\emph{Members of Congress shall be privileged
from arrested except in cases of treason, felony, or breach of the
peace,\rf{blackstone1}{110} and their freedom of speech shall not be impeached
or questioned in any court or place outside of Congress.}\rf{blackstone1}{109}
Although this topic was not debated, the Committee sees the need to establish
these privileges somewhere in the plan.  They belong to Congress in common
with all other legislative bodies that have existed in America since its first
settlement, under every variety of government, and they constitute
longstanding privileges of both houses of the British
Parliament.\rf{story2}{325}}

\cl{Clause 2}{Exclusion of legislators from offices}
\cq{No Senator or Representative shall, during the Time for which he was
elected, be appointed to any civil Office under the Authority of the United
States which shall have been created, or the Emoluments whereof shall have
been increased during such time; and no Person holding any Office under the
United States, shall be a Member of either House during his Continuance in
Office.}

\dd{The Chairman}{The Chair recognizes James Madison of Virginia.}

\dd{James Madison (VA)}{We may face a danger that members of Congress create
offices or augment the stipends of existing offices for their own
gratification.\rf{farrand1}{380}  I move that members of Congress be
ineligible during their term of service and for one year afterward to any
offices that were established or augmented during the time of their being
members.

This motion remedies the problems we experience due to the unnecessary
creation of offices and increases of salaries.  If we shut the door against
these evils, we might properly leave it open for the appointment of members to
other offices as an encouragement to the legislative
service.\rf{farrand1}{386}}

\dd{The Chairman}{The floor is open to debate on the motion.}

\dd{Rufus King (MA)}{Excluding them for a whole year after their term is
impossible.  You are going to utopian lengths.  It is a mere
cobweb.\rf{farrand1}{379}}

\dd{Pierce Butler (SC)}{We have no way of judging mankind but by experience.
Take Parliament's flimsy exclusion.  It ruins their government.  A man takes a
seat in Parliament to get an office for himself or friends or both, and this
is the source from which flows its great venality and
corruption.\rf{farrand1}{379}  If anything, the motion does not go far enough
and would be easily evaded.\rf{farrand1}{386}}

\dd{James Wilson (PA)}{We should exclude sitting congressmen from any other
office, but this exclusion should last no longer than that member's term.  We
should hold forth every honorable incentive for men of abilities to enter
public service --- this is truly a republican principle.  I must have strong
reasons to disqualify a good man from office.  For if I do, a sycophant or
social climber may fill it instead.\rf{farrand1}{379-80}}

\dd{George Mason (VA)}{Mr.\ Wilson, do you really believe this exclusion will
prevent the best characters from coming forward?  Are you not struck at
seeing the luxury and venality that has already crept in among us?  The
members of Congress will make or multiply offices in order to fill them.  If
not checked, we shall have ambassadors to every petty state in Europe --- the
little republic of San Marino not excepted.

I admire many parts of the British constitution and government, but I detest
the corruption.  By increasing the officers of government, corruption pervades
every town and village in the kingdom.  Won't the same causes here produce the
same effects?\rf{farrand1}{380-1}}
%I consider this motion the cornerstone on which our
%liberties depend.  If we reject it from our plan, we are erecting a fabric
%for our destruction.\rf{farrand1}{380-1}}

\dd{James Wilson (PA)}{What I believe, Col.\ Mason, is that we should not
stigmatize with the name of ``venality'' the laudable ambition of rising into
the honorable offices of the government --- an ambition that all wise and free
governments chose to cherish rather than to check.  The members of Congress
have perhaps the hardest and least profitable task of any who engage in the
service of the state.  Should this merit be a
disqualification?\rf{farrand1}{387}  Should their talents be a
punishment?\rf{farrand1}{380}  The state of Pennsylvania, which has gone
further than any state into the policy of fettering power, has not rendered
its legislators ineligible to offices of government.\rf{farrand2}{288}}

%\dd{Nathaniel Gorham (MA)}{Mr.\ Madison, your proposal is unnecessary and
%injurious.\rf{farrand1}{375}  Even the corruption of English government has
%this advantage:  It grants stability.  We do not know what the effect would be
%if members of Parliament were excluded from offices.  The elections are the
%real issue.  The great bulwark of our liberty is our frequency of elections,
%while their great danger is their septennial Parliaments.\rf{farrand1}{381}}

\dd{Alexander Hamilton (NY)}{One of the great and essential principles
necessary for the support of government is \emph{influence} --- a dispensation
of those regular honors and emoluments that produce an attachment to the
government.  Almost all the weight of these is on the side of the states and
must continue as long as the states continue to exist.  All the passions of
avarice, ambition, and self-interest, which govern most individuals and all
public bodies, fall into the current of the states and do not flow into the
stream of the general government.  The former, therefore, will generally
overmatch the latter and render any confederation
precarious.\rf{farrand1}{285}

We have taken up many ideas upon trust and, at last pleased with our own
opinions, established them as undoubted truths, such as when we condemn the
danger of influence in the British government without duly reflecting how far
it was necessary to support a good government.  Take mankind as they are, and
what are they governed by?  Their passions.  One great error is that we
suppose mankind more honest than they are.  Our prevailing passions are
ambition and self-interest, and it will ever be the duty of a wise government
to avail itself of those passions and submit them to the public good.

If we adopt this motion, perhaps a few men may step forward, whether from
patriotic motives or to display their talents or to reap the advantage of
public applause, but we would destroy the motive for most.  I am against all
exclusions and refinements except only in this case:  When a member takes his
seat, he should vacate every other office.  It is difficult to put any
exclusive regulation into effect.  We must in some degree submit to the
inconvenience.\rf{farrand1}{382}}

\dd{George Mason (VA)}{Are there really so few citizens who would undertake
congressional service without the incentive of lucrative offices?  We should
certainly encourage genius and virtue.  For aught I know, genius might be
encouraged with avarice and self-interest.  But that virtue might be
encouraged by such vice is an idea quite new to me.\rf{farrand1}{387}

In Virginia all offices are appointed by the legislature, and many of their
appointments are most shameful, which is why I believe Mr.\ Madison's check is
insufficient.  It will soon cease to be any check at all.  Nepotism and cabal
will evade the exclusion.\rf{farrand1}{392}  The people should and will refuse
such a government.\rf{farrand1}{392}}

\dd{James Madison (VA)}{I intended the motion to be a middle ground between
universal eligibility and absolute disqualification.  Col.\ Mason, I too am
aware of the partiality in the Virginia assembly.  But haven't you yourself
also witnessed that the reluctance of the best Virginians to engage in the
legislative service too often yielded the success of unfit characters?  We
should fairly compare the advantages and disadvantages on both sides of the
issue.\rf{farrand1}{388}

All public bodies tend to support their members, but it is not always done
from base motives.  Friendship and knowledge of the abilities of those with
whom they associate may produce it.  If you bar such attachments and rely only
on the patriotism of the members, you will fill Congress with indifference and
deprive the government of its greatest strength and support.\rf{farrand1}{392}
Virginia would have been without many of its best legislators if they had been
ineligible to the honorable offices of the state.

If public bodies tend to favor their own members, it is equally true that they
tend to be misled when taking strangers on second-hand reports.  Nor would we
obviate the partiality of such bodies by disqualifying their own members.
Candidates would hover around the seat of Congress and court the favor of the
members.  A great proportion of state offices are appointed this way, and in
the general government the evil will be still greater.\rf{farrand1}{388-9}

Our goal should be to fill all offices with the fittest characters and to draw
the wisest and worthiest citizens into Congress.\rf{farrand1}{388}  If we
expect useful characters to serve, we must entice them.  Congress must be the
road to public honor, and the advantages of my motion are greater than any
possible inconvenience.\rf{farrand1}{392}}

\dd{Rufus King (MA)}{We are refining too much in this business.  The idea of
preventing intrigue and solicitation of offices is a fantasy.  Mr.\ Madison,
you declare no member shall himself be eligible to any office.  But will this
restrain him from gaining appointments for his son, his brother, or any other
object of his bias?  We are losing the advantages on one side without avoiding
the evils on the other.\rf{farrand1}{387}  Instead, open the eligibility to
all men.\rf{farrand1}{393}}

%\dd{Roger Sherman (CT)}{The motion does not go far enough.  Imagine that a new
%office is created.  A person serving in one office might be transferred to the
%new one, and then a member of Congress could be appointed to the former.  For
%example, a new embassy might be established in a new court and an ambassador
%taken from another in order to create a vacancy for a favorite
%member.\rf{farrand1}{387-8}}

\dd{Daniel Jenifer (MD)}{The senators of Maryland are chosen for five years
and can hold no other office during their term.  This exclusion has earned
them the greatest confidence of the people.\rf{farrand1}{389-90}}

\dd{Gouverneur Morris (PA)}{This would be a dangerous step to take in the
national government, Mr.\ Jenifer.  We should give the senators every
incentive to invest in our government.  Deprive them of this eligibility, and
they will become inattentive to our welfare.  The wealthy will ever exist, and
you can never be safe unless you gratify them in the pursuit of honor and
profit.\rf{farrand1}{518}}

\dd{George Mason (VA)}{Then we might as well remove all restrictions on
Congress.  This would let us better encourage that exotic corruption, which
might not otherwise thrive so well in American soil, and better complete that
aristocracy, which is probably on the minds of some present gentlemen.  We
should invite into the legislative service those generous and benevolent
characters who will do justice to each other's merit by carving out offices
and rewards for it.  In the present state of American morals and manners, I am
sure we will lose few friends to the plan by giving premiums to a mercenary
and depraved ambition.\rf{farrand2}{284}}

\dd{Charles Pinckney (SC)}{Barring congressmen from offices would be degrading
and all the more misguided since their election would already imply they have
the confidence of the people.  I hope to see the Senate become a school of
public ministers, a nursery of statesmen.  But that will never happen unless
we can entice the fittest men to serve.\rf{farrand2}{283}}
%
%I propose the following in place of Mr.\ Madison's motion:  The members of
%each house shall be incapable of holding any office under the United States
%for which they or any others for their benefit receive any salary, fees, or
%emoluments of any kind, and the acceptance of such an office shall vacate
%their seat.\rf{farrand2}{283-4}}

\dd{Elbridge Gerry (MA)}{I am not so fond of public ministers and statesmen
that I wish to establish nurseries for them.  If men will not serve in
Congress without the prospect of public office, our situation is deplorable
indeed.  If our best citizens are actuated by such mercenary views, we had
better choose a single despot at once.  It will be easier to satisfy the
gluttony of one than of many.\rf{farrand2}{285}}

\dd{John Mercer (MD)}{A fundamental principle of political science is that
whenever the rights of property are secured, an aristocracy will grow out of
it.  Representative governments also necessarily become aristocratic because
the rulers, being few, can and will draw emoluments for themselves from the
many.  The governments of America will become aristocracies.  They are
already.

Public measures are calculated for the benefit of the governors, not the
governed.  The people are dissatisfied and complain.  They change their
rulers, and the public measures change, but it is only a change of one scheme
of emoluments and rulers for another.  The people gain nothing by it but
instability and uncertainty.

Governments can only be maintained by \emph{force} or \emph{influence}.  The
President, as we have so far conceived him, has no force.  Deprive him of the
influence to appoint members of Congress to executive offices, and he becomes
a mere phantom of authority.  The aristocrats will not even let him in for a
share of the plunder.  Without such influence, the aristocracy and the people
will war.  Nothing else can protect the people from those speculating
legislatures, which are now plundering them throughout the
states.\rf{farrand2}{284-5}}

\dd{Elbridge Gerry (MA)}{If you are right, Mr.\ Mercer, then our government is
a government of thieves.  We should certainly have one rather than many
employed in it.  The Constitution, as it currently stands, is as complete an
aristocracy as has ever been framed.  The people will examine the system with
suspicion.  Do you think they will ever agree to it?

I support Mr.\ Madison's original motion.  If we are concerned that this will
injure Congress by keeping out men of abilities who are willing to serve in
other offices, we should require, as a qualification to such offices, that the
candidate should have served a certain time in Congress.\rf{farrand2}{285-6}}

\dd{Gouverneur Morris (PA)}{%The legislators should be eligible to offices, so
%long as they vacate their seats when they accept office.\rf{farrand2}{286}
Suppose a war breaks out, and a number of your best military characters are
congressmen.  Must we lose the benefit of their services?\rf{farrand1}{380}
Exclude the officers of the army and navy, and you form a band opposed to the
civilian power.  You will stimulate them to despise and reproach those talking
lords who dare not face the foe.  Let them rouse this spirit at the end of a
war, before the troops have laid down their arms, and though the civil
authority be entrenched in parchment to the teeth, they will cut their way to
it.\rf{farrand2}{286}}

\dd{Edmund Randolph (VA)}{I am against opening a door to influence and
corruption.  No arguments had made an impression on me until Mr.\ Morris
spoke.  While I still support Mr.\ Madison's motion, I am willing to exempt
military appointments from the exclusion.\rf{farrand2}{290}}

\dd{Charles Pinckney (SC)}{No state in the Union bars its legislators from
holding other offices.  In South Carolina, even the judges can run for the
legislature.  So why would a broader eligibility offend the people?  If the
state constitutions were revised today, I am certain that the people would
diminish, rather than multiply, such restrictions.\rf{farrand2}{287}}

\dd{Abraham Baldwin (GA)}{Your example of the state legislatures is
inapplicable, Mr.\ Pinckney.  They are so numerous that an exclusion of their
members does not leave proper men for offices.  The case will be otherwise for
the general government.\rf{farrand2}{491}}

\dd{Charles Pinckney (SC)}{If this plan is ratified, the first Congress will
be composed of the ablest men to be found.  The states will select such men to
put the government into operation.  Should the Committee accept Mr.\ Madison's
motion, the great offices, including the Supreme Court, which might be for
life, must be filled while those most capable of filling them will be
disqualified.\rf{farrand2}{491}}

\dd{John Mercer (MD)}{What led to the appointment of this convention?  The
corruption and mutability of the legislatures of the states.  If the plan does
not remedy these problems, it will not recommend itself.  It is a great
mistake to suppose that the Constitution will govern us.  The men whom it will
bring into the government will govern us.  The paper only marks out the mode
and the form.  Men are the substance and must do the business.

I repeat my previous statement:  Governments can only be maintained by force
or influence.  It is not the King of France but 200,000 soldiers who govern
that kingdom.  We will have no such force here.  If the general government
does not provide the influence, the best men will stay home and prefer
appointments within their own states.\rf{farrand2}{288-9}}

\summary{\emph{No senator or representative may hold a civil office without
first vacating his congressional seat.  Nor shall any member of Congress be
appointed to an office that he may have had a hand in creating or amending.}
The Committee's provision is closer to the exclusions in the state
constitutions than Mr.\ Madison's initial motion.  The people, being already
familiar with such disqualifications in the states, should find it
sufficient.}
