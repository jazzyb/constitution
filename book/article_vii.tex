\addpart{Article VII}{Ratification}
\cq{The Ratification of the Conventions of nine States, shall be sufficient
for the Establishment of this Constitution between the States so ratifying the
Same.}

\dd{The Chairman}{The Chair recognizes James Madison of Virginia.}

\mn{Ratification conventions}
\dd{James Madison (VA)}{Amendments to the Articles of Confederation require
ratification by the state legislatures.  In this respect the Articles are
defective.  When conflict arises between the general government and the
states, the legislatures will always favor the latter authority.  For this
reason the new Constitution should be ratified by the supreme authority ---
the American people.\rf{farrand1}{122-3}  To this end I move that the
Constitution be submitted to state conventions, recommended by the state
legislatures, and chosen expressly by the people to consider and
ratify.\rf{farrand1}{22}}

\dd{The Chairman}{The floor is open to debate on the motion.}

\dd{Elbridge Gerry (MA)}{You do not want to submit the new system before the
people of Massachusetts.  They have the wildest ideas of government in the
world.  They want to abolish the upper house of the state legislature and give
all the remaining powers of the government to the lower
house.\rf{farrand1}{123}}

\dd{George Mason (VA)}{We must resort to the people.  The state legislatures
do not have the authority to ratify.  They are creatures of the state
constitutions and cannot be greater than their creators.  Let us even assume
for the sake of argument that they would have the authority.  It would still
be wrong to refer the plan to them because succeeding legislatures would have
the power to undue the acts of their predecessors.  And in each state the
national government would stand on the tottering foundation of a mere
legislative act.\rf{farrand2}{88}}

\dd{Oliver Ellsworth (CT)}{Col.\ Mason, you argue, first, that the
legislatures have no authority to ratify and, second, that if they did, their
successors could rescind the ratification.  Your second point is simply
incorrect.  An act to which the states by their legislatures make themselves
parties becomes a compact from which no one of the parties can recede of
itself.

But regarding your first point, a novel set of ideas seems to have crept into
our political discourse since the Articles of Confederation were written.  We
did not then consider conventions of the people because the state legislatures
were thought competent enough.  The people accepted the ratification of the
Articles without complaint.  The fact is, we exist at present as a federal
society united by a charter, one article of which is that alterations may be
only made by the legislative authority of the states.\rf{farrand2}{90}}

\dd{Edmund Randolph (VA)}{One not-so-novel idea that has pervaded all our
proceedings is that the states will oppose the new system.  Let every man who
supports the plan compare his current positive opinions of this Constitution
to the reluctance he held at the start.  Local demagogues, who will be
degraded from the importance they now hold, will spare no efforts to prevent a
like progress in the popular mind, which will be necessary for the adoption of
the plan.  We must, then, transfer this subject from the legislatures, where
this class of men have their full influence, to a field in which their efforts
will be less mischievous.\rf{farrand2}{89}}

\dd{Oliver Ellsworth (CT)}{A greater general government means greater public
debt, yet the people of the eastern states want to abolish all public debt.
We are more likely to achieve ratification through the legislatures than
through that class of people.\rf{farrand2}{90}}

\dd{George Mason (VA)}{If such a class of people would already view the plan
with suspicion, why would they be less suspicious of the judgment of the
legislatures on the matter?  Some state governments are not even derived from
the clear and undisputed authority of the people.  Some of the best and wisest
citizens I know consider the Virginia constitution to be established by an
assumed authority.  A national constitution derived from such a source would
provoke severe criticisms.\rf{farrand2}{89}}

\dd{Edmund Randolph (VA)}{Indeed.  Some states have set the authority of the
common law above that of the Confederation, which has had no higher sanction
than legislative ratification.\rf{farrand2}{89}}

\dd{Elbridge Gerry (MA)}{Mr.\ Randolph, you and Col.\ Mason seem to assume
an unconstitutionality in the present federal system and in some of the state
governments.  Inferences drawn from that assumption are inadmissible.  The
Articles of Confederation are paramount to any state constitution.  The last
article of which authorizes alteration of the Confederation only through the
state legislatures.

But the legislators will decide the matter regardless.  They will either
conform to or influence the will of the people, who will never decide anything
on their own.  A convention of the people would accomplish nothing but
confusion.\rf{farrand2}{89-90}}

\dd{James Madison (VA)}{We cannot let the legislatures ratify the plan.  The
difference between a system founded on the legislatures only and one founded
on the people is the true difference between a \emph{treaty} and a
\emph{constitution}.  The two have the same moral obligation, but their
political operations are distinct.  A law violating a treaty might be
respected by a judge as law, though an unwise one.  But a law violating a
constitution established by the people themselves would be considered null and
void.  Furthermore, the doctrine of treaties is that a breach of any article
by one party frees the other parties from their engagements.  The doctrine of
constitutions excludes such an interpretation.\rf{farrand2}{93}}

\dd{Luther Martin (MD)}{The plan must go through the legislatures.  The
Maryland constitution in particular prevents any amendment of its government
unless the change passes the legislature.  Nevertheless, I am confident that
both Maryland's legislature and people will oppose the
Constitution.\rf{farrand2}{476}}

%\dd{Rufus King (MA)}{The constitution of Massachusetts was made to be
%unalterable until 1790, yet this will not be a difficulty.  The state must
%have contemplated a recurrence to first principles before they sent delegates
%to this convention.\rf{farrand2}{476-7}}

\dd{James Madison (VA)}{The difficulty in Maryland, Mr.\ Martin, is no
different than in other states where officers have taken oaths to uphold their
governments.  The people are the fountain of all power, and by resorting to
them all difficulties are removed.  They can alter constitutions as they
please.\rf{farrand2}{476}}

\dd{Rufus King (MA)}{While I believe that the state legislatures are legally
competent to the task of ratifying the Constitution, we should prefer a
reference to the authority of the people in conventions.  It provides the most
certain means of removing all doubts about the legitimacy of the new
Constitution and is our most likely means of drawing the best men of the
states to make the decision.\rf{farrand2}{92}
%In the states many able men are excluded from the legislature but may be
%elected into a convention.  Among them are members of the clergy, who are
%generally friends of good government.  Their services were invaluable in the
%formation and establishment of the constitution of
%Massachusetts.\rf{farrand2}{90}

Furthermore, we are more likely to carry the ratification through a single
convention in each state than by forcing it through two houses of the
legislature.  The state legislators will raise objections because they believe
that they will either lose power under the new system\rf{farrand1}{123} or
that their oaths will bind them to support and maintain the existing
constitutions.\rf{farrand2}{92}}

\dd{Gouverneur Morris (PA)}{Since some men perceive difficulties with
conventions in their states, perhaps we could remove the requirement of a
convention and let each state pursue its own method of
ratification.\rf{farrand2}{475}}

\dd{Rufus King (MA)}{That would be equivalent to giving up on the Constitution
altogether, Mr.\ Morris.\rf{farrand2}{476}}

\dd{Gouverneur Morris (PA)}{On the contrary, I mean to facilitate the adoption
of the plan by letting the states follow the method that will best allow them
to ratify based on their constitution.\rf{farrand2}{476}}

\mn{Partial ratification}
\dd{James Wilson (PA)}{Even a unanimous ratification via the state conventions
could be easily defeated by the inconsiderate or selfish opposition of a few
states.  We should make some provision for ratifying this Constitution under a
\emph{partial} union of the states, with a door open for the admission of the
rest.\rf{farrand1}{123}}

\dd{Nathaniel Gorham (MA)}{Mr.\ Wilson's suggestion is another point in favor
of ratification by the people.  If the last article of the Confederation were
pursued, then the unanimous consent of the states would be necessary.  Should
all the states suffer themselves to be ruined if Rhode Island alone persists
in her opposition to general measures?  New York, too, is so attached to
taxing her neighbors that she may join the opposition.\rf{farrand2}{90}}

\dd{Oliver Ellsworth (CT)}{Mr.\ Gorham, if the necessity of our situation is
so urgent to warrant a new compact among a part of the states founded on the
consent of the people, then the same pleas are equally valid in favor of a
partial compact founded on the consent of the legislatures.\rf{farrand2}{91}}

\dd{Gouverneur Morris (PA)}{That is a non sequitur, Mr.\ Ellsworth.  You are
assuming --- erroneously --- that we are proceeding on the basis of the
Confederation.  This convention is unknown to the Confederation.  If the
Confederation is to be pursued, no alteration can be made without the
unanimous consent of the legislatures.  Judges would consider legislative
alterations not conformable to the Articles of Confederation null and void.
Whereas if we appeal to the people of the United States, a majority may alter
the federal compact.\rf{farrand2}{92}}

\dd{James Wilson (PA)}{I move that the ratification of \emph{seven} states
should be sufficient for the commencement of the new
government.\rf{farrand2}{468}}

\dd{Daniel Carroll (MD)}{All thirteen should be required.  The Confederation
was unanimously established, and unanimity is necessary to dissolve
it.\rf{farrand2}{469}}

\dd{James Madison (VA)}{If anything less than ten is sufficient, then
a minority of the population might force the Constitution on the whole body of
the people.\rf{farrand2}{469}}

\dd{James Wilson (PA)}{The clause would only bind the states that ratify the
Constitution.  We must in this case return to the original powers of society.
The house on fire must be extinguished without a scrupulous regard to ordinary
rights.\rf{farrand2}{469}}

%\dd{Gouverneur Morris (PA)}{Perhaps the Constitution could be ratified in a
%twofold way.  We could require a smaller number in the event that the
%ratifying states are contiguous and a larger number if
%dispersed.\rf{farrand2}{468}}

\dd{James Madison (VA)}{I move for any seven or more states entitled to at
least thirty-three members in the House of Representatives.  This would
require the consent of a majority of the states and people.\rf{farrand2}{475}}

\dd{Alexander Hamilton (NY)}{The number of states required for ratification
should be left up to the state conventions to decide.  I move that if each
convention should think the plan ought to take effect among nine ratifying
states, the same should take effect accordingly.\rf{farrand2}{560}}

\dd{Nathaniel Gorham (MA)}{Some states will agree that nine states are
sufficient.  Others will require unanimity for the purpose.  And the
conflicting ratifications will defeat the plan altogether.\rf{farrand2}{560}}

\dd{Alexander Hamilton (NY)}{No convention convinced of the necessity of the
plan will refuse to give it effect on the adoption of nine states.  This
method is less exceptional than Mr.\ Wilson's motion and will attain the same
end.\rf{farrand2}{560}}

\dd{Elbridge Gerry (MA)}{I object to the indecency of dissolving in so slight
a manner the solemn obligations of the Articles of Confederation.  If nine of
thirteen can dissolve the present compact, then six of nine will hereafter
dissolve the new Constitution.\rf{farrand2}{561}

\mn{Congressional approval}
We should at least submit the Constitution to the Confederation Congress for
approval.  Not doing so would be improper and offensive.  We cannot annul the
Confederation with so little scruple or formality.\rf{farrand2}{559-60}}

\dd{Thomas Fitzsimons (PA)}{Congressional approval would force Congress to
make an act inconsistent with the Articles under which they hold their
authority.\rf{farrand2}{560}}

\dd{Rufus King (MA)}{It would be more respectful to Congress, Mr.\ Gerry, to
submit the plan generally to them rather than in a form that expressly and
necessarily required their approval or disapproval.\rf{farrand2}{561}}

\dd{James Wilson (PA)}{It is worse than folly to rely on the approval of the
congressmen from Rhode Island.  Several delegates in this convention have
spoken against the plan.  Under such circumstances can we safely make the
assent of Congress a necessity?  After spending four or five months on the
arduous task of framing a government for our country, we are at the close
trying to throw insuperable obstacles in the way of its
success.\rf{farrand2}{562}}

\mn{Second constitutional convention}
\dd{Charles Pinckney (SC)}{I move that the legislatures shall call their state
conventions as speedily as circumstances will permit.  These words will
impress the necessity of calling conventions so that enemies of the plan might
not put any more obstacles in its way.  When the Constitution first appears
with the sanction of this convention, the people will be favorable to it.
Over time the state officers will conspire and turn the popular current
against it.\rf{farrand2}{478}}

\dd{Luther Martin (MD)}{I agree with you, Mr.\ Pinckney, but for a different
reason.\rf{farrand2}{478}  The American people would never adopt the
Constitution if given time to consider and understand it.  But if we emphasize
speed, then a desire for novelty, a conviction that some change is necessary,
or a confidence in the members of this convention might obtain its adoption.
If the people must ratify the plan, then it is better that they reject
deliberately than adopt hastily and afterwards regret it.\rf{farrand3}{294}}

\dd{George Mason (VA)}{Mr.\ Martin is right.  We should give the people time
to amend the proposed Constitution.  I would sooner chop off my right hand
than give my approval in its present state.  I believe the current plan should
be presented before another constitutional convention.\rf{farrand2}{479}}

\dd{Edmund Randolph (VA)}{I agree.  To that end I move that the state
conventions should be at liberty to propose amendments to the Constitution to
be submitted to another general convention, which may accept or reject them as
they judge proper.\rf{farrand2}{479}}

\dd{Gouverneur Morris (PA)}{I, too, long for a second constitutional
convention that might have the firmness to provide a stronger government,
which we seem afraid to do.\rf{farrand2}{479}}

\summary{\emph{The Constitution shall be submitted to conventions of the
people for its ratification.  The ratification of nine states shall be
sufficient to establish this Constitution.}  Only the express authority of the
people could give due validity to the Constitution.  To require the unanimous
ratification of all thirteen states might subject the essential interests of
the whole to the impulsivity or corruption of a single
member.\rf{story3}{710}}
