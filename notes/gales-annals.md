# [Annals of Congress](https://memory.loc.gov/ammem/amlaw/lwac.html)

* Summer of 1789 in the House of Representatives

## [June 8](https://memory.loc.gov/cgi-bin/ampage?collId=llac&fileName=001/llac001.db&recNum=221)

* 441: Madison introduces the amendments and calls for a committee of the
  whole to review them and vote on them
* 441: Smith is concerned that the government is not even fully running - they
  have other business to attend to before trying to amend it
* 442: Jackson: we cannot amend the government until we have experience with
  it
* 443: Jackson (GA): it should be postponed until March 1, 1790
* 444: Madison: if we keep postponing the matter, our constituents will get
  suspicious
* 445: Sherman: postponing the organization of the government in favor of
  amending it will alarm 20 where one is consoled
* 446: Page: if we don't consider the amendments, some of the states may call
  a new convention - and that would be much worse for us
* 451: amendments enumerated:
  1. power is derived from the people and they have the right to change it
  2. change to the number of representatives in each state
  3. amendment of compensation in A.I,S.6,c.1
  4. Declaration of Rights:
     1. freedom of religion
     2. freedom of speech
     3. freedom of assembly
     4. right to bear arms
     5. no soldier shall be quartered in a house
     6. double jeopardy, right to remain silent, and due process
     7. no cruel and unusual punishment
     8. no unreasonable searches and seizures
     9. right to a speedy and public trial, to face one's accusers, and have
	counsel
     10. the afore mentioned rights shall not be construed to diminish the
	 importance of other rights retained by the people
  5. no state shall violate the equal rights of conscious, freedom of the
     press, jury trial in criminal cases
  6. limitations on appeal (?)
  7. trials shall occur in the vicinity of the crime with a jury composed of
     local freeholders
  8. the three branches of government shall not exercise one another's powers,
     and the states retain any powers not explicitly delegated to the national
     government
  9. renumber articles due to inclusion of new amendments
* 454: begins Madison's enumeration of objections to the bill of rights
* 461: Jackson: "We are not content with two revolutions in less than fourteen
  years; we must enter into a third, without necessity or propriety."
* 468: Madison's propositions are referred to a committee of the whole

## August 15
* here begins the debate on the bill of rights
