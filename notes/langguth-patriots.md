# Jefferson 1775-76

(p. 335) - John Adams thought the "extreme democratic" ideas in Thomas Paine's
*Common Sense* were dangerous and wrote an essay to refute them.

# Long Island 1776

(p. 366-73) - Good summary of the debates surrounding the Articles of
Confederation.  A number of issues surely continued into the debate of the
Constitution.

[John Adams on the secrecy of the debates over the Articles of Confederation](https://www.masshist.org/digitaladams/archive/doc?id=A1_42&bc=%2Fdigitaladams%2Farchive%2Fbrowse%2Fautobio_by_date.php):
> Thus We see the whole Record of this momentous Transaction. No Motions recorded. No Yeas and Nays taken down. No Alterations proposed. No debates preserved. No Names mentioned. All in profound Secrecy. Nothing suffered to transpire: No Opportunity to consult Constituents. No room for Advice or Criticisms in Pamphlets, Papers or private Conversation. I was very uneasy under all this but could not avoid it. In the Course of this Confederation, a few others were as anxious as myself. Mr. Wilson of Pennsylvania, upon one Occasion moved that the debates should [be] public, the Doors opened, galleries erected, or an Adjournment made to some public Building where the People might be accommodated. Mr. John Adams seconded the Motion and supported it, with Zeal. But No: Neither Party were willing: some were afraid of divisions among the People: but more were afraid to let the People see the insignificant figures they made in that Assembly. Nothing indeed was less understood, abroad among the People, than the real Constitution of Congress and the Characters of those who conducted the Business of it. The Truth is, the Motions, Plans, debates, Amendments, which were every day brought forward in those Committees of the whole House, if committed to Writing, would be very voluminous: but they are lost forever. The Preservation of them indeed, might for any thing I recollect be of more Curiosity than Use.

See [John Adams' journals](https://founders.archives.gov/?q=Ancestor%3AADMS-01-02-02-0006&s=1511311111&r=9) for what little information there is about these debates.

# Yorktown 1781

(p. 518) - Some discussion of Hamilton's concerns about the new nation, its
economy, and government.  He wanted a strong, central government to expand the
powers of Congress rather than the loose union under the AoC.
