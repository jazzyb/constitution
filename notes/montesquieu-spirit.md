# GENERAL NOTES
* Jefferson justified his preference for an agrarian republic rather than an
  industrial one on Montesquieu's account of social conditions that make
  republican government possible
* anti-federalists worried that the constitution did not encourage civic
  virtue; that it ignored and actively discouraged virtue
* p. xxvii: "[John] Witherspoon ... added the quality of moral earnestness and
  anti-aristocratic self-reliance that can lead to political action as well as
  to the ministry.  In Federalist 10, Madison argues that a large republic
  could halt the republican tendency to develop majority and minority factions
  that dominate political life.  The effort to disperse large divisions and
  the confidence in large, modern governments follows in the Scottish
  tradition."
* the author of the introduction goes on to attribute the balance of powers in
  the constitution to Montesquieu's thought

# Part 1

## Book 1:  On laws in general

### § 1
* just as the material world is governed by invariable laws, so is the
  political -- the objects political law operates upon, humans, merely lack
  the consistency of the former
* the abstract laws which should govern intelligent agents have a reality
  (like math) even if intelligent agents don't exist
* (his explanations of the abstract laws that *ought* to govern human
  relations puts me in mind of game theory)

### § 2
* direct criticism of Hobbes:  denies that man's initial nature is a state of
  war -- "peace is the first natural law"
* men are drawn to one another and thus the desire for society is a natural
  law

### § 3
* state of war begins when individuals "find their strength" in society (not
  sure what that means)
* a system of government must suit the people under it; different cultures
  require different laws based on the temperaments, lifestyles, etc. of its
  people

## Book 2:  On laws deriving directly from the nature of the government

### § 1
* three kinds of government
  * republican: the people as a body have sovereign power
    * democracy:  when all the people together have sovereign power
    * aristocracy:  when only a part of the people have the sovereignty
  * monarchical:  rule by one under the law
  * despotical:  rule by one without established law

### § 2
* the people of a republic should elect a council or senate to represent them
* most citizens are competent enough to elect but not competent enough to
  rule; many republics have placed strict limits on the classes who rule
* public votes should be a fundamental law of democracy
* democracy also requires the people to make their own laws; the senate can
  make *temporary* laws, but the people should vote to make them permanent as
  was done in Rome and Athens

### § 3
* exorbitant power given to a citizen of a republic is worse than a monarchy,
  for when the monarchy is established, laws are put in place to curb their
  influence, but no such laws would need exist in a republic, and it would
  have no way to handle it
  * there are exceptions when a republic for whatever reason needs a
    magistrate with exorbitant power, but in that case the laws will be in
    place to support that government
* the best aristocracy is one in which the number and type of people without
  representation are so small as to not be oppressed by those in power
* the most imperfect is one in which the people who obey are enslaved by the
  people who command
* the closer an aristocracy resembles a democracy the more perfect it is, and
  the more it resembles a monarchy the worse it is

### § 4
* something must hold the king in check in a monarchy -- Montesquieu calls
  this a "depository of laws"
* the depository of laws is the preserver and interpreter of laws
* it cannot be chosen by the king else it would only reflect his momentary
  will
* in states without an explicit depository of laws, religion sometimes fills
  in the void

### § 5
* the fundamental law of despotism is that the despot will/should appoint a
  vizier to rule to allow himself to enjoy his position
* (I'm not sure if Montesquieu is saying that is good or bad or just the way
  things are.)
* the greater the subjects and matters of business, the less deliberation the
  despot will give them

## Book 3:  On the principles of the three governments

### § 3
* the citizens of a democracy must have *virtue*
* without virtue the people turn to ambition and avarice
* whereas the virtuous people want to be free within the law, without virtue
  they want to be free against it -- "what was a *rule* is now called
  *constraint*"
* (looks like "virtue" is explained in Book 5, § 2)

### § 4
* virtue is required but not as necessary to the aristocracy
* a great virtue will make the nobles equal to the plebs
* a lesser virtue will at least make the nobles equal to one another
* moderation found in virtue is the soul of such governments

### § 5
* virtue is unnecessary in monarchy, only law is needed and takes its place

### § 6
* honor -- "prejudice of each person and each condition" -- takes the place of
  virtue in monarchies

### § 7
* ambition can accomplish good in a monarchy even though it is bad in a
  republic

### § 8
* honor has no use in despotic states for all men are slaves

### § 9
* fear -- as opposed to virtue or honor -- is the necessary ingredient of
  despotism

### § 10
* religion is the only thing that has a chance of countering the despot's will
* really the only difference between the king of the monarchy and the one of
  the despotism is that the former is an enlightened ruler

### § 11
* republics, monarchies, and despotisms can exist without virtue, honor, and
  fear respectively; but if they do lack these qualities, they are
  imperfect/unstable

## Book 4:  That the laws of education should be relative to the principles of the government

### § 2
* honor must be taught in monarchies from the womb
* honor is:
  * politeness of manners towards others especially in the court
  * frankness, not for truth as an end in itself, but to demonstrate that one
    is daring and free and does not care what others think of them
  * courage to refuse orders that would bring dishonor on one or one's
    sovereign even if those orders have come from the king himself

### § 3
* education should be limited in despotisms; what education there is should
  bring about servility
* "[N]o one is a tyrant [in despotisms] without at the same time being a
  slave.  Extreme obedience assumes ignorance in the one who obeys; it assumes
  ignorance even in the one who commands; he does not have to deliberate, to
  doubt, or to reason; he has only to want."

### § 5
* in republics political virtue is the renunciation of the self
* in order to preserve democracy, all must love their laws and homeland
* education in republics must inspire this feeling

### § 6
* (examples of Greek institutions which may be useful in republics)

### § 7
* the education of the Greeks can only be accomplished in small states "where
  one can educate the general populous and raise the whole people as one
  family"
* a common currency accomplishes this in large societies (have I interpreted
  the last paragraph correctly???)

### § 8
* Plato, Aristotle, and Polybius indicate that the music of a people reflects
  or generates their constitution
* in Greek states any profession that could lead to earning money were beneath
  "free men"; e.g. craftsmen were denied citizenship

## Book 5:  That the laws given by the legislature should be relative to the principle of the government

### § 2
* political virtue:  love of the homeland and goodness in mores

### § 3
* love for one's democracy is a love of equality and a love of frugality

### § 4
* love of equality and frugality must be established in the laws of
  democracies to encourage their virtues

### § 5
* democratic laws should limit the control of land and inheritance to make
  sure no one grows too powerful
* equal distribution of land can only happen at the beginning of a democracy

### § 6
* land portions should not only be equal but small; the land should be enough
  to support a family but no more
* if a democracy is founded on commerce, wealth may encourage frugality and
  work ethic, but an excess of wealth destroys the spirit of commerce by
  transgressing against the equality of the democracy
* the laws of commerce in such a democracy should allow the poor to make
  enough to get by and prevent the rich from becoming too wealthy so they must
  continue to work to preserve or acquire

### § 7
* equal distribution of resources is not possible in all democracies; changes
  to laws that cause extremes should be avoided
* the democratic virtues may be encouraged through virtuous examples in the
  senate (a body where people are chosen for their age and wisdom) and an
  attachment to tradition
* if the senate is to rule (and thus to be a depository of mores), senators
  should serve for life; if the senate is to plan public business they should
  serve for three months
* subordination of the young to the old -- paternal authority -- encourages
  good mores
* likewise, subordination of the citizens to the magistrates gives force to
  the laws

### § 8
* in an aristocracy moderation and simplicity of manners on the part of the
  nobles takes the place of the virtue of equality
* nobles should blend in with the general population
* the sources of disorder in aristocracies are:
  1. extreme inequality between the rulers and the ruled
  2. extreme inequality among the members of the ruling class
* nobles should pay as much if not more money than the populous
* distributions to the people lead to disorder in democracies, but can be a
  benefit in aristocracies
* aristocrats should not levy taxes; this could lead to oppression
* aristocrats should not engage in commerce; because they set the laws, they
  may create monopolies that keep the populous out
* "commerce is the profession of equal people"

### § 9
* monarchies should be hereditary
* noble lands like noble persons should have privileges
* laws should prevent an undue burden on the populous that prevents their
  making a living

### § 10
* because execution of laws is faster in monarchy (as a result of the power
  being in only one) the law should introduce a certain slowness in the
  execution of the monarch

### § 13
* "When the savages of Louisiana want fruit, they cut down the tree and gather
  the fruit.  There you have despotic government."

### § 14
* laws should be simple and few in despotisms
* despotic governments need only secure the prince's palace to maintain
  control
* despotism works best when it is surrounded by deserts or barbarians, not by
  other states
* religion can be used for control by offering another source of fear
* the despotic prince should not claim all lands and make himself heir to all
  his subjects' property; to do so will take away all industry from the
  people; land and homes will fall into disarray
* local custom may be used to limit the greed of the prince
* the death of the prince will cause disorder without established laws of
  succession to the throne
* despotic princes usually take too many wives and have too many children;
  their family resembles the kingdom:  it is too weak and the leader
  too strong
* even given its inherent instability, despotisms are so common because
  despotism is easy -- only passion is required and every ruler has that

### § 15
* despotism makes prosperity for subjects nearly impossible

### § 16
* in a despotism all magistrates should be more answerable to the prince than
  any other person with authority over them; the prince cannot allow any of
  his officials to acquire their own power or authority

### § 17
* presents for the prince are fine in governments where no one is a citizen --
  like despotisms
* in republics magistrates should not receive any presents from the people for
  doing their duty

### § 18
* rewards from the sovereign to citizens:
  * despotism:  money
  * monarchy:  titles or other honors which lead to fortune
  * republic:  only testimony to the person's virtue

### § 19
* republics should force citizens to accept public employment; there is no
  need for such a law in monarchies
* a republic may appoint a magistrate to a lesser position; a monarchy would
  dishonor someone to do the same; and within a despotism it makes no
  difference
* in republics civil and military employment should be shared; in monarchies
  these two spheres should be kept distinct
* public offices should not be sold in republics or despotisms; it results in
  inequality in the former, and in the latter the prince must be able to
  appoint and remove officials at his will; it is a good thing in monarchies
  because it inspires industry
* republics should have supervisors of public morals ("censors"); not so in
  monarchies where the nature of honor keeps the citizens in check; neither in
  despotisms either

## Book 6:  Consequences of the principles of the various governments in relation to the simplicity of civil and criminal laws, the form of judgments, and the establishment of penalties

### § 1
* argument for legal precedent:  consistency in judgments
* one job of the legislator is to correct the errors of legal precedent
* there cannot be legal equality in governments that distinguish between
  people
* there is no court -- due to scarcity of laws -- in a despotism

### § 3
* in republican governments the courts must judge by the letter of the law,
  and they cannot interpret to the detriment of the citizen when it would
  endanger his property or his life

### § 4
* in monarchies, judges act as arbiters; they work out rulings with one
  another
* in republics, the judges must not communicate and must simply state guilt,
  innocence, or uncertainty

### § 5
* the prince is judge in despotisms
* the sovereign cannot be judge in monarchies or risk descending into
  despotism
* it would also take away his ability to pardon because it would make no sense
  to sentence someone to then turn around and pardon them

### § 6
* in monarchy there must be a distinction between the minsters of the prince
  and judges

### § 8
* origin of public prosecutor

### § 9
* in a moderate state the legislator will focus more on preventing crimes than
  punishing them
* "the more severe the punishments, the nearer the revolution"

### § 10
* spirit of monarchy in the old french laws:
  * in pecuniary punishment nobles are punished more severely than non-nobles
  * in corporal punishment nobles are punished much less severely

### § 11
* the more integrity the people have, the fewer penalties for crime are needed

### § 12
* government must not be quick to "correct" defects in the people with extreme
  punishments -- the old, moderate punishments usually suffice
* failure to punish results in more laxity than too moderate penalties
* being accused and found guilty should be the worst part of a penalty

### § 13
* examples of Japanese despotism
* the heir to the throne was smothered and all who knew about it kept quiet
  because it would cause bloodshed for everyone
* overly harsh punishments prevent execution of the laws

### § 15
* the Roman laws and how their penalties changed as their government changed

### § 16
* penalties must be proportioned to the crime
* if the penalty for murder is much harsher than robbery, then people will rob
  but avoid murder; if the penalties are the same, then the robber will always
  murder to hide the crime
* where there are no differences in penalties, there must be some difference
  in pardons

### § 18
* both pecuniary and corporal penalties must be used

### § 19
* laws of retaliation are found in despotisms, but moderate governments must
  temper it

### § 21
* in monarchies which run on honor, disgrace is often punishment enough
* clemency has much use in monarchies

## Book 7:  Consequences of the different principles of the three governments in relation to sumptuary laws, luxury, and the condition of women

### § 1
* luxury depends on the inequality between citizens
* for wealth to be equally divided, the state must provide only what is
  necessary to survive; otherwise, some will spend and others accrue and
  inequality will increase

### § 2
* the less luxury a republic has, the more perfect it is
* as luxury increases, political virtue decreases; the people turn from the
  love of the homeland to love of the individual; they become enemies to laws
  which hamper them

### § 3
* in aristocracies (because moderation should be a virtue) the aristocrats may
  have wealth, but they should not spend it -- thus no luxury

### § 4
* luxury is obviously necessary in monarchies
* because of the extreme wealth inequality, if the nobles do not spend, the
  poor will starve
* luxury is also necessary in despotisms -- if one is favored enough to have
  wealth to enjoy today, he does not know if it will be there tomorrow, so one
  must give over to one's vanities

### § 5
* sumptuary laws are only valid in monarchies when they are used to curb
  foreign imports

### § 6
* sumptuary laws can be used to curb laziness that would damage the state
* take China:  there are so many people and they produce so little food that
  everyone needs to work; therefore, they introduced legislation to prevent
  luxury

### § 7
* luxury has been the cause of numerous revolutions in China

### § 8
* "good legislators require a certain gravity in the mores of women"

### § 9
* in monarchies women have much luxury and little restraint
* in despotisms women *are* the luxury and are essentially slaves
* in republics women are free and do not have the luxury to get themselves in
  trouble

### § 10
* tribunals for women's crimes (except adultery) were held domestically in Rome

### § 11
* (fascinating:) as the accusations of women moved from the domestic sphere to
  the public in Roman society, men were given the opportunity to punish women
  by unjustly accusing them of adultery; the law was eventually changed so
  that before accusing the woman of adultery, the husband had to first be
  accused of encouraging her loose morals; accusations afterwards virtually
  ceased

### § 12
* in Rome women were under perpetual male guardianship

### § 13
* laws concerning women's mores changed as the Roman form of government changed

### § 15
* dowries should be considerable in monarchies
* " " " little in republics
* " " " non-existent in despotisms since women are essentially slaves there

### § 17
* women are just as capable at ruling an empire as men
* their natural gentleness can make for a very good government

## Book 8:  On the corruption of the principles of the three governments

### § 2
* democracy becomes corrupted when there is inequality and also extreme
  equality such that the people want to be the equal of those who command
* they believe they have a right to lead more than those in power
* this opens the way to despotism

### § 3
* "extreme equality" is having no master; the spirit of true equality is in
  having one's equal for a master

### § 4
* the people of a democracy may become ungovernable if great success on their
  part creates arrogance at the rulers

### § 5
* "aristocracy becomes corrupted when the power of the nobles becomes
  arbitrary"
* "extreme corruption occurs when the nobility becomes hereditary"
  * then it is an oligarchy
* nobles in aristocracies can maintain their principles when they find the
  laws burden them more than delight them

### § 6
* monarchies are corrupted when:
  1. a prince shows his power through change rather then maintaining order
  2. a prince reduces the needs of the state and its people to himself alone
  3. a prince begins to distrust his subjects

### § 7
* continued from above:
  4. the honor bestowed by the sovereign actually dishonors the recipient --
     as was the case for bestowing honors on informers
  5. the subjects think that what makes them owe everything to their king
     makes them owe nothing to their homeland

### § 8
* there is no drawback in moving between monarchy and republic, only falling
  into despotism

### § 10
* despotism is corrupt by its nature

### § 11
* if the principles are true and uncorrupted, the outcomes of the people's
  actions can't help but be good

### § 14
* a slight change in the constitution of the people can begin corruption

### § 16
* republics work best over small lands
* when the size of the republic grows so might a citizen's considerations and
  fortunes (which reduces moderation)
* monarchies are better when larger than a republic

### § 17
* monarchies should be medium sized -- too large and the king's
  deputies/governors might have enough power and distance from the king to
  disobey the king

### § 19
* a large empire demands despotism to keep governors in fear to obey the king

### § 20
* therefore, the size of the state forms its government and vice versa

# Part 2

## Book 9:  On the laws in their relation with defensive force

### § 1
* the difficulty of all republics -- democracies as well as aristocracies --
  is that if they are small, they will be conquered by a larger state, but if
  they are large, then they will be corrupted by internal vice
* the "federal republic" form of government fixes this; it is an association
  of sovereign republics which confederate for the common defense
* internally federation has all the advantages of small republics and
  externally the advantages of a large monarchy

### § 2
* federation is only possible for republics because the state of monarchies is
  war and expansion

### § 3
* a necessary law for federations:  no member state can make an alliance
  without the consent of all the others

### § 4
* despotic states, by comparison, separate themselves; they will even leave
  their borders barren or place feudal states between their capital as a
  buffer between their enemies

### § 5
* monarchies have strong borders with fortifications because a monarch can
  entrust power to subordinates where the despot cannot

### § 6
* states, in particular monarchies, must not be so large that they cannot
  quickly defend against threats to any of their borders
* thus, while monarchies should want to expand, they should also be mindful to
  limit their expansion to their ability to defend their territory

### § 10
* a monarch is better off having weak neighbors than conquering the same
  neighbors and having to defend those lands with his own power

## Book 10: On the laws and their relation with offensive force

### § 2
* states may go to war for the same reasons as men -- to defend themselves
* however, whereas men should never initiate an attack because they have
  access to just tribunals, states do not have such access and may
  occasionally need to strike first to preserve a longer peace
* *but* the only reason to do so is in a spirit of justice

### § 3
* conquest derives from the right of war -- that is it is just when it is done
  to preserve, not to destroy
* the relationship of a conqueror to the conquered happens in four ways:
  1. it allows the conquered to continue to rule themselves
  2. it establishes a new government and laws for the conquered
  3. it destroys the society and scatters it
  4. it exterminates all the citizens
* the conqueror should choose whichever option is more likely to preserve the
  empire
  * for example, if the conquered are enslaved, it should only be done long
    enough to prevent rebellion and integrate the slaves into society; once
    integrated they should be made full citizens

### § 4
* sometimes being conquered can be an advantage if there was no other way to
  usurp a bad government than to destroy it from the outside
* every conqueror has the obligation to make right the necessary evil he
  needed to do to succeed at his conquest

### § 6-7
* for a republic to conquer another republic goes against its nature; you
  cannot force a conquered people to submit and maintain democracy

### § 9, 11
* monarchies should only conquer neighbors to the limit that it can sustain
* as much of the conquered culture, mores, and laws should be preserved; only
  the army and the sovereign should change

### § 13
* Charles XII is an example of how not to conquer

### § 14
* Alexander the Great is an example of how to conquer

### § 15
* in order to prevent a conquered nation from falling into despair or
  rebellion, the people should still have some role in ruling themselves
* (The example Montesquieu gives is the Tartars in China making their military
  and government half Chinese and Half Tartar.)

### § 16
* a despotism must always maintain a personal retinue to protect the prince

### § 17
* a conquering despot would do well to return rule to the conquered sovereign
  as a feudal lord and thereby gain an ally

## Book 11: On the laws that form political liberty in its relation with the constitution

### § 2
* people use the word 'liberty' to mean any form of government that they prefer
* in democracies people are prone to confuse the power of the people with the
  liberty of the people

### § 3
* 'liberty' is the right to do everything the laws permit

### § 4
* power naturally leads to abuse; therefore, power must be checked by power

### § 6
* three powers:
  1. legislative power
  2. foreign executive power (executive)
  3. domestic executive power (judicial)
* if the legislature and the executive are joined, there is no liberty because
  nothing would prevent tyrannical laws from being carried out tyrannically
* if the legislature and judges are joined, life and liberty of the people
  would be arbitrary
* if executive and judges are joined, judges would become oppressors
* (when Montesquieu speaks of "judges" he means "jurors", too)
* jurors should be drawn from the people
* jurors should be of the same condition as the accused
* legislative representatives should be drawn from regions within the state
  and not from the body of the state at large
* there are always people -- nobles -- in a republic who are distinguished
  from the hoi polloi for one reason or another
* the legislature should be made up of these nobles on the one hand and
  representatives from the people in the other
* nobility should be hereditary
* the executive should be in the hands of a single person for immediate action
* the executive should have the power to convene the legislature as a check on
  legislative power
* the legislature should not have the power to reciprocally check the executive
* the legislature should have the right to examine how its laws are being
  carried out
* there are exceptions to the legislature and judicial branch being joined:
  1. nobles should be judged by their peers -- the noble house of the
     legislature
  2. the legislature must judge and moderate its own laws
  3. if a person violates the rights of the people and the magistrates will
     not punish, then the representatives of the people must take up as
     accusers the case with the nobles as judges
* the only legislative right the executive should have is the power of veto
* troops must either (1) be drawn from the people for a year only or (2) the
  legislature must be able to disband them when it pleases and the troops must
  live among the people and not in separate barracks

### § 8 - 19
* examples of all of the above from the history of Greece, Rome, and Gaul

## Book 12: On the laws that form political liberty in relation to the citizen

### § 2
* political liberty consists in security or at least in the opinion of
  security
* security is foremost about protecting people who are accused -- having laws
  against perjury, etc.

### § 3
* a man must be condemned by at least two witnesses

### § 4
* the penalty should match the crime
* there are four kinds of crimes:  those against...
  1. religion
  2. public mores
  3. tranquility
  4. security
* the penalty for simple sacrilege ought to be excommunication
* secret sacrilege ought not to be punished and is only a matter between the
  individual and god
* penalties for crimes against mores ought to be fines, shaming, and expulsion
  from the town
* against tranquility, exile and correction
* punishments for crimes against the security of goods ought to be punishments
  against the goods, not corporal punishment
* the death penalty should be reserved for crimes which endanger life

### § 5
* one must be very careful when hearing or punishing the crimes of magic and
  heresy; there is usually very little evidence, and even the best citizens
  can be accused

### § 6
* care must be taken with "the crime against nature" as well (homosexuality?);
  many in the past have been punished for it on the witness of a single child
  or slave

### § 7
* high treason should be well-defined to ensure that not every innocent action
  can be misconstrued as disrespect towards the sovereign

### § 8 - 10
* the crime of high treason should not be confused with other crimes (such as
  counterfeiting) so as to maintain the horror of treason

### § 11
* the law cannot punish thoughts, only actions

### § 12
* speech is only a crime when it prepares, accompanies, or follows a criminal
  act
* speech is a difficult thing to make a crime or punish -- the same words said
  in different tones can convey completely different meanings, and silence can
  often say more than words -- do we wish to punish silence?

### § 13
* writings can be more dangerous than speech, but when they do not cause high
  treason, they should not be considered high treason

### § 14
* a person's modesty should not be violated when punishing them

### § 15
* slaves may be informers, but they cannot be witnesses

### § 17
* failure to report knowledge of a crime should not be a crime itself unless
  it is the crime of high treason

### § 18
* once a republic has put down a rebellion, it is important to return as
  quickly as possible to the way things were run before

### § 20
* laws should be put in place to protect the accused until they are found
  guilty

### § 21
* creditors should not have the right to enslave their debtors to pay their
  debts

### § 23
* monarchs should not employ spies against their own people
* if a person has obeyed the laws, his home should be his asylum

### § 24
* no accuser should be anonymous

### § 25
* monarchs should govern by example and mustn't let their subjects feel like
  slaves

### § 26
* monarchs should be accessible to their people

### § 27
* monarchs should be an example of the virtue they wish to see in their people

### § 28
* monarchs should be extra mindful of how they banter with their subjects;
  even the most minor insult can be grievous coming from the mouth of one's
  sovereign

### § 29
* in despotisms religion can be used to fix what is arbitrary in the civil code

## Book 13: On the relations that the levy of taxes and the size of public revenues have with liberty

### § 1
* taxes should be levied with the needs of the state and its citizens in mind
* the *real* needs of the citizens should not be neglected to pay for the
  *imaginary* needs of the state
* taxes should be based on what the citizens *should* give, not what they
  *can* give
* if it is based on what they can give, then it should be based on what they
  are *always* able to give

### § 2
* nature rewards hard work
* if arbitrary power takes away that reward, it causes distaste of work and
  laziness

### § 4 - 6
* a republic should not tolerate increasing the taxes of slaves
* this holds for monarchies and despotisms as well

### § 7
* duties on commodities are the best way to tax
* such a tax should be paid by the seller

### § 8
* the duty paid should be only a fraction of the value of the thing

### § 9
* when tax law is confusing, the tax-collector -- as the interpreter of the
  law -- has arbitrary power over men's fortunes

### § 12
* the more liberty the subjects have, the higher taxes can be levied

### § 17
* the increase of troops takes money unnecessarily from the people
* it is contagious among countries; when one does it, all their neighbors feel
  compelled to do the same

### § 18
* well-governed states should save a surplus of taxes -- just like a man
  shouldn't live pay-check to pay-check

### § 19
* direct taxes (collected by paid officials of the government) should be
  preferred over tax-farming (tax-collectors who are paid by taking part of
  the taxes collected)

### § 20
* collecting taxes should not be made a wealthy, honored profession

# Part 3

## Book 14: On the laws in their relation to the nature of the climate

(There are many tenuous connections that Montesquieu makes between the effects
the cold has on the body and how it affects the passions of the people and,
thereby, their form of government.  None of this seems correct, nor does it
have much to do with the research I'm engaging in, so I'm going to read it but
skip making notes unless it has something to do with the political philosophy
of the founding fathers.)

## Book 15: How the laws of civil slavery are related with the nature of the climate

### § 1
* slavery is detrimental to both the master and the slave
* it makes the master harsher and more cruel
* there is little difference between slave and subject in despotisms
* in monarchy it is detrimental
* it runs counter to the constitution of republics
* it only encourages a power and luxury that men should not have

### § 2
* three ways that slavery was justified
  1. captives were enslaved because the only other option was to kill them
    * counter:  only in the heat of battle may it be unnecessary to kill,
      after the battle, there is no need to murder
  2. debtors could sell themselves to pay off debts
    * counter:  no free man can sell himself; selling implies a price; when a
      man becomes enslaved, all his property goes to his master; therefore,
      the master gets everything for nothing
  3. children of slaves became slaves by birth
    * counter:  this is the same as saying a man can be enslaved before he is
      born
* the law can punish because the criminal has enjoyed the protection of the
  law, but the law of slavery has never been useful for the slave; it is by
  its nature against him

### § 3
* "Knowledge makes men gentle, and reason inclines toward humanity; only
  prejudices cause these to be renounced."

### § 4
* religion will sometimes resort to slavery to win converts

### § 5
(A defense of African slavery:)
* sugar would be too expensive if free men cultivated and collected it
* just look at them -- they're black, not white
* African slaves do not have common sense; proof of this:  they value glass
  necklaces more than gold
* we cannot assume they are men like us
* the injustices towards African slaves are exaggerated

### § 6
* the only justified form of slavery is the one in which men willing give
  themselves over to a master -- "a gentle right of slavery"

### § 7
* some argue (Aristotle) that some men are born slaves
* this is counter to the theory of natural law in which all men are born equal

### § 8
* even given the above justifications, there are only a few places on earth in
  which slavery can be justified
* when one has slaves, the solution to a problem is to throw more men at it
* when men are paid for their work, one thinks of ways to make the work more
  efficient using machines or better technique

### § 9
* the nobles of Europe say they want slaves, that it would be more useful, but
  none of them are volunteering to become slaves
* "Do you want to know whether the desires of each are legitimate in these
  things?  Examine the desires of all."

### § 10
* two sorts of slavery:
  * "real" -- slaves to the land, serfs
  * "personal" -- household slavery
* extreme abuses happen when the two are combined

### § 12
* in Islamic states the master is the master of the virtue and honor -- as
  well as life -- of the female slaves
* this leads to laziness
* the power of the master should not extend beyond things of service to him --
  e.g. for sex

### § 13
* large numbers of slaves are not a burden in despotisms
* in moderate governments, the slaves should not be too numerous
* seeing other men be free, to have the protection of civil society and to not
  be a part of it, makes man bitter and vengeful; it leads to rebellion

### § 14
* arming slaves isn't as much of a problem in monarchies as it is in republics
  where a slave bearing arms is basically the equal of a citizen

### § 15
* the more citizens are warlike, the less they have to fear from armed slaves

### § 16
* the less distinction between the slaves and masters (with respect to work
  performed) the less the masters will need to fear the slaves; e.g. if the
  masters perform the same work beside the slaves, eat with the slaves, they
  will have nothing to fear
* the more the masters have to fear from the slaves, the more cruel the laws
  must become to maintain security; the less to fear, and the more "gentle"
  the law may be

### § 17
* the law must force master to take care of the health of their slaves and to
  clothe them
* master and slave who do not get along should be separated; e.g. the slave
  should be sold to a new master
* even though the law is necessarily different for the slave than the master,
  the law must protect slaves from injury as far as is possible

### § 18
* a republic cannot survive with too many slaves; likewise it cannot survive
  if too many slaves are freed all at once because they will not be able to
  take care of themselves
* the laws must prevent either extreme
* if all the slaves are to be freed, the state must find a way to do it
  piecemeal

## Book 16: How the laws of domestic slavery are related to the nature of the climate

(how climate affects the differences between the sexes and the role of women;
see note for Book 14 above)

## Book 17: How the laws of political servitude are related to the nature of the climate

(same as Book 14; see note above)

## Book 18: On the laws in their relation with the nature of the terrain

(how the terrain and access to resources gives rise to different governments;
many parts are more believable than Book 14, but still not applicable to my
research)

## Book 19: On the laws in their relation with the principles forming the general spirit, the mores, and the manners of a nation

### § 3
* there is real tyranny, but there is also a tyranny of opinion that people
  may feel when the ruler governs in a way that runs counter to the mores of
  the nation

### § 5
* laws should follow the spirit of the people when it is not contrary to the
  principles of government

### § 12
* changing the mores and manners of a despotic state is dangerous and could
  lead to revolution

### § 14
* laws should be reformed by laws and manners reformed by manners; it is bad
  policy to try to reform one by the other
* crime is improved with penalties, manners improved by example

### § 16
* mores are concerned with internal conduct, the law with external

### § 27
* (Montesquieu goes into a great deal of detail explaining what exactly it is
  about England that makes it England.  Similar to his description of its
  constitution in Book 11, § 6.  Honestly, it sounds like he is attributing
  arbitrary qualities of the country to their politics and international
  success.  Seeing the results and reasoning backwards as it were.)

# Part 4

## Book 20: On the laws in their relation to commerce, considered in its nature and its distinctions

(Begins with a *beautiful* invocation of the muses.)

### § 1
* commerce encourages gentle mores and vice versa

### § 2
* commerce produces peace between nations; they learn to depend on one another
* the same can not be said for commerce's affect between individuals
* in countries affected most by commerce, there is a sense of legal justice
  but also a sense that nothing should be given for free
* countries without commerce produce banditry, but they also have a strong
  sense of hospitality

### § 3
* two types of poor:
  1. some are poor because of their government and have no virtue because
     poverty is a part of their servitude
  2. some are poor because they avoid the comforts of life; their poverty is
     part of their liberty

### § 4
* in monarchies, commerce is founded on luxury, to meet the demands and
  fancies of the nation
* in republics, commerce is economic; the people are willing to invest their
  wealth for greater wealth

### § 6
* commerce that produces nothing can be useful -- bringing goods from one
  place to another can serve to increase trade in the future
* commerce which is a net loss can also be useful -- for example, whaling is
  an enterprise which rarely returns its cost, but the people who build the
  ships and equip them are also the largest investors, so they *over all*
  benefit from the enterprise

### § 7
* england's politics/laws change in pursuit of its commerce -- e.g. tariffs

### § 8
* economic progress is hampered by laws that limit commerce -- by, e.g.,
  denying exports of items produced in the country or they may only trade with
  ships of their country

### § 9
* the greater the competition the more just the price of goods

### § 10
* banks do not belong in monarchies -- they introduce a state where the one
  with power might be without wealth and the one with wealth might be without
  power
* similarly trading companies do not suit monarchies

### § 11
* free ports are useful in republics -- what the state loses in taxes it makes
  up for by the wealth generated from the industriousness of individuals

### § 12
* liberty tends to produce more obstacles to commerce than servitude
* (Montesquieu implies that the increased regulation improves the "liberty of
  commerce", but that seems to counter his other implications above that lack
  of regulation improves economic commerce.)

### § 13
* just as tax-farming is bad, the same being done by those collecting duties
  is bad

### § 14
* the state should never seize or confiscate the commodities of foreign
  traders

### § 15
* civil debts should not be punished corporally, but commercial debts may
  because commerce is in the interest of the state more than the individual

### § 16 - 17
* heirs should be forced to pay their father's debts (what a terrible law)

### § 19 - 22
* princes and nobles should not engage in commerce
* traders may become nobles, but they would have to give up their profession

### § 23
* a country with fewer exports than imports impoverishes itself

## Book 21: On the laws in their relation to commerce, considered in the revolutions it has had in the world

(All about the history of commerce in the known world from the time of the
Greeks.  In general this looks like another low information book.  I'll fill
in notes for chapters only when it seems appropriate.)

### § 1
* *what* is traded depends on the resources that are naturally available in as
  well as the culture of each country
* the indians export commodities and import metals

### § 2
* Africans can be taken advantage of because traders are able to sell them
  things of no value for high prices

### § 3
* the commerce of europe is balanced between the lazy south who has plenty of
  resources and the industrious north who need to trade for necessities

### CH. 4
* in ancient times most trade in europe happened along the mediterranian;
  since all the ports had pretty much the same resources, trade was not as
  extensive

### § 5
* "The history of commerce is that of communication among peoples."

### § 6
* commerce -> wealth -> luxury -> perfection of the arts

### § 20
* "[W]henever one prohibits a thing that is naturally permitted or necessary,
  one only makes dishonest the people who do it."

### § 21
* many european colonies were established to extend commerce rather than the
  ancient reason of founding a new town or empire
* as a result the mother country sets laws that the colony must only trade
  with the mother country
* the loss of commercial liberty is made up for by the protection of the
  mother country

### § 22
* spain became impoverished because they "abandoned natural wealth in order to
  have a wealth of sign" -- gold and silver -- which decrease in worth in
  proportion to their abundance

## Book 22: On laws in their relation to the use of money

### § 1
* as commerce increases bartering becomes cumbersome and money is necessary

### § 2
* precious metals make for good currency because they are durable, little worn
  by use, can be divided many times without being destroyed, and carried
  easily
* the ease with which one can exchange their silver for commodities
  demonstrates the health of a country's economy

### § 3
* (Montesquieu calls counterfeit currency "ideal money" as opposed to
  legitimate currency which he calls "real money".)
* there should be laws to prevent counterfeiting because trade is uncertain
  enough as it is; it should not be made more uncertain through bad faith

### § 5 - 6
* precious metals work due to their scarcity; history teaches that finding
  new, large quantities of silver or gold will cause inflation

### § 7
* how is the price of a thing fixed in silver or gold?
  * imagine the world only had a single commodity; 1/n<sup>th</sup> the total
    amount of silver would equal 1/n<sup>th</sup> the total commodity
  * in the real world 1/n<sup>th</sup> the total silver equals
    1/n<sup>th</sup> a portion of the total commodities in the world

### § 8
* Africans have a system of currency that is not based on availability but
  rather on how much they arbitrarily esteem they have for each commodity

### § 10
* (definitely need to reread this whole section if you are ever in the mood to
  study economics)
* "the exchange":  the relative abundance or scarcity of the currency of
  various countries
* the prince of a state can:
  1. establish a proportion between a quantity of silver and the same quantity
     of currency
  2. fixes a proportion between the various metals used for money
  3. sets the weight and grade of each coin
  4. sets the ideal value of the coin based on the above
* the relative value of currencies one with another is not entirely based on
  the quantity of silver but rather the relative need of funds between
  countries
* (Montesquieu has an interesting several paragraphs on the relative worth of
  currencies based on debt between states that is worth giving a second look
  to if economics ever come up.)
* the relative value of currencies one with another may not match the exchange
  of each currency with silver; if the exchange is the same, it is said to be
  "at par"; if high "high", and if low "low"
* when traders do a great deal of business in a country the exchange
  invariably rises

### § 13
* in roman times the emperors tampered with their currencies by creating
  alloyed currencies -- e.g. silver-coated copper
* modern-day (18th century) exchanges have curtailed these lies

### § 14
* exchanges give a certain amount of economic freedom to the subjects of
  despotic states who would not have had it otherwise

### § 15
* with the exchange wealth no longer belongs to any state in particular
* (interesting example of how the exchange has affected the laws in Italy)

### § 17
* currency based on public debt (as opposed to being tied to, e.g., silver) is
  disadvantageous to a country:
  1. if foreigners posses too much money that represents a debt, they will draw
     from the nation a considerable sum in interest every year
  2. lowers the exchange
  3. impost levied for payment on the debt hurts manufacturers
  4. it takes wealth from those who are industrious and gives to the idle
* if a state goes this route with their currency, it must be because they
  derive a great wealth from elsewhere

### § 18
* each state can play the part of creditor and debtor, but it cannot become a
  debtor to too great a degree or it will no longer be a creditor
* the state must form a fund for "amortization" to pay for the interest

### § 19
* in order to have the best commerce, silver must be able to be lent at
  interest -- but it must not be too high or it will drive away borrowers

## Book 23: On laws in their relation to the number of inhabitants

### § 1
* humans, unlike other animals, find all kinds of reasons not to propagate
  without bounds

### § 2
* marriage propagates the species better than the alternative because a stable
  family provides the resources necessary for children

### § 4
* in most countries the wife becomes a member of the husband's family
  (although Montesquieu implies this an arbitrary rule)
* family names give the idea of a succession of the family that instills a
  desire to extend its duration
* societies having no family name and only a given name for an individual are
  not as good

### § 5
* in societies with many wives, every child should inherit

### § 6
* bastards are a more "odious" thing in republics (where social mores are more
  important) than in monarchies

### § 7
* some states have magistrates who arrange and regulate marriage
* however, fathers should do this since the love of their children will lead
  them to choose a mate for their child prudently

### § 8
* in england daughters marry according to their own fancy without their
  parent's permission
* maybe this is because monastic celibacy has not been established there and
  so the women have no other recourse except marriage

### § 10
* in a state of nature, celibacy is difficult but many children is not
* when a state is formed, the opposite occurs

### § 11
* in rich or superstitious nations the poor multiply because they do not bear
  the burden of society but are themselves the burden of society
* however, people who are poor because they live under a harsh government have
  few children because they cannot provide for themselves much less new mouths

### § 12
* the more girls that are born in proportion to boys the more people the state
  will produce

### § 14
* the amount of land required for the cultivation of a state's resources
  (whether it be pasture land, grain crops, or vineyards) limits the number of
  inhabitants

### § 15
* when the land is unequally divided, some will be able to cultivate more
  produce than they can consume
* when that is the case, they will have to employ workers to help cultivate it
* this will provide extra time for some who must take up some kind of art to
  occupy their spare time
* (I'm not sure I correctly interpreted Montesquieu's argument above.)
* machines that create more efficiency are not necessarily desired -- they can
  reduce the labor required and deprive people of their livelihood

### § 17
* "Sir William Petty has assumed in his calculations that a man in England is
  worth what he would be sold for in Algiers.  This can be good only for
  England:  there are countries in which a man is worth nothing; there are
  some in which he is worth less than nothing."

### § 21
* the corruption of the mores in the roman empire led to a distaste for
  marriage in its citizenry
* in rome they feared that not enough children were being born to allow the
  republic to survive and so instituted penalties for citizens who did not
  marry and rewards to those who did and had many children
* religion greatly influences propagation -- sometimes encouraging it and
  sometimes retarding it
* the fewer marriages there are, the less fidelity even within the marriages

### § 23
* ("On the state of the universe after the destruction of the Romans". I
  really like this section.  I won't bother quoting the whole thing though.
  The bit about men coming out of the ground at the end is a reference to the
  quote on p.442.)

### § 28
* (In Montesquieu's time, the depopulation of Europe was a great concern.
  That may add context to a lot of his beliefs here in Book 23.)

### § 29
* "A man is not poor because he has nothing, but because he does not work."
* the man without wealth but with a craft is no poorer than the man with
  wealth but who does not work
* a man who teaches his children a craft increases his inheritance with the
  number of children
* a man cannot do so with his wealth and must divide the inheritance among
  children
* "A few alms given to a naked man in the streets does not fulfill the
  obligations of the state, which owes all its citizens an assured sustenance,
  nourishment, suitable clothing, and a kind of life which is not contrary to
  health."
* wealthy states should provide poorhouses to care for citizens who cannot
  care for themselves
* but poorhouses are not a cure-all; they can inspire a spirit of laziness
  which increase general poverty
* "I have said that wealthy nations needed poorhouses because fortune was
  subject to a thousand accidents, but one feels that short-term help would be
  preferable to perpetual establishments.  The ill is temporary; help must be
  of the same nature and applicable to the particular accident."

# Part 5

## Book 24: On the laws in their relation to the religion established in each country, examined in respect to its practices and within itself

### § 2
* a prince who is an atheist cannot be trusted; he has nothing to control him
* idolatry is better than atheism

### § 3
* islam is more likely to produce despotism than christianity does

### § 4
* truth aside, one can observe that christianity has more benefits for men
  than islam does

### § 5
* catholicism better suits monarchies
* protestantism better suits republics

### § 7
* religion should not be made into law
* religious precepts are set to perfect men, and perfection is not the purpose
  of the law
* if the laws try to perfect men, there will be no end to the laws that are
  made

### § 8
* "religion, even a false one, is the best warrant men can have of the
  integrity of men"

### § 10
* stoicism produced the best rulers

### § 14 - 17
* since both religion and laws are used to make men better, when either
  departs from that end, the other will move toward it; if the religion is
  lax, the laws will be harsher

## Book 25: On the laws in their relation with the establishment of the religion of each country, and of its external police

(Most of this book deals with comparisons of religions which isn't quite
applicable to what I want to study.  However, he does speak about some
intersections between religion and law, and I mention those notes below.)

### § 5
* the wealth of the clergy should not be allowed to grow unchecked because
  they are a perpetual organization, and whatever enters their "family" will
  never leave it

### § 9
* if a state allows many religions, it should demand that they tolerate one
  another as well as the state; otherwise, the religion which is repressed
  when in the minority becomes repressive when the majority

### § 10
* if the state has established a state religion, it must not allow the
  establishment of another

### § 11
* a prince should not change the established religion

### § 12
* penal laws should be avoided in the matter of religion

### § 13
* interesting letter by a Jew written in response to an 18-year-old Jewish
  woman being burned in an auto-da-fe in Lisbon

## Book 26: On the laws in the relation they should have with the order of things upon which they are to enact

### § 2
* do not confuse religious laws with civil
* religious laws are concerned with the perfect; therefore, they should never
  change
* civil laws are concerned with the good; therefore, they can change

### § 3
* laws requiring women to disclose their "immodesty" go against natural law
  because secrecy or privacy is a means of self-defense
* no one can call for a divorce except the couple involved

### § 4
* it goes against the laws of nature to make it a crime for a wife or son to
  fail to reveal the crime of their husband or father, respectively

### § 5
* athenian law obligated sons to feed their father's if they had become
  destitute unless the son:
  1. was born of a prostitute
  2. was (sexually) abused by the father
  3. was not taught a trade by the father
* the first two are good laws, but the last is not since in its case the
  father has only violated a civil regulation

### § 6
* inheritance is not a right of natural law
* who is allowed and how they inherit is for each society to decide
* *feeding one's children is a natural law -- inheritance is not*

### § 7
* the natural law (e.g. self-defense) is higher than religious law

### § 8
* in church law the infidelity of the husband or the wife are the same crime
* in civil law they are not
  * the wife's infidelity may result in a bastard which will be the husband's
    responsibility to care for
  * the husband's infidelity may result in a bastard, but it will not be the
    wife's responsibility

### § 9
* religious laws seek to improve the individual
* civil laws seek to improve society in general
* religion should not serve as the basis of civil law because religious
  principles are not often concerned with the general good of society

### § 10
* however, there may be cases where a civil law places limits on religious
  practice
* take the example of a religion which forbids polygamy, entering a state
  which (civilly) allows it
* the civil law should not allow a man to obey the religious precept of having
  only one wife unless there exists some way to compensate the wives he is
  leaving

### § 11 - 12
* human tribunals must not be ruled by religious convictions
* civil justice is concerned only with the guilt or innocence of a man's
  actions, not the condition of his soul

### § 13
* (civil laws and religions have different expectations of marriage)

### § 14
* mothers and sons should not be allowed to marry because the son owes
  unlimited obedience to his mother and the wife owes unlimited obedience to
  her husband -- this contraction overthrows natural law
* fathers marrying daughters is wrong as well but less repugnant -- since it
  is the father's obligation to raise his children, he has a relationship of
  education to them
* there should be a barrier to relationships between the educator and the
  educated
* incest between brothers and sisters inspires the same horror because, since
  they share the same household, their attraction and sexual union would upset
  the mores of the household
* repugnance to incest between first cousins comes from times when cousins
  often lived under the same roof like siblings
* the only thing that has been able to overcome our innate avoidance of incest
  is (false) religious practices -- Zoroaster, Egyptian pharaohs, etc.

### § 15
* the state cannot take the property of citizens even in the spirit of the
  public good
* citizens must be compensated if their property is used for the public good

### § 16 - 18
* laws made for the good of the state are "political laws"
* laws made for the good of the individual are "civil laws"
* example: the laws of inheritance of the monarchy are *political* laws
  because they are concerned with preservation of the government; the laws of
  inheritance among the people are *civil* laws because they are concerned
  with individuals only

### § 19
* what can be decided in a household by domestic law should not be decided by
  civil law (-- I think that's what it's saying???)

### § 20
* princes are subject to natural law -- because all nations exist in conflict
  with each other -- but not to civil laws

### § 21
* ambassadors should not be subject to the civil or political laws of a
  country because they are stand-ins for a prince and need the right to speak
  and act freely without fear of reprisal

### § 23
* political law is determined by this maxim:  "THE WELL-BEING OF THE PEOPLE IS
  THE SUPREME LAW"
* when a political law contradicts this principle, a new law must preserve it

### § 24
* police should be subject to "regulations", not "laws"
* crimes are punished by the laws
* police regulations are punished by the magistrate who oversees them
* because of the nature of law enforcement, the punishments should not be as
  harsh as the civil laws

# Part 6

## Book 27: On the origin and revolutions of the Roman laws on inheritance

(Only one chapter.  A discussion of how the laws of inheritance developed in
the Roman empire.  There is an interesting bit about the history of "trusts".
They were originally invented because laws prevented certain people from
inheriting.  So the way to get around this was to entrust the inheritance to a
legally valid heir who would then distribute it to the invalid heirs.
Otherwise there is not much interesting with respect to my research.)

## Book 28: On the origin and revolutions of the civil laws among the French

(Exhaustive examination of the history and development of the laws of France
from the Roman Empire to modern (mid-1700s) times.)

### § 38
* "To invite when one must not constrain, to lead when one must not command,
  is the supreme skill.  Reason has a natural empire; it has even a tyrannical
  empire:  one resists it, but this resistance is its triumph; yet a little
  time and one is forced to come back to it."

## Book 29: On the way to compose the laws

### § 1
* the spirit of the legislator should be the spirit of moderation
* the political and moral good is always found between two limits

### § 4 - 5
* sometimes a law can be made which goes contrary to its intent because the
  legislator has either not thought through the consequences or poorly
  understands it

### § 6 - 7
* the same law can have very different effects within different (cultural)
  circumstances

### § 10
* two laws may seem contradictory, but we must judge them by what they
  accomplish, not how they are worded

### § 11
* one cannot simply compare two laws from two different countries -- the
  system of laws in each country must be compared as a whole

### § 12
* (see § 10) two laws may seem the same, but again, we must compare their
  outcomes

### § 13 - 14
* laws must not be separated from the political or legal context in which they
  developed

### § 16
* the laws, to be understood, must be simple and direct
* (Montesquieu gives examples of laws using the words "worthy" and
  "distress".)
* vague or relative words such as those above should not be used in the law
* pecuniary penalties should not impose exact prices for punishments because
  the worth of currency fluctuates
* the law should deal only with material reality and should leave itself out
  of the spiritual realm
* the laws should not be subtle and should be written to be understood by a
  person of average understanding
* limit details -- exceptions, modifications, etc. -- to the bare necessity
  for the law to work
* changes must not be made to the law without sufficient reason
* for example, if a punishment is currently sufficient, there is no reason to
  increase it to be more burdensome
* if the reason for a law is legislated, then the reason given should be good
* the law can presume (e.g. insider trading presumes that an insider is making
  trades for material gain), but the law must not ask judges/jurors to presume
  (e.g. hate crimes)

### § 17
* (I don't understand.  Something to do with "edicts" and "decrees" being a
  good way of legislating, but "rescripts" are bad.)

### § 18
* uniformity in the laws isn't necessary so long as they achieve their purpose
  (peace in the society)

### § 19
* the passions and prejudices of the legislator are always present in the law

## Book 30: The theory of the feudal laws among the Franks in their relation with the establishment of the monarchy

(History of the laws of the Franks.  Similar to Book 28.  At least half the
book is a critique of a similar history book by Jean Baptiste Dubos.)

## Book 31: The theory of the feudal laws among the Franks in their relation to the revolutions of their monarchy

(Continuation of Book 30.)
