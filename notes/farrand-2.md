# July 14, Saturday

Luther Martin opened by calling for a single vote on the report as a whole.

Gerry wished before this question be put that they reexamine the introduction
of new states from the West.  "They will if they acquire power like all men,
abuse it.  They will oppress commerce, and drain our wealth into the Western
Country."(II, 3)  [Unlike the Atlantic states towards the western
territories???]  He thought a limit should be placed on the introduction of
new states that they should never outnumber the original thirteen.  He moved,
"that in order to secure the liberties of the states already confederated, the
number of representatives in the first branch of the states which shall
hereafter be established shall never exceed in number, the representatives
from such of the states as shall accede to this confederation."(II, 3)

King seconded the motion.

Sherman said there was no probability that the number of future states would
exceed the existing states.  Even if that did happen, it would be so far into
the future that it wasn't worth accounting for it now.  Besides our
descendants will as likely be members of the western states as the Atlantic.
For this reason alone, we ought to make no discrimination between them.

Gerry countered that if some children emigrated, then others would stay
behind, and he thought they must provide some means of protecting the
interests of those who stay.  Foreigners are emigrating to that land, too, and
it is uncertain what will become of it.

The committee voted against Gerry's motion.

Rutledge proposed that they reexamine the propositions relating to money bills
and equality of votes in the second branch.

Sherman preferred to vote on the whole motion at once.  Any question would
have them reiterating the same ground all over again.

Luther Martin agreed with Sherman.  He did not like the whole plan, but he
would prefer it over nothing.

Wilson remarked that even though the committee was divided on the question of
equality in the second branch, had their constituents voted as the delegates
did, then the motion would be two-thirds against and only one-third for.  What
hope do our constituents have when they find out their essential principles of
justice have been violated from the outset?  He hoped both the money bills and
equality clauses would be reconsidered.

Luther Martin disagreed with Wilson's "two-thirds" remark.  The largest
states, Massachusetts and Virginia, are the weakest.  He would rather two
confederacies be formed than a single one formed on any principle other than
an equality of votes in the second branch *at least*.

Wilson commented that of course one who says the minority does more than the
majority would think the minority was stronger than the majority.  Next, he
will be saying that the minority is richer than the majority; although, that
will likely be forgotten when the question of taxes and troops are brought up.

Gerry criticized Martin's language towards Massachusetts.  The states should
vote not equally but per capita (as individual representatives).  The money
bills should not be reconsidered.  Even though he did not wholly approve of
the bill, he would vote for it rather than throw it out altogether.

Pinckney moved that instead of an equality of votes, the second branch should
be organized as follows:

| State | # Delegates |
|-------|-------------|
| New Hampshire | 2 |
| Massachusetts | 4 |
| Rhode Island | 1 |
| Connecticut | 3 |
| New York | 3 |
| New Jersey | 2 |
| Pennsylvania | 4 |
| Delaware | 1 |
| Maryland | 3 |
| Virginia | 5 |
| North Carolina | 3 |
| South Carolina | 3 |
| Georgia | 2 |
| **Total** | **36** |

Wilson seconded the motion.

Dayton would never give up the equal vote of the smaller states -- yielding
their security for their rights.

Sherman preferred an equality of votes not for the security of the position
but for the preservation of the state governments.  He had no objection to the
members of the second branch voting per capita as had been suggested by Gerry.

Madison felt the motion of Pinckney was a reasonable compromise.

Gerry did not like the motion but could see no hope of success.  An
accommodation must be made, but it cannot occur on the ground of the present
motion.  He was against the partial confederacy that had been implied by
others.

King regretted that he had to differ from Gerry on this point.  The proposed
government is a general and national government over the people.  There is no
situation in which it will act as a federal government over the states rather
than individual citizens.  The objects of the government (the people) ought to
influence its operations.  There is no reason that the rule of representation
should differ between the first and second branches.  Two objections were made
against proportional representation.  The first -- that the principle went
against the present compact -- was inapplicable.  The second -- that the
smaller states were in danger of being overrun -- was unfounded; the general
government would have no temptation to intrude on the state governments.  He
would have no objection to aggregating all the state debts into one national
debt of about 70 million dollars to be discharged by the national government.
In order to secure the protection of the state governments, he proposed that
there should be a third branch of the legislature with equal votes to secure
their rights and dignities.  He felt that Massachusetts would never yield to
an equality of votes.  No government can last long that is not founded on just
principles, and it would be better to submit the confederation to more
confusion and convulsion than to allow such an evil as equal suffrage between
the states.

Strong spoke on the present report.  A committee had been appointed and had
proposed the current plan even though some members of that committee did not
want equal suffrage amongst the states.  Congress is at an end.  If no
accommodation is made, then the union is dissolved.  The small states had made
a concession on the point of money bills.  It was reasonable that the small
states should want concessions in return.  Given this he would approve the
report altogether.

Madison asserted that founding the legislature on equality of suffrage rather
than proportional representation was laying the confederation on improper
principles.  The current Congress, founded on those principles, has
disappointed every hope placed on it.  Paterson, himself an advocate for
equality of suffrage, observed "that representation was an expedient by which
the meeting of the people themselves was rendered unnecessary; and that the
representatives ought therefore to bear a proportion to the votes which their
constituents if convened, would respectively have."(II, 8)  Was this not
applicable to both branches of the legislature?  It was asserted that the
general government ought to be partly national and partly federal -- one house
representing the people and the other representing the states.  If there was
any validity to this argument, he would assent that it must be so; otherwise,
it must be abandoned.  The ground of compromise ought to be that when the
national government acts only on the people, the people ought to decide, but
when the government acts on behalf of the states, then the states get to
decide.  But there is no ground.  He asked for a single instance in which the
general government would not make a decision on the people individually.  The
people of the large states will accrue a proportional importance relative to
their superior numbers.  They will do it under a common government or alone --
they will secure it either way.  Delaware does not have equal weight with
Pennsylvania either in Congress or out of Congress.  He enumerated the
objections to equal suffrage:

1. the minority could veto the will of the majority
2. the minority could extort the rest for assent to other necessary measures
3. the minority could impose measures on the rest
4. the evil would only increase with time as new states would be admitted
   under the unjust principle
5. it would give a perpetuity to the control of the Northern states over the
   Southern

The real differences of interests lie between the Northern and Southern
states formed by the institution of slavery and its consequences.  If a
proportional representation be agreed upon, then day by day, the control in
the legislature would reach an equilibrium.

Wilson wished to add only a few words to what Madison had just said.  No one
had yet countered the argument that proportional representation was in any way
unjust.  It is only argued that equal suffrage in at least one branch is
necessary to secure the preservation of the states.  But is an equality of
votes necessary for that purpose?  Do the large states wish less than the
small to maintain their power?  "Are the large states less attached to their
existence, more likely to commit suicide than the small?"(II, 10)  The fault
of the existing confederacy is inactivity.  No one accuses them of governing
too much, only too little.  We are here to remedy this problem, but an
equality of votes brings us right back to the impotence of the present
Congress.  The new government must be founded on right first principles.

Ellsworth asked two (rhetorical) questions.  Of Wilson, "whether he had ever
seen a good measure fail in Congress for want of a majority of states in its
favor."(II, 11)  Of Madison, "whether a veto lodged with a majority of the
states even the smallest, could be more dangerous than the qualified veto
proposed to be lodged in a single executive magistrate, who must be taken from
some one state."(II, 11)

Sherman expected that the legislature would in some cases act on the federal
principle of requiring quotas.

Pinckney's motion was voted against.  King writes in his journal, "This
question was taken and to my mortification by the vote of Massachusetts lost
on the 14th July".(II, 12)

The committee adjourned.

Richard Henry Lee (who declined to serve on behalf of Virginia) writes to his
brother, Francis Lightfoot Lee, of rumors that the convention is proposing a
constitution similar to Britain's with a single executive and two houses of
the legislature.(IV, 169)

# July 16, Monday

The convention opened with a vote on the report as a whole.  It passed in the
affirmative.  The complete text of the resolution (including amendments)
follows (II, 13-14):

> Resolved -- That in the original formation of the Legislature of the United
> States the first branch thereof shall consist of sixty-five members -- of
> which number:

| State | Number of Delegates |
|-------|---------------------|
| New Hampshire | 3 |
| Massachusetts | 8 |
| Rhode Island | 1 |
| Connecticut | 5 |
| New York | 6 |
| New Jersey | 4 |
| Pennsylvania | 8 |
| Delaware | 1 |
| Maryland | 6 |
| Virginia | 10 |
| North Carolina | 5 |
| South Carolina | 5 |
| Georgia | 3 |

> But as the present situation of the states may probably alter in the number
> of their inhabitants the Legislature of the United States shall be
> authorized from time to time to apportion the number of representatives: and
> in case any of the states shall hereafter be divided, or enlarged by
> addition of territory, or any two or more states united, or any new states
> created within the limits of the United States the Legislature of the United
> States shall possess authority to regulate the number of representatives: in
> any of the foregoing cases upon the principle of their number of
> inhabitants, according to the provisions hereafter mentioned, namely,
>
> Provided always that representation ought to be proportioned according to
> direct taxation; and in order to ascertain the alteration in direct
> taxation, which may be required from time to time by the changes in the
> relative circumstances of the states -- Resolved that a census be taken
> within six years from the first meeting of the Legislature of the United
> States, and once within the term of every ten years afterwards of all the
> inhabitants of the united states of the manner and according to the ratio
> recommended by Congress in their resolution of April 18, 1783 -- and that
> the Legislature of the United States shall proportion the direct taxation
> accordingly.
>
> Resolved that all bills for raising or appropriating money and for fixing
> the salaries of the officers of the Government of the United States shall
> originate in the first branch of the Legislature of the United States, and
> shall not be altered or amended by the second branch -- and that no money
> shall be drawn from the Public Treasury but in pursuance of appropriations
> to be originated by the first branch.
>
> Resolved that in the second branch of the Legislature of the United States
> each state shall have an equal vote.

The sixth resolution was then agreed to without debate.  Reading:

> That the National Legislature ought to possess the legislative rights vested
> in Congress by the Confederation.(II, 16-17)

The next resolution was then brought up for debate.  Reading:

> And moreover to legislate in all cases to which the separate states are
> incompetent; or in which the harmony of the U.S. may be interrupted by the
> exercise of individual legislation.(II, 17)

Butler called for a specific enumeration of powers and an explanation of some
of the terms, particularly "incompetent", in order to make a judgment.

Gorham states that they are intentionally vague.  They are only meant to be
general principles which will be expanded later into precise and explicit
details.

Rutledge agreed with Butler's objection.  He moved that a clause ought to be
appended to specify the powers in general terms.

A vote was taken on Rutledge's motion, and it was divided.

Randolph expressed his disappointment in the committee.  "The vote of this
morning (involving an equality of suffrage in the second branch) had
embarrassed the business extremely."(II, 17)  The powers that had so far been
proposed had assumed a proportional representation in both houses of the
legislature.  He had intended to offer some propositions that might assuage
some of the distrust of the smaller states by allowing an equality of votes in
certain cases [-- see below].  But since the vote has come out as it has (and
New York would have voted for it as well), he would prefer to adjourn and let
the larger states figure out what their next steps would be in this "crisis"
and that the smaller states might also deliberate on how to conciliate.

[Randolph's accommodations were never officially delivered for debate.  A
summary of the accommodations that Randolph intended to propose follow(III,
55-6):
1. That the states have an equality of suffrage regarding certain questions
   such as declaration of war or admission of new states, but in all other
   questions suffrage should be proportional
2. That for certain questions more than a mere majority should be necessary
   for passing in the senate
3. That the states be allowed to make any laws not contrary to the articles of
   union
4. That there be a veto on state laws, **but** the state shall have the right
   to appeal the matter before the national judiciary
5. That if any individual finds himself oppressed by a law of his state, he
   may take the matter up with the national judiciary who may veto the law if
   it be found to be against justice or equality]

Paterson agreed that it was "high time" to adjourn.  The rule of secrecy
should be lifted, and the delegates should be able to consult with their
constituents.  No conciliation was possible from the small states on less than
equal suffrage in the second branch.

C. C. Pinckney wished to know whether Randolph meant adjournment for only the
day or adjournment indefinitely.  If he were to return to South Carolina, he
would never return.  It was also fanciful to think the states would ever
accord separately.

Randolph specified that he only meant adjournment until tomorrow.

Paterson seconded the motion.

The vote was divided.

Broom was against an indefinite adjournment.

Gerry stated that Massachusetts had been against adjournment for only the day
because they saw "no new ground of compromise"(II, 19).  Now that the states
seem to be willing to make an effort, they are for adjournment.

Rutledge could see no need of an adjournment because he could see no chance
for compromise.  The small states are fixed.  The large states only need to
decide if they will yield or not.  He figured they ought to do the best they
can and stay rather "than abandon everything to hazard."(II, 19)

The committee adjourned.

George Wythe officially resigns from the convention on this day.(III, 59-60)
He had been absent from the convention since June 5.

# July 17, Tuesday

The hour before the convention officially began this day, representatives from
the larger states (and a few from the smaller) met to figure out where to go
from here.  Madison characterizes the discussion as a time wasting effort.
Some on the side against equal suffrage felt that, since a division was
unavoidable between the larger and smaller, the larger states (with the
majority of the people) should develop their own proposal and submit it to the
people of the United States as a competitor to that by the smaller.  Others
felt that they should yield to the smaller states' demands and not let the
perfect be the enemy of the good.  "It is probable that the result of this
consultation satisfied the smaller states that they had nothing to apprehend
from a union of the larger, in any plan whatever against the equality of votes
in the second branch."(II, 20)

Gouverneur Morris moved to reconsider the whole resolution from yesterday.  He
wished to know how the legislature would be modeled before enumerating its
powers.  Since the second branch of the legislature still had an equality of
suffrage, he feared that would have an effect on the powers given to it.  The
motion was not seconded.  Madison writes, "It was probably approved by several
members, who either despaired of success, or were apprehensive that the
attempt would inflame the jealousies of the smaller states."(II, 25)

Sherman moved to make the following change to the sixth resolution, replacing
[or adding after -- I'm not sure --] "of individual legislation" with the
following:

> to make laws binding on the people of the United States in all cases which
> may concern the common interests of the union; but not to interfere with the
> government of the individual states in any matters of internal police which
> respect the government of such states only, and wherein the general welfare
> of the United States is not concerned.(II, 25)

Wilson seconded the motion.

G. Morris opposed it.  The "internal police" of the states ought to be
infringed in many cases including the case of paper money.

Sherman explained his meaning by enumerating several powers such as the power
of levying taxes on trade but not direct taxation.

G. Morris remarked that taxes on consumption was omitted and assumed that
meant that Sherman intended the government to resort to quotas and
requisitions "which are subversive of the idea of government."(II, 26)

Sherman acknowledged his omission of direct taxation.  Some provision must be
made for supplying the deficiency of other taxation, but he had not come up
with any specifics.

[In a letter to the Governor of Connecticut on Sept. 26, 1787 in introducing
the new Constitution, Sherman states that the taxation of imports will likely
be the principle means of revenue for the nation.  A direct taxation on the
states based on their population *could* be raised but need not be as long as
the states furnish their quotas.(III, 99-100)]

Sherman's motion was voted against.

Bedford moved for the following amendment to the sixth resolution:

> and moreover to legislate in all cases for the general interests of the
> union, and also in those to which the states are separately incompetent, or
> in which the harmony of the United States might be interrupted by the
> exercise of individual legislation.(II, 26)

G. Morris seconded the motion.

Randolph called the amended law "formidable", noting that it allowed the
general government to violate all the laws of the states and their
constitutions and with meddling with their internal police.  "The last member
of the sentence is also superfluous, being included in the first."(II, 26)

Bedford argues that it is not more formidable than the clause as it currently
stands.

Bedford's motion passed in the affirmative.

The committee now took up the final clause of the sixth resolution ("to
negative all laws..." etc.).

G. Morris opposed the power as terrible to the states and unnecessary if the
general legislature was given enough authority.

Sherman, too, thought it unnecessary.  State courts would not consider any law
valid if it went against the authority of the union.

Luther Martin skeptically asked, "Shall all the laws of the states be sent up
to the general legislature before they shall be permitted to operate?"

Madison thought the power was essential.  We need a general government as
powerful as we are proposing *because* the states will pursue their interests
to the detriment of the interests of the general government.  It will continue
to disturb the system unless effectively controlled by a veto on the state
laws.  Laws will be passed and have a negative effect before the national
legislature or judiciary can repeal them.  In all cases the state courts are
dependent on the legislatures.  [Examples Georgia and Rhode Island.]  A
national veto is the most effective and mild means to preserve the system.
The British veto shows the utility of the power.  There may be some way of
extending the power to veto into the state governments in order to give a
temporary effect to laws of immediate necessity.

G. Morris found himself more and more opposed to the veto.  The states would
be disgusted by the proposal.

Sherman believed the veto was an example of a wrong principle, being the
belief that a state law contrary to the articles of union would be a valid
law.

Pinckney urged for the necessity of the veto.

The motion to agree to the veto of state laws was voted against.

L. Martin moved for the following resolution:

> that the legislative acts of the U.S. made by virtue and in pursuance of the
> articles of union, and all treaties made and ratified under the authority of
> the U.S. shall be the supreme law of the respective states, as far as those
> acts or treaties shall relate to the said states, or their citizens and
> inhabitants -- and that the judiciaries of the several states shall be bound
> thereby in their decisions, any thing in the respective laws of the
> individual states to the contrary notwithstanding(II, 29)

It was agreed to without debate.

From the ninth resolution, "that the executive be a single person" was agreed
to without debate.

On the clause of the ninth resolution that the executive be chosen by the
national legislature:

G. Morris thought that if the executive was chosen by the legislature -- a
body who also had the power of impeachment over him -- he would be a creature
of the legislature.  The executive ought to be elected by the people at large.
They will never fail to chose a man of distinguished character.  If the
legislature choses him, then it will be the result of cabals and intrigue.  He
moved to replace "national legislature" with "citizens of the United States".

Sherman felt the "sense of the nation" would be better expressed by the
legislature then the people at large.  The people could never be informed well
enough on character, and no man will ever get a majority of votes.  The people
will generally vote for a man from their own state, and therefore men from the
largest state will have the best chance at taking the position.

Wilson addressed the concern that a majority of the people would never concur.
Perhaps there is no reason that a majority be needed to elect the executive --
this is not a requirement in most states.  If this is insufficient, then the
same might be done in the general government as is done in Massachusetts.  If
a majority of citizens cannot agree on a candidate, then the legislature
decides.  This would at least ensure a respectable candidate and prevent a
great deal of intrigue.  An argument against appointment by the legislature is
that he would be too dependent on them to defend the interests and liberties
of the people.

Pinckney was surprised that this was being argued.  There are many objections
to an election by the people.  They will vote for demagogues.  The most
populous states may combine to put their man in power.  The national
legislature, having the most interest in the laws, will elect the fittest man
to execute them.

G. Morris objected that the people of various states could never combine.  The
greatest danger of that lay in the legislature.  Pinckney says that the people
will be lead by a few designing men.  That can only happen in a small
district, not across the entire continent.  It is true from experience in New
York that partisans will carry their home districts, but it never has an
effect on the state at large.  The people, while they may be uninformed of
what happens in the legislature, will never be uninformed of the illustrious
characters who have earned their confidence.  If the executive be dependent on
the legislature, then it will result in tyranny by the legislature.

Mason notes the curious language used in these debates on the legislature.  At
one moment a man argues that the legislature is entitled to confidence and
unlimited power.  In another moment the same man argues that they are governed
by intrigue and corruption and cannot be trusted at all.  He likens letting
the people chose an executive to letting a blind man chose a color.  The
extent of the country makes it impossible for the average person to judge the
character of the respective candidates.

Wilson argued that there was no contradiction as Mason stated.  The
legislature can be confided in in some respects and should be distrusted in
others.  The appointment to great offices where the legislature might have
motives contrary to the public interest ought to arouse suspicion.  There are
notorious examples of such corruption in other legislative bodies.

Williamson made an analogy that election by the people was to election by the
legislature as election by lot was to election by choice.  The people are
aware of men of great character today, but that will not always be so.  The
people will begin to vote for men of their own state, and the largest state
will carry the vote.  (Virginia excluded because her slaves will never be
given suffrage.)  Since the executive's salary will be fixed, and he will be
ineligible to run for a second term, he will not be as dependent on the
legislature as many suppose.

The election by the people was voted against.

L. Martin moved that the executive be chosen by electors from the state
legislatures.  Broom seconded the motion.  It was voted against.

On the question to the words "to be chosen by the national legislature", it
passed unanimously in the affirmative.

"For the term of seven years" postponed without debate by Houston and G.
Morris.

"To carry into execution the national laws" and "to appoint to offices in
cases not otherwise provided for" both agreed to without debate.

Houston, seconded by Sherman, moved to strike out "to be ineligible a second
time".

G. Morris believed that taking away the possibility of reappointment
undermined good behavior.  "It was saying to him, make hay while the sun
shines."(II, 33)

On Houston's motion it passed in the affirmative.

The committee took up the question of term length (currently seven years).

Broom thought that now that a second term was on the table, the term should be
shorter.

McClurg moved to replace "of seven years" with "during good behavior".  By
making him eligible for a second term, they had made him forever dependent on
the legislature.  This would ensure his dependence.  [Madison suspects this
motion was made to enforce the argument against re-eligibility of the
executive by holding out an indefinite term as the only other alternative for
dependence from the legislature.]

G. Morris seconded the motion and was very pleased to hear the suggestion.
"This was the way to get a good government."(II, 33)  He had argued as he had
before under the assumption this would never be suggested.  Provided the
executive held his position during good behavior, he was indifferent to the
means of election.

Broom approved the motion.

Sherman disagreed.  Such a tenure is by no means safe or admissible.  With
re-eligibility now written back in, he will basically be on good behavior
anyway.

Madison stepped up and took a position less out of sincerity but more in order
to absorb some of the criticism he feared would be directed at McClurg whom he
greatly respected.  "The Doctor though possessing talents of the highest
order, was modest and unaccustomed to exert them in public debate."(II, 34)
The executive, legislative, and judiciary powers must be kept as independent
of one another as possible.  The executive will not be independent if he
serves at the pleasure of reappointment by the legislature.  This is the
reason the judges are not up for reappointment.  [Arguments for why
reappointment for the executive is worse than it would be for the judges.]
For this reason it is absolutely necessary that the executive and legislature
be independent.  Whether or not this motion accomplishes that sufficiently is
another question as it depended on the practicability of instituting a
tribunal for impeachment.  Regardless, respect towards Dr. McClurg entitles
the proposition to fair hearing until a less objectionable suggestion for
maintaining independence is suggested.

Mason pointed out that this motion had been moved before and voted against,
and he trusted that it would be voted against again.  Executive "during good
behavior" is only a softer way to say executive for life.  And it is a short
move from that to hereditary monarchy.  If the motion succeeded, he would live
to see the revolution.  If not him, then surely his children and
grandchildren.

Madison, on the contrary, wanted to prevent monarchy.  All power was being
thrown into the legislature of late.  The state governors are impotent; the
legislatures are omnipotent.  If nothing is done to check it, a revolution of
some sort will result.

G. Morris agreed with Madison.  The best way to keep monarchy at bay is to
give the people a republican government that will make them happy and prevent
a desire for change.

McClurg stated that he was not so afraid of the shadow of monarchy that he
would not be willing to approach it nor was he so wedded to republican
government that he could not see the possibility for tyranny exercised under
that form.  The executive must be independent from the legislature, and the
only way of ensuring that after re-eligibility had been added was to appoint
the executive for good behavior.

The motion of "during good behavior" was voted down.  [The vote was 4 to 6.
Madison said that not more than 3 or 4 people actually wanted the executive to
serve for life.  Those for the motion wanted to alarm those against into
reconsidering re-eligibility of the executive office and appointment by the
legislature.(II, 36)  Hamilton used this vote later in life to defend himself
from charges that he didn't support a republican government because he had
suggested an executive for life:  "the highest toned of any of the
propositions made by [Hamilton], was actually voted for by the representatives
of several states, including some of the principle ones, and including
individuals who, in the estimation of those who deem themselves the only
republicans, are pre-eminent for republican character."(III, 368-9) and "Thus,
if I sinned against *Republicanism*, Mr. Madison was not less guilty".(III,
398)]

The motion to strike out "seven years" was voted down.

The committee adjourned.

# July 18, Wednesday

"It was unanimously agreed that the vote which had struck out the words 'to be
ineligible a second time' should be reconsidered tomorrow."(II, 36)

The tenth resolution was postponed without debate.

Ditto the first clause of the eleventh resolution.

Taking up:  "the Judges of which to be appointed by the second Branch of the
National Legislature".

Gorham certainly preferred an appointment by the senate over the whole
legislature, but he felt that even that body would be too numerous.  He
suggested instead that the judges ought to be appointed by the executive with
the consent of two-thirds of the senate.  This is how Massachusetts did it.

Wilson said that he preferred an appointment by the executive without consent
of anyone else, but he would be willing to accept Gorham's suggestion.  He
moved that the judges be appointed by the executive, and G. Morris seconded
the motion.

Luther Martin was adamant about an appointment by the senate.  Being taken
from all the states, they would be the most capable of making a fit choice.

Sherman agreed with L. Martin.  The judges ought to come from many different
places, and this was best done by the senate rather than the executive.

Mason pointed out that the judges appointed might be called to try
impeachments of the executive.  If this were to be the case, they ought not to
have been chosen by the executive.  Also, the executive would remain in office
for a considerable time in the capital which would have to be in some state.
This would create local and personal attachments to the state which would
deprive equal merit and promotion elsewhere.

Gorham felt that given the office, the executive would be careful to look at
all the states for proper judges.  The senators will be as likely to form
local attachments as the executive.  Public bodies have no personal
responsibility and succumb to intrigue and cabal.

Gouverneur Morris thought it improper that the impeachment of the executive be
tried by the supreme judges.  They would be drawn into intrigue with the
legislature, and an impartial trial would be frustrated.  Plans for a
prosecution of the executive might even be made beforehand.  "He thought
therefore that no argument could be drawn from the probability of such a plan
of impeachments against the motion before the house."(II, 42)

Madison suggested an appointment by the executive with the consent of at least
one third of the senate.  This gives the responsibility to the executive for
the appointment while allowing the senate to prevent a corrupt nomination.

Sherman wanted appointment by the senate.  They would be nearly equal to the
executive, have more wisdom, and have a more diffuse knowledge of characters
to draw upon for nomination.  It would be less likely that they could intrigue
with the majority of the senate than the executive.

Randolph preferred the senate to appoint.  Personal responsibility for the
choices might be gained by requiring their individual votes to be entered in
the journal.  The senate would provide a more diffuse selection of candidates.

Bedford thought there were good reasons not to allow the executive the
appointment.  He would be required to know more about characters than the
senate.  He could gratify states and gain their favor with appointments from
their states.  In the present method, the executive cannot be punished for
mistakes.

Gorham remarked that the senate could have no better information on characters
than the executive.  The executive would be more answerable to public censure
for bad appointments.

The committee voted against appointment by the executive (without consent of
the senate).

Gorham moved "that the judges be nominated and appointed by the executive by
and with the advice and consent of the second branch and every such nomination
shall be made at least ______ days prior to such appointment."(II, 44)
Massachusetts for 140 years had used this method.  If appointment were left to
either house, it would be a "piece of jobbing".

G. Morris seconded the motion.

Sherman preferred it over an uncontested appointment by the executive but
thought it fettered the senate too much.

The vote on Gorham's motion was divided and failed.

Madison moved "that the judges shall be nominated by the executive and such
nomination shall become an appointment if not disagreed to within ______ days
by two-thirds of the second branch of the legislature."(II, 38)

G. Morris seconded the motion, but by common consent the motion was postponed
until tomorrow.

The clause "to hold their offices during good behaviour" of the eleventh
resolution was agreed to without debate.

The last clause of the eleventh resolution regarding compensation was taken
up.

G. Morris moved to strike out "or increase".  The legislature ought to be able
to increase based on the circumstance, and that ought not to create an
dependence in the judges.

Franklin favored the motion.  Revenue or the business of the department might
increase as the country grows.

Madison agreed that dependence would be less if increase alone were struck
out, but a dependence would be there.  What if senators are parties in suits
before the judges?  Increase or diminution might be guarded against by fixing
the salary to some commodity like wheat.  If the business of the department
increases, then the solution will be to hire more people, not pay them more.
An increase of salaries could be contrived so as to not affect those currently
in office.

G. Morris stated that the price of, e.g., wheat may not alter but the living
situations -- and necessary compensation -- might.  The salary must be
regulated by the style and manners of living in a country.  Additional
business would need to be performed by additional labor within that single
tribunal.  Additional compensation for additional labor must not be
prohibited.

The motion to strike out "or increase" passed in the affirmative.

The twelfth resolution was taken up.

Butler did not think inferior tribunals were necessary.  State courts could
fulfill the role.

L. Martin agreed.  They will create opposition and jealousy by interfering
with the state's jurisdiction.(II, 45-6)  In his "Genuine Information", Martin
gives more arguments against:  The inferior courts of the nation government
would eventually swallow up the jurisdiction of the state courts.  They would
also create an additional and enormous expense because they would need to be
duplicated with all their officers in most counties in all the states.(III,
206-7)

Gorham pointed out that there were already inferior federal courts in the
states which tried piracies.  The inferior courts were necessary for making
the national legislature effective.

Randolph did not believe that the state courts can be trusted with national
laws.  The policies of the general and local will often be at odds.

G. Morris thought the resolution was necessary.

Sherman was willing to grant the power to the legislature but wished they
would rely on the state courts whenever possible.

Mason thought circumstances might arise that they did not now foresee which
would make the power absolutely necessary.

The committee voted for the twelfth resolution in the affirmative.

The phrase "impeachments of any national officers" was struck from the
thirteenth resolution.  Madison suggested the whole resolution be reworded to
say, "that the jurisdiction shall extend to all cases arising under the
national laws: and to such other questions as may involve the national peace
and harmony".  This was agreed to without debate.

The fourteenth resolution was agreed to without debate.

The fifteenth resolution being taken up.

G. Morris thought the assumption of Congress's continuance ought to be
omitted.  Congress ought not to be continued until all the states adopt the
reform.  It may become expedient to give effect to it if a certain number of
states adopt it.  [What?]

Madison stated that the purpose of the clause was to prevent an interval
period in which neither government is in charge if the old one ceases before
the new one can begin.

Wilson did not entirely approve of the wording but acknowledged that something
must be provided for to prevent any suspicion that the Confederation is
dissolved along with the government.

The fifteenth resolution was voted against.

The sixteenth resolution being taken up.

G. Morris against the resolution.  He was very concerned that the current laws
of Rhode Island would be guaranteed.

Wilson stated that the resolution was only meant to secure the states against
"dangerous commotions, insurrections, and rebellions."(II, 47)

Mason felt that it would be bad if the general government could not prevent
rebellions within states -- to "remain a passive spectator of its own
subversion."(II, 47)

Randolph made clear that there were two parts to the resolution:  (1) secure
republican government (2) suppress rebellion.  Both provisions are necessary.

Madison moved to change the resolution to "that the constitutional authority
of the states shall be guaranteed to them respectively against domestic as
well as foreign violence."(II, 47-8)

McClurg seconded the motion.

Houston was afraid of perpetuating the state constitutions.  Georgia in
particular had a bad one.  The general government might also have difficulty
distinguishing between conflicting parties each of which claims the sanction
of the constitution.

L. Martin preferred to leave the suppression of rebellions up to the states
themselves.

Gorham thought it strange to have knowledge of an insurrection in the empire
and do nothing to stop it.  An enterprising citizen might establish a monarchy
in a state and spread his influence from state to state while the general
government could do nothing to prevent its own destruction.  As long as
disputants engage only in words they are harmless to the general government,
but once they draw swords it will be in the interest of the general government
to end the war no matter how difficult it is to decide on the merits of the
combatants.

Carroll asserted that the provision was essential and every state ought to
wish for it.  No room ought to be left to doubt that the general government
will come to the aid of the states.

Randolph moved to amend Madison's motion to add, "and that no state be at
liberty to form any other than a republican government."(II, 48)  Madison
seconded the motion.

Rutledge thought the amendment was unnecessary.  It was implied in the power
to come to the aid of states against rebellion.

Wilson moved to reword the whole resolution to read, "that a republican form
of government shall be guaranteed to each state and that each state shall be
protected against foreign and domestic violence."

Madison and Randolph preferred Wilson's wording and withdrew both their
motions.

The vote on Wilson's motion passed in the affirmative without debate.

The committee adjourned.

Madison writes to Thomas Jefferson that evening apologizing that the
proceedings are still under secrecy and therefore can tell him nothing.  He
does not know how much longer the work will take but does not figure it will
be done quickly.  "[I] have little doubt that the people will be as ready to
receive as we shall be able to propose, a government that will secure their
liberties and happiness."(III, 60)

# July 19, Thursday

Luther Martin moved to reinstate the words "to be ineligible a second time" to
the ninth resolution.

Gouverneur Morris argued that the best way to understand which attributes
should hold on the position of the executive was to look at them as a whole.
Quotes Montesquieu that a republican government cannot control a large extent
of country and admits that the U.S. is such a great country.  Therefore, the
executive should have such vigor and power to pervade every part of it.  He
must in particular control the legislature.  The executive must be the
guardian of the people against the influence of the legislature who in time
will come to represent the interests of the aristocracy.  The addition of the
senate was meant to be a legal check on a house of commons that might
legislate too much, but it is not a check on legislative tyranny.

The executive will have the power to appoint officers to administer public
affairs and judges.  Both of these will be felt immediately by the people, and
the people can judge well of them.  What effect, therefore, will ineligibility
to reelection have?
1. It will discourage acting in a way that will hold the esteem of the people
   by taking away the reward of reappointment.
2. He will be tempted to make the most of his time accumulating wealth and
   helping his friends.
3. It will lead to violations of the constitution because in times of danger
   to the nation he will be more tied to the forms of the constitution rather
   than doing what needs to be done.  [I *think* this is his argument here?]

Making the executive impeachable ties him too closely to the will of the
legislature, and he will be no check on it.  If he is to be a guardian of the
people, let him be elected by the people.  If he is to be a check on the
legislature, let him be unimpeachable.  Let him have a short term to hold him
accountable to the people.

Some argue that the people will not be a good judge of character.  If the
legislature have knowledge of a candidate's character, he must have such
notoriety and immanence that the people will have heard of him, too.  An
unimpeachable executive is not as dangerous as some say.  The subordinate
officers that the executive appoints -- ministers of finance, war, etc. --
*will* be impeachable, and the executive will not be able to do much without
them.  The executive's term should be for two years.

An election by the people over so great an empire can't be influenced by the
lies that pervade at the local level.  Perhaps the people's representatives
might influence the people, but they would not be able to do so very well.  If
the executive were popular, the representative would find himself unpopular by
opposing him, and if unpopular, by supporting him.

There is no means of making the executive independent of the legislature
except by either giving him an office for life or having the people elect him.
Other might think two years is too short a term, but he would continue in his
office as long as the people approved of him.  Such an ingredient in the plan
of government would be extremely palatable to the people.  Obviously, he felt
the whole structure of the executive branch deserved reconsideration.

Randolph supported the motion by Martin.  Assuming the executive is appointed
by the legislature, he should not be able to court reappointment in order to
remain independent.  He will probably be appointed by the legislature jointly
or nominated by the first branch and approved by the second.  In either case
the larger states will have the most influence.  If he can court reelection,
he will make his views subservient to those of the larger states.  The
legislature might also continue an unfit man in office in place of electing a
fit man in order to get their way.  Some argue that without a constitutional
reappointment, the executive will use unconstitutional means to maintain his
power.  This would only be the case if the people had become so corrupt as to
render all precautions hopeless.  An election by the legislature and incapable
for a second term will be more acceptable to the people than the plan
suggested by Morris.

[Before the Virginia Convention the next year, Randolph would admit that the
debates in the convention had changed his mind on this point.  "For, unless
you put it in his power to be reelected, instead of being attentive to [his
constituents'] interests, he will lean to the augmentation of his private
emoluments."(III, 329)]

King preferred re-eligibility for a second term.  As Sherman had already said,
the constitution should not prevent someone who has proved himself the fittest
candidate for an office from holding it.  It would be improbable for the
majority of the people to agree on a single candidate, but an appointment by
electors chosen by the people for such a purpose might be the least
objectionable.

Paterson agreed almost entirely with King.  The electors should be
proportional to the states such that the smallest were given one elector and
the largest were given three.

Wilson gathered that almost unanimously the delegates believed that if the
executive was appointed by the legislature then he must be ineligible for a
second term.  He was pleased that the idea of election directly (or nearly
directly) by the people was gaining ground.

Madison stated that it was more important that the executive and legislature
be independent than any other two combinations as a coalition of the two would
be the most dangerous enemy of public liberty.  Reappointment by the
legislature would not ensure the necessary independence, but perhaps even
appointment the first time would not either.  He believed the people were the
best source of election for the executive.  They would be as likely as any
other source to choose someone of a distinguished character; they could only
know and vote for someone of general attention and esteem.  He could see only
one difficulty:  The northern states had more eligible voters than the
southern states which would render the votes of the latter ineffective.
However, "[t]he substitution of electors [*since slaves counted for
three-fifths in representation*] obviated this difficulty and seemed on the
whole to be liable to the fewest objections."(II, 57)

Gerry asserted that if the executive is elected by the legislature, then ought
not to be eligible for a second term; otherwise, he will be completely
dependent.  The people should not elect the executive.  They are ignorant and
likely to be misled by a few designing men.  He preferred an election of the
executive by electors chosen by the state governors.  The pattern was the
people of the states to elect the lower house, the state legislatures to elect
the senate, and the state executives to elect the national executive.  The
states would be strongly attached to the general government under this system.
If the executive were elected by the people and did his duty, he would kicked
out for it -- like [Bowdoin](https://en.wikipedia.org/wiki/James_Bowdoin) in
Massachusetts or
[Sullivan](https://en.wikipedia.org/wiki/John_Sullivan_\(general\)) in New
Hampshire.

On the question by Morris to reconsider the general construction of the office
of the executive, it passed unanimously in the affirmative.

Ellsworth moved to strike out the appointment by the legislature and replace
it with

> To be chosen by electors appointed for that purpose by the legislatures of
> the states, in the following proportion
>
> *One person* from each state whose numbers, according to the ratio fixed in
> the resolution, shall not exceed 100,000 -- *Two* from each of the others,
> whose numbers shall not exceed 300,000 -- and *Three* from each of the
> rest.(II, 50)

Broom seconded the motion.

Rutledge was against all the other methods of election except appointment by
the national legislature.  Ineligibility to reelection would make his office
sufficiently independent.

Gerry agreed with Ellsworth but preferred appointment by the state executives
instead of the state legislatures.  He moved that the electors suggested by
Ellsworth should be 25 in number and alloted according to a specific
proportion.  [See II, 58 for a list of these proportions.]

On the question of the national executive being chosen by electors, it passed
in the affirmative.

On the question of the electors being chosen by the state legislatures, it
passed in the affirmative.

The specific ratio of electors for the states was postponed without debate.

L. Martin [once again] moved that the executive be ineligible a second time.

Williamson seconded the motion.  He did not trust electors chosen for
occasional purposes.  They would not be the most respectable citizens being
persons not holding any high office and would be liable to undue influence.

Ellsworth figured that anyone might be appointed an elector except national
legislators.

The convention voted against Martin's motion.

On the question of the executive term being seven years, it was voted against.

King did not want to shorten the term too much.

G. Morris preferred a short term.  If too long a term, then the power of
impeachment would be necessary.

Butler was against too frequent elections.  Georgia and South Carolina were
too far to send electors often.

Ellsworth wanted six year terms.  Too frequent elections and the executive
would be too weak.  He would have duties that would make him momentarily
unpopular, and his administration would be attacked and misrepresented.

Williamson also supported six years.  "The experience will be considerable and
ought not to be unnecessarily repeated."(II, 59)  If the elections are too
frequent, they will drive away superior candidates.

On the question for six years, it passed in the affirmative.

The committee adjourned.

Over the next few days, the following suggestion found its way into several
newspapers:

> So great is the unanimity, we hear, that prevails in the Convention, upon
> all great federal subjects, that it has been proposed to call the room in
> which they assemble -- Unanimity Hall.(III, 60)

Livingston writes to John Jay that he will be returning to Philadelphia
tomorrow to take the place of one of his colleagues.(IV, 174)

Williamson and Alexander Martin, both of North Carolina, are running out of
money.(IV, 174-5)

William Blount, who has been absent since July 2, writes, "I must confess not
withstanding all I heard in favor of this system I am not in sentiment with my
colleagues for as I have before said I still think we shall ultimately end not
many years just be separate and distinct governments perfectly independent of
each other."(IV, 175)

# July 20, Friday

Ellsworth's motion regarding the ratio of electors from yesterday was taken
up.

Madison pointed out that this would eventually make all the states equal since
they would eventually have the necessary inhabitants to earn 3 electors.  The
ratio ought to be temporary or set so as to adjust itself with the growth of
the nation.

Gerry moved that in the first instance the ratio ought to be the following for
the states:

| State | Electors |
|-------|---------:|
| New Hampshire | 1 |
| Massachusetts | 3 |
| Rhode Island | 1 |
| Connecticut | 2 |
| New York | 2 |
| New Jersey | 2 |
| Pennsylvania | 3 |
| Delaware | 1 |
| Maryland | 2 |
| Virginia | 3 |
| North Carolina | 2 |
| South Carolina | 2 |
| Georgia | 1 |
| **Total** | **25** |

The current question was postponed to take up Gerry's motion.

Ellsworth suggested two electors each be given to New Hampshire and Georgia.

Broom and [Luther?] Martin moved to postpone the question of the ratio of
electors, to the leave the question up to the committee of detail.

The committee voted against Broom's motion.

Houston seconded Ellsworth's motion.

The committee voted against this motion as well.

Williamson moved as an amendment to Gerry's motion that in future elections
the number of electors should be equal to the number of representatives in the
first branch.

On the question of Gerry's ratios, the motion passed in the affirmative.

The committee then took up the clause of the ninth resolution related to the
question of impeachment of the executive.

Pinckney, [probably] seconded by G. Morris, moved to strike out that part of
the resolution.  He ought not to be impeachable while in office.

Davie believed that if the executive was unimpeachable in office, then he
would spare no expense to keep himself reelected.  He considered impeachment
"an essential security for the good behavior of the executive."(II, 64)

Wilson concurred with Davie.

G. Morris believed the executive could do no criminal act without
co-conspirators who may be punished.  If he is reelected, that would be a
sufficient proof of his innocence.  Who is to impeach, and how will it work?
Will it suspend the functions of the office?  If not, then the crimes will
continue.  If so, then it will render the executive dependent on those who can
impeach him.

Mason said that impeachment was the most important point to continue.  No man
is above justice, much less the one who can commit the most extensive
injustice.  He had preferred a method of appointment by the legislators
directly.  One objection to the method by electors is that they could be more
easily corrupted.  This was good enough reason in favor of impeachment.
"Shall the man who has practiced corruption and by that means procured his
appointment in the first instance, be suffered to escape punishment, by
repeating his guilt?"(II, 65)

Franklin said the clause was actually favorable to the executive.  What is the
recourse when there is no formal method of impeachment?  Assassination --
which not only deprives the executive of his life as well as his office but
also removes the opportunity to vindicate his character.  It is best to
provide a means for the punishment of the executive *and* his acquittal when
he is unjustly accused.

G. Morris admits that corruption and other offenses ought to be impeachable,
but they should be clearly enumerated and defined.

Madison thought it indispensable to provide for a means of defending the
country from the incapacity, negligence, or treason of the executive.  A
limited term is not a sufficient security as he might immediately change as
soon as he takes office.  A single executive is different from a legislature.
Individual legislators can be corrupted, but the soundness of the remaining
members would maintain the integrity of the whole.  With a single man,
total corruption or loss of capacity would be more likely, and either could be
fatal to the republic.

Pinckney did not see the necessity of impeachment.  If the legislature holds
the power of impeachment over the executive, the executive loses his
independence, and his veto power would become insignificant.

Gerry urged for impeachment.  A good executive will not fear it; a bad one
ought to be kept in fear of it.  He hoped no one here believed that a chief
magistrate could do no wrong.

King reminded the members of a principle they all held since the beginning:
That the three great branches of government out to be independent of one
another as much as possible.  This would not be the case if the executive were
impeachable by the legislature.  They had all agreed that the judges ought to
be impeachable, but that was because they held their term during good
behavior.  [King supported a term of good behavior for the executive as well.]
If this were the case for the executive, he too would support impeachment.
But since the executive serves a limited term, it should be up to the electors
to decide if he discharged his duties sufficiently well.

Randolph declared that guilt, wherever found, ought to be punished.  The
executive will have great opportunities to abuse his power -- particularly in
times of war when the military and, to an extent, the public money is in his
hands.  If no legal means of punishment is provided, then it will be inflicted
by insurrection.  He acknowledged that the legislature and executive must be
separated as much as possible.  He suggested (as had been suggested by
Hamilton before) of impeachment by a forum of judges from the states and
requiring some preliminary inquest to determine if just grounds for
impeachment existed.

Franklin tells the history of the Prince of Orange.  During a war Holland and
France made an agreement to unite their fleets at a certain time and place.
When the Dutch fleet did not show up, people suspected that the "Statholder"
was at fault.  However, since he could not be impeached and no investigation
took place, he remained in power as the suspicion made his opponents more
formidable which eventually led to violence.  Had impeachment been possible,
the matter could have been investigated, and had he been guilty, he would have
been removed.  If innocent, he could have been restored to public confidence.

King countered that the Statholder held his position for life.  In such a
case, impeachment is applicable.  With limited terms, the periodical
responsibility of the electors are sufficient.

Wilson observed that if impeachment is pursued, it ought also to be made
applicable to the senators who will sit as long as the executive.

Pinckney presumed that the powers of the executive could be so circumscribed
that impeachments were unnecessary.

G. Morris spoke up to declare that his opinion had changed.  Impeachment was
necessary.  The executive was not like the typical magistrate who held his
term for life or even had a hereditary interest to the country.  He may be
bribed by a greater interest to betray his trust.  The executive ought to be
impeachable for treachery, corruption of electors, and incapacity.  (The
latter being not so much a punishment as a simple removal from office.)  Our
executive is not the king.  He is the prime minister, and the people are the
king.  We should, however, take care to architect the manner of impeachment so
as to avoid a dependence on the legislature.

On the question of the executive being removable by impeachment, the motion
passed in the affirmative.

On the clause "to receive a fixed stipend, by which he may be compensated for
the devotion of his time to public service", it was agreed to without debate.

On the clause "to be paid out of the national Treasury", it was agreed to
without debate.

Gerry and G. Morris moved "that the electors of the executive shall not be
members of the national legislature, nor officers of the U.S., nor shall the
electors themselves be eligible to the supreme magistracy."  This was agreed
to without debate.

McClurg asked whether, before the committee of detail is appointed, they
should determine *how* the executive is to carry the laws into effect.  Is
there to be a standing army?  Will he assume command of the militias?

Wilson agreed that some additional directions to the committee would be
necessary.

King answered that the committee will have the discretion to establish those
details themselves.

The committee adjourned.

# July 21, Saturday

Williamson moved that the electors should be paid out of the public treasury.
It was agreed to without debate.

Wilson moved that the judiciary should be combined with the executive in the
veto power.  He acknowledged that this motion had been voted against in the
past, but he felt it was important enough to try again.  The judges should
have an opportunity to prevent encroachments on the people and themselves.  Of
course, as expositors of the law, the judges would be able to protect their
constitutional rights.  Yet laws may be destructive or unwise but not so
unconstitutional to justify denying them effect.  A veto will give them this
opportunity to prevent such laws.

Madison seconded the motion.

Gorham did not see the advantage.  The judges cannot be presumed to have
knowledge of public policy.  The judiciary in England has no power, and their
jurisdiction is not invaded.  The executive should have the power with at most
the authority to consult the judges for their opinions.

Ellsworth supported it.  The judges will have more knowledge about the laws
which the executive cannot be expected to always possess.  The "law of
nations" will frequently come up of which the judges should be competent.

Madison considered it of great importance.  It would prevent legislative
encroachments on the judiciary.  It would support the executive by giving
confidence and firmness to the veto power.  It would even help the legislature
by preserving the consistency and technical propriety of the laws.  If any
objection exists, it would be that it gives too much strength to the executive
or legislature, but this is not the case.  Without this cooperation, the
legislature would overpower them both.  Our experience in the states shows
that the legislature eventually absorbs all power.  This is the real danger to
the American constitution.

Mason had always supported it.  It would give confidence to the veto without
which it would be of little avail.

Gerry was surprised this was coming up again.  The purpose of the veto was to
protect the executive from legislative encroachment.  Therefore, the executive
would be the best judge of exercising that power.  There were a number of
objections.  I created a dangerous coalition between the executive and
judiciary.  It made statesmen of the judges and expected them to be the
guardians of the people -- the proper jurisdiction of the legislature.
Pennsylvania provides a better solution:  "a person or persons of proper
skill, to draw bills for the legislature."(II, 75)

Strong agreed with Gerry.  The power of making the laws ought to be kept
separate from that of expounding them, lest the judges be influenced by the
part they had in framing them.

G. Morris stated the reasons for supporting the veto was because there needed
to be some check on the legislature.  Who should exercise that check?  An
executive who only serves for six years and is impeachable is not a sufficient
check.  The argument against the judges having this check is that the
expositors of the law shouldn't have a hand in making it.  The judges of
England, by example, have an influential hand in making the law.  They consult
on difficult bills.  They may even be members of the legislature or sit on the
council to the king.  The public liberty is at a greater danger from
legislative tyranny than from any other source.  Therefore a strong check is
necessary, and our executive office is too weak to provide the necessary
firmness alone.  [Examples of laws that will need to be vetoed:  "emissions of
paper money, largesses to the people -- a remission of debts and similar
measures".(II, 76)]  "The press is indeed a great means of diminishing the
evil, yet it is found to be unable to diminish it altogether."(II, 76)

L. Martin believed it was dangerous to combine the judiciary and executive in
this way.  A knowledge of legislative matters are no more likely to exist in
the judges than the legislators.  The judges already have a check on the
legislature in their official capacity.  Give them the veto, and they will
have a double veto.  The judges will lose the confidence of the people if they
engage in vetoing popular measures from the legislature.  In what measure and
proportion are they to be included in the suggested veto?

Madison claimed that he could see no danger to the separation of powers in
combining the executive and judges in the veto power.  If a constitutional
distinction of the powers on paper was sufficient, then this veto power would
not be necessary.  But experience teaches us to distrust this security and to
provide defensive powers to guarantee the paper provisions.  The objection
against the combination of the executive and judges in the veto power either
had no foundation, or it did not carry the power far enough.  If the veto
power in the judiciary went against the principle of separation of powers,
then the executive should not have the veto power either for the same reason.

Mason expected the legislature to often attempt to pass pernicious laws.  The
veto has the effect of not only preventing the passage of laws but also of
preventing demagogues from trying to pass them.  The judges will not have a
double veto as Martin said.  But the judges in their official capacity do not
have a veto.  They can only void unconstitutional laws, but they can't void
laws that are detrimental but not strictly unconstitutional.  He wished for
the inclusion of the judges in vetoing every improper law.  They would have
practice in considering the true principle of laws being passed.

Wilson states that the separation of powers does not demand different branches
cannot act on the same objects.  The legislature is divided into two houses,
but they act on the same object.

Gerry would rather give the executive alone an absolute veto than combine the
judiciary and executive against the legislature because the latter will be
unwilling to enter into a contest with the former.

G. Morris was surprised that any defensive provision for securing the
separation of powers would be considered an improper mixture of them.  If each
department consisted of a single man, would anyone object to uniting any two
to prevent encroachments by the third.  "As well it might be said that if
three neighbors had three distinct farms, a right in each to defend his farm
against his neighbors, tended to blend the farms together."(II, 79)

Gorham did not deny that a check on the legislature was necessary.  There are
two arguments against the combination that have not been addressed:
1. The judges ought to expound on the laws without any prepossessions on them.
2. The judges will outnumber the executive which will take the veto completely
   out of his hands and give him no defense against either department.

Wilson counter-argued.  Gerry stated that the combination would unite the
executive and judiciary against the legislature.  The fact is, they need the
added weight to balance the power of the legislature.  Responding to Gorham's
first point:  "supposing the prepossion to mix itself with the exposition,
the evil would be overbalanced by the advantages promised by the
expedient."(II, 79-80)  [What does that mean?]  To the second point:  A
detailed rule of voting may prevent it.

Rutledge declared the judges the most unfit to be involved in the veto.  They
ought not to pass judgment on a law until it comes before them.  It is also
unnecessary because if the executive needs a council to advise him he will
have the minister of finance, war, etc.

Wilson's motion for combining the veto power in the judiciary and executive
was voted against.

The tenth resolution (unamended) was then passed without debate.

Madison's postponed motion of July 18 "that the judges should be nominated by
the executive and such nominations become appointments unless disagreed to by
two-thirds of the second branch of the legislature" was resumed.

Madison restated his reasons for the motion:
1. The executive would be more capable of selecting fit candidates than the
   legislature -- even the second branch.
2. In cases of flagrant partiality or error, two-thirds of the senate could be
   joined to prevent it.
3. Now that the states represented in the senate have equal suffrage, the two
   authorities -- the people and the states -- should both have a stake in the
   appointment.  If only the senate made the appointment, a majority of states
   -- though only a minority of the people -- might appoint a judge which
   could not be justified on any principle.  The northern states would carry
   and leave the southern states out of the decision.

Pinckney preferred to leave the appointment with the senate exclusively.  The
executive would not have the knowledge of characters or the confidence of the
people for the responsibility.

Randolph would have preferred Gorham's suggestion based on the Massachusetts
constitution, but he supported this motion.  Appointments by groups always
descend into intrigue and cabal.  This would be true not matter which house of
the legislature had the authority.

Ellsworth stated that he would support a veto in the executive on appointment
by the senate which could be overruled by two-thirds of the senate but
preferred an absolute appointment by the senate.  Being a single man, the
executive will not have necessary knowledge of characters.  He will be more
vulnerable to intrigues than the senate.  The people will not like any
augmentation of his influence.  Hence, the veto of his nomination in the
senate will be an ideal only.  A nomination under such circumstances is
equivalent to an appointment.

G. Morris supported the motion.  The states (represented by the senate) will
often have a personal interest in the choice of judges.  "Next to the
impropriety of being a judge in one's own cause, is the appointment of the
judge."(II, 82)  The senate will be less informed of characters than the
executive.  The senators must rely on descriptions from their friends.  The
executive, on the other hand, having to interact with every region of the
union will have a more diverse knowledge.  If the executive can be trusted
with the command of the army, he can be trusted with the appointment of
judges.  If the objections to appointment of the executive by the legislature
had any weight, some weight must be given to the same arguments against the
appointment of the judges by the senate.

Gerry stated that the appointment of the judges -- like every power in the
constitution -- should give satisfaction to both the people and the states.
This motion does neither.  The executive cannot possibly be more informed of
characters than the senate.  The two-thirds requirement is also too high to be
of much consequence.  The senate would be composed similar to the current
Congress, and the appointments of Congress had been generally good.

Madison stated that he had chosen the two-thirds mostly arbitrarily, but he
was willing to change it to a straight majority if others wished.

Mason differed from his colleagues.  The motion essentially gives the power to
the executive alone as there will not likely be a sufficient disagreement to
the first nominations to veto.  Appointment by the executive will give him too
much influence over the judiciary.  He did not think the differences between
northern and southern states was applicable to this case.  It might require
some precautions in regulation, but it has no connection with the judiciary.

On Madison's motion, it was voted against.

On the clause "the Judges of which to be appointed by the second Branch of the
National Legislature" as it now stands, it passed in the affirmative.

The committee adjourned.

[Ellsworth's letter to his wife this day -- found in IV, 177 -- is hilarious
but has no connection to the convention.  I just wanted to mention it here so
I don't forget about it.]

Hugh Williamson writes to James Iredell on Sunday that he is hopeful that they
will have a detail of the amendment to the federal system soon.  He expects
the work to be done by September.(III, 61)

On Sunday Ben Franklin writes to John Paul Jones "that the convention goes on
well, and that there is hope of great good to result from their
counsels."(III, 61)

# July 23, Monday

New Hampshire is represented for the first time today.  The delegates having
arrived last Saturday.(III, 61)

| State | New Delegates |
|-------|---------------|
| New Hampshire | John Langdon |
|               | Nicholas Gilman |

The seventeenth resolution was approved without debate.

The eighteenth resolution was taken up.

Williamson suggested that a reciprocal oath be required of the national
officers to support the state governments.

Gerry moved to insert as an amendment that the national officers be bound by
oath to support the national government as well.  It was agreed to without
debate.

Wilson was not fond of oaths as they provided no security.  Good government
does not need them, and bad government ought not to be supported.  He feared
that binding them to a version of the government would prevent them from
amending the government which had been just agreed to.

Gorham also doubted that oaths would be of much use but made the point that
binding oaths would not prevent amendment.

Gerry agreed with Gorham's points.  There was, however, one good that might
come of oaths to the national government.  Officers of both governments had
until now considered the states as distinct from the general government and
had given a preference to the state.  The current resolution will cure that
error.

The resolution as stated was agreed to unanimously.

The nineteenth resolution was taken up.

Ellsworth moved, seconded by Paterson, that the plan be referred to the state
legislatures for ratification.

Mason thought the resolution as currently written was one of the most
important and essential resolutions.  The state legislatures have no power to
ratify it.  Being the creations of the state constitutions, they can be no
greater than their creators.  In some of the states, the citizens do not feel
their state governments rest on the authority of the people -- as is the case
in Virginia.  A national constitution derived from this questionable authority
would be exposed to severe criticisms.  Even if the legislatures had
sufficient authority and ratified it, succeeding legislatures, having equal
authority, could undo the past acts.  The new union would sit on a weak and
tottering foundation.  We must resort to ratification by the people.

Randolph asks, Who is most likely to oppose the new system?  It is the local
demagogues who derive their power from the state constitutions.  They will
spare no efforts to impede ratification for fear of losing their importance.
We must transfer ratification out of the hands of the legislatures where their
efforts will be less mischievous.

Gerry believed the arguments by Mason and Randolph incorrectly assumed an
unconstitutionality in the present federal system.  The confederation is
paramount to any state constitution.  The last article of the AoC authorizes
alteration of the constitution by the state legislatures.  Only great
confusion will result by putting the amendments before the people.  They will
never agree to anything.  The people will do nothing that their rulers cannot.
The rulers will either conform to or influence the will of the people.

Gorham enumerated reasons he preferred referring the amendments to the people
instead of the legislatures:
1. The people will be more objective and candid than their representatives who
   risk losing power under the new system.
2. Most of the state legislatures are composed of multiple houses.  It will be
   harder to get the amendments through than a single convention.
3. Many able men are sometimes excluded from the legislatures.  Clergy, e.g.,
   are friends to good government, and they were valuable in forming the
   Massachusetts constitution.
4. The legislatures will be interrupted by other business which will push off
   ratification for years, if not frustrate it altogether.
5. If the last article of the AoC is pursued, then only unanimous ratification
   will suffice.  Does anyone believe Rhode Island will approve?  Other states
   -- like New York -- may also follow in her footsteps.  Therefore, serious
   consideration needs to be made of getting the system up and running without
   unanimity.

Ellsworth said that if any legislatures felt incompetent to ratify, then they
would be at liberty to advise with their constituents and devise a more
competent plan.  More can be expected from the legislatures than the people.
The people of the eastern states wish to get rid of public debt, and the
present amendments will only strengthen public debt.  Mason's worry about a
weak foundation of the state legislatures is unfounded.  "An act to which the
states by their legislatures, make themselves parties, becomes a compact from
which no one of the parties can recede of itself."(II, 91)  With respect to
the legislatures having no authority in this case, a new set of ideas seem to
have crept into the public conscious since the AoC.  We exist in a federal
system by the consent of the state legislatures, not to conventions of the
people.  If our situation is so urgent and necessary to warrant a new compact
among only a part of the states by the consent of the people, then the same
pleas would be equally valid in favor of a partial compact founded on the
consent of the legislatures.

Williamson, by his reading of the resolution, believed that the present plan
could be ratified by either the legislatures or conventions recommended by the
legislatures.  He personally preferred conventions as they were most like to
be made of the ablest men of the state.

G. Morris thought that Ellsworth's last argument was a non sequitur.  If
alteration of the present constitution is pursued, then it *must* be approved
by all the states.  Any judge would declare a "partial" alteration as null and
void under the federal compact.  However, in a case of appeal to the people --
the supreme authority -- the compact may be altered by a majority of them,
just as the state constitutions can be altered by a majority of the people.
Ellsworth errs in thinking that we are preceding in accordance with the AoC.
This convention is unknown to the AoC.

King agreed with Ellsworth's arguments.  Having said that, he still believed a
convention of the people was the best approach.  They would be the most
certain way of obviating disputes and doubts about the legitimacy of the new
constitution as well as the most likely way of assembling the best men of the
state.  It was also a way of getting around of some of the scruples that some
state legislators had from their oaths to support and maintain the existing
constitutions.

Madison believed the legislatures were incompetent to make the proposed
changes because the new constitution would make inroads into the states *and*
it would set a dangerous doctrine to allow a legislature to change the
constitution under which it held its existence.  Some legislatures by their
state constitutions did not have the authority to alter the federal compact.
The difference between a system founded on the state legislatures and one
founded on the people is the same as a difference between leagues/treaties and
constitutions.  In the case of *moral obligation* the two were equally
inviolable.  In the case of *political operation* there were two important
distinctions in favor of the latter:
1. A law violating a preexisting law or treaty would be respected by the
   judges as law, although an unwise one.  A law violating the constitution
   ratified by the people would be null and void.
2. The doctrine of the law of nations holds that in the case of treaties, any
   one party breaching the treaty frees the other parties from their
   obligations.  In the case of a union of the people under the constitution,
   the nature of the pact excludes this interpretation.

Comparing the two modes, state conventions should be preferred over the
legislatures for examining and adopting it.

On Ellsworth's motion to offer the amendments to the state legislatures
instead of the people, it was voted against.

G. Morris moved that the plan be made to a single general convention.  It was
not seconded.

On the nineteenth resolution, it passed in the affirmative.

G. Morris and King moved "that the representation in the second branch of the
legislature of the United States consist of ______ members from each state who
shall vote per capita."(II, 85)

[C. C. Pinckney, defending the result of the above resolution before the South
Carolina legislature, stated, "the mode of voting in that body *per capita*,
and not by states, as formerly, would be a strong inducement to us to keep up
a full representation [...]  We shall thus have no delay, and business will be
conducted in a fuller representation of the states than it hitherto has
been".(III, 252)]

Ellsworth had always approved of voting in that mode.

G. Morris wished for the senate to be a numerous body and moved for the blank
to be filled with *three*.  If two members only are allowed, then a majority
of a quorum may be only 14 members which was too small for the public trust.

Gorham preferred two.  A small number was preferential for deciding some
matters like war and peace.  Besides, the number of states will soon increase.
The strength of the general government will lie not in the largeness but the
smallness of the states.

Mason thought three from each state would make the senate too numerous and
produce too much expense.

Williamson said that if the number is too great, the distant states would not
be on an equal footing with the nearer.  He approved of voting per capita.

On the question to fill the blank with *three*, it was voted against.

On the question of filling it with *two*, it passed unanimously.

Carroll did not have a specific objection, but he was worried about making
such a hasty innovation.

The motion passed in the affirmative.

Houston moved, seconded by Spaight, to reconsider the election of the
executive by electors chosen by the legislature.  He urged the extreme
inconvenience and expense of gathering together electors from all the states
for the sole purpose of electing the executive.

The motion was approved without debate and tomorrow was set for the
reconsideration.

Gerry moved that the resolutions for the establishment of a national
government (minus the executive sections) be referred to a committee of detail
to report a constitution.

C. C. Pinckney reminded the convention that if the southern states did not get
some security against the emancipation of slaves or taxes on imports, then he
would be bound by his duty to his state to vote against the report.

The committee of detail was approved without debate.

The convention agreed that the committee of detail consist of five members to
be appointed tomorrow.

The committee adjourned.

# July 24, Tuesday

The question on the appointment of the executive resumed.

Houston moved that the executive be elected by the national legislature rather
than the previously discussed electors.  It was improbable that capable men
would become electors in the more distant states.

Spaight seconded the motion.

Gerry opposed it.  The election of the executive being so important, great men
will seek the position in earnest.  If this motion is agreed to, then it will
be necessary to make the executive ineligible to serve a second term to ensure
the independence of his office -- an extremely repugnant idea.

Strong did not believe that an election by the legislature would require an
ineligibility to serve a second term.  By the time the executive serves his
first term, there will be a new legislature sitting.  The addition of the
electors makes the government too complex.  He, too, thought that the best
characters of the states would not seek the position.

Williamson supported the original resolution -- that the executive hold the
office for seven years and ineligible for a second term.  The electors would
be neither the first or second rate men of the state.  He also did not like
the unity of the executive, preferring three men instead, one from each region
of the union.  Different parts of the union have different interests, and the
appointment of a single executive will be dangerous for the regions he is not
from.  A single executive will be an elective king.  He will try to keep
himself in the position for life and create a path for the succession of his
children.  It was certain that eventually we will have a king, but he wished
to push this inevitability out as far as possible.  With the protection of an
ineligibility to a second term, he had no objection to a term of ten or even
twelve years.

Gerry moved that the state legislatures should vote for the executive in the
same proportion as had been determined to choose electors.  If a majority of
votes did not pick the same person, then the first branch of the national
legislature should choose two of the top four men, and the second branch
should choose the executive from those two.

King seconded Gerry's motion.  "[A]nd on the question to postpone in order to
take it into consideration, the *noes* were so predominant that the states
were not counted."(II, 101)

On Houston's motion of the election of the executive by the national
legislature, it passed in the affirmative.

Luther Martin and Gerry moved to reinstate the ineligibility of the executive
to a second term.

Ellsworth believed the executive would be reelected if his conduct warranted
it, and he would be more likely to render himself worthy of it if he could be
rewarded with a second term.  The best characters are more likely to seek the
position in the first place if we do not force a necessary degradation after a
fixed term.

Gerry understood that others wanted to make the executive independent of the
legislature.  The best way of doing this was increasing the length of the
term.  It would be better to set it to 10, 15, or 20 years and ineligible
afterwards.

King wished to make him re-eligible.  Gerry's suggestion is too great an
advantage to be given up for the small effect on his dependence.  These are
essentially terms during good behavior.

L. Martin suspended his previous motion and moved that the term be for eleven
years.

Gerry moved for fifteen.

King moved for twenty because it was the medium life of princes.  Madison
writes, "This might possibly be meant as a caricature of the previous motions
in order to defeat the object of them."(II, 102)

Davie moved for eight.

Wilson was sorry to see the election by the legislature reinstated and the
difficulties the issue caused in the convention.  The inconvenience of the
method of election was such that he would agree to any length of term to get
rid of the dependence on it.  No length of term would be equivalent to a
proper method of election except a term held during good behavior.
Ineligibility to a second term seemed to suggest that there was some point at
which a person no longer has a capacity for the office.  The Doge of Venice
was elected at the age of 80, and popes have generally been older.  "If the
executive should come into office at 35 years of age, which he presumes may
happen and his continuance should be fixed at 15 years at the age of 50, in
the very prime of life, and with all the aid of experience, he must be cast
aside like a useless hulk.  What an irreparable loss would the British
jurisprudence have sustained, had the age of 50 been fixed there as the
ultimate limit of capacity or readiness to serve the public."(II, 102-3)  He
hoped that a better mode of election would be adopted.  He moved to postpone
the current question to give more time for further deliberation.

Broom seconded the postponement.

Gerry wondered if they should refer the clauses relating to the executive to
the committee of detail.  Perhaps they might be able to come up with an idea
that will unite the house.

Wilson suggested a method which had not yet been mentioned.  The executive
could be elected for a term of six years by a small number of the legislature
-- perhaps only 15 -- chosen by lot, not by ballot.  Intrigue could be
avoided, and dependence would be diminished.  "This was not he said a digested
idea and might be liable to strong objections."(II, 103)

Gouverneur Morris asserted that voting by the legislature was the worst
method.  If the legislature has the power both to appoint and impeach, the
executive will be a mere creature of it.  Originally he had been against
impeachment, but now he was convinced that impeachments must be provided for
if the term is for any duration.

Others (Mason) had accused him of an inconsistency of trusting the legislature
in some occasions and distrusting them in others.  This is not the case.  The
legislature *ought* to be trusted when their interests align with the public
good, distrusted otherwise.  Much had been said about intrigues to get the
executive into office.  Less had been said about the intrigues to oust him.
Some leader will find a way to supplant him.  If a good government cannot now
be formed, we might find ourselves with something worse than a limited
monarchy.

In order to make the executive independent of the legislature, the invention
of making him ineligible a second time had been devised.  This is to say we
should give him the benefit of experience and then deprive ourselves of the
use of it.  Make him ineligible and set his term to 15 years, will he cease to
be a man?  He will conspire, with the power of the sword, to maintain his
position.  The most difficult task before them is to balance the
power of the executive -- too little power and the legislature will control
him, too much power and he will become a despot.  He preferred a short term,
re-eligibility, and a different method of election.  He was not sure about
Wilson's method of election just proposed, but it deserved consideration.
Chance should be preferred over intrigue.

On the question to postpone, it was voted against.

Wilson then officially moved "that the supreme executive shall be chosen every
______ years by ______ electors to be taken by lot from the national
legislature; the electors to proceed immediately to the choice of the
executive, and not to separate until it be made."(II, 97)

Carroll seconded the motion.

Gerry worried that this was committing too much to chance.  If unworthy men be
picked, the country will be saddled with an unworthy executive.  No method of
election involving the legislature could be a good one.

King argued that a lot might pick a majority of men from the same state who
would pick a man from that state.  We ought to be governed by reason and not
chance.  He wished the matter to be postponed since no one seemed satisfied.

Wilson did not even believe this was the best method.  He still preferred an
election by the people.  He seconded the postponement.

G. Morris observed that the chances were "almost infinite" against a majority
of electors from the same state.

On the question of postponement, it was agreed to unanimously.

Carroll noted that "the clause declaring that direct taxation on the states
should be in proportion to representation, previous to the obtaining an actual
census"(II, 106) as objectionable, and he would oppose it if included in the
report from the committee of detail.

G. Morris hoped the committee of detail would strike out the whole clause.  He
had only meant it as a bridge to get past a certain point in the debate.  Now
that that point had passed, the principle could be removed.  It was liable to
strong objections.  [Madison notes that the original purpose was to lessen the
objection to the representation of the southern states based on the population
of slaves.]

The delegates Rutledge, Randolph, Gorham, Ellsworth, and Wilson were selected
for the committee of detail.

The committee adjourned.

# July 25, Wednesday

Question on the executive clauses/resolutions taken up.

Ellsworth moved "that the executive be appointed by the legislature"(II, 108)
"except when the magistrate last chosen shall have continued in office the
whole term for which he was chosen, and be re-eligible in which case the
choice shall be by electors appointed for that purpose by the several
legislatures".(II, 107)  Thus the executive may be reelected without a
dependence on the legislature.

Gerry argued that any election by the legislature was radically wrong.  He
moved that the executive be elected by the state governors with the advice of
their councils (and where there were no councils, electors chosen by the state
legislatures).

Madison pointed out that there were objections to every conceivable method.
Among the methods were either to rely on the judiciary or legislature.  The
first was out of the question.  The second had a number of objections against
it:
1. The election of the executive would agitate and divide the legislature.
2. Candidates would intrigue with the legislature making the winner
   subservient to his faction's view.
3. It opens up the election of the executive to foreign influence.

Among the state powers, the executive could be elected by the state
legislatures, governors, or courts.  Election by the state legislatures runs
into many of the same problems as the national legislature.  The state
governors would be liable to intrigue and influence of foreign powers.  State
judges would not be viewed as a proper source of appointment.

The best options available were between an election directly by the people or
indirectly through electors.  Electors, being specially chosen for the
purpose, would be less likely to be influenced by cabal or corruption.
However, he preferred an election by the people directly.  This method, too,
had objections but none so dangerous as those before.  There was a point to be
made that people would be more likely to choose candidates from their own
states which would prevent great men from smaller states of taking the office.
He thought something might be come up with to obviate it.  The second valid
objection was the different proportion between the eligible voters in the
north and south.  His answer was that this would continually decrease as more
people moved to the south and that local considerations must give way to the
general interest.  As a southerner, it was a sacrifice he was willing to make.

Ellsworth stated that there was no answer for the objection that state
citizens will always choose their own man.  The larger states will always
carry the election.

On Ellsworth's motion, it was voted against.

Pinckney moved that in the election by the legislature, no man would be
eligible for more than six years in any twelve-year period.  This would have
the advantage and avoid some of the inconvenience of an absolute ineligibility
to a second term.

Mason supported it.  It made the executive independent without sacrificing the
advantage of future services.  He preferred election by the national
legislature, but he admitted that there was great danger in foreign influence
thereby.

Butler said that the two objects to avoid are domestic intrigue and foreign
influence.  Election by the national legislature avoids neither entirely.
However, the government should not be made so complex that it disgusts the
states.  This would be the case if the people elected.  He preferred
appointment by electors chosen by the state legislatures and equal suffrage in
this case.  He was against re-eligibility in all scenarios.

Gerry felt Pinckney's motion lessened the evil.

G. Morris was against a rotation of the office.  We would be always governed
by the scholars, never the masters.  The evils he saw to be avoided were:
1. undue influence of the legislature
2. instability of councils
3. misconduct in office

To prevent the first, we run into the second.  A change of men is ever
followed by a change of measures.  "Rehoboam will not imitate Solomon."(II,
113)  The rotation of the office will not prevent intrigue.  If the executive
does not have the opportunity to serve another term, he will likely look
forward to joining the legislature and will do nothing to endanger his
popularity there.  To avoid the third evil, impeachments would be necessary --
an additional reason against election by the legislature.  He thought election
by the people the best.  He would be willing to support Wilson's motion -- it
would not destroy cabal and dependence, but it might diminish it.

Williamson thought the principle objection against an election by the people
was the difficulty it would put on the smaller states, as had been previously
mentioned.  His suggestion was that each person make three votes.  The first
would likely be from his own state, but the other two would likely come from
somewhere else and as likely from a small state as a large.

G. Morris liked the idea, but wanted to amend it such that each man made two
votes, and one must not come from his state.

Madison thought something valuable could be made from this suggestion.  An
objection which occurred to him would be that after voting for the man from
their state in first place, they would throw away their second vote on someone
obscure to ensure the election of their first choice.  In all likelihood most
people would vote both with sincerity.  It might provided "that the executive
should not be eligible more than ______ times in ______ years from the same
state."(II, 114)

Gerry the people are to ignorant to put the election of the executive in their
hands.  Some set of men dispersed throughout the union and acting in concert
would delude the people and control the appointment.  The Order of the
Cincinnati is such an organization.  His respect for the men of the society
notwithstanding (Hamilton, Washington, and others were members of this
society), he would not be blinded to the danger of giving them this power.

Dickinson leaned towards an election of the people as the best and purest
mode.  There were valid objections against it, but none so dangerous as an
election by the legislature.  The principle objection was the preference of
voters for their own statesmen.  But can we turn this to a useful purpose?
Have the people of each state nominate their best candidates.  Of the thirteen
candidates, let the legislature or electors chosen by them make the final
appointment.

On Pinckney's motion that no one serve more than six years in a twelve-year
term, it was voted against.

The committee adjourned.

John Jay wrote to George Washington encouraging him to restrict the office of
the executive to only natural born citizens.(III, 61)

Otto (a French diplomat) writes to the Count of Montmorin about rumors he has
heard about the convention.  He gets many of them correct.  Of note is the
push-back he expects to see from New York:

> The partisans of the reform, Monseigneur, take care, in the meanwhile, of
> publicly attacking the most formidable of their antagonists.  Their features
> are chiefly directed against the Governor of New York, the most active and
> dangerous enemy of the power of Congress.  His personal interest is for him
> not to sacrifice any of his prerogatives and to preserve to his State all
> the rights of sovereignty.(III, 63 *translated*)

# July 26, Thursday

Mason began by summarizing the methods of election that had been proposed and
their objections:
1. Direct election by the people -- supposes that an act, which ought to be
   performed by those with the best view of character, should be performed by
   those with the least
2. State legislatures or state executives -- strong motions have been observed
<!-- #3 is same as #2 above -->
4. Electors chosen by the people -- has been rejected
5. Williamson's suggestion that the people vote for several candidates --
   looks promising at first, but on closer inspection has fatal objections;
   the Order of the Cincinnati will control any direct election
6. Dickinson proposes a method which would exclude every man who is unpopular
   in his own state -- even if the cause of the unpopularity might recommend
   him tot he states at large
7. Electors chosen by lot -- this will probably not be supported

After looking at all the objections, he felt compelled to choose election by
the state legislature as the best method available with the least downside.
Election for a second term ought to be absolutely prohibited.  The primary
goal of good government is to preserve the rights of the people, and the
executive should at a fixed term return to the mass from which they came to
make them feel and respect those rights and interests.  He moved to reinstate
the original resolution as it had been agreed to before, "that the executive
be appointed for seven years and be ineligible a second time."(II, 120)

Davie seconded the motion.

Franklin stated that the principle of republican government was that the
people were the superiors and the officers of government were their servants.
Some thought that the executive stepping down would be a denigration.  Instead
it should be looked at as a promotion.  "[I]t would be imposing an
unreasonable burden on them, to keep them always in a state of servitude, and
not allow them to become again one of the masters."(II, 120)

On Mason's motion, it passed in the affirmative.

G. Morris was against the whole resolution.  If indeed it was a principle of
republican government that its officers return to the mass of the people, why
do the judges serve during good behavior?  No one had proposed or even
conceived that the legislators should be ineligible for a second term.
Assuming that stepping down from office was a promotion rather than a
denigration, there is "no doubt that our executive like most others would have
too much patriotism to shrink from the burden of his office, and too much
modesty not to be willing to decline the promotion."(II, 120)

On the whole resolution:

> that a national executive be instituted -- to consist of a single person --
> to be chosen by the national legislature -- for the term of seven years --
> to be ineligible a second time -- with power to carry into execution the
> national laws -- to appoint to offices in cases not otherwise provided for
> -- to be removable on impeachment and conviction of malpractice or neglect
> of duty -- to receive a fixed compensation for the devotion of his time to
> the public service, to be paid out of the national treasury(II, 121)

it passed in the affirmative.

Mason moved

> that the committee of detail be instructed to receive a clause requiring
> certain qualifications of landed property and citizenship of the U.S. in
> members of the legislature, and disqualifying persons having unsettled
> accounts with or being indebted to the U.S. from being members of the
> national legislature

Debtors had frequently gotten into the state legislatures in order to promote
laws that would shelter their delinquency.

Pinckney seconded the motion.

G. Morris felt that if the qualifications were proper they ought to be applied
to the electors rather than the elected.  There are few debtors to the U.S.
government, and many having "unsettled accounts".  Such a discrimination would
be unjust and cruel.  The delay of settlement is more often the fault of the
government than the individuals involved.

Gorham supported the motion.

Mason mentioned the parliamentary qualifications that had been adopted under
Queen Anne and had met with universal acceptance.

Madison had witnessed the evils spoken of by Mason but thought it better to
limit the exclusion only to people who had received public money and had not
accounted for it.

G. Morris worried that the proposed resolution would allow members of the
government to find ways to exclude otherwise meritorious citizens from elected
office indefinitely.  The parliamentary qualifications mentioned by Mason had
been disregarded in practice and were "but a scheme of the landed interest
against the monied interest."(II, 122)

Pinckney and C. C. Pinckney moved to amend the resolution under question to
extend the qualifications to the executive and judiciary as well.  This was
agreed to without debate.

Gerry thought the advantages of the regulation out-weighted the possible
inconvenience of excluding a few worthy individuals.  In his opinion the
motion did not go far enough.

King worried that the *landed* qualification would exclude the monied
interests who might be needed in emergencies to the public safety.

Dickinson was against wealth qualifications in the constitution.  They would
necessarily be incomplete and would tie the hands of the legislature from
supplying omissions.  He disagreed with weaving a veneration of wealth into
the constitution.  Poverty and virtue were the objects of republican
principle.  "It seemed improper that any man of merit should be subjected to
disabilities in a republic where merit was understood to form the great title
to public trust, honors, and rewards."(II, 123)

Gerry argued that if protection of property was one goal of government,
provisions for securing it cannot be improper.

Madison moved the strike "landed" out of the qualification.  Landed
possessions were no certain measure of real wealth.  It had often happened in
the states that men had acquired property on credit and then joined the
legislatures to pass laws to limit the power of their creditors.  If a small
plot of land be made the qualification, it would be no security.  If a large
plot, it would exclude proper representatives of those classes of citizens who
are not landholders.  It was a provision throughout the union that
representatives be chosen from distinct districts so that the interests of
different regions be represented.  This principle is no less true for the
different classes of the nation -- the landed, commercial, and manufacturing
-- to make their rights felt and understood in the public councils.  He
wished some other means of qualification be developed that did not rely on
property.  He felt G. Morris was right to think the qualifications would make
more sense in the electors than the elected.  There would be difficulty,
however, finding a standard that would suit the circumstances and opinions of
the different states.

G. Morris seconded the motion.

The motion for striking out "landed" passed in the affirmative.

The first part of Mason's motion also passed in the affirmative.

The second part disqualifying debtors and other people having unsettled
accounts being taken up.

Carroll moved to strike out "having unsettled accounts".

Gorham seconded the motion.  It would disadvantage the commercial and
manufacturing classes the most since they were most likely to have dealings
with the public.

Luther Martin thought that if these words were struck out but the part about
debtors retained, then it would be in the interest of the latter classes to
keep their accounts unsettled as long as possible.

Wilson was for striking the whole thing out.  People could combine with rivals
to delay settlements in order to prolong qualifications of candidates.  We are
creating a constitution for future generations, not just our particular
circumstances.  The time may come again that individuals will be called to
aid the nation by opening accounts with the public, and such acts are a
measure of patriotism.

Langdon agreed with Wilson.  The people would never accept so many exclusions.

Gerry thought if the arguments just stated were to prevail, we would have a
legislature composed of debtors, pensioners, placemen, and contractors.  The
people would be pleased with the qualifications as they would know they
secured them against unnecessary burdens.  He moved to add pensioners to the
resolution, but this was immediately voted against.

G. Morris thought the last clause relating to public debtors will exclude
every importing merchant.  He thought to draw rules from the experience of
only the recent past that are to operate on succeeding generations is not very
wise.

On the question of striking out "persons having unsettled accounts", it passed
in the affirmative.

Ellsworth preferred to strike out the whole clause relating to debtors and
leave the prevention of such evils to the prerogative of the legislature.  How
small a debtor are we excluding?  Will everyone owing taxes be excluded?  How
will the voters know who are or aren't public debtors?

Pinckney supported the clause relating to qualifications of property but
disliked the exclusion of public debtors which went too far.  It would exclude
those who purchased confiscated property and purchasers of western lands from
the government.

On the question of disqualifying debtors, it was voted against.

Mason thought it might be proper to disqualify the seats of state governments
from being seats of the national government.  The first objection is that it
would produce questions of jurisdiction.  The second is that the mixture of
the two legislatures would produce a local preference into national affairs.
He moved that this disqualification ("until the necessary public buildings
could be erected"(II, 127)) be submitted to the committee of detail.

Alexander Martin seconded the motion.

G. Morris was not against the idea but cautioned that it might make enemies of
Philadelphia and New York who had expectations of being the seat of the
general government.

Langdon approved of the idea but asked what would happen if a state moved the
seat of its own government after the general government had been decided on.

Gorham said that the precaution could be gotten around if the national
legislature delayed the erection of the public buildings.

Gerry stated that neither a state capital nor large commercial city should be
the seat of the general government.

Williamson liked the idea but like the arguments which had come before,
thought it might make enemies or be evaded.

Pinckney thought state governments ought to be avoided but nearby or another
large town would be appropriate.

Mason did not mean to press the motion.  If it excited any passions against
the system, he was content to withdrawn the motion.

Butler supported fixing the seat of government in the constitution and making
it a central location.

The final resolutions were submitted to the committee of detail unanimously.
Both the Pinckney Plan and New Jersey Plan were referred to the committee.

The committee adjourned until Monday, August 6 to give the committee of detail
time to draft their report.

James Monroe writes to Thomas Jefferson that he is impressed by what he hears
coming out of the convention.  He will likely vote for whatever they
recommend.  He has heard a rumor that the general government will have a
negative on the state laws [*obviously out of date*].  He thinks this is a
good idea.(III, 65)

Nicholas Gilman writes to his cousin:

> Much has been done (though nothing conclusively) and much remains to do -- A
> great diversity of sentiment must be expected on this great occasion:
> feeble minds are for feeble measures and some for patching the old garment
> with here and there a shred of new stuff; while vigorous minds and warm
> constitutions advocate a high-toned monarchy -- This is perhaps a necessary
> contrast as "all natures difference keeps all natures peace" it is probable
> the conclusion will be on a medium between the two extremes.(III, 66)

He closes by predicting the business of the convention will not be done before
the first of September.(III, 66)

James McClurg writes to James Madison that he would return to the convention
if he thought he could improve the proceeding, but since it is filled with the
finest minds, he does not see the use and worries that he could only add
division.(III, 67 & IV, 205)  He writes of the concerns of some of the
statesmen back in Virginia:

> The doctrine of three confederacies, or great republics, has its advocates
> here.  I have heard Hervie [who?] support it, along with the extinction of
> state legislatures within each great department.  The necessity of some
> independent power to control the assembly by a negative, seems now to be
> admitted by the most zealous republicans -- they only differ about the mode
> of constituting such a power.  B. [possibly Beverley?] Randolph seems to
> think that a magistrate annually elected by the people might exercise such a
> control as independently as the King of G.B.(IV, 205)

John Langdon writes to Joshua Bracket:

> The convention -- well now see the convention:  Figure to yourself the great
> Washington, with a dignity peculiar to himself, taking the chair.  The
> notables are seated, in a moment and after short silence the business of the
> day is opened with great solemnity and good order.  The importance of the
> business, the dignified character of many, who compose the convention, the
> eloquence of some and the regularity of the whole gives a ton to the
> proceedings which is extremely pleasing.  Your old friend takes his seat.
> Conscious of his upright intentions, and as far as his poor abilities will
> go keep his eye single to what is righteous and of good report.(IV, 201)

# Comparison of Amended Randolph Resolutions and the Final Proposal to the Committee of Detail

| | **June 13** (I, 228-32) | | **July 26** (II, 129-134) |
|-|-------------------------|-|---------------------------|
| 1 | Resolved that it is the opinion of this Committee that a national government ought to be established consisting of a Supreme Legislative, Judiciary, and Executive. | 1 | Resolved that the Government of the United States ought to consist of a Supreme Legislative, Judiciary, and Executive |
| 2 | Resolved that the national Legislature ought to consist of Two Branches. | 2 | Resolved that the National Legislature ought to consist of two Branches |
| 3 | Resolved that the Members of the first branch of the national Legislature ought to be elected by the People of the several States for the term of Three years; to receive fixed stipends, by which they may be compensated for the devotion of their time to public service; to be paid out of the National Treasury; to be ineligible to any Office established by a particular State or under the authority of the United States (except those peculiarly belonging to the functions of the first branch) during the term of service, and under the national government for the space of one year after its expiration. | 3 | Resolved that the Members of the first Branch of the Legislature of the United States ought to be elected by the People of the several States -- for the Term of two Years -- to be the Age of twenty-five Years at least -- to be ineligible to and incapable of holding any Office under the Authority of the United States (except those particularly belonging to the Functions of the first Branch) during the Time of Service of the first Branch |
| 4 | Resolved that the Members of the second Branch of the national Legislature ought to be chosen by the individual Legislatures; to be of the age of thirty years at least; to hold their offices for a term sufficient to ensure their independency, namely seven years; to receive fixed stipends, by which they may be compensated for the devotion of their time to public service -- to be paid out of the National Treasury; to be ineligible to any Office established by a particular State, or under the authority of the United States (except those peculiarly belonging to the functions of the second branch) during the term of service, and under the national government, for the space of One year after its expiration. | 4 | Resolved that the Members of the second Branch of the Legislature of the United States ought to be chosen by the Individual Legislatures -- to be of the Age of thirty Years at least -- to hold their Offices for the Term of six Years; one third to go out biennially -- to receive a Compensation for the Devotion of their Time to the public Service -- to be ineligible to and incapable of holding any Office under the Authority of the United States (except those peculiarly belonging to the Functions of the second Branch) during the Term for which they are elected, and for one Year thereafter |
| 5 | Resolved that each branch ought to possess the right of originating acts. | 5 | Resolved that each Branch ought to possess the Right of originating Acts |
| 6 | Resolved that the national Legislature ought to be empowered to enjoy the legislative rights vested in Congress by the confederation -- and moreover; to legislate in all cases to which the separate States are incompetent: or in which the harmony of the United States may be interrupted by the exercise of individual legislation; to negative all laws passed by the several States contravening, in the opinion of the national legislature, the articles of union, or any treaties subsisting under the authority of the union. | | |
| 7 | Resolved that the right of suffrage in the first branch of the national Legislature ought not to be according to the rule established in the articles of confederation: but according to some equitable ratio of representation -- namely; in proportion to the whole number of white and other free citizens and inhabitants of every age, sex, and condition including those bound to servitude for a term of years, and three-fifths of all other persons not comprehended in the foregoing description, except Indians, not paying taxes in each State. | 6 | Resolved that the Right of Suffrage in the first Branch of the Legislature of the United States ought not to be according to the Rules established in the Articles of Confederation but according to some equitable Ratio of Representation |
| | | 7 | Resolved that in the original Formation of the Legislature of the United States the first Branch thereof shall consist of sixty-five Members of which Number New Hampshire shall send *three* -- Massachusetts *eight* -- Rhode Island *one* -- Connecticut *five* -- New York *six* -- New Jersey *four* -- Pennsylvania *eight* -- Delaware *one* -- Maryland *six* -- Virginia *ten* -- North Carolina *five* -- South Carolina *five* -- Georgia *three*.  But as the present Situation of the States may probably alter in the Number of their Inhabitants, the Legislature of the United States shall be authorized from Time to Time to apportion the Number of Representatives; and in Case any of the States shall hereafter be divided, or enlarged by Addition of Territory, or any two or more States united, or any new States created within the Limits of the United States, the Legislature of the United States shall possess Authority to regulate the Number of Representatives in any of the foregoing Cases, upon the Principle of the Number of Inhabitants, according to the Provisions herein after mentioned namely -- Provided always that Representation ought to be proportioned according to direct Taxation: And in order to ascertain the Alteration in the direct Taxation, which may be required from Time to Time, by the Changes in the relative Circumstances of the States -- |
| | | 8 | Resolved that a Census be taken, within six years from the first Meeting of the Legislature of the United States, and once within the Term of every ten Years afterwards, of all the Inhabitants of the United States in the Manner and according to the Ratio recommended by Congress in their Resolution of April 18, 1783 -- And that the Legislature of the United States shall proportion the direct Taxation accordingly |
| | | 9 | Resolved that all Bills for raising or Appropriating Money, and for fixing the Salaries of the Officers of the Government of the United States shall originate in the first Branch of the Legislature of the United States, and shall not be altered or amended by the second Branch; and that no money shall be drawn from the public Treasury but in Pursuance of Appropriations to be originated by the first Branch |
| 8 | Resolved that the right of suffrage in the second branch of the national Legislature ought to be according to the rule established for the first. | 10 | Resolved that in the second Branch of the Legislature of the United States each State shall have an equal Vote |
| | | 11 | Resolved that the Legislature of the United States ought to possess the legislative Rights vested in Congress by the Confederation; and moreover to legislate in all Cases for the general Interests of the Union, and also in those Cases to which the States are separately incompetent, or in which the Harmony of the United States may be interrupted by the Exercise of individual Legislation |
| | | 12 | Resolved that the legislative Acts of the United States made by Virtue and in Pursuance of the Articles of Union, and all Treaties made and ratified under the Authority of the United States shall be the Supreme Law of the respective States so far as those Acts or Treaties shall relate to the said States, or their Citizens and Inhabitants; and that the Judiciaries of the several States shall be bound thereby in their Decisions, any thing in the respective Laws of the individual States to the contrary notwithstanding |
| 9 | Resolved that a national Executive be instituted to consist of a Single Person, to be chosen by the National Legislature, for the term of Seven years; with power to carry into execution the National Laws; to appoint to Offices in cases not otherwise provided for, to be ineligible a second time, and to be removable on impeachment and conviction of malpractice or neglect of duty; to receive a fixed stipend, by which he may be compensated for the devotion of his time to public service; to be paid out of the national Treasury. | 13 | Resolved that a National Executive be instituted to consist of a Single Person, to be chosen by the National Legislature, for the Term of seven years, to be ineligible a second time; with power to carry into execution the national Laws, to appoint to Offices in cases not otherwise provided for, to be removable on impeachment and conviction of malpractice or neglect of duty, to receive a fixed compensation for the devotion of his time to public service, to be paid out of the public Treasury |
| 10 | Resolved that the national executive shall have a right to negative any legislative act: which shall not be afterwards passed unless by two-thirds parts of each branch of the national Legislature. | 14 | Resolved that the national Executive shall have a Right to negative any legislative Act, which shall not be afterwards passed by, unless by two-third Parts of each Branch of the national Legislature |
| 11 | Resolved that a national Judiciary be established to consist of One supreme Tribunal; the Judges of which to be appointed by the second Branch of the National Legislature; to hold their offices during good behaviour; to receive, punctually, at stated times, a fixed compensation for their services: in which no increase or diminution shall be made so as to affect the persons actually in office at the time of such increase or diminution. | 15 | Resolved that a national Judiciary be established to consist of one Supreme Tribunal -- the Judges of which shall be appointed by the second Branch of the national Legislature -- to hold their Offices during good Behavior -- to receive punctually at stated Times a fixed Compensation for their Services, in which no Diminution shall be made so as to affect the Persons actually in Office at the Time of such Diminution |
| | | 16 | Resolved that the Jurisdiction of the national Judiciary shall extend to Cases arising under the Laws passed by the general Legislature, and to such other Questions as involve the national Peace and Harmony |
| 12 | Resolved that the national Legislature be empowered to appoint inferior Tribunals. | 17 | Resolved that the national Legislature be empowered to appoint inferior Tribunals |
| 13 | Resolved that the jurisdiction of the national Judiciary shall extend to cases which respect the collection of the national revenue: impeachments of any national Officers: and questions which involve the national peace and harmony. | | |
| 14 | Resolved that provision ought to be made for the admission of States, lawfully arising within the limits of the United States, whether from a voluntary junction of government and territory, or otherwise, with the consent of a number of voices in the national Legislature less than the whole. | 18 | Resolved that Provision ought to be made for the Admission of States lawfully arising within the Limits of the United States, whether from a voluntary Junction of Government and Territory, or otherwise, with the Consent of a number of Voices in the national Legislature ______ less than the whole |
| 15 | Resolved that provision ought to be made for the continuance of Congress and their authorities until a given day after the reform of the articles of Union shall be adopted; and for the completion of all their engagements. | | |
| 16 | Resolved that a republican Constitution, and its existing laws, ought to be guaranteed to each State by the United States. | 19 | Resolved that a Republican Form of Government shall be guaranteed to each State; and that each State shall be protected against foreign and domestic Violence |
| 17 | Resolved that provision ought to be made for the amendment of the articles of Union, whensoever it shall seem necessary. | 20 | Resolved that provision ought to be made for the Amendment of the Articles of Union, whensoever it shall seem necessary |
| 18 | Resolved that the Legislative, Executive, and Judiciary powers within the several States ought to be bound by oath to support the articles of Union. | 21 | Resolved that the legislative, executive, and judiciary Powers, within the several States, and of the national Government, ought to be bound by Oath to support the Articles of Union |
| 19 | Resolved that the amendments which shall be offered to the confederation by the Convention, ought at a proper time or times, after the approbation of Congress to be submitted to an assembly or assemblies of representatives, recommended by the several Legislatures, to be expressly chosen by the People to consider and decide thereon. | 22 | Resolved that the Amendments which shall be offered to the Confederation by the Convention ought at a proper Time or Times, after the Approbation of Congress, to be submitted to an Assembly or Assemblies of Representatives, recommended by the several Legislatures, to be expressly chosen by the People to consider and decide thereon |
| | | 23 | Resolved that the Representation in the second Branch of the Legislature of the United States consist of two Members from each State, who shall vote *per capita* |
| | | 24 | Resolved that it be an instruction to the Committee to whom were referred the proceedings of the Convention for the establishment of a national government, to receive a clause or clauses, requiring certain qualifications of property and citizenship in the United States for the Executive, the Judiciary, and the Members of both branches of the Legislature of the United States |

# Notes from the Committee of Detail

Delegates:
1. John Rutledge
2. Edmund Randolph
3. Nathaniel Gorham
4. Oliver Ellsworth
5. James Wilson

## III

Wilson's notes contain an outline of the Pinckney Plan, so we know they at
least went over that.(II, 134-7)

## IV

Edmund Randolph (with amendments by John Rutledge [in *italics*]) has an
outline of the first draft of the constitution along with a few general
principles for the committee to follow:  [Note the ~~strikeouts~~.  Many
changes were made by striking some earlier parts out.  I only note what
differs from the resolutions as submitted.]

* "Two things deserve attention"(IV, 183)
  1. Express essential principles only.  The constitution will be rarely
     altered so specifics should be left to the accomodation of time and
     context.
  2. Use simple, precise language.  "~~For the construction of a constitution
     of necessity differs from that of law~~".(II, 183)
* There should be a preamble.
  * BUT not for designing the objects of government and policy (see the
    Massachusetts preamble) -- "This display of theory, howsoever proper in
    the first formation of state governments, is unfit here; since we are not
    working on the natural rights of men not yet gathered into society, but
    upon those rights, modified by society, and interwoven with what we call
    the rights of states".(IV, 183)
  * NOR to pledge the parties to the observance of the articles.  Perhaps the
    end would be a better place for that as in the AoC.
  * INSTEAD it should declare that:
    1. The present federal government is insufficient to the general happiness
    2. The present convention was a response to this fact
    3. The only way to fix it was to establish a supreme legislature,
       executive, and judiciary
* The two branches of the legislature are named the "house of delegates" and
  the "senate".
* They consider setting a maximum size of the house of delegates, but this is
  crossed out in the outline.
* A majority of delegates counts as a quorum, but a smaller group may meet to
  punish non-attending members. (Ditto for senate.)
* Delegates may not be arrested on legislative business. (Ditto for senate.)
* "~~[The delegates'] wages shall be...~~"(IV, 185)
* The house may not adjourn without the consent of the senate for more than
  one week.
* Senators have an minimum age of 25 years instead of 30.
* No property requirements for delegates, but there is one (though left blank)
  for the senators.
* The wages of the senators are based on the value of wheat as Madison
  proposed.
* "The senate shall not adjourn without the concurrence of the house of
  delegates for more than ~~one week~~ *3 days*".(IV, 187)
* The legislature have the power to raise taxes with the following
  exceptions/restrictions:
  * no taxes on exports
  * direct taxation apportioned to representation
  * no poll tax that does not apply to all individuals under the above
    limitation
  * no indirect tax which is not common to all
* Ditto regulate commerce with the following restrictions:
  * no duty on exports
  * no prohibition on the importation that the states think proper to admit
  * no duties by way of such prohibition
* The passage of navigation acts require the consent of two-thirds of both the
  house and senate.
* Additional powers of the legislature:
  * To make war, raise armies and fleets
  * Tribunals and punishments against offenses of the laws of nations
  * Piracy
  * Appoint inferior tribunals under the supreme judiciary
  * The exclusive right to coin money -- states may not print paper money or
    accept anything but specie as legal tender
  * Regulate naturalization
  * Call forth the aid of the militia to enforce treaties, repel invasion, and
    suppress rebellion
  * Establish post offices
  * Suppress state rebellions on behalf of the state legislatures
  * *Declaring the crime and punishment of counterfeiting*
  * Enact articles of war
  * Regulate the force permitted to be kept in each state
  * Borrow money and appoint a treasurer
  * Declare treason
* Only the house can pass acts concerning money bills
* Only the senate can:
  * pass acts making treaties of commerce and peace *and alliance*
  * appoint the judiciary
  * send ambassadors
* The executive is called the *Governor* by Rutledge
* The executive is the commander-in-chief
* *The executive shall give a state of the union speech from time to time.*
* The executive shall swear fidelity to the union *with an oath of office*
* On vacancy in the executive position, the president of the senate will
  succeed to the position
* The jurisdiction of the supreme court shall be appellate only (to hear
  appeals); the inferior tribunals will be the original tribunals.
* New states are admitted with the consent of two-thirds of the legislature

They end these notes with the following outline of an argument to the people
explaining the existence of the new government and furnishing the advocates of
the new system with talking-points:
1. State the general goals of a confederation.
2. Show with specifics how the confederation has fallen short of these goals.
3. The necessary powers follow as a consequence of the previous defects.
4. Can these powers be vested in Congress?  They cannot.
5. Refute the desire for partial confederations.
6. Therefore, a national government with taxation powers, etc. is the best way
   to achieve these goals.
7. A short summary of the main points of the new constitution.
8. Conclusion.

## V

Draft in Wilson's handwriting.

* refers to the first house of the legislature as the "House of
  Representatives"
* in order to vote for a representative, the elector must be 21 years of age,
  have resided in the US for one year prior to the day of election, and own 50
  acres of land
* ends with an outline for the layout of the articles of the constitution

## VI

More notes in Wilson's handwriting.

* a preamble that does not reference any of the reasons for a preamble that
  were listed in section IV above:

> We the People of the States of New Hampshire, Massachusetts, Rhode Island
> and Providence Plantation, Connecticut, New York, New Jersey, Pennsylvania,
> Delaware, Maryland, Virginia, North Carolina, South Carolina, and Georgia do
> ordain, declare, and establish the following constitution for the government
> of ourselves and of our posterity.(II, 152)

* the new nation's name is the "United People and States of America"
* each representative must have been in the US for three years prior to and be
  a resident of the state he was chosen for as of the day of the election
* each senator must be thirty years old, have lived in the US for four years
  prior, and be a resident of the state he was chosen for on the day of the
  election
* the times, places, and manners for electing congressmen are to be determined
  by the state legislatures but may afterwards be changed by the national
  legislature
* the legislature has the authority to set the property qualifications of its
  members
* "The members of each house shall receive a compensation for their services
  to be ascertained and paid by the state in which they shall be chosen".(II,
  156)
* Wilson writes the lone line "Freedom of Speech"(II, 156) -- not sure what he
  was referring to

## VII

Extracts (in Wilson's handwriting) from the Pinckney Plan and the New Jersey
Plan (of Paterson).

## VIII

Wilson's notes -- a continuation of section VI above.

* In addition to the veto of the executive, Wilson states a rule that any law
  not signed within ______ days of being received by the executive will
  automatically become law.
* It sounds like the senate may hear disputes between the states? (II, 160-2,
  first column)  The process:
  1. One party states their matter before the senate and applies for a
     hearing.
  2. The senate sets a day for both parties to appear before the senate.
  3. The parties shall jointly appoint judges to hear the matter in question.
  4. **If they cannot agree**, then the senate shall choose three judges from
     each state, and the parties will take turns striking out one at a time
     until there are only thirteen.  From the thirteen, seven to nine
     (inclusive) will be drawn by lot and at least five of them will hear the
     case.
  5. The judges will decide the case provided that a majority agree in the
     determination.
  6. **If a party is absent on the day to pick judges**, then the secretary or
     clerk of the senate will strike out names from the list chosen by the
     senate on the absent party's behalf.
  7. **If either party does not show up or defend their claim**, then the
     judges will determine the case any way.
  8. The judgment is final and conclusive.
  9. The proceedings will be a matter of public record.
  10. Each judge will be required to take an oath administered by a judge of
      the Supreme Court before hearing and determining the case.

## IX

Draft of the constitution submitted on August 6th.  It is in Wilson's
handwriting with modifications by Rutledge (in *italics*).

* name is the "United States of America"
* *the legislature is to meet on the first Monday of each December*
* the qualifications for the electors will be the same as those in the several
  states for the most numerous branch of their own legislatures
* house of representatives *shall have the sole power of* impeachment
* freedom of speech and debate in the legislature
* legislators shall not be arrested while in attendance and going and coming
  except in the cases of treason, felony, and breach of the peace
* "Governor" was the term used for the executive in the earliest draft, but
  this is crossed out and replaced with "*President*"
* with respect to bills passed by the legislature, the President has *seven*
  days to sign or reject it before it becomes law
* "The United States shall not grant any title of nobility."(II, 169) and "No
  state shall grant any title of nobility."(ditto)
* the title of the President of the United States is "His Excellency"
* the president has the authority to adjourn both houses if they cannot agree
  on a time of adjournment [this power is apparently based on the
  Massachusetts constitution (III, 312)]
* first presence of the presidential oath of office:

> I ______ solemnly swear, -- or affirm -- that I will faithfully execute the
> Office of President of the United States of America.

* the name of the judiciary was originally the "national court", but this was
  crossed out and changed to "supreme court"

# August 4, Saturday

McHenry writes:  "Returned to Philadelphia.  The committee of convention ready
to report.  Their report in the hands of Dunlop the printer to strike off
copies for the members."(II, 175)

# August 6, Monday

| State | New Delegates |
|-------|---------------|
| Maryland | John Francis Mercer |

Rutledge delivered the original draft of the constitution on behalf of the
committee of detail.  \[See the draft [here](https://www.usconstitution.net/draft_aug6.html).\]

[Note also the marginal notes on the surviving copies discussed at IV,
207-12.]

After an initial motion was voted down to adjourn until Wednesday, the
committee adjourned until tomorrow.

Davie writes to James Iredell that the remaining "work will rather be tedious
than difficult."(III, 68)

After the meeting, according to McHenry, the delegates from Maryland met to
go over the report and prepare themselves to act in unison tomorrow.  They met
where Carroll was staying.  Mercer asked if any of them thought Maryland would
approve of the constitution.  McHenry was uncertain.  Martin did not believe
they would.  He said that the present draft would not have gotten this far if
Jenifer had voted with him.  Jenifer defended himself that he *did* vote with
Martin until he saw that preventing its progress was in vain.(II, 190)

McHenry proposes a plan that they bring about a motion to postpone the report
and try to amend the AoC without altering the sovereignty of suffrage.  If the
motion fails, then they should try to render the present system to be as
perfect as they can manage.  They should spend the rest of their time together
assuming that the motion will fail and going over the draft to see what they
would like to change.  In this way they can appear unanimous in their
judgments before the convention.(II, 190)

Carroll could not agree to the motion because he did not think the AoC could
be amended to meet its designed goals.  McHenry believed it could, but
Mercer and Jenifer believed it could not.  Thus the motion was rejected.(II,
190-1)

Martin during the conversation observed that he was against two branches of
the legislature.  He was against the people electing the representatives.  He
wished some measure of sovereignty to be left with the state governments, and
he wanted to see a confederation that could act together in response to
national emergencies.  Martin would be absent from the convention on business
to New York until the next Monday.(II, 191)

McHenry suggested meeting before the convention tomorrow to hash out their
differences.  If they could not act unanimously going forward, there would be
no reason to remain -- wasting time and money without rendering any service.
All except Martin (due to his absence) agreed to meet.(II, 191)

McHenry notices that Mercer has a list of delegates to the convention with
check-marks beside some.  Asking what the list was, Mercer laughs and tells
him it is a list of delegates who are for a monarchy.  McHenry and Martin both
copy the list.(II, 191-2)  The next year Martin claims he has a list of at
least twenty delegates who wished to have a king.  Mercer when asked about it
doesn't remember the conversation but denies he would have ever said king or
monarchy.(III, 306 & 320)

McHenry prepared a list of questions for his colleagues so they could all get
on the same page(II, 191):

> Art. IV. Sec. 5.:  All bills for raising or appropriating money, and for
> fixing the salaries of the officers of Government, shall originate in the
> House of Representatives, and shall not be altered or amended by the Senate.
> No money shall be drawn from the Public Treasury, but in pursuance of
> appropriations that shall originate in the House of Representatives.

* "Will you use your best endeavors to obtain for the senate an equal
  authority over money bills with house of representatives?"

> Art. VII. Sec. 6.:  No navigation act shall be passed without the assent of
> two thirds of the members present in the each House.

* "Will you use your best endeavors to have it made a part of the system that
  'no navigation act shall be passed without the assent of two thirds of the
  representation from each state'?"

* "In case alterations cannot be obtained will you give your assent to [the
  above sections] as they stand in the report?"

* "Will you also (in case these alterations are not obtained) agree that the
  ratification of the conventions of nine states shall be sufficient for
  organizing the new constitutions?"

# August 7, Tuesday

Luther Martin is absent from the convention starting on this day.(II, 209)

McHenry showed his list of questions from the day before to Carroll, Jenifer,
and Mercer.  They all agreed with the sections as they were stated in the
draft.  McHenry, having doubts that they had given themselves enough time to
consider the implications of the propositions, suggested to Carroll that they
all meet again that evening to discuss the draft.(II, 209)

Pinckney moved that the draft be submitted to a committee of the whole.

Gorham and others opposed this as introducing unnecessary delay.  The motion
was voted down.

The preamble:

> We the people of the States of New Hampshire, Massachusetts, Rhode Island
> and Providence Plantations, Connecticut, New York, New Jersey, Pennsylvania,
> Delaware, Maryland, Virginia, North Carolina, South Carolina, and Georgia,
> do ordain, declare, and establish the following Constitution for the
> Government of Ourselves and our Posterity.

Article I:

> The style of the Government shall be, "The United States of America".

and Article II:

> The Government shall consist of supreme legislative, executive; and judicial
> powers.

were all agreed to without debate.

Article III was then considered:

> The legislative power shall be vested in a Congress, to consist of two
> separate and distinct bodies of men, a House of Representatives and a
> Senate; each of which shall in all cases have a negative on the other. The
> Legislature shall meet on the first Monday in December every year.

Mason did not think each branch having a negative on the other was proper "in
all cases" such as elections for appointments.

Gouverneur Morris moved to replace "all cases" with "legislative acts".
Williamson seconded the motion.

Sherman against.  It would restrain the operation of the clause too much
especially in the case of election ballots, which he approved of.

Gorham believed elections ought to be made by joint ballot.  Separate ballots,
if both houses had different favorites, would take too much time to settle.
An argument against is that joint ballots do not give the senate its due
weight, but the public's tranquility and welfare ought to prevail.

Wilson agreed with Gorham and wished to see a joint ballot particularly for
the election of the president.

Mason thought Morris's amendment extended too far.  Treaties would fall under
"legislative acts".  The senate has the sole right over those, but this would
give the house a veto.  He proposed that the clause be restrained to "cases
requiring the distinct assent" of both houses.

G. Morris thought treaties were not laws, and Mason's proposition was
redundant.

Madison, seconded by C. C. Pinckney, moved to strike out the whole clause
relating to the mutual veto power since the respective powers of both houses
were expressed in a subsequent article.

G. Morris's amendment was voted against, and Madison's was agreed to.

Madison asked the committee of detail why the meeting of the legislature had
been fixed.  He suggested that it only state that at least one meeting ought
to be made a year.

G. Morris moved to strike out the whole sentence.  The legislature ought not
to be tied down to a particular time or even be required to meet every single
year if the public business does not require it.

Pinckney supported Madison.

Gorham believed if the time was not fixed, then disputes would arise.  The
constitutions and charters of the New England states had long set a time to
meet, and this had produced no inconvenience.  The legislature should meet
once a year in order to check the executive.

Ellsworth was against striking out the clause.  The legislature will not know
if public business requires their meeting until they do meet.  There could be
nothing wrong in fixing the day as the current convention would be able to
rule on that matter as well as the legislature.

Wilson thought it best to fix the time.

King could not conceive of a necessary meeting every year.  "A great vice in
our system was that of legislating too much."(II, 198)  The states make most
of the laws, but the nation will create few -- namely related to commerce and
revenue.  Once these laws are decided, modifications will be rare and easily
made.

Madison thought fixing the time by law rather than by constitution was better.
It might happen that the legislature would draw together to make a ruling and
finish the session right before the appointed meeting time.  It would be
inconvenient to call them together so soon and without any necessity.  One
annual meeting should be required.

Mason agreed with Madison.  The extent of the country would make yearly
meetings necessary.  And even if not, the legislature, besides *legislative*
powers, has *inquisitorial* powers which should not be kept in suspension.(II,
199)  "They must meet frequently to inspect the conduct of the public
offices."(II, 206)

Sherman supported fixing the time and frequent meetings of the legislature.
The great extent of the country and varying state of affairs will supply
enough business.

Randolph was against fixing any day irrevocably, but since there were no other
provisions in the draft to set periods for meeting, he did not want to strike
it out altogether.  He moved to add "unless a different day shall be appointed
by law".  Madison seconded his motion.

The motion passed in the affirmative.

G. Morris moved to replace December with May.  Most European measures were
planned in the winter, and since our measures would be influenced by those, we
should wait until we have information on those in the spring.

Madison seconded the motion.  December would be the most inconvenient time to
travel to the seat of government.

Wilson agreed with Madison.

Ellsworth feared that a summer session would interfere too much in private
business, most of the legislators likely being involved with agriculture.

Randolph believed a December session would work better with the political
calendar of most of the states.

G. Morris's motion was voted down.

Read, seconded by G. Morris, moved to alter the article to give the executive
an absolute veto over the legislature.  "He considered this as so essential to
the constitution, to the preservation of liberty, and to the public welfare,
that his duty compelled him to make the motion."(II, 200)

On the motion, it was voted against.

Rutledge felt that as the article stood, it is not clear that the legislature
should meet annually.  At which point words to that effect were added without
debate.

The amended Article III read (II, 200):

> The legislative power shall be vested in Congress, to consist of two
> separate and distinct bodies of men, a House of Representatives and a
> Senate.  The Legislature shall meet at least once every year, and such
> meeting shall be on the first Monday in December unless a different day
> shall be appointed by law.

Article IV, Section 1 taken up:

> The members of the House of Representatives shall be chosen every second
> year, by the people of the several States comprehended within this Union.
> The qualifications of the electors shall be the same, from time to time, as
> those of the electors in the several States, of the most numerous branch of
> their own legislatures.

G. Morris, seconded by Fitzsimons, moved to strike out the last sentence
relating to qualifications of the electors in order that a clause might be
added restricting the right to freeholders.

Williamson opposed it.(II, 201 & IV, 208)

Wilson said this part of the report had been well-considered by the committee,
and he doubted whether a better condition could be devised.  It would be
difficult to devise a uniform qualification for all citizens of the several
states.  It would be disagreeable for a person to be able to vote for their
state representative but excluded from voting for their national
representative.

G. Morris stated that such hardship would not be a new innovation.  States
already have different qualifications for voting for governor and the
different legislative houses.  The clause also makes the qualifications of the
national legislature too dependent on the will of the states.

Ellsworth supported the clause as it stood.  The people will not support a new
constitution that will disenfranchise them.  The states are the best judge of
the qualifications of their own people.(II, 201)  If the national legislature
has the power to alter the qualifications, then they may disqualify three
quarters of the electorate.  This would go far towards aristocracy.(II, 207)

Mason pointed out that eight or nine states had extended suffrage beyond
freeholders.  What will the people say in those states when they are
disenfranchised?  A power to alter the qualifications of the electors is a
dangerous power to place in the hands of the legislature.

Butler agreed that the people are jealous of their suffrage.  In Holland
abridgments to the right have shored up an aristocracy in the senate.

Dickinson considered the freeholders to be the best guardians of liberty.
Restricting the vote to them is a necessary defense against the dangerous
influence of the multitudes without property or principle and who will abound
in our country in time.  The mass of our citizens are at this time freeholders
and will be pleased with the qualification.(II, 202)  Those who are not
freeholders are represented in the state legislatures who elect the
senators.(II, 207)  "No one could be considered as having an interest in the
government unless he possessed some of the soil.  The fear of an aristocracy
was a theoretical fiction.  The owners of the soil could have no interest
distinct from the country."(II, 209)

Ellsworth asked how the freehold will be defined.  Ought not any man who pays
a tax have a say in the representation who will levy that tax?  Should the
merchants and manufacturers not be allowed a voice?  Taxation and
representation ought to go together.(II, 202)  "[T]here is no justice in
supposing that virtue and talents are confined to freeholders".(II, 207)

G. Morris feared the aristocracy that might grow from the house of
representatives.  "I think I shall oppose this constitution because I think
this constitution establishes an aristocracy."(II, 207)  Give the vote to
people without property, and the rich will buy their votes.  In time the
country will abound with manufacturers and mechanics who will receive their
bread from their employers.  Will they be guardians of liberty?  Will they
prevent aristocracy?  You say that taxation should go with representation.  I
say that the man who does not give his vote freely is not represented.  The
ignorant and dependent cannot be trusted with the vote any more than children
-- they lack prudence, and they have no will of their own.  The qualification
will not be unpopular since 90% of the nation is at present freeholders.  With
respect to the merchants and manufacturers, if they have wealth and value the
right, they can acquire it.  If they do not, then they don't deserve it.

Mason suspected that this disagreement was the result of ancient prejudices:
The freehold is the qualification in Britain, and therefore, it must be the
only proper qualification.  Every man with a permanent common interest in
society ought to share in its rights and privileges.  "Was this qualification
restrained to freeholders?  Does no other kind of property but land evidence a
common interest in the proprietor?  Does nothing besides property mark a
permanent attachment?  Ought the merchant, the monied man, the parent of a
number of children whose fortunes are to be pursued in their own country, to
be viewed as suspicious characters and unworthy to be trusted with the common
rights of their fellow citizens?"(II, 203)

Madison felt that the right of suffrage -- being one of the fundamental
articles of republican government -- ought not to be left to the legislature.
Whether or not the qualification be a freehold depended to him on how easily
it could be accepted in states without the qualification.  Based on its own
merits, the freeholders of the country are the "safest depositories of
republican liberty."(II, 203)  In time the great majority of people will not
only lack land but any kind of property.  Either they will combine together in
influence and the rights of property and public liberty will not be safe in
their hands, or (which is more likely) they will become the tools of ambitious
men.  Mason misjudges the situation in England.  The qualifications of the
majority of electors in the cities and boroughs are as low as in any state,
and it is here, rather than in the counties, that bribery and the influence of
the crown is most dangerous.

McHenry does not speak but writes in his journal in response to Madison and
Morris, "The old ideas of taxation and representation were opposed to such
reasoning."(II, 210)

In the 1820s Madison writes a "clarification" of his speech on this day that
is more nuanced and supports universal suffrage.  However, both King's and
McHenry's notes suggest that Madison agreed more closely with Morris's
sentiments.  Madison's later notes likely reflect his changing sentiments on
the matter over the years rather than a clarification of his point on this
particular day.  A summary of his later statements follow:  The duty of
government is to protect both liberty and property.  The owners of wealth must
not be allowed to transgress the rights of the common people, nor should the
common people be allowed to take the property of the wealthy.  A country can
be said to belong to its freeholders.  They have a natural right to the soil
which they have worked and improved.  A number of schemes may be proposed to
balance the two interests against one another.  After examining each of them,
Madison finds it better to err on the side of universal suffrage, for it is
better that the freeholders -- who have an interest in property and liberty --
be deprived of half their share in government (property) than the common
people -- who have only an interest in their personal liberty -- be deprived
of their whole share.(III, 450-5)

Franklin thought they should not injure the lowest class of freemen -- they
were men of virtue and great integrity.(II, 208)  Americans captured by the
British in the war, due to their patriotism, refused to change sides when
given the option.  The elected have no right to narrow the privileges of the
electors.  Parliament set a minimum value on freeholds for electors.  Soon
afterwards they subjected the people without a vote to peculiar labors and
hardships.  It's not certain how many men would be considered freeholders --
the sons of a substantial farmer may not themselves be freeholders and would
not approve of being disenfranchised.  He figured a great many men would fall
into that camp.

Mercer stated that the constitution was objectionable on many points but none
more so on this one.  The people cannot know and judge the characters of the
candidates.  The worst choice will be made.  The people of the towns can unite
their votes behind a single candidate, but the people of the country being
dispersed will scatter their votes among a variety of candidates.

Rutledge did not think it wise to exclude the vote to freeholders.  It would
divide the people.

The motion of G. Morris was voted against.

The committee adjourned.

After the convention McHenry meets with Carroll.  They agree to oppose the
following sections of the draft(II, 210-2):
1. Article IV, Section 5 -- *that money bills should originate solely in the
   house*:  This gave too much power to the house and would surely end in the
   destruction of the legislature.  Without equal powers, the two houses were
   not equal checks on one another.
2. Article VII, Section 6 -- *no navigation act passing without two-thirds of
   both houses*:  This put the dearest interest of trade under the control of
   four states or of 17 members of the house and 8 members of the senate.
3. Article VII, Section 1 -- *lay and collect taxes and regulate interstate
   commerce*:  "We almost shuddered at the fate of the commerce of Maryland
   should we be unable to make any change in this extraordinary power."(II,
   211)  A military would need to be provided for, national courts maintained,
   and a "princely president to be provided for".  The revenue for this would
   be drawn from commerce.  Thereby Maryland's revenue would be taken away,
   but her expenses would not be lessened.(II, 212)
4. Article XXII -- *ratification by convention*:  That nine states only should
   be sufficient.

> We had taken an oath to support our constitution and frame of government.
> We had been empowered by a legislature legally constituted to revise the
> confederation and fit it for the *exigencies of government* and
> *preservation of the union*.  Could we do this business in a manner contrary
> to our constitution?  I feared we could not.  If we relinquish any of the
> rights or powers of our government to the U. S. of America, we could not
> otherwise agree to that relinquishment than in the mode our constitution
> prescribed for making changes or alterations in it.(II, 211-2)

Jenifer and Mercer said they would go along with McHenry and Carroll.  Jenifer
did not think the mischiefs of the national government would be as bad as they
made them sound.  Mercer did not approve of the new constitution, that it was
weak.  He wished for something better, but if no one else would support him,
then he would go with the flow.(II, 212)

# August 8, Wednesday

The committee continued the debate on Article IV, Section 1.

"Mercer expressed his dislike of the whole plan and his opinion that it never
could succeed."(II, 215)

Gorham had never seen an inconvenience in allowing non-freeholders to vote.
The mechanics and manufacturers of Philadelphia, New York, and Boston vote as
well as any freeholders.  Against Madison, the cities are not the seat of
crown influence and bribery in England; it is the boroughs.  It has nothing to
do with freeholders but with the smallness of the electorate.  The people will
not approve of their right to vote being abridged.

Mercer objected that the people freeholders or not were given no guidance in
their choices.  Candidates ought to be nominated by the state legislatures.

Article IV, Section 1 passed unanimously in the affirmative.

Article IV, Section 2 taken up:

> Every member of the House of Representatives shall be of the age of twenty
> five years at least; shall have been a citizen in the United States for at
> least three years before his election; and shall be, at the time of his
> election, a resident of the State in which he shall be chosen.

Mason supported a "wide door" for immigration but did not want to see
foreigners make our laws for us.  A foreign nation like Britain might bride
their way into our legislature.  He, seconded by G.  Morris, moved for "seven"
years instead of "three".

The motion passed in the affirmative.

Sherman moved to replace "resident" with "inhabitant".

Madison seconded the motion.  Both are vague, but at least "inhabitant" allows
for a person to be away for a time on private or public matters.  Virginia had
experienced great disputes over the meaning of the term "resident" for her
state representatives which often turned on how well or poorly the person was
liked.

Wilson also preferred "inhabitant".

G. Morris opposed both terms.  They should require nothing more than a
freehold.  In New York both terms had been determined by nothing more than the
arbitrary will of the majority.  Besides people rarely choose a non-resident.

Rutledge moved that a residence of seven years in the state should be
required.  An immigrant from New England to Georgia would need that long to
acquire a thorough knowledge of his new state.

Read objected that we were now forming a *national* government.  Such a
regulation does not correspond with us all being one people.

Wilson agreed with Read.

Madison pointed out that new states in the West might have no representation
in that case.

Mercer thought the rule would create a greater alienation among the states
than existed even under the AoC.  It would write state prejudices and
distinctions into the constitution that was meant to prevent them.  Maryland
had seen her share of disputes over the term "resident".

Ellsworth thought seven years was too long but supported some fixed time --
perhaps one or three years.

Dickinson proposed that it read "inhabitant actually resident for ______
years" to render the wording less vague.

Wilson worried that even if a short time were inserted in the blank, it might
be used to exclude some legislators who could not be said to be residents of
their state while at the seat of government.

Mercer stated it would exclude men who had once been inhabitants returning to
their original state.

Mason thought seven years was too long but agreed with the principle.  Rich
men of neighboring states might get into the legislature after having been
rejected in their own state.

The committee agreed unanimously to replace "resident" with "inhabitant".

Ellsworth and Mason moved to insert "one year" for prior inhabitance.

Williamson was against requiring an previous residence.  Elected
representatives will feel compelled to conform to the will of their
constituents.

Butler and Rutledge moved for "three years" instead of one.

Both motions for one and three years were voted down.

Article IV, Section 2 as amended was agreed to unanimously.

Article IV, Section 3 taken up:

> The House of Representatives shall, at its first formation, and until the
> number of citizens and inhabitants shall be taken in the manner herein after
> described, consist of sixty five Members, of whom three shall be chosen in
> New Hampshire, etc.

C. C. Pinckney and Pinckney moved that South Carolina be given "six"
representatives.

The motion was voted down, and the section was passed in the affirmative.

Article IV, Section 4 taken up:

> As the proportions of numbers in different States will alter from time to
> time; as some of the States may hereafter be divided; as others may be
> enlarged by addition of territory; as two or more States may be united; as
> new States will be erected within the limits of the United States, the
> Legislature shall, in each of these cases, regulate the number of
> representatives by the number of inhabitants, according to the provisions
> herein after made, at the rate of one for every forty thousand.

Williamson moved to replace "according to the provisions herein after made"
with "according to the rule hereafter to be provided for direct taxation"
(referencing Article VII, Section 3).

The motion passed in the affirmative.

King wished to know what influence the vote just passed was to have on the
part of the report concerning the admission of slaves into the rule of
representation.  He worried that the change would prevent objection to the
latter part.  He could not support slaves in the rule of representation, and a
great many people in the union would not.  The two great objects of the
general government are defense against foreign invasion and defense against
internal sedition.  Should some states be allowed to introduce a weakness that
will make that defense more difficult?  The hands of the legislature are tied
on two counts:  They cannot halt the import of slaves, and they cannot tax
exports.  Shall one part of the union be bound to defend the other and that
other not only increase its danger but also withhold the compensation for the
additional burden?  The exports produced by the slaves should be taxed to
provide a better defense for their masters.  Some limit must be set to the
importation of slaves.  They cannot allow unlimited importation *and* allow
them to be counted in representation.  The northern states would never agree
to it.

Sherman viewed the slave trade as wicked, but the point of representation had
been settled after so much difficulty and deliberation that he could not
oppose it now.  Besides, he did not think the present amendment prevented
opposition to the latter part of the report.

Madison objected to the 1 for every 40,000 ratio being permanent.  The future
growth of the nation would make the number of representatives excessive.

Gorham did not think it likely that the government they were framing would
last that long.  "Can it be supposed that this vast a country including the
western territory will 150 years hence remain one nation?"(II, 221)

Ellsworth pointed out that if the government did indeed last that long, the
constitution can be amended.

Sherman and Madison moved to add "not exceeding" before the ratio.  It was
agreed to without debate.

G. Morris moved to insert the word "free" before "inhabitants".  Slavery is a
"nefarious institution".  It spreads misery and poverty in all the states
where it is legal.  If the slaves are men, then make them citizens.  If they
are property, then why are the houses of Philadelphia not counted which are
worth more than all the slaves of South Carolina?  "The admission of slaves
into the representation when fairly explained comes to this:  that the
inhabitants of Georgia and South Carolina who goes to the coast of Africa, and
in defiance of the most sacred laws of humanity tears away his fellow
creatures from their dearest connections and damns them to the most cruel
bondage, shall have more votes in a government instituted for the protection
of the rights of mankind, than the citizen of Pennsylvania or New Jersey who
views with a laudable horror, so nefarious a practice."(II, 222)  The north
will be forced to defend the slave masters from their slaves.  The imports in
the north will be taxed more heavily than the slaves imported into the south.
Not only is the legislature prevented from halting the importation of slaves,
the southern states are practically encouraged to do it to increase their
representation.  "He would sooner submit himself to a tax for paying for all
the Negroes in the United States than saddle posterity with such a
constitution."(II, 223)

Dayton seconded the motion.

Sherman doubted the objections brought up regarding the admission of slaves
into the representation.  By his estimation the free men of the southern
states were being represented based on their taxes, and the slaves are
included in the estimate of those taxes.

Wilson thought this argument was premature.  The current amendment to the
clause would not bar objection to the latter section.

The motion to insert "free inhabitants" was voted against.

Dickinson moved to insert "provided that each state shall have one
representative at least" which was agreed to without debate.(II, 223)
Dickinson revised this section of the report in his copy to read, "according
to the rule hereinafter for direct taxation not exceeding the rate of one for
every forty thousand, provided that every state have at least one
representative and that the representation of the most populous state shall
not exceed that of the least populous more than on the proportion of twenty to
one."(IV, 209)

Article IV, Section 4 was agreed to as amended.

Article IV, Section 5 was taken up:

> All bills for raising or appropriating money, and for fixing the salaries of
> the officers of Government, shall originate in the House of Representatives,
> and shall not be altered or amended by the Senate. No money shall be drawn
> from the Public Treasury, but in pursuance of appropriations that shall
> originate in the House of Representatives.

Pinckney moved to strike the section out entirely.  It was no advantage to the
house, and if the senate can be trusted with its other powers, it surely can
be trusted with money bills.

Gorham was against allowing the senate to *originate* but supported allowing
them to *amend*.

G. Morris thought it proper that the senate should originate money bills.
They sit continually, consist of fewer congressmen, and will be better able to
prepare them.

Mason approved of the section.  It was part of the compromise for making the
terms of the senators so long (which he supported).  Giving this power to the
senate would support aristocracy -- the power of the few over the many.  An
aristocratic body should be ever suspected of an encroaching tendency.  They
should never have a hold of the purse strings.

Mercer considered the power to originate money bills as so advantageous that
it rendered the equality of votes in the senate of no consequence. [???]

Butler supported the section as stated.

Wilson was opposed to it regardless of the compromise.

Ellsworth did not particularly care, but since some from the larger states
thought it important, he was willing to let it stand.

Madison wanted to strike it out.  It fettered the government and would create
altercations between the two houses.

On the question for striking out the section, it passed in the affirmative.

The committee adjourned.

# August 9, Thursday

Randolph disapproved of the expunging of the section on money bills from the
draft, saying it endangered the success of the plan.  He gave notice that he
intended to reconsider the vote.

Williamson agreed.

Wilson gave notice that he intended to reconsider the qualification of seven
years of citizenship instead of three for members of the house of
representatives.

Article IV, Section 6:

> The House of Representatives shall have the sole power of impeachment. It
> shall choose its Speaker and other officers.

and Section 7:

> Vacancies in the House of Representatives shall be supplied by writs of
> election from the executive authority of the State in the representation
> from which it shall happen.

agreed to without debate.

Article V, Section 1 taken up:

> The Senate of the United States shall be chosen by the Legislatures of the
> several States. Each Legislature shall choose two members. Vacancies may be
> supplied by the Executive [of the state] until the next meeting of the
> Legislature. Each member shall have one vote.

Wilson objected to vacancies being supplied by the state governors.  It
removes the appointment too far from the people.  Many governors are already
appointed by the state legislature.  He had always felt this was wrong and
more so when that same executive appoints members to the national legislature.

Randolph thought it was necessary to prevent too long of absences in the
senate.  Some state legislatures meet but once a year.  Since the senate has
the fewer members, absences will be of greater consequence.  The governors can
be trusted with appointments of so short a time.

Ellsworth noted that it says the governor *may* supply the vacancy.  The power
will not be used when the state legislature is in session.  Since there are
only two members for each state, vacancies will be important to fill.

Williamson believed the provision was necessary since senators may resign or
not accept.

On the question of striking out the clause, it was voted against.

Williamson moved to insert after the third clause "unless other provision
shall be made by the legislature".

Ellsworth was willing to trust the legislature *or* the executive, but he
did not want to give the former the discretion to refer appointments to
whomever they pleased.

The motion was voted against.

Madison wanted to make the clause more clear -- particularly on the point that
senators could refuse a position -- and moved that the third clause be
replaced with "Vacancies happening by refusals to accept, resignations, or
otherwise may be supplied by the legislature of the state in the
representation of which such vacancies shall happen, or by the executive
thereof until the next meeting of the legislature."(II, 232)

G. Morris agreed with Madison with respect to senators refusing.  If senators
are barred from holding other offices, the legislature could appoint a man
against his consent to the senate and deprive the US government of his
services.

The motion was agreed to unanimously.

Randolph moved to vote on the section independently of the last clause.  He
wished the vote on the last clause to take place after reconsideration of the
money bills section.  If that latter section was not reinstated, then he
intended to vary the representation in the senate.

Strong agreed with Randolph.

Read had not considered that money bills were any advantage to the larger
states.  If they valued them and it were a condition of the equality of votes
in the senate, he would not object to reinstating it.

Wilson urged that it was no advantage of the larger states, it would be a
source of contention between the houses, and all principal powers of the
legislature had some relation to money.

Franklin considered the two clause to be connected ever since the compromise
was agreed to.

Mason did not think this was the time to discuss this point.  It was clear to
him that money bills should be restrained to the representatives who were the
immediate choice of the people.

Williamson stated that his state, North Carolina, had agreed to an equality in
the senate assuming that money bills would be confined to the house.  He was
surprised to see the smaller states going back on the deal they had made.

On the acceptance of the amended Article V, Section 1 (excluding the last
clause), it passed in the affirmative.

Randolph moved that the last clause be postponed.

Someone observed [but who?] that this was not necessary since even if the
money bills were reinstated, it would still be proper for the members of the
senate to have one vote a piece.  A postponement of the second clause granting
two members per state would have been more proper.

Mason saw no impropriety in postponing the last clause.  Each state may have
two members, but each member may have different weighted votes.  If the money
bills were not restored, he would out of duty oppose equal suffrage in the
senate.

G. Morris supposed these threats were addressed to the smaller states in order
to induce them, against their better judgments, into restoring the money bills
section.  He announced that he considered the section on money bills
intrinsically bad and would continue to support equal suffrage in the senate.

Wilson pointed out that while some supposed that money bills were advantageous
for the larger states, two of the larger states -- Pennsylvania and Virginia
-- voted against them.

Randolph agreed with Mason that the postponement was not improper because the
two sections were connected.

The postponement of the last clause was voted against.  Then, the last clause
was passed in the affirmative.

Randolph then gave notice that he would reconsider Article V, Section 1 along
with Article IV, Section 5.

Article V, Section 2 was taken up:

> The Senators shall be chosen for six years; but immediately after the first
> election they shall be divided, by lot, into three classes, as nearly as may
> be, numbered one, two and three. The seats of the members of the first class
> shall be vacated at the expiration of the second year, of the second class
> at the expiration of the fourth year, of the third class at the expiration
> of the sixth year, so that a third part of the members may be chosen every
> second year.

G. Morris moved to insert the words "they shall be assembled in consequence
of" after "immediately after".  The insertion and the whole of the section was
agreed to without debate.

Article V, Section 3 was taken up:

> Every member of the Senate shall be of the age of thirty years at least;
> shall have been a citizen in the United States for at least four years
> before his election; and shall be, at the time of his election, a resident
> of the State for which he shall be chosen.

G. Morris, seconded by Pinckney, moved to replace four with fourteen.  We must
not allow strangers into our public councils.

Ellsworth opposed the motion for discouraging "meritorious aliens" from
immigrating to this country.

Pinckney believed that since the senate had the power of foreign treaties, it
was imperative that none had foreign attachments.  The Athenians made it a
death sentence for any stranger to intrude into their legislative proceedings.

Mason approved approved of the motion.  He would have gone further and limited
the senate to *only* natives had not so many foreign-born distinguished
themselves in the war for independence.

Madison saw the benefit of some restrictions on the office but thought any
restriction *in the constitution* would be improper.  First, the national
legislature has the right of naturalization and by that virtue may fix
different periods of residence as conditions for different privileges of
citizenship.  Second, it would put it out of the power of the legislature to
confer the full rank of citizenship to meritorious foreigners and prevent the
most desirable foreigners from immigrating to the US.  It would be unlikely
that the state legislatures would appoint any significant number of foreigners
to the senate or that foreign governments would seek to influence those
senators who would be most closely watched.

Butler opposed the appointment of senators without a long residence in the US.
We should not only worry about foreign attachments but also ideas of
government so distinct from ours that they would be dangerous.  He mentioned
himself as an example of someone who would have been an improper public
servant without having lived in the US for so long.  [He was Irish from County
Carlow.]

Franklin was not against a reasonable time period but did not want to see
"illiberality inserted in the constitution."(II, 236)  When immigrants look
about and choose our country in which to obtain more happiness, it is proof of
an attachment which ought to excite our confidence.

Randolph disapproved of fourteen years.  He could go to seven but no further.
Think of the language of the patriots during the revolution and the principles
laid down in our state constitutions.  Foreigners may have fixed their
fortunes in this country with faith in those invitations, and they will be
hostile to a system with such a proposal.

Wilson, himself an immigrant from Scotland, wondered aloud at the irony of
being denied a position under the constitution which he had helped frame.
[Possibly and exaggeration.  He had been a resident in America since the 1760s
unless he's counting his citizenship as starting with the ratification of the
AoC in 1781.]  He reiterated all the prior arguments against the motion.  "To
be appointed to a place may be a matter of indifference.  To be incapable of
being appointed, is a circumstance grating and mortifying."(II, 237)

G. Morris argued "[t]hat we should not be polite at the expense of
prudence."(II, 237-8)  Some Indian tribes offer guests their wives and
daughters.  Is this a proper model for us?  I will invite them to my table,
offer them food, give them lodgings, but I would not go so far as to offer
them to bed my wife.  "It is said that we threw open our doors -- invited the
oppressed of all countries to come and find an asylum in America.  This is
true we invited them to come and worship in our temple, but we never invited
them to become priests at our alter."(II, 242)  Foreigners will enjoy many
privileges as citizens of the US, but they should not hold the highest offices
of government.  "The men who can shake off their attachments to their own
country can never love any other."(II, 238)  Appoint a foreigner to the
senate, and he will seek to increase the commerce of his mother country.(II,
238)  If seven years of study must be applied to become a shoe-maker, at least
fourteen are necessary to learn to be an American legislator.(II, 243)

The motion to substitute fourteen was voted against.

G. Morris then moved for thirteen years -- also voted against.

C. C. Pinckney moved for 10 years -- also voted against.

Franklin reminded the convention that omitting the restriction in the
constitution would not require such people to be chosen for the legislature.

Rutledge figured that since seven years were decided as a restriction for the
house, then a longer restriction should be chosen for the senate, being more
powerful.

Williamson thought it more necessary to guard the senate then the house since
bribery and cabal are more likely in the state legislatures than the people at
large.

Randolph would agree to nine years -- with the expectation that it would be
reduced to seven if Wilson's reconsideration of Article IV, Section 2 should
reduce the restriction on the representatives.

On the question of nine years, it passed in the affirmative.

The term "resident" was replaced with "inhabitant" without debate.

Article V, Section 3 as amended was agreed to unanimously.

Article V, Section 4:

> The Senate shall choose its own President and other officers.

was agreed to without debate.

Article VI, Section 1 taken up:

> The times and places and manner of holding the elections of the members of
> each House shall be prescribed by the Legislature of each State; but their
> provisions concerning them may, at any time be altered by the Legislature of
> the United States.

Madison and G. Morris moved to replace "each house" with "the house of
representatives".

A division of the question was called, and the first clause of the section was
agreed to unanimously.

Pinckney and Rutledge moved to strike out the second clause.  The state
legislatures must be relied on to have jurisdiction in this case.

Gorham thought it would be improper to take this power form the national
legislature -- like the British parliament leaving the circumstances of
elections to the counties.

Madison believed the state legislatures may frustrate the general interest
with their local prejudices.  In many states the legislatures have, by
manipulating the times, places, and manners of elections, concentrated power
in a few regions.  What is to stop them from doing the same in the general
government?  Since the representatives represent the people, the means of
election should be relatively equivalent in each of the states, and the
national legislature should ensure this.  There is no danger to the power
because the national legislature derives their representation ultimately from
the same source as the state legislatures -- the people.  If we have
confidence in the latter, we must have confidence in the former.  [See also
his argument for this section in the Virginia convention -- III, 311-2.]

King believed the national legislature should have the power.  The appointment
of the general government by the state legislatures was fatal to the federal
system, but some men here still think it might work this time.  [See also his
argument for this section in the Massachusetts convention -- III, 267-8.]

G. Morris "observed that the states might make false returns and then make no
provisions for new elections".(II, 241)

Sherman thought the clause should be retained even thought he himself had
confidence in the state legislatures.

McHenry, later in the Maryland convention, justified the clause that it was
meant to thwart the efforts of any state to undermine the national government.
It is a means of securing the liberty of the people against encroachments by
their (state) government.(III, 148)

Davie, later in the North Carolina convention, justified the clause that it
was to prevent a dissolution of the government by designing states.  In Rhode
Island a faction seized control of the state government and refused
representation in Congress.  With the power in this section, no state can
withdraw the representatives of the people from the national assemblies.(III,
344-5)

The motion was voted down.

The word "respectively" was inserted after "State".

Read moved to reword the last clause to read, "but regulations in each of the
foregoing cases may at any time be made or altered by the Legislature of the
United States."(II, 242)  This would give the legislature power not only to
alter the provisions of the states but to make regulations in case the states
failed or refused altogether.

Article VI, Section 1 was agreed to as amended unanimously.

The committee adjourned.

This evening around 9pm Elbridge Gerry returns to Philadelphia with Alexander
Hamilton.(IV, 215)

# August 10, Friday

Article IV, Section 2 taken up:

> The Legislature of the United States shall have authority to establish such
> uniform qualifications of the members of each House, with regard to
> property, as to the said Legislature shall seem expedient.

Pinckney criticized the committee for failing to come up with qualifications
of property on their own and instead leaving it in the hands of the national
legislature.  This meant that there would be no qualifications on the initial
class.  If they happened to be quite wealthy, they may set the bar too high.
If poor, then too low.  As much as he was against an undue aristocratic
influence in the government, he believed the government officers must have
enough property to make them respectable and independent.  Property must be
tied to reputation to secure a faithful administration.  He moved that the
President, Judges, and Legislators swear that they possessed an unencumbered
estate of ______.  He would leave the motion blank, but his personal
preference was for not less than $100,000 for the President and half that
amount for the Judges and Legislators.

Rutledge seconded the motion and admitted that the lack of qualifications was
because the committee could not decide on an amount.  Too high, and the people
would reject it.  Too low, and the qualification would be worthless.

Ellsworth was against fixed qualifications.  Time and circumstances will
change too often.  The southern statesmen were on average more wealthy than
the north, and no uniform qualification could suit both.  For these reasons it
is better to leave it to the discretion of the legislature than to fix it in
the constitution.

Franklin disliked the whole qualification.  If poverty exposes one to
temptation, it is no less true that possession of property increases the
desire for more property.  The greatest rogues he had ever met were also the
riches rogues.  The scriptures say that rulers should hate covetousness.  The
constitution will be read abroad.  If it is partial to the rich, it will lose
esteem among the liberal in Europe and discourage the common people from
emigrating.

"The motion of Mr. Pinckney was rejected by so general a *no*, that the states
were not called."(II, 249)

Madison opposed the section, thinking it a dangerous power for the
legislature -- as dangerous as allowing them to fix their own salaries or
privileges.  The qualifications of the electors and elected were so
fundamental in republican governments that they needed to be fixed in the
constitution.  A republic could be converted into an aristocracy or an
oligarchy.  A stronger faction may use it to set qualifications based on
arbitrary distinctions to keep out the influence of a weaker faction.

Ellsworth did not view the power as dangerous.  Such a power in the electors
would be dangerous and more likely to be abused.

G. Morris moved to strike out "with regard to property" to generalize the
possible qualifications.

Williamson thought Morris's motion was foolish.  The legislature could set any
arbitrary qualification, e.g. all legislatures must be lawyers.

Madison pointed out that the British Parliament had the power to set
qualifications for electors and elected, and its abuse was widely recognized.

To strike out "with regard to property" was voted against.

Rutledge proposed that the qualifications should be the same as for the state
legislatures.

Wilson thought it best to remove the section.  A uniform rule is improbable,
and a restriction to property would limit regulating any other qualification.

On the question to agree to the section as is, it was voted against.

(Next Monday was set for reconsidering Article IV, Section 2 to restore three
years as a qualification for representatives.)

Article VI, Section 3 taken up:

> In each House a majority of the members shall constitute a quorum to do
> business; but a smaller number may adjourn from day to day.

Gorham moved to set the number of a quorum to less than the majority.
Otherwise, they could experience inconvenience and delay waiting for enough
members to attend.

Mercer agreed.  A quorum too large allows some members to leave and hold the
proceedings hostage as had already happened in some states.  Parliament sets a
small quorum, and no inconvenience has been experienced from it.

Mason maintained that a country so extensive could not allow so small a
quorum.  The states closest to the seat of government could always push their
own agenda through.  If the legislature had the power to reduce the number,
they might reduce it as low as they pleased.  Mercer was right in saying that
seceding members could abuse the rule, but he had also seen it used
beneficially in Virginia.  Indeed, he felt that a simple majority was not
enough.

King felt the inconvenience of a large quorum outweighed the advantage of the
central states.

G. Morris, seconded by Mercer, moved to fix it to a (current) simple majority:
33 members in the house and 14 in the senate.

King preferred to set the simple majority as the minimum number for a quorum,
allowing the legislature to set a higher number if necessary.  He thought the
future increase of members would render a majority quorum cumbersome.

Mercer seconded King's motion in place of Morris's.

Ellsworth opposed it.  The people will not be pleased to know only a few men
could impose a burdensome law on them.  He reminded the convention that the
number or representatives would be so limited to make too many absences
unlikely.  A better rule would be to give each house the authority to require
the attendance of absent members.

Wilson agreed with Ellsworth.

Gerry observed that only 17 of 33 in the house or 8 of 14 in the senate would
carry a vote which could be done by two large states (plus two small states in
the senate).  He proposed that a quorum in the house should be between 33 and
50 leaving the intermediate number to the discretion of the legislature.

King thought that the legislature could be trusted with a smaller quorum since
it would require a full two-thirds of both houses to overcome the presidential
veto.

Carroll believed the motion would not be a security when the legislature
expands and the quorums must be increased.

On King's motion, it was voted against.

Randolph and Madison moved to append the following to the end of the section
in question:  "and may be authorized to compel the attendance of absent
members in such manner and under such penalties as each house may
provide."(II, 254)

The motion was passed in the affirmative, and the section was accepted as
amended unanimously.

Article VI, Section 4:

> Each House shall be the judge of the elections, returns and qualifications
> of its own members.

and Article VI, Section 5:

> Freedom of speech and debate in the Legislature shall not be impeached or
> questioned in any Court or place out of the Legislature; and the members of
> each House shall, in all cases, except treason, felony, and breach of the
> peace, be privileged from arrest during their attendance at Congress, and in
> going to and returning from it.

were agreed to without debate.

Article VI, Section 6 taken up:

> Each House may determine the rules of its proceedings; may punish its
> members for disorderly behavior; and may expel a member.

Madison believed that expulsion was too important a power to be exercised by a
bare majority of a quorum.  He moved to insert "with the concurrence of
two-thirds" between "may" and "expel".

Randolph and Mason approved.

G. Morris thought a majority of a quorum could be trusted.  He worried about
the opposite extreme:  that a few men may keep a member in who ought to be
expelled.

Carroll agreed with Madison.

On the motion for two-thirds to expel a member, it passed in the affirmative.

Article VI, Section 6 was accepted as amended.

Article VI, Section 7 taken up:

> The House of Representatives, and the Senate, when it shall be acting in a
> legislative capacity, shall keep a journal of their proceedings, and shall,
> from time to time, publish them: and the yeas and nays of the members of
> each House, on any question, shall at the desire of one-fifth part of the
> members present, be entered on the journal.

G. Morris, seconded by Randolph, moved that any individual ought to be
authorized to call for them.  The small states might find it difficult to get
a concurrence of one-fifth.

Sherman preferred to do away with the section altogether.  The list of yeas
and nays do more harm than good when they do not record the voter's reasons.

Ellsworth agreed with Morris.

Mason liked the section as it stood, being the middle ground between two
extremes.

Gorham agreed with Sherman's argument and had seen the abuses in
Massachusetts.

The motion to grant a single member authority to call a roll was voted
against unanimously.

Carroll and Randolph moved to replace "each house" with "house of
representatives" and append "and any member of the senate shall be at liberty
to enter his dissent."

G. Morris and Wilson observed that the house would have the right to complain
if it were not extended to them, and if that was done, the journals would
become filled with rejoinders and replications.

On Carroll's motion, it was voted against.

Gerry moved to strike out "when it shall be acting in its legislative
capacity" (in order to extend the right to the senate when executing their
other powers) and insert "except such parts thereof as in their judgment
require secrecy" after the words "publish them".

Madison writes in his journal that others had planned to add these same
provisions to the section and vesting the additional authorities in the
senate.

On the motion to strike out the words, it passed in the affirmative.

The committee adjourned.

# August 11, Saturday

Article VI, Section was taken back up.

Madison and Rutledge moved to reword the section to read:

> Each house shall keep a journal of its proceedings and shall from time to
> time publish the same; except such part of the proceedings of the senate
> when acting not in its legislative capacity as may be judged by the house to
> require secrecy.(II, 257)

Mercer believed the motion implied that the senate would be given powers other
than legislative and hoped this would not be the case.

The motion was voted down.

Gerry and Sherman moved to insert "except such as relate to treaties and
military operations" after "publish them".  They wished to give each house a
discretion in such cases.

Their motion was voted against.

Ellsworth believed the whole clause should be stuck out since it seems to be
so objectionable.  The legislature will not fail to publish from time to time
because the people will hold them accountable for it.

Wilson did not agree.  Removing the clause would be improper.  The legislature
should not have the option of hiding their proceedings from their
constituents.  Besides a similar clause exists in the AoC, and it would raise
the suspicions of adversaries of the constitution if it were left out.
[Article IX of the AoC requires the journals of Congress to be published
*monthly*.]

Mason felt the people would be rightly alarmed if the proceedings of their
legislature were secret.

Sherman believed the legislature can be trusted in this case.

On the vote of the first clause of the section, it passed unanimously in the
affirmative.

On the words "except such parts thereof as may in their judgment require
secrecy", it passed in the affirmative.

On the last clause of the section, it passed unanimously in the affirmative.

In the Virginia ratifying convention, Mason and Madison argued over this
section, specifically over the phrase "from time to time".  Mason said that
the phrasing was chosen so matters relating to the military or foreign
negotiations might be kept secret, but he worried that the ambiguity of the
phrase could be abused to keep matters of public expenditures from the people
indefinitely.  Madison argued that the chosen phrasing was better than any
fixed period.  If the accounts of public receipts and expenditures were
published at short, set periods, the full context of the expenses could not be
apprehended.  But by allowing for Congress to publish at the most convenient
times, the records would be more full and satisfactory to the public.  Mason
pointed out that the AoC required monthly publishing which was infinitely
better than depending on men's virtue to publish or not.  Madison pointed out
that that rule had led to inconveniences in the confederation which led him to
choose this phrasing.  "[I]t was impossible, in such short intervals, to
adjust the public accounts in any satisfactory manner."(III, 326-7)

In the North Carolina ratifying convention, Davie stated that it was the sense
of the convention that journals would be published at the rising of Congress
every year and at the end of each session if they met more often.(III, 345)

Article VI, Section 8 taken up:

> Neither House, without the consent of the other, shall adjourn for more than
> three days, nor to any other place than that at which the two Houses are
> sitting. But this regulation shall not extend to the Senate, when it shall
> exercise the powers mentioned in the ______ article.

King noted that the section allowed the legislature to adjourn to new places
which was inconvenient.  The mutability of the seat of government was a
problem in the confederation.  He thought, at the least, that it should
require a law to be passed to move the seat of government.

Madison agreed with King.

G. Morris proposed inserting "during the session".

Spaight worried that this would fix the seat of government in New York -- the
present seat of Congress.  If the president is a northern man, they will never
be able to remove them.

G. Morris felt that this distrust was unfounded.

Madison felt a central residence for the government was much more essential to
the new government than the old because the new one would require more people
from more distant reaches of the country.  These considerations would force a
central residence even if a low were required.  But in order to quiet
suspicions, Madison moved to reword the section in the following manner:

> The Legislature shall at their first assembling determine on a place at
> which their future sessions shall be held.  Neither house shall afterwards,
> during the session of the house of representatives, without the consent of
> the other, adjourn for more than three days, nor shall they adjourn to any
> other place than such as shall have been fixed by law.

Gerry thought it would be wrong to allow the president to veto the will of the
legislature on this subject at all.

Williamson agreed with Spaight.

Carroll agreed with Gerry.

Mercer thought Madison's motion would serve no purpose because the two houses
would never agree on the point.

The motion of Madison was voted against.  Eventually the last sentence of the
section was struck out, and the words "During the session of the legislature"
were prefixed onto the section.  The section was then agreed to as amended.

In the Virginia ratifying convention, Monroe worried that the section made the
house of representatives too dependent on the senate since the senate would
have to agree to their adjournment.  Madison stated that it would be very
exceptional to allow either house to adjourn without the consent of the other
house without any regard for public exigencies.  On the possibility that the
houses do not agree on adjournment, the president has the power [Article II,
Section 3 of the US Constitution] to adjourn both houses until he thinks
proper.(III, 312)

Randolph then moved to reconsider Article IV, Section 5 (regarding money
bills).  Since an equal suffrage had been fixed in the senate, the larger
states required this compensation in the house.  This section will make the
plan more acceptable to the people because they will expect safe-guards
against the power of the aristocratic body in the senate.  He reminded the
smaller states that this was a condition of the compromise whereby they
acquired an equal vote in the senate.  He would move to reinstate the section
with an additional clause specifying that the bills in question should be for
the purposes of revenue in order to prevent an objection to the ambiguity of
the phrase "raising money" and that the senate should not alter or amend so as
to increase or diminish the sum.

Williamson seconded the motion.

Pinckney felt the reconsideration was a waste of time.  He felt the section
had not been a part of the compromise.  The compromise in his mind had been
that the house of representatives would have proportional representation.

The motion to reconsider was approved and Monday was assigned.

The committee adjourned.

# August 13, Monday

Article IV, Section 2 reconsidered [as so far amended]:

> Every member of the House of Representatives shall be of the age of
> twenty-five years at least; shall have been a citizen of the United States
> for at least seven years before his election; and shall be, at the time of
> his election an inhabitant of the state in which he shall be chosen.(II,
> 267)

Wilson, seconded by Randolph, moved to replace "seven" with "four".  It would
be proper for the electors to prefer a longer citizenship but unnecessary to
chain them to it.

Gerry wished that in the future the offices be limited to native born
Americans.  We will be the targets of spies and foreign influence, and many of
the most influential men in Massachusetts share these concerns.

Williamson preferred "nine" instead of "seven".  Wealthy immigrants do more
harm than good by their "luxurious examples".(II, 268)

Hamilton was in general against minute restrictions.  He moved that the
section require *only* citizenship and inhabitance.  The qualification of
naturalization can be left to the discretion of the legislature.

Madison seconded the motion.  We should encourage immigrants of merit and
republican principles to come.  America is indebted to immigrants for her
settlement and prosperity.  There is a possible danger of foreign influence,
but it is unlikely to happen to any dangerous degree.  Our experience has been
that the people will in general prefer natives of this country.  Bribery and
other illegal means of influence are more like to be practiced by natives than
foreigners who will be under extra scrutiny.

Wilson cited the example of Pennsylvania:  She was the youngest settlement
(besides perhaps Georgia), but her population and prosperity was a testament
to their encouragements to immigration.  Almost all the general officers in
the Pennsylvania army had been foreigners, and three of her delegates to this
convention -- R. Morris, Fitzsimons, and Wilson -- were foreign born.  He
withdrew his motion to support Hamilton's.

Pinckney in the house of representatives in 1820 details the example of
Pennsylvania:

> The wise conduct of William Penn, and the unexampled growth of Pennsylvania
> were cited ... that all foreigners of industrious habits should be welcome,
> and none more so than men of science, and such as may bring to us arts we
> are unacquainted with, or the means of perfecting those in which we are not
> yet sufficiently skilled -- capitalists whose wealth may add to our commerce
> or domestic improvements; let the door be ever and most affectionately open
> to illustrious exiles and sufferers in the cause of liberty; in short, open
> it liberally to science, to merit, and talents, wherever found, and receive
> and make them your own.(III, 444)

Butler was against allowing foreigners into the public councils.

On Hamilton's motion, it was voted against -- as was Williamson's motion for
nine years and Wilson's renewed motion for four.

G. Morris moved to add a provision that the limitation of seven years should
not affect anyone now a citizen.

Mercer seconded the motion.  It was necessary not to disenfranchise those who
had become citizens under the present laws from being in all respects
equivalent to natives.

Rutledge pointed out that every qualification might as well be a
disenfranchisement.  The minimum age of twenty-five years is a
disenfranchisement.  The policy respecting the years of citizenship is as
important for citizens now as it will be for those naturalized in the future.

Sherman stated that the US has not invited foreigners to the US that they
should enjoy equal privilege with native citizens.  This has been the power of
the individual states alone.  Therefore, the US under the new law is at
liberty to make any restrictions they deem necessary.

Gorham believed that currently foreigners are equivalent to natives when they
are naturalized and, therefore, did not wish to make the restriction
retrospective.

Madison criticized Sherman's argument.  By his reasoning, if our treaties or
public debts become inconvenient, we would need only to fashion a new
constitution.  Assuming that this is a principle, as he said, that the states
alone are bound to, didn't the states appoint this convention?  And will not
the makers of the new law be the violators?  This principle would lend fuel to
our critics.

G. Morris disagreed with Rutledge's assessment.  No one had ever pledged to
those under the age of 25 that they would have the same privileges as those
over.  The same could not be said about naturalized foreigners and native
citizens.

Pinckney asserted that due to the varieties of naturalization laws in the
different states, the US could not be bound to respect them all.

Mason agreed with Sherman's point.  If agents of Great Britain should work
themselves into our councils, they might make, for example, commercial
regulations that would prove to be pernicious to US interests.

Wilson read from the Pennsylvania constitution which granted all the rights of
citizenship to any foreigner who resided in the state for two years.  Then, he
read the AoC which made a citizen of one state the citizen of all.
Pennsylvania had an obligation to maintain the faith pledged to her citizens
of foreign birth.  A breach of this faith would deter immigration to the US.

Mercer concurred with Wilson's points.

Baldwin believed that discrimination of one's place of birth was no more
objectionable than that of age -- which all agreed to the propriety of.

The motion of G. Morris was voted against.

Carroll moved for "five" instead of "seven" which was voted against.

The section was then unanimously agreed to as formerly amended.

Wilson moved that in Article V, Section 3 -- qualifications of senators --
"nine" be replaced by "seven".  This was voted against.

Article IV, Section 5 reconsidered.

Randolph moved that the section be amended to read:

> All bills for raising money for the purposes of revenue, or for
> appropriating the same, shall originate in the House of Representatives; and
> shall not be so altered or amended by the Senate, as to increase or diminish
> the sum to be raised, or change the mode of raising or the objects of its
> appropriation.

Mason noted that the amendment removed all the objections as the section first
stood.  The arguments in favor of the section now have their full force.  The
senate does not represent the people but the states; therefore, it does not
have the right to tax the people -- this is the same reason against Congress
doing so under the AoC.  The senate is constructed differently from the house.
They will not be elected frequently nor return to their constituents
frequently.  They will naturally pursue schemes to increase their wealth.  A
bare veto does not have the same power as that of originating bills.  The
purse strings should be held by the direct representatives of the people.

Wilson was opposed to an equality of suffrage in the senate but did not wish
to multiply vices in the system.  This section would cause unnecessary strife
between the two houses as has happened in every state legislature which has
tried it.  The house will insert other things into money bills and, by making
them conditional on one another, destroy the liberty of the senate.  War,
commerce, and revenue are the great objects of the general government, and all
of them are connected with money.  The senate would be excluded from
originating any important bills whatsoever.

Gerry noted that taxation and representation are strongly associated in the
minds of the people.  The plan will inevitably fail if the direct
representatives of the people are not given the sole power to originate money
bills.

G. Morris pointed out that the senate will still be able to use their veto to
extort the house's concurrence on their favorite measures.

Madison thought it would be proper to allow the senate to diminish the sums to
be raised.  Why should the senate be restrained from checking the extravagance
of the house?  He agreed with Wilson that the amended section would raise
unnecessary contention between the houses and had done so in Virginia.  He
especially wondered at the ambiguity of "revenue", "amend", "alter", and
"increased or diminished".  Much proposed legislation might indirectly affect
any of these things.  Who might say in some cases whether revenue was the
principle object of a bill?  The section provides no check against the evils
its supporters think it will.  Either the senate will yield to the money bills
of the house -- in which case a valuable check will be lost -- or the
inflexibility of the senate will force the house to adapt its bills to their
wishes -- in which case there will be no point to the house's exclusive power.
With respect to the compromise, five states voted against equal suffrage in
the senate.  Of those five, four vote against this section or are divided.
"What obligation then can the small states be under to concur against their
judgments in reinstating the section?"(II, 277)

Dickinson:  "Experience must be our only guide.  Reason may mislead us."(II,
278)  Experience has verified the principle of restraining money bills to the
direct representatives of the people.  Disputes between the houses will arise
either way -- they will have different bills they want to pass.  The people
will be against any system that does not give their representatives the purse
strings.  Eight states have given the lower house the exclusive power to
originate money bills while allowing the upper house the right to amend.  It
would be proper for the general government to do the same.

Randolph stated that his principal goal with the motion was to prevent
objections and secure adoption of the plan.  The people will see an
aristocracy in the senate and a little king in the president.  Their alarms
will already be raised without taking a power from their representatives to
which they have grown accustomed.  The power to declare war should probably
also be placed in the house of representatives.  He was not against limiting
the restriction to *mere* money bills so that other bills relating to commerce
can be originated in the senate.

Rutledge believed the people will consider this a mere "tub for the whale" --
having no real consequence, and they will more likely see it as a means of
placating them than a means of securing their rights.  If anyone is given an
exclusive right to the origination of money bills, then it should be the
senate.  The senators will be more conversant in business and having more
leisure will be better able to digest the bills.  In South Carolina such a
rule distracts every session with unnecessary altercations.

Carroll stated that the same difficulties arise in Maryland on the question of
which bills are to be considered money bills.

On the motion by Randolph to reinstate the section with his amendments, it was
voted against.

Madison writes in his journal that until the last vote Washington had
disapproved of and voted against the exclusive right of originating money
bills.  However, on this last vote he supported it.  Washington said he voted
in the affirmative because it didn't hold too much weight with him, and it was
so preferred by others, who if disappointed, might be less willing to concur
on points of real weight.(II, 280, footnote)

The committee adjourned.

Two days later the Pennsylvania Herald would write:

> The debates of the federal convention continued until five o'clock on Monday
> evening; when it is said, a decision took place upon the most important
> question that has been agitated since the meeting of this assembly.(III, 69)

[Not even close.]

# August 14, Tuesday

Article VI, Section 9 taken up:

> The members of each House shall be ineligible to, and incapable of holding
> any office under the authority of the United States, during the time for
> which they shall respectively be elected: and the members of the Senate
> shall be ineligible to, and incapable of holding any such office for one
> year afterwards.

Pinckney argued that making the legislators ineligible to hold office was
degrading to them.  The senate should contain the fittest men, but it will
cease to be a magnet of statesmanship and talent.  He moved, seconded by
Mifflin, to replace it with the following:

> The members of each house shall be incapable of holding any office under the
> United States for which they or any other for their benefit receive any
> salary, fees, or emoluments of any kind -- and the acceptance of such office
> shall vacate their seats respectively.(II, 282)

Mason, sarcastically, proposed that striking out the whole section would
better encourage the aristocratic corruption that otherwise would not find
root on American soil.  It would open our councils to those legislators who
would carve out offices and rewards for one another's merit.

Mercer argued that the governments of the states are already aristocratic.
"Governments can only be maintained by *force* or *influence*."(II, 284)  The
President in this scheme has no force.  Deprive him of influence by making the
legislators ineligible to hold executive offices, and you will deprive him of
all authority.  Without the influence, the aristocracy will battle the people
when the battle should be between the President and the aristocracy.

Gerry could not agree with Pinckney that the disqualification was degrading.
Few ministers and ambassadors will be necessary, but if we let the senate
appoint them as it seems to be intended and we allow them to appoint from
their own ranks, embassies will multiply for their own sakes.  If we have not
men who will serve in the legislature without eligibility to offices, then our
situation is deplorable indeed.  Let us choose a single despot now.  It will
be easier to satisfy the appetites of one man than of many.  It is much more
important that the senators do not have offices than the current members of
Congress because the former have much more power than the latter.  He moved to
make the representatives also ineligible to offices for one year after their
term.  If anyone thinks this will prevent fit men from serving in the
legislature, then we should make it a requirement for the offices that one
must have held a position in the legislature.

G. Morris felt that if you prevented the legislature from taking positions in
the army and navy, then you set up opposing interests to one another.  The
generals will look at the "talking lords who do not face the foe" and will
take the power from them by force.  If the people choose to give the
legislators their confidence, why should we deny their service?

Williamson had voted against the money bills, but now America has its House of
Lords.  Now we are to also have a whole legislature that will carve out
offices for itself.  He had not seen a corrupt measure in the legislature of
North Carolina that couldn't be traced back to "office hunting".

Sherman believed the constitution should provide as few temptations as
possible.  Men of abilities will increase as the country grows more populous
and educated.

Pinckney pointed out that no state had made legislators ineligible to office.
In South Carolina judges may serve in the legislature.  It cannot be said that
the people will find this offensive.

Wilson could not oppose the section as it stood.  He also could not set aside
his judgment in order to flatter the prejudices of the people.  The national
government would need offices to attract fit men to serve and render them
useful to the people rather than serve in their state governments.  The
ambition to serve in offices of dignity and trust is not ignoble or culpable.
Pennsylvania has gone farther than any other state to fetter abuses of power,
and even she had not rendered the legislators ineligible to offices of
government.

Ellsworth did not think the mere postponement of office would discourage able
men from serving in the legislature.  They would serve two or seven years in
public office for the qualification.  They should not be eligible for office
while serving.

Mercer reminded them that this convention had been called together because of
the corruption of the state legislatures.  If our plan does not remedy that,
then it will not be recommended.  The constitution will not govern the nation.
It only marks out the form.  *Men* must govern the nation, and it is uncertain
whether the best will be willing to do it if they are ineligible to offices of
state.  The most influential men would rather stay at home and receive
appointments in their own state.

Wilson was not satisfied with the scenario Ellsworth proposed.  Either the
members must continue to serve in the legislature -- disqualifying themselves
from office -- or they must say to their constituents, 'We served you before
from the mercenary view of qualifying for public office, and now that that is
done, we will not seek election again.'

G. Morris rhetorically asked what might have happened in the revolution had
members of Congress or the state legislatures been ineligible to serve as
officers in the army.

On the question for postponing the section to take up Pinckney's motion, it
was lost.

G. Morris moved, seconded by Broom, to insert "except offices in the army or
navy, but in that case their offices shall be vacated" after "office".

Randolph continued to be opposed to striking out the clause entirely, but the
arguments with regard to disqualifying our greatest commanders from serving in
war had made an impact.  He would support Morris's exception.

Butler and Pinckney moved for a general postponement of the section until they
had decided on exactly what powers should be vested in the senate.  At that
point it would be easier to judge the adequacy of appointing senators to
offices.

The postponement was agreed to without debate.

Article VI, Section 10 taken up:

> The members of each House shall receive a compensation for their services,
> to be ascertained and paid by the State, in which they shall be chosen.

Ellsworth felt that too much dependence on the states would be caused by this
section.  He moved that the wording be changed to "that they should be paid
out of the Treasury of the US an allowance not exceeding ______ dollars per
day or the present value thereof."(II, 290)

G. Morris pointed out that payment by the states would put an unequal burden
on the most distant states.  He moved that the payment be out of the national
treasury and the amount to be at the discretion of the legislature.  There
should be no fear they will overpay themselves.

Butler argued for payment by the states -- particularly for the senate.  They
will be so long from their states that they will lose sight of their
constituents unless their salary is dependent on them.

Langdon believed it would be unjust to force the distant states to bear the
burden of their representatives traveling to and from the seat of government.

Madison wants a stability in the general government that is lacking the
states.  "The senate was formed on the model of that of Maryland.  The
revisionary check on that of New York.  What the effect of a union of these
provisions might be could not be foreseen."(II, 291)  [I'm not sure what
Madison's position is, but I think he is against allowing the national
legislature to pay themselves.]

Gerry agrees with Butler to an extent.  However, he also sees the difficulty
on the other side, that they may turn out senators by reducing their salaries.

Mason points out that the section makes the house of representatives dependent
on the state legislatures, so both houses will become the instruments of state
policies.

Broom could see no reason to prevent the national legislature from setting its
own salaries.  The state legislatures had this power, and there was no
complaint of it.

Sherman worried that the legislature might set their salaries so low that only
the independently wealthy will seek office.  The best plan might be to fix a
moderate allowance from the national treasury and let the states add to it as
they saw fit.  He moved that the allowance be five dollars per day.

Carroll expressed surprise at seeing this section in the draft at all.  It
makes both houses completely dependent on the states.  If a member does not
vote in support of the state's policies, they will be starved; if they do,
they will be rewarded.  The new government is nothing more than two
Congresses.

Dickinson understood the arguments ad difficulties on both sides but was
convinced that the new government must be independent of the prejudices of the
state legislatures.  He proposed that an act be passed every 12 years by the
national legislature setting their wages.  If the legislature is dependent on
the states, it would have been better if we had never met in this room.

Ellsworth was personally willing to trust the legislature to set their own
wages but knew the objections it would produce.  He thought the wording of his
motion allowing for changes in the value of money would help.

L. Martin asserted that if the senate is to represent the states, then they
should be paid by the states.

Carroll countered that the senate was to manage the affairs of the whole
country and not just their state interests.  They ought not to be paid by or
otherwise dependent on the states.

On the question for paying the legislators out of the national treasury, it
passed in the affirmative.

Ellsworth moved that the pay be fixed at five dollars per day (or the present
value thereof) while attending the legislature and the same for every thirty
miles of travel to and from the legislature.

Strong preferred four dollars, leaving the states to make additions.

On the question of five dollars, it was lost.

Dickinson moved, seconded by Broom, that the wages of both houses be the same.

Gorham pointed out how unreasonable this would be.  The senate will serve a
longer term; they will most likely have to move their family; in time of war,
they may have to sit constantly; their salaries *should* be higher.

Dickinson withdrew his motion.

"It was moved and agreed to amend the section by adding 'to be ascertained by
law'."(II, 293)

The section as amended was agreed to unanimously.

[In the Virginia ratifying convention, Patrick Henry challenges Madison on this
section.  He worries that the national legislature would be able to fix their
wages without limitation.  He believes there would be no inconvenience if the
states fix the wages of their representatives, and the public mind would be
eased.(III, 313)

Madison acknowledged that he would prefer the wages to be fixed in the
constitution if it were practical.  However, due to the changing values of
money and other goods [such as wheat which he brought up earlier in the
debates], a fixed sum would become inadequate in time.  He also thought it
improper that the general government be dependent on the state governments.
The weakness of that system had already been experienced under the
confederation.  The states will be only concerned with local interests and not
national ones.  What prevents the state legislators from raising their pay to
four or five pounds?  Why, the indignation of the people.  The same will
prevent the abuse in the national legislature.(III, 313-4)]

The committee adjourned.

# August 15, Wednesday

Article VI, Section 11:

> The enacting style of the laws of the United States shall be: "Be it enacted
> by the Senate and Representatives in Congress assembled."

was agreed to without debate.

Article VI, Section 12 taken up:

> Each House shall possess the right of originating bills, except in the cases
> before mentioned.

Strong moved to amend the article to read:

> Each house shall possess the right of originating all bills except bills for
> raising money for the purposes of revenue, or for appropriating the same and
> for fixing the salaries of the officers of the Government which shall
> originate in the house of representatives; but the senate may propose or
> concur with amendments as in other cases.(II, 297)

Mason seconded the motion.  He wished to take this power form the senate who
he felt "could already sell the whole country by means of treaties."(II, 297)

Gorham supported the amendment.  The senate will first acquire the habit of
preparing money bills, and then the practice will grow into an exclusive
right.

G. Morris opposed it as unnecessary and inconvenient.

Williamson said that there were some who thought this restriction essential to
liberty and some who thought it unimportant.  Why shouldn't we indulge the
former?  They will be reluctant to grant any powers to the senate while they
can originate money bills.  Those of us who want a strong senate will lose
more than we gain by pressing this issue.  He moved, seconded by Rutledge, to
postpone the subject until they had enumerated the powers of the senate.

Mercer wished not to return to a reconsideration of this section.  Addressing
Mason's comment about treaties, he preferred that the power to make treaties
rest in the executive -- not the senate -- with the caveat that they would not
be the final law of the land until ratified by legislative authority.

Mason did not worry that treaties would repeal law.  He worried that the
senate by treaties might alienate territory.  If Spain took control of
Georgia, e.g., the senate might by a treaty dismember the union.

On the question for postponing the section, it passed in the affirmative.

Article VI, Section 13 taken up:

> Every bill, which shall have passed the House of Representatives and the
> Senate, shall, before it become a law, be presented to the President of the
> United States for his revision: if, upon such revision, he approve of it, he
> shall signify his approbation by signing it: But if, upon such revision, it
> shall appear to him improper for being passed into a law, he shall return
> it, together with his objections against it, to that House in which it shall
> have originated, who shall enter the objections at large on their journal
> and proceed to reconsider the bill. But if after such reconsideration, two
> thirds of that House shall, notwithstanding the objections of the President,
> agree to pass it, it shall together with his objections, be sent to the
> other House, by which it shall likewise be reconsidered, and if approved by
> two thirds of the other House also, it shall become a law. But in all such
> cases, the votes of both Houses shall be determined by yeas and nays; and
> the names of the persons voting for or against the bill shall be entered on
> the journal of each House respectively. If any bill shall not be returned by
> the President within seven days after it shall have been presented to him,
> it shall be a law, unless the legislature, by their adjournment, prevent its
> return; in which case it shall not be a law.

Madison moved, seconded by Wilson, that bills passed in the legislature be
signed into law by both the executive and the judiciary.  If either vetoes the
bill, then two-thirds must pass it, but if both veto it, then three-fourths
should be required.

Pinckney opposed the interference of judges in the legislative business.  It
would involve them in parties and color their decisions.

Mercer approved the motion.  He disapproved of the doctrine that the judges,
as expositors of the constitution, should have the right to declare a law
void.  The law should be cautiously made, and then be uncontrollable.  This
motion is the right way to make the judiciary separate and independent.

The motion was lost.

G. Morris regretted that the check could not be agreed to.  He suggested
three-fourths be required to overturn a presidential veto.  He had no great
faith in the veto power as long as the president was appointed by the
legislature.  The state legislatures have time and time again passed laws to
issue paper money.  Were the national legislature of this plan formed and war
broke out, paper money would be resorted to again if not guarded against.
Requiring three-fourths would not be a complete remedy, but it might prevent
the hasty passage of laws.

Dickinson was impressed by Mercer's comment.  He, too, did not wish to allow
the judges to void the law, but he could not think of a better way to preserve
their independence.

G. Morris suggested an *absolute* presidential veto.  He could no agree with
others that the judges must be bound to call a direct violation of the
constitution law.  Encroachments of the popular branch on the executive should
be guarded against.  He wished for the section to be postponed so that a
better check than requiring only two-thirds to overrule should be devised.

Sherman asked if it is better to trust one man or all the others in agreement.
He disapproved of judges meddling in the legislative process.  The veto power
is good enough as it stands.

Carroll stated that when two-thirds was decided on, the number in a quorum was
not fixed.  Now that a majority is a quorum, 17 in one house and 8 in the
other might carry a bill into law.  He thought the controlling power of the
executive could not be determined until they had determined how the formation
of that department would be regulated.  He wished the matter to be postponed.

Gorham criticized the difficulties and postponements.  Some said they could
not agree to the form of government until the powers were defined, and others
cannot agree on the powers until they see how it is formed.  A majority for a
quorum is as large as necessary, and this is the size in most of the states.

Wilson was most apprehensive of the legislature swallowing up the rest of the
government.  'King' and 'tyrant' go together in most men's minds, not
'legislature' and 'tyranny', but where the executive is not given sufficient
power, the last two preponderate.  After the destruction of the king in Great
Britain, the parliament became a greater tyrant than had been exercised by any
of her monarchs.  We have not guarded against that situation by sufficient
defensive power in the executive or the judiciary.

Rutledge was against postponing and the tediousness of these proceedings.

Ellsworth agreed.  We grow more and more skeptical.  We will be unable to make
any decision if we continue down this path.

The question for postponement was lost.

Williamson moved, seconded by Wilson who repeated Carroll's arguments, that
"two-thirds" be replaced by "three-fourths".

On this motion, it passed in the affirmative.

Madison observed that if the veto was confined to "bills", then the veto might
be gotten around by passing acts under the name of "resolutions" or "votes",
etc.  He moved to add "or resolve" after "bill" in the beginning of the
section with exceptions for votes of adjournment.

The motion was rejected.

"Seven days" was replaced with "ten days (Sunday excepted)".

Article VI, Section 13 as amended was agreed to.

The committee adjourned.

# August 16, Thursday

Randolph moved to add a Section 14 to Article VI:

> Every order, resolution, or vote, to which the concurrence of the Senate and
> House of Representatives may be necessary (except on question of adjournment
> and in the cases hereafter mentioned) shall be presented to the President
> for his revision; and before the same shall have force, shall be approved by
> him, or, being disapproved by him, shall be repassed by the Senate and House
> of Representatives, according to the rules and limitations prescribed in the
> case of a bill.(II, 303)

Sherman thought it unnecessary except in the case of money being taken out of
the public treasury, in which case the exception might be provided in another
place.

The motion passed in the affirmative.

Article VII, Section 1 taken up:

> The Legislature of the United States shall have the power to lay and collect
> taxes, duties, imposts and excises; etc.

L. Martin asked why there was a distinction between "duties" and "imposts".
If they mean the same thing, one word should be used.  If different, they
should be clearly defined.

Wilson stated that "imposts" are related solely to commerce whereas "duties"
extend to a variety of uses such as stamp duties, etc.

Carroll reminded the convention of the great impropriety of allowing a
majority to be a quorum.  [This seems like a bit of a non sequitur.]

Mason urged an explicit connection between the power of levying duties and
taxes with the prohibition on taxes on exports in Article VII, Section 4.  He
hoped the northern states did not wish to deny the southern this security.  He
moved to append to the first clause "provided that no tax duty or imposition
shall be laid by the Legislature of the United States on articles exported
from any state."(II, 306)

Sherman had no objection to the provision, only that it take the parts of the
report out of order.

Rutledge did not care about the order as long as the second part of the
section relating to the importation of slaves was kept.

G. Morris thought the provision was inadmissible anywhere.  It might not be
equitable to tax imports without taxing exports, and sometimes taxing exports
would be the easiest of the two.

Madison made a number of points:
1. The taxing of exports is proper, and since the states cannot exercise it
   with propriety separately, it should be done collectively.
2. It might be done only on American goods which have no rivals in foreign
   markets, like tobacco.
3. It would be unjust to the states who export their neighbor's produce to
   leave it to be taxed by the latter.
4. The greatest exporters (the Southern states) are most in need of naval
   protections, and so it is no problem if the burden falls heaviest on them
   to pay for it.
5. We are not setting these rules for the present moment only, and time will
   equalize the situation of the states.

Williamson believed the clause against export taxes was reasonable and
necessary.

Ellsworth was against export taxes but was against the motion because he felt
the clause against was in its proper place.

Wilson was against prohibiting general taxes on exports, pointing out the
injustice done to New Jersey and Connecticut.

Gerry thought the legislature could not be trusted with the power, that they
might act partially to raise one part of the country at the expense of
another.

G. Morris countered that the legislature might ruin the country with any of
the powers granted to her.  Taxes on exports are perfectly acceptable.  Taxes
on exports are a necessary source of revenue because it will be a long time
before the people have enough money to be taxed directly.

Mercer was against giving Congress the power to tax imports.  The states have
now the right to tax th imports and exports of their noncommercial neighbors.
It is enough to take one of those things away.  It is said the southern states
are in most need of naval protection, but the opposite is the case.  Were it
not for the promotion of trade by northern ships, the southern states could
ship their cargo via foreign ships who would not need protection.

Sherman thought it wrong to tax imports except perhaps on some goods where
export should be discouraged.  The complexity of America's economy renders it
difficult to tax exports equally.  The oppression of the noncommercial states
(New Jersey and Connecticut, etc.) was prevented by the power to regulate
trade between the states.  The power to tax exports will wreck the whole.

Carroll was surprised anyone objected to taxes on exports.

The issue relating to exports was dropped until the section should be taken
up.

On the question to agree to the first clause of Article VII, Section 1, it
passed in the affirmative.

Clauses 2-5 were agreed to without debate:

> To regulate commerce with foreign nations, and among the several States;

[Dickinson wrote in the margin of his copy before the above, "no preference or
advantage to be given to any persons or place -- laws to be equal".(IV, 209)
Mason wrote to note Article VII, Section 6 and Article VIII.(IV, 209)]

>
> To establish an uniform rule of naturalization throughout the United States;
>
> To coin money;
>
> To regulate the value of foreign coin;

[Dickinson wrote in the margin of his copy before the above, "no money to be
drawn out of the national treasury, unless by an appropriation thereof by
law".(IV, 209)]

>
> To fix the standard of weights and measures;

Gerry moved, seconded by Mercer, to add post-roads to the clause on post
offices.

On the question, it passed in the affirmative.

G. Morris moved, seconded by Butler, to strike out "and emit bills on the
credit of the United States" in the 7th clause.  If the US has credit, they
would be unnecessary; if not, unjust and useless.

Madison wondered if it would be sufficient to not make them legal tender.
This would remove the temptation to emit them with unjust views, and
promissory notes might be appropriate in emergencies.

G. Morris argued that striking out the words would allow for "notes of a
responsible minister" which would have all of the benefits without the
negatives.  The monied interest will oppose the constitution if paper
emissions are not prohibited.

Gorham was for striking out.

Mason hated paper money but admitted that some emergency might one day require
it.  He did not on that account wish to tie the hands of the legislature.  The
revolution, for example, could not have been waged with the prohibition.

Gorham stated that the necessary power in that case was in borrowing.

Mercer liked paper money and opposed a prohibition of it altogether.  It would
be wrong to excite opposition to it from the friends of paper money.  The
monied interests will already be on the side of the new constitution.  It
would be wrong to further purchase their attachment with the loss of the
opposite class of citizens.

Ellsworth supported the prohibition on paper money.  The experiments had been
tried and failed, and now all respectable Americans are disgusted by it.  The
prohibition will gain more allies among the influential in support of the
constitution than almost anything else in it.  The power may do harm but never
good.

Randolph made the same points as Mason.

Wilson believed that barring paper money would have a healthy effect on the
credit of the United States.

Butler remarked that no government of Europe considers paper money a legal
tender.  Our government should lack the power.

Mason repeated his earlier points, adding that if no European country had the
explicit power, it was also true that none were restrained from emitting paper
money.

Read "thought the words, if not struck out, would be as alarming as the mark
of the Beast in Revelation."(II, 310)

Langdon would rather reject the entire plan and "make General Washington
despot of America"(III, 305) than allow the three words "and emit bills".

On the motion for striking it out, it passed in the affirmative.  Madison, who
had argued against it, voted in favor of the motion.  He became convinced that
the emission of the clause would not prevent the use of public notes by the
government.  It would only prevent paper currency from being made a legal
tender for public or private debts.

The clause for borrowing money was agreed to unanimously.

The committee adjourned.

# August 17, Friday

Article VI, Section 1 resumed at clause 9:

> To appoint a Treasurer by ballot;

Gorham moved, seconded by Pinckney, that it would be more convenient to insert
"joint" before "ballot".

Sherman opposed it, saying it favored the larger states.

Read moved, seconded by Mercer, that the executive should bear the
responsibility of appointments.  The state legislatures demonstrate the
impropriety of leaving the choices to Congress.

On Gorham's motion, it passed in the affirmative.

Mason opposed Read's motion.  The representatives of the people should appoint
the keeper of the people's money.

On Read's motion, it was lost.

In Mason's notes, he supported both the joint ballot as well as a possibility
of multiple treasurers.(IV, 209)

The next two clauses agreed to without debate:

> To constitute tribunals inferior to the Supreme Court;
>
> To make rules concerning captures on land and water;

The next clause taken up:

> To declare the law and punishment of piracies and felonies committed on the
> high seas, and the punishment of counterfeiting the coin of the United
> States, and of offenses against the law of nations;

Madison moved to strike out everything after "declare the law".

Mason doubted "the safety of it, considering the strict rule of construction
in criminal cases.  He doubted also the propriety of taking the power in all
cases wholly from the states."(II, 315)

G. Morris thought the authority needed to be extended to include *any*
counterfeiting such as foreign currency.  Mason writes the same thing in his
notes.(IV, 209)

Randolph did not think striking the clause was necessarily exclude the power
and doubted the efficacy of the word "declare".

Wilson favored the motion.  Strictness was not necessary to give authority.

The motion passed in the affirmative.

G. Morris moves to reword the beginning to "To punish piracies and
felonies..." which passed in the affirmative.

Madison and Randolph moved to insert "define and" before "punish".

Wilson thought "felonies" was sufficiently defined by common law.

Dickinson agreed with Wilson.

Mercer supported the motion.

Madison argued that "felony" was vague in common law.  If the laws of the
states prevailed, then the citizens of different states would face different
punishments for the same crimes at sea.  "There would be neither uniformity
nor stability in the law".(II, 316)

G. Morris preferred "designate" to "define" since the latter limited one to a
preexisting meaning.

The motion passed in the affirmative.

Ellsworth moved to reword the clause in question to:

> To define and punish piracies and felonies committed on the high seas,
> counterfeiting the securities and current coin of the United States and
> offenses against the law of nations.(II, 316)

This was agreed to without debate.

Clause 13 was taken up:

> To subdue a rebellion in any State, on the application of its legislature;

Pinckney moved, seconded by G. Morris, to strike out "on the application of
its legislature".

L. Martin opposed it as a dangerous power.  The state's consent must precede
the introduction of extraneous force.

Mercer agreed with Martin.

Ellsworth moved to append "or executive" to it.

G. Morris argued that the executive might be at the head of the rebellion.
The general government should enforce obedience wherever necessary.

Ellsworth still did not agree with an invasion of national force without
consent.  He suggested appending "or without it when the legislature cannot
meet."(II, 317)

Gerry was against "letting loose the myrmidons of the United States on a state
without its own consent."(II, 317)  Shay's Rebellion would have been a
bloodier conflict if the general government had entered the fray.

Langdon agreed with Pinckney.  The mere apprehension of national force will
make insurrectionists think twice.

Randolph thought that if the national legislature is to judge whether or not a
state legislature can meet, then that amendment is as objectionable as
Pinckney's motion.

G. Morris said that we have formed a strong man to protect us, but at the same
time we want to tie his hands.  The legislature can be trusted to preserve the
public tranquility.

On the motion to append "or without it when the legislature cannot meet" to
the clause, it passed in the affirmative.

Madison and Dickinson moved to add "against the Government thereof" after
"State" since there might be a rebellion against the United States.  It was
agreed to without debate.

On the clause as amended, it was lost.

Clause 14 was taken up:

> To make war;

Pinckney opposed this power in the legislature.  The proceedings would be too
slow.  The power should be given to the senate alone.  They will be more
acquainted with foreign affairs and capable of proper resolutions.  Since the
senate has an equality of suffrage, there can be no worry that the larger
states will drag the rest into war.

Butler preferred the power to be with the President who will have all the
requisite qualifications and will not make war unless the nation supports it.

Madison and Gerry moved to replace "make" with "declare", leaving the power to
repel sudden attacks to the President.

Sherman agreed with the clause as it stood.  "Make" is a more broad power than
"declare", and the President should already be understood to be able to repel
attacks.

Gerry never expected any republican to give the President the power to declare
war.

Ellsworth pointed out that there is a difference in the powers to make war and
peace.  The former should be difficult, and the latter should be easy.  Making
war is also a simple act, and peace might require intricate and secret
negotiations.

Mason preferred "declare" over "make".  The President cannot be trusted with
the power, and the Senate is not so constructed to possess it.  Making war
should be a difficult process.

On the motion to replace "make" with "declare", it passed in the affirmative.

The motion to strike it out was disagreed to without a vote.

Butler moved to give the legislature the power to make peace.

Gerry seconded him.  The senate, being a smaller body, are more likely to be
corrupted by an enemy than the legislature as a whole.

On the motion for adding "and peace" after "war", it was lost.

The committee adjourned.

Gerry writes to his wife, "Some members of the convention are very impatient,
but I do not think it will rise before three weeks."(IV, 227)

# August 18, Saturday

Madison submitted a list of powers that should be added to those already
enumerated for the legislature.  [They are long so I will not enumerate them
here, see II, 321-2.]

Mason believed the general government rather than the states be given the
reigns of the militias.  They would be better led and trained than under
thirteen different organizations.  He hoped that there would be no standing
army in peace time other than the manning of a few forts.  He therefore moved
to add "a power to regulate the militia" to Madison's list.

Gerry believed "some provision ought to be made in favor of public
securities"(II, 326) and also of letters of marque which are not explicitly
included in the power of war.  Regarding the bit about public securities,
Ellsworth (as "The Landholder") accuses him of wanting the new government to
honor the full value of Continental paper money because he owns $10,000 worth
of it.(III, 171)  Gerry denies this and clarifies "that he proposed the public
debt should be made neither better nor worse by the new system, but stand
precisely on the same ground by the Articles of Confederation".(III, 240)

Rutledge moved "that funds appropriated to public creditors should not be
diverted to other purposes."(II, 326)  This motion was agreed to.

Mason agreed with Rutledge's motion on principle but worried that it would be
a dangerous fetter in time of war.  He worried about perpetual taxes.[???]
His motion to refer a clause "for restraining perpetual revenue" was agreed to
without debate.

Rutledge moved, seconded by King and Pinckney, that a committee be made to
consider the general government taking on the debts of all the states.  It was
only right since the general government was also going to remove the right to
tax imports which is a major source of income for many of the states.

Sherman agreed the motion was just but preferred to authorize the legislature
to assume the debts rather than to say it *must* be done.

Ellsworth preferred that the state debts be equally shared among the states.

King reminded Ellsworth that there would be many opponents of the plan if they
proposed to consolidate the resources of the states without also taking on
their debts.  King himself was not convinced of the practicability of the
decision, but he felt it was worth referring to a committee for consideration.

On the motion to consider the consolidation of the state debts, it passed in
the affirmative.

The motions by Gerry wrt public securities and letters of marque were agreed
to without debate.

King suggested that if the general government were consolidating the debt,
they ought also to get the "unlocated" lands of the states.  Williamson
concurred.

A grand committee was appointed consisting of:
* Langdon
* King
* Sherman
* Livingston
* Clymer
* Dickinson
* McHenry
* Mason
* Williamson
* C. C. Pinckney
* Baldwin

Rutledge, because of the impatience of the public and many of the members,
moved that the convention meet at 10:00 every morning (excluding Sunday) until
they adjourn at 16:00, and there can be no motion for adjournment in between.
Wilson said later "that at 4 o'clock they should break up, even if a member
was in the middle of his speech."(III, 138)

The motion passed in the affirmative.

Ellsworth proposed that there ought to be a council for the President
consisting of the president of the senate, chief justice, and ministers of
foreign affairs, domestic affairs, war finance, and marine.  [~~He probably
means a council related to the veto rather than the cabinet we are familiar
with.~~ **OR** he might actually mean a cabinet along the lines of what G.
Morris submits to the committee on August 20 -- *see below.*]

Pinckney wished that the motion be postponed as G. Morris had given notice of
proposing a similar motion but was not currently on the floor.  His preference
was that the President should have the power to call on others for council as
he choose.

Gerry opposed allowing, e.g., the finance minister and the chief justice
anything to do with legislative affairs.  These men would have other matters
to attend to.

Dickinson said that the other offices should be part of a council only if
appointed by the legislature, but not in the case of by the President.

Clause 15 taken up:

> To raise armies;

Gorham moved to append "and support" to the clause which passed unanimously,
and then the clause as amended was agreed to unanimously.

Dickinson writes in his notes opposite this clause, "to erect forts".(IV, 209)

Clause 16:

> To build and equip fleets;

was modified to read "To provide and maintain a navy" and agreed to
unanimously.  They also added the clause "to make rules for the government and
regulation of the land and naval forces" from the AoC.

Gerry was concerned that there was no check against standing armies in times
of peace.  The people would oppose the plan with such an omission.  He, along
with L. Martin, moved that there not be more than ______ thousand troops kept
up by the general government in time of peace.  He suggested two or three
thousand for the blank.

[There is a later anecdote, likely apocryphal, that Washington suggested a
counter-motion that "no foreign enemy should invade the United States at any
time, with more than three thousand troops."(IV, 229)]

C. C. Pinckney asked Gerry if no troops could ever be raised unless attacked.

Gerry replied that if nothing prevented them, then some states may establish
military governments.

Langdon saw no reason to so distrust the representatives of the people.

Dayton stated that preparations for war are often made in times of peace.
For all we know some standing force may in time become necessary.

The motion of Gerry and Martin was lost unanimously.

Mason moved as an additional clause "to make laws for the regulation and
discipline of the militia of the several states reserving to the states the
appointment of the officers".(II, 330)  He was concerned with the uniformity
of the militia throughout the union.

C. C. Pinckney agreed with Mason, believing that the states would never
preserve the discipline of their militias.

Ellsworth agreed with submitting the militias to the authority of the national
government as much as possible but felt Mason's motion went too far.  The
states should not be forced to relinquish this power.  He moved, seconded by
Sherman, "that the militia should have the same arms and exercise and be under
rules established by the general government when in actual service of the
United States and when states neglect to provide regulations for militia, it
should be regulated and established by the legislature of the United
States."(II, 331)

Dickinson agreed that the states ought never to give up complete control of
their militias, nor should they be asked to.  He proposed that the general
government be in authority of one-fourth of the militias at any one time, and
by rotation will discipline the whole.

Butler urged that all the militias be under the general government who had all
the responsibility of general defense.

Mason now worried about creating insurmountable objections to the plan.  The
withdrew the original motion and moved for the clause "to make laws for
regulating and disciplining the militia, not exceeding one-tenth part in any
one year, and reserving the appointment of officers to the states."(II, 331)

C. C. Pinckney renewed Mason's original motion.  There was no reason to
distrust the general government on this account.

Langdon seconded the renewal.  He worried more about the confusion of
authorities than putting the whole thing under the general authority.

Madison likewise did not see how the power could be divisible between two
authorities.  The authority over the militias should rest with the power of
general defense.  If the states can trust the general government with the
public treasury, they can trust it with the public force.

Ellsworth thought it impossible to come up with a uniform regulation of the
militia.  Three shillings worth of a penalty will enforce obedience better in
New England than forty lashes elsewhere.

Pinckney thought the states would be willing to cede the power but admitted
that he had little faith in the militia.  The nation needed a real military
force as was seen in the latest Shay's Rebellion.

Sherman noted that the states might want the militia for defense and enforcing
their own laws.

Gerry lacked the confidence that others showed in the general government to
possess authority over the militias and believed the states would lack
confidence as well.

Mason thought Sherman made a good point and moved to add an exception to his
motion "of such part of the militia as might be required by the states for
their own use."(II, 333)

Read did not believe the appointment of the officers of the militias should be
left to the states.  Some states appointed them by the legislatures, some by
the people.  He thought they should at least insist on an appointment by the
state governors.

The convention then appointed both the motion of Mason as well as that of C.
C. Pinckney to the grand committee.

The committee adjourned.

George Washington, in a letter to Henry Knox, seems upset with the progress of
the convention and does not entirely approve of the plan so far.  Yet, he
wishes that Congress and the people adopt whatever comes out of the convention
because "it is the best that can be obtained at the present moment under such
diversity of ideas as prevail."(III, 70)

A rumor begins going around, apparently started this date by the New York
Daily Adviser, that the convention is proposing to establish a monarchy under
the Bishop of Osnaburgh.(II, 333, footnote)  [Could this be a modified
reference to the request from Gorham to Prince Henry of Prussia to become king
of the US?(*Shay's Rebellion:  The Making of an Agrarian Revolution*, p.82)]
This rumor might have also inspired the anecdote told by James McHenry of the
woman who asked Franklin, "Well, Doctor, what have we got, a republic or a
monarchy?"  To which Franklin replied, "A republic if you can keep it."(III,
85)

A response to the rumor was printed in The Pennsylvania Journal on August 22:
"tho' we cannot, affirmatively, tell you what we are doing, we can,
negatively, tell you what we are not doing -- we never once thought of a
king."(III, 74)

# August 20, Monday

Pinckney submitted a number of propositions to be considered by the committee
of detail.  [The full list of them can be found at II, 334-5.]  What makes
some of these propositions important is that they would eventually be appended
to the constitution as the Bill of Rights.  (Others such as habeas corpus
would be included elsewhere.)  A selection follows:

> The liberty of the press shall be inviolably preserved.
>
> No soldier shall be quartered in any house in time of peace without consent
> of the owner.
>
> No religious test or qualification shall ever be annexed to any oath of
> office under the authority of the United States or any individual state.

Pinckney writes of his propositions after the convention, "The three first
[Habeas Corpus, Trial by Jury, and Freedom of the Press] essential in free
governments; the last [the prevention of Religious Tests], a provision the
world will expect from you, in the establishment of a system founded on
republican principles and in an age so liberal and enlightened as the
present."(III, 122)

G. Morris, seconded by Pinckney, then submitted his own propositions which
detailed a "Council of State" and its officers who would council the President
in his affairs.  [See the full list at II, 335-7.]  The officers include:

1. The Chief Justice of the Supreme Court
2. Secretary of Domestic Affairs
3. Secretary of Commerce and Finance
4. Secretary of Foreign Affairs
5. Secretary of War
6. Secretary of the Marine

A Secretary of State should also be appointed by the President to hold his
office during pleasure and serve as a secretary to the Council of State and
public secretary to the President.

Gerry moved that the committee of detail be instructed to report proper
qualifications for the president as well as the method of impeaching Supreme
Court judges.  This was submitted.

Clause 17:

> To call forth the aid of the militia, in order to execute the laws of the
> Union, enforce treaties, suppress insurrections, and repel invasions;

was postponed until the power over the militia could be settled by the
committee of detail from yesterday.

Mason moved to give Congress the power "to enact sumptuary laws" (laws
attempting to regulate the consumption of luxury).  "No government can be
maintained unless the manners be made consonant to it."(II, 344)

Ellsworth believed the best means of achieving the goal was through taxation.
Food and drink should be taxed if they are to be regulated.

G. Morris argued that sumptuary laws created a landed nobility by fixing their
present possessions in the posterity of the great landholders.(II,334)  "If men
had no temptation to dispose of their money they would not sell their
estates."(II, 351)

Gerry stated "the law of necessity is the best sumptuary law."(II, 344)

On the motion for sumptuary laws, it was lost.

Clause 18 was taken up:

> And to make all laws that shall be necessary and proper for carrying into
> execution the foregoing powers, and all other powers vested, by this
> Constitution, in the government of the United States, or in any department
> or officer thereof;

[See Hamilton's comment on this clause in *The Federalist, No. XXXIII*, III,
239.]

Madison and Pinckney moved to add "and establish all offices" after "make all
laws".  They thought some may raise trivial objections that the former is not
included in the latter.

G. Morris, Wilson, Rutledge, and Ellsworth urged that the motion was not
necessary.

On the motion to establish offices, it was lost.

The clause as reported was then agreed to unanimously.

Article VII, Section 2 taken up:

> Treason against the United States shall consist only in levying war against
> the United States, or any of them; and in adhering to the enemies of the
> United States, or any of them. The Legislature of the United States shall
> have power to declare the punishment of treason. No person shall be
> convicted of treason, unless on the testimony of two witnesses. No attainder
> of treason shall work corruption of blood, nor forfeiture, except during the
> life of the person attainted.

Madison thought the definition of treason was too narrow.  It did not go as
far as the
[Statute of Edward III](https://en.wikipedia.org/wiki/Treason_Act_1351).
More latitude should be left to the discretion of the legislature.

Mason supported the Statute of Edward III.

G. Morris wanted the United States to have an exclusive right to define
treason.  In a contest between a state and the union, everyone could be
considered a traitor to one or the other.

Randolph did not like the term "adhering".  The British statute says "giving
them aid or comfort" which was more extensive.

Ellsworth considered the two wordings the same.

G. Morris disagrees and says the section does not go far enough.

Wilson felt that "giving aid and comfort" were explanatory, not operative, and
it was better to omit them.

Dickinson also did not like the addition of "giving aid and comfort".  This
extended too far.  What is meant by two witnesses?  Must they witness the same
act, or can they be witnesses of different acts?  The proof of an overt act
should be explicit.

Johnson also considered "giving aid and comfort" to be explanatory and thought
something should mention "overt acts" in the section.  Treason cannot be both
against the US *and* a state -- the sovereignty is one in the same.

Madison suggested changing the "and" before "adhering" to an "or".  Otherwise
a person might be required to commit both acts to count as treason.  The
individual states are left with a concurrent power to define and punish
treason against themselves which might involve a double punishment.

It was moved that the whole clause be recommitted but was lost.

Wilson and Johnson moved that "or any of them" be removed after "United
States" which was agreed to without debate.

Madison asserted that the same act may be treason against the general
government and a particular state.

Ellsworth did not think this was a danger to the general authority.  It is
paramount to the state authorities any way.

Johnson still asserted that there could be no treason against a particular
state.  They are not sovereigns -- even under the confederation -- much less
so under the new system.

Mason said that the states will still retain a part of their sovereignty.  One
might commit an act of treason against the state that is not treasonous to the
US.  He cited Bacon's Rebellion as an example.

Johnson stated that in that case it would amount to treason against the
sovereign, the supreme sovereign, the United States.

King thought the controversy over what was or wasn't treason isn't as
important others suppose.  The legislature might punish other capital crimes
under names other than "treason".

G. Morris and Randolph moved to substitute the wording of the British statute:

> Whereas it is essential to the preservation of liberty to define precisely
> and exclusively what shall constitute the crime of treason, it is therefore
> ordained, declared, and established that if a man do levy war against the US
> within her territories, or be adherent to the enemies of the US within the
> said territories, giving them aid and comfort within their territories or
> elsewhere, and thereof be provably attainted of open deed by the people of
> his condition, he shall be adjudged guilty of treason.(II, 347)

On this question, it was lost.

It was moved to strike out "against the United States" in an effort to define
treason more generally.  And it passed in the affirmative.

It was moved to insert after "two witnesses" the words "to the same overt
act".

Franklin supported this motion.  Trials of treason were usually passionate and
perjury too easy to make use of against the innocent.

Wilson could see both sides.  Treason might be difficult to prove in some
circumstances, as in traitorous correspondence with an enemy.

On the question of the same overt act, it passed in the affirmative.

King moved, seconded by Broom, to give the US "sole power" to declare the
punishment of treason.  The vote to strike out "against the US" which defined
treason more generally.  The states may still prosecute serious offenses as
high misdemeanors.

Wilson again could see both sides and distrusted his present judgment on the
matter.

On inserting the word "sole", it was lost.

Wilson felt the section was now too ambiguous.  Either "sole" should have been
inserted or "against the US" retained.

King stated that there was no distinction between treason to a state and to
the US.  Treason against one is treason against the other.

Sherman stated there was a difference -- which laws are being resisted forms
the line.

Ellsworth agreed with Sherman and asserted that the each sovereign ought to
have the authority to defend themselves.

Dickinson agreed with King that war against one implied war against the other,
but the Constitution must be made clear on that matter.

Wilson and Ellsworth moved to reinstate "against the United States" after
"treason" which passed in the affirmative.

Madison was not satisfied with the clause as it now stood.  As treason against
the US implies a treason against the states and vice versa, the same act may
be tried and punished twice.  G. Morris agreed.

It was moved and seconded to amend the clause to read "treason against the US
shall consist only in levying war against them or in adhering to their
enemies" which passed in the affirmative.

Mason moved to insert "giving them aid and comfort".  "Adhering" was too
indefinite to be used alone, which passed in the affirmative.

L. Martin moved to insert after "testimony of two witnesses" the words "or on
confession in open court".  Some thought the amendment was superfluous, but it
passed in the affirmative.

Article VII, Section 2 was agreed to unanimously.

[L. Martin in his *Genuine Information* claims that he foresaw a situation in
which a state might go to war (justly) against the general government to
preserve its sovereignty.  Anyone who fought on the side of their state in
such a conflict would commit treason against the US, and any citizen of the
state who took the side of the US would be committing treason against their
state.  In order to save the citizens from this dilemma, he claims that he
proposed the following clause:

> Provided, that no act or acts done by one or more of the states against the
> United States, or by any citizen of any one of the United States, under the
> authority of one or more of the said states, shall be deemed treason, or
> punished as such; but, in case of war being levied by one or more of the
> states against the United States, the conduct of each party towards the
> other, and their adherents respectively, shall be regulated by the laws of
> war and of nations.

He notes that this motion was not accepted.(III, 223)  There is no mention of
this motion being made on this day by the official Journal or Madison or
McHenry.  Scanning the journals, I can also find no time later when Martin
supposedly introduces this motion.]

Article VII, Section 3 taken up:

> The proportions of direct taxation shall be regulated by the whole number of
> white and other free citizens and inhabitants of every age, sex and
> condition, including those bound to servitude for a term of years, and three
> fifths of all other persons not comprehended in the foregoing description,
> (except Indians not paying taxes) which number shall, within six years after
> the first meeting of the Legislature, and within the term of every ten years
> afterwards, be taken in such manner as the said Legislature shall direct.

The words "white and other" were struck out unanimously as being superfluous.

Ellsworth moved that the first census be taken "three" years after the first
meeting of the legislature.  It passed in the affirmative.

King asked, What was the precise meaning of "direct taxation"?  No one had an
answer.(II,350)  Dickinson in his notes opposite this section has the same
question.(IV, 209)

Gerry moved to append the following clause to the section:

> That from the first meeting of the legislature of the US until a census
> shall be taken all monies for supplying the public treasury by direct
> taxation shall be raised from the several states according to the number of
> their representatives respectively in the first branch.(II, 350)

Langdon argued that this would be an unreasonable burden on New Hampshire.

Carroll opposed it.  The number of representatives were not exactly
proportionate enough to be used for taxation.

Before a question was taken, the committee adjourned.

Alexander Hamilton writes to Rufus King that he has notified his other New
York colleagues that he will accompany them to Philadelphia to be present in
convention.  He asks King to let him know when the business is about to finish
so he can be there.(III, 70)

Hugh Williamson writes to Governor Caswell of North Carolina, "It will be
sufficient for us if we have the satisfaction of believing that we have
contributed to the happiness of millions."(III, 71)

# August 21, Tuesday

Livingston delivered a report from the committee of detail elaborating the
motions passed on the US taking on the debt of the states, and the method by
which the US would share authority of the militias with their states.  [See
II, 352.]

The report includes the phrase "for the common defense and general welfare"
which would find its way into the preamble and Article I, Section 8 of the
finished constitution.  There was some suspicion after the convention that
this phrase justifies any possible actions by Congress.  Madison explains the
origin of the phrase in III, 483-94.

Gerry was concerned with the wording of the first paragraph of the report
which granted the new legislature the *power* to "fulfill the engagements
entered into by Congress"(II, 355) but not the *obligation* to fulfill those
engagements.  This would worry the public creditors of the US government.

Sherman assured Gerry that it meant no more nor less than the current powers
and obligations of Congress.

Ellsworth moved to postpone the consideration of the report which was passed
in the affirmative without debate.

Article VII, Section 3 [regarding direct taxation and census] resumed from
yesterday.

Dickinson moved to postpone this section to reconsider Article IV, Section 4
to limit the number of representatives allowed to the large states.  Without
the limit the small states would be reduced to insignificance, and the slave
trade would be encouraged.

Sherman agreed with the reconsideration but did not see the point of
postponing the current motion.  Dickinson then withdrew his motion.

The section in question was then agreed to.

Sherman moved, seconded by G. Morris, to append the following to the section:

> and all accounts of supplies furnished, services performed, and monies
> advanced by the several states to the United States, or by the US to the
> several states shall be adjusted by the same rule(II, 356)

Gorham did not think it needed to be explicitly stated.  The legislature will
do what is right.  Congress has the power and are exercising it.

Sherman said that the rule did need to be explicitly stated or it would not
exist under the new system.

Ellsworth stated that though the contracts of Congress would be binding, a
rule should be provided to execute them.

Sherman withdrew his motion to take up a motion by Williamson instead, to
append:

> By this rule the several quotas of the states shall be determined in
> settling the expenses of the late war.(II, 357)

Carroll thought this would make the unanimous consent of the new constitution
by the states difficult, and Williamson's motion was postponed.

Mason called for the taking up of Article VI, Section 12 which had been
postponed at the beginning of August 15.  He wanted to make a decision on
money bills before continuing.

Gerry took back up his motion of yesterday regarding his amendment to Article
VII, Section 3.  Since the principle acts of government would occur in that
period, the states ought to pay in proportion to their share in them.

Ellsworth thought it was unjust.  The ratio between representatives and
inhabitants is not accurate enough.  A state may only have one representative
but ought to pay one-and-one-half part or some other fraction.  He proposed to
amend the motion by appending "subject to a final liquidation by the foregoing
rule when a census shall have been taken."(II, 358)

Madison clarified that the number of representatives was only conjectural and
temporary until the first census.

Read stated that the requisitions of Congress had been adjusted based on local
and temporary circumstances.  [Implying that there was no hard-set rule that
taxation must match representation.]

Williamson opposed Gerry's motion.

Langdon pointed out that New Hampshire had no delegates present when her
representation was decided.  If she had been given more than her share, he no
longer wanted them.

Butler felt Gerry's motion was founded on "reason and equity."(II, 358)

Ellsworth's amendment to Gerry's motion was agreed to unanimously.

King thought the power of taxation granted to the legislature made Gerry's
motion unnecessary.

On Gerry's motion, it was lost.

On Mason's request to take back up Article VI, Section 12, it was lost.

L. Martin thought direct taxation should be used only when absolutely
necessary.  The states will be the best judges of how to collect it.  He
moved, seconded by McHenry, to append the following to the section:

> And whenever the legislature of the US shall find it necessary that revenue
> should be raised by direct taxation, having apportioned the same, according
> to the above rule on the several states, requisitions shall be made of the
> respective states to pay into the continental treasury their respective
> quotas within a time in the said requisitions specified; and in case any of
> the states failing to comply with such requisitions, then and then only to
> devise and pass acts directing the mode, and authorizing the collection of
> the same.(II, 359)

On Martin's motion, it was lost without debate.

L. Martin in his *Genuine Information* says that without this amendment, the
general government has "the dangerous and oppressive power in the general
government of imposing direct taxes on the inhabitants"(III, 205).  He also
worries that the method of indirect taxation -- uniformly taxing imposts, etc.
-- will fall harder on some states than others.(III, 205)

Article VII, Section 4 taken up:

> No tax or duty shall be laid by the Legislature on articles exported from
> any State; nor on the migration or importation of such persons as the
> several States shall think proper to admit; nor shall such migration or
> importation be prohibited.

[On the euphemism "migration or importation of such persons" meaning "slave
trade", Wilson says the language was chosen because it had been adopted by 11
out of 13 states in 1783.(III, 160)  L. Martin in his *Genuine Information*
says, "the same reasons which caused them to strike out the word 'national'
and not admit the word 'stamps', influenced them here to guard against the
word 'slaves'.  They anxiously sought to avoid the admission of expressions
which might be odious in the ears of Americans, although they were willing to
admit into their system those things which the expressions signified."(III,
210)  Madison says, "Hence the descriptive phrase 'migration or importation of
persons'; the term migration allowing those who were scrupulous of
acknowledging expressly a property in human beings, to view *imported* persons
as a species of emigrants".(III, 436-7)]

Langdon believed by his reading that the states will still be allowed to tax
exports.  Therefore, e.g., New Hampshire will be taxed by the states exporting
her produce.  The section was suggested because the southern states feared
that the northern states will oppress their trade.  This can be better guarded
against if the legislature requires two-thirds or three-fourths to pass such
bills.

Ellsworth thought it good as it stood.  The power of the general government to
regulate interstate trade will protect the states from one another in this
case.  If it does not, then the non-exporting states will begin exporting on
their own, and the effort will defeat itself.  There are good reasons for not
wanting the general government to tax exports:
1. It will discourage industry.
2. Given the different produce of the states, there would be no way to
   uniformly tax.  Tobacco, rice, and indigo would be the easiest to tax, but
   of course this would be unjust to the southern states.
3. It would engender jealousies.

Williamson said that even thought Virginia had taxed tobacco from North
Carolina, he would not agree to this power.

G. Morris supported the power.  There is a good argument that exporting states
will tax their non-exporting neighbors, and the non-exporting states will not
have the large trading port to directly export themselves.  There are good
reasons to have the power to tax imports such as revenue and laying trade
embargoes during war.  Export taxes could also encourage industry by taxing
raw materials more than manufactured goods.

Butler believed that a power of export taxes in the general government would
be too alarming to the "staple states"(II, 360).

Langdon suggested a prohibition on states taxing the produce of other states
that are shipped from their ports.

Dickinson felt that no matter how inconvenient the power might be at present,
it is more dangerous to forever withhold the power from the legislature.  It
would be better to exempt particular goods from the power.

Sherman asserted that the states will never give up all power over trade.  An
enumeration of exemptions would be difficult and improper.

Madison made the same arguments as Dickinson and G. Morris.  Most agreed that
the revenue of the general government would principally be drawn from trade --
that includes imports and exports.  Some method of taxing both could result in
relative uniformity of taxation among the states.

[In 1828 Madison wrote, "That the encouragement of manufactures was an object
of the power to regulate trade, is proved by the use made of the power for
that object, in the first session of the first Congress under the
Constitution".(III, 477)  See also Appendix A, CCCXC.]

Ellsworth did not agree that embargoes can only be effected by this power.

McHenry concurred and said he assumed embargoes were implied by the power of
war.

Wilson stated that Pennsylvania was an exporter for all of her neighbors.  In
favoring the power over exports, he was setting aside the interest of his
state for the general interest.  The argument against the power only had
weight if the general government were compelled -- rather than *authorized* --
to exercise the power.  The government will only have half a regulation of
trade.  The US will be able to get more beneficial commercial treaties with a
power over exports than over imports alone.

Gerry worried that the power would be used to oppress and extort the states.
"We have given it more power already than we know how will be exercised."(II,
362)

Fitzsimons would be against an immediate tax on exports but supported the
power in time -- when the US became a manufacturing country.

Mason stated that if he were for making the states mere corporations, he would
be for the export power.  An interested majority will always oppress a
minority.  This is the case between the 8 northern states and the 5 southern
states, who will bear the burden of an export tax.  Imports are similar
throughout the states, but exports are very different.  The experiment of
taxing tobacco had been tried in Virginia and failed.

Clymer countered that every state might suspect that their particular exports
would be the ones to bear the burdens of a tax.  He moved to qualify the power
to be restrained by regulations of trade by adding "for the purpose of
revenue" after "duty".

On Clymer's motion, it was lost.

Madison moved, seconded by Wilson, to add the words "unless by consent of
two-thirds of the legislature".  He thought this requirement to tax exports
was better than an outright prohibition.

On Madison's motion, it was lost.

On the first clause of the section, it passed in the affirmative.

L. Martin proposed to amend the section to allow either a prohibition or
taxation of the slave trade.  First, the three-fifths rule of representation
encourages the trade.  Second, slavery was a domestic weakness that the other
states would be bound to protect.  Third, to constitutionally protect the
institution of slavery would be inconsistent with the principles of the
revolution and dishonorable to the American character.

Rutledge did not believe this section encouraged the importation of slaves nor
did he fear insurrections.  He would readily exempt the other states from
coming to the aid of the southern states on the condition of a slave uprising.
"Religion and humanity had nothing to do with this question -- Interest alone
is the governing principle with nations".(II, 364)  The southern states will
not be members of the union unless this institution is protected.  It is in
the north's best interest as well to increase the slaves because it will
increase the commodities they will carry.

Ellsworth wished to leave the clause as it stood.  The interests of the states
belong to themselves, being the best judges, to decide.  What enriches a part
will enrich the whole.  The confederation does not care on this point and
neither should the new government.

Pinckney said that South Carolina will never agree to the plan if it prohibits
the slave trade.  The state has watched every proposed extension of power
jealously that it did not interfere with the importation of slaves.

The committee adjourned.

# August 22, Wednesday

Article VII, Section 4 (last two clauses) resumed.

Sherman advised that, even though he disapproved of slavery, it would be best
to leave the rest of the section as is.  It would be better to have as few
objections as possible to the new scheme of government.  The abolition of
slavery will continue state-by-state and will eventually end slavery in the
union on its own.

Mason viewed the slave trade as a dangerous and infernal practice.  Had the
British been wiser they would have instigated uprisings during the revolution.
The histories of the Greece and Sicilian slave insurrections should be a
warning.  Maryland and Virginia have ended the slave trade, but it continues
in South Carolina and Georgia.  Slavery will spread to the western territories
if it is not halted throughout the union.  It prevents the immigration of
whites who enrich the country.  It turns slave masters into petty tyrants.
Since nations cannot be judged by heaven in the next life, providence will
punish our national sin with national calamities in this life.  The states, it
is true, currently have a right to import slaves, but as happens with many
other rights in the new government, they should be given up.  The general
government must have the power to prevent the increase of slavery.

Ellsworth argued that if the reasons for stopping the trade are moral, then we
must also free the slaves already in the country.  As population increases
poor white laborers will be preferred over slaves.  Slavery will eventually
die out on its own.  One can even say that the danger of domestic insurrection
will be an incentive to treat slaves kindly.

Pinckney argued that slavery was justified by the examples of all other
countries in the world.  Greece, Rome, and other ancient empires practiced it,
and England, France, and Holland give it sanction now.  In all ages half of
humanity has been slaves.  The states have the exclusive right to stop
importation on their own -- Pinckney, himself, would vote for it.  Any attempt
to take the right from the states will create opposition to the plan in the
southern states.

In the South Carolina House of Representatives, Pinckney would defend his
position:  "[W]hile there remained one acre of swampland uncleared of South
Carolina, I would raise my voice against restricting the importation of
negroes [...] the flat swampy situation of our country, obliges us to
cultivate our land with negroes, and that without them South Carolina would be
a desert waste."(III, 254)

C. C. Pinckney stated that an exclusion of this clause should be considered an
exclusion of South Carolina from the union.  The signatures of every delegate
from South Carolina on the plan will not get it adopted in that case.
Virginia has more than enough slaves, but South Carolina and Georgia need
more.  If the slave trade is halted, then Virginia will have a monopoly on the
provision of slaves.  The slave trade is good for the whole union.  The more
slaves, the produce that can be generated and the more consumption and the
more revenue for the public treasury.  He would allow for slaves to be taxed
as other imports however.

Baldwin asserted that an "attempt to abridge one of her favorite
prerogatives"(II, 372) would confirm to Georgia that the general government
only existed for the interests of the central states.  If left alone, Georgia
will probably stop the evil herself.

Wilson observed that if South Carolina and Georgia really intended to prohibit
the import of slaves in time, they would never refuse to unite because the
importation would be prohibited.  As the section now stands, slaves are the
only import that cannot be taxed.  This is reason enough to change it.

Gerry believed that while the general government should not legislate the
conduct of slaves in the states, it should not sanction it either.

Dickinson did not think the power was one the states ought to have.  The
question is whether or not the importation of slaves will promote or impede
the happiness of the union, and this can only be answered by the national
government.

Williamson thought the southern states could not unite if the clause was
struck out.  It is wrong to force a state to do anything which it disagrees to
and is unnecessary.

King thought that if two states would not unite with the clause struck out,
then other states might not unite with it preserved.  The northern and eastern
states would certainly have a problem with slaves being the *only* import that
cannot be taxed.

Langdon believed the general government must have the power because the states
cannot be trusted to end it themselves, notwithstanding what their delegates
have said.

C. C. Pinckney candidly declared that he did not believe South Carolina would
stop her imports completely but only occasionally as she now does.  He moved
that slaves be taxed equal to other imports which would remove one objection.

Rutledge seconded the motion.  He said that the southern states would never be
foolish enough to give up so important an interest.

G. Morris wished the amendment to be committed along with taxes on exports and
a navigation act.  The northern and southern states could see this as a
compromise.

Butler would never agree to tax exports.

Sherman said it was better to allow the southern states to import slaves than
to unite without them.  He thought the tax was worse because it implied the
slaves were *property*.  He admitted that if the power to prohibit the trade
was given to the general government it would be exercised.  Were he in the
position, he would find it his duty to exercise the power.

[Madison concurs with Sherman in the Virginia Ratifying Convention:  "Great as
the evil [the slave trade] is, a dismemberment of the union would be
worse."(III, 325)  He goes on to worry that South Carolina and Georgia might
have solicited the aid of foreign powers otherwise.]

Read agreed with G. Morris.

Sherman stated that the clause concerning taxes on exports had already been
agreed to and therefore could not be committed.

Randolph supported committing in the hopes some middle ground might be found.
He would sooner lose the whole constitution than commit to the clause as it
stood.  On the one hand, the Quakers and Methodists and many others without
slaves would revolt.  On the other, two states would be lost to the union.

On the question of committing the rest of Section 4 and Section 5 of Article
VII, it passed in the affirmative.

Pinckney and Langdon moved to commit Section 6 (navigation acts) as well.

Gorham did not see the propriety and stated that the eastern states' only
motivations for union were commercial -- they could defend themselves, they
did not fear foreign invasion, and they did not need the aid of the southern
states.

Wilson wished for commitment in the hopes it would reduce the proportion of
votes required.

Ellsworth worried that they were losing the moderate and middle ground.  The
two states we are risking to lose might be the first of many to form several
confederations and probably not without bloodshed.

On the question to commit Section 6, it passed in the affirmative.  The
committee appointed for these sections were Langdon, King, Johnson,
Livingston, Clymer, Dickinson, L. Martin, Madison, Williamson, C. C. Pinckney,
and Baldwin.

Rutledge then reported from the committee who had taken into consideration the
propositions of Madison and Pinckney(II, 366-7):  [**NOTE** Article VI,
Section 1 in the original report has become Section 2 in this report.]
* Appended to Article VII, Section 1, Clause 1:  "for payment of the debts and
  necessary expenses of the United States -- provided that no law for raising
  any branch of revenue, except what may be specially appropriated for the
  payment of interest on debts or loans shall continue in force for more than
  ______ years."  [Where/When was the new Section 1 proposed???  What is it???]
* Appended to Article VII, Section 2, Clause 2:  regulate commerce with
  Indians
* Appended to Article VII, Section 2, Clause 16:  to secure "the common
  property and general interests and welfare" when it doesn't interfere with
  the jurisdiction of the states
* Article X, Section 1:  qualifications for the President -- 35yo, citizen,
  inhabitant for 21 years
* New Article X, Section 3:  provide for a cabinet similar to that proposed by
  G. Morris on August 20
* Appended to Article XI, Section 2:  judges impeachable by house, triable by
  senate
* Insert into Article XI, Section 3:  jurisdiction of the Supreme Court
  extends to cases between the US and a state and the US and an individual

Gerry and McHenry moved that "the legislature shall pass no bill of attainder
[declare a person guilty without trial] or any ex post facto [retroactive]
law".(II, 375)

Gerry thought them necessary.  The number of national legislators being fewer,
they were less to be trusted.

G. Morris thought the prohibition of retroactive laws were unnecessary but of
bills of attainder essential.

Ellsworth said it was superfluous to prohibit retroactive laws because they
went against common law.

Wilson believed that even mentioning retroactive laws in the constitution
would make people believe they didn't know the first thing about the
principles of legislation.

On the first clause relating to bills of attainder, it passed unanimously in
the affirmative.  The second clause relating to ex post facto laws was taken
up.

Carroll said that experience told them the explicit prohibition was necessary.
The state legislatures had passed retroactive laws, and they had taken effect.

Wilson countered that if the prohibitions in the state constitutions had not
prevented them, it would be useless to insert them into this constitution.

Williamson noted that the prohibition exists in the North Carolina
constitution, and even though it was no prevention to passing such laws
anyway, the judges can still void the law.

"Johnson thought the clause was unnecessary and implied an improper suspicion
of the national legislature."(II, 376)

Rutledge in favor of the clause.

McHenry writes in his notes:  "To say that the legislature shall not pass an
ex post facto law is the same as to declare they shall not do a thing contrary
to common sense -- that they shall not cause that to be a crime which is no
crime".(II, 379)

On the second clause relating to ex post facto laws, it passed in the
affirmative.

The report delivered on August 21 was taken up, beginning with the first
clause:

> The Legislature of the US shall have power to fulfill the engagements which
> have been entered into by Congress(II, 377)

Ellsworth argued it was unnecessary.  The US, regardless of the agents,
entered into the agreements; therefore, the new agents will be bound to the
same agreements.

Randolph thought it necessary.  Unless the new government is explicitly given
the same authority as the old, it will have no authority on the matter.

Madison agreed with Randolph.  He referenced the attempts by debtors of
British subjects to try to get out of their debts by asserting that the old
government had been dissolved at the end of the war.

Gerry concurred with Madison and Randolph.

G. Morris moved to replace with "...shall discharge the debts and fulfill...".

It was moved to amend the motion by replacing "discharge the debts" with
"liquidate the claims" which was lost.

The motion of Morris was agreed to unanimously.

It was moved and seconded to strike out the following words from the second
clause of the report:

> and the authority of training the militia according to the discipline
> prescribed by the United States(II, 377)

Before a question was taken, the committee adjourned.

# August 23, Thursday

The convention took back up with the question yesterday regarding the
authority of the militia:

> To make laws for organizing, arming, and disciplining the militia, and for
> governing such parts of them as may be employed in the service of the US
> reserving to the states respectively, the appointment of the officers, and
> authority of training the militia according to the discipline described.

Sherman moved to strike out the clause from yesterday, "and the authority of
training, etc."  It was unnecessary because the states will have the authority
as long as they do not give it up.

Ellsworth felt it was better to keep it in.  The same logic would apply to the
bit about assigning officers.  He worried that the term "discipline" was so
vast as to include all power.

King explained what the terms meant:
* "organizing": proportioning the officers and men
* "arming": specifying the kind, size, and caliber of arms
* "disciplining": setting the rules for manual exercise, etc.

Sherman withdrew his motion.

Gerry would as soon disarm the citizens of Massachusetts as give the command
of the state militia to the general legislature.  It would be despotism.

Madison observed, as explained by King, that "arming" did not refer to
furnishing arms nor did "disciplining" refer to punishment.

King stated that those meanings were included as well.  "Arming" implied
authority to regulate *how* the militia was furnished.

Dayton moved to postpone the current question to take up a reworded version of
the current question which specified that only a part of the militia would be
in the control of the general government for a time.

On the question to postpone, it was lost.

Ellsworth and Sherman moved to postpone the second clause in favor of a motion
that left the organization of the militia to the general government but the
execution to the states.

Langdon did not understand the animosity some men saw between the general and
state governments.  They are not enemies.  They are different institutions
which both exist for the good of their citizens.  In transferring power from
one to the other, I merely move from my left hand what it cannot use as well
and place into my right.

Gerry thought it was more like taking form the good hand and putting into the
bad.  Does anyone think their liberty will be safer in the hands of less than
a hundred men from all over the continent than in the hands of two or three
hundred from the same state?

Dayton disagreed with the effort to make the militias uniform.  Different
equipment and tactics would be necessary in different states.

C. C. Pinckney preferred the wording of the committee.

Madison stated that the concern was over the discipline of the militia.  The
states had neglected this duty almost as much as their requisitions.  The
militia is no better in the hands of the states than if each county was in
charge of disciplining their part of the militia.  The militias are a national
concern and ought to be controlled by the national government.

L. Martin didn't believe the states would ever give up the militias.  Even if
they did, the general government would be no better at running them than the
states.

Randolph was for limiting the general government wherever there was danger,
but there is none here.  The power cannot be abused unless the whole mass is
corrupted.  The state legislatures everywhere neglect the discipline of the
militias, being too concerned with courting popularity.  Leaving the states
the power to appoint officers negates every apprehension of abuse.

On Ellsworth's motion, it was lost.

A motion was made to recommit the second clause, but it was lost.

To agree to the first part of the clause, it passed in the affirmative.

Madison moved to amend the second part to say "the appointment of the officers
*under the rank of general officers*".

Sherman said that if the people are so far asleep as to allow the general
government to choose their generals, then every man of discernment would rouse
the alarm.

Gerry felt that had they abolished the state governments, established a king,
and a proper senate, then it would make sense to consolidate this power in the
general government.  But isn't this power inconsistent with the existence of
the state governments?  We are pushing this experiment too far, and we are
risking civil war.

Madison asserted that the greatest danger of our situation is a disunion of
the states.  To prevent that we must grant sufficient powers to the general
government.  And since a standing army is the greatest danger to liberty, it
must be prevented with a provision for a good militia.

In the Virginia Ratifying Convention, Randolph concurs that militias are
safer than standing armies:  "With respect to a standing army, I believe there
was not a member in the federal convention who did not feel indignation at
such an institution."(III, 319)  In hindsight in 1815, G. Morris writes, "The
danger we chiefly meant to provide against was, the hazarding of the national
safety by a reliance on that expensive and inefficient force [-- the standing
army].  An overweening vanity leads the fond many, each man against the
conviction of his own heart, to believe or affect to believe, that a militia
can beat veteran troops in the open field and even play of battle. [...] Those
[with experience in the revolutionary war] knew well that to rely on militia
was to lean on a broken reed. [...] to rely on undisciplined, ill-officered
men, though each were individually as brave as Caesar, to resist the
well-directed impulse of veterans, is to act in defiance of reason and
experience."(III, 420-1)

On Madison's motion, it was lost.

On the appointment of all officers by the state, it passed in the affirmative
unanimously.

On the clause that the US would prescribe the discipline of the militia, it
passed in the affirmative.

Article VII, Section 7:

> The United States shall not grant any title of Nobility.

agreed to without debate.

Pinckney proposed an additional section to Article VII:

> No person holding any office of profit or trust under the US shall without
> the consent of the legislature accept of any present, emolument, office, or
> title of any kind whatever from any king, prince, or foreign state.

in order to prevent foreign influence of officers of the US which passed
unanimously in the affirmative.

Rutledge moved to amend Article VIII to read:

> This Constitution and the laws of the US made in pursuance thereof, and all
> treaties made under the authority of the US shall be the supreme law of the
> several states and of their citizens and inhabitants; and the judges in the
> several states shall be bound thereby in their decisions, any thing in the
> constitutions or laws of the several states to the contrary notwithstanding.

which was agreed to without debate.

Article IX, Section 1 taken up:

> The Senate of the United States shall have power to make treaties, and to
> appoint Ambassadors, and Judges of the Supreme Court.

G. Morris thought the senate was too numerous, subject to cabal, and without
responsibility for the power.  It is especially dangerous to have the body
which can try judges of the Supreme Court also be the one to appoint them.

Wilson agreed.

The article was waved [postponed or struck out???].

Article VII, Section 1, clause 18 resumed:

> To call forth the aid of the militia, in order to execute the laws of the
> Union, enforce treaties, suppress insurrections, and repel invasions;

G. Morris moved to strike out "enforce treaties" as superfluous since treaties
were laws.  It was agreed to without debate.

G. Morris moved to alter the wording to "To provide for calling forth the
militia to execute the laws of the Union, suppress insurrections, and repel
invasions" which passed in the affirmative.

On the question to agree to the clause as amended, it passed unanimously in
the affirmative.

Pinckney moved, seconded by Broom, to give Congress the power to veto state
laws with a two-thirds majority.  Now that there was an equality of suffrage
in the senate, there was no danger from the larger states.  [In 1818 in a
letter to John Quincy Adams, Pinckney says he changed his opinion after the
convention on this point.(III, 428)]

Sherman said it was unnecessary since the US laws are paramount to state laws
in the new system.

Madison proposed that it be committed.  He agreed with the principle but
thought some modification might be appropriate.

Mason wondered aloud how it would be implemented.  Would every bill for a new
road or bridge have to be sent to the national legislature for approval?  Will
the legislature sit constantly to review and revise state laws?  He was not an
enemy of the plan, but he wished for explanations of the obvious objections.

Williamson thought it unnecessary and a revival of the question was a waste of
time.

Wilson considered this central to the defense of the general government from
the states.  It is better to veto an improper bill before it becomes law than
to void it once passed.

Rutledge emphasized that this would and ought to prevent the constitution from
being accepted.  Are the states to be mere corporations shackled to the
national government?

Ellsworth observed that the power would require either:

1. all state laws to be sent to the national legislature for approval
2. OR state laws to be repealed by the same
3. OR the state governors to be appointed by the national government and have
   veto power over their laws

If the latter, then it ought to be stated explicitly.

Pinckney thought the latter would be the best solution.

G. Morris did not agree with the proposition but supported committing it for
further consideration.

Langdon favored the proposition.

On the question to commit the motion, it was lost.

Pinckney then withdrew the proposition.

The first clause of Article VII, Section 1 was amended to read "...legislature
*shall* lay and collect...".

Butler worried that it would compel payment to both "the Blood-suckers who had
speculated on the distresses of others" and "those who had fought and bled for
their country."(II, 392)  He gave notice that he would tomorrow call for a
reconsideration to distinguish between those classes of people.

Article IX, Section 1 resumed.

Mason writes in his notes (but does not speak) that since treaties are laws of
the land and can be injurious, it might be necessary to require a two-thirds
majority to pass a treaty.(IV, 209)

Madison argued that the President, being an agent of the general government,
should make treaties since the senate represents the states and the states
alone.

G. Morris moved to amend the section to add "but no treaty shall be binding on
the US which is not ratified by law."(II, 392)

Madison believed that ratifications would be inconvenient for treaties
alliance in war.

Gorham emphasized the disadvantages if negotiations must be previously
ratified, and if not ratified previously, then the diplomats and ministers
would be at a loss on how to proceed.

G. Morris believed the difficulty the qualification would make with respect to
treaties will be an advantage.  First, it will force foreign ministers to come
here rather than the reverse which is what we should want.  Second, the more
difficult it will be to make treaties, then the more valuable each one will
be.

Wilson pointed out that without some oversight, the senate could form treaties
ti line their own pockets.  [...I think...]

Johnson stated that there was some incongruity in saying that the acts of an
ambassador -- fully authorized by one body to represent his government --
should depend on another body for ratification.

Gorham answered Morris by saying that we do not want negotiations here,
especially if the legislature is involved.  The treaty will be influenced by
two or three men who will be corrupted by the foreign ambassador.  "In such a
government as ours, it is necessary to guard against the government itself
being seduced."(II, 393)

Randolph observed that everyone had objected to Morris's motion and moved to
postpone the motion to give everyone time to better think on it.

On the question to postpone, it was lost.

On Morris's motion, it was lost.

They added after "Ambassadors" the words "and other public ministers".

The several clauses of the section were each postponed in turn.

Madison wondered aloud if a distinction might be made between treaties, e.g.
allowing the President to make treaties for temporary alliances while
requiring the approval of the whole legislature to make others.  [There is an
interesting difference of opinion between Washington and Madison about by what
means and under what conditions the President can approve treaties without the
consent of the house of representatives.  Washington seems to have a very
straight-forward interpretation of the constitution on this position, but his
appeal to the journals of the constitutional convention rankles Madison who
makes an argument that we cannot use the intents of the founding fathers at
the convention in our determination of the law.(III, 371-5)  I mention it here
because Madison's argument is worth keeping in mind while considering the
founders' intents myself.]

Article IX, Section 1 was referred to the committee of five.

The committee adjourned.

# August 24, Friday

Livingston reported from the committee of detail regarding Sections 4-6 or
Article VII.  Section 4 (second half):

> The migration or importation of such persons as the several states now
> existing shall think proper to admit, shall not be prohibited by the
> legislature prior to the year 1800, but a tax or duty may be imposed on such
> migration or importation at a rate not exceeding the average of the duties
> laid on imports.(II, 400)

Section 5 to remain as is:

> No capitation tax shall be laid, unless in proportion to the Census herein
> before directed to be taken.

Section 6 (regarding navigation acts) was struck out.

Butler moved, seconded by Pinckney, that Article VII, Section 1, Clause 1 be
reconsidered tomorrow concerning the payment of domestic debts.

Randolph wished it to be reconsidered to provide for state debts.

On the question to reconsider, it passed in the affirmative.

Article IX, Sections 2 & 3 taken up (regarding land disputes between states).

Opposite Section 2, Dickinson in his notes wrote, "A power to terminate all
discensions [sic] with a state, likely to disturb the public peace."(IV, 210)

Rutledge pointed out that these provisions were modeled on those found in the
AoC.  However, under the new system, the Supreme Court will make them
unnecessary.  He moved, seconded by Johnson, to strike them out.

Sherman and Dayton concurred.

Wilson wished to postpone instead of strike out.  It might be useful in cases
where a national judge has an interest in one of the parties.

Gorham agreed with Wilson.  Land disputes might be better handled by these
provisions than in the Supreme Court.

On the question for postponing, it was lost.

On the question for striking out, it passed in the affirmative.

Article X, Section 1:

> The Executive Power of the United States shall be vested in a single person.
> His style shall be, "The President of the United States of America;" and his
> title shall be, "His Excellency." He shall be elected by ballot by the
> Legislature. He shall hold his office during the term of seven years; but
> shall not be elected a second time.

The first three clauses were agreed to without debate.

Rutledge moved to elect the President by "joint ballot", being the most
convenient method.

Sherman objected that it would deprive the states in the senate of their
negative.

Gorham argued that it doesn't matter who the senate represents.  The ultimate
goal should be the public good.  Without a joint ballot, there will be too
much delay and confusion.

Dayton argued that a joint ballot would essentially give the appointment to
the House.  The appointment will be so important that it will ensure the
houses concur on the person.

Carroll moved, seconded by Wilson, to replace "by the legislature" with "by
the people".  It was lost.

Brearly opposed a joint ballot.

Wilson thought it reasonable to give the larger states a larger share in the
appointment.  There was danger of delay from disagreement.  The senate already
has peculiar powers granted to it that would balance the loss of some power by
a joint ballot in this case.

Langdon stated that a general officer ought to be elected by a general (joint)
voice.  Separate votes were used by the New Hampshire legislature, and they
made things unnecessarily difficult.

Wilson worried that, since the President of the US would also be the President
of the Senate, the senate might have interest in delaying the appointment at
times.

Madison pointed out that the advantage of the largest state to the smallest
was only 4 to 1 which isn't so great considering the population difference is
10 to 1.  This is reasonable considering the position is President of the
*people* not the *states*.  The senate already has an advantage since, as
President of the Senate, the President can force three-fourths of the other
house necessary to pass a law.

Mason also agreed in his notes with the joint ballot.(IV, 210)

On the question of inserting "joint", it passed in the affirmative.

Dayton moved, seconded by Brearly, to give each state one vote in the
appointment.  The motion was lost.

Pinckney moved to insert after "legislature" the words "to which election a
majority of the votes of the members present shall be required"(II, 403).
This motion passed in the affirmative.

Read moved that if the votes resulted in a tie, the president of the senate
would break the tie.  The motion was lost.

G. Morris did not approve of election by the legislature.  The influence of
the legislature will lead to tyranny.  The executive will court popularity in
the legislature by sacrificing his rights and then after his office will
return to the legislature to enjoy the fruits of his policy.  He moved,
seconded by Carroll, that the president be chosen by electors chosen by the
people.  The motion was lost.  [5 - 6]

Dayton moved to postpone the consideration of the last two clauses which was
lost.

Broom moved to refer the clause to a committee which was also lost.

On the question of the first part of Morris's motion "to be chosen by
electors", it was lost.

The consideration of the remaining clauses of the section were postponed at
the insistence of the New Jersey deputies.

Article X, Section 2 taken up (regarding the powers and duties of the
president).

G. Morris moved to strike out "he may", making it a *duty* to recommend in
order to avoid offense at his doing so.  The motion passed in the affirmative.

Sherman objected to the wording allowing the President to appoint officers not
otherwise provided for by the Constitution.  The ambiguity could allow the
president to fill offices like generals in the army.  He moved to correct this
by adding "or by law" after "Constitution".

Madison moved to replace "officers" with "to offices" in order to prevent
interpretations that the president may create new offices with his
appointments.  It passed in the affirmative.

On the motion of Sherman, it was lost.

Dickinson moved to reword the clause to say "and shall appoint to all offices
established by this constitution, except in cases herein otherwise provided
for, and to all offices which may hereafter be created by law."(II, 405)

Randolph remarked that the power of appointment was formidable in both the
executive and legislative branches and wondered if the legislature should not
refer some appointments to some state authority.

On Dickinson's motion, it passed in the affirmative.

Dickinson then moved, seconded by Randolph, to append to his last amendment,
"except where by law the appointment shall be vested in the legislatures or
executives of the several states".

Wilson stated that the amendment would make the state legislatures pass no law
creating offices unless they were to appoint them.

Sherman disagreed with "legislatures" being in the motion which was struck out
by the movers.

Before the vote could be put, the committee adjourned.

The convention voted unanimously to change the times of meeting from 10am to
3pm.

# August 25, Saturday

Article VII, Section 1, Clause 1 being reconsidered.

Mason objected to the word "shall".  To fulfill the engagements and discharge
the debts with such certainty was too strong.  It may be impossible to comply
with, but even if it is, it might increase the speculation against original
creditors in distress.  Those who bought stock in an open market are not
included, and it is difficult to draw the line.  The wording of "shall" might
also imply the government would have to accept the old continental paper
money.

Langdon wished to do more than leave creditors in their present state.

Gerry observed that since the public had received the value of the literal
amount, that value ought to be paid to someone.  They ought to have foreseen
what would happen to the poor soldiers.  He would not object to revising the
debt in such a way that those who had been defrauded would be repaid.

Butler meant neither to increase nor diminish the security of the creditors.

Randolph moved to change the clause:  "All debts contracted and engagements
entered into, by or under the authority of Congress shall be as valid against
the US under this constitution as under the confederation."(II, 414)

Johnson thought the clause was unnecessary.  Of course, the debts of the US
under one government would be valid under the next government.  If anything
should be said, it should be a mere declaration like Randolph's.

G. Morris urged the use of the word "shall" as a compliance with the public
faith.  He agreed with Johnson's assessment but preferred the explicit
statement with "shall" because it would create more supporters of the plan.

The motion of Randolph passed in the affirmative.

Sherman moved to append an amendment to the clause explaining that the debts
were for the "common defense and general welfare", but this was deemed
unnecessary.

[Gerry notes before the House of Representatives in 1790 that the above clause
also implies the assumption of the state debts as well.(III, 361)  There are
other statements before the House which appear to be a part of a larger
debate, see II, 412, note 5.]

The report of the committee was taken up regarding the compromise of the
importation of slaves and navigation acts.

Pinckney moved, seconded by Gorham, to replace the year "1800" with "1808".

Madison said, "Twenty years will produce all the mischief that can be
apprehended from the liberty to import slaves.  So long a term will be more
dishonorable to the national character than to say nothing about it in the
constitution."(II, 415)

Wilson in the Pennsylvania Ratifying Convention stated, "I consider this as
laying the foundation for banishing slavery out of this country; and though
the period is more distant than I could wish, yet it will produce the same
kind, gradual change which was pursued in Pennsylvania."(III, 161)

The motion passed in the affirmative.

G. Morris moved to make the clause read "the importation of slaves into North
Carolina, South Carolina, and Georgia"(II, 415) shall not be prohibited.  It
would avoid an ambiguity with respect to naturalization and would imply that
this part of the constitution was a compliance with those states.  However, if
the language offends the delegates from those states, he would withdraw it.

Mason approved of specifying the term "slaves" but worried that calling out
the states explicitly would offend the people of those states.

Sherman and Clymer preferred the euphemisms as the explicit terms were not
pleasing to some people.

Williamson was against slavery "but thought it more in favor of humanity, from
a view of all circumstances"(II, 415-6) to allow those states to have slaves
in the union than to reject them from the union.

G. Morris then withdrew his motion.

Dickinson wished to confine the clause to those states who had not already
outlawed the importation of slaves.  He moved to amend the clause to read:
"The importation of slaves into such of the states as shall permit the same
shall not be prohibited by the legislature of the US until the year 1808."(II,
416)

It was lost unanimously.

Baldwin before the House of Representatives in 1798 claims that the word
"slave" was not used because "Congress by none of their acts had ever
acknowledged the existence of such a condition".(III, 376)  Wilson in the
Pennsylvania Ratifying Convention says much the same thing, that the wording
was chosen because it was the wording that was already used by Congress.(III,
160)  Madison says in a letter of 1819 that the term "migration" allowed those
who didn't want to explicitly refer to human beings as property "to view
*imported* persons as a species of" immigrants.(III, 437)  Mason characterizes
the compromise over this matter as "a compromise between the eastern and two
southern states, to permit the latter to continue the importation of slaves
for twenty odd years; a more favorable object to them than the liberty and
happiness of the people."(III, 305)

The first part of the report was then agreed to as amended.

Baldwin in order to more explicitly define "the average duty" moved to replace
"average on duties of imports" with "common impost on articles not enumerated"
which was agreed to unanimously.

Sherman against the tax since it acknowledged that men were property.

King, C. C. Pinckney, and Langdon considered the tax necessary to allow the
importation.

Mason believed not to tax would be equivalent to incentivizing the importation
of slaves.

Gorham stated that it was better to think of it not as implying slaves are
property but as a means of discouraging their import.

G. Morris remarked that given the ambiguity of the phrases, the clause as it
now stands implies that free immigrants might be taxed.

Sherman countered Gorham by saying that the smallness of the tax implied that
revenue -- not discouragement -- was the object.

Madison did not agree with admitting into the constitution the idea that men
are property.  Taxing does not apply since men are not like other merchandise,
e.g. consumed.

Mason answered G. Morris that the wording is necessary to prevent the
introduction of convicts.  [What???]

The clause was finally amended and agreed to read:  "but a tax or duty may be
imposed on such importation not exceeding $10 for each person".(II, 417)

Article VII, Section 5 was agreed to.

Article VII, Section 6 was postponed.

Madison moved, seconded by G. Morris, to insert into Article VIII after "all
treaties made" the words "or which shall be made".  This was meant to
emphasize and remove doubt of the force of preexisting treaties.  It was
agreed to without debate.

Carroll and L. Martin expressed the fear of their constituents that under the
power of regulating trade, the general legislature would have the power to
favor particular ports for imports and exports.  They gave the example of
ships bound for Baltimore being forced to enter and clear at Norfolk.  They
moved a provision that the general legislature could not demand ships bound
for one port to enter a different port in another state.

[L. Martin in his *Genuine Information* is alarmed that the legislature might
still force ships to go to an inconvenient port within a state which would
discourage importation to that port and have the same effect as the one they
are trying to prevent.(III, 214)]

Gorham thought the precaution was unnecessary and possible to evade.(II, 418)
Regarding the specific example, it might be better to stop at Norfolk for a
better collection of the revenue.(II, 420)

McHenry and C. C. Pinckney made a couple of propositions.  The first was a
more explicit version of Carroll's and Martin's.  The second was "All duties,
imposts, and excises, prohibitions or restraints laid or made by the
legislature of the US shall be uniform and equal throughout the US."(II, 418)

[Hugh Williamson in the House in 1792 clarified that these provisions were
meant to prevent the possibility of one part of the union gaining advantages
at the expense of another part of the union.(III, 365)]

The above propositions were referred to a committee consisting of a member of
each state.  [See II, 418 for list of delegates.]  [This was a motion by
McHenry II, 420.]

On the question of Dickinson's motion at the end of business yesterday to
amend Article X, Section 2, it was lost.

In the same section, after "ambassadors" was inserted "and other public
ministers".

Mason notes beside this line that "this was not the idea of the
convention".(IV, 210)

G. Morris moved, seconded by Broom, to strike out "and may correspond with the
supreme Executives of the several States" as unnecessary and implying that the
President could not correspond with others.  It passed in the affirmative.

Sherman moved to replace "power to grant reprieves and pardons" with "power to
grant reprieves until the ensuing session of the Senate, and pardons with the
consent of the Senate".(II, 419)  On this question, it was lost.

After "pardon" was inserted the words "except in cases of impeachment" which
was passed in the affirmative without debate.

On the question to agree to "but this pardon shall not be pleadable in bar",
it was lost.

Pinckney gave notice that he would move that three-fourths of the legislature
would be required to pass a law regarding the regulation of trade or a
navigation act.(II, 420)

The committee adjourned.

Gerry writes to his wife this evening:

> I am exceedingly distressed at the proceedings of the convention, being
> apprehensive and almost sure they will, if not altered materially, lay the
> foundation of a civil war. [...] I never was more sick of anything than I am
> of conventioneering -- had I known what would have happened, nothing would
> have inclined me to come here.(IV, 241-2)

# August 27, Monday

Article X, Section 2 resumed.

L. Martin moved to add after "reprieves and pardons" the words "after
conviction".

Wilson objected because there were times when a pardon might be offered before
conviction in order to obtain the testimony of accomplices.  Martin withdrew
his motion.

Sherman moved to add after "and of the Militia of the several states" the
words "when called into the actual service of the US".  The question passed in
the affirmative.

Mason asks in his notes "whether the president ought to command the army
personally"?(IV, 210)

G. Morris postponed (without debate) the clause regarding impeachment by the
House and conviction by the Supreme Court because the method would be improper
"if the first judge was to be of the privy Council."(II, 427)

G. Morris objected to the president of the senate being the President's
successor and suggested the chief justice instead.

Madison also objected to the succession of the president of the senate.  The
senate might delay appointment while the man with veto power existed in their
body.  During a vacancy the executive powers should be executed by the
Council to the President.

Williamson believed the legislature ought to have the power to provide for
occasional successors and moved that the clause relating to successors be
postponed.

Dickinson seconded.  The standard is too vague.  What is a "disability", and
who is to be the judge of it?

The postponement was agreed to without debate.

Mason and Madison moved to add to the oath of office, "and will to the best of
my judgment and power preserve, protect, and defend the Constitution of the
US."(II, 427)  On the question, it passed in the affirmative.

Article XI, Section 1 taken up:

> The Judicial Power of the United States shall be vested in one Supreme
> Court, and in such inferior Courts as shall, when necessary, from time to
> time, be constituted by the Legislature of the United States.

Johnson suggested that judicial power ought to extend to equity as well as law
and moved to insert after the first "United States" the words "both in equity
and law".

Read objected to investing the same court with both powers.

On the question, it passed in the affirmative.

On agreeing to Section 1 as amended, it passed in the affirmative.

The committee agreed to the section as amended.  Article XI, Section 2 taken
up:

> The Judges of the Supreme Court, and of the Inferior Courts, shall hold
> their offices during good behavior. They shall, at stated times, receive for
> their services, a compensation, which shall not be diminished during their
> continuance in office.

Dickinson moved, seconded by Gerry, to insert after "good behavior" the words
"provided that they may be removed by the executive on the application by the
senate and house of representatives."(II, 428)

G. Morris thought it was contradictory to say the judges hold their positions
during good behavior but remove them without a trial.  It is fundamentally
wrong to subject the judges to so arbitrary an authority.

Sherman said it would not be a contradiction or improper as long as it was a
part of the constitutional regulation of the supreme court.  Britain has a
similar statute.

Rutledge stated that as long as the Supreme Court is to judge between the
general and state governments, the motion cannot be allowed.

Wilson stated that the British system was less dangerous than the proposed
system on this count since the House of Lords and Commons rarely agreed on any
point.  "The judges would be in a bad situation if made to depend on every
gust of faction which might prevail in the two branches of our
government."(II, 429)

Randolph opposed the motion as weakening too much the independence of the
judges.

Dickinson did not believe that two bodies formed by different methods and
principles would improperly unite to discharge a judge.

On Dickinson's motion, it was lost.

Madison and McHenry moved to insert "increased or" before "diminished".
Madison in the Virginia Ratifying Convention said that this was necessary
since the judges served during good behavior; they would need an increase of
salary throughout their service, especially if their work/responsibility
increased.(III, 332)

G. Morris opposed it and referenced his previous arguments against it.

Mason opposed the amendment.  He stated that there would be a way to increase
salaries so as to account for changes in the value of metals, and this was the
only argument that had weight.  [I don't understand what he's saying.  II,
429]

C. C. Pinckney the Supreme Court will need men of superior talents who will
require large salaries -- larger than the US can afford before the first
appointments.  It would not look good for new judges to come in with larger
salaries than those with experience.

G. Morris stated that the prevention can be evaded anyway.  A judge need only
resign and then be reappointed at a higher salary.

On the motion, it was lost.

Randolph and Madison then moved to amend it so that increases could not take
effect until three years after the legislature passed them, but this was also
lost.

Article XI, Section 3 taken up -- relating to the jurisdiction of the Supreme
Court.

The clause "to the trial of impeachments of officers of the United States" was
postponed without debate.

Johnson moved to insert "this Constitution and the" before "laws" -- extending
the jurisdiction of the Supreme Court to the expounding of the law.

Madison thought this might go too far.  It ought to be limited to cases of a
judiciary nature.  "The right of expounding the Constitution in cases not of
this nature ought not to be given to that department."(II, 430)

The motion of Johnson was agreed to unanimously.  Madison writes, "it being
generally supposed that the jurisdiction given was constructively limited to
cases of a judiciary nature".(II, 430)

Rutledge moved to strike out "passed by the legislature" and to append at the
end of the same clause the words "and treaties made or which shall be made
under their authority"(II, 431).  These motions passed in the affirmative.

The clause beginning "in cases of impeachment" was postponed.

G. Morris wished to know what was meant by the clause beginning "In all the
other cases before mentioned, it shall be appellate..."  Does this extend to
matters of fact as well as law and Common as well as Civil law?

Wilson said that the committee had meant them to, yes.  That had been their
interpretation of the jurisdiction of the court of appeals.

Dickinson moved to explicitly state after "appellate" the words "both as to
law and fact" which was agreed to without debate.

[L. Martin in *Genuine Information* explains that "as to fact" means that it
will be a trial without a jury.  This obviously worries him in the cases of
criminal law.(III, 220-1)  Madison in the Virginia Ratifying Convention says
that a jury at this level would be impracticable, though he regrets it cannot
be done safely.(III, 332)]

[Randolph in the Virginia Ratifying Convention states, "I object to the
appellate jurisdiction [of the Supreme Court] as the greatest evil in
it."(III, 310)]

Madison and G. Morris moved to replace "the jurisdiction of the supreme court"
with "the judicial power" which was agreed to without debate.

The motion to amend with "in all other cases before mentioned the judicial
power shall be exercised in such manner as the legislature shall direct"(II,
431) was lost.

Mason writes in his notes beside "under such regulations as the Legislature
shall make" the words "a more explicit definition seems necessary here"(IV,
210), but he says nothing.

The last sentence beginning "The Legislature may assign..." was struck out
without debate.

Sherman moved to insert after "between citizens of different states" the words
"between citizens of the same state claiming lands under grants of different
states" (according to a similar provision in the ninth article of the
confederation) which was agreed to without debate.

[Mason in the Virginia Ratifying Convention has several problems with the
judiciary.  He believes the expansive jurisdiction of the Supreme Court will
bring about the destruction of the state governments.(III, 330)  He thinks
that to give them jurisdiction in a dispute between a citizen and his state is
"utterly inconsistent with reason or good policy."(III, 331)]

The committee adjourned.

# August 28, Tuesday

Sherman reported from the committee the following proposition:

> Nor shall any regulation of commerce or revenue give preference to the ports
> of one state over those of another, or oblige vessels bound to or from any
> state to enter clear or pay duties in another and all tonnage, duties,
> imposts, and excises laid by the legislature shall be uniform throughout the
> US.(II, 437)

[Both Madison and the official journals say that this is to "be inserted after
the 4 clause of 7th section"(II, 437).  However, there is no 7th section to
which this fits.  I think they mean it is inserted between the 4th and 5th
section of Article VII.]

The proposition from the report is postponed.

Someone moved to replace "it shall be appellate" with "the supreme court shall
have appellate jurisdiction" in Article XI, Section 3 to avoid the ambiguity
of whether "it" referred to the Supreme Court or the judicial power, which
passed in the affirmative.

Article XI, Section 4 was amended (unanimously) to read:

> The trial of all crimes (except in cases of impeachment) shall be by jury,
> and such trial shall be held in the state where the said crimes shall have
> been committed; but when not committed within any state, then the trial
> shall be at such place or places as the legislature may direct.(II, 438)

[Dickinson wrote that the original version of the section was ambiguous
regarding crimes committed out of the US.(IV, 210)  Such questions probably
lead to the above rewording.]

[Spaight in the North Carolina Ratifying Convention says:

> It was impossible to make any one uniform regulation for all the states
> [regarding trial by jury], or that would include all cases where it would be
> necessary.  [...]  It was therefore left to the legislature to say in what
> cases it should be used; and as the trial by jury is in full force in the
> state courts, we have the fullest security.(III, 349)
]

Pinckney moved to secure habeas corpus "that it should not be suspended but on
the most urgent occasions, and then only for a limited time not exceeding
twelve months."(II, 438)

Rutledge did not believe that there would ever be an occasion to suspend
habeas corpus at the same time throughout all the states.

G. Morris moved "the writ of habeas corpus shall not be suspended, unless
where in cases of rebellion or invasion the public safety may require it."(II,
438)

Wilson doubted if a suspension could be necessary as the discretion exists
with the judges on whether to keep in jail or admit to bail.

The first part of G. Morris's motion until "unless" was agreed to.

Article XI, Section 5:

> Judgment, in cases of Impeachment, shall not extend further than to removal
> from office, and disqualification to hold and enjoy any office of honor,
> trust or profit, under the United States. But the party convicted shall,
> nevertheless be liable and subject to indictment, trial, judgment and
> punishment according to law.

was agreed to without debate.

[Opposite this article, Mason wrote in his notes that the method of impeaching
judges and all great offices of the government needs to be established.(IV,
210)]

Article XII taken up:

> No State shall coin money; nor grant letters of marque and reprisal; nor
> enter into any Treaty, alliance, or confederation; nor grant any title of
> Nobility.

Wilson and Sherman moved to insert after "coin money" the words "nor emit
bills of credit, nor make anything but gold and silver coin a tender in
payment of debts"(II, 439).  This made the prohibitions in the first clause of
Article XIII absolute rather than allowable via the legislature.

Gorham thought the prohibition was secure enough as is requiring the
permission of the national legislature.  An absolute prohibition of paper
money would rouse opposition from the partisans.

Sherman thought this crisis was the appropriate time to institute a
prohibition of paper money.  If the consent of the legislature was required,
then the supporters of paper money would do every thing they could to
influence the legislature to allow it.(II, 439)  In a letter presenting the
constitution to the governor of Connecticut, Sherman and Ellsworth said that
the prohibition of paper money was "necessary as a security to commerce, in
which the interest of foreigners, as well as of the citizens of different
states, may be affected."(III, 100)

Davie in the North Carolina Ratifying Convention explains that the
constitution prohibits the future creation of paper money by the states but
does not invalidate the paper money currently in circulation.  There was a
worry that because so much property and contracts relied on the paper money at
this point that to void it all at once would be a cure worse than the
disease.(III, 349-50)

The motion passed in the affirmative.

King moved to add a prohibition against states interfering in private
contracts.

G. Morris thought this went too far.  The Supreme Court is a protection of
contracts within their jurisdiction, and within a state, the state's decisions
must be allowed whatever the mischief among themselves.

Sherman asked Morris, "Why then prohibit bills of credit?"(II, 440)

Wilson favored King's motion.

Madison admitted that there could be inconveniences but the utility of the
prohibition outweighed it.  However, a veto over the state laws would have the
same effect.  The state legislatures would find ways around the prohibition.

Mason agreed with Morris.  Cases where this would be proper may arise that
cannot be foreseen.

Wilson countered the arguments against by asserting that only retrospective
interferences should be prohibited.

Madison thought the prohibition on ex post facto laws already prevented
Wilson's suggestion.

Rutledge moved instead of King's wording to insert "nor pass bills of
attainder nor ex post facto laws"(II, 440) which passed in the affirmative.

Madison moved to insert after "reprisal" the words "nor lay embargoes" because
such acts are unnecessary, impolitic, and unjust.

Sherman thought the states should keep the power "to prevent suffering and
injury to their poor."(II, 440)

Mason thought it would be dangerous.  Sudden embargoes had been necessary
during the revolution.  The national legislature will not sit continuously and
might not act quickly enough.

G. Morris considered the provision unnecessary.  The power of regulating
interstate trade in the general legislature would be enough to prevent
mischief.

On the question, it was lost.

Madison moved, seconded by Williamson, to move the words "nor lay imposts or
duties on imports" from Article XIII to here to make the prohibition absolute.
A majority of states already tax their neighbors, and they could control
enough of the legislature to allow the injury.

Sherman thought the power could be safely left with the state legislature.

Mason argued that certain states would want to use impost duties to encourage
the manufacture of products they had an advantage over -- like hemp in
Virginia.

Madison countered that that method of encouragement would require impost
duties on foreign states as well as other states in the union which would
cause the same problems due to a want of power over interstate commerce.

On the question, it was lost.

Article XII as amended agreed to unanimously.

Article XIII taken up:

> No State, without the consent of the Legislature of the United States, shall
> emit bills of credit, or make any thing but specie a tender in payment of
> debts; nor lay imposts or duties on imports; nor keep troops or ships of war
> in time of peace; nor enter into any agreement or compact with another
> State, or with any foreign power; nor engage in any war, unless it shall be
> actually invaded by enemies, or the danger of invasion be so imminent, as
> not to admit of delay, until the Legislature of the United States can be
> consulted.

King moved to insert after "imports" the words "or exports".  The motion
passed in the affirmative.

Sherman to insert after "exports" the words "nor with such consent but for the
use of the US" so that all duties would be placed in the public treasury.

Madison approved of the motion but worried that they were making the commerce
system too complicated.

G. Morris thought the motion necessary to prevent the Atlantic states from
taxing the western territories and drive their settlers into the hands of the
British.

Clymer thought encouragement of settling the west was suicide on the part of
the original states.  If the states cannot be left to regulate their own
commercial interest without affecting the other states, then it is clear that
they are not meant to compose one nation.

King thought the motion interfered too much with the states.  Revenue was a
power of the general legislature.

On the question, it passed in the affirmative.

Article XIII as amended was then agreed to.  [At the end of his copy of the
article Dickinson wrote, "and then to continue no longer than Congress shall
otherwise determine."(IV, 211)]

[Interesting view of economics during this time in Appendix A, CCCXC
(Madison).  I need to learn more about 18th-century commercial/financial
theory to understand a lot of the debates on these subjects.]

Article XIV was taken up:

> The Citizens of each State shall be entitled to all privileges and
> immunities of citizens in the several States.

Pinckney believed some clause needed to be added to respect the property of
slaves.

Article XIV was agreed to.

Article XV taken up:

> Any person charged with treason, felony or high misdemeanor in any State,
> who shall flee from justice, and shall be found in any other State, shall,
> on demand of the Executive power of the State from which he fled, be
> delivered up and removed to the State having jurisdiction of the offense.

"High misdemeanor" was replaced with "other crime".  There was a concern that
the former was too technical a term and limited the intent of the provision.

Butler and Pinckney moved that slaves should be delivered up similar to
criminals.

Wilson said that this would oblige the executive of the state to do it at
public expense.

Sherman said there is no more reason to include slaves than to demand a state
seize and deliver up a horse.

Butler withdrew the motion to allow time to add a provision separately from
this article.

Article XV as amended was agreed to unanimously.

The committee adjourned.

# August 29, Wednesday

Article XVI taken up:

> Full faith shall be given in each State to the acts of the Legislatures, and
> to the records and judicial proceedings of the Courts and magistrates of
> every other State.

Williamson did not quite understand the meaning of this article and moved to
replace this article with the words of the AoC from which it was derived
(Article IV):

> Full faith and credit shall be given in each of these States to the records,
> acts, and judicial proceedings of the courts and magistrates of every other
> State.

Wilson and Johnson supposed that it meant the judgments (including legislative
acts) of one state should be the ground of actions in others.

Pinckney moved to commit the article with the following proposition:  "To
establish uniform laws upon the subject of bankruptcies, and respecting the
damages arising on the protest of foreign bills of exchange."(II, 447)

Gorham was for agreeing to the article and committing the proposition.

Madison wanted to commit both.  He wished that the legislature might be
authorized to provide for the execution of judgments in other states.  This
can be safely done and is justified by the nature of the union.

Randolph argued that there was no precedent of one nation executing the
judgments of a court from another nation.  He moved for the following
proposition instead:

> Whenever the act of any state, whether legislative, executive, or judiciary
> shall be attested and exemplified under the seal thereof, such attestation
> and exemplification shall be deemed in other states as full proof of the
> existence of that act -- and its operation shall be binding in every other
> state in all cases to which it may relate and which are within the
> cognizance and jurisdiction of the state wherein the said act was done.(II,
> 448)

On the question to commit both the article and Pinckney's motion, it passed in
the affirmative.  Randolph's motion was also committed without debate.

G. Morris moved to commit the following proposition which was also passed in
the affirmative:

> Full faith ought to be given in each state to the public acts, records,
> judicial proceedings of every other state; and the legislature shall by
> general laws determine the proof and effect of such acts, records, and
> proceedings.(II, 448)

Dickinson mentions that Blackstone's
[*Commentaries on the Laws of England*](https://lonang.com/library/reference/blackstone-commentaries-law-england/)
define "ex post facto" only in relation to criminal laws, so further provision
must be made to restrain the same mischief with respect to civil law.

The proposition of the report to strike out Article VII, Section 6 was taken
back up.

Pinckney moved to postpone the report in favor of the following proposition:

> That no act of the legislature for the purpose of regulating the commerce of
> the US with foreign powers, or among the several states, shall be passed
> without the assent of two-thirds of the members of each house.(II, 449)

[Madison says "Martin" seconded it.  Alexander (NC) makes more sense than
Luther in this context since the motion is primarily desired by the southern
states.]

Pinckney explained that there were five distinct commercial interests in the
union based on the products of the individual states.  The different interests
could lead to oppressive regulations.  The regulation of commerce was a
concession by the south who did not currently need commercial protections.

C. C. Pinckney admitted it was in the south's interest to have no commercial
regulation.  However, since the eastern states had protected the importation
of slaves, the southern states should be just as conciliating in allowing
bare majority control over commerce.  Our southern constituents, though
prejudiced against the eastern states, will understand the compromise.

Clymer argued that the different interests between the states were difficult
enough without creating unnecessary restrictions.  The northern states will be
ruined without some power to combat foreign trade regulations.

Sherman argued that the diversity of interests was itself a security against
the majority.

Pinckney replied that the two great interests between the north and south
remained.

G. Morris argued that the navigation acts were necessary and in the south's
ultimate best interest.  By requiring American-made boats manned by American
sailors, preferences for American ships will multiply until they are cheaper
to ship by than any foreign boat.  Shipping is a precarious property in need
of public protection.

Williamson supported the motion and was not shy in admitting that his support
was based on the interests of the constituents of North Carolina.  The motion
itself is probably not necessary.  If northern shipping costs become too
oppressive, then the south will start building its own ships.  However,
southerners are apprehensive on this subject and would appreciate the
precaution.  Besides no useful measure had been lost in Congress for want of
nine votes.

Spaight against the motion.  As Williamson just said, the southerners can
build their own ships.

Butler admitted that the commercial interests of the north and south are as
different as those of Russia and Turkey, but in the interest of conciliation,
he would vote against the motion.

Mason argued that the government must be constructed to obtain "the confidence
and affection of the people".(II, 451)  The southern states are a minority in
both houses.  Do we expect them to deliver themselves bound hand and foot to
the north?

Wilson said that if every particular interest ought to be secured, then we
must insist on unanimity.  The minority will be governed by their interest as
much as the majority, and if one of them must be bound hand and foot, it
should be the former.  The history of Congress shows the difficulty of
requiring nine votes for approval.

Madison said that the south apprehends increased economic costs if navigation
acts and commercial are allowed.  However, our power to defend against foreign
influence on our economic and commercial interests is better secured with a
bare majority than two-thirds.  An abuse of the power is unlikely given the
independence of the senate, the veto of the executive, and the agricultural
interests of the interior and western territories.  The southern states would
also benefit from the security of increased maritime strength and the
increased consumption of their produce.

Rutledge was against the motion.  At worst a navigation act would temporarily
increase costs of shipping on the southern states.  Since we are building a
great empire, we should look to the general long term benefits.  It is
necessary that we secure "the West India trade to this country", and a
navigation act was required to do it.

"Randolph said that there were features so odious in the Constitution as it
now stands, that he doubted whether he should be able to agree to it."(II,
452)  [This is of note because Randolph did not sign the final constitution,
and this is the first time he mentions his doubts about it.]  If Pinckney's
motion failed to pass, it would complete the deformity in the system.  Some
have argued that a bare majority is better protection against foreign
influence than two-thirds.  However, foreign influence would be easier over
the President who could then with his veto force a three-fourths vote.

Gorham wondered why the eastern states would agree to the union if the
government is so fettered to prevent interests that they could pursue on their
own.  The different situations in the northern and middle states will be a
security in favor of the interests of the south.  Foreign ships will never be
altogether excluded as long as they are in treaty with us.

On Pinckney's motion, it was lost.  The report on striking out Section 6
(regarding two-thirds vote for the passage of navigation acts) was agreed to
unanimously.

Butler moved to insert after Article XV the following proposition:

> If any person bound to service or labor in any of the United States shall
> escape into another state, he or she shall not be discharged from such
> service or labor, in consequence of any regulations subsisting in the state
> to which they escape, but shall be delivered up to the person justly
> claiming their service or labor.(II, 453-4)

which was agreed to without debate.

Article XVII was taken up:

> New States lawfully constituted or established within the limits of the
> United States may be admitted, by the Legislature, into this Government; but
> to such admission the consent of two thirds of the members present in each
> House shall be necessary. If a new State shall arise within the limits of
> any of the present States, the consent of the Legislatures of such States
> shall be also necessary to its admission. If the admission be consented to,
> the new States shall be admitted on the same terms with the original States.
> But the Legislature may make conditions with the new States, concerning the
> public debt which shall be then subsisting.

G. Morris moved to strike out the last two sentences of the article.  The
legislature ought not to be bound to admit western states on these terms.

Madison said that the western states neither would nor ought to join a union
which would not have them on equal footing with the original states.

Mason said that if emigration could be halted into the western territories,
then the motion might be proper.  However, they are being settled whether we
want them to or not.  We should treat them with an equality that will make
them friends instead of enemies.

G. Morris clarified that he did not wish to discourage the settlement of the
west -- which was impossible.  He just did not intend to place the power into
their hands.

Sherman was against the motion.

Langdon supported the motion.  Circumstances may one day arise in which it
will not be convenient to admit new states on equal footing.

Williamson also supported the motion.  The existing states (including the
small) are equal under the Confederation, and for that reason have a right to
it in the senate.  New western states will not have such justification.

On the question of striking them out, it passed in the affirmative.

G. Morris moved to replace the current article with the following:

> New states may be admitted by the legislature into this union, but no new
> state shall be erected within the limits of any of the present states
> without the consent of the legislature of such state as well as of the
> general legislature.(II, 455)

[It's worth noting that Morris's wording here and his opinion of the wording
in the final plan is that "Congress [cannot] admit, as a new state, territory
which did not belong to the United States when the Constitution was
made."(III, 404)]

The first clause until "union" was agreed to without debate.

L. Martin opposed requiring the consent of the legislature of the state to
form new states within its borders.  Nothing would alarm the limited states
more than requiring the consent of, e.g., Virginia to establish new states
within her limits.  Shall Vermont be forced to submit to the states claiming
it?  [At this time Vermont was an independent state separate from the
Confederacy.  She was considered to be within the state limits of New York;
although, the jurisdiction of the New York government was not recognized to
extend into Vermont.]  Ditto for the state of Franklin and the western
territories of Virginia.

The motion to substitute was agreed to.  The committee then took up the debate
on the article as amended.

Sherman thought it was unnecessary.  The union cannot dismember a state
without its consent.

Langdon thought, like Martin, that the proposition would excite great
opposition to the plan.

G. Morris thought the limited states would support it since it supports the
idea of dismembering the larger states.

Butler disagreed with Martin.  Nothing but confusion could come out of
dismembering states without the consent of the state governments.

Johnson concurred with Sherman's point but worried that as the clause stood,
Vermont would be subject to New York.  Vermont ought to be compelled to enter
the union separately.

Langdon said his objections were based on the case of Vermont.  If Vermont
does not join the union (and therefore remains exempt from taxes), then it
would injure her neighboring states.

Dickinson disapproved of the article.  It is improper to require the small
states to help secure the extensive territories of the large states.

Wilson stated that when the majority of a state wish to divide, they may do
so.  The general government should not have the power to aid the minority by
dividing a state against its consent.

G. Morris stated that if the forced division of the states is the object of
the new system, then the delegates from those states will soon take their
leave.

The committee adjourned.

Gerry suspects that someone at the convention is intercepting and reading his
letters.(IV, 247)

# August 30, Thursday

Article XVII as amended by G. Morris taken back up.

Carroll moved to remove the consent of the state to its being divided.  He
believed the purpose of the clause was to try to prevent domestic
insurrections, but the western territories belonged to the United States in
common and thus should be up to the general government to decide.  He proposed
a commitment that somewhere in the plan should be an assertion of the
jurisdiction of the general government over the lands ceded to the US in the
Treaty of Peace.

L. Martin seconded the motion for commitment.

Rutledge could not believe that delegates were proposing states would be cut
up without their consent.  Vermont is a special case which will be provided
for.  No one should fear that Virginia and North Carolina will call on the
general government to defend their western lands.

Williamson assured the convention that North Carolina was more than willing to
give up her western lands, but the US should not compel the states to do so.
He was for saying nothing in the constitution and leaving the matter as it
currently is.

Wilson against the commitment.  He would have no objection to leaving the
matter as is.  The power to tear apart a political society without their
consent should alarm everyone.

On the motion for commitment, it was lost.

Sherman moved for a slight rewording to allow the special case of Vermont, but
it was voted against.

Johnson moved to add after "shall be" the words "hereafter formed or" to allow
Vermont to join without the consent of New York which passed in the
affirmative.

G. Morris moved to replace "limits" with "jurisdiction".  Once again this was
to provide for Vermont since she was technically within the borders New York
even though New York's jurisdiction did not extend over her.  This motion also
passed in the affirmative.

L. Martin thought it was unreasonable to force the settlers of the western
territories to remain under their current state governments.  The people of
those regions may become the majority, but the majority of counties and the
power will remain in the east.  They may keep the western lands in subjection
with the protection of the general government to prevent "insurrections".
Martin attacked Wilson's idea of political societies.  When the rights of the
small states were threatened, then political societies were mere phantoms.
Now that the interests of the larger states are threatened, then political
societies are of sacred importance.  The small states should not be forced to
secure the western lands of the larger states.  Martin moved for a rewording
that would allow the general government to create new states out of the
current ones which was lost.(II, 464)  Martin would later argue that if the
general government has the responsibility of defending state governments from
insurrection, then they should have the authority to decide in favor of the
rebellion to erect a new state.(III, 225)

On Morris's substitution as amended, it passed in the affirmative.

Dickinson moved to append a clause regarding the combination of two states
with the consent of the states and the legislature.  It passed in the
affirmative.

Carroll moved to append "Provided nevertheless that nothing in this
Constitution shall be construed to affect the claim of the US to vacant lands
ceded to them by the Treaty of Peace."(II, 465)  [Worth mentioning that this
same wording existed in Martin's last motion.]  This referred not only to
lands not claimed by any particular state but also to the western territories
of some of the states.

Wilson against the motion.  Nothing in the constitution affected the claims of
the US, and it would be best to add nothing and leave everything as is.

Madison considered any claims to be within the jurisdiction of the Supreme
Court to decide.  It is better to remain silent.  If something should be said,
then the motion does not go far enough and should declare that the claims of
particular states should also not be affected.

Sherman thought the motion was harmless, especially with Madison's addition.

Baldwin agreed with Sherman.  Georgia has gained much under the Treaty of
Peace, but she was in danger at the end of the war of *uti possidetis* [-- a
principle in international law that, unless otherwise provided for by a
treaty, the possessor of land at the end of a conflict holds that land].

Rutledge thought it wrong to insert unnecessary provisions.

Carroll withdrew his motion and replaced it with a new wording taking into
account the objections.

G. Morris moved to postpone to take up the following:

> The legislature shall have power to dispose of and make all needful rules
> and regulations respecting the territory or other property belonging to the
> United States; and nothing in this constitution contained, shall be so
> construed as to prejudice any claims either of the US or of any particular
> state.(II, 466)

The postponement was agreed to.

L. Martin moved to append to the motion, "But all such claims may be examined
into and decided upon by the Supreme Court of the United States."(II, 466)

G. Morris thought this was unnecessary as the constitution already provides
for this situation.

L. Martin believed it was proper to remove all doubt on the matter.

The amendment by Martin was lost, and then Morris's motion passed unanimously
in the affirmative.

Article XVIII taken up:

> The United States shall guarantee to each State a Republican form of
> Government; and shall protect each State against foreign invasions, and, on
> the application of its Legislature, against domestic violence.

The word "foreign" was stuck out as superfluous, being already implied by
"invasions".

Dickinson moved to strike out "on the application of its legislature".  It is
essential that the general government should suppress domestic violence even
if it proceeds from the legislature itself.

Dayton stated that Rhode Island was an example of why this power was needed.

On the question, it was lost.

On question for replacing "domestic violence" with "insurrections", it was
lost.

Dickinson moved to allow the Executive to request help as well since the
occasion might prevent the legislature from meeting which passed in the
affirmative.

L. Martin moved to specify "in the recess of the legislature" to the last
amendment, but it was lost.

On the article as amended, it passed in the affirmative.

Article XIX taken up:

> On the application of the Legislatures of two thirds of the States in the
> Union, for an amendment of this Constitution, the Legislature of the United
> States shall call a Convention for that purpose.

G. Morris thought the legislature should be allowed to call a convention
whenever they please.

The article was agreed to unanimously.

Article XX taken up:

> The members of the Legislatures, and the Executive and Judicial officers of
> the United States, and of the several States, shall be bound by oath to
> support this Constitution.

The words "or affirmation" added after "oath".

Pinckney moved to add, "but no religious test shall ever be required as a
qualification to any office or public trust under the authority of the United
States."(II, 468)

Sherman thought it was unnecessary since the prevailing liberality was a
security against such tests.

G. Morris and C. C. Pinckney approved the motion.

The motion was agreed to unanimously and the whole article was agreed to.

[Luther Martin in his *Genuine Information* says:  "[T]here were some members
*so unfashionable* as to think, that a *belief of the existence of a Diety*,
and of a *state of future rewards and punishments* would be some security for
the good conduct of our rulers, and that, in a Christian country, it would be
*at least decent* to hold out some distinction between the professors of
Christianity and downright infidelity or paganism."(III, 227)]

[Jonas Phillips, a Jew of Philadelphia, writes to the convention on Sept. 7
that the Constitution of Pennsylvania requires a religious test affirming the
divine inspiration of the New Testament before being able to hold office.  He
requests that they leave out such religious tests from the national
constitution.(III, 78-9)]

Randolph in the Virginia Ratifying Convention says that this article allows
anyone to hold office no matter their particular religious sect and that no
single sect will ever be able to infringe on the religious freedom of
others.(III, 310)

Article XXI taken up:

> The ratifications of the Conventions of ______ States shall be sufficient
> for organizing this Constitution.

Wilson proposed "seven" for the blank, being a majority and sufficient for the
plan.

G. Morris thought there ought to be two different provisions:  one smaller
number in the case that the ratifying states are contiguous and a larger
number in case they are dispersed.

Sherman thought that since the current government requires unanimity, at least
ten ought to be required.

Randolph thought "nine" was a familiar enough majority in the current
confederation to work.

Wilson now mentioned "eight" as preferable.  [Did Madison get this wrong since
he already moved for "seven", or does he think a bare majority would not be
enough to be approved by the convention?]

Dickinson asked:
1. Is the agreement of Congress essential to the establishment of the plan?
2. Can the refusing states be deserted?
3. Can Congress oppose the final plan?

Madison feared that if nine or fewer were necessary for ratification, then the
constitution could be put into force over the whole though only a minority
might ratify it.

Wilson stated that only the ratifying states would be bound to it.  "The house
on fire must be extinguished without a scrupulous regard to ordinary
rights."(II, 469)

Butler supported nine.  One or two states should not prevent the others from
securing their safety.

Carroll moved for unanimity.  The existing confederation had been unanimously
established; therefore, unanimity must dissolve it.

King agreed with Carroll.  Otherwise, the constitution will operate on the
whole even if only a part ratifies it.

The committee adjourned.

[Butler is in possession of a number of objections to the new system so far.
It is unlikely that they are his.  The same objections are brought up by Mason
on September 15 meaning he could be the author.(IV, 249)]

# August 31, Friday

Article XXI taken back up.

King moved to append "between the said states" to the article to confine the
authority of the new constitution to only those states ratifying it, which
passed in the affirmative.

Madison moved to fill the blank with "any seven or more states entitled to 33
members at least in the house of representatives according to the allotment
made in the 3rd section of article 4."(II, 475)  This would require a
concurrence of a majority of both states and people.

Sherman noted that it may be improper to concur on less than unanimity given
the current make-up of the confederation.

G. Morris moved to strike out "conventions of" to leave the states to pursue
their own methods of ratification.

Carroll pointed out that Maryland had its own method of altering its
constitution and no other method could be pursued in that state.

King thought that Morris's motion was equivalent to giving up on the plan.
Conventions will avoid all the obstacles of the legislatures.  Opponents of
the plan will wish to avoid conventions.

G. Morris meant to facilitate the adoption of the plan by allowing the state's
to follow the method allowed in their constitutions.

Madison stated that since powers are being taken from the states to give to
the general government, the state legislatures will likely try to thwart its
adoption.  The people are the fountain of all power.  By resorting to them,
all difficulties are avoided.  They can alter constitutions as they please.

McHenry countered that officers of Maryland are under oath to support the
method of alteration described in their constitution.

Gorham and Pinckney both urged the wording of "convention".

L. Martin preferred to refer the plan to the state legislatures.  In Maryland
it won't matter; the people and the legislature will be against it.

King stated that the constitution of Massachusetts by law could not be changed
until 1790.  Yet, this was no problem.  By sending delegates to this
convention, the legislature must have had an idea of falling back onto first
principles.

On Morris's motion, it was lost.

On filling the blank with "thirteen", it was lost.

Sherman and Dayton moved to fill with "nine".

Wilson and Clymer supported Madison's motion of a majority of states and
people.

Mason preferred "nine" as it was already a familiar majority in the AoC.

"Ten" was lost, but "nine" was agreed to.

Article XXI as amended was agreed to.  [See II, 477, footnote 9 -- lots of
good perspectives on how many states ought to be necessary for ratification
covering the first days of the convention all the way through the state
conventions.]

Article XXII taken up:

> This Constitution shall be laid before the United States in Congress
> assembled, for their approbation; and it is the opinion of this Convention,
> that it should be afterwards submitted to a Convention chosen, under the
> recommendation of its legislature, in order to receive the ratification of
> such Convention.

[Dickinson added in the margins of his notes, "the Union to be perpetual".(IV,
211)]

G. Morris and Pinckney moved to strike out "for their approbation" which
passed in the affirmative.  L. Martin voted against, feeling that it was a
violation of the method outlined in the AoC.(III, 228)

G. Morris and Pinckney then moved to amend the article:

> This Constitution shall be laid before the US in Congress assembled; and it
> is the opinion of this Convention that it should afterwards be submitted to
> a Convention chosen in each state, in order to receive the ratifications of
> such Convention; to which end the several Legislatures ought to provide for
> the calling Conventions within their respective States as speedily as
> circumstances will permit.(II, 478)

G. Morris wanted to impress the necessity of speedy conventions on the states.
When the plan is first submitted, it will have the approval of the people, but
if we put ratification off too long, then the politicians will be able to turn
the people against it.

L. Martin agreed with Morris's assessment but preferred to think that the
people will only ratify it if hurried and surprised.

Gerry agreed with Martin and thought the current plan was full of vices and
its method of ratification illegitimate.

On the motion to amend, it was lost.

Gerry moved to postpone the article.

Mason seconded.  He would sooner cut off his right hand than put the article
in the constitution as it now stands.  He wished to see some points not yet
decided settled before he would approve or disapprove of the plan.  He would
approve of another general convention to decide the whole subject.

G. Morris agreed with postponement and also longed for another convention.  We
are afraid to provide for a vigorous government that we need.

Randolph did not approve of the constitution as it stood.  The state
conventions should be allowed to submit amendments to another general
convention which may reject or incorporate them.

The question for postponing was lost.

Article XXII as amended passed in the affirmative.

Article XXIII taken up:

> To introduce this government, it is the opinion of this Convention, that
> each assenting Convention should notify its assent and ratification to the
> United States in Congress assembled; that Congress, after receiving the
> assent and ratification of the Conventions of ______ States, should appoint
> and publish a day, as early as may be, and appoint a place for commencing
> proceedings under this Constitution; that after such publication, the
> Legislatures of the several States should elect members of the Senate, and
> direct the election of members of the House of Representatives; and that the
> members of the Legislature should meet at the time and place assigned by
> Congress, and should, as soon as may be, after their meeting, choose the
> President of the United States, and proceed to execute this Constitution.

The article through "assigned by Congress" was approved without debate -- the
blank being filled with "nine" as in Article XXI.

On a motion to postpone the rest of the article, it was lost.

G. Morris moved to strike out the bit about the President since the method of
his appointment had not been fully figured out yet.  It passed in the
affirmative.

Article XXIII as amended was agreed to unanimously.

[Dickinson in his notes proposed a means of calling another general convention
if the states fail to ratify within a specific period of time.(IV, 211)]

The report of the 28th was then taken up.

On the clause to be inserted after Article VII, Section 4, "nor shall any
regulation of commerce or revenue give preference to the ports of one state
over those of another", it passed unanimously in the affirmative.

On the clause, "or oblige vessels bound to or from any state to enter clear or
pay duties in another".

Madison thought the restriction could be inconvenient, as in the case of the
Delaware River if a vessel cannot make entry below the jurisdiction of
Pennsylvania.

Fitzsimons agreed that it was inconvenient but more convenient than requiring
vessels bound for Philadelphia to enter below the jurisdiction of the state.

Gorham and Langdon said that this would overly fetter the government.

Carroll and Jenifer wished the clause to be agreed to.  It was an important
point in Maryland.

On the question, it passed in the affirmative.

The word "tonnage" was struck out without debate.

On the clause, "and all duties, imposts, and excises laid by the legislature
shall be uniform throughout the US", it passed unanimously in the affirmative.

Sherman moved to commit all postponed sections of the plan and reports not
acted on which passed in the affirmative.

The committee adjourned.

[George Mason proposed a number of alterations to the current plan to the
committee on postponed parts which was assigned this day.  See IV, 251-2.]

# September 1, Saturday

A few postponed reports were presented this day.  Article VI, Section 9 to be
replaced with:

> The members of each house shall be ineligible to any civil office under the
> authority of the US during the time for which they shall respectively be
> elected, and no person holding an office under the US shall be a member of
> either house during his continuance in office.(II, 484)

To add to Article VII, Section 1 the clause:

> To establish uniform laws on the subject of bankruptcies.(II, 483)

To insert a new article as Article XVI:

> Full faith and credit ought to be given in each state to the public acts,
> records, and judicial proceedings of every other state, and the legislature
> shall by general laws prescribe the manner in which such acts, records, and
> proceedings shall be proved, and the effect which judgments obtained in one
> state shall have in another.(II, 485)

The committee adjourned.

# September 3, Monday

New article from the end of yesterday continued.

G. Morris moved to replace, at the end of the provision, "which judgments
obtained in one state shall have in another" with "thereof".

Mason favored the motion, especially if "effect" was restrained to judicial
proceedings.

Wilson said that if the legislature was not allowed to *declare* the effect,
then it would be no more than what already occurs between independent nations.

Johnson thought the wording would authorize such a declaration.

Randolph thought the power would strengthen objections to the plan.  The
powers of the general government were so loosely defined that they will usurp
all state power.  He preferred the original wording which restrains the
legislature to providing for the effect of *judgments*.

On the question, it passed in the affirmative.

Madison moved to replace "ought to" with "shall" and the "shall" after
"legislature" with "may".  It passed.

The article as amended was agreed to.

The clause from yesterday relating to bankruptcies taken up.

Sherman said that in England some cases of bankruptcy are punishable by death,
and he did not want that here.

G. Morris said he would agree to it because he saw to way to abuse this power
in the legislature.

On the question, it passed in the affirmative.

Pinckney moved to postpone the question on the report from yesterday regarding
legislators and holding offices and replace it with:

> The members of each house shall be incapable of holding any office under the
> US for which they or any other for their benefit receive any salary, fees,
> or emoluments of any kind, and the acceptance of such office shall vacate
> their seats respectively.(II, 489-90)

He strenuously supported allowing members of Congress to take offices.  He
thought the practice might be like the Romans "in making the temple of virtue
the road to the temple of fame."(II, 490)

The motion was lost.

King moved to insert "created" before "during".  This would exclude members of
the first class when the offices would be created.

Williamson seconded the motion.  There was no reason to prevent legislators
from merely filling vacancies during their term.

Sherman was for preventing legislators from taking offices entirely.  The
eligibility to offices gives too much influence to the executive.  The motion
ought to at least prevent appointment to created positions *and* positions
whose salary has increased during their term.  The prevention is easily
evaded:  An officer could be moved to a newly created post, and then a
legislator appointed to fill the "vacancy".(II, 490)  Interestingly, Sherman
changed his mind before writing a letter to the *New Haven Gazette* the next
year where he said, "There are some offices which a member of congress may be
best qualified to fill, from his knowledge of public affairs acquired by being
a member, such as minister to foreign courts, etc."(III, 355)

G. Morris thought eligibility to office *lessened* the influence of the
executive.  The executive can appoint family and friends to offices and
thereby gain influence over the voting members.  But if he appoints the
members directly, there is no such advantage.

Gerry thought eligibility of members would cause good officers to be driven
out to make way for legislators.

Gorham supported the amendment.  As it stands, the provision goes further than
any other country.  The state legislatures have shown that there is no danger
in eligibility to office.  Indeed, it has been an inducement for fit men to
enter office.

Randolph "inflexibly fixed" against inviting men into the legislature only for
appointment to office.

Baldwin said the states were different.  If state legislators were ineligible
to office, there would not be enough fit men to hold state offices.  The same
will not be true for the general government.

Mason stated that the provision will keep out corruption.

Wilson agreed with Morris and felt the ineligibility would diminish the
general energy of the government.  The disqualification would be degrading.

Pinckney stated that the first legislature will be filled by the ablest men.
If they are not eligible for office -- including offices for life like the
judiciary -- then the positions will be filled while those most qualified are
ineligible.

On King's amendment, it was lost (divided).

Williamson moved, seconded by King, to insert "created or the emoluments
whereof shall have been increased" before "during" which passed in the
affirmative.

The provision as amended was agreed to unanimously.

[Patrick Henry in the Virginia Ratifying Convention would say of this section,
"There is no restraint on corruption.  They may be appointed to offices
without any material restriction, and the principle source of corruption in
representatives, is the hopes and expectations of offices and
emoluments."(III, 313)  Madison counters that the real worry was that they
might create new offices which would be a burden on the people, but there
should be no reason to make legislators ineligible to offices that their
fellow citizens can hold.  No state constitution goes this far.(III, 314)]

The committee adjourned.

Nicholas Gilman writes to the governor of New Hampshire that many states are
buying public securities -- and that New Hampshire should do the same -- in
the expectation that a more efficient general government will pay them
off.(IV, 258)  [I think I interpreted that correctly.]

# September 4, Tuesday

The committee of eleven reported on the postponed sections from August 31.
The clauses agreed to were as follows:

(1) Article VII, Section 1, Clause 1:

> The legislature shall have power to lay and collect taxes, duties, imposts,
> and excises to pay the debts and pay for the common defense and general
> welfare of the US.(II, 497)

and (2) Article VII, Section 1, Clause 2 appends "and with the Indian tribes".
Both were agreed to without debate.

(3) Article IX, Section 1:

> The senate of the US shall have power to try all impeachments; but no person
> shall be convicted without the concurrence of two-thirds of the members
> present.(II, 497)

was postponed to take up (4) Article X, Section 1, to replace the method of
election with:

> He shall hold his office during the term of four years, and together with
> the vice-President, chosen for the same term, be elected in the following
> manner, viz.  Each state shall appoint in such manner as its legislature may
> direct, a number of electors equal to the whole number of senators and
> members of the house of representatives, to which the state may be entitled
> in the legislature.  The electors shall meet in their respective states, and
> vote by ballot for two persons, of whom one at least shall not be an
> inhabitant of the same state with themselves; and they shall make a list of
> all the persons voted for, and of the number of votes for each, which list
> they shall sign and certify and transmit sealed to the seat of the general
> government, directed to the president of the senate.
>
> The president of the senate shall in that house open all the certificates;
> and the votes shall be then and there counted.  The persons having the
> greatest number of votes shall be president, if such number be a majority of
> that of the electors; and if there be more than one who have such a
> majority, and have an equal number of votes, then the senate shall choose by
> ballot one of them for president; but if no person have a majority, then
> from the five highest on the list, the senate shall choose by ballot the
> president.  And in every case after the choice of the president, the person
> having the greatest number of votes shall be vice-president; but if there
> should remain two or more who have equal votes, the senate shall choose from
> them the vice-president.  The legislature may determine the time of choosing
> and assembling the electors, and the manner of certifying and transmitting
> their votes.(II, 497-8)

Gorham disapproved of appointing the vice-president even if he doesn't have a
majority of the remaining votes.  As it stands a very obscure man with few
votes may be elected to that position.

Sherman said the point of this clause was (1) to remove the "ineligibility",
i.e. allow the president to serve multiple terms and (2) to make the executive
more independent of the legislature.  As the choice of the president is to be
made from the top five candidates, obscure characters holding that position
are unlikely.  He had no objection to choosing the vice-president similar to
the president as Gorham implied.

Madison worried that by allowing the senate to choose form the top five, the
electors will be too concerned with making a list of candidates rather than
trying to make a definitive choice.  In which case, the election will be in
the senate's hands alone.  It would give the nomination of candidates to the
largest states.  G. Morris concurred.

Randolph and Pinckney wished for an explanation of why the method of electing
the executive had been changed.

G. Morris enumerated the reasons of the committee:
1. the danger of intrigue and faction in the legislature
2. the inconvenience of denying the executive a second term
3. it would not be proper to allow the senate to both appoint *and* impeach
   the executive
4. no one had seemed to be completely satisfied by appointment by the
   legislature
5. many desired an immediate choice by the people
6. the necessity of making the executive independent of the legislature

Cabal and corruption were avoided since electors would vote at the same time
so distant apart.

Mason approved of the way the committee had avoided many of the earlier
objections, but now 19 times in 20 the senate would choose the president.

Butler thought the new plan was not without objections but much better than
appointment by the legislature.

Pinckney stated a number of objections:
1. the senate would likely appoint
2. the electors will not be familiar enough with all the candidates to make an
   informed choice
3. reeligibility endangers the public liberty
4. the same body of men who elect him will also be his judges in case of an
   impeachment

Williamson wasn't sure if the advantages of reeligibility outweighed the
disadvantages of election by the senate.  The senate should at least be
limited to the two highest choices.

G. Morris stated that the principle objective of this means of election was to
avoid cabal.  Any other objections, e.g. reeligibility, can be corrected.

Baldwin was coming around to the plan.  As states improved their intercourse
with one another, important people would become more well-known in distant
areas of the union, and the senate would be less likely to become the final
arbiter.

Wilson said, "This subject has greatly divided the house, and will also divide
people out of doors.  It is in truth the most difficult of all on which we
have had to decide."(II, 501)  The new plan removes cabal and allows for
multiple terms for the president.  However, perhaps the whole legislature
ought to decide from a smaller selection of candidates.  The house of
representatives, owing to their shorter terms, will be freer from influence
and faction than the permanence of the senate.

Randolph preferred the previous method of election.  Why was the senate chosen
and not the whole legislature?  He also worried that appointment would fall
more likely to states near the seat of government.

G. Morris answered that the senate was preferred because fewer would be able
to say, 'You owe your position to us.'  The senate will have less impact on
his general good conduct than his reappointment.

McHenry notes that there is no provision for a new election on the death or
removal of the president.(II, 504)

The remainder of the report was postponed to give delegates time to
familiarize themselves with it.

Pinckney moved, seconded by G. Morris, moved for a clause declaring "that each
house shall be judge of the privilege of its own members"(II, 502).  By
"privilege" he meant "privilege from arrest, government of members, and
expulsion"(III, 384).

Randolph and Madison moved to postpone this motion, doubting the propriety of
such a power.

G. Morris thought it was so obvious that no postpone was necessary.

Wilson thought the power was implied, and the explicit expression needless.
It would raise doubts about why the same power wasn't expressed for, e.g.,
courts who are also judges of their own privileges.

Madison distinguished the power to judge privileges that had been previously
established with a power, as stated in the provision, which seemed so liberal.
It would be better to provide a means of ascertaining privileges *by law*
rather than leave it up to the respective house.  Some provision should also
explicitly state what privileges belong to the executive.

The committee adjourned.

McHenry notes that a motion should be made to allow the national legislature
to erect light houses or preserve the navigation of harbors, or declare
navigable waters "common high ways", etc., and he intends to make this motion
tomorrow.(II, 504)

John Collins, a Rhode Island delegate to the Continental Congress, writes to
the President of Congress, "I have not as yet lost all hopes of getting a
representation to the general convention timely, that their report may be made
in the name of the thirteen United States, the idea of a report from twelve
states only appears extremely disagreeable, I shall spare no pains to prevent
it".(III, 76-7)

William Livingston writes to John Jay of the likely outcome of the
constitution:  "it is less to be feared that the birth will be such a fetus as
a ridiculous mouse than a [huge, terrible monster]."(IV, 259)

# September 5, Wednesday

The committee of eleven reported on new provisions [enumerated].

(1) Article VII, Section 1, Clause ?:  After "to declare war" added "and grant
letters of marque and reprisal".  Agreed to without debate.

(2) Article VII, Section 1, Clause ?:  After "to raise and support armies"
added "but no appropriation of money to that use shall be for a longer term
than two years".

Gerry thought two years (instead of one) implied a standing army which is
dangerous to liberty and unnecessary, even for so great an extent as the US.
The people will not agree to this.

Sherman stated that the provision *permitted* only, not *required*, two years.
The legislature will be elected every two years.  A one year limit on
appropriations would be inconvenient because there might not be sessions that
often.  There ought, however, to be some restriction on the size, etc. of
armies in peace.

The clause was agreed to unanimously.

(3) Replace Article VI, Section 12:

> All bills for raising revenue shall originate in the house of
> representatives and shall be subject to alterations and amendments by the
> senate.  No money shall be drawn from the treasury but in consequence of
> appropriations of law.(II, 508-9)

G. Morris moved, seconded by Pinckney, to postpone.  He would dissent if he
was not satisfied with other parts remaining to be settled.  It was postponed.

(4) Insert as the second to last clause in Article VII, Section 1:

> To exercise exclusive legislation in all cases whatsoever over such district
> (not exceeding ten miles square) as may by cession of particular states and
> the acceptance of the legislature become the seat of the government of the
> United States and to exercise like authority over all places purchased for
> the erection of forts, magazines, arsenals, dock-yards, and other needful
> buildings.(II, 509)

[Jonathan Dayton would justify this provision later in the senate:  "The
provision of the constitution had arisen from an experience of the necessity
of establishing a permanent seat for the government.  To avert the evils
arising from a perpetual state of mutation and from the agitation of the
public mind whenever it is discussed, the constitution had wisely provided for
the establishment of a permanent seat, vesting in congress exclusive
legislation over it."(III, 408)]

The clause through "become the seat of the government of the United States"
accepted without debate.  The remainder taken up.

Gerry warned that the power of the last clause could be used to buy up the
territory of a state, and the strongholds placed on it would enslave it to the
general government.

King thought the provision as a whole was unnecessary but would move, seconded
by G. Morris, to insert after "purchased" the words "by the consent of the
legislature of the state" which would make it safe.

The clause was agreed to unanimously as was (5):

> To promote the progress of science and useful arts by securing for limited
> times to authors and inventors the exclusive right to their respective
> writings and discoveries.(II, 509)

Gerry gave notice that he would move to reconsider Articles XIX through XXII.

Williamson gave notice that he would move to reconsider the article fixing the
number of representatives.  There were too few, and Rhode Island should be
given more than one and to stifle any pretext arising from her absence.

The appointment of the executive was taken back up.

Pinckney once again laid out the problems with the plan:
1. Electors will not be knowledgeable of the candidates and will prefer men of
   their own state.
2. The divided votes will leave the appointment with the senate making the
   president a creature of that body.
3. He will conspire with the senate against the house of representatives.
4. The changes remove the ineligibility to repeat terms, allowing the
   president to hold office for life under the endorsement of the senate.

Gerry was not against this method of election itself but wished to see what
powers would be given to the president before making a decision.

Rutledge was against the plan and concurred with Pinckney's arguments.  He
moved to postpone the plan and take up the original appointment by joint
ballot of the legislature, but the motion to postpone failed.

Mason admitted he had not yet made up his mind.  There were objections to the
original method, but there are some to this one, too.  First, rarely will a
majority of of votes fall to any one man, leaving the appointment to the
senate.  Second, given the powers of both the president and the senate, a
coalition between the two will subvert the whole constitution.  The greatest
objection would be avoided by removing the senate from the plan.  He moved to
strike out "if such number be a majority of that of the electors".

Williamson seconded the motion.  He preferred to make the one with the most
votes (without a majority) the president rather than leave it to the senate.
Appointment by the senate will lead to corruption and aristocracy.

G. Morris thought that appointment by the senate would be an exceptional case
rather than the rule.  The electors make two votes, and half, being not of the
elector's state, will by necessity be for well-known men.  If the president
has satisfied the people, a majority of electors will reappoint him without
resort to the senate.  If disliked, the electors will unite against him to
remove him from office.

Mason said that those who thought there was no danger of not having a majority
should defer to the concern of those who do.

Sherman said, in support of the plan, that while the small states have an
advantage in the appointment by the senate, the large states have an advantage
in the nomination by the electors.

The motion of Mason was lost.

Wilson moved to replace "senate" with "legislature".

Madison didn't like any part of the legislature being the final arbiter.  He
feared the large states would focus too much on making a list of candidates
than appointing the officer -- especially since they would have a majority in
the legislature as a whole.  On the other hand, if the senate alone is the
final arbiter, the large states will try to make a conclusive decision since
the small states will have the advantage otherwise.

Randolph already believed parts of the plan laid the groundwork for a
monarchy.  Now by granting the senate influence over the president, we are
setting up a real and dangerous aristocracy.

Dickinson supported Wilson's motion.

The motion of Wilson was lost.

Madison and Williamson moved to replace "majority" with "one-third".

Gerry objected that this would make it too easy for only three or four states
to elect whomever they pleased.

Williamson argued that there are seven states which total less than a third of
the population of the whole US.  If the senate appoint, then one-sixth of the
people will have the power.

The motion by Madison was lost.

Gerry suggested the final arbiter should be six senators and seven
representatives chosen by joint ballot of both houses.

King believed the influence of the small states over the election of the
president was somewhat balanced against the origination of money bills in the
house (where the large states are advantaged).

Mason moved, seconded by Gerry, to replace "five" senators with "three" in the
amendment which was lost.

Spaight and Rutledge moved to replace "five" with "thirteen" which was also
lost.

Madison and Williamson moved to insert after "electors" the words "who shall
have balloted" so that electors who abstain from voting are not counted
towards the majority of votes needed.  On this question, it was lost.

Dickinson moved, in order to remove ambiguity, to add after "if such number be
a majority of the whole number of the electors" the word "appointed" which
passed in the affirmative.

Mason stated that he would prefer the government of Prussia to the plan before
them which puts all the power into the hands of seven or eight men who will be
an aristocracy worse than absolute monarchy.

In the last sentence after "the time of choosing and assembling the electors"
was inserted "and of giving their votes".

The committee adjourned.

# September 6, Thursday

McHenry approached G. Morris, Fitzsimons, and Gorham to insert an amendment
into the plan giving the legislature the power to erect piers and preserve the
navigation of harbors.  Gorham against, but Morris thinks it can be done under
the power to "provide for the common defense and general welfare."  Both
Fitzsimons and Morris support it.(II, 529)

The clause of the report concerning the election of the president was taken
back up.

King and Gerry moved to add a qualification that no elector can be a
legislator or office-holder of the US which passed in the affirmative.

Gerry proposed, as a means of removing the dependence of the president's
reelection on the senate, that if the sitting president was not reelected by a
majority of votes and no other candidate had a majority, then the president
would be chosen by the legislature rather than the senate.

King believed the idea was good, satisfied particular objections, promoted
unanimity, and was unlikely to be needed.

Read opposed it and said more was needed to satisfy many of the objections to
the plan.

Williamson thought it was a satisfying solution to the undue influence of the
senate.

Sherman liked the amendment but suggested that each state should have one vote
to give the small states an advantage in the appointment since the large
states already have an advantage in the nomination.

G. Morris liked the motion.  The president wouldn't have to grant offices to
appease the senate and thereby remove their influence over appointments.

Wilson thought this amendment was an improvement on the original plan but
stated that there was still too much that was dangerous.  The original plan
had a dangerous tendency to aristocracy by giving too much power to the
senate.  They will appoint the president and through him appoint the offices
-- including the judges of the Supreme Court.  The three branches of
government will all be blended into the single body of the senate.  They will
be in constant session and give themselves high salaries.  [Even in the
Pennsylvania Ratifying Convention, Wilson, though he supports the
constitution, still thinks the senate is too powerful.(III, 162)]

McHenry notes, "Montesquieu says an officer is the officer of those who
appoint him.  This power may in a little time render the senate independent of
the people.  The different branches should be independent of each other.  They
are combined and blended in the senate. [...] If this is not **aristocracy**,
I know not what it is."(II, 530)

G. Morris compared the power of the senate so far with the power in the
original report of detail to demonstrate that there was no additional power
being given to the senate.  The senate can only pick a candidate from a list
of five, and even then they will not get this opportunity with a successful
vote of the electors.  In the original report, they nominated the judges; now
they can only concur with the president on the nomination -- this is no
additional power.  Where are the dangerous innovations which establish an
aristocracy?

Williamson answered that the aristocracy derives from the change in the method
of electing the president which makes him dependent on the senate.

Clymer said the part he felt was aristocratic was in the printed plan -- that
of giving the senate the power to appoint to offices.

Hamilton disliked the scheme of government so far proposed, but since he
intended to support it as better than nothing, he wished to make a few
remarks.  The amendment is better than the original plan.  In the report, the
president is a monster elected for seven years.  His ineligibility to
reelection will tempt him to subvert the government.  If granted eligibility,
he will corrupt the legislature into continuing his office.  Given the
different interests of the different states, there was unlikely to be a
majority in the electors' votes, and the senate (in the present mode) will
have to decide.  The nomination of offices, being done with the concurrence of
the president and senate, will perpetuate the president and aggrandize both.
The solution is to allow the highest number of votes -- regardless of the
majority or not -- determine the president.  The objection that has been given
is that too small a number might appoint; however, under the current plan, the
senate, who can choose from the top five, might choose the nominee with the
fewest votes.(II, 524-5)  Also take the power to try impeachments from the
senate.(II, 531)

Spaight and Williamson moved to replace "four" with "seven" which was lost --
then "six" which was also lost.

On the question to keep the term "four", it passed in the affirmative.

On the wording of the provision in question until "entitled in the
legislature" inclusive, it passed in the affirmative.

The following motions were made:
* insert after "transmit" the words "under the seal of the state" (**LOST**)
* insert after "appointed" the words "and who shall have the given their
  votes" (**LOST**)
* insert after "counted" the words "in the presence of the senate and house"
  (**PASSED**)
* insert "immediately" before "choose" (**PASSED**)
* insert after "votes" the words "of the electors" (**PASSED**)

Spaight said that if they must have electors, then they ought to meet
altogether at the seat of the general government and elect without any
reliance on the legislature.  He moved, seconded by Williamson, for that, but
it was lost.

After "transmitting their votes" was added the words "but the election shall
be on the same day throughout the United States".(II, 526)

On the question for "if such number be a majority of that of the electors
appointed", it passed in the affirmative.

On the question to refer the eventual appointment of the president to the
senate, it passed in the affirmative.

Madison moved, seconded by Pinckney, that at least two-thirds of the senate
needed to be present for the election of the president.

Gorham thought it would delay the election to require more than a majority.

On the motion of Madison, it passed in the affirmative.

Williamson suggested that the legislature instead of the senate should vote by
state instead of per capita.

Sherman moved to replace "legislature" with "house of representatives" in
Williamson's amendment.

Mason liked the motion as lessening the aristocratic influence of the senate.

On the motion to replace the reference to the senate with "The house of
representatives shall immediately choose by ballot one of them for president,
the members from each state having one vote"(II, 527), it passed in the
affirmative.

G. Morris suggested that the president only be reelectable if he has a
majority of electoral votes, and he should not be put on the list of five
without a majority.

Madison remarked that as a majority of members in the house would make a
quorum, under the amendment by Sherman, the election of the president could be
carried by only two states -- Pennsylvania and Virginia.

On a motion that the election of the president be referred to the house on an
equality of votes, it passed in the affirmative.

King moved to add "but a quorum for this purpose shall consist of a member or
members from two-thirds of the states, and also of a majority of the whole
number of the house of representatives".(II, 528)

Mason thought this obviated Madison's observation.

The first part, regarding two-thirds of the states, was agreed to, but the
last clause, regarding a majority of members, was lost.

[The full clause of the election of the president as amended can be found at
II, 528-9.  See footnote 23 on all the references to this decision.  There
were too many for me to bother going through now -- the first few offered no
new information.]

The committee adjourned.

Madison writes to Thomas Jefferson, "I hazard an opinion nevertheless that the
plan, should it be adopted, will neither effectually answer its national
object, nor prevent the local mischiefs which everywhere excite disgust
against the state governments."(III, 77)

# September 7, Friday

The question on the executive taken back up.

Randolph moved:  "The legislature may declare by law what officer of the US
shall act as President in case of death, resignation, or disability of the
president and vice-president; and such officer shall act accordingly until the
time of electing a president shall arrive."(II, 535)

Madison moved, seconded by Morris, to replace the ending with "until such
disability be removed or a president shall be elected".(II, 535)  The amended
motion passed in the affirmative.

Some worried that the method of electing the president would be difficult to
other than at the fixed times.

Gerry moved, seconded by Madison, that in the election of the president by the
house, no state may vote with fewer than three members; if a state does not
have three representatives, then the remainder should be filled in with
senators of that state.  Otherwise, the members of small states will have
votes that will count too much.  An election could be carried by five members.

Read said that the states with only one representative would be in danger of
having no vote at all due to, perhaps, the illness of the representative or a
senator.

Madison replied that that problem is already present.  So small a number
should not be allowed to elect.  It is too prone to corruption.  The
representatives of a minority of the people might reverse the will of a
majority of the states and the people.

Gerry withdrew the first clause of his motion.  The question on the second
clause ("a concurrence of a majority of all the states shall be necessary to
make such choice"), it passed in the affirmative.

On the provision:

> No person except a natural-born citizen or a citizen of the US at the time
> of the adoption of this constitution shall be eligible to the office of
> president; nor shall any person be elected to that office, who shall be
> under the age of thirty-five years and who has not been in the whole at
> least fourteen years a resident within the US.(II, 498)

it was agreed to without debate.

The following provision taken up:

> The vice-president shall be ex officio president of the senate, except when
> they sit to try the impeachment of the president, in which case the chief
> justice shall preside, and excepting also when he shall exercise the powers
> and duties of president, in which case and in case of his absence, the
> senate shall choose a president pro tempore.  The vice-president when acting
> as president of the senate shall not have a vote unless the house be equally
> divided.(II, 498)

[Davie in the North Carolina Ratifying Convention said that the point of the
vice-presidential office (as president of the senate) was so that if a
tie-breaking vote needed to be cast, the one doing it could be objective,
having not been elected to the office by any particular state, but by the
whole union.(III, 344)]

[Butler in 1803:  "It was never intended by the constitution that the
vice-president should have a vote in altering the constitution."(III, 400)]

Gerry said that we might as well put the president at the head of the
legislature.  There will necessarily be a close intimacy between the president
and vice-president that will make the position improper.  There should be no
vice-president.

G. Morris said, "The vice-president then will be the first heir-apparent that
ever loved his father."(II, 537)  If there is no vice-president, then the
president of the senate will amount to the same thing.

Sherman did not see a danger.  Some member of the senate would need to be
president and would be deprived of his vote unless there is a tie of the
votes.

Randolph against the provision.

Williamson did not want a vice-president.  He is introduced only for the sake
of a valuable method of election which requires two positions to be appointed
at the same time.

Mason said the vice-president was an encroachment on the rights of the senate,
mixing too much the executive and legislative powers.  He didn't want to vest
appointments to office in either branch of the legislature but also understood
the danger of leaving the power solely with the executive.  To avoid both, a
privy council of six members should be set up for the president -- two from
each of the three regions of the union.  The senate should only concur on
ambassadors and treaties.  This prevents the danger and expense of a
constantly sitting of the senate.

On the question of the vice-presidential provision, it passed in the
affirmative.

The following provision taken up:

> The president by and with the advice and consent of the senate, shall have
> power to make treaties; and he shall nominate and by and with the advice and
> consent of the senate shall appoint ambassadors, and other public ministers,
> judges of the supreme court, and all other officers of the US, whose
> appointments are not otherwise herein provided for.  But no treaty shall be
> made without the consent of two-thirds of the members present.(II, 498-9)

[See footnotes 15 on II,538 and 18 on II,539 for more discussion.]

Wilson moved, seconded by Fitzsimons, to add "and house of representatives"
after "senate".  Treaties are legislation and ought to be treated like any
other legislation.  He understood the necessity of secrecy but thought the
sanction of law outweighed it.

Sherman thought the senate could be trusted with the power and that the
necessity of secrecy was inconsistent with putting it before the whole
legislature.

The motion of Wilson was lost.

The first sentence was agreed to unanimously.

Wilson asserted that a good executive requires a responsibility in the
appointment of officers which is lacking with the concurrence of the senate.
He preferred the privy council suggested by Mason as long as there was no
obligation on the part of the president to follow their advice.

Pinckney thought the senate should only have a hand in appointing ambassadors
who should not be appointed by the president.

G. Morris said that the nomination of the president is the responsibility, but
the concurrence of the senate is the security.

Gerry thought the concern with "responsibility" was ridiculous since the
president, not knowing all characters, could always plead ignorance.

King mentioned that the privy council suggested by Mason was really no better
than using the senate for the same purpose.  There should be no worry that the
senate will sit constantly.  Inferior offices will be appointed by the heads
of those departments, not the senate.  The people will also not let too many
offices be created which will influence the expense and influence of the
government.

On the provision until except for the last sentence, it passed in the
affirmative.

Spaight moved "that the president shall have power to fill up all vacancies
that may happen during the recess of the senate by granting commissions which
shall expire at the end of the next session of the senate" which was agreed to
without debate.

The last sentence taken up.

Wilson objected to requiring as many as two-thirds to approve.  The minority
will control the majority.

King agreed and pointed out that the president serves as a check which does
not exist for other legislative acts where two-thirds is required.

Madison moved to insert "except treaties of peace" since they should be made
easily which was agreed to without debate.

Madison moved, seconded by Butler, to allow two-thirds of the senate to make a
treaty of peace without the concurrence of the president.  The president will
derive many powers from the state of war that he might be reluctant to give
up.

Gorham thought the precaution was unnecessary as the legislature, not the
president, would control the means of carrying on the war.

G. Morris did not see a danger of giving the president this power.  No peace
should be made without the say of the general guardian of the national
interests.

Butler said the motion was a necessary security against ambition and
corruption in the president.

Gerry figured that treaties of peace should require more effort since the
dearest interests -- trade, territories, etc. -- will be in greater danger of
sacrifice than any other occasion.

Williamson thought treaties of peace should be done with the same concurrence
as other treaties.

On the motion of Madison, it was lost.

On the motion to add exception for treaties of peace, it passed in the
affirmative.

Article X, Section 2:  insert after "into the service of the US" the words
"and may require the opinion in writing of the principle officer in each of
the executive departments, upon any subject relating to the duties of their
respective offices"(II, 499) taken up.

Mason cautioned that rejecting a council to the president was "an experiment
on which the most despotic governments had never ventured".(II, 541)  He moved
to postpone to take up the following:

> That it be an instruction to the committee of the states to prepare a clause
> or clauses for establishing an executive council, as a council of state for
> the president of the United States, to consist of six members, two of which
> from the eastern, two from the middle, and two from the southern states,
> with a rotation and duration of office similar to those of the senate; such
> a council to be appointed by the legislature or by the senate.(II, 542)

Franklin seconded the motion.  We fear cabals in multitudes but have too much
confidence in individuals.  Experience showed that favoritism in appointments
was the most prevalent abuse in monarchies.  A council will not only be check
on bad presidents but also a relief to good ones.

G. Morris reminded them that a similar motion had been brought up once before
and the principle argument against it was that by persuading a council to
agree to his wrong measures, the president would acquire protection from
responsibility for the appointments.

Wilson preferred the council to the senate for appointments.

Dickinson supported a council.  Some discussion of executive measures should
go on before the president.

Madison supported the motion.

On the question, it was lost.

On the question of the insertion from the report, it passed in the
affirmative.

Williamson and Spaight moved "that no treaty of peace affecting territorial
rights should be made without the concurrence of two-thirds of the members of
the senate present."(II, 543)

King moved to extend the motion to "all present rights of the United
States".(II, 543)

The committee adjourned.

# September 8, Saturday

King moved to strike out "exceptions of treaties of peace" from requiring
two-thirds of the senate to approve.

Wilson wished to strike out two-thirds altogether.  If we cannot trust the
majority, then we should not be one society.

G. Morris against.  If the legislature knows that two-thirds of the senate are
required to make peace, then they will be less likely to make war.  If a
majority of the senate are for peace but unable to make it, then they will
likely sabotage the war effort by vetoing supplies.

Williamson remarked that since treaties of peace are to be made in the house
with equal suffrage, a minority of the representation, being a majority of the
representatives, would have the power to decide the conditions of peace.
There is no danger that the exposed states (SC, GA, etc.) will start an
improper war on the western territory.

Wilson reasoned that if two-thirds are necessary to make peace, then a
minority may continue a war against the will of the majority.

Gerry asserted that the essential rights of the union cannot be put in the
hands of so small a number as the majority of the senate.  The senate will be
more likely to be corrupted by foreign influence.

Sherman was against the senate having the only say and moved, seconded by G.
Morris, that no such rights should be ceded without the sanction of the
legislature.

Madison felt that it had been too easy in the present Congress to make
treaties even when nine states were required.

On the question to strike out "except treaties of peace", it passed in the
affirmative.

Wilson and Dayton move to strike out two-thirds required for making treaties
which is lost.

Rutledge and Gerry moved that "no treaty be made without the consent of
two-thirds of all the members of the senate" which was lost.

Sherman moved, seconded by Gerry, that "no treaty be made without a majority
of the whole number of the senate".(II, 549)

Williamson:  "This will be less security than two-thirds as now required."

Sherman:  "It will be less embarrassing."

[Me:  "LOL."]

On the question, it was lost.

Madison moved that a quorum of the senate consist of two-thirds of all the
members.

G. Morris stated that one man could break the quorum.

Madison replied that that is true for any quorum.

The question was lost.

Williamson and Gerry moved "that no treaty should be made without previous
notice to the members and a reasonable time for their attending"(II, 549-50)
which was lost.

On the question relating to treaties by two-thirds of the senate, it passed in
the affirmative.

Gerry moved that no officer be appointed unless the office is created by law.
This was voted against as unnecessary.

The following provision modifying the latter part of Article X, Section 2 was
taken up:

> He shall be removed from his office on impeachment by the house of
> representatives and conviction by the senate for treason or bribery and in
> case of his removal as aforesaid, death, absence, resignation, or inability
> to discharge the powers or duties of his office, the vice-president shall
> exercise those powers and duties until another president be chosen or until
> the inability of the president be removed.(II, 499)

Mason wished to know why the provision restrains impeachment to treason and
bribery only.  For example, subversion of the constitution is not
"treasonous".  Since bills of attainder are forbidden, it is important to
extend the power of impeachments.  He moved, seconded by Gerry, to add
"maladministration" to the list.

Madison worried that such a vague term would amount to a term during the
pleasure of the senate.

G. Morris said an election every four years will prevent maladministration.

Mason replaced "maladministration" with "other high crimes and misdemeanors
against the state".

And on the question amended, it passed in the affirmative.

Madison objected to trial of the president by the senate, especially since he
was impeached by the other house and for any act which might be called a
misdemeanor.  This makes the president improperly dependent on the
legislature.  The supreme court should try impeachments.

G. Morris did not trust any other tribunal with the responsibility than the
senate.  The supreme court are too few and might be corrupted.  He, too,
worried about a dependence on the legislature leading to legislative tyranny,
but there is no danger of that here.  There is no reason for the senate to
unjustly conspire against the president when they can just turn him out in
four years.

Pinckney echoed the sentiments that the trial by the senate created a
dependence between the executive and the legislature.  If the president vetoes
a favorite law, then the two houses can conspire to throw him out.

Williamson thought, considering the number of ways in which the president is
associated with the senate, there was more danger of too much leniency towards
the president than too much rigor.

Sherman thought the supreme court was inappropriate for the task since they
are appointed by the president.

Madison moved to strike out the words "by the senate" after the word
"conviction", but it was lost.

The word "state" after "misdemeanors" was replaced with "United States" to
remove any ambiguity.

The motion of Mason as amended passed in the affirmative.

The clause "the vice-president and other civil officers of the US shall be
removed from office on impeachment and conviction as aforesaid" was added to
the provision on the subject of impeachments.

The postponed clause (3) from Sept. 5 was taken back up.

It was moved to replace "and shall be subject to alterations and amendments by
the senate" with the wording from the constitution of Massachusetts "but the
senate may propose or concur with amendments as in other bills" which was
agreed to without debate.  [Elbridge Gerry, in the Massachusetts Ratifying
Convention, says that this change in wording "*effectually destroyed*" the
compromise of the provision.(III, 265)]

On the first part of the question that all money bills would originate in the
house, it passed in the affirmative.

The clause (3) from Sept. 4 (regarding impeachment by the senate) was taken
back up.

Gerry moved to add "and every member shall be on oath" which was agreed to.

On the question of clause (3), it passed in the affirmative.

Gerry repeated his motion giving the legislature the power to establish
offices which was again lost.

McHenry moved, the president not yet authorized to convene the senate, that
"[the president] may convene both or either of the houses on extraordinary
occasions".(II, 553)

Wilson said he would vote against the motion since it implied the senate could
be in session when the legislature wasn't which would be improper.

On the question, it passed in the affirmative.

A committee was appointed to rework the articles as amended consisting of
Johnson, Hamilton, G. Morris, Madison, and King.

[See the amended version of the August 6 draft II, 565-580.]

Williamson moved, seconded by Madison, to reconsider the number of
representatives in the house.

Sherman thought the provision was already sufficient.

Hamilton very much supported the motion.  He considered himself a friend to a
vigorous government, but the popular branch needs to have a broader
foundation.  The number of members in the house of representatives are so few
now that the people will be jealous that their liberties are not sufficiently
defended.  [Hamilton in the New York Ratifying Convention claims the reason
for this number eventually being reduced was, "when his excellency the
president expressing a wish that the number should be reduced to thirty
thousand, it was agreed to without opposition."(III, 337)]

On the question, it was lost.

The committee adjourned.

Hamilton writes to "Mr. Duche", "You can tell the Marquis de la Fayette
something which will bring him the greatest pleasure, that is, there is every
reason to believe that if the new constitution is adopted his friend General
Washington will be the chief."(IV, 263)

Elbridge Gerry writes to his wife, "I am myself of opinion that Thursday will
finish the business to which I have every prospect at present of giving my
negative."(IV, 264)

# September 10, Monday

Gerry moved to reconsider Article XIX [-- see August 30].  The plan is to be
paramount to the state constitutions.  Two-thirds may concur to call a
convention, the majority of which may bind the whole union to innovations that
may subvert the state constitutions.  Is this a situation we want?

Hamilton seconded the motion but for different reasons than Gerry and did not
find his consequences alarming.  We all want an easier method of amending the
constitution than the AoC provided.  This is especially true to correct any
defects that might present themselves in the new system.  However, the state
legislatures will never call a convention unless it is to gain more power.
The national government will know best what needs to be amended.  The
legislature ought to be able to call a convention with the concurrence of
two-thirds of both houses.  There is no danger since the people will
ultimately decide the outcome.

Madison supported the motion because of the vagueness of "call a Convention
for that purpose".  How was it to be formed?  What rules would it have?  What
would be the force of its acts?

On the question to reconsider, it passed in the affirmative.

Sherman moved, seconded by Gerry, to add "or the legislature may propose
amendments to the several states for their approbation, but no amendments
shall be binding until consented to by the several states."(II, 558)

Wilson moved to insert "two-thirds of" before "the several states" which was
lost, but "three-fourths" was agreed to unanimously.

Madison moved, seconded by Hamilton, to postpone the article to take up:

> The legislature of the United States whenever two-thirds of both houses
> shall deem necessary, or on the application of two-thirds of the
> legislatures of the several states, shall propose amendments to this
> constitution, which shall be valid to all intents and purposes as part
> thereof, when the same shall have been ratified by three-fourths at least of
> the legislatures of the several states, or by conventions in three-fourths
> thereof, as one or the other mode of ratification may be proposed by the
> legislature of the US.(II, 559)

Rutledge said he could never agree to give such a power to the states who are
prejudiced against the property of slaves.  The following words were added to
the proposition:  "provided that no amendments which may be made prior to the
year 1808 shall in any manner affect the 4 and 5 sections of the VII
article".(II, 559)

The motion by Madison as amended was passed in the affirmative.

Gerry moved to reconsider article XXI and XXII.  He opposed the striking out
of the requirement of the approbation of Congress and objected to an annulment
of the AoC with so little formality.

Hamilton agreed with Gerry.  The formal approval of Congress was necessary.
It is also wrong to allow nine states to institute a new government on the
ruins of the old.  The number of states necessary for ratification should be
decided by the state legislatures.

Gorham argued that the states will not agree on the number and will defeat the
ratification.

Hamilton asserted that no convention once convinced of the plan's necessity
would refuse to give it effect on the adoption of nine states.  This method is
less objectionable than the current wording of the plan and will achieve the
same end.

Fitzsimons reminded them that the clause in question had been struck out to
save Congress from an act which was inconsistent with the AoC.

Randolph agreed that this part of the plan needed to be changed.  He had
supported radical changes to their system of government from the beginning and
had submitted a number of propositions based on republican principles for that
purpose.  However, those principles had unfortunately been departed from.  The
state conventions should be allowed to offer amendments, and these amendments
should be referred to a final general convention to create the constitution.
He did not expect his proposal to meet with success, but he felt it was his
duty to try.

Wilson opposed reconsideration for any of the above reasons.

King thought it was more respectful to submit the plan to Congress without the
requirement of their approval.  Nine states are sufficient for ratification,
and this is better as an explicit part of the constitution than as a
supplemental recommendation.

Gerry thought that the solemn obligations of the AoC should not be dissolved
in so slight a manner.  If nine out of thirteen can dissolve the AoC, then six
of nine will be able to dissolve the next.

Sherman agreed with King on how the plan should be submitted to Congress but
agreed with Hamilton on the method of ratification, that it should be a
separate recommendation not part of the constitution itself.

On the question for reconsideration, it passed in the affirmative.

Hamilton then moved, seconded by Gerry, to postpone XXI to take up the
following instead:

> Resolved that the foregoing plan of a constitution be transmitted to the US
> in Congress assembled, in order that if the same shall be agreed to by them,
> it may be communicated to the legislatures of the several states, to the end
> that they may provide for its final ratification by referring the same to
> the consideration of a convention of deputies in each state to be chosen by
> the people thereof, and that it be recommended to the said legislatures in
> their respective acts for organizing such convention to declare, that if the
> said convention shall approve of the said constitution, such approbation
> shall be binding and conclusive upon the state, and further that if the said
> convention should be of opinion that the same upon the assent of any nine
> states thereto, ought to take effect between the states so assenting, such
> opinion shall thereupon be also binding upon such state, and the said
> constitution shall take effect between the states assenting thereto.(II,
> 562)

Wilson did not think it wise to require the approval of Congress for the plan.
Rhode Island will not approve, Maryland and the delegates from other states
are opposed to various parts of it, and New York has not been present for
weeks.  After four months of work on a new government, we are suggesting
throwing more obstacles in the way of its ratification.

Clymer thought the proposition by Hamilton would fetter and embarrass Congress
since its approval would be a breach of the AoC.

King and Rutledge agreed with Clymer.  If the states are to provide that nine
states are sufficient to put the plan into effect, then we should count all
our labor as lost.

The motion by Hamilton was lost.

Article XXI was then agreed to unanimously.

Hamilton withdrew the remainder of his motion on XXII since the end was
defeated by the last vote.

Williamson and Gerry moved to reinstate the approbation of Congress into
article XXII which was lost unanimously.

Randolph took the opportunity to enumerate the problems he had with the plan:
* the senate was to try the president for impeachment
* three-fourths (instead of two-thirds) were required to overcome a
  presidential veto
* the house of representatives was too small
* there are not enough limits on standing armies
* "on the general clause concerning necessary and improper laws"(II, 563) [???]
* no restraint on navigation acts
* the power to lay duties on exports
* the general legislature can interfere with the executives of the states
* no definite boundary between the general and state legislatures or
  judiciaries
* the unqualified power of the president to pardon treasons
* no limit on the legislatures regarding their own compensations

He could not approve of a plan that he was certain would end in tyranny.  He
would not impede the convention but must hold himself separate in case he was
chosen to represent his state in the ratifying convention.  The only way he
could approve of the plan as is, is to allow the states to make amendments
that will be decided by a final general convention.  He proposed a resolution
to this effect which Franklin seconded.

Mason urged that the motion lay over for a day or two to see what final report
would be submitted.

Pinckney moved to instruct the committee of detail to prepare an address to
the people to go with the new plan which was referred to the committee without
debate.

Randolph moved to refer to the committee a motion regarding pardons of treason
which was agreed to without debate.

The committee adjourned.

# September 11, Tuesday

No convention -- waiting for the committee of style and arrangement to finish
the final report.

Livingston writes to John Jay, "But my hopes of returning by the time expected
are a little clouded by reason of there being certain creatures in this world
that are more pleased with their own speeches than they can prevail upon
anybody else to be."(IV, 267)

# September 12, Wednesday

[See the report of the committee of style II, 590-603.]

[See draft of the letter of Congress to accompany the plan II, 583-4.]

Williamson moved that two-thirds, instead of three-fourths, of the legislature
might override a presidential veto.  He had earlier supported the latter but
was persuaded that the former is best because the latter gave the president
too much power.

Sherman agreed.  Neither the states nor the people would like to see so small
a minority and the president prevail over the general voice.  It is more
probable that a single man be mistaken or betray the public trust than
two-thirds of the legislature.

G. Morris stated that the difference between the two proportions amounted to
only two members in the senate and five members in the house.  Three-fourths
is in the best interest of the distant states as they will be absent more
often.  We should fear the excess of law more than its deficiency.  Two-thirds
is done in New York, and it has not held back the excess.

Hamilton testified to the truth of the example of New York.

Gerry thought it was dangerous to go too far in the other direction as well.
Three-fourths puts too much power in a few men -- the president and a handful
of legislators.  The veto power is not for defending the general interest; it
is for defending the executive department.  With three-fourths a few
conspiring senators, hoping for nominations to offices, can work with the
president to impede proper laws.  The vice-president being the president of
the senate increases this danger.

Wilson\* feared too many laws as opposed to too few.  The prevention of bad
laws might become too difficult if two-thirds is required.  [The Journal says
this argument was presented by Williamson -- except **Williamson was the one
who proposed the change.**  The editor does not mention the contradiction.
These statements are consistent with Wilson's on Aug. 15, so I think it was
just a mistake on the editor's part.]

Mason supported the motion.  Regarding the example of New York:  It depends on
what the real merits of the laws were.  Perhaps others of different opinions
could paint the abuses as occurring on the opposite side.  The primary object
should be to guard against too great an obstacle to the veto power.

G. Morris argued that there was a public interest in the stability of the law.
There is not much danger on the other end.  If one man stands in the way of
proper legislation, then he will be replaced within four years.

Pinckney thought three-fourths put too much power into the hands of a few
senators.

Madison stated that the object of the veto is two-fold:  (1) to defend the
executive and (2) to prevent injustice.  The state legislatures have
demonstrated that their checks are insufficient.  Weighing the danger from the
weakness of two-thirds with the danger from the strength of three-fourths
indicated that the former was the greater danger.

On the question, it passed int he affirmative.

Williamson observed that there was no provision for juries in civil cases and
suggested the necessity of it.

Gorham did not think it was possible to distinguish
[equity cases](https://legal-dictionary.thefreedictionary.com/equity) from
those in which juries are proper.  We can leave it to the representatives of
the people to decide.

Gerry thought juries were necessary to guard against corrupt judges.  He moved
that the committee of style ought to provide a clause securing trial by jury.

[Wilson in several places (see footnote 5 II, 587) explains that there is no
way to enumerate the trials that should have a jury from those that don't, and
it can be left to the legislature to determine that on a case by case basis.]

Mason agreed with the points of Gorham.  The statement of a general principle
may be sufficient.  The plan should really be prefaced with a Bill of Rights.
It would protect the people and given the rights already outlined in many of
the state constitutions, the list would only take a few hours to write.

Gerry moved for the Bill of Rights, seconded by Mason.

Sherman supported securing the rights of the people, but the declarations of
rights in the state constitutions are already sufficiently in force.

Mason pointed out that any US law will supersede a state's Bill of Rights.

On the question to prepare a Bill of Rights, it was lost.

[Wilson, before the Pennsylvania Ratifying Convention, said that "a bill of
rights is neither an essential nor a necessary instrument in framing a system
of government, since liberty may exist and be as well secured without it.
[...]  and when the attempt to enumerate them is made, it must be remembered
that if the enumeration is not complete, everything not expressly mentioned
will be presumed to be purposely omitted."(III, 143-4)  "Enumerate all the
rights of men!  I am sure, Sir, that no gentleman in the late convention would
have attempted such a thing."(III, 162)  George Washington supports Wilson's
reasoning.(III, 298)]

[C. C. Pinckney in the South Carolina House of Representatives in defense of
not enumerating a bill of rights said, "by delegating express powers [to the
government], we [the people] certainly reserve to ourselves every power and
right not mentioned in the constitution."(III, 256)]

Mason moved to reconsider the clause regarding exports.  The restrictions on
duties on exports will prevent necessary revenue for the inspection and
safe-keeping of produce.  This will be ruinous to the southern states.  He
moved for the following addition:

> provided nothing herein contained shall be construed to restrain any state
> from laying duties upon exports for the sole purpose of defraying the
> charges of inspecting, packing, storing, and indemnifying the losses, in
> keeping the commodities in the care of public officers, before
> exportation(II, 588)

Any other form of tax would be inconvenient for the planters before the actual
delivery for exportation.  [Rufus King gives some more context behind this
situation III, 268.]

Madison seconded the motion.  At worst it was harmless.  At best it might
restrain the states to tax only enough to fulfill the explicit purpose.
Although, the best protection from abuse might be the power in the general
government to regulate interstate trade.

G. Morris supported the motion and did not consider, e.g., the price for
storage laid on tobacco in Virginia as a duty.

Dayton worried that the provision would still allow Pennsylvania to tax New
Jersey for "inspection duties".

Gorham and Langdon thought the security offered no protection in the case that
one state exported its goods through another state.  Which state jurisdiction
would judge if duties were being laid beyond what was necessary?

Madison said that the same authority in every other similar case would -- the
supreme court.  A veto over state laws would have been a better security, but
that was overruled.

Fitzsimons stated that incidental duties on tobacco and flour never have been
and never can be considered duties on exports.

Dickinson moved, seconded by Butler, that the assent of Congress should be
required for inspection duties.  Otherwise, New Hampshire, New Jersey, and
Delaware will be oppressed by their neighbors.

The committee adjourned.

# September 13, Thursday

Mason criticized at length "on the extravagance of our manners, the excessive
consumption of foreign superfluities, and the necessity of restricting it"
with economic and republican arguments.  He moved, seconded by Johnson, to
appoint a committee to report articles for encouraging "economic frugality and
American manufactures"(II, 606).

The motion passed in the affirmative, and a committee was made, but Madison
records that no report ever came of it.

Mason renewed his amendments on export duties from yesterday, only slightly
rewording it and adding the following to give Congress control in cases of
abuse:  "but all such regulations shall in case of abuse, be subject to the
revision and control of Congress".(II, 607)

The motion passed in the affirmative without debate.

Article I, Section 2 from the newest report was taken up.

Randolph moved to replace "servitude" with "service" since the former implied
slavery and the latter free men, which was agreed to.

Dickinson and Wilson moved to strike out "and direct taxes" thinking it
improperly associated with the representatives in this section.

G. Morris clarified that the purpose of the term was to give the appearance of
excluding slaves in the representation.  They were to count towards direct
taxation and only "*incidentally*" counted towards representation as a result.

The motion was lost.

Article I, Section 7 regarding the veto.

Madison moved, seconded by Randolph, to clarify that the day a bill was
presented did not count as one of the ten days the president had to veto or
sign into law.

G. Morris said the amendment was unnecessary because "the law knows no
fraction of days."(II, 608)

The motion was lost.

Johnson reported from the committee of style further instructions -- to
replace Articles XXII and XXIII of the draft --  to be sent to Congress along
with the final plan.  The wording is not different from what was sent on
Sept. 17. (II, 608-9 & 665-6)

The committee adjourned.

# September 14, Friday

Williamson moved to reconsider the number of representatives.  He would like
to see an increase generally by half and at least two for the smallest states.
The motion was lost.

Article I, Section 3.

Madison struck out "by lot", hoping that some rule might prevent both senators
of a state from leaving at the same time.  "Ex officio" struck out as
superfluous, and after "oath" inserted "or affirmation".  All were unanimous.

Rutledge and G. Morris moved that persons impeached should be suspended until
tried and acquitted.

Madison and King stated that the president is already too dependent on the
legislature since one house can impeach him and the other try him.  The
intermediate suspension will put him under the power of a single house.  They
will be able to temporarily remove the president at any moment to make way for
another who will be more favorable to their views.

The motion was lost.

Article I, Section 4.  Added "except as to the places of choosing senators" to
the first clause unanimously.(II, 613)  This was to prevent the general
legislature from setting the place away from the usual sessions of the state
legislature which would be inconvenient and unnecessary for the regulation of
elections.(III, 311)

Article I, Section 5.

Mason and Gerry moved to require the house of representatives to record all
proceedings assuming that they would not need the same level of secrecy that
the senate required.  It was pointed out that some secrecy might be needed,
e.g., when declaring war, and the motion was lost.

Article I, Section 6.

Baldwin observed that the disqualification against Congressmen serving for
offices created during his term did not extend to offices created by the
Constitution.  The members of the first session would evade the
disqualification.

Article I, Section 8.

Rutledge moved to strike out the election of the treasurer and let the office
be appointed like any other public office.

Gorham and King believed that the people would object to an appointment of the
treasurer.

G. Morris countered that if he is not appointed by the legislature, he will be
more closely watched and more quickly impeached.

Sherman stated that since both houses have the power to appropriate money,
they should also be able to appoint the officer to oversee that appropriation.
*But* they should do it with the appropriation, not by joint ballot.

C. C. Pinckney stated that the treasurer is appointed by joint ballot in South
Carolina.  The consequence is that bad appointments are made, and the
legislature refuses to remove their own officer.

On the motion, it passed in the affirmative.

G. Morris moved to remove "punish" in the piracies clause.  This would make
the phrase "offenses against the law of nations" definable as well as
punishable.

Wilson criticized the arrogance and ridiculousness of the US government having
the authority to *define* the law of nations.

G. Morris argued that defining is proper in the case of *offenses* though.
The concept of the "law of nations" is itself too vague to be a rule.

On the question, it passed in the affirmative.

Franklin moved, seconded by Wilson, to add after "post roads" the words "a
power to provide for cutting canals where deemed necessary".(II, 615)

Sherman objected because the union as a whole will pay the expense, but only
the place with the canal will see the benefit.

Wilson argued that instead of an expense, they might become a source of
revenue.

Madison moved, seconded by Randolph, to enlarge the motion "to grant charters
of incorporation where the interest of the US might require and the
legislative provisions of individual states may be incompetent."(II, 615)  He
intended to secure an greater communication between the states which transport
by canals would create.

King said the power was unnecessary.

Wilson argued that it was necessary for the national government to prevent the
obstruction by a state of the general welfare.

King said that the provision will cause division and prejudice in the states.
Philadelphia and New York will object on the grounds that it will lead to the
establishment of a bank; in other places, mercantile monopolies.

Wilson asserted that the canals will facilitate communication with the west.
With respect to banks, it will not create the opposition you fear.  With
respect to mercantile monopolies, they are already included in the power to
regulate trade.

Mason wished to limit the provision to canals only.  He disagreed with Wilson
that monopolies were already implied by the constitution.

On the question, it was lost.

Madison in 1831 writes of the reasons for trying to pass a motion for internal
improvements generally and canals particularly(III, 494-5):
1. Congress would have the best and most popular revenue to create such costly
   works.
2. In some cases canals would be beneficial to the national view but not the
   local.
3. In some cases they would be important for the national interest but would
   violate the interest of the state.
4. A series of useful canals may be built through several states for the
   benefit of trade that would be impossible to do if left to state authority.

Madison and Pinckney moved "to establish a university, in which no preferences
or distinctions should be allowed on account of religion."(II, 616)

Wilson supported.

G. Morris thought that centralizing the general government in a single place
would achieve this anyway.

On the question, it was lost.

Mason wanted to make clear the danger of standing armies and moved, seconded
by Randolph, to preface before the clause on organizing the militia the
explanation, "And that the liberties of the people may be better secured
against the danger of standing armies in time of peace..."(II, 617)

G. Morris "opposed the motion as setting a dishonorable mark of distinction on
the military class of citizens"(II, 617) which Pinckney and Bedford agreed
with.

On the question, it was lost.

Article I, Section 9.

Mason moved to reconsider the "ex post facto" clause.  Specifically, he wanted
to strike out those words because it was uncertain whether it applied only to
criminal cases, and no legislature can avoid them in civil cases.

Gerry seconded the motion but wanted to extend the prohibition to civil cases
which he thought possible.

On the question, it was lost unanimously.

Pinckney and Gerry moved to insert a declaration "that the liberty of the
press should be inviolably observed."(II, 617)

Sherman stated that the provision is unnecessary because Congress has been
given authority over the press.

On the question, it was lost.

Pinckney later seems to agree with Sherman, "The general government has no
powers but what are expressly granted to it; it therefore has no power to take
away the liberty of the press."(III, 256)

Read moved, seconded by Williamson, to add after "capitation tax" the words
"or other direct tax".  The rule, by giving a different cast to the meaning of
the clause, was to prevent Congress from saddling the states with past
requisitions.  The motion passed in the affirmative.

On motion of Mason, after "census" added "or enumeration" which passed in the
affirmative.

Mason moved, seconded by Gerry, "that an account of the public expenditures
should be annually published."(II, 618)

G. Morris thought this would be impossible.

King believed the term "expenditures" required them to account for every
minute cent, which would be impracticable.  Congress could make a monthly
statement, but it could only be a general one.

Fitzsimons agreed with King.

Madison moved to replace "annually" with "from time to time" to leave the
frequency of the publication to the legislature.  Requiring too much will
result in nothing being done at all.  This happened under the present Congress
who were supposed to publish twice a year on this subject.  The punctuality
being impossible, they ceased the practice altogether.

Wilson seconded the motion.  Many financial reports cannot be published at set
times.

Pinckney and Sherman supported it.

On the question of replacing "annually" with "from time to time", it passed in
the affirmative.

The motion by Mason also passed int he affirmative amended to "And a regular
statement and account of the receipts and expenditures of all public money
shall be published form time to time."(II, 619)

Article I, Section 10.

The first clause was slightly reworded but keeps the original meaning.

Gerry expounded on the important of public faith.  Observing that the states
were prevented from impairing the obligation of contracts, he moved that the
national Congress be given the same prohibition.  He was not seconded.

Davie, before the North Carolina Ratifying Convention, would say that just
because the section in question does not prohibit the general government from
interfering with public securities does not mean they are implicitly vested
with the power.(III, 350-1)

The committee adjourned.

# September 15, Saturday

Carroll urged and moved that an address to the people be prepared to accompany
the final plan.

Rutledge thought the motion would produce too much delay.  There should be no
address to the people until we know if Congress will approve.  Congress can
prepare a proper address if they choose.  Sherman agreed.

On the question, it was lost.

Langdon moved for a reconsideration that one representative be added to both
North Carolina and Rhode Island.

Sherman had originally thought that five was the proper share but "subsequent
information" persuaded him that one more was appropriate.

On the motion to reconsider, it passed in the affirmative.

King thought that any further consideration would delay the plan.  There was
no official proof that North Carolina deserved another representative, and
giving Rhode Island another -- giving her one fourth of the representation of
Massachusetts -- was unjust.

Pinckney supported adding one more to North Carolina.

Bedford supported one more for Rhode Island and Delaware.

On the question of two representatives for Rhode Island, it was lost.

On the question of six for North Carolina, it was lost.

Resuming Article I, Section 10.

In consequence of Mason's provision of Sept. 13, the first half of the second
paragraph was reworded to read:

> No state shall, without the consent of Congress, lay any imposts or duties
> on imports or exports, except what may be absolutely necessary for executing
> its inspection laws; and the net produce of all duties and imposts, laid by
> the state on imports or exports, shall be for the use of the treasury of the
> United States; and all such laws shall be subject to the revision and
> control of the Congress.(II, 624)

The rest of the paragraph was taken up.

McHenry and Carroll moved that "no state shall be restrained from laying
duties of tonnage for the purpose of clearing harbors and erecting
lighthouses."(II, 625)

Mason claimed that the Chesapeake was in need of such expenses.

G. Morris believed that the present wording of the constitution did not
restrain the states from doing this as is.  The exception proposed will imply
the opposite and actually put the states in a worse situation.

Madison countered that Morris's analysis depended on the interpretation of the
wording of "to regulate commerce".  The power may also be restrained by
treaties.  For himself, he believed more and more that the regulation of
commerce should be wholly under a single authority.

Sherman did not believe there was any danger in co-jurisdiction over commerce.
The general government's authority is paramount and can control interferences
of state regulations when such interferences happen.

Langdon thought the laying duties of tonnage was essential to the regulation
of commerce; thus, the states should have nothing to do with it.  He moved
that "no state shall lay any duty on tonnage without the consent of
Congress".(II, 625)

On the prohibition on the states, it passed in the affirmative.

Article II, Section 1 -- the President.

Replaced "or the period of choosing another president arrive" with "or a
president shall be elected".

Rutledge and Franklin moved to append to clause (f), "and he shall not receive
within that period any other emolument from the US or any of them"(II, 626)
which passed in the affirmative.

Article II, Section 2 -- regrading pardons.

Randolph moved for "except cases of treason".  Otherwise, the power was too
great.  The traitors may be the president's own co-conspirators.  [L. Martin
makes these same points in *Genuine Information*.(III, 218)]

Mason supported.

G. Morris preferred that there be no pardon for treason than let the power
devolve to the legislature.

Wilson asserted that pardon for treason was necessary, and it is best in the
hands of the president.  If the president himself is guilty, then he can be
impeached and prosecuted.

King stated that the power of pardon should be left in the executive rather
than the legislature.  The legislature -- too much guided by the passions of
the moment -- is unfit for the purpose.  In Massachusetts, one assembly would
have hung all the insurgents, and the next would have pardoned them all.
Although, pardon should require the concurrence of the senate.

Madison admitted the arguments against placing pardons of treason in the
legislature, but the power is so inappropriate in the executive that he were
prefer to see it in former -- perhaps the senate as a council of advice with
the president.

Randolph worried too much about the dangerous combination of the president and
the senate to admit them into a share of that power.

Mason thought the senate had too much power, but the legislature as a whole
can be trusted with it.  The senate must concur and the president can require
two-thirds of both houses.

On the motion of Randolph, it was lost.

G. Morris moved, seconded by Sherman, to append to clause (a) -- making of
treaties and appointments -- "but the Congress may by law vest the appointment
of such inferior officers as they think proper, in the president alone, in the
courts of law, or in the heads of departments."(II, 627)

Madison thought if it was necessary it did not go far enough.  Superior heads
of offices should sometimes appoint lesser officers beneath them.

G. Morris thought [you guessed it] it was unnecessary.

On the question, it passed unanimously in the affirmative.

Article II, Section 1, clause (b) -- election of the president.  The phrases
"and not per capita" and "by the representatives" struck out as superfluous.

Article II, Section 2 again.  After "officers of the US whose appointments are
not otherwise provided for" added "and which shall be established by law".

Article III, Section 2, paragraph 3 -- trial of crimes.

Pinckney and Gerry moved to append "and a trial by jury shall be preserved as
usual in civil cases."(II, 628)

Gorham (and King) thought there was too much difference between the
constitution of juries and trials in the different states to guarantee this.

C. C. Pinckney thought the clause would be embarrassing in the constitution.

The motion was lost.

Article IV, Section 2, paragraph 3 -- fugitive slave clause.  The word
"legally" was argued as ambiguous -- "favoring the idea that slavery was legal
in a moral view"(II, 628) -- and was struck out.  The phrase "under the laws
thereof" added after "state".

Article IV, Section 3 -- admission of new states.

Gerry moved to include the admission of "a state and part of a state"(II, 628)
which was disagreed to by the majority as already implied by the present
wording.

Article IV, Section 4 -- guarantee republican government.  After "executive"
added "when the legislature cannot be convened".(II, 629)

Article V -- amendments.

Sherman worried that even three-fourths of the states might do something fatal
to other states -- e.g. abolish them or remove their representation from the
senate.  He thought the particular wording of the slave trade clause be
generalized to guarantee that no state would be deprived of its internal
police or equality in the senate.

Mason did not approve of the plan of amendment in the constitution.  Since
amendments by any method depend on Congress, no amendments will ever be
attained if the government becomes oppressive -- and it will.  [There was some
sort of kerfuffle about this incident which I don't fully understand.  See
App. A CCLXIX & CCLXXXI.]

G. Morris and Gerry moved to amend so as to require a convention on
application of two-thirds of the states.

Madison did not see why, if two-thirds of the states wanted an amendment,
Congress would not have the count to propose the same thing.  He did not
object to providing conventions for the purpose but thought that difficulties
might arise with respect to the form, quorum, etc.

On the question, it was agreed to unanimously.

Sherman moved to strike out "three-fourths" which was lost.

Gerry moved to strike out "or by conventions in three-fourths thereof" which
was lost.

Sherman moved for his previous suggestion that no state should be affected in
its internal police, etc.

Madison thought that this was a slippery slope.  Include this provision and
every state will insist on them for boundaries, exports, etc.

On the question, it was lost.

Sherman moved, seconded by Brearly, to strike out Article V altogether which
was lost.

G. Morris moved "that no state, without its consent, shall be deprived of its
equal suffrage in the senate."(II, 631)  Many of the small states were
apparently murmuring about this fear.  The motion passed without debate.

Mason did not want a bare majority to pass navigation acts.  It would raise
the price of freight, and a few merchants in Philadelphia, New York, and
Boston would monopolize the trade of southern staples.  He moved "that no law
in nature of a navigation act be passed before the year 1808 without the
consent of two-thirds of each branch of the legislature"(II, 631) which was
lost.

Randolph criticized the indefinite and dangerous power given to Congress.  He
moved "that amendments to the plan might be offered by the state conventions,
which should be submitted to and finally decided on by another general
convention".  The passing of this motion would be the only way he would put
his name to the plan.  He did not know if he would oppose the plan in his
state convention, but he wanted to leave himself open to the possibility based
on the plan as it was now constructed.  [Randolph, in the Virginia house of
delegates, defends this and previous motions on the grounds that it resembles
more closely the ratification of the original AoC.(III, 125)  He goes into
more details of his reasons in III, 123-7.]

[Washington writes to Randolph in 1788 that when Randolph made this motion
Washington "entertained a latent hope [...] that a more perfect form would be
agreed to".(III, 242)  However, by that point he is convinced that another
debate over the constitution would produce even more anger and confusion.  The
only possibility he can see now is that the constitution as drafted will be
ratified or the union will be dissolved.(III, 242)  Madison agrees.(III, 354)]

Mason seconded the motion.  He was certain the current constitution would end
in a monarchy or an aristocracy.  The people have not gotten a say.  A second
convention will be able to take their views into account.  He will not sign or
support the plan either here or in Virginia.  Like Randolph, he would be
willing to sign with the provision that another convention would be called.
[He goes into more details of his reasons in II, 637-40.]

[Madison to Washington criticizes Mason's objections.  Some he feels are
over-reactions.  Others he thinks Mason waited too long to so vociferously
oppose.(II, 129-131)]

Pinckney was not without his own objections to the plan, but another
convention would do nothing but add confusion to the effort.  The states will
never agree with one another in their amendments.  If this constitution fails,
the ultimate decision will be made by the sword.  For that reason, the plan
has his support.

Gerry enumerated the objections which prevented him from signing the plan:
1. the duration and reeligibility of the senate
2. the ability of the house to conceal their journals
3. the authority of Congress over places of election
4. the unlimited power of Congress over their own salary
5. Massachusetts does not have enough representatives in the house
6. three-fifths of slaves are to be represented as if they are free citizens
7. the power to regulate commerce will lead to corporations and monopolies
8. the vice-president is head of the senate

He could support the plan only if the following errors were corrected:
1. the power of the general legislature to make whatever laws they determine
   are necessary and proper
2. the power to raise armies and money without limit
3. to establish tribunals in civil cases without juries which will turn the
   supreme court into a
   [Star Chamber](https://en.wikipedia.org/wiki/Star_Chamber)

[The star chamber "became synonymous with social and political oppression
through the arbitrary use and abuse of the power it wielded."(see wiki link
above)]

The best fix he could see would be to provide for a second general convention.
[L. Martin defended Gerry's decision that his "opposition proceeded from a
conviction in his own mind that the government, if adopted, would terminate in
the destruction of the states and in the introduction of a kingly
government."(III, 260)]

On the motion for a second general convention, it was lost unanimously.

On the question to agree to the constitution as amended, it passed unanimously
in the affirmative.(II, 633)  Five hundred copies were ordered to be
printed.(II, 634)

The committee adjourned.  Washington writes in his diary that the convention
did not adjourn until 6pm.(III, 81)

# September 17, Monday

An unknown author relays an anecdote of Franklin to Thomas Jefferson the day
before (Sunday).  He called all the Pennsylvania delegates together saying
that he knew they had many doubts and objections to the system, that even he
had many objections to the system.  He encouraged them, however, to support
the system anyway because it was the best system that could have been framed
given the circumstances.  "I repeat, I do materially object to certain points
and have already stated my objections -- but I do declare that those
objections shall never escape me without doors as, upon the whole, I esteem
the constitution to be the best possible that could have been formed under
present circumstances."(III, 105)

The constitution was read in its entirety to the assembled convention.

Franklin had prepared a speech which Wilson read on his behalf.  He urged that
all the delegates would sign the document and present a unified front of
support for it regardless of their concerns because (he felt) no better
constitution could be produced given the present difficulties.  The current
plan can be well administered, and it will only become a despotism "when the
people shall become so corrupted as to need despotic government, being
incapable of any other."(II, 642)  He moved that the signatures to the
constitution appear under the heading, "Done in convention by the unanimous
consent of the states present the 17th of Sept. -- in witness whereof we have
hereunto subscribed our names."  The ambiguity of the phrasing had been chosen
by G. Morris to gain the signatures of the dissenting members (Randolph,
Mason, and Gerry).(II, 643)  Pinckney records Franklin saying, "That the
opinions of the errors of the constitution born within the walls of the
convention should die there and not a syllable be whispered abroad."(III, 301)

Gorham hoped it was not too late to make a change and moved that the
representation of the house be changed from one for every forty-thousand to
one for every thirty-thousand.  This would reduce some of the objections to
the plan.  King and Carroll seconded.

Washington rose to put the question and made a few observations.  There should
be as few reasons to object to the plan as possible.  He had found this to be
among the exceptionable parts of the plan.  It is a late amendment, but it
will give much satisfaction to see it adopted.

[L. Martin objected to the motion when he heard about it believing that it put
more power/representation in the larger states to the detriment of the smaller
states.(III, 199-200)]

[Sherman, after the ratification of the constitution, said that if he had to
do it again, he would have preferred one representative in
forty-thousand.(III, 358)]

On the question, it passed unanimously in the affirmative.

On the question to agree to the signing of the constitution, it passed
unanimously in the affirmative.

Randolph said that he refused to sign not because he intended to oppose the
plan but because he did not want to tie himself to it when he was certain that
it would fail to be ratified.  Not taking a side would allow him to act
consistent with the public good, whatever that might turn out to be.

G. Morris stated that he had doubts too, but this plan was the best hope for
the union.  Our choice is between a national government and a general anarchy.
He reminded them that signing the document only indicates that the *states*
are unanimous.

Williamson suggested that maybe the signatures could be confined to the letter
to Congress if this would better suit the dissenting members.

Hamilton thought everyone should sign it.  A few, by refusing to sign the
constitution, would create unnecessary obstacles to the plan.  The alternative
is anarchy and convulsion.

Blount had considered not signing the plan but was relieved by the final form
and would be willing to sign to attest that the plan was the unanimous act of
the states.

Franklin encouraged Randolph to sign the plan.

Randolph asserted again that his not signing it -- though he might regret it
one day -- was dictated by his conscience.  He believed that presenting the
constitution as the *final* plan to be accepted or rejected with no provision
for amendment would really result in the anarchy and convulsions which were
feared.

Gerry had treated the drafts of the plan with the respect he felt they
deserved, but now that it was an act of the convention, he respected it as
such.  He hoped it would not appear disrespectful to acknowledge that he
feared the civil war that might result from the resistance to this
constitution.

C. C. Pinckney -- though he pledged himself to sign it -- stated that they
would gain few converts with the ambiguity of the proposed form of signing it.
The form should be more candid.

Franklin thought it was too soon to be too direct with their approval before
Congress and their constituents have approved it.

Ingersoll considered the signatures to only be a recommendation which did not
pledge the signers to support the constitution at all events.

On the question of the form of the signature, it passed in the affirmative.

King suggested that the journals of the convention ought either to be burned
or placed in the care of the President.  If made public, they would be taken
out of context to prevent adoption.

Wilson preferred the latter option.  There could be false reports and rumors,
and there should be some means of contradicting them if necessary.

On the question of handing over the journals to the President, it passed in
the affirmative.  [William Jackson handed over the journals "after burning all
the loose scraps of paper which belong to the convention".(III, 82)]

When asked what was to be done if someone asked for the journals, it was moved
and passed "that he retain the journal and other papers, subject to the order
of Congress, if ever formed under the constitution."(II, 648)

The delegates then signed the constitution.  [Dickinson, who is absent, asked
Read by letter to sign his name to the constitution for him.(III, 81)]

McHenry signed and records in his journal his reasons for doing so even though
he opposed many parts of it:(II, 649-50)
1. He distrusted his own judgment when placed against the majority of able and
   patriotic men.
2. The constitution provides a means of amendment for any errors.
3. Weighing the inconvenience and evils of the present system against the
   possible evils and probable benefits and advantages of the new system, he
   determined that it deserves his support.

Franklin looked at the president's chair, on the back of which a rising sun
was painted.  He remarked, "Painters had found it difficult to distinguish in
their art a rising sun from a setting sun.  I have often and often in the
course of the session, and the vicissitudes of my hopes and fears as to its
issue, looked at that behind the President without being able to tell whether
it was rising or setting:  But now at length I have the happiness to know that
it is a rising and not a setting sun."(II, 648)

The committee adjourned sine die.
