# [Vices of the Political System of the United States](http://teachingamericanhistory.org/library/document/vices-of-the-political-system/)

1.  Failure of states to comply with constitutional requisitions
2.  States encroaching on federal authority
    * Georgia making treaties with the Indians
    * Massachusetts raising troops
3.  States violating or not abiding by treaties that the confederation made
4.  States treat one another like foreign countries rather than a union
    especially in trade
5.  Lack of concern by individual states in matters that concern the whole
    union
6.  There is no protection in the states to prevent sedition
    1. A minority of 1/3, if they possess sufficient military skills, may
       conquer the majority
    2. Poverty may exclude too many people from suffrage, and those people
       will have no recourse but to try to overthrow the government
    3. "Where slavery exists the republican Theory becomes still more
       fallacious."
7.  The federal government lacks the ability to sanction or coerce the states
8.  State laws trump acts of Congress
9.  The states have too many laws
10. The state laws change so much as to produce instability
11. The injustice of these laws brings into question the fundamental
    principles of republican government
    1. Representatives care only about their own interests and ambitions.
       This would be no trouble if they were ousted in the next election, but
       they are able to dupe others into believing that their selfish
       interests are the common good.
    2. Because of the weakness of the federal government, there is nothing to
       check the tyranny of the majority over the minority.  "In absolute
       Monarchies, the prince is sufficiently, neutral towards his subjects,
       but frequently sacrifices their happiness to his ambition or his
       avarice. In small Republics, the sovereign will is sufficiently
       controuled from such a Sacrifice of the entire Society, but is not
       sufficiently neutral towards the parts composing it. As a limited
       Monarchy tempers the evils of an absolute one; so an extensive Republic
       meliorates the administration of a small Republic."
12. "Impotence of the laws of the States"
