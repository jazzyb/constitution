# Prelude

[NOTE:  There are two Pinckney's both from South Carolina at the convention --
Charles Pinckney, 29, and his first-cousin-once-removed Charles Cotesworth
Pinckney, 32.  This leads to some confusion in the records.  When "Mr. C.
Pinckney" or "General Pinckney" is mentioned, I assume the author is speaking
of Charles Cotesworth Pinckney who I refer to below as "C. C. Pinckney".  When
only "Mr. Pinckney" is mentioned, I assume they are speaking of Charles
Pinckney referred to below as just "Pinckney".]

[TODO:  Somewhere in the introduction we need to define meanings for various
words:  republic, democracy, federal, national, anarchy (see
van-cleve-government.md), etc.]

[TODO:  Translate the foreign letters in Vol. III (e.g. XLI)
 EDIT:  It looks like the letters are on
 [wikisource](https://en.wikisource.org/wiki/The_Records_of_the_Federal_Convention_of_1787/Volume_3/).
 You can use Google Translate to get the translation.]

On February 21, 1787 Congress, then operating under the Articles of
Confederation, passed a resolution to make "alterations" and remedy "defects"
that existed in the AoC.  The convention was set for May 14th of the same year
to be held in Philadelphia "for the sole and express purpose of revising the
Articles of Confederation."(Farrand, Vol. III, pp. 13-14)

All the states save Rhode Island would elect delegates to represent then in
the convention.  The measure was voted in the negative by the house of
Assembly [the what?] although it was supported by the governor and upper
house.(Farrand, Vol. III, p. 47)  Those "Merchants Tradesmen" who still
supported the measure wrote instructions via James Varnum on what they would
like to see in the alterations.  They were mostly economic -- that the
regulation of both foreign and domestic commerce be run by the national
government -- no taxes on goods traveling between states, a single national
impost paid on goods at the port of entry, and a national currency -- also to
consider Vermont to be entered into the union.(Farrand, Vol. IV, p. 2; Vol.
III, pp. 18-19)

On June 10 after considerable progress had been made on the way to the
eventual constitution, James Madison wrote to James Monroe saying he would be
concerned if Rhode Island sent delegates so late into the convention:  "If her
deputies should bring with them the complexion of the state, their company
will not add much to our pleasure, or to the progress of the business."(IV,
67)

Jonathan Dayton would later say, "Look through that instrument [the
constitution] from beginning to end, and you will not find an article which is
not founded on the presumption of a clashing of interests."(III, 400)

[III, CLVIII: Interesting view of the convention by one delegate who did not
agree.]
[See Appendix B about letters of appointment for various states.]

[See:  III, CCCCI:  Madison's own account of the atmosphere leading up to the
constitutional convention.]
\[Additionally, see Madison's [Vices of the Political System of the United States, April 1787](https://founders.archives.gov/documents/Madison/01-09-02-0187)]

## Questions
* What happened in Annapolis prior to the convention?
* What did the various delegates and state legislatures anticipate would
  happen at this convention?
* What did Madison, Hamilton, et al have planned?

# May 14, Monday

George Washington was among the first delegates to arrive in Philadelphia on
scheduled day of the convention on a drizzly Monday morning(Farrand, Vol. IV,
p. 327).  [CORRECTION:  G.W. arrived the Sunday evening before.(III, 20; IV,
6)]  By the end of the day only some of the delegates from Virginia and
Pennsylvania would have arrived.(Farrand, Vol. IV, p. 1)  It was decided [by
whom?] that the convention would meet once a majority of states were
present.(Vol. I, p.  1; IV, 5, 20)  A state would be considered "represented"
when at least two delegates from that state were present.(IV, 8)

Washington was one of the biggest proponents of replacing the AoC with a
stronger central government.  He blamed the impotence of Congress with
prolonging the war and increasing the expense of it unnecessarily.(Van Cleve,
p. 31)  [Reread *Patriots* for more info on the troubles Washington went
through during the war that led to this belief.]

[See also pp. 125-138 in *Shays's Rebellion* by Leonard L. Richards.  The
(mostly wrong) news about Shays's Rebellion stirred Washington out of his
retirement and framed many of the debates at the convention.]

# May 18

By the end of the week, the delegates were getting nervous and wondered if the
convention would even get started.  Jared Ingersoll, a delegate from Penn.,
feared that the "federal Government seems to be expiring."(IV, 7)  Questions
throughout the week regarding how long it might take to complete the project.
Ingersoll continues, "how soon any other System may get established, it is
impossible to predict."  G.W.:  "it is impossible to say when it will end.  I
have not even a hope that it will end with dispatch."(IV, 6)  The two ate
together the next night.

# May 19

William Pierce, delegate from Georgia, desired a government which respected
the liberties of the people but able to do what needed to be done quickly.  He
felt that slow, minor changes to government were best; otherwise, the people
cannot grow accustomed to the law.  Hence, he did not want too many changes to
the current confederate system.  He thought that the country must not embrace
monarchy or the whole country would descend into anarchy.  For him there is a
lesson to learn from Rome:  There must be a set constitution of laws or a
constantly shifting government will give way to despotism.(IV, 9-10)  [Compare
his thoughts with the various places that Montesquieu mentions the Romans.]

# May 21

Costs for room and board during the convention would exceed £200.(IV, 12,
footnote 1)

Henry Knox wrote to John Sullivan to urge the New Hampshire delegates to
arrive to the convention quickly.  He feared that the country was close to
anarchy [that word again], and the convention was the only way to avoid the
"most flagitious evils that ever afflicted three millions of freemen."(IV,
12-13)

[From wikipedia: "Knox in early 1787 sent George Washington a draft proposal
for a government that bears significant resemblance to what was eventually
adopted." -- need to track that down if it exists.  Washington made
[notes](notes/washington-notes.md) on it.]

George Read in a letter to John Dickinson writes that he fears the large
states will push the small states around at the convention.(III, 26)

# May 22

The North Carolina delegation becomes the fifth state to be represented.(IV,
14)

The delegates from Massachusetts were given orders that there must be three
(as opposed to two) delegates to represent their state.(III, 584; IV, 14)

# May 23

Daily the delegates have met at the State House waiting on a quorum to vote.
The states represented are New York, Pennsylvania, Delaware, Virginia, North
and South Carolina.  There is also a delegate each from Massachusetts,
Maryland, Georgia, and New Jersey.  They wait on any other delegate from those
last states (or two more from Mass.) to make a quorum.(IV, 16)

Members have been talking amongst themselves informally about the form of
government they intend.  They do not talk of the states going their separate
ways, but the possibility is there.  Most favor an organization of two
branches of legislature and one of the executive, but no one can agree on how
extensive their powers will be.  Pinckney of S. Carolina has been shopping his
proposal around.  It is a combination of the Articles of Confederation with
the various state constitutions.(IV, 16-17)

## Questions
* How were the state constitutions organized before the convention, and how
  much did their organization lend itself to the eventual form of the US
  Constitution?

# May 24

William Grayson writes to James Madison with rumors that the Eastern states
have a plan for amendments and they will not depart from it:  a single
legislature of two houses, the lower house triennially elected, a senate and
executive with longer terms.(III, 26)  [I assume he is reporting what he hears
in Congress.]

# May 25, Friday

| State | Delegate |
|-------|----------|
| Massachusetts | Rufus King |
| New York | Alexander Hamilton |
|          | Robert Yates |
| New Jersey | David Brearly |
|            | William Churchill Houston |
|            | William Paterson |
| Pennsylvania | Gouverneur Morris |
|              | Robert Morris |
|              | Thomas Fitzsimons |
|              | James Wilson |
| Delaware | Richard Basset |
|          | Jacob Broom |
|          | George Read |
| Virginia | George Washington |
|          | Edmund Randolph |
|          | John Blair |
|          | James Madison |
|          | George Mason |
|          | George Wythe |
|          | James McClurg |
| North Carolina | Alexander Martin |
|                | William Richardson Davie |
|                | Richard Dobbs Spaight |
|                | Hugh Williamson |
| South Carolina | John Rutledge |
|                | Charles Cotesworth Pinckney |
|                | Charles Pinckney |
|                | Pierce Butler |
| Georgia | William Few |

[Note that Yates makes no mention of the delegate from Georgia.  Some of these
delegates might not have arrived until the next Monday or Tuesday.]

The delegates met in the long room of the State House.  They took the upper
floor, and the street below was covered with earth so the horse hooves would
not disturb the assembly.  This same building held the Supreme Court [of
Pennsylvania I presume] which was in session during the convention.(Vol. III,
pp. 58-59)  [Manasseh Cutler in the same journal entry from June 18 tells a
story about Ben Franklin about to make a joke involving a two-headed snake
which couldn't make up its mind on how to go around a branch and how it was
similar to the new United States, but he was silenced before he could continue
because the convention was supposed to be secret.]  [Thomas Jefferson
disagreed with the decision to keep the proceedings secret:  "I am sorry they
began their deliberations by so abominable a precedent as that of tying up the
tongues of their members.  Nothing can justify this example but the innocence
of their intentions and ignorance of the value of public discussions."(III,
76)]

The first order of business was to elect the officers for positions in the
convention.  Washington was nominated for president of the convention by
Robert Morris and elected unanimously.  Ben Franklin, notes Madison, would
have been the only other member of the convention to be nominated to that
office.  Franklin had even planned to nominate Washington himself, but the
poor weather (59 degrees and rain all day) and health confined Franklin to his
home.(I, 4)

Washington humbly thanked the convention for the honor they conferred on him.
He felt embarrassed never having held such an office before and begged the
forgiveness of the members for any errors that he might make in his duties.(I,
3-6)  His breakfast companion had noted his "very grave" attitude that
morning.(IV, 20, footnote 12)

Next the secretary was to be elected.  James Wilson nominated Temple Franklin
[not on the list of delegates in the journal].  Hamilton nominated William
Jackson.(I, 4)  Jackson was elected 5 to 2 due primarily to his influence and
less to his fitness for the job.(I, Intro, footnote 6)  We have letters as
early as late April wherein Jackson begs George Washington and William Samuel
Johnson of Connecticut for their support for his appointment as
secretary.(III, 18)  It would be Madison who would become the de facto
secretary of the convention having been the one to write the most detailed
journals.  Several members took pains to make sure he had copies of their
speeches and motions.(I, xvi)

Nathaniel Gorham would frequently serve as the "chair" of the convention,
meaning he, instead of Washington, would preside over convention debates.
Interestingly, during Shays's Rebellion in Nov. 1786 before the convention:

> [Gorham] became desperate.  In his fear of "democratic excesses" and "mob
> rule," he called for a new king, someone to replace the ousted George III, a
> genuine European blueblood to rule over a limited American monarchy.  He
> tried to sound out Prince Henry of Prussia... The prince rejected the
> proposal as foolhardy.(Richards, *Shays's Rebellion*, 128)

[Door keeper:  Joseph Fry]
[Messenger:  Nicholas Weaver]

The delegates from Delaware informed the convention that they were being held
by their legislature to observe the fifth article of the AoC guaranteeing a
single vote to each state.(I, 6)

Three members were chosen to draw up and order of rules to follow throughout
the convention:  Wythe, Hamilton, C. Pinckney.  The convention was then
adjourned until 10am Monday (May 28).(I, 4, 6)

## Questions
* How did the other delegates (or state legislatures) feel about Delaware's
  orders?
* Were any other delegates given orders about how to vote or how to represent
  their state?

# May 27

James Madison:  "Every reflecting man becomes daily more alarmed at our
situation."(III, 28)

# May 28, Monday

| State | New Delegates |
|-------|---------------|
| Massachusetts | Nathaniel Gorham |
|               | Caleb Strong |
| Connecticut | Oliver Ellsworth |
| Delaware | Gunning Bedford |
| Maryland | James McHenry |
| Pennsylvania | Benjamin Franklin |
|              | George Clymer |
|              | Thomas Mifflin |
|              | Jared Ingersoll |

Eight states represented.  [Yates claims nine, the last being Connecticut(I,
13), but I don't see how that is possible with only one delegate, and no one
else mentioning them.]

Wythe opened the meeting that day by reporting the rules the committee had
drawn up for the convention.  [Nothing unusual stood out.]  There were a few
motions made to the rules.  Rufus King (seconded by Mason), Butler, and
Spaight all had corrections they wished in the rules.  The convention
adjourned for the rules committee to make the requested changes which were
eventually added the next day.(I)

After the rules were read through twice, a letter was read from merchants and
tradesmen of Rhode Island who were unhappy with the state's refusal to send
delegates.  The Rhode Island merchants voiced two general opinions:  First,
that the regulation of both foreign and domestic commerce rest in the National
Council.  Second, that Congress have power to compel states to support
national purposes.  Since they themselves were not delegates, the merchants
refused to enter into any details of their vision of the national government,
only that they be such as "strengthen the union, promote commerce, increase
the power, and establish the credit of the United States."(III, 19)

## Questions
* What were the reasons the lower house of Rhode Island had for refusing to
  send delegates?

# May 29, Tuesday

| State | New Delegates |
|-------|---------------|
| Massachusetts | Elbridge Gerry |
| Delaware | John Dickinson |

Final rules are presented and agreed to.

Randolph, after expressing the faults of the present system, presents what
would become the "Virginia Plan".  This plan had been worked on since the
Virginia delegates had arrived in Philadelphia(III, 23) and drafted primarily
by Madison[source?].  Randolph, being the governor of Virginia at the time,
was chosen for its sponsor(III, 409).  Madison had presented a draft of the
plan to Randolph one month prior.(IV, 549)  [Randolph, even though he
presented the initial plan, refused to sign the final constitution.  See his
reasons for this III, 307-9.]

[See Madison biography for the sources of the Virginia Plan.]

Randolph began by suggesting four topics of inquiry:
1. The properties that the federal government ought to possess
2. The defects of the Articles of Confederation
3. The danger of the dissolution of the Confederacy
4. The proposed solution (Virginia Plan)

The ideal government should secure against foreign invasion, prevent
dissensions between states or rebellions from within, support the states with
blessings they could not produce themselves (e.g. improvement of inland
navigation, agriculture, and manufacturing), compel states by force to act in
the interest of the union, and be above and supersede the state constitutions.

While the original authors of the articles had performed admirable work, they
could not foresee the dangers that now gripped the confederacy.  The federal
government could not prevent foreign invasion and could not prevent states
from starting wars.  Neither militias nor drafts being sufficient for defense,
a standing army was necessary, and this required money which the federal
government did not have.  The federal government could not arbitrate quarrels
between the states.  There were good economic reasons for controlling domestic
and foreign commerce at the national level.  The federal government, being
inferior to the states, could not protect itself from the state governments.

While Randolph presented his analysis, Gunning Bedford silently noted his own
commentary:  Several of the above points are solved by granting Congress the
power to demand soldiers and money from the states.  Regarding the
"encroachment of the states", the legal boundary between the powers of the
federal and state governments should be as precise as possible, and any
questions should be determined by a judiciary.  Regarding improvement and
"public works", the federal government has no business having such power.  Not
only is it the state's own prerogative, but "to erect great works would allow
[the federal government] to draw money independent of the states and would end
in aristocracy, oligarchy, and tyranny."(IV, 27)

Randolph continued:  Government everywhere was lax and courted anarchy.
McHenry quotes Randolph: "Our chief danger arises from the democratic parts of
our constitutions.  It is a maxim which I hold incontrovertible that the
powers of government exercised by the people swallows up the other branches.
None of the [state] constitutions have provided sufficient checks against the
democracy."(I, 26-7) [Further details by McHenry at III, 145-6 when he spoke
before the Maryland House of Delegates.]

Randolph then proposes the solution -- fifteen resolutions that would come to
be known as the Virginia Plan.  [The most authoritative text for the plan is
Madison's.(I, 20-22)]

\[see the [side-by-side with amendments](#comparison-of-original-and-amended-randolph-resolutions)]

The Virginia Plan consists of the following resolutions:
* The Articles of Confederation ought to be corrected and enlarged to
  accomplish the objectives of "common defense, security of liberty, and
  general welfare".
* A national legislature of two houses should be elected based on the
  populations of free individuals of the states.
* The first house should be elected by the people.
* The second house should consist of members elected by the first house out of
  a pool of nominees selected by the state legislatures.
* Each house has the right to originate bills.
* The legislature has the power to:
  * legislate in any way which states may be "incompetent" or where individual
    legislation would upset the peace of the union
  * negate all laws passed by the states
  * call the full force of the union against any state who does not fulfill
    its duties under the union
* A national executive is appointed by the legislature who is vested with all
  the executive powers of congress granted by the AoC.
* The national executive is limited to a single term.
* A national judiciary is established as one or more supreme and inferior
  tribunals to determine questions and crimes of national significance.
* The national executive and national judiciary together have veto power over
  legislative acts.
* Provision made for admitting new states.
* Territory and republican government are guaranteed to each state.
* A means of amending the "Articles of Union" should be created that does not
  require the assent of the national legislature.
* The executive and judiciary are bound by oath to support the Articles of
  Union.

The plan sought to establish a partly national government in place of the
purely federal government of the AoC believing that it was impossible for the
federal government of the AoC (even amended) to accomplish the objectives of
the first resolution.  [See III, CCCLXXII for Madison's defense of the
objectives.]  For Madison the term "national", as opposed to "federal", meant
that the new government was to act on and receive its sanction from the people
rather than the states.(III, 473-475)

Afterwards, Pinckney submitted his own plan to the committee.(III, 604-609)
While the Virginia delegates worked out their plan amongst themselves before
the convention met, Pinckney had discussed his plan with (at least) George
Read.(III, 25)  Pinckney had written a speech to go along with his submission,
but due to lack of time, it was never read aloud.(III, 106-123)  [*This speech
should be viewed with a grain of salt since it was not printed until after the
convention, and Pinckney may have modified some of his thoughts.  George
Washington says of its publication later that year:  "Mr. C. Pinckney is
unwilling... to lose any fame that can be acquired by the publication of his
sentiments."(III, 131)*]  The year before Pinckney had been a member of the
Continental Congress and had helped to draw up a report recommending seven
important changes or amendments to the Articles of Confederation.(III, 602)
The plan he presented was largely based on his previous work and the New York
constitution of 1777.

In the letter he originally intended to read to the delegates, Pinckney points
out that the main issues with the AoC were (1) placing in a single body all
executive, legislative, and judicial powers when those ought to be held by
different bodies so as to mutually check one another's power and (2) a
document that is vague and lacking in all manner of issues that now seem
obvious, e.g. how to compel states to oblige by their duties under the
confederation.  Given these defects he advises the delegates to treat the AoC
as no more than a good starting point for a better frame of government.(III,
108)  The present understanding of the sovereignty of the states must be
relinquished; the state legislatures must be limited to local concerns.  If
any federal power rests within the control of the state legislatures, they
will defeat its operation.(III, 112-3)

There is no surviving text of the original plan as it was presented to the
convention, and Pinckney seemed to update and change his notes throughout the
years, so it is very doubtful that his later notes (which resemble the
final constitution with incredible prescience) are accurate.  However, based
on letters before the convention, debates recorded during it, and some of
Pinckney's writings immediately after, the combined scholarship of A. C.
McLaughlin, J. Franklin Jameson, and Max Farrand were able to construct a
"fairly good idea of its original form".  The following information is based
on that reconstruction.(Appendix D)

The layout of Congress in the AoC was 2-7 delegates selected by the state
legislatures, and each state would have one vote.  Pinckney suggests two house
of Congress, a Senate and a "House of Delegates".  Each state would get a
number of delegates in proportion to their population (with Blacks counting as
three-fifths).  The Senate was to be appointed by the delegates.  Each member
of both houses would get one vote.(III, 605)  Anywhere the AoC required a
majority of nine states, Pinckney suggested the assent of two-thirds of both
houses.(III, 608)

Pinckney points to the parliament of Great Britain and the state legislatures
as proof that proportional representation works.  "Montesquieu, who had very
maturely considered the nature of a confederated government, gives the
preference to the Lycian, which was formed upon this model."(III, 109)  [Might
be a reference to Book 9?]

The federal Congress has the exclusive power to (listing only differences
with AoC):
* raise a military and select their officers
* levy public taxes
* regulate trade between states
* levy duties on imports and exports
* establish post-offices and raise revenue from them
* coin money
* appoint officers for the departments of
  1. Foreign Affairs
  2. War
  3. Treasury
  4. Admiralty
* appoint judges for a federal judicial court [similar to the Supreme Court]
* institute and appoint judges for "Courts of Admiralty" in each state for
  hearing and determining all maritime crimes
* order and regulate the militia of any state

Pinckney deems the afore-mentioned departments or "Cabinet Councils" necessary
to the operation of a modern state where situations or connections oblige them
to consult or interact with other powers.(III, 111)

The establishing of post-offices and granting of regulation of commerce to the
federal government is so to give the federal government a revenue separate
from the states and not under their influence.(III, 116-7)  Likewise, the
exclusive power to coin money is to secure federal revenue.  If states have
the right to issue their own currency, then there is nothing preventing them
from forcing the federal government to accept their base or deprecated
currency in payment.(III, 118)

The federal regulation of the state militias is necessary not just for the
speedy reaction to foreign invasion, but also because the militia is the only
force the confederacy possesses, it is necessary for the coercion of wayward
member states.(III, 118)  Without coercion the states will -- as they have
shown in the past -- neglect their duties.(III, 119)  It should be noted,
though, that it does not sound like he supports a standing army.

The members of both houses would jointly elect a president who would be vested
with the executive authority of the United States.  Pinckney suggests the
length of his term be seven years, but he is not devoted to any particular
time period.  He suggests also that there be no term limit.  By making the
office possibly perpetual, he reasons, the president would be encouraged to
continue his good behavior, and thereby, the office would become more
respectable.(III, 111)

The duties of the president would be to inform Congress of the state of the
union, to recommend matters to their consideration, to collaborate with the
executives of the states, to advise with the cabinet council, and to act as
the commander-in-chief of the army and navy.  Additionally, he would have the
right to convene or prolong the legislature "under extraordinary
circumstances".  He would be removable by impeachment [even though Pinckney is
against this on July 20].(III, 606)  The House of Delegates would have the
authority to impeach.  The Senate and federal judicial court would try the
impeachment.(III, 608)

Article XIV of Pinckney's plan establishes the means of compelling and
enforcing the payment of quotas of each state.  Article XV establishes the
means of admitting, annexing, dividing, and consolidating each state (with the
consent of the states in question).  Pinckney thinks the inequality between
small and large states will eventually correct itself.  The western
populations at the frontiers of large states will wish to split from the
original state.  Alternatively, some small states due to lack of power will
wish to combine with larger states.(III, 120)

(After the convention, Pinckney would claim the plan also invested in the
federal government the exclusive right to establish the means of citizenship
and naturalization, the seat of the government, copyrights for authors, and a
"Federal University" as well as guaranteeing to the people habeus corpus,
freedom of the press, and prevention of religious tests for office.  However,
it is doubtful based on the record of the debates that these points were a
part of the original plan.(III, 609))

Pinckney mentions that the majority of citizens see the addition of national
power into the federal government as an improper attempt to gain dangerous and
unnecessary power.  Such people have no idea of the weaknesses and disorders
present in the system.  The dangers of disunion are worse than the dangers of
consolidation of power at the national level.  "It is the anarchy, if we may
use the term, or worse than anarchy of a pure democracy, which I fear."(III,
114-5)  Pinckney sees a lesson in the corruption of the confederation of the
Greek city-states that the US should avoid.  The opposition to the authority
of the federal head weakened the Greek confederation and opened the door to
foreign invasion.

[SUPPOSITION:] These two proposals would set the stage for the two primary
philosophical camps that would debate in the coming weeks in the convention.
The Pinckney Plan sought to reform the Articles of Confederation, while the
Virginia Plan sought to completely replace the confederacy with a "strong
consolidated union".(I, 24)

## Questions
* What aspects from Madison and Pinckney's plans were borrowed from previous
  political philosophers?
* How do we reconcile Randolph's negative view of "democracy" with Madison's
  insistence that the national government was to be sanctioned by the people?

# May 30, Wednesday

| State | New Delegates |
|-------|---------------|
| Connecticut | Roger Sherman |

Two questions with respect to the Virginia Plan were debated this day:
1. Should the federal government be replaced with a national government?
2. How should the states be represented in the national legislature?

The debate on the Virginia Plan began with delegates questioning the meaning
of the words "national" (as in national government) and "supreme" (as in
supreme legislature).  Pinckney, who had submitted the opposing plan the day
before, asked Randolph if by "supreme" he meant to abolish the state
governments altogether.

Randolph replied that he intends to take no more power from the states than
would allow the national government enough power to protect and defend itself.

Butler admitted that he had entered the convention worried about giving too
much power to the federal government, but his concerns were with the power
given to the single body of Congress.  However, if the power of the new
government is to be divided among three bodies, he is more open to the
proposition.  He hoped that debate would shed more light on the matter and
would like to hear if the states can be preserved by any method other than
national government.

C. C. Pinckney voiced his concern that the act of Congress and commission of
delegates which called together this convention did not allow the members to
discuss a completely different form of government than the federal system.

Gerry entertained the same doubt and questioned whether they should even
distinguish between "national" and "federal".  To do so is to question whether
they have the right to propose a totally different government.  To determine
the insufficiency of the AoC or its impossibility of amendment must end in a
dissolution of the confederation.

McHenry in his notes quotes the act of Congress calling the convention
together.  The purpose of the convention is for "revising the articles of
confederation" and to render the "federal constitution" adequate to the
preservation of the union.

Dickinson desires to remain confederated.  He thinks it best to all agree that
the AoC are defective, *then* to proceed to determining the powers necessary
to achieve the objectives for which the confederation was formed.

G. Morris tried to distinguish the difference between a "federal" and "supreme
national" government.  A federal government is a compact between sovereigns
resting only on good faith.  A national government has a complete operation to
compel every part to do its duty.  He supported the national government
because there can be one and only one supreme power in any society.  "We had
better take a *supreme* government now than a despot twenty years hence -- for
come he must."(I, 43)

Mason made two points in support of the national government:  First, coercion
and punishment of delinquent members was necessary.  Second, punishment cannot
be meted out on states collectively; a national government operated on the
level of individuals, rather than states, and could punish just the guilty who
required it.

Sherman agreed that Congress had not been given sufficient power, particularly
the power to raise money.  However, he hesitated to veer too far from the AoC
because it would do no good to produce a set of recommendations or amendments
that the states would then reject.

[Jeremiah Wadsworth (to Rufus King) said of Sherman:  "[He], I am told, is
disposed to patch up the old scheme of Government.  This was not my opinion of
him when we chose him:  he is as cunning as the Devil, and if you attack him,
you ought to know him well; he is not easily managed, but if he suspects you
are trying to take him in, you may as well catch an eel by the tail."(III,
34)]

Read and C. C. Pinckney move to strike the words "national" and "supreme" from
the resolution, but they fail.

The committee next looked to the question of how votes in the proposed
national legislature ought to be decided.  The initial suggestion (by
Randolph) was that representation ought to be proportioned based on the
property of the states or the number of free individuals -- whichever seemed
best.

Madison asked that the question of representation by free individuals be
ignored for the time being since it might occasion debate which would distract
from the general proposition at hand, that being that the current method of
representation needs to be changed.

King pointed out that if they ignored the statement about free individuals,
then the only remaining method would be based on property.  Since the worth of
the states is constantly varying, this would be a poor method of
representation.  Madison consented to King's argument and withdrew his
proposition.

Randolph then moved to change the wording of the proposition under question
such that the suffrage of the national legislature ought to be apportioned
differently than the present system.  However, Read reminded the delegates
that Delaware was prevented by their commission from changing the method of
suffrage.  If this line was followed, the delegates from Delaware might be
required to leave the convention.  [It's worth noting here that this
instruction in the Delaware commission was suggested by Read himself in a
letter to Dickinson the previous January.(III, 575, footnote 6)  Read feared
that proportional representation would effectively prevent a small state like
Delaware from having any say in the new government and would eventually lead
to the downfall of the state.]

G. Morris replied that such an early secession would be lamentable, but the
proposed change was so fundamental a point in a national government that it
could not be dispensed with.

Madison pointed out that equal suffrage among states -- while it made perfect
sense for the confederation -- did not make sense under a national government
any more than equal representation in state legislatures regardless of the
population of a county would.  Read did not seem satisfied by this response.

Several members observed that based on the wording of the Delaware commission,
the delegates would not be required or justified in seceding even if the
motion carried.  However, the committee agreed to postpone the vote and
adjourned.

At the end of the day, George Washington assessed the convention so far in a
letter to Thomas Jefferson:  "The business of this convention is as yet too
much in embryo to form an opinion of the conclusion.  Much is to be expected
from it by some; not much by others; and nothing by a few.  That something is
necessary, none will deny; for the situation of the general government, if it
can be called a government, is shaken to its foundation, and liable to be
overturned by every blast.  In a word, it is at an end; and, unless a remedy
is soon applied, anarchy and confusion will inevitably ensue."(III, 31)

# May 31, Thursday

| State | New Delegates |
|-------|---------------|
| Georgia | William Pierce |

The first resolution taken up by the committee was "that the national
legislature ought to consist of two branches".  The resolution was agreed to
by all present with the lone exception of Franklin who had a personal
preference for a single house of the legislature.  Yates, who preferred to
amend the AoC rather than abolish it, was clearly in disagreement with the
spirit of the convention; however, he notes in his journal:  "As a previous
resolution had already been agreed to, to have a supreme legislature, I could
not see any objection to its being in two branches."(I, 55)

The bicameral legislature had been advocated for by John Adams in his 1776
work, "Thoughts on Government".(III, 33)  The Continental Congress had
requested advice for how the states ought to frame their constitutions.
Adams' "Thoughts" is a response to their request as well as a counter-point to
the political proscriptions within Thomas Paine's "Common Sense".  Prior to
the convention, the work had already been used as a blueprint for the
constitutions of North Carolina, Virginia, New Jersey, New York, and
Massachusetts.
[source](http://oll.libertyfund.org/titles/adams-revolutionary-writings)

Paine, like Franklin the author of the Pennsylvania constitution, supported a
democratic legislature.  He thought the federal congress should have at least
390 members to better represent the people.(Common Sense)  In "Thoughts"
Adams, on the other hand, advises against a single house of Congress.  He
worried that a single house of the legislature would behave like a single
individual -- that it would succumb to its own vices.  He also worried that it
could encroach upon the executive powers.("Thoughts on Government")
[Although, it seems like his second house of legislature, the "council" or
senate,  behaves very much like an executive rather than a legislature.]  [It
is worth noting that Adams is a primary author of the Massachusetts
constitution that was written to ensure the wealthy controlled the Senate of
the state, hinting that he may not have supported the same democratic
sympathies as Paine or Franklin.(Van Cleve, 28-9).]

The next resolution that followed was "that the members of the first branch of
the National Legislature ought to be elected by the people of the several
States."

Sherman objected that the people should have as little to do with the
government as possible.  They are constantly being misled by bad information.
Instead, the members ought to be elected by the state legislatures.

Gerry elaborated upon Sherman's objection:  "The evils we experience flow from
the excess of democracy.  The people do not want virtue; but are the dupes of
demagogues."(I, 48)  Gerry reports that in his home state of Massachusetts,
the people are misled on a daily basis by false reports that no one can
immediately refute.  For example, the people clamor for a reduction in
government salaries, below even that of due provision, particularly against
the governor.  "It would seem to be a maxim of democracy to starve the public
servants."(I, 48)  He relates that the governor of Massachusetts, though still
a republican, had been taught from experience not to wholly trust the will of
the people.

Mason argued that the largest house of the legislature must be elected by the
people.  It should be the "grand depository of the democratic principle" --
analogous to Britain's House of Commons.  We should not think of it as
representing the populations of the states, but also as representing people of
the different districts within the states.  Even in Virginia the people of
different areas have different interests and views.  It is true that lately we
have been too democratic, but we shouldn't let that push us to the opposite
extreme.  Every class of people deserve the same rights.  He wondered at the
indifference of the superior classes to the rest of humanity.  He reminded the
delegates that no matter how elevated their current station, time would
certainly distribute their posterity to the lowest rungs of society.  They had
a familial and *selfish* motive to care as much for the rights of the lowest
members of society as the highest.

Wilson agreed with Mason and likened the form of government that they were
trying to create to a pyramid.  The base of the pyramid should be as broad as
possible by drawing from the greatest number of electors.  No government,
especially a republic, can last long without the confidence of the people.  He
was also concerned about giving too much power to the state legislatures to
choose the national legislature.  After all, it was the states -- *not* the
people -- who had had a more negative interference on the federal government.

Madison considered the popular election of one house of the legislature to be
essential to a free government.  He observed that the upper houses of some
state legislatures were already chosen by electors once removed from the
people.  If the first house is elected by the state legislatures, the second
chosen from the first, the executive appointed by the second, with other
supporting offices and positions filled by the executive, then the rulers will
lose sight of the people and their interests.  He is an advocate for filtering
and refining popular appointments, but as Mason said, opposition to democracy
can be taken too far.  Like Wilson, he felt that the firmest foundation for
their frame of government would be the breadth of the people rather than the
pillars of the state legislatures.

Gerry pointed out that Mason's analogy with the House of Commons was
fallacious given that the American situation is so different from Britain's.
[How so?]  However, he stated that he was not against perhaps having the
people *nominate* a number of representatives from which the state
legislatures could choose.

Butler thought election by the people to be impracticable.  The state
legislatures are simply more informed to choose the members of the national
legislature who would best achieve the objectives of the national government
-- common defense, security of liberty, and general welfare.

Spaight felt that it was necessary to investigate the means of electing the
Senate before deciding on the election of the first house.  Thus, he moved to
vote on the resolution that the second branch of the national legislature
should be appointed by the state legislatures.

King interjected that Spaight's move was premature and out of order.  Unless
the committee takes up one issue at a time, then the debates will never end.

Butler worried that removing too much power from the states would destroy the
security and balance of interests among the states which were necessary to
preserve.  Butler asked Randolph to explain the extent of his ideas regarding
the Senate -- in particular to give the number of members he envisioned.  Only
then could he give his opinion on the means of electing the first branch.

Randolph could not put an exact number on them but that they should certainly
be fewer than the House of Commons -- small enough to be exempt from the
"passionate proceedings" to which numerous assemblies are prone.  A good
Senate was intended to be a check against the powers of democracy.  He thought
that an appointment by the state legislatures would not have the necessary check
but that appointment by the first branch would.

Butler replied that until a number was given, he would be unable to vote on
it.

[According to Madison] King spoke up again to point out to the committee that
Spaight's method of electing the Senate is impracticable.  The Senate would
have to be very numerous -- 80 or 100 members -- for Delaware to even get one
representative unless the whole concept of proportion among the states is
disregarded.  Spaight conceded the point and withdrew his motion.

Wilson offered the proposition that the second branch ought to also be elected
by the people but from "districts" rather than states.  He drew his
inspiration from how the senators of New York were chosen from the unions of
several election districts.

Madison felt that the means of election in the original proposition was the
best.  Wilson's suggestion would almost guarantee that small states would have
no representation in the Senate.  The people of the more populous states would
likely choose members from their own state rather than superior candidates
from the smaller states.  He had watched this happen at the local level in
Virginia.

Butler moved to postpone the vote on the first resolution in order to take up
the question of the second branch.

King once again objects for the same reasons stated previously.

Sherman believed appointing the senators from the first branch would make the
two houses too dependent and would undermine the checks that Randolph
intended.  He favored the election of a single senator from each state
legislature.

Mason agreed with Sherman and pointed out further that appointments from the
first branch would create vacancies that would cost too much time and money to
fill.

Mason does not voice these following thoughts but writes in his notes that
electing senators from districts would be confusing by blending together the
representations of multiple states.  He notes the objections stated above that
each state choosing their own senators would make the body too numerous.
"Might not this objection be obviated, by appointing duly the representation
to each state, giving the smaller states an integer, and confining the larger
states to sending a smaller number of senators than their proportion, to
deliver their due number of votes?"(IV, 38)  He notes that such a method would
be unacceptable for the first branch, but the objections do not apply with
equal force to the senate.

Pinckney (drawing from the plan he had submitted earlier), like Wilson,
suggested dividing the United States into four "divisions".  From each
division a certain number would be nominated, and from those nominees would be
appointed the Senate.

Pierce agreed with many of the other delegates that knowing the means of
appointing the Senate would help them determine how to appoint the first
branch.  He believed that unless the people have a say in some part of the
national government, they might as well continue using the confederation.  The
influence of the states needs to be reduced in the national government, but
one branch of the legislature ought to represent the states, and that should
be the Senate.

Nevertheless, the original resolution (that the first branch should be elected
by the people) was voted on and carried in the affirmative by six states.

The committee proceeded to vote on the next resolution "that the second, (or
senatorial) branch of the national legislature ought to be chosen out of the
first branch by persons nominated by the state legislatures."

Pinckney moved to remove the nomination by the state legislatures.  The
committee voted against by nine states.  On the resolution as stated, the
committee then also voted against by seven states.  The clause was disagreed
to, and a void was temporarily left in this part of the plan.

The whole assembly agreed without debate that both houses should have the
power to originate laws and that all the current powers of Congress should be
transferred to the proposed national legislature.

The committee next debated the resolution for giving "legislative power in all
cases to which the state legislatures were individually incompetent."

Pinckney and Rutledge both objected to the term "incompetent" and thought it
was too vague.  They could not vote until they saw an exact enumeration of the
powers that were granted to the national legislature by this term.

Sherman agreed that it was vague, and yet, it would be too difficult to
enumerate all the powers in detail.  He believed it would be improper for the
national legislature to negative any state laws.

Madison said it was necessary to adopt some general principles, that they were
wondering from one topic to another without settling on any principles.

Wythe and King agreed with Madison.  King added that such principles only
establish the powers given up by the people to the legislature and federal
government but no further.

Butler still feared that they were running into an extreme by taking too many
powers from the states.  He again asked Randolph to explain his intentions.

Randolph stated it was impossible to enumerate the powers to which the
national legislature would extend at this time but emphasized that he did not
intend to give the national legislature indefinite powers.  He was opposed to
such inroads into the states' jurisdictions and had no intention of moving
from that position.

Wilson re-emphasized that enumerating the powers of the national legislature
was impossible.

Madison admitted that he had come to the convention with the desire of
thoroughly defining the powers of the legislature but doubted its
practicability.  His desires remained, but his doubts had grown stronger.
"[I] should shrink from nothing which should be found essential to such a form
of government as would provide for the safety, liberty, and happiness of the
community.  This being the end of our deliberations, all the necessary means
for attaining it must, however reluctantly, be submitted to."(I, 53)

Madison's words apparently had an effect as the resolution as it was stated
passed in the affirmative by nine states.  Only Sherman voted against.

The committee then quickly voted and agreed to the following resolutions:
* "...in all cases in which the harmony of the US may be interrupted by the
  exercise of individual legislation."(I, 61)
* "To negative all laws passed by the several states contravening in the
  opinion of the national legislature the articles of union."(I, 61)

To the last, Franklin added, "or any treaty subsisting under the authority of
the union."(I, 61)

Lastly, they considered "to call forth the force of the union against any
member of the union failing to fulfill its duty under the articles
thereof."(I, 61)

Gerry encouraged the committee to reword the resolution so that the people did
not become alarmed when they heard it.

Madison said that the more he reflected on the use of force, the more he
doubted the practicability, justice, and efficacy of it when applied to a
people collectively and not individually.  Granting this power to the union
would provide a means for its own destruction.  Use of force against a state
would look like a declaration of war and would justifiably be considered by
the party attacked as a dissolution of all previous compacts by which they
might be bound.  He hoped that the system they were framing would render such
a power unnecessary and moved to postpone the clause.

The assembly agreed, and the convention was adjourned.

# June 1, Friday

| State | New Delegates |
|-------|---------------|
| Georgia | William Houstoun |

The committee immediately took up the following resolution:

> Resolved that a national executive be instituted; to be chosen by the
> national legislature; for the term of ______ years.
>
> to receive punctually at stated times a fixed compensation for the services
> rendered; in which no encrease or diminution shall be made so as to affect
> the magistracy existing at the time of such encrease or diminution; and
>
> to be ineligible a second time; and that besides a general authority to
> execute the national laws, it ought to enjoy the executive rights vested in
> Congress by the confederation.

Pinckney voiced his support for a powerful executive but feared that investing
in him the executive powers of the current Congress would include the powers
to declare war and peace, which would make him the worst kind of monarch -- an
elected one.

Wilson motioned to make the executive a single person.  C. C. Pinckney
seconded the motion.  A long pause filled the room for some time after that.
Gorham [or perhaps Washington, Madison says "Chairman"] finally broke the
silence and asked if the motion should be put to a vote.  Franklin replied
that the motion was too important not to hear others' sentiments before a
vote.  Rutledge spoke up and criticized the delegates for their shyness on
this motion and others previously.  It looked to him like the delegates feared
that they would be held to their initial opinion if they spoke too frankly,
too soon.  He did not believe that this should be the case.

Rutledge continued on the topic of the motion:  The executive power should be
vested in a single person.  A single man would feel the greatest
responsibility for the office and administer the public affairs best.
However, he should not be given the powers of war or peace.

Sherman said that the executive was nothing more than an extension of the
legislature and, as such, should be appointed by the legislature rather than
the people.  Since the legislature would be the best judges of the needs of
the office, it should be up to them to appoint a singular or plural executive
as the task and experience demanded.

Wilson largely agreed with Rutledge.  An executive needed secrecy, vigor,
dispatch, and to be held responsible for his actions in office; a single
individual suits those requirements better than a multitude.  He added that he
did not think they should use the powers of the executive of the British
monarch as a blue-print for determining the powers of the American executive.

Madison agreed with Wilson that war and peace were not executive powers.  He
felt the best form of the executive office would be an individual with a long
term ministered to by a council.(I, 70)  The council would have the right to
advise and record but not control the authority of the executive.(I, 74)

Gerry favored giving the executive a council "to give weight and inspire
confidence"(I, 66).  The council would be the medium through which the people
would communicate with the executive.(I, 74)

Randolph opposed placing all executive power into a single individual.  He
called this "the fetus of monarchy"(I, 66).  There was no reason to borrow
this aspect of the executive from the British system.  He considered the
advantages that were argued for placing control with a lone individual --
vigor, dispatch, and responsibility -- but questioned why such necessities
could not be found in three men as well as one.

Wilson replied that placing the executive in an individual, rather than being
the fetus of monarchy, was the best safe-guard against it.(I, 66)  He reminded
Randolph that America had opposed parliament in the last war, not the British
King -- they opposed a corrupt multitude instead of an individual.(I, 71)
According to Pierce, he also referenced the thirty tyrants of Athens and the
Decemviri of Rome.(I, 74)  He repeated that he believed the British model was
inapplicable to the American situation.(I, 66)  The American system was closer
to Montesquieu's 'confederation of republics'(I, 71) -- "the happiest kind of
government with the most certain security to liberty."(I, 74)  [See also
Montesquieu, Part 2, Book 9, §1 on "federal republics".]

The committee then agreed to the first part of the motion, that a national
executive be instituted but postponed the question of the number of members
that should make it up.

Madison thought that the committee should enumerate the powers of the
executive before deciding whether it should consist of one or more men.  Once
they had agreed on the extent of executive authority, they could better
determine whether to place the authority in one or many.  He further moved
that after "executive be instituted" they should add "with power to carry into
effect the national laws, to appoint to offices in cases not otherwise
provided for, and to execute such other powers as may from time to time be
delegated from the national legislature".  Seconded by Wilson.

C. C. Pinckney suggested they also insert into Madison's proposition "...to
execute such other powers *not legislative nor judiciary in nature* as may
from time to time..."  His cousin Pinckney moved to strike out the addition as
it was superfluous as it was included in "power to carry into effect the
national laws" which Randolph seconded.

Madison stated that he believed a few of the phrases were probably
unnecessary but that it didn't hurt to retain them.

The words objected to by Pinckney were struck out, and a further vote negated
Madison's addition.

The committee next took up the method of appointment and duration of the
executive's term.

Wilson said that he was, in theory, for election of the executive by the
people because the nominees would be people "whose merits have general
notoriety"(I, 68).  New York and Massachusetts had shown that this method
could be successful.

Sherman wanted appointment by the legislature since it would be their law the
executive would be enacting.  "An independence of the executive on the supreme
legislature, was in his opinion the very essence of tyranny if there was any
such thing."(I, 68)

Wilson moved that the duration be a short term, three years, provided that
re-election was allowed.

Pinckney moved for seven years.

Sherman was for three years and said that preventing re-election would throw
out the best qualified for the office.

Madison supported a minimum of seven years and was against reappointment to
prevent the executive from plotting with the legislature for reappointment.

Bedford was strongly opposed to a term as long as seven years.  What if the
executive turned out after appointment to be unfit for office?  Impeachment
protected from malfeasance but not incapacity.  He supported three year terms
and ineligibility for reappointment after nine.

The committee voted on whether to fill in the blank with seven years, and it
passed with 5 ayes, 4, noes, and one divided (Mass.).

Next, the committee took up the question of the method of appointment.

Wilson repeated his stance that the people ought to elect the executive.  He
wanted both house of the legislature as well as the executive to elected by
the people without any influence from the state legislatures in order the
legislature and executive be separated from the influence of each other and
the states.

Rutledge suggests the executive be appointed by the upper house.

The convention adjourned.

Madison, after the convention, would write to Jefferson about this day:  "The
first of these [debates,] as respects the Executive, was particularly
embarrassing.  On the [many questions related to the executive office] tedious
and reiterated discussions took place."(III, 132)

Yates, obviously unhappy with the way the convention was proceeding, wrote to
his uncle that night, "Alas, sir!  My forebodings are too much realized".(IV,
41).

[Mason wrote a letter to his son that evening after the convention.  It adds
no facts about the proceedings but could add some color -- their motto ought
to be "*festina lente*", the convention is filled with men of "fine republican
principles", he worries about running from democracy to the opposite extreme,
etc.(III, 32-3)]

# June 2, Saturday

| State | New Delegates | Newly Absent Delegates | Reason for Absence |
|-------|---------------|------------------------|--------------------|
| Connecticut | William Samuel Johnson | | |
| Maryland | Daniel of St. Thomas Jenifer | | |
| Maryland | | James McHenry | dangerously sick brother (I, 75) |
| New York | John Lansing | | |

According to Washington, Jenifer arrived at the convention with permission to
represent Maryland as their only delegate (after McHenry's departure).  Now
eleven states are represented.(III, 33)  [However, another "version" of this
diary entry records Washington believing that twelve states are
represented.(IV, 44)  Yates's Journal agrees with eleven(I, 89) as does
Washington's June 3 entry.(IV, 46)  *Why are there multiple versions of
Washington's June 2 entry?*]

Benjamin Rush notes on this day that he is impressed by the punctuality with
which Benjamin Franklin attends the convention and engages in the committee's
debates at his age.  Both Franklin and Dickinson are reported as feeling
honored to be a part of so respectable a body and that since all those present
agree on the goals of the convention, they should all shorty reach consensus
on the means to achieve them.  Rush ascribes much of this agreement to John
Adams's ["Thoughts on Government"](notes/adams-thoughts.md).  "Our illustrious
minister in this gift to his country has done us more service than if he had
obtained alliances for us with all the nations of Europe."(III, 33)

The meeting this day began with an initial motion and second to postpone
further voting on the executive resolutions in favor of taking up again the
resolution of the upper house of the legislature.  This was quickly voted
against, and the committee resumed their debate about the executive, beginning
with the method of appointment.

Wilson suggested a method of appointment whereby the United States would be
divided into districts and out of each district the people would elect
electors who would themselves elect the executive.  Wilson preferred this
method of election because it removed influence from the states, and it would
inspire more confidence in the people than an election by the national
legislature would.

Gerry opposed having the legislature appoint the executive.  There would be
constant favors passed back and forth to secure election.  He liked the
principle of Wilson's motion but did not like the idea of leaving the states
out altogether.  He did not like having the people elect even the electors
("being too little informed of personal characters in large districts, and
liable to deception"(I, 80)).  He preferred the state legislatures elect the
electors or the executive directly.

Williamson could see no point in having the people elect electors who would
stand in the same relation as the representatives of the state legislatures
and create greater trouble and expense to assemble them to vote.
Comparatively, appointment by the state legislatures would be easy and least
liable to objection.

Wilson's motion was voted against in favor of the resolution "to be chosen by
the national legislature for the term of seven years"(I, 77).

Franklin arose and said he had written his motion and speech on a letter since
he no longer trusted his memory to record his thoughts and his age prevented
him from standing for long.(III, 481)  Wilson volunteered to read the motion.
Franklin motioned that the part of the resolution about "services rendered" be
struck out and replaced with "whose necessary expenses shall be defrayed, but
who shall receive no salary, stipend fee or reward whatsoever for their
services."(I, 81)

Wilson read aloud Franklin's speech justifying the change.  Franklin worried
about combining offices of *honor* with offices of *profit*.  He worried that
even if they initially set the executive's salary at a modest sum, it would
"nourish the fetus of a King"(I, 83) (quoting Randolph).  He gave examples
from Britain, France, and Quaker communities where honorable offices are held
without salary, and they attract "a sufficient number of wise and good men"(I,
84).  In particular, Franklin singled out Washington:  "To bring the matter
nearer to home, have we not seen the great and most important of our officers,
that of General of our armies executed for eight years together without the
smallest salary, by a Patriot whom I will not now offend by any other praise;
and this through fatigues and distresses in common with the other brave men
his military friends and companions, and the constant anxieties peculiar to
his station?"(I, 84-5)

Hamilton seconded Franklin's motion due to his respect for the man and his
position.  The committee voted to postpone the motion.  Madison reports, "It
was treated with great respect, but rather for the author of it, than from any
apparent conviction of its expediency or practicability."(I, 85)

Dickinson moved "that the executive be made removable by the national
legislature at the request of a majority of the legislatures of individual
states."  Dickinson did not like the idea of impeaching the "Great Offices of
State", but it was necessary to place the power of removing somewhere.  He
insinuated that some men of the convention wanted to abolish the states
altogether, but he preferred considerable powers, including the request for
impeachment, be left in their hands.  Bedford seconded.

Sherman believed the national legislature should be able to impeach at
pleasure.  [See the report of Wadsworth above on May 30 about Sherman.  That
letter was written the day after this (June 3).]

Mason concurred that some method of removal of office is necessary.  He
opposed making the executive a "mere creature of the legislature"(I, 86) as a
violation of the fundamental principles of good government.  [I assume he is
opposing here the resolution just passed that the executive be appointed by
the national legislature?]

Madison and Wilson both thought it unjust to give the same weight to small
states as large states if the executive had clearly committed a crime.  The
minority could in effect overrule the majority.  It was an authority they felt
the states did not need.

Dickinson rose and spoke again at great length.  He stated that a limited
monarchy was one of the best forms of government and that it was not certain
that the same benefits could be derived from their present form of government.
He stated that the executive authority that many of them supported was not
consistent with republican government and was only possible under a monarchy.
Far from being defeated by this fact, he felt they should all support the
aspects of their particular form of republican government which brought
stability.  He felt these strengths were the two houses of the legislature and
the confederation of independent states (at which point he again accused some
of those present of wishing to abolish).

He added that he wished that the equality of the states be preserved in at
least one of the house of the legislature and that the other be represented by
numbers relative to quotas paid by the states into the general treasury.

Dickinson's impeachment resolution was voted on and negated.

It was voted in the affirmative to make the executive ineligible for
re-election after seven years.

Williamson moved (seconded by Davie) that they add "and to be removable on
impeachment and conviction of malpractice or neglect of duty."  The motion was
affirmed.

C. C. Pinckney moved that the number of national executives be a single
person.  He believed the reasons for this so obvious and conclusive that no
one would oppose the motion.

[SURPRISE] Randolph opposed the motion.  He said he would not be doing his
country justice if he sat by and said nothing in opposition to what he felt
was a grave mistake.  The people would never accept a single magistrate being
too similar to a monarch.  The nominee for the position would always be
someone from the center of power in the country, and therefore, the remote
regions would never be represented.  Three members should be chosen from three
distinct regions in the United States.

Butler countered Randolph's position.  A single magistrate is the best
security for representing remote interests since by his very nature he must
represent the interests of the whole country.  If men are chosen from
different regions, there will be a constant struggle over local interests, and
this would be particularly bad in military matters.

On that note the convention adjourned.

The next day on June 3, Washington wrote to his son with his first optimistic
report during his trip:  "The sentiments of the different members seem to
accord more than I expected they would."(IV, 46)

Rufus King in a letter to Henry Knox was more cautious:  "we proceed slowly --
I am unable to form any precise opinion of the results".(IV, 47)

At the time of the convention, Benjamin Franklin was the president of the
Pennsylvania Society for the Abolition of Slavery.  On this day, the Vice
President of the society, Jonathan Penrose, wrote a letter to Franklin to be
addressed to the whole committee imploring them to bring up the suppression of
the African slave trade in their deliberations.  He says, "In vain will be
their pretensions to a love of liberty or a regard for national character,
while they share in the profits of a commerce that can only be conducted on
rivers of human tears and blood."(IV, 44)  Unfortunately, the society was
informed on July 2 that Franklin had not presented the address thinking "it
advisable to let them lie over for the present."(IV, 44)  Tench Coxe, a
Philadelphia politician, told James Madison in 1790 that he had been the one
to convince Franklin not to present the letter to the convention.(III, 361)

[Lots of mentions of the extreme cold and rain for this time of year in
Philadelphia.(e.g. IV, 46-7)]

# June 4, Monday

The committee started to consider the other resolutions of the Virginia Plan
when Pinckney (seconded by Wilson) proposed to limit the executive to a single
person.

Wilson defended the motion by countering Randolph's arguments.  Randolph's
reasons against a single executive derived primarily from its unpopularity,
that he did not think the people would accept it.  However, the people accept
a single state governor without suspicion.  Admittedly, the degree of power is
different in the state governors than the national executive, but there is
still no precedent of multiple executives.  Wilson could see nothing but
uncontrolled animosities in a multitude of equal magistrates.

Wilson supposes that Randolph wants specifically *three* equal members because
an even number could too easily be divided.  However, an odd number does not
fix the issue.  In a court of law, there are only two sides to a question --
guilt or innocence.  In executive and legislative matters, on the other hand,
questions often have several sides.  Each member of the three-headed
magistrate might support a separate solution, and no two agree.

Sherman supports Wilson's position for all the reasons stated.  He reminds the
committee that every governor also has an advisory council.  Giving the same
to the national executive would attract the confidence of the people.

Williamson asks Wilson whether or not he supports a council.

Wilson does not want a council.  Councils are more often used to cover
malpractice than to prevent it.

Gerry once again expresses how impracticable three equal magistrates would be,
especially in military matters.

A vote for a single individual executive passes.

The committee then debated on the next item in the Virginia Plan:  "Resolved
that the national executive and a convenient number of the national judiciary
ought to compose a council of revision."(I, 94)

Gerry questions whether the judiciary ought to form a portion of it since
they, by their nature, already have a sufficient check against the
encroachments on their department -- deciding on a law's constitutionality.
He moved to postpone this vote to debate the following clause:  "that the
national executive shall have a right to negative [veto] any legislative act
which shall not be afterwards passed by ______ parts of each branch of the
national legislature."(I, 98)

King seconds the motion and supports Gerry's assessment that the judiciary
should not be a part of the council of revision.  The judiciary should be
independent of the executive if the executive has veto power.  They should be
able to judge the merits of a case without the bias of having had a hand in
its formation.

Wilson thinks the proposition does not go far enough.  The executive needs an
*absolute* veto.  Without an unqualified veto power, the executive will have
no defense against the legislature.  Although, he would be willing to modify
the proposition such that jointly the executive and judiciary have an absolute
veto.

King, too, supports the absolute veto.(I, 105)

The committee votes to postpone the original vote to take up Gerry's motion on
the veto.

Wilson and Hamilton move to strike out the qualifying clause at the end of the
proposition to give the executive an absolute veto.  Hamilton argues that
they should not be worried the power will be used too often.  For example, the
king of England had not vetoed a law since the end of the revolution.

Gerry states that the legislature would be made up of the best men of the
community, and as such the executive would not need so great a control over
them.

Franklin rose to speak and first apologized to his colleagues who he had the
greatest respect for, but he could not sit and remain silent.  He was against
the executive veto power altogether on account his experience with under the
proprietary government of Pennsylvania.  The Governor used the veto power to
bribe the legislature into passing acts.  The king of England had not had to
exercise his veto power because the bribes he was already giving parliament
made it unnecessary.  If the executive has the power of veto, more money would
be demanded until the whole legislature had come under submission to the
executive office.

Sherman was against giving any one man the power to stop the law.

Madison is against an absolute veto(III, 494), prefers revisional
authority.(I, 106)

Wilson believed that, though the power of absolute veto was necessary, it
would be seldom used.  Franklin's example of Pennsylvania was under a former
colonial government where the Governor was appointed by the crown rather than
the people.  A qualified veto might work in times of peace, but if animosities
rose between the executive and legislature, the former ought to be able to
defend himself.

Butler would not have voted to place executive power in a single magistrate
had he known an absolute veto power would be given to him.  In all governments
the executive power is constantly increasing, but the gentlemen in the
assembly seem not to worry about an abuse of executive power.  Why could not a
Catiline or a Cromwell arise in America?

Bedford opposed every check on the legislature, including the veto and the
council of revision.  The Constitution should clearly outline the boundaries
of legislative power, and that should be enough to prevent an encroachment
into the other departments.  Separating the legislature into two houses was
sufficient control within the legislature itself.

Mason was apparently out when the vote was taken for setting the executive to
a single individual.  Nevertheless, he read a speech he had written supporting
three equal members in the executive which also had something to say about the
issue at hand.  [Mason's full speech can be found at I,110-4 and IV,49-51.
The arguments in favor of a three-headed executive are much more clear than
the fragments from Randolph that we see in Madison's and others' notes.]

Mason believes giving the individual executive veto power will lead to bribery
and hereditary monarchy.  He suggests that it might be enough to allow the
executive to merely temporarily suspend the law -- to give the legislature
time to come up with a better solution or to overrule the executive.  He hopes
that the dangerous suggestions the assembly keeps coming up with will be
considered good reason to expand the number of the executive in the future.(I,
101-2)

Franklin uses a historical example of the Prince of Orange of the Netherlands
to demonstrate that although a single executive may be good, and we do not
need to worry about him overstepping his bounds, we do not know who his
successor will be.  Therefore, we should not define the authority of the
executive based on what we think the best man might do.  "The first man, put
at the helm will be a good one.  No body knows what sort may come
afterwards."(I, 103)

The committee vote against the absolute veto.  Only Wilson, Hamilton, and King
vote for it.(I, 108)

Butler moved to alter the resolution to read:  "that the national executive
have a power to suspend any legislative act for the term of ______."(I, 103)
Seconded by Franklin.

Gerry observed that the power of suspension would have all the negatives of
absolute veto against useful laws but lack any of the advantages against
harmful laws.

The committee voted unanimously against Butler's motion.  They then voted in
the affirmative to insert two-thirds into the veto qualification resolution
and also voted in the affirmative for the whole veto resolution as Gerry
initially stated it.  [I,104,note 19 here says see Appendix A, XLI -- a note
in French I cannot read.]

The committee then passed in the affirmative the resolutions "that a national
judiciary be established to consist of one supreme tribunal, and of one or
more inferior tribunals."

Wilson moves (seconded by Madison) to add a convenient number of the national
judiciary to the executive as a council of revision.  Madison states that the
only way to make a government work as it should is to provide a means of
collecting its wisdom in the aid of each other when necessary.  Placing
members of the judiciary into the executive's council of revision does this.
It by no means interferes with the separation between the executive and
judiciary.(I, 110)

Dickinson disagreed.  The two departments are distinct.  The executive is the
executor of the law.  The judiciary is the *expounder*.(I, 110)

Ordered (by Hamilton(I, 104)) to be taken into consideration tomorrow.(I, 106)
[The debate over this motion not continued until June 6.]

The convention adjourned.

# June 5, Tuesday

| State | New Delegates | Newly Absent Delegates | Reason for Absence |
|-------|---------------|------------------------|--------------------|
| New Jersey | William Livingston | | |
| Virginia | | George Wythe | the serious declension of his lady's health (III, 35) |

[NOTE:  Lansing (NY) was absent due to sickness this day.(I, 127)  He
maintains an entry in his journal of the following debates which he copied
from Yates.(IV, 53-4)]

The words "one or more" were struck out before "inferior tribunals" in the
prior resolution.  The clause "that the national judiciary be chosen by the
national legislature" was brought under consideration.

Wilson was against.  Appointment by so numerous a body would lead to intrigue
and partiality.  A single, responsible individual (the executive) appointing
the judges would be best.

Rutledge did not want to imbue so great an authority in a single person.  The
people will think we are creating a monarchy.  Also, there is no reason for
inferior tribunals -- the state judiciaries already fulfill the role.

Franklin notes that they are thinking of two methods for appointing the
judiciary, but they should not be so limited in their ideas.  In Scotland, the
lawyers appoint from among their own ranks.  This fills the judgeship
positions with the best lawyers because those who vote for them wish to take
them out of the market and acquire their business.

Madison disliked appointment by the legislature (or any numerous body) or the
executive.  He was against the legislature because, besides the points already
brought up by Wilson, the legislators would not have the requisite knowledge
to make the correct judgment.  He leaned towards the senate only -- numerous
enough to be trusted but no so large to succumb to the base motives.

Hamilton suggested the executive nominate judges to the senate who would have
the right to reject or approve.(I, 128)

Madison moved to strike out "the national legislature" and leave it blank for
the time being.  Wilson seconds.  The motion passed in the affirmative.

Wilson gave notice that on a future day he would like to reconsider the
inferior tribunals.

Pinckney gave notice that when the question about appointment of the judiciary
came up again, he would like to restore "the national legislature".  Many
years later on March 5, 1800, Pinckney would speak before the senate, "I have
always been of the opinion, that it was wrong to give the nomination of Judges
to the President."(III, 385)

Moved and seconded to agree to the following part of the ninth resolution:

> to hold their offices during good behavior and to receive punctually, at
> stated times, a fixed compensation for their services, in which no increase
> or diminution shall be made, so as to affect the persons actually in office
> at the time of such increase or diminution

This passed in the affirmative, and the remainder of the resolution was
postponed.  The tenth resolution was then taken up:

> resolved that provision ought to be made for the administration of States
> lawfully arising within the limits of the United States, whether from a
> voluntary junction of government and territory or otherwise, with the
> consent of a number of voices in the national legislature less than the
> whole

It passed in the affirmative.

Paterson wished to postpone the next resolution, and it was postponed
unopposed.

The following resolution passed without debate:

> resolved that provision ought to be made for the continuance of a Congress
> and their authorities and privileges, until a given day, after the reform of
> the articles of union shall be adopted, and for the completion of all their
> engagements

The next was taken up:

> that provision ought to be made for hereafter amending the system now to be
> established, without requiring the assent of the national legislature

"Pinckney doubted the propriety or necessity of it."(I, 121)

Gerry for it.  Periodic revision would be necessary for giving immediate
stability to the new government.  There was no example in states with this
provision that indicated its impropriety.

The resolution was postponed without opposition.

The following was taken up and ultimately postponed:

> requiring oath from the state officers to support national government

The next resolution was debated:

> that the amendment which shall be offered to the confederation, ought at a
> proper time or times after the approbation of congress to be submitted to an
> assembly or assemblies of representatives, recommended by the several
> legislatures, to be expressly chosen by the people, to consider and decide
> thereon(I, 126)

Sherman believed that such popular ratifications were unnecessary.  The AoC
already provides a means of amendment by Congress and ratification by the
state legislatures.

Madison argued that this was one way in which the AoC were defective.  "[T]he
new Constitution should be ratified in the most unexceptionable form, and by
the supreme authority of the people themselves."(I, 123)

Gerry was afraid of referring the new system to the people of the Eastern
states.  They were in favor of removing their senates and giving all power to
the lower house.

Butler argued that the several state legislatures should not confirm the
ratification of the new government because they have already sworn to the old
government under which they act.  The people should choose deputies for the
purpose of ratifying it.(I, 128)

King believes that the people, having already consented to a federal
government, have already given implicit right to the state legislatures to
decide on amendments.  On the other hand, he wondered if a convention in each
state might be better and would carry the ratifications through better than
both houses of a state legislature.

Wilson believed that the people through convention are the only power that can
ratify a new government.  He also wondered if they might not be able to ratify
the constitution with only a partial assent of the states and provide a door
open for accession of the rest.

Along the same lines Pinckney hoped that, e.g., only nine states might unite
under it if the ratification was non unanimous.

The word "perpetual" in the AoC came up in debate.  Some delegates believed
"that the several states should meet in the general council on a footing of
complete equality each claiming the right of sovereignty"(I, 129).  Butler
stated that the word "perpetual" meant only the constant existence of the
union in some form, not the particular words which compose the AoC.(I, 129)

The committee voted to postpone the vote for the last resolution.

Pinckney and Rutledge moved to take up the election of the first house of the
national legislature tomorrow.  Agreed to.

Rutledge then moved to consider the inferior tribunals for the national
judiciary and remove them from the resolution.  The present state tribunals
were sufficient for the purpose and appeal to the supreme, national tribunal
would maintain consistency across judgments.  Inferior tribunals would be seen
as an encroachment on the jurisdiction of the states and might hinder
ratification of the new constitution.

Madison countered that unless inferior tribunals were distributed throughout
the union with *final* authority on legal matters, the appeal process -- a new
trial with both sides bringing their witnesses all the way to the capital --
would be a burden.

Wilson agreed with Madison and added that the admiralty jurisdiction ought to
be given wholly to the national government.

Sherman favored the motion.  The inferior tribunals would be an unnecessary
expense when the state tribunals would suffice.

Dickinson argued that if there were a national legislature, there ought to be
a national judiciary, and the former should be given the authority to
institute the latter.

The committee voted to strike out the inferior tribunals, and it passed in the
affirmative.

Madison and Wilson then jumped on Dickinson's suggestion and moved to add to
the resolution "that the national legislature be empowered to institute
inferior tribunals."(I, 125)  The observed that there was a distinction
between setting up such tribunals absolutely and granting the legislature the
authority to establish them or not.

Butler argued that the people and the states would not approve of such an
encroachment.  It should be avoided even if it is in the people's best
interest.  Like Solon of Athens, we must give the people the best government
they will accept, not the best government we can devise.

King argued that the cost of inferior tribunals would be significantly less
than the cost of appeals from the state courts.

The addition to the resolution passed in the affirmative.

The committee adjourned.

G. Morris felt later (before the senate in 1802) that by investing the
legislature with the power to establish inferior tribunals and simultaneously
"[i]t has declared that the judicial power so vested shall extend to the cases
mentioned, and that the Supreme Court shall not have original jurisdiction in
those cases"(III, 391) the constitution is worded so as to *impel* the
legislature to establish inferior tribunals.

There is no mention of Mason speaking this day, but given his comments during
ratification in the Virginia legislature, he states that the creation (or
allowance) of the inferior tribunals "goes to the destruction of the
legislation of the states, whether or not it was intended."(III, 330)  He goes
on to suspect that there might have been something of a conspiracy from the
start by some men at the convention to bring about the eventual consolidation
of the state governments into the national and that this resolution was a part
of it.(III, 330-1)

# June 6, Wednesday

The convention this day began with Pinckney moving to reconsider the election
of the first house of the national legislature [the house of representatives].
The preferred that the first house be elected by the legislatures of the
several states rather than the people.  Rutledge seconded the motion.

In early 1789 Pinckney would write to both King and Madison on the topic of
election by the people.  He would tell King that the election of the house of
representative by the people "is the greatest blot in the constitution."  And
to Madison he would write, "it will in the end be the means of bringing our
councils into contempt and that the legislature are the only proper judges of
who ought to be elected."(III, 355)  Interestingly in 1818 he would win
election to the house of
representatives.([source](https://en.wikipedia.org/wiki/Charles_Pinckney_(governor)#Career))

Gerry worried about going too far to the extreme in either direction.  In
Massachusetts the worst men (lately men of "infamous" crimes [-- he no doubt
means Shays's Rebellion]) get elected to office by the people.  However, "[i]f
the national legislature are appointed by the state legislatures, demagogues
and corrupt members will creep in."(I, 140)  One branch of the legislature
must be elected by the people to earn their trust, but he suggested that the
method of the election might be modified "to secure more efficiently a just
preference of merit"(I, 132) by having the people elect a set of nominees
(double the number of seats (I, 142)) out from whom the state legislatures
appoint the representatives.

Wilson believed that the government ought to possess the "mind or sense" of
the people at large, that the representatives "ought to be the most exact
transcript of the whole society."(I, 132)  Therefore, they must be elected by
the people.  They needn't worry about the representatives as long as the
voting districts are sufficiently large.  Small districts sometimes lead to
terrible representation.  The people will support this change since the losers
will be the state political powers.

With respect to making voting districts sufficiently large, Hamilton notes
(but does not speak) that even in large districts a small portion may carry
the election, and an influential demagogue may move an entire region.(I, 147)

Sherman thought that the people should elect the representatives only if the
states are abolished.  Since the states are being maintained, then the state
legislatures should appoint the lower house in order to preserve harmony
between the states and the national government.  The people already
sufficiently participate in the confederation by electing their state
legislatures.  The responsibilities of the national government are few:
defense against foreign and domestic aggression, treaties with foreign
nations, and regulation and taxing of foreign commerce.  These
responsibilities do not concern the people.  "I am against a general
government and in favor of the independence and confederation of the states,
with powers to regulate commerce and therefrom draw a revenue."(I, 143)

Mason makes many of the same arguments as Wilson above.  Congress in the
existing confederation represents the states, but the newly proposed union
will represent the people; therefore, the people must elect their
representatives.  "It should be [such a representation of the people], that
even the diseases of the people should be represented -- if not, how are they
to be cured?"(I, 142)  No government is perfect, and improper elections are
inseparable from republican governments, but the advantages of this form of
government outweigh its downsides.

Madison agreed with Sherman in the responsibilities of government but thought
he stopped short.  Two other principle responsibilities of government are the
security of private rights and the dispensation of justice.  Interference with
these republican principles in the states was the reason -- more than anything
else -- that they sat in this convention today.  Republican governments always
risked a tyranny of the majority against the minority.  The best prevention is
to enlarge the public sphere, to cover so great a number of interested parties
and factions that no one group has enough of a majority to infringe upon the
rights of any other group.  He intended to frame a republic at such a scale
that it would control the evils they had recently experienced.  [Compare this
with Montesquieu who thought republican governments incapable of growing too
large and still maintaining their republican principles.]

[Worth noting that Mason agrees with Madison's arguments on August 13:  "He
admitted that notwithstanding the superiority of the Republican form over
every other, it had its evils.  The chief ones, were the danger of the
majority oppressing the minority, and the mischievous influence of demagogues.
The General Government of itself will cure these.  As the States will not
concur at the same time in their unjust and oppressive plans, the general
government will be able to check and defeat them, whether they result from the
wickedness of the majority or from the misguidance of demagogues."(II, 273-4)]

Hamilton does not rise to speak but writes in his notes counter-arguments to
Madison's speech above:  No matter if members are drawn from half the globe,
once they are assembled, they will still be liable to the same passions of
popular assemblies.  The geographical distinctions in the union will become
pronounced -- e.g. Southern vs. Eastern interests.(I, 146)

Dickinson thought it necessary that one house be derived from the people and
the other house derived from the states.  The format of the British parliament
seems ideal.

Read criticized the delegates for their attachment to the state governments.
The national government must eventually swallow the states.  He hoped they did
not try to fix the old confederation and instead built something entirely new.
[This coming from the guy who on two occasions (III,25-6 and III,575)
cautioned others that the small states must maintain their sovereignty?
Curious...]

Pierce like Dickinson prefers the first house to be elected by the people and
the second by the states.

C. C. Pinckney doubted that the people would chose better than the
legislatures.  Many people in his state of South Carolina wanted paper money
to be legal tender, but the legislature had enough character not to pass such
a law.  Besides the state legislatures would be more likely to thwart the
national government if they felt they were unduly excluded from participation.

Wilson replied to Read's statements.  There was no incompatibility between a
national government and state governments as long as the latter were limited
to local matters.  History taught that, far from the general government
swallowing the regional governments, it was usually the regional that usurped
the general.

Butler preferred to decide the method of election *after* they had figured out
representation, whether by wealth or free inhabitants.  "I am content to unite
with [Mr. Read] in abolishing the state legislatures and becoming one nation
instead of a confederation of republics."(I, 144)

Pinckney's motion passed in the negative.

Wilson moved the reconsider the vote which had removed the judiciary council
of revision from the executive.

Madison seconded the motion.  He worried that a single executive might succumb
to putting his personal interest before the national interest.  Adding a
council would not only diminish this risk, but the council would also lend
support to the executive against encroachment by the legislature.

Madison addressed both of the previous objections.  *First, the judges should
not have a hand in generating the laws that they might one day have to judge.*
However, it would be unlikely that a judge would have to rule on just the
particular law that he had once revised.  Additionally, his role in the
revision would be so small as to have no real significance on his judgment.
Much more would be gained from his council to the executive.

*Second, the judiciary must be distinct from the other two departments.*  The
purpose of separating the departments of the government was so no one could
encroach upon the function of the other.  Annexing part of the judiciary to
the council of revision fulfills this purpose.  Whatever undesirable outcome
the other delegates were worried about -- whether the legislature tried to
encroach on the power of the executive or infringe on the rights of the people
or pass unwise laws -- the role of the judiciary in the council of revision
would be suitable to prevent it.

Gerry felt that a single executive would be more impartial than one "seduced
by the sophistry of the judges."(I, 139)

King pointed out that if the principle of responsibility was a reason for
having a single magistrate, then surely the same reason supported him having
sole revisionary power.

Pinckney had been in favor of a council of revision represented by the various
officers of government.  He had since relinquished the idea since the
executive would have the power to consult with them at will.  He was opposed
to an introduction of the judges.

Mason quoted as saying, "The purse and sword must not be in the same
hands."(I, 144)  If the legislature have both the power to raise revenues and
direct war, then he supported restraining their power as much as possible --
including through a revisionary council.

Dickinson agreed with King regarding the principle of responsibility.  He
thought the judiciary council an improper mixture of powers.

Wilson countered that the principle of responsibility only applied to his
executive duties.  "The revisionary duty was an extraneous one, calculated for
collateral purposes."(I, 140)

Williamson supported a requirement of 2/3 majority for passing laws in place
of the revisionary council.

The motion on the council of revision passed in the negative.

Pinckney informed the committee that tomorrow he would move to reconsider the
veto power of the national legislature over the laws of the states.

The committee adjourned.

After the convention this day, Washington and Madison wrote eloquently to
others about the anxiety and optimism they had for the convention so far.(III,
34-7)

# June 7, Thursday

[Jonathan Dayton (NJ) writes that Houston resigned from the convention some
time before this day due to his ill health.(IV, 59)]

Pinckney once again gave notice to the committee that he would reconsider the
national veto over state laws.  The committee fixed tomorrow for that purpose.

Dickinson moved that the members of the second house of the legislature be
appointed by the several state legislatures.  [The current method under debate
for appointing congressmen to the senate assumes an unequal representation of
states based on either population or wealth, not the two-per-state equal
representation that we have today.]

Sherman seconded the motion.  He thought this would help maintain the harmony
between the state and national governments.

Pinckney pointed out that if they gave the smalls states like Delaware one
senator only, then there would be at least 80 members which is too many.

Dickinson said he had two reasons for the motion.  First, they would better
represent the will of the states.  Second, "because he wished the senate to
consist of the most distinguished characters, distinguished for their rank in
life and their weight in property, and bearing as strong a likeness to the
British House of Lords as possible; and he thought such characters more likely
to be selected by the state legislatures, than in any other mode."(I, 150)  He
didn't mind if there were "80 and twice 80 of them"(I, 150).  Their number
needed to be sufficiently large to balance the first house.

Williamson preferred a small number of senators -- perhaps 25 -- but thought
each state should have at least one.

Bulter wished to know the ratio of representation before giving an opinion.

Jenifer wished representation to be proportional to contribution.(IV, 57)

Wilson felt that both houses of the legislature should rest on the same
foundation -- the people.  If they did not, then dissensions would arise
between them.  He moved that Dickinson's motion be postponed in order to take
up instead the election of the senators out of districts combined from the
states for that purpose.  Morris seconded him [Probably Robert?].

Wilson conceived of combining states into a number of districts -- at least
four.  The senators elected from those districts would be divided into lots
after the first election so that some of their seats would expire after the
first year, others the second, etc.  so that there would be new senatorial
elections annually.  It would be up to the national legislature to redivide
the districts in the future as the population grew.(Iv, 58-9)

Read proposed that the senate be chosen by the national executive from a set
of nominees elected by the state legislatures.  He felt others might be
alarmed at his suggestion, but he needed to be frank.  No one seconded or
supported his motion.

Madison pointed out that if Dickinson's motion was agreed to, then they would
either need to get rid of proportional representation of the states -- unjust
-- or make the senate large -- unsuitable to republican government.  If the
senate is meant to introduce stability, then enlarging their number will
introduce vices.  Their political power is indirectly proportional to their
number.

Gerry summarizes the four methods of appointing the senate that have been
mentioned so far:
1. by the house of representatives
   * this would create a dependency on the first house that they wished to avoid
2. by the national executive
   * too close to monarchy for anyone to agree to
3. by the people
   * The people can be divided into two classes:  the landed commoners and the
     merchants.  The first house already represents the landed interests.  The
     commercial interests need the senate to secure their interests.
4. by the state legislatures
   * This is the best method as it secures the commercial interests from the
     oppression of the landed.

Dickinson argued that appointment by the states was necessary as a check
against democracy -- like the House of Lords vs. the House of Commons.  If the
legislature was fully run by the people, the national government would
experience all the problems currently experienced by the individual states.
He likened the states to planets which ought to be left to freely follow their
own orbits.  He accused Wilson of wishing to extinguish the planets.

Wilson reminded the committee that they must not use the British system as
their guide.  It does not suit the American spirit; we have no noble families.
He worried more about the states devouring the national government than the
alternative.  He was not for 'extinguishing the planets' as Dickinson had put
it, but nor did he expect the planets to warm or brighten the sun.  The states
had their jurisdiction separate from the national government.  Furthermore, he
could not understand why Dickinson would think appointment by the states would
be a solution to the encroachment of landed interests.  If the landed
interests had already overrun the state legislatures as many delegates
claimed, who wouldn't expect such interests to carry over to the senate?
Subdividing the states into large enough voting districts would encourage the
election of upright men.

Madison could not see the advantages claimed by Dickinson in letting the
states choose the senators.  The perceived great evils of giving too far into
the people's will was the creation of paper money and other schemes -- schemes
enacted by the legislatures.  In was contradictory to use the state
legislatures as examples of unchecked democracy and then claim those same
legislatures were the necessary check for that democracy.

Sherman believed that election by districts would not produce as fit
candidates as would the state legislatures.

Gerry insisted that the commercial and monied interests would be more secure
in the hands of the state legislatures.  "The former have more sense of
character, and will be restrained by that from injustice."(I, 154)  In many
instances it is the people who are for poor schemes like paper money while the
legislatures are against it.  He saw three arguments against the district
method so proposed:
1. It is impracticable, and fraud would be unavoidable.
2. Small states in a district with a larger one would never see their citizens
   elected.
3. It would create a new source of discord within districts.

Pinckney preferred the state legislatures over the districts.  It would avoid
the rivalries and discontentments inherent in the district method.  He
suggested dividing the states into three classes by population and giving each
one 1, 2, or 3 senators.

The committee voted against Wilson's motion for districts.

Mason observed that they had studiously provided means for the various
branches of government to defend themselves.  Shouldn't they grant the same
protections to the states?  While it was true that in ancient confederations
the members overcame the general, the system they have proposed is different.
We have established that the national legislature will have veto power over
the states.  Thus, they had more cause to worry the states would be
devoured.(I, 159-60)  Allowing the state legislatures to appoint the senators
was a means of protecting the states' interests from encroachment by the
national government.

Dickinson's motion to have state legislatures elect the senators passed in the
affirmative.

Gerry gave notice to the committee that tomorrow he would move to reconsider
the election of the national executive by the state governors instead.

The committee adjourned.

# June 8, Friday

The committee began by reconsidering the resolution granting the national
legislature the authority "to negative all laws passed by the several States
contravening, in the opinion of the national legislature, the articles of
union; or any treaties subsisting under the authority of the union."(I, 162)
Pinckney moved that it be replaced with "to negative all laws which to them
shall appear improper."(I, 162)

Pinckney argued that the universality was necessary to protect the national
prerogatives.  Congress had been defeated because they did not have this power
to exercise against the states, and treaties were continually violated as a
result.  The British Crown holds veto power and those assembled believed it
was a good thing.  Were not the states now more one country than the colonies
had been then?

Madison seconded the motion as he saw it was entirely necessary to the
continued function of the union.  Experience had shown the danger of the
encroachment of the states on the general government.  A veto was the mildest
possible response.

Madison writes to Jefferson shortly after the convention, "[Veto power over
the state laws] appears to me necessary.  1. to prevent encroachments on the
general authority.  2. to prevent instability and injustice in the legislation
of the states."(III, 134)  He continues, "A constitutional negative on the
laws of the states seems equally necessary to secure individuals against
encroachments on their rights."(III, 135)  He later hoped the (eventually
proposed) judicial review would keep the state governments in their proper
jurisdiction and have the same effect as a veto, but he admitted that it is
easier to prevent the passage of a law than to declare it void after it has
been passed.(III, 134)

Williamson spoke against the motion.  The national legislature should only be
able to veto state laws that will encroach on those of the national
government.(I, 169)

Madison wishes the exact line of jurisdiction between the states and the
general government could be ascertained, but it is impossible.  If the state
judiciary judges on a law that abridges the right of the national government,
how would it be repealed?  By the state who made it?  How would you compel
them?(I, 169 & IV, 60)  Without veto power, coercion on the part of the
national government would be necessary -- an impossibility.  Veto power makes
the threat of force unnecessary.  To continue the solar system analogy, this
power keeps the planets in centrifugal motion; without it they will fly out of
their orbits and disturb the order of the whole system.  [Note:  The other
journals (not Yates and Lansing) place the last half of this paragraph in
Madison's opening remarks.]

Gerry thought the power unnecessary.  He thought a formal statement of protest
from the national government would be sufficient and threat of force if not.
He had no objection to explicitly defining the sorts of laws the national
government could veto, e.g. paper money.  He worried that the proposed
resolution would extend to laws already made and especially ones regarding
state militias which the states depended on for defense.  If the national
legislature could declare the militias illegal, it could enslave the states.
The people have never asked for this power and will not accept it.  The states
are also ignorant of each other's interests, and representatives would not be
good judges of different local laws.  New states that enter the union might be
under foreign influence.  Should they have a right to veto other states?

Pinckney supposes that the resolution does not affect laws already made.  The
adoption of the new articles of union would be itself a repeal of the states'
laws so far as they were inconsistent with the national laws.(I, 170)

Sherman wanted the cases in which a veto could be used to be explicitly
defined.

Wilson believed the power was right.  Individuals do not have the legal right
of choosing which laws to obey or disobey.  Why should the states be treated
differently?  An explicit list of cases in which the veto is appropriate is
impracticable.  We must err on one side or the other.  Shouldn't we err in
favor of the national government?  "We must remember the language with which
we began the revolution, it was this, Virginia is no more, Massachusetts is no
more -- we are one in name, let us be one in truth and fact."(I, 172)  No
sooner did the state governments form after victory than they began to chase
after their own interests.  The business of the convention is to fix the vices
of the AoC.  Lacking a veto, the vice of sacrificing the general interest for
local remains.

Dickinson thought it impossible to draw the lines between proper and improper
use of the veto.  He repeated Wilson's argument that they must err one way or
the other, and he felt it better to err on the side of the general government.
The danger of encroachment is greater from the states.(I, 167)  With respect
to the question of vetoing the state militias, the senate (representing the
interest of the states) would never do such a thing.

Bedford pointed out that since they were revising the ratio of representation,
Delaware would have only 1/90 the say in the legislature while Virginia and
Pennsylvania would together possess 1/3.  What protection do the small states
have from the large states in such a system if their economic interests, e.g.,
are at odds?  How is the veto even exercised?  Are the laws of the states to
be suspended until they can be determined appropriate by a body hundreds of
miles away who may be incapable of appropriately judging them?  Will the
national legislature continually be in session to revise state laws?

Madison responded that Bedford have made good points, and his questions ought
to be answered before a vote.  Madison had in mind that in the case of urgent
laws, the states would be free to carry them out until the national
legislature had a chance to review them -- as was done previously when they
were colonies.  He suggested that perhaps the veto power be held by the senate
alone so that the more numerous house might not be obliged to sit constantly.
Madison asked Bedford what would happen if the union dissolved because their
task at the convention failed.  Proportional representation, he felt, was
necessary to this aim.  If the large states are as ambitious and greedy as Mr.
Bedford claims, will the small states be any more secure when the general
government is dissolved?

Butler was vehemently against the veto.  It was unjust to the more distant
states and would never be approved by them.

The committee then voted on the resolution of veto powers against the state
laws.  It failed to pass.  The only three states to vote in the affirmative
were Virginia, Pennsylvania, and Massachusetts -- the three largest states by
population at the time.

Gerry and King moved that tomorrow they would like to reconsider the method of
electing the national executive.

Pinckney and Rutledge moved to add the following after the fourth resolution
(about the legislature):

> resolved that the states be divided into three classes -- the first class to
> have three members, the second two, and the third one member each -- that an
> estimate be taken of the comparative importance of each state, at fixed
> periods, so as to ascertain the number of members they may from time to time
> be entitled to

Before any debate began, the committee moved to adjourn.

[James Monroe writes to Jefferson during the convention that he hears news
about a veto on the state's laws, and he thinks it proper.(III, 65)]

# June 9, Saturday

| State | New Delegates |
|-------|---------------|
| Maryland | Luther Martin |

[Luther Martin delivered his voluminous thoughts on the convention to the
Maryland legislature on Nov. 29, 1787.(III, 172-232)  It would be worth
examining his points at a later date.]

Gerry, as he had given previous notice the day before, moved:

> that the national executive should be elected by the executives of the
> states whose proportion of votes should be the same with that allowed to the
> states in the election of the senate(I, 175)

Gerry asserted that if the national legislature made the choice, as the
resolution presently read, then there would be no independence between the
legislative and executive branches.  The executive would make deals to become
elected, and then would show partiality to those who helped elect him.  He
supposed the state executives would be a better method and would pick the
fittest man for the office.

Randolph spoke against the resolution.  The people would not trust such an
appointment.  The small states would never have a chance to have the executive
chosen from their state.  Appointments would be poor because state executives
know little beyond their own state.  The governors are dependent on the state
legislatures, and their appointments would be partial to the interests of
their state.  Such a national executive would not defend national sovereignty
from the encroachment of the states.

The motion voted upon pass unanimously in the negative.

Paterson moved to take up consideration of the resolution related to
legislative suffrage, being:

> that the rights of suffrage in the national legislature ought to be
> apportioned to the quotas of contribution, or to the number of inhabitants,
> as one or the other rule may seem best in different cases(I, 181)

Brearly seconded the motion.  The current articles support equal suffrage
among the states because without it the smaller states would have been
devoured.  A ratio of representation might seem fair at first, but on closer
examination was unfair and unjust.  The three largest states -- Virginia,
Pennsylvania, and Massachusetts -- would carry everything.  The smaller states
would be forced to throw their lot behind one of the larger states in order to
have any weight at all.  The destruction of the equality of voting astonished
and alarmed him.  Is it fair that Georgia has an equal vote with Virginia?
Probably not, but the only remedy would be to redraw the state lines into
thirteen equal parts.

Paterson asserted that proportional representation struck at the very
existence of the smaller states.  He reminded the committee that their
convention instructions stated "that the articles of the confederation were
therefore the proper basis of all the proceedings of the convention."(I, 178)
We ought to remain within those limits, or the people will rightly charge us
with usurpation.  The people do not clamor for a national government instead
of a federal one.  Even if we had the right to go beyond the federal scheme,
the people would not have it.  A confederacy assumes equal representation.  If
we are to become a nation instead, we must abolish the states as they exist.
There was no more reason to believe a large state should have more of a vote
than a smaller one, than to suppose that a man worth £4000 should have forty
times the vote of a man worth £100.  "Give the large states an influence in
proportion to their magnitude, and what will be the consequence?  Their
ambition will be proportionally increased, and the small states will have
everything to fear."(I, 178)  Britain once offered to allow American colonial
representation in parliament.  What influence would 200 have had against 600
British representatives?  He was strongly attached to the present form of
government:  The people choose their state representatives, and the states
choose their federal representatives.  Wilson had earlier hinted that the
larger states might be forced to confederate among themselves alone by a
refusal of the others.  Let them, but know that they have no authority to
compel the others.  New Jersey will never accept the current plan before the
committee.  She would rather submit to a monarch or a despot.  Paterson ended
by asserting that he would not only appose the plan here but do everything in
his power when he returned home to defeat it there.

[See Paterson's notes for his above speech I,185-91.]

Yates does not rise to speak but writes in his notes that he agrees with all
the points Brearly and Paterson raised.(I, 183)

Wilson hoped that if the confederation dissolved, a majority or minority might
unite for their defense.  He defended proportional representation on the
principle that, Mr. Paterson had already admitted, *individuals* are the
measure of suffrage.  Are the citizens of Pennsylvania not equal to those of
New Jersey?  "Representatives of different districts ought clearly to hold the
same proportion to each other, as their respective constituents hold to each
other."(I, 180)  Wilson commended Paterson on his frankness and said that he
would be frank as well(I, 183):  If the small states would not confederate on
this plan, Pennsylvania would not confederate on any other.  In a state of
nature, each man is a sovereign over himself and naturally equal.  Can this
equality be retained when he becomes a member of civil government?  No.  \[see
Locke on [the state of nature](notes/locke-government.md#chap-ii-of-the-state-of-nature)
and [civil society](notes/locke-government.md#chap-vii-of-political-or-civil-society)]
Neither can a sovereign state when it becomes a member of a federal
government.  "If New Jersey will not part with her sovereignty, it is in vain
to talk of government."(I, 180)  Repartitioning the states would be desirable,
but it is impossible.

Williamson compared the case of the states to the case of counties within the
states.  Proportional representation is accepted as just in the latter;
therefore, it cannot be called unjust in the former.

Paterson proposed that, given the importance of the matter in question, the
vote be postponed until tomorrow -- agreed.

The committee adjourned.

That evening, Brearly wrote to another New Jersey delegate who had not yet
attended, Jonathan Dayton, "My colleagues, as well as myself, are very
desirous that you should join us immediately.  The importance of the business
really demands it."(IV, 64)

# June 11, Monday

| State | New Delegates |
|-------|---------------|
| Georgia | Abraham Baldwin |

The debate concerning the rule of suffrage in the national legislature was
resumed.

Sherman proposed that the proportion of suffrage in the house of
representatives ought to be according to the number of free inhabitants, and
in the senate each state should have one vote.  Since the states would be
possessed of certain individual rights, they would need some means to protect
themselves or the large states would overpower the rest.  The House of Lords
have an equal vote with the House of Commons so as to protect their interests.

Rutledge proposed that the suffrage of the first branch should be apportioned
according to the contribution of the individual states.

Butler agreed and added that "money is power".  The states ought to have
weight in government in proportion to their wealth.

King, seconded by Wilson [or Rutledge, see I,196 footnote], moved:

> that the right of suffrage in the first branch of the national legislature
> ought not to be according to the rule established in the articles of
> confederation, but according to some equitable ratio of representation(I,
> 196)

Dickinson argued for the proportion of suffrage and representation to be
according to the states' actual contributions.  By connecting the interests of
the states with their duty, the latter will be performed.

King did not know what method would be used to levy the national revenue, but
he supposed that imports might be the preferred one.  If this is the case,
then Connecticut and New Jersey, being non-importing states, would have no
representation.

Franklin put his thoughts on paper and gave it to Wilson to read:  He was
concerned with how quickly the question of representation had turned to
contention.  He preferred that the delegates think of themselves as agents of
the whole union rather than those of their particular states.  Representation
should be apportioned according to state population or wealth.  There is no
good reason to believe the large states will swallow the smaller.  Scotland
was in this same situation when they were allowed seats in parliament.  There
was a fear that England would overwhelm them, but Franklin could not recollect
a prejudicial judgment against the Scots since then.  It would even be
possible -- if the smaller states teamed up -- to swallow the larger states.
The interests of the citizens *are* the interests of their state.  He
supported redrawing the state boundaries if a practicable and permanent
solution existed, but he doubted it did.

Franklin suggested a method of keeping representation equal:  Let us determine
how much the weakest state can contribute.  Then, every other state will agree
to contribute that amount.  Thus, each state will have an equal representation
in the legislature.  If the national government finds that the total is not
enough, then they could ask the larger states for a voluntary contribution in
addition to and distinct from the required amount.

King and Wilson's motion was agreed to.

Rutledge moved to add "namely, according to the quotas of contribution"(I,
193) to the motion just agreed to.  [Yates says Dickinson, instead of
Rutledge, moved to add "according to the taxes and contributions of each state
actually collected and paid into the national treasury."(I, 205)]

Butler seconded the motion.  While the national government will have the right
to make and collect taxes, the states ought to have the responsibility of
levying the taxes themselves.(I, 205 & IV, 69)

Wilson supposed that there would be other methods of raising revenue by the
national government than just state taxes -- one being the post office.(IV,
69)  He moved, seconded by Pinckney, to postpone Rutledge's motion in order to
add instead:

> in proportion to the whole number of white and other free citizens and
> inhabitants of every age, sex, and condition, including those bound to
> servitude for a term of years, and three-fifths of all other persons not
> comprehended in the foregoing description, except Indians, not paying taxes
> in each state(I, 193)

This was the current rule in Congress for eleven states that apportioned
quotas for revenue.  A census to reevaluate ought to be taken every 5, 7, or
10 years.

Gerry asked why they should count blacks who were considered as much property
in the south as horses and cattle were further northward.(I, 206)  It would
be "as dishonorable and humiliating to enter into compact with the slaves of
the southern states, as it would with the horses and mules of the
eastern."(III, 198)

Madison was of the opinion that the general method of representation ought to
be fixed but to leave the business of working out the details to a
subcommittee.(I, 206)

Wilson's motion was chosen and agreed to over Rutledge's.

Sherman moved, seconded by Ellsworth, that each state have only one vote in the
senate.  Everything hinged on this, and the smaller states will not ratify the
constitution otherwise.

This motion was voted down 6 to 5.

Wilson moved, seconded by Hamilton, that the suffrage in the senate be the
same as in the house of representatives.

This motion passed 6 to 5.  The five states who opposed this motion (and
supported the previous one) were Connecticut, Delaware, Maryland, New Jersey,
and New York.  [See Gerry's description of these votes: "and when under
consideration of the convention, it produced a ferment, and a separate
meeting, as I was informed, of most of the delegates of those five states, the
result of which was a firm determination on their part not to relinquish the
right of equal representation in the senate".(III, 264)]

The next resolution to add "or partition" after "voluntary junction" in the
eleventh resolution passed in the affirmative.  This was moved by Madison.(I,
206)

Read disapproved of any resolution which granted anything to distinct states.
"There can be no cure for this evil but in doing away [with] states altogether
and uniting them all into one great society."(I, 202)

The eleventh resolution was amended, again by Madison(I, 206), to read:

> resolved that a republican constitution and its existing laws ought to be
> guaranteed to each state by the United States

Randolph stated that republican government must be the basis of the union.  No
state should have the right to become a monarchy.(I, 206)

Madison's motion passed unanimously in the affirmative.

The thirteenth resolution was taken up for amending the constitution without
the consent of the national legislature.  Several members did not see the
necessity of the resolution.

Mason pointed out that the constitution they were working on would no doubt be
found to be defective in some way just as they had found the AoC to be
defective.  They should provide an easy constitutional way to amend it rather
than trust changes to "chance and violence."  It would be improper to require
the consent of the legislature because the reason for amending may be that
they are abusing their power.  Randolph agreed.

The resolution was postponed.

The fourteenth resolution was taken up requiring an oath from the members of
state governments to support and protect the national constitution.

Sherman opposed the unnecessary intrusion into the states' jurisdictions.

Williamson believed the resolution unnecessary because the new constitution
would become the law of the land.(I, 207)

Randolph considered it necessary to prevent competition between the national
and state governments.  "We are erecting a supreme national government; ought
it not to be supported, and can we give it too many sinews?"(I, 207)

Gerry disagreed.  He thought it as reasonable to bind the national
representatives to oaths to the states to prevent their annihilation as it was
to bind the state representatives to national oaths.

Luther Martin moved to strike out the words "within the several states".

The committee voted and agreed to the fourteenth resolution as stated without
L. Martin's change.  [Yates claims they approved Gerry's suggestion
instead.(I, 207)  But that doesn't make sense.]

The committee adjourned.

Elbridge Gerry writes to James Monroe about the importance of the meeting.  He
believes that unless they come up with an agreed upon constitution, "*Force* I
expect will plant the standard."(III, 45)  He worries about anarchy, foreign
invasion, and domestic insurrection.

William Blount (NC) writes to Joseph Clay that he will be leaving to join the
convention "in a few days".(IV, 70)

# June 12, Tuesday

The committee opened with a vote on the fifteenth resolution which passed in
the affirmative.

Sherman moved, seconded by Ellsworth, that the members of the house of
representatives be elected every year.  Sherman chose the length of the term
arbitrarily in order to open up a discussion.

Rutledge preferred every two years.

Jenifer preferred every three years.  If elections are too often, it will
produce indifference, and the best men would be unwilling to serve for so
unstable an office.

Madison seconded Jenifer's proposal for three year terms.  Instability is one
of the vices of our present republic to be remedied.  It will take three years
just to get up to speed on the interests of the different states in so vast a
nation.  Most of a year term, on the other hand, will be spent primarily
preparing for travel and traveling to and from the capital.

Gerry said that the people of New England will never trust a term of office
longer than a year.  Annual elections are the only defense against tyranny.
He was against three-year terms as much as he was against hereditary monarchy.

Madison did not think they should guess at what the people would approve or
disapprove.  There is as little chance at knowing what the people in their
ignorance would prefer today as there would be to know what they would approve
of if they had the same insight as the present delegates or what they would
approve of six or twelve months from now.  We ought to base our deliberations
solely on what would make a proper government.  Such a plan, combined with the
respectability of this convention, will recommend itself in time.

Gerry asserted that it was important to consider what the people would be
willing to approve which is the heart of all good legislation.  If we agreed
that a limited monarchy were the best form of government, we would be in the
wrong to foist a style of government on a people and social institutions which
it did not suit.

The committee voted and approved three-year terms for the house of
representatives.

The committee voted to strike out any age requirement for the house of
representatives.

Madison moves to consider a fixed compensation for representatives.  To leave
compensation up to the state legislatures might create a dangerous dependence.
He thought they could perhaps set the compensation based on some fixed
commodity like wheat.

Mason seconded the motion.  It would be unfair to leave compensation for
representatives to the discretion of the states.  Representatives might feel
an inequality based on differences of compensation when they should all be on
equal footing.  If the states did not offer enough of a compensation, then the
representatives might be made up not of the fittest delegates but only those
who were most willing to serve for the lowest compensation.

Madison's motion was voted on and approved.

Franklin moved to strike out the word "liberal" from the resolution of
compensation just agreed to, worrying that the compensation might grow out of
bounds.  He likened it to the progression of tithing in apostolic times to the
papal system of today.  He preferred "moderate" if anything.

The word "liberal" was struck out without argument.

The committee approved a motion by Pierce that the wages should be paid out of
the national treasury.

[Seven amendments to Randolph's resolutions are voted on.  None of these
changes seem important, and Madison records no debates, so they are probably
not worth listing explicitly.(I, 216-8)]

Spaight moves to set the terms of senators at seven years.

Sherman believed seven years was too long.  If the senator was good, he would
be reelected.  If he was bad, he should be gotten rid of at an earlier
opportunity.  He preferred five years, being half-way between the
representative and presidential terms.

Pierce preferred three years.  Seven years was too close to the British
parliamentary terms which were looked down upon by most good statesmen.

Randolph wanted seven year terms.  Stability of office was necessary in the
senate to combat the democracy of the house of representatives.  Otherwise,
the more numerous body will overwhelm it.  He worries that the executive will
join with the demagogues of the house to undermine the constitution, and the
senate must guard against that.

Madison asserted that seven years was in no way too long.  Stability was what
was most needed as that was the attribute most lacking in republican
government.  He worried that even with that long of a term, the senate would
be overrun by the democratic house.  Maryland bore the most resemblance to
this suggestion, and there were no suspicions of danger from the senators'
longer terms.  On the other hand, the states with the same length of terms as
the other house were no match for the instabilities of the latter.(I, 219)  He
worried that if they failed to secure the happiness of the people and
stability of the republican system, the people would turn back to monarchy.(I,
221)  [Yates attributes the last sentence to one of Madison's earlier
arguments.  However, when comparing the wording between Madison's and Yates's
accounts, it sounds closer to the last line of Madison's account of this
argument.  Hence why I've placed it here.]

The committee voted in the affirmative for a senatorial term of seven years.

Butler moved, seconded by Rutledge, that the senators receive no salary or
compensation.

The committee voted against the motion.  Madison, in his journal, suspected
that the reason this failed was that the delegates worried that if no salary
was paid by the national government, then the states would pay it, and the
inequality that was debated above with respect to the representatives would
apply.

It was then moved, and the committee agreed, to set the stipends and
ineligibility rules of the senate identically to the house.

[Another list of resolutions and votes that Madison just copied from the
Journal.(I, 211-2, 220)]

The committee adjourned.

# June 13, Wednesday

The ninth resolution (on the national judiciary) which had been postponed at
the end of the convention yesterday was taken up.

Randolph moved, seconded by Madison, "that the jurisdiction of the national
judiciary shall extend to all cases, which respect the collection of the
national revenue, impeachments of any national officers, and questions which
involve the national peace and harmony".  The wording was agreed to.

Pinckney moved, seconded by Sherman, moved that the judges be appointed by the
national legislature.

Madison objected to appointment by the whole legislature.  They would not be
competent judges of the qualifications that would make a good judicial
official.  He preferred appoint by the senate alone.  They would be better
judges and numerous enough to be trusted.

Pinckney withdrew his motion, and the committee agreed to judicial appointment
by the senate unanimously.

Gerry moved to limit the origination of "money bills" [bills relating to
money?] to the house of representatives.  Being the democratic branch of the
legislature, he felt it was the people who should hold the purse strings.

Butler saw no reason for the distinction and criticized Gerry and other
delegates for trying too hard to follow the frame of the British government
even when it wasn't applicable.  If too many discriminations were placed on
the senate, then the best men will choose instead to serve in the house where
they can tack on other clauses to money bills.

Gerry said that although they were all still sore about the war, that was no
reason to make our government in as many ways as possible the opposite of them
either.(I, 238)

Madison observed that no one agreed on why this particular distinction was
made in the House of Lords.  He could see no reason not to trust the function
to the senate any more than the house.  If a senator had some nefarious bill
he intended to pass, there was no reason he couldn't find a representative to
introduce it in the house for him.  If Gerry really wanted to prevent such a
thing on principle, he ought to restrain the *amendment* as well as
origination of money bills.

King concurred with the arguments against the motion.

Read favored the proposition but did not want to extend the principle to
amendments.

Pinckney felt the question was premature.  They had not even worked out the
ratio of representation in the branches.  If they had the same proportional
representation, they should have the same power.

Sherman assures them that there is no danger in the senate having this power
regardless of how it is formed.  The senate is the representatives of the
people, too, and they must also pay their taxes.  In Connecticut both houses
may originate such bills, and there has been no trouble.

C. C. Pinckney points out that South Carolina does have such a distinction,
and it is a constant source of disputes between the two branches.

Williamson points out one advantage to the distinction:  Since the
representative is elected by the people, the people can hold the one who makes
the motion accountable.  [But wouldn't that be true for every bill, not just
money ones?]

Gerry's motion is voted down.

Gorham then read the resolutions as they had been amended so far (see below).
Afterwards the committee adjourned.

Luther Martin commented on it later to the Maryland legislature:  "To these
propositions, so reported by the committee, no opposition was given by that
illustrious personage [Washington], or by the President of the State of
Pennsylvania.  They both appeared cordially to approve them, and to give them
their hearty concurrence; yet this system I am confident, Mr. Speaker, there
is not one member in this House who would advocate, or who would hesitate one
moment in saying it ought to be rejected."(III, 178)

## Comparison of Original and Amended Randolph Resolutions

| | **May 29** (I, 20-2) | | **June 13** (I, 228-32) |
|-|--------|-|---------|
|  1 | Resolved that the articles of Confederation ought to be so corrected & enlarged as to accomplish the objects proposed by their institution; namely. "common defence, security of liberty and general welfare." | | |
|    | |  1 | Resolved that it is the opinion of this Committee that a national government ought to be established consisting of a Supreme Legislative, Judiciary, and Executive. |
|  2 | Resolved therefore that the rights of suffrage in the National Legislature ought to be proportioned to the Quotas of contribution, or to the number of free inhabitants, as the one or the other rule may seem best in different cases. | | |
|  3 | Resolved that the National Legislature ought to consist of two branches. |  2 | Resolved that the national Legislature ought to consist of Two Branches. |
|  4 | Resolved that the members of the first branch of the National Legislature ought to be elected by the people of the several States every ______ for the term of ______ ; to be of the age of ______ years at least, to receive liberal stipends by which they may be compensated for the devotion of their time to public service; to be ineligible to any office established by a particular State, or under the authority of the United States, except those peculiarly belonging to the functions of the first branch, during the term of service, and for the space of ______ of its expiration; to be incapable of re-election for the space of ______ after the expiration of their term of service, and to be subject to recall. |  3 | Resolved that the Members of the first branch of the national Legislature ought to be elected by the People of the several States for the term of Three years; to receive fixed stipends, by which they may be compensated for the devotion of their time to public service; to be paid out of the National Treasury; to be ineligible to any Office established by a particular State or under the authority of the United States (except those peculiarly belonging to the functions of the first branch) during the term of service, and under the national government for the space of one year after its expiration. |
|  5 | Resolved that the members of the second branch of the National Legislature ought to be elected by those of the first, out of a proper number of persons nominated by the individual Legislatures, to be of the age of ______ years at least; to hold their offices for a term sufficient to ensure their independency, to receive liberal stipends, by which they may be compensated for the devotion of their time to public service; and to be ineligible to any office established by a particular State, or under the authority of the United States, except those peculiarly belonging to the functions of the second branch, during the term of service, and for the space of ______ after the expiration thereof. |  4 | Resolved that the Members of the second Branch of the national Legislature ought to be chosen by the individual Legislatures; to be of the age of thirty years at least; to hold their offices for a term sufficient to ensure their independency, namely seven years; to receive fixed stipends, by which they may be compensated for the devotion of their time to public service -- to be paid out of the National Treasury; to be ineligible to any Office established by a particular State, or under the authority of the United States (except those peculiarly belonging to the functions of the second branch) during the term of service, and under the national government, for the space of One year after its expiration. |
|  6 | Resolved that each branch ought to possess the right of originating Acts; that the National Legislature ought to be empowered to enjoy the Legislative Rights vested in Congress by the Confederation & moreover to legislate in all cases to which the separate States are incompetent, or in which the harmony of the United States may be interrupted by the exercise of the individual Legislation; to negative all laws passed by the several States, contravening in the opinion of the National Legislature the articles of Union; and to call forth the force of the Union against any member of the Union failing to fulfill its duty under the articles thereof. |  5 | Resolved that each branch ought to possess the right of originating acts. |
|    | |  6 | Resolved that the national Legislature ought to be empowered to enjoy the legislative rights vested in Congress by the confederation -- and moreover; to legislate in all cases to which the separate States are incompetent: or in which the harmony of the United States may be interrupted by the exercise of individual legislation; to negative all laws passed by the several States contravening, in the opinion of the national legislature, the articles of union, or any treaties subsisting under the authority of the union. |
|    | |  7 | Resolved that the right of suffrage in the first branch of the national Legislature ought not to be according to the rule established in the articles of confederation: but according to some equitable ratio of representation -- namely; in proportion to the whole number of white and other free citizens and inhabitants of every age, sex, and condition including those bound to servitude for a term of years, and three-fifths of all other persons not comprehended in the foregoing description, except Indians, not paying taxes in each State. |
|    | |  8 | Resolved that the right of suffrage in the second branch of the national Legislature ought to be according to the rule established for the first. |
|  7 | Resolved that a National Executive be instituted; to be chosen by the National Legislature for the term of ______ years, to receive punctually at stated times, a fixed compensation for the services rendered, in which no increase or diminution shall be made so as to affect the Magistracy, existing at the time of increase or diminution, and to be ineligible a second time; and that besides a general authority to execute the National laws, it ought to enjoy the Executive rights vested in Congress by the Confederation. |  9 | Resolved that a national Executive be instituted to consist of a Single Person, to be chosen by the National Legislature, for the term of Seven years; with power to carry into execution the National Laws; to appoint to Offices in cases not otherwise provided for, to be ineligible a second time, and to be removable on impeachment and conviction of malpractice or neglect of duty; to receive a fixed stipend, by which he may be compensated for the devotion of his time to public service; to be paid out of the national Treasury. |
|  8 | Resolved that the Executive and a convenient number of the National Judiciary, ought to compose a council of revision with authority to examine every act of the National Legislature before it shall operate, & every act of a particular Legislature before a Negative thereon shall be final; and that the dissent of the said Council shall amount to a rejection, unless the Act of the National Legislature be again passed, or that of a particular Legislature be again negatived by ______ of the members of each branch. | 10 | Resolved that the national executive shall have a right to negative any legislative act: which shall not be afterwards passed unless by two-thirds parts of each branch of the national Legislature. |
|  9 | Resolved that a National Judiciary be established to consist of one or more supreme tribunals, and of inferior tribunals to be chosen by the National Legislature, to hold their offices during good behaviour; and to receive punctually at stated times fixed compensation for their services, in which no increase or diminution shall be made so as to affect the persons actually in office at the time of such increase or diminution; that the jurisdiction of the inferior tribunals shall be to hear & determine in the first instance, and of the supreme tribunal to hear and determine in the dernier resort, all piracies & felonies on the high seas, captures from an enemy; cases in which foreigners or citizens of other States applying to such jurisdictions may be interested, or which respect the collection of the National revenue; impeachments of any National officers, and questions which may involve the national peace and harmony. | 11 | Resolved that a national Judiciary be established to consist of One supreme Tribunal; the Judges of which to be appointed by the second Branch of the National Legislature; to hold their offices during good behaviour; to receive, punctually, at stated times, a fixed compensation for their services: in which no increase or diminution shall be made so as to affect the persons actually in office at the time of such increase or diminution. |
|    | | 12 | Resolved that the national Legislature be empowered to appoint inferior Tribunals. |
|    | | 13 | Resolved that the jurisdiction of the national Judiciary shall extend to cases which respect the collection of the national revenue: impeachments of any national Officers: and questions which involve the national peace and harmony. |
| 10 | Resolved that provision ought to be made for the admission of States lawfully arising within the limits of the United States, whether from a voluntary junction of Government & Territory or otherwise, with the consent of a number of voices in the National legislature less than the whole. | 14 | Resolved that provision ought to be made for the admission of States, lawfully arising within the limits of the United States, whether from a voluntary junction of government and territory, or otherwise, with the consent of a number of voices in the national Legislature less than the whole. |
|    | | 15 | Resolved that provision ought to be made for the continuance of Congress and their authorities until a given day after the reform of the articles of Union shall be adopted; and for the completion of all their engagements. |
| 11 | Resolved that a Republican Government & the territory of each State, except in the instance of a voluntary junction of Government & territory, ought to be guaranteed by the United States to each State. | 16 | Resolved that a republican Constitution, and its existing laws, ought to be guaranteed to each State by the United States. |
| 12 | Resolved that provision ought to be made for the continuance of Congress and their authorities and privileges, until a given day after the reform of the articles of Union shall be adopted, and for the completion of all their engagements. | | |
| 13 | Resolved that provision ought to be made for the amendment of the Articles of Union whensoever it shall seem necessary, and that the assent of the National Legislature ought not to be required thereto. | 17 | Resolved that provision ought to be made for the amendment of the articles of Union, whensoever it shall seem necessary. |
| 14 | Resolved that the Legislative Executive & Judiciary powers within the several States ought to be bound by oath to support the articles of Union. | 18 | Resolved that the Legislative, Executive, and Judiciary powers within the several States ought to be bound by oath to support the articles of Union. |
| 15 | Resolved that the amendments which shall be offered to the Confederation, by the Convention ought to at a proper time, after the approbation of Congress to be submitted to an assembly or assemblies of Representatives, recommended by the several Legislatures to be expressly chosen by the people, to consider & decide thereon. | 19 | Resolved that the amendments which shall be offered to the confederation by the Convention, ought at a proper time or times, after the approbation of Congress to be submitted to an assembly or assemblies of representatives, recommended by the several Legislatures, to be expressly chosen by the People to consider and decide thereon. |

# June 14, Thursday

Paterson reported that some of the deputies, including those from New Jersey,
had been developing a purely federal plan in response to the Virginia Plan.

The committee adjourned in expectation that they would have the proposed plan
tomorrow.

William Blount hears the opinion of Pierce that evening that the convention
"will not rise before the middle of October", and "very little is done and
nothing definitive".(IV, 76)

# June 15, Friday

Paterson presented the resolutions which he and others had prepared as a
federal contrast to the national system originally submitted by Randolph and
that the committee had revised.  [The following is a summary of the
resolutions.  The most accurate version is recorded by Madison.(I, 242-5 &
III, 611-6)]

1. The AoC ought to be revised to render the constitution adequate to good
   government and to preserve the union.
2. Congress ought to have power to tax (in order to raise revenue) and to
   negotiate trade and commerce.  Any disputes should be handled first by the
   state judiciary in which the offense occurred with possible appeal to the
   judiciary of the United States.
3. Congress may requisition money from the states in proportion to the number
   of free inhabitants and 3/5 of slaves.  If requisitions are not complied
   with in the time specified, then Congress may pass acts authorizing the
   collection.  None of the powers of Congress can be exercised without the
   consent of at least ______ states.
4. Congress is to elect a federal Executive to have roughly the same powers as
   listed in the revised Virginia Plan.  [The resolution leaves the executive
   open to a unity or plurality.]
5. The Executive is to appoint judges to a single supreme tribunal to be the
   federal Judiciary.  The judges have roughly the same powers and
   responsibilities as those in the revised Virginia Plan.
6. Federal law trumps state law, and the several state judiciaries shall be
   bound to Congressional decisions.  The Executive has the power to call down
   a sufficient force of the confederation to compel obedience to
   Congressional acts and treaties.
7. Provision ought to be made for the admission of new states to the union.
8. Rules for naturalization should be the same in every state.
9. A citizen of one state, found guilty of a crime in a different state, shall
   be treated just like a criminal of the state in which the offense occurred.

After reading the resolutions, Paterson added that the federal government he
had in mind required a standing army to give the government weight.(I, 246)

Lansing seconded the resolutions.(IV, 75)

Luther Martin later reported on the development of the Paterson Resolutions
(New Jersey Plan):  "[W]hile those [Randolph Resolutions] were the subject of
discussion in the committee of the whole House, a number of members, who
disapproved them, were preparing *another system*, such as *they thought more
conducive to the happiness and welfare of the states*."(III, 174)  "The
members who prepared these resolutions were principally of the Connecticut,
New York, Jersey, Delaware, and Maryland delegations."(III, 178-9)

Madison agrees with these facts in his journal and singles out L. Martin as
*the* delegate from Maryland.  Connecticut and New York were against a
national system, preferring to merely expand the powers of Congress.  Delaware
and New Jersey wanted to maintain the equality of suffrage of the states in
the legislature.  Dickinson (DE) said to Madison, "You see the consequence of
pushing things too far.  \[...\] [W]e would sooner submit to a foreign power,
than submit to be deprived of an equality of suffrage, in both branches of the
legislature, and thereby be thrown under the domination of the large
States."(I, 242, footnote) According to Madison, "The eagerness displayed by
the members opposed to a national government from these different motives
began now to produce a serious anxiety for the result of the Convention."(I,
242, footnote)

While Paterson is supposed by Farrand to be the principle author of the New
Jersey Plan, being the one who presented it to the convention, Lord Dorchester
writes to Lord Sydney (Oct. 14, 1788) that it was "supposed to be the
production of Governor [William] Livingston" (NJ).(III, 353-4)  Madison
presumes that, while Livingston was not active in the debates on the floor of
the convention, he had an active influence in some of the committees.(III,
496)

Madison moved for a subcommittee to compare and contrast the two plans before
them.(I, 246)

Butler moved to discuss the resolutions immediately.(IV, 75)

Lansing proposed, considering the importance of the resolutions, that the
committee take the rest of the day to make copies and familiarize themselves
with the new resolutions.(I, 246)

Hamilton cannot say he prefers one plan over the other.(I, 246)

The committee adjourned.

In contrast to the recorded statement by Madison, Dickinson seems to be in
good spirits that evening when he writes, "The Convention is very busy -- of
an excellent temper -- and for Abilities exceeds, I believe any Assembly that
ever met upon this Continent, except the first Congress."(IV, 75)

# June 16, Saturday

[Farrand suggests reading Luther Martin's *Genuine Information* (Appendix A,
CLVIII) for the debates June 16-18.  Pinckney summarizes these same debates
and their solution.(III, 249,252-3)]

Lansing asked that the first resolutions of both proposals, Paterson's and
Randolph's, be read, and he compared them.  Paterson sustains the sovereignty
of the states, Randolph destroys them; the first, certain general powers for
the general good, the latter, negatives on all state laws.  He had two primary
objections to Randolph's resolutions:

1. The convention does not have the authority to completely change the United
   States from a federal system.
2. The proposal will never be adopted.

The language of the convention instructions is explicit:  "it is, upon the
revision of the present confederation, to alter and amend such parts as may
appear defective, so as to give additional strength to the union."(I, 257)
Had the New York legislature known what this committee would be proposing,
they would have never sent delegates to it.  This scheme is too novel, the
changes suggested too sudden, and the states will never willingly cede their
powers to a general government.  The people are already familiar with
Congress, and they will readily approve simple augmentations to it.

Paterson had given his criticisms of Randolph's plan earlier, and he would not
repeat himself.  Instead, he would discuss the strengths of his plan, namely
that it was in accordance with the powers of the convention and the sentiments
of the people.  I do not speak for myself but for those who sent me.  Our
purpose is not to construct the best possible government but only the best
government we have been authorized to make and be approved.  Whether or not we
believe we are still under the laws of the confederation, each state is
sovereign and independent.  We meet here as equals.  If we think proportional
representation is really the best method of suffrage, then why do we not use
it here in this convention?(I, 250)  Equal suffrage is guaranteed by the fifth
article.  It is true that the thirteenth allows us to amend it but only with
unanimous consent.(I, 258)

If the sovereignty of the states is to be maintained in the federal
government, then the representatives must be elected from the states and not
from the people.  Wilson has stated that all power is derived from the people,
and I agree with him that proportional representation based on population is
right in principle.  Unfortunately, it is wrong for present application.  When
independent societies contract together for mutual defense, they enter into
the compact as equals.  Destroy the equality between them, and you endanger
the rights of the citizens at the state level by usurpation in the
national.(I, 259)  The solution to the proposed plan is to equally divide the
state boundaries.  Try it and see if the people will allow it.

The efficacy of either plan depends on the power granted to the general
government, not whether such power is drawn from the states or from the
people.  Randolph urges two legislatures are necessary in order to provide a
check on one another.  But that is not necessary in my plan (or the current
confederation) because the delegations from the different states are checks on
each other.  Do the people complain that Congress is only one body?  No, they
complain because Congress does not have sufficient power.  With proper powers
Congress will act with more energy and wisdom than the proposed national
legislature because the representatives will be fewer, more secreted, and of
better quality by the means of election.

The Randolph proposal is also unnecessarily expensive.  Allow one delegate
each to Georgia and Delaware, and the senate will be composed of 90 senators.
The house of representatives being more numerous must have at least 180
members.  That is 270 representatives traveling to the nation's capital at
least once a year -- in some cases from the most distant reaches of the union.
Based on our current finances, can anyone seriously consider such a plan?
Merely enlarging the powers of Congress will save us this expense, and all
governmental purposes will be provided for.

Wilson, since he had had time to examine both proposals at length, compared
the principle points of the proposals as he saw them(I, 176-8):

| Randolph | Paterson |
|----------|----------|
| two branches of the legislature | single branch |
| based on the original authority of the people | based on the derivative authority of the state legislatures |
| proportional representation based on population | equal representation among states without regard to population or importance |
| single executive | [possible] plural executive |
| majority empowered to act | small minority able to control |
| national legislature empowered to legislate in all cases where the states are incompetent or the harmony of the US is in danger | Congress invested with additional powers only in a few inadequate instances |
| veto laws contrary to national laws or treaties | call forth confederate power to compel obedience |
| executive removable on impeachment and conviction | ...on application of removal by the state executives |
| executive veto | none |
| inferior tribunals of the national judiciary | none |
| jurisdiction of national tribunal extends to revenue | ...only on appeal from the state judiciary |
| jurisdiction of national tribunal extends to questions of national peace | only limited jurisdiction |
| ratification of the plan by delegates elected by the people for that explicit purpose | alterations to the articles must be confirmed by the legislature of every state |

Wilson then addressed the primary objections to the Virginia Plan.  With
respect to the power of the convention, he felt empowered to propose anything.
In this he felt indifferent between the two plans.  With respect to the
sentiments of the people, he found it difficult to discern.  He could not
convince himself that the state governments were so beloved and a national
government so obnoxious to the people as other delegates assumed.  Why should
national government be unpopular?  Will a citizen of Delaware be degraded by
becoming a citizen of the United States?  Where do the people look for relief
from their present evils?  Is it in the reformation of their state
governments?  No, sir.  It is from the national councils that they expect
relief.  Do not worry that they will follow a national government.  An
additional strength of Randolph's plan is that it recommends the ratification
to the people and not the states.

As much as augmentation of the AoC is desirable, we should be extremely
reluctant to grant more power to Congress because (1) Congress does not derive
its power from the people, and (2) it is a single body.  "Inequality in
representation poisons every government."(I, 261)  The legislature of Britain
is notoriously corrupt for this reason, and the security of private rights is
maintained only due to the independence of the judiciary from the legislature.

Regarding the single branch of Congress:  Despotism comes in many guises.  We
have so far worried about executive and military despots, but is there no
danger of despotism by the legislature?  In one branch there is no check on
its power excepting an inadequate one -- the virtue and good sense of its
members.

Vesting the executive power in a single magistrate is desirable over a
plurality.  In order to control legislative authority, you must divide it; to
control executive authority, you must unite it.  History proves the danger.
[See historical examples I, 254,261.]

Wilson cut himself short here believing that he had already taken up too much
of the convention's time.  He felt the arguments he had already made were
sufficient for rejecting Paterson's plan.  He would go on to justify his
stance in the Pennsylvania ratification convention by saying that the were not
foisting a constitution on the republic; they were submitting it to the
consideration of the people who were to accept or reject it as they
please.(III, 143)  [Spaight, by the way, would make the same justification
before the North Carolina convention:  "What the convention has done is a mere
proposal."(III, 351)]

C. C. Pinckney [-- Yates(I, 261), although Madison says this is Pinckney(I,
255), Lansing is ambiguous(IV, 78)] believed that if New Jersey was given an
equal vote in the legislature, then she would submit to the national system.
He agreed with Wilson that the convention had the authority to suggest any
changes it felt necessary to remedy the evils which had necessitated the
convention in the first place.

Ellsworth proposed "that the legislative power of the US should remain in
Congress".  No one seconds.  Madison thinks this motion was made to focus the
committee on the primary principle of the New Jersey Plan.(I, 255)  Yates
instead says that Ellsworth proposes they take up the second resolution of the
new plan.(I, 262)

Randolph believed it would be treason *not* to propose anything they felt
necessary to save the republic:  "We would be traitors to our country if we
did not embrace this parting angel."(IV, 79)  The confederation was broken and
needed substantial reform.  [Examples given I,262; Shays's Rebellion mentioned
on 263.]  The committee would not have gone so far with the initial
resolutions if the majority of the states had not thought they were necessary
and authorized.  The question is whether we will propose a national plan or a
federal one, and the latter has already shown its insufficiency.(I, 255-6)
Paterson said that amendment of the fifth article requires unanimous consent
because of the thirteenth.  "This surely is false reasoning, since the whole
of the confederation upon revision is subject to *amendment and alteration*;
besides our business consists in recommending a system of government, not to
make it.  There are great seasons when persons with limited powers are
justified in exceeding them, and a person would be contemptible not to risk
it."(I, 262)

There are only two methods by which we can achieve a general government:
compulsion, as suggested by Paterson, or "real legislation", as suggested by
the other plan.  Compulsion would be "*impracticable, expensive,* [and] *cruel
to individuals.*"(I, 256)  We must resort to a national legislature over
individuals rather than a federal legislature over states.  If the Union has
been so far safe from despotism under Congress, it is only due to the
impotency of Congress.  Power must be given to the legislature to regulate
trade, naturalization, and put down domestic insurrection.  His own experience
satisfied him that Congress -- with its representation and method of election
-- can never be trusted with those powers.(I, 256)  "I am certain that a
national government must be established, and this is the only moment when it
can be done -- And let me conclude by observing, that the best exercise of
power is to exert it for the public good."(I, 263)

The committee adjourned.

[See Appendix A, CLXVII for why Yates and Lansing left the convention early.
This will come up later.]

# June 18, Monday

James Varnum (RI) wrote a letter to Washington this day assuring him that the
upper house, governor, and council of Rhode Island supported the convention,
but the state was still unable to send delegates because a large majority of
the lower house was against it.  He characterizes the supporters as educated
members of the upper class and those against it as unprincipled demagogues who
take advantage of the common people whose attention is wholly directed to
abolishing all debts.(III, 48)

Gorham, the chairman of the committee, wrote a letter this day to a
congressional delegate:  "In short, the present federal government seems near
its exit; and whether we shall in the convention be able to agree upon mending
it, or forming and recommending a new one, is not certain.  All agree,
however, that much greater powers are necessary to be given, under some form
or other."(IV, 93)

The convention that day began with Dickinson moving to modify the wording of
Paterson's first resolution to read:

> Resolved that the articles of confederation ought to be revised and amended,
> so as to render the government of the United States adequate to the
> Exigencies, the preservation, and the prosperity of the Union.

Dickinson had, unknown to the other delegates, worked all weekend on a new
plan of government.  This motion was the first resolution in his plan.  It is
likely that he would have moved for the full proposal of his plan had the
first resolution been accepted.(IV, 84-5, footnote)  Dickinson's plan was an
attempt at consolidating the Virginia and New Jersey plans with the addition
of three equal executives to be elected from the three regions of the union --
an organization suggested by Randolph on June 2.(IV, 88-91)

Unfortunately for Dickinson, Hamilton unwittingly interfered with his plan.

The committee voted to postpone the first resolution of the New Jersey Plan to
take up Dickinson's motion, but before any discussion could begin, Hamilton
stood to deliver a speech which would be (correctly and incorrectly)
recollected and retold for years after.  Hamilton's son, John Church Hamilton,
a historian, stated that the speech "occupied in the delivery between five and
six hours, and was pronounced by a competent judge, (Gouverneur Morris), the
most able and impressive he had ever heard."(I, 293, footnote)  [Based on a
comment by Madison on July 2, G. Morris might not have been been present at
the convention to hear this speech.(I, 511, footnote)  William Shippen, a
Philadelphia physician and former delegate to the Continental Congress,
records that Hamilton's speech was only three hours long.(IV, 97)  Though he
was not present to attest to this first-hand.]

Hamilton explained that he had remained mostly silent this far into the
convention because of his great respect for the men of the assembly and his
hesitance at publicly disagreeing with them.  That he did not share the same
views as his other colleagues from New York, Yates and Lansing, that the AoC
could be merely amended also caused some difficulty.  However, the convention
had reached a crisis, and he felt it was his duty to say whatever he could to
help preserve the public safety and happiness.

He admitted that he did not approve of either plan.  He was much more strongly
opposed to the New Jersey Plan.  The states could not remain sovereign and
maintain the confederation, and thus no amendment that preserved their
sovereignty was worth considering.  On the other hand, I have doubts that the
Virginia Plan can be made effective.

Some of the recent debates hinge on the meaning of the word 'federal'.  A
federal government is an association of independent communities into one.
Confederacies can be made among different states, but they can also be made
among individuals -- as the Diets of Germany are.  Some say the Virginia Plan
departs from the federal form since it is to operate on individuals.  He,
however, agreed with Randolph that whether or not they had expressly been
given the power, they ought to do everything possible to frame a stable
government.  "The great question is what provision shall we make for the
happiness of our country?"(I, 284)

The principles necessary for the support of government are:
1. active and constant interest in supporting it
   * each state has its own internal interests opposed to the whole
2. the love of power
   * the states wish to hold on to their own power rather than give it to the
     general government
3. habitual sense of obligation from the people
   * the people are attached to their state governments; they do not feel the
     same attachment to the general
4. force, i.e. the coercion of laws or the coercion of arms
   * Congress has very little of the former and none of the latter
   * (and besides coercion of arms would never work on the states; it would
     mean war)
5. influence
   * men of merit ought to be rewarded with honor and profit
   * avarice, ambition, and self-interest "which govern most individuals and
     all public bodies"(I, 285) are harnessed by the states but not by the
     general government
   * [This sounds like the fundamental principle of monarchies (and
     aristocracies to an extent) from Montesquieu.  Titles and other honors
     which lead to fortune lend stability to monarchical power.  Books 3-5]
   * [historical examples I, 285]

Whatever solution we are looking for must shift all the above principles away
from the states and into the general government to lend it any stability.  The
scheme of New Jersey does not produce this effect.  Giving more power to
Congress will eventually result in a bad government or no government.

Look to the federal institution of Germany or the Swiss cantons or the
Helvetic union.  They are all inefficient at defending from foreign invasion
and preventing domestic conflict.(I, 296)

He was discouraged when he looked at the extent of what a general government
would have to rule.  The maintenance of the general government would be a
great expense, and he thought great expense might be gained by extinguishing
the state governments and replacing them with a general government.  He wasn't
actually proposing such a action but nor was he completely ruling it out.  He
believed that the British government was perhaps the best in the world and
wondered aloud if only a government like it could rule in America.

In every community where industry is encouraged, there will be a division of
people into the few (wealthy) and many ("landed").  If power is only given to
one or the other, then they will oppress their opposite.  Power must be given
to both to defend themselves from the other.  An good example is the balance
between the House of Lords and Commons.  No temporary senate will serve the
purpose as well as their Lords.  Even seven years will not provide enough
firmness to counter the violence and turbulence of the democratic spirit.

On the topic of the executive, a hereditary monarch seems to be the best
model.  The hereditary interest of the king is so interwoven with that of the
nation that he is immunized from foreign influence and corruption -- which is
the biggest danger for republics.  The British system further makes him both
sufficiently independent and controlled to fulfill the purpose of government
domestically.

Therefore, both the American executive and the senators ought to serve during
good-behavior.  This will induce a sacrifice of public affairs and an
acceptance of public trust to ensure the services of the best citizens.  This
is republicanism and not monarchy or aristocracy as long the positions are
still elected by the people.  An ambitious, temporary magistrate would seek to
prolong his power.  In time of war, he might refuse to step down and use the
emergency to maintain his position against the law.  An executive for life
would not have such a motivation and would be more focused on doing the right
thing in the situation than protecting his station.

Hamilton anticipated that some would criticize his plan as an *elective
monarchy* -- the worst kind.  But isn't this a question of semantics.  If my
suggested magistrate is an elected monarch for life, then isn't the one in the
Virginia Plan an elected monarch for seven years?  He thought the criticisms
of elective monarchs were drawn from a few cherry-picked historical examples
and not from the general character of the office.

Hamilton submitted his plan for consideration, not as a formal proposal but
instead to more clearly explain his views and the resolutions he might propose
in the future to the Virginia Plan.  He admitted it would go beyond the ideas
of most of the members, but because the confederation was dissolving or had
already dissolved, he believed the people were losing their confidence in
democracy, and they would eventually wish for a plan that went beyond
Randolph's.(I, 291)  "He described the Virginia Plan as being nothing but
democracy, checked by democracy".(III, 413)  "I hoped after so long a course
of pork that our diet would be changed, but I find it is pork still with only
a change of sauce."(III, 404)

Hamilton read his plan with the following general resolutions:

1. A legislature of two houses -- an assembly and a senate.
2. Assembly members to be elected by the people for a term of three years.
3. Senators to be elected by electors elected by the people for a term of
   good-behavior (i.e. life).
4. The Executive to be elected by electors elected by electors elected by the
   people [yes -- tertiary election, see Appendix F] for a term of
   good-behavior with authority of legislative veto, appointing officers,
   commander-in-chief, treaties, pardon, etc.
5. On death of the executive, his office shall be filled by the president of
   the senate until a successor is elected.
6. Senate to have sole authority of declaring war and peace and approving
   treaties and executive appointments.
7. Supreme judiciary in which judges hold their terms during good-behavior as
   well.
8. Legislature empowered to establish inferior tribunals for the judiciary.
9. Rules for impeachment and conviction of government officers.
10. The governor of each state to be appointed by the general government with
    the power of veto over state laws.
11. The general government to have complete control over the state militias.

[See Appendix F.  The above are derived from Madison's copy which is probably
the most accurate with probable changes in the forth article taken from
Appendix F.]

The committee adjourned.

For years after the convention, Hamilton would at various times be accused of
attempting to institute a monarchy and abolish the states.(III, 617,
footnotes)  The accusations were doubtlessly produced from misunderstandings
and retellings of some parts of this day's speech.  He had an answer for those
who impugned his character, "To arraign the morals of any man, because he
entertains a speculative opinion on government different from ourselves, is
worse than arrogance.  He who does so, must entertain notions in ethics
extremely crude, and certainly unfavorable to virtue."(III, 396)

Morgan Lewis, New York governor from 1804-1807, asked Hamilton about his famed
speech and the reason for suggesting the president be elected for life.  Lewis
recorded Hamilton's reply:

> "My reasons," he said, "were, an exclusion, as far as possible, of the
> influence of executive patronage in the choice of a chief magistrate, and a
> desire to avoid the incalculable mischief which must result from the too
> frequent elections of that officer."  In conclusion, he made the following
> prophetic observation:  "You nor I, my friend, may not live to see the day,
> but most assuredly it will come, when every vital interest of the state will
> be merged in the all-absorbing question of *who shall be the next*
> **President**?"

# June 19, Tuesday

When Gorham took the chair, he addressed the convention that the committee who
had considered the proposals from Randolph and Paterson had disagreed with
Paterson's and were now recommending the convention continue forward with the
Virginia Plan.

The convention voted against Dickinson's resolution from the day before.

Madison stood to address the criticisms of the Virginia Plan that he had heard
from the federalist camp for the past several debates.  Madison pointed out
some of the inconsistencies in Paterson's federal plan.  First, even though
Paterson said that a confederation operated on the states, instead, like
Hamilton had earlier stated, confederations *may* operate on individuals, and
in fact, some of the resolutions in Paterson's plan operated on individuals.
Second, Paterson's plan assumed that the delegates in Congress represented the
states and not the people; however, in both Connecticut and Rhode Island the
delegates were elected directly by the people and not the state legislatures,
and there was no change to this method in the plan.

Madison replied to Paterson's earlier statement that since the confederation
had been entered into by unanimous consent, it could only be dissolved by
unanimous consent.  This does not represent any known law of compacts.  In
fact, if any member of a compact breaches the fundamental principles of the
compact, then all other members are at liberty and the compact is absolved.
[Possible reference to Locke, Ch. 2?]  There are possible hypothetical
exceptions which might exist in a compact to prevent this, but none of these
exceptions are even implied in the AoC.  In our case, "violations of the
federal articles had been numerous and notorious."(I, 315)

Madison then asked a series of questions meant to see if Paterson's plan
achieved the goals it sought to achieve, namely to preserve the union and to
remedy the evils felt by the states individually and as a whole.

(1) "Will it prevent the violations of the law of nations and of treaties
which if not prevented must involve us in the calamities of foreign wars?"(I,
316)  Congress already holds complaints from every country of the violations
of states to US treaties.  The plan leaves the will of the states as
uncontrolled as before.

(2) "Will it prevent encroachments on the federal authority?"(I, 316)  The
states have constantly encroached on the authority of the general government
by, e.g., engaging in treaties with one another (VA-MD and PA-NJ) and raising
a standing army (MA).  [More examples from Yates I, 326.]  Ancient examples
also bear witness to the tendency of the members to encroach on federal
authority.  [He gives historical examples.] The plan was particularly
defective in two regards:  First, ratification was to occur through the
legislatures instead of the people and therefore could never make federal law
even paramount to state law.  Second, because the plan gave the federal
judiciary appellate jurisdiction only, there was a danger of undue acquittals
in the state judiciaries.  This is compounded by the right of the governors to
pardon.

(3) "Will it prevent trespasses of the states on each other?"(I, 317)  Madison
singles out paper money as one such trespass by "debtor" states against
"creditor" states.  The plan, without even a veto over state laws, allows
these transgressions to continue.

(4) "Will it secure the internal tranquility of the states themselves?"(I,
318)  The recent insurrections in Massachusetts have shown the danger all the
states are in.  The plan provides no means of fixing this defect.  Republican
theory is based on the assumption that the majority holds right and power.
However, this can break down:
1. If the minority consist of those with the skills and habits of military
   life, then one-third may conquer the remaining two-thirds.
2. A minority of *citizens* may become a majority if enough other citizens in
   the majority do not hold the right of suffrage.  Such people are more
   likely to join the standard of sedition.
3. "Where slavery exists, the republican theory becomes still more
   fallacious."(I, 318)

(5) "Will it secure a good internal legislation and administration to the
particular states?"(I, 318)  Nothing in Paterson's plan addressed the evils of
the state legislatures, namely the multiplicity, mutability, injustice, and
impotence of the state laws.

(6) "Will it secure the union against the influence of foreign powers over its
members?"(I, 319)  We do not know for sure that such influence has been tried,
but naturally, they will.  [He gave more historical examples.]  By not giving
a veto over state laws, the plan left the door open for such "pernicious
machinations".

(7) Where would it leave the small states?  Under the plan the small states
would still have to fund the burden of sending their own representatives to
Congress, a cost which had prevented many of the smaller states from sitting
for extended periods of time.  There have also been curious cases in which
states sent as representation citizens and statesmen of other states.  This
was possibly done to avoid the cost of funding the delegates, but did it not
give votes to another state while taking them away from the smaller state?

(8) What will happen if the advocation of Paterson's "inadmissible" plan
prevents the adoption of any plan?  If the union dissolves, then one of two
outcomes will happen:  (1) The states remain independent and sovereign, or (2)
two or more new confederations will be formed.  In the first case, will the
smaller states feel more safe from their larger neighbors than they feel under
a confederation bound together for mutual benefit?  In the second case, do the
smaller states expect the larger states will confederate with them on the
principle of equal suffrage?  Will they get a better deal than they are
getting under the Virginia Plan?

The real problem that the federalists have is in the representation of
suffrage, and if this could be agreed to, then all others would be
surmountable.  [He addresses the proposal to divide the states evenly, but
since that was never a serious issue, I do not include it.]  What will be done
about the representation of western states that will eventually enter the
union from the western territories?  They will begin with small populations,
and thus their representation will be "right and safe".  But if they are given
equal suffrage, "a more objectionable minority than ever might give law to the
whole."(I, 322)

King moved that the committee rise and report that the New Jersey Plan was
inadmissible and that they should move forward with the Virginia Plan.(I, 327)
[Lansing said this was Wilson.(IV, 96)]

Dickinson -- who had tried to move his own resolutions yesterday -- suggested
contrasting the two and consolidating the parts that the committee might
approve.(I, 327)  Though he did not go into to details aloud, Dickinson had
drawn up his concerns with both plans in his notes(IV, 96):

Objections to the New Jersey Plan:
1. One branch of legislation is not enough.
2. All new states admitted on an equal footing which is dangerous.
3. Revenue depends on requisition and coercion.
4. Quotas to be settled in an unequal matter.

Objections to the Virginia Plan:
1. Proportional representation in both branches is unreasonable and dangerous.
2. He distrusts the indefinite power of the legislature.
3. A single executive -- "no instance of its ever being done with safety".

The convention voted to reject the consideration of Paterson's resolutions and
took up Randolph's resolutions once again.  Lansing says in his notes that he
submitted to the decision "because the sentiments of the committee on the
question of representation in the first instance could not be pointedly
taken."(IV, 96)

Ellsworth moves that first resolution of the Virginia Plan be taken up.(IV, 96)

Wilson wished to clarify that by the term "national" he did not intend that
they swallow up the state governments as Hamilton seemed to desire.
"National" and "federal" were not so different terms to him.  The general
government would necessarily need to be divided into districts for proper
governance, and the states would fulfill that role.

Hamilton supported the proposition but worried that he had been misunderstood
yesterday.  He meant only that no boundary of jurisdiction could be drawn
between the national government and the states, and as a result, indefinite
power must rest in the former.  If it is limited at all, then it will
eventually lead to the states usurping the authority of the general government
entirely.  As *states* he thought they ought to be abolished, but he concurred
with Wilson's idea that the states should serve as subdivisions of the general
government.

King believed that the terms "state", "sovereign", "national", and "federal"
had been misapplied in the recent debates.  The states were not sovereigns.
They could not declare war or negotiate peace.  They were dumb, for they could
not speak to foreign powers.  They were deaf, for they could not hear
propositions from such sovereigns.  They were impotent, for they could not
even raise their own armies or fit their own vessels for war.  If the United
States represents a confederation, it also represents a consolidation.  The
AoC is partly federal but also partly national or constitutional as it applies
to the individuals of states and not the states alone.  If the states had ever
had sovereignty, they had certainly divested themselves of large portions of
it.  He doubted the practicability of annihilating the states, but he thought
much of their power ought to be taken from them.(I, 323-4,331-2)

Luther Martin considered the separation from Great Britain to have placed the
states in a state of nature towards one another [Locke].  They had entered
into the confederation on equal footing, and they had met to amend it on the
same footing.  He could never support a plan that would introduce an
inequality and place ten states at the mercy of Massachusetts, Pennsylvania,
and Virginia.

Wilson did not admit that when the colonies became independent of Great
Britain that they also became independent of each other.  The Declaration of
Independence declared the states free *unitedly*, not *independently*.

Hamilton agreed with Wilson that the states had not been thrown into a state
of nature.  He could not agree (as had been implied by Madison) that the
confederation could be dissolved by partial infractions of it.  Even though
the states met equally here, that alone was no reason they couldn't change the
system.  There were two reasons he could see that the small states would have
nothing to fear from the large states.  First, the large states are separated
geographically and there's no reason that they necessarily share the same
interests.  Second, because there is a gradation from Virginia to Delaware,
there's no reason an ambitious combination of the larger states could not be
counteracted by the rest.  Within states, larger counties have not been seen
to dominate smaller counties.  [Really?  Massachusetts, perhaps?]  The more
authority that is given to the national government, the less the small states
will need to worry about abuse from the large states.

The committee adjourned.

It's worth mentioning here the debates between Lansing and Hamilton in the New
York ratification convention.  Hamilton claimed that the state governments
were necessary to secure the liberty of the people.  Lansing pointed out that
Hamilton himself had said on the floor of Independence Hall that the authority
of the state governments ought to be subverted.  Lansing ended his speech with
"But the honorable gentleman's reflections have probably induced him to
correct that sentiment."  The official record for the day states, "Mr.
Hamilton here interrupted Mr. Lansing, and contradicted, in the most positive
terms, the charge of inconsistency included in the preceding observations.
This produced a warm personal altercation between these gentlemen, which
engrossed the remainder of the day."(III, 338)

Nathan Dane writes to Rufus King this day that he thinks that the people ought
to never see anything about the convention but the final report, that they
should never know whose interests succeeded or failed at the convention.  He
warns King that Pierce (GA) is talking publicly about the proceedings.(III,
48-9)

William Davie (NC) writes at the end of the day, "We move slowly in our
business; it is indeed a work of great delicacy and difficulty, impeded at
every step by jealousies and jarring interests."(IV, 97)

# June 20, Wednesday

| State | New Delegates |
|-------|---------------|
| North Carolina | William Blount |

Ellsworth moved, seconded by Gorham, moved to alter the first resolution to
replace "national government" with "the Government of the United States",
removing the reference to *national* which had so upended the debates so far.
He could not imagine that any breach of the current confederation meant that
it was dissolved.  He considered it as still subsisting and as such wished the
resolutions to go forward as *amendments* to the AoC.  He preferred that the
ratification go through the legislatures instead of the people.

Randolph did not object to the alteration but suspected that Ellsworth wasn't
being completely forward about his reasons for changing it.

The motion was agreed to unanimously.

Madison explained many years later that the term "national" was not meant to
express the extend of the new government's power but rather its "mode of
operation" -- an operation over individuals, like other nations, instead of
over states.(I, 474-5)  It would also appear as a *national* government in its
dealings with foreign powers, being able to, e.g., regulate foreign commerce
without the states' approval.(I, 529)

The committee took up the second resolution -- removing the reference to
"national" -- "Resolved that the Legislature ought to consist of two
branches."

Lansing moved instead that the second resolution should say "that the powers
of legislation be vested in Congress."(I, 336)  He had already stated that he
was against the innovation of two branches because the convention was not
given the power to change so much and because he distrusted the public mind.
He addressed a number of criticisms from the members yesterday:
1. Madison had pointed out that two states had the people appoint
   congressional delegates, but Lansing countered that those delegates
   represented not the people of their state but represented a sovereign
   state.
2. Randolph had held that the public safety necessitated a change in the form
   of government, but Lansing felt that they should also consider the public
   danger of such a change.  "We must be certain that it will secure important
   and equal benefits to all.  If destitute of these convictions we should be
   traitors to our country."(IV, 98)
3. Wilson had voiced his feeling that the convention was at liberty to suggest
   whatever changes they liked, but Lansing believed that they ought to make
   recommendations which had a chance of being accepted.  The people have not
   voiced any opinion whatsoever on whether they would prefer a national
   government, and no one state will part with its sovereignty, much less
   thirteen.
4. Hamilton had stated that the larger states did not share the same interests
   with one another, thus, the smaller states should not fear they would unite
   against the smaller states; however, Lansing pointed out that if this were
   true, then it wouldn't matter if suffrage remained apportioned equally or
   changed as now proposed.

Lansing worried about the veto on state laws.  This was equivalent to
abolishing the states.  The means of sending acts to the national legislature
to be approved before passing them into law would be slow and unwieldy for so
many states -- many hundreds of miles from the state capital.  The legislature
would be forced to pass judgment on a law a day.  How could we trust
legislators from different regions to be competent judges of foreign laws?
Would a Georgian senator know what was best for the people of New Hampshire?

Lansing's final objection was that the proposed system was too novel and
complex.  "Fond as many of you are of a general government, do any of you
believe it can pervade the whole continent so effectually as to secure the
peace, harmony, and happiness of the whole?"(I, 345)  There was no telling
what its long term effects would be.  He could only assume that it end in the
general government swallowing the states or vice versa.  "You are sowing the
seeds of rivalship, which at last must end in ruin."(I, 345)

Mason expressed surprise that these points were still being brought up.  The
principle objections to the current resolution were a want of power and a want
of practicability.  Does the first point apply to a mere *recommendation* from
the convention?  He agreed with Randolph that there were times in which the
power must be seized for the good of all.  He gave as an example, the US
commissioners who had brokered the peace treaty with Great Britain against
some of the recommendations of Congress.  The second point was even more
groundless.  Congress has tried to enlarge their powers and failed.  From this
Lansing deduces that the present proposal must also fail, but the opposite
lesson should be drawn:  A mere augment of Congressional power will never
succeed.  Lansing says the public mind is unsettled, but they are well settled
in at least two respects:  (1) in republican government and (2) in two
branches of the legislature.  They are so committed to the latter that nearly
every state legislature takes this form.  It is so ubiquitous that if it
weren't so aligned with the public will, it would have to be the result of a
miracle.  The only exceptions are the Pennsylvania legislature and Congress,
and only the latter is not chosen by the people themselves.  What has been the
consequence?  The people have time and again refused to give that body more
power.

The New Jersey Plan admitted that the only way to enforce the will of Congress
was through military coercion.  The thought is horrifying.  Civil liberty and
military execution are incompatible.  At least a national government could
operate on individuals instead of against the innocent as well as the guilty.
However, he did wish to preserve the state governments, seeing them as
necessary as a general government.  He recognized the difficulty in drawing
the legal boundary of their jurisdiction but hoped that leaving a means of
amending the proposal to posterity would suffice to correct any defects.

Luther Martin agreed with Mason regarding the necessity of the state
governments.  In fact, he would support them at the expense of the general
government -- "for I consider the present system as a system of slavery."(I,
347)  Martin corrected Mason that it was the state *legislatures*, not the
people, who refused to grant more powers to Congress.  When declaring
independence from Britain the people preferred the establishment of thirteen
independent states over a consolidated nation.  They look to their state for
the protection of their liberties, and they look to the federal government to
protect the states from foreign influence and to protect the smaller states
from the ambition of the larger.  He was against state conventions for
ratification, against assisting states against rebellious subjects, thought
the Paterson resolutions no more coercive than the nation plan, and against
the extension of a national judiciary into the states as ineffectual.

Before the Maryland ratification convention, Martin would say, "And it was
shown, that even Adams, who, the reviewers have justly observed, appears to be
as fond of *checks* and *balances* as Lord Chesterfield of the *Graces*, even
*he* declares that a council consisting of *one* branch has always been found
*sufficient* in *federal* government."(III, 192)

Sherman seconded Lansing's motion.  While two branches are necessary in the
states, they are not necessary for a federal government.  The current federal
government was sufficient to wage a victorious war and did it perhaps as well
as any government could.  The complaint is not that Congress is unwise or
unfaithful but that their power is insufficient.  The powers of Congress can
be so modified that the federal and state governments will be mutual
supporters of one another.  In defense of states not paying the requisitions,
each state was worried about raising too much revenue from taxes which would
be a burden on their people.  Almost all the states agreed that the general
government ought to derive revenue from trade.  There is no grounds to believe
that the people distrust Congress.  If another branch were added to Congress
to represent the people, it would be an embarrassment.  The people would
poorly turn out to vote, and a few men in large districts would carry the
vote.  They would have no reason to put more trust in this branch than they
already do in Congress.  The disparity in state sizes was the main problem.
The larger states had not yet suffered from equal representation with the
smaller states.  [I think the larger states would beg to differ.]  To
consolidate the states as some had supposed would dissolve our current
treaties with foreign nations.  [No, it would not.]  He ended by stating that
he would be willing to support two branches of the legislature as long as the
upper house would maintain equal suffrage among the states.

Wilson said the considerations before could be broken down into three
considerations(I, 348,350):

1. whether one or two houses in the legislature
2. whether they are elected by the state legislatures or the people
3. whether equal suffrage or based on proportional representation

He stated that two houses of the legislature were necessary.  Confederations
have historically not lasted long, and they often held together only because
of outside pressure.  In Congress, many necessary changes had been prevented
by only a few small members of the union.  The current form of the general
government had been forged by necessity, not by choice.  "Like the wisdom of
Solomon in adjudging the child to its true mother, from a tenderness to it,
the greater states well knew the loss of a limb was fatal to the confederation
-- they too, through tenderness sacrificed their dearest rights to preserve
the whole.  But the time is come, when justice will be done to their claims.
Situations are altered."(I, 348)  Private citizens would be indifferent
between their representatives being state or national as long as they exercise
the law for their happiness.  Representatives have an interest in the law
being exercised to the body to which they belong.  If the congressional powers
are expanded, then the state legislatures will always look to the national
legislature with jealously.

Lansing's resolution was voted down.

The committee adjourned.

Franklin did not rise to speak this day but would go into retirement thinking
that the two branches in the legislature were unnecessary.(III, 297)

[There is an unattributable draft speech that was never delivered this day
IV,100-5.  Hutson suspects it is Jared Ingersoll.  Since, the author is
suspect, it is not worth including in the debates above but worth pointing out
its existence.  "The people of the United States, excited to arms by the
insidious designs of the then Mother Country have become admirers of liberty
warmly and passionately so.  They snuff tyranny in every tainted gale.  They
are jealous of their liberty.  They are pleased with their present
governments, they think them as energetic as they ought to be framed, *they
are continually planning subdivisions of the present governments*, they are
complaining of the expense of the present governments."(IV, 104-5, emphasis
mine)]

# June 21, Thursday

| State | New Delegates |
|-------|---------------|
| New Jersey | Jonathan Dayton |

Johnson characterized the support for the New Jersey Plan over that of
Virginia as being primarily about maintaining state sovereignty.  Although the
Virginia Plan did not attempt to abolish the states explicitly, some of its
supporters and adversaries had framed it in that language.  The worry from
many of the supporters of the New Jersey Plan was that even though the states
maintained some limited power under the Virginia Plan, it would not be enough
to defend from the overreach of the national government.  If they could be
persuaded that the states would be able to maintain some of their sovereignty
under the Virginia Plan without an equal vote in the general government, then
many of the objections to the plan would be removed.

Wilson spoke of his respect for Johnson and attempted to answer his challenge.
He thought the difficulty of the plan was not 'how will the states defend
against the national government' but rather 'how will the national government
defend against the encroachment of the states.'  The state legislatures will
be given the power to chose their senators for one house of the legislature.
This gives the states the opportunity to defend their rights.  Ought not a
reciprocal opportunity be maintained in the national legislature to defend it
from the states?  Frankly, he could see no danger in the national government
encroaching on the jurisdiction of the states.  Any attempt to do so would
raise an alarm in the national legislature as well as the state legislatures.
The people who rely on the state governments to secure their rights would
never allow the national government to take from the states what the people
wished them to retain.  He was worried that in spite of ever precaution, the
national government would be constantly encroached upon by the states.

Madison believed the national government was in more danger from the states
than vice versa.  History and our present condition had demonstrated this.
However, some will say that the new proposal which places more power in the
general government will change this tendency.  Let's say hypothetically that
an indefinite power is given to the general government, and the states are
reduced to mere dependent corporations.  Since the power of the general
government is derived from the people, why would one suppose that the general
government would attempt to take any power from the states that the people
found beneficial and desirable?  Take the townships of Connecticut for
example:  Has there been any attempt by the state legislature to acquire local
power that the people find convenient?  The general government is similar.

Secondly, the outcome if the general encroached on the states would be less
fatal than the reverse.  The objection to encroachment on the state
governments is that the general government could never rule the local
jurisdictions effectively.  The objection is not an abuse of power but the
imperfect use of such power over so vast a continent.  However, were it
practicable to extend the national powers to local jurisdictions, the people
would not be less free as members of one great republic than they would be as
members of thirteen independent states.  If, on the other hand, the states
were to usurp the power of the national government, we need not describe the
result here; we are already experiencing such a calamity.

\[Interestingly, in 1819 after the
[McColloch v.  Maryland](https://en.wikipedia.org/wiki/McCulloch_v._Maryland)
decision, Madison expresses surprise at the ruling of the court.  He believes
that the decision grants unlimited, unconstitutional powers to the national
legislature at the expense of the state legislatures:

> But it was anticipated, I believe, by few if any of the friends of the
> Constitution, that a rule of construction would be introduced, as broad and
> as pliant as what has occurred. Those who recollect, and still more, those
> who shared in what passed in the State Conventions, through which the people
> ratified the Constitution, with respect to the extent of the powers vested
> in Congress, cannot easily be persuaded that the avowal of such a rule would
> not have prevented its ratification.(III, 435)

See [the full letter](https://founders.archives.gov/documents/Madison/04-01-02-0455).\]

The vote on whether the legislature ought to consist of two branches passed in
the affirmative.

Luther Martin voted against the motion.  In the Maryland ratification
convention, he stated that, were the general government to abolish the states,
he did not worry only about the "imperfect use of such power" (as Madison had
put it).  He worried that the imperfect use of such power would result in the
republic once again splitting into separate states; although, this time one
could not guarantee that the collapse which would cause such a split would
result in state governments built upon the same republican principles they
currently enjoyed.(III, 196-7)

The committee then took the third resolution into consideration.

C. C. Pinckney moved that the first branch of the legislature, instead of
being elected by the people, ought to be elected in a manner directed by the
legislature of each state.  The change would bring more satisfaction as the
states could accommodate the method to the convenience and opinion of the
people.  It would avoid the influence of large counties which would carry
based on the present method of election.  If an election ends up disputed,
then it will have to be ruled on by the general legislature at a great expense
of money and time for distant parts of the union.

Madison opposed the motion.  He admitted that there were difficulties, but
they could be ironed out as the committee developed the proposal in more
detail.(I, 364)

Martin Luther seconds the motion.(I, 358)  Martin believed that although the
people should have the right to elect their state legislatures, the general
government ought to be federal, and thus, only the state legislatures had the
right to elect federal representatives.(III, 284)

Hamilton felt that state power over the general government ought to be guarded
against, and therefore, the first house needs to be elected by the people
directly.  Besides there is a chance that the state legislatures will one day
disappear.  We should not enshrine into law a system that one day may fail.

Mason asserted that at least one house must be chosen by the people.  We have
seen the evils of pure democracy, but if we mean to preserve the rights of the
people under a national government, it is a necessary component.  If you
change this component, the states will oppose national taxation powers.(I,
364)

Sherman preferred an election by the legislatures but was also fine with the
plan as it stood.

Rutledge felt that there was no great difference, with respect to their
rights, between the people directly or indirectly (through the legislatures)
electing their representatives.  The state legislatures will probably produce
fitter characters than the people.  Had the people been given the option to
elect delegates to Congress or this convention, they would not have chosen so
well.

Wilson countered that the difference between direct and indirect election was
immense.  The state legislatures will *no doubt* have interests opposed to the
general government and *may* have interests opposed to the people as well.

King agreed with Wilson.  The legislative representatives would likely be
subservient to the state governments and may take up policies that run counter
to the general good.

C. C. Pinckney reiterated that the state governments ought to have a share in
the national government.  If they do not, then smaller states like South
Carolina would have a small share in the benefits of the general government.

Pinckney's motion was voted down.

C. C. Pinckney moved that the first branch be elected by the people by a
method to be directed by the legislatures but then waved the last part about
the legislatures supposing that it would be better to try to pass that
provision in the detailed plan.

On election by the people, the resolution passed in the affirmative.

The committee then took up the term of the house of representatives to be
three years.

Randolph proposed changing the term to two years.  He knew the people of the
states preferred annual elections in order to hold their representatives
accountable, but the distances to travel were so far that annual elections
would be too much trouble for the distant parts of the union.

Dickinson thought two years would still be too inconvenient and preferred
three in order to prevent a change of too many representatives at the same
time.  A three year term where a third are up for reelection every year would
be best.

Ellsworth moved for one year terms only because the people seem so fond of
them.  Strong seconded and supported him.

Wilson preferred annual elections for the same reasons as others had stated.
He did not think it would be inconvenient since they would perhaps not sit for
even one-half or one-fourth of the year.

Madison held that even two years would be inconveniently short.  Besides the
long distance that the delegates would have to travel from distant regions of
the union, no time would be given to new members to acquire enough knowledge
of the general affairs of the union to do their duty well.

Sherman preferred annual or biennial terms.  It would be good for
representatives to be forced to return to their states periodically so that
their interests don't begin to differ from those of their constituents.

Mason preferred two years over one.  If the elections were too frequent, then
the middle states would have an advantage in the national legislature since
the distant states would risk being longer unrepresented.

Hamilton urged three year terms.  There could not be too much or too little
dependence on the popular sentiments.  The representatives in the House of
Commons serve for seven year terms, and this had not blunted the democratic
edge.  If elections occur too often, then people will care less about them.

The committee voted to replace the term with "two years".

The committee adjourned.

# June 22, Friday

[Lansing absent from the convention this day.(IV, 108)]

The committee opened business by taking up consideration of part of the third
resolution, namely "to receive fixed stipends to be paid out of the public
treasury."

Ellsworth moved that the representatives ought to be paid from their state
treasuries since one's style of living could vary greatly between the states.
What might be reasonable compensation in one state could be unpopular in
another.

Williamson agreed.  New states entering the union from the western territories
would, being poor, pay little into the national treasury.  Their
representatives would have differences from those of the original states, and
it would not be right for the old states to "pay the expenses of men who would
be employed in thwarting their measures and interests."(I, 372)

Gorham disagreed.  The states were always paring down the pay of their
representatives.  This would discourage the ablest men from seeking the office
and executing its functions.  The method of payment should not be
constitutional.  The national legislature should provide for their own wages
from time to time as was done in the states.  There had been no abuse of this
means in the state legislatures.

Randolph felt Ellsworth's motion would create unnecessary dependency on the
states.  Hence, why the representatives should be paid from the national
treasury.

King agreed with Randolph, but contra Gorham, he felt the method of payment
should be defined in the constitution.  Otherwise, leaving the question up to
the legislature would bring opposition to the whole document.

Sherman believed the amount and means of payment should be left to the state
legislatures.

Wilson was against fixing the amount since it would require changes from time
to time.  The national representatives should be as independent of the states
as possible.

Madison supported paying out of the national treasury *and* fixing the
payment.  Although, he wondered if it might be done by fixing it to some
standard that would not vary with circumstances.  In the Virginia ratifying
convention he stated explicitly that he was worried about an inflation or
deflation of currency if the salary was fixed to a specific amount.(III,
313-4)  He called out Williamson for not treating the people to the west as
brethren and equals.  It was for the good of all that they should attract the
most capable individuals to those positions.

Hamilton thought that fixing the wages would be inconvenient and that they
should be paid from the national treasury in order to be independent from the
states.  Payment would necessarily be unequal between the states because
travel from the furthest states would require more money.

Wilson moved that the salaries of the first branch be ascertained by the
national legislature and paid out of the national treasury.

Madison didn't think the legislature should ascertain their own compensation.
"It would be indecent to put their hands into the public purse for the sake of
their own pockets."(I, 374)

Wilson's motion was voted down.

[Madison seems confused about the order of motions this day, so the rest is
taken from Yates.]

Mason moves that the representatives "receive an adequate compensation for
their services and to be paid out of the national treasury."  The motion
passes.

Rutledge wished to take up the consideration of "to be paid out of the
national treasury."

Hamilton reiterated his previous point.  The state governments will be the
rivals of the general government and thus, cannot be trusted with compensation
for national representatives.

Ellsworth points out that the ratification of the constitution will need the
support of the states.  If he returns to his state and tells them that we
could not trust the states with these sorts of powers, they will never adopt
this government.

Wilson does not support carrying the proposal to the state legislatures.  It
should be voted on by the people.

Rutledge's motion was voted against.  [Did he actually propose anything?]

[Everything below (unless otherwise stated) is back to Madison.]

The phrase "adequate compensation" was voted on to replace "fixed stipend" in
the affirmative.

Butler moved that the question be taken on both points jointly, namely
"adequate compensation to be paid out of the national treasury."  Initially,
it was ruled out of order, but the convention voted that it was not.  "The
question was then postponed by South Carolina in right of the state."(I, 375)
[Whatever that means.]

Mason moved to insert an age requirement of 25 years to the resolution.  "It
had been said that Congress had proved a good school for our young men.  It
might be so for anything he knew but if it were, he chose that they should
bear the expense of their own education."(I, 375)

Wilson was against abridging the rights of election in any shape.  Who should
prevent a person for voting for someone they think has the requisite genius or
ambition for the office?  We might as well set an upper age limit as well.

Mason's motion passed in the affirmative.

Gorham moved to strike out the section under the third resolution about
ineligibility for office during one's term.  He thought this would be
"unnecessary and injurious."

Butler asserted that the protection was necessary.  In Great Britain men got
into parliament only to get offices for themselves or their friends.

King thought they were carrying the system of exclusion to utopian lengths.
Such a restriction would discourage merit.  The executive might also use it as
an excuse to make bad appointments.

Wilson against discouraging merit.  It might prove fatal in war time by
rendering our best commanders ineligible for service.

Madison stated that the exclusion was necessary otherwise there would be a
temptation to create new offices or increase the stipends for an office in
order to gratify members.  Madison preferred that *at least* a member be
excluded from holding a position which was created or augmented while he was
in office.(I, 380)

Mason was for preventing any possible abuse or corruption.  Such a
disqualification is a corner stone upon which our liberties depend.

Gorham stated that one cannot apply the corruption of British government to
America.  Even the corruption there has a purpose as it gives their government
stability.  We do not know the effect it would have had if members of
parliament were excluded from offices.(I, 381)  [The implication being (and
Hamilton says the same below) that the motivation of honorable positions may
have good effects in addition to their bad ones.]

Hamilton could see inconvenience on both sides.  "We must take a man as we
find him, and if we expect him to serve the public, [we] must interest his
passions in doing so.  A reliance on pure patriotism had been the source of
many of our errors."(I, 376)  "One great error is that we suppose mankind more
honest than they are."(I, 381)  He agreed with Gorham that it would impossible
to say what the result of such an exclusion would have been in Britain.  He
supported only the following clause:  that when a representative takes his
seat, he should vacate every other office.(I, 382)  [There is a lot of
Montesquieu-esque language in this speech as recorded by Yates that is
reminiscent of his June 18 speech.]

Gorham's motion to strike out ineligibility for office was voted down.

The committee adjourned.

# June 23, Saturday

The committee resumed their debate over the third resolution.

On a motion from Gorham (I, 391), the committee took up a vote for the
postponed clause yesterday (by South Carolina), namely "adequate compensation
to be paid out of the national treasury."  The clause was voted down.

C. C. Pinckney moves to strike out the ineligibility of representatives for
offices established by a particular state.  This could place an inconvenience
on the representative and the state for a restriction of little benefit.

Wilson stated that if you strike out that clause, the delegate may prefer
their attachment to the state which would be injurious to the general
government.(IV, 109)

Sherman seconds the motion:  "It would seem that we are erecting a kingdom at
war with itself."(I, 386)

Gorham believed the clause was necessary to give the general government energy
-- "prevailing opinions are too democratic."(IV, 109)

The motion was passed in the affirmative, and the words were struck out.

Madison moved to make the representatives "ineligible during their time of
service and for one year after -- to such offices only as should be
established, or the emoluments thereof, augmented by the legislature of the
United States during the time of their being members."(I, 386)  The
unnecessary creation of offices and increase of salaries were the real
problems they should avoid.

Alexander Martin seconded the motion.

Butler thought the amendment did not go far enough, and it would easily be
evaded.  Quotes Montesquieu on it being unwise to entrust a person with power
who abuse of said power works to their advantage.(I, 391)  [May be a reference
to Book 5 §19 ???]

Rutledge, in an effort to keep the legislature as pure as possible, thought it
better to prevent the appointment of representatives to all offices.(I, 386)
Honesty ought to prevail in the lower house; ability in the upper.(IV, 109)

Mason agreed with the others before him that it was only a partial remedy for
the evil.  Both the British parliament and the Virginia legislature suffer
from the abuses and corruption of such appointments.  Surely there are a
sufficient number of citizens who would serve the people without inducement to
offices.  It was said that genius and virtue ought to be encouraged.  Genius
may be, but virtue cannot be encouraged by such venality.

Madison's wish was that the legislative body be as uncorrupted as possible.
Any public body will support its members, but this is not always done because
of corruption or venality -- friendship and a knowledge of the abilities of
one's peers also produce it.  You cannot always rely on the patriotism of
members.  If you do, you will limit the available nominees.  We must allure
people.  The legislature should be the road to public honor.(I, 392)

King believed that this was a not a problem they would be capable of fixing.
You say you want to prevent a man from appointing himself to a position.  Do
we also prevent him from appointing a son, a brother, or a friend?

Wilson supported Madison's amendment feeling it sufficiently checked the
corrupting influences they wished to fix.  He criticized Mason's use of the
word "venality" to refer to the laudable ambition of rising to honorable
offices in government.

Sherman thought the amendment did not go far enough and pointed out how it
could be evaded with the following example:  "A new embassy might be
established to a new court and an ambassador taken from another, in order to
*create* a vacancy for a favorite member."(I, 388)  He hoped public service
would be enough of an inducement itself without resorting to desirable
offices.

Gerry agreed with Sherman's point.  He responded to King by pointing out that,
while it was true someone might make appointments based on nepotism, he
assumed a person would go to greater lengths to appoint themselves, and the
clause at least prevents that.

Madison admitted that there were good reasons for and against allowing
legislative appointments.  The object is to fill the offices with the fittest
characters.  He worried -- as has happened in the state governments -- about
representatives not being familiar with such men and appointing to offices
based on misleading information or second-hand authority.  This would be worse
in the national legislature.  Therefore, it's best that the legislators be
allowed to appoint from their peers.  The legislature of Virginia would be
without many of its best and most capable members if they had been ineligible
for Congress or other governmental offices.

Butler believed that if a character was fit for an office, he would never be
unknown.

Mason believed that even if the legislators are disqualified, honorable
offices would not have any difficulty attracting qualified individuals.

Jenifer pointed out that in Maryland senators could hold no other office
during their term, and that regulation had earned the confidence of the
people.

Madison's motion was voted down.

Sherman moved to add "and incapable of holding" after "eligible to offices".
Agreed to without debate.

The phrases "established" and "national government" were removed from the
third resolution.

Spaight moved to divide the ineligibility clause into "ineligible during their
term" and "ineligible for one year after".  The former phrase passed in the
affirmative.

Mason thought a year off was essential, else appointees might resign to
fulfill the post or be guaranteed a spot at the close of their term.(I, 394)

Gerry pointed out how this once occurred in Congress.(I, 394)

Hamilton said the clause could be evaded many different ways.(I, 394)

Rutledge admitted as much but thought that was no argument against doing
anything they could to prevent as much corruption as they could.(I, 394)

The second clause was voted against.

The committee adjourned.

# June 24, Sunday

[See Rufus King's letter on this date in volume IV, especially footnote.]

Multiple delegates began to experience severe financial strain throughout the
convention as is recorded in their letters.  A British agent operating in New
York informed London that one of the delegates to the convention was indebted
to him and was willing to send him samples of the constitutional debates.
There are no records that London approved the project or that any such
information was transmitted to London.  The delegate has never been
identified.(IV, 111)

# June 25, Monday

The fourth resolution, regarding the upper house of the legislature, was taken
up.

Pinckney opened with a long speech about the particular character of the
United States and how that ought to inform the construction of the
legislature.  Compared to other countries, particularly those in Europe, the
people of the United States are relatively equal in standing.  Almost all
freemen have the right to vote for their representation, and only a modest
wealth could allow any person to run for office.

As good as the constitution of Great Britain is [and Pinckney thinks it is the
best formed], it cannot be an example for us.  We have no nobility, and our
proposed executive is not as fixed and dangerous as the one in Britain.  Thus
there is no need to have an upper house as a check between the executive and
the Commons.  Our situation will not change for the foreseeable future because
of our relative equality and the vast western territory that will prevent the
increase of the poor and discontented.

The situation of our country is this -- a government capable of extending
civil and religious liberty to the people and making them happy at home.  This
is the end of republican establishments.  We mistake our purpose if our aim is
to make ourselves respectable abroad.

He distinguishes three classes in American society:
1. Professional men
2. Commercial men
3. Landed interests

These three classes all depend upon one another.  The government we propose
must suit the culture and society of our people.  The British government is
not suited to us.

Because of the size of the United States, the state governments are necessary
to ensure the protection and rights of the people.  He proposes a change to
the resolution if no better arguments are made.(I, 412)  "States ought to be
divided into five classes -- to have from one to five votes."(IV, 112)

Randolph moved that the fourth resolution be divided and the parts taken into
individual consideration as they had done with the third resolution.(I, 112)

The word "national" was replaced with "United States".

The method of constituting the upper house was taken up.

Gorham thought there was some validity to the argument that Virginia would
have 16 times as many senators as Delaware.  The senators from Virginia would
be more likely to band together than 16 other representatives from different
states.  The larger counties of Massachusetts did not have a representation of
delegates in the exact ratio of their inhabitants.  The same could be done for
the states -- perhaps as Pinckney suggested.

Read stated that justice needed to be done to the old relationships between
the states before a new one could be created.  The western lands were won by
the United States as a whole from Britain, but these lands have been
appropriated by the larger states.  The western lands should be sold to pay
for the debt of all the states.  Only after that can we consider a
proportional representation.

Gorham did not know what they could do about that in this convention.
Congress was presently trying to rectify the injustices between the states.
The best that they could do would be to ensure the formation of a government
that would be able to do justice to the smaller states.

Wilson stated that the people will be citizens of both the general government
and their respective states.  The general government is a government of the
people, not the states.  In so far as they are represented in the general
government, we ought to pretend the state governments do not exist.  If the
legislatures elect the senators, then they will only represent local
interests.  He moved that the proportional representation be the same between
the two houses of the legislature and "that the second branch of the
legislature of the national government be elected by electors chosen by the
people of the United States"(I, 414).  No one seconded the motion.

Ellsworth favored the wording of the resolution as it presently existed.
Whether the senator is elected by the people or the legislature, he will still
be a citizen of the state he represents.  The local interests of the states
will find their way into the general government regardless.  Wisdom and
firmness is necessary in the senate, and this is more likely to come from the
legislature than the people.  The United States are so vast that the only hope
they have of supporting the general government is to entrust part of that
governance to the state governments.

Johnson urged the preservation of the state governments who would otherwise be
at the mercy of the general government.

Madison moved to postpone the current debate on the fourth resolution to take
up the eighth, regarding the right of suffrage in the upper house.  He figured
that agreeing to the right of suffrage would avoid some of the differences on
this resolution.  [Yates attributes this to Madison, but Madison attributes
this to Butler *seconded* by Madison.]

Williamson felt that the happiness of the people depended on securing the
existence of the state governments.  Before voting on the issue of the senate,
he preferred to know the number of senators that would be in the senate.  He
moved to insert "who shall bear such proportion to the number of the first
branch as to ______"(I, 407)  No one seconded his motion.

Mason pointed out that they all agreed that a necessary power of government is
for its own self-defense.  The checks and balances they proposed between the
branches were for their respective self-defense.  Doesn't it make sense to
provide a means of self-defense to the state governments?  The only remaining
way of doing this is to allow them to appoint the senators.

The committee voted against postponing the fourth resolution for a discussion
of either the seventh or eighth resolutions.

The committee voted in the affirmative to have the senate elected by the state
legislatures.

The committee unanimously voted to set the age limit to 30 years.

The committee struck out the words "sufficient to ensure their independency".

The committee took up the consideration of term of seven years.

Gorham suggests four years with one-fourth being up for reelection every year.

Randolph favored a rotation and moved to insert after "seven years", "to go
out in fixed proportion."(I, 408)

Williamson suggests six year terms.  Seconded by Sherman.

Read suggests they hold their offices during good behavior.  [Madison says
that Robert Morris seconds this, but Yates says it was not seconded.  Since
the official Journal shows not record of a vote on this motion, I am inclined
to side with Yates.]

C. C. Pinckney suggests four years.  Any longer and we would risk them losing
sight of the interests of their states.  He would come to support the
interests of the state in which he lived and served more than the state he
represented.  The distant states would labor under the most disadvantages.

Sherman moved to strike out the word "seven" so that the committee might take
a vote on each of the proposals in turn.  This was agreed to.

On the question to fill in the blank with "six", the vote failed.  The vote to
fill it with "five" also failed.

The committee adjourned.

# June 26, Tuesday

The committee took up the duration of the term of senators.

Gorham, seconded by Wilson, moved to fill the blank with "six years, one-third
to go out biennially".

C. C. Pinckney opposed six years in favor of four.  He made the same argument
he made yesterday.

Read, seconded by Broom, moved to fill the blank with "nine years, one-third
to go out triennially".(I, 418)

Madison asserted that if they wanted to judge rightly on this resolution, they
would do well to be mindful of the purpose of this house of the legislature:
first to protect the people from their rulers, second to protect the people
from their own "transient impressions".  The first purpose is secured by
dividing the legislature into two separate houses where different bodies of
men can act as checks on each other.  The latter purpose would require a body
of men of a longer term than the first in order to acquire a competent
knowledge of the public interests.

They also needed to protect the minority interests from an unjust majority.
There existed class distinctions -- rich and poor -- in all civilized nations
with distinct interests.  Pinckney was correct yesterday when he argued that
there is less distinction between these classes in America, but nevertheless,
they still exist.  "The man who is possessed of wealth, who lolls on his sofa
or rolls in his carriage, cannot judge of the wants or feelings of the day
laborer."(I, 431)  In time the poverty-stricken might outnumber "those who are
placed above the feeling of indigence."(I, 422)  While we have not seen an
agrarian revolution yet, we have seen enough attempts at wealth-redistribution
that we can imagine the future dangers.  The upper house with a sufficient
wisdom and virtue must defend against these populist dangers.

Given the danger, the senate should serve for a considerable duration.  Nine
years is perhaps sufficient.  The term of the senate should be long enough
that they would not need to seek a second term.  The current plan may decide
the future fate of republican government.  We ought to provide every guard to
its liberty.

Sherman pointed out that government should be instituted so that it does not
endanger the liberties of those it governs.  By giving too much permanency to
a term, you risk preserving bad government.  Frequent elections are necessary
to secure the good behavior of the rulers.  Stability is achieved by those
rulers who are good enough to secure reelection.  He supported either six or
four year terms.

Read believed it was in the interests of the small states that state
attachments should be extinguished as much as possible, that the senate should
be so constituted to have the feelings of the citizens of the whole.

Hamilton agreed with Madison that this could very well be the last republican
experiment, and if they did not give it the necessary wisdom and stability, it
would be disgraced and lost to mankind.  He admitted that he did not look
favorably on republican government, but he directed his points to those who
did.  He was a zealous advocate for liberty.  There existed an unequal
distribution of property, and this inequality was a result of liberty itself.
He reminded Sherman that the first house of the legislature was formed to be a
guardian of the interests of the poorer citizens.  In our present condition,
the governments have given way entirely to the people.  He asked Sherman
whether any state would dare impose and collect a direct tax on the people.
It is for those causes -- and not the frequency of elections -- that the
effect ought to be ascribed.

Gerry was certain that all the men present had the same interests, but they
differed every way in the means to achieve them.  There was not one in a
thousand men who would support anything approaching monarchy.  The people may
not approve the plan if the terms are too long.  If the plan is rejected,
there may be violent opposition or influence by foreign powers.  Madison's
argument against majority-tyranny and wealth-redistribution are correct, but
the settlement of the western territories would prolong any troubles.  He
admitted that evils could arise from too frequent elections.  He suggested
four or five year terms only because the people would not approve of anything
longer.

Wilson said that nations have two relations:  first to their citizens, then to
other foreign nations.  The senate ought to support the latter.  The
difficulty the US has had in securing treaties is because foreign nations do
not perceive a stability in our government.  He thought nine year terms with a
rotation of one-third would address this problem.  The primary
counter-argument he could see is that the men holding this term might
eventually make themselves hereditary.  He thought the rotation was necessary
so that at no point would every senator wish to extend his term.

Wilson's nine year term was voted against, and six years with one-third in
rotation every two years was passed in the affirmative.

The compensation of the senators was then considered.

C. C. Pinckney asserted that no salary ought to be given to the senate since
their members represented the wealthy class.  If no allowance was to be made,
then only the wealthy would pursue it.

Franklin seconded the motion.  He figured that many of the men at the
convention would be fit for the senate.  If the position were lucrative, then
they would be accused of having carved out places for themselves.

Pinckney's motion was voted down.

Williamson, seconded by Ellsworth, moved to change the wording to "to receive a
compensation for the devotion of their time to the public service."(I, 427)
The change of wording was meant to leave out the word "fixed" to leave more
room to modify the resolution.

Williamson's motion was agreed to.

Ellsworth moved to pay the senators out of the respective states rather than
the national treasury.  Since the senators represented the states, the house
should have the confidence of the states.

Madison contended that tying the senators so closely to the states would
undermine the purposes of the body.  The senate would become like Congress --
a body representing only the interests of the states -- rather than the
impartial guardians of the general good that it is meant to be.

Dayton preferred to pay them from the national treasury to keep them
sufficiently independent of the state governments.

Ellsworth's motion was voted against.

Mason did not make a formal motion but wondered aloud:  If the senate was
intended to, among other things, protect the rights of property, perhaps there
should be qualification of property on the office.

A question was taken on whether the words "paid out of the public treasury
should stand".  The vote failed.

Butler, seconded by Williamson, moved to strike out the ineligibility of
senators to state offices.

Wilson worried about the additional dependence that this would create on the
senators to the states.

C. C. Pinckney believed the state governments should be made, as much as
conveniently possible, a part of the general government.  To that end, the
state should pay its representative and reserve the right to recall him home
to serve a local office.

Williamson moved to split the ineligibility clause in two:
1. to be ineligible to hold offices under the United States
2. to be ineligible to hold offices under the particular state

Gerry and Madison moved to add "and for one year thereafter" which was agreed
to.

The first part of the clause was agreed to unanimously, but the second voted
down.

The fifth resolution was then passed unanimously in the affirmative.

The committee adjourned.

Ellsworth writes a letter to his wife about how tired he is growing of the city
of Philadelphia and the people in it.  "I mix with company without enjoying it
and am perfectly tired of flattery and forms."(IV, 121)

Lansing writes, "The business of the convention is going on very slowly and it
is still in such a stage as to render the result very dubious."(IV, 121)

# June 27, Wednesday

[Here begins the great debates leading to the Great Compromise of July 16.
Farrand suggests reading Appendix A CLVIII [17-24] for a summary of the
debates on proportional representation.]

Rutledge moved to postpone the sixth resolution to take up the seventh and
eighth which established the suffrage of the two houses of the legislature.

On the subject of proportional representation, Luther Martin spoke for three
hours until, Madison reports, he said he was too exhausted to continue.(I,
438)  Yates said of his speech, "As his arguments were too diffuse, and in
many instances desultory, it was not possible to trace him through the whole,
or to methodize his ideas into a systematic or argumentative arrangement."(I,
438-9)

Martin essentially repeated points that had already been made during previous
debates:
* The United States was meant to be a federal -- *not* a national --
  government
* If the states are made subordinate to the national, then eventually the
  national will take complete control
* "I have never heard of a confederacy having two legislatures.  Even the
  celebrated Mr. Adams, who talks so much of checks and balances, does not
  suppose it necessary in a confederacy"(I, 439)
* The people have no right to approve a new form of government without the
  consent of those to whom they have delegated power over state purposes
* The states have tried their best to comply with the acts of Congress --
  "weak, contemptibly weak as that body has been"(I, 437) -- but have been
  unable not due to their own fault but due to the heaviness of the private
  debts and waste of property during the war
* The examples previously mentioned by Madison (compacts between PA and NJ or
  MA raising troops) are not fatal violations of the AoC
* Justified his view that the states were sovereign and free with excerpts
  from Locke,
  [Emer de Vattel](http://oll.libertyfund.org/titles/vattel-the-law-of-nations-lf-ed),
  Lord Somers \[possibly John Somers ???\],
  and [Joseph Priestly](http://www.iep.utm.edu/priestly/#H3)
* Since states are equal with one another in a state of nature, they cannot
  give up their equality of suffrage without giving up their essential liberty
* Virginia, Massachusetts, and Pennsylvania would run the national government
  under a scheme of proportional representation; they would have enough votes
  to amend the laws to eventually take over the government
* Sixteen delegates from Virginia, e.g., would be more likely to act in
  concert than sixteen delegates collected from other states
* The smaller states, not the larger, gave up their essential rights
  * Counters Wilson on June 20:  "They gave up their claim to the
    unappropriated lands with the tenderness of the mother recorded by Solomon
    -- they sacrificed affection to the preservation of others."(I, 441)
* Equal representation in Congress is not the problem -- want of power is the
  problem

The committee adjourned.

William Samuel Johnson writes to his son:

> [M]uch information and eloquence has been displayed in the introductory
> speeches, and we have hitherto preserved great temperance, candor, and
> moderation in debate, and evinced much solicitude for the public weal.  Yet,
> as was to be expected, there is great diversity of sentiment, which renders
> it impossible to determine what will be the result of our
> deliberations.(III, 50)

Pierce, in a letter, likens the debates to the debate by the seven chiefs over
the ultimate form of government for the Persian empire.  He writes, "Nothing
can conquer the force of local habit.  Some are for one thing, and some are
for another, but I believe we shall ultimately agree on some sort of
government."(IV, 123)

# June 28, Thursday

Luther Martin resumes his discourse from yesterday:
* Under the Virginia Plan a minority (the large states) may tax the
  majority(I, 453)
* General government ought to be formed for the states instead of the people
* People are already represented by their state governments(I, 453)
* Proportional representation will enslave the smaller states whether the
  delegates are chosen by the legislatures or the people
* If the people, instead of the state legislatures, ratify the proposal, the
  interest of the governed will be separated from the governors, and anarchy
  and confusion will result(I, 453-4)
* The committee should not propose any plan offensive to the state rulers;
  they will likely have influence over the people and will prevent the plan
  from being adopted
* The large states should have nothing to fear from equal suffrage
* He would rather see a dissolution and partial confederacies than the current
  plan
* But the confederation cannot be dissolved except by the consent of all the
  contracting parties -- Is the old confederation dissolved because some of
  the states want a new confederation?

Madison says of the speech in his journal, "This was the substance of the
residue of his discourse which was delivered with much diffuseness and
considerable vehemence."(I, 445)

[Farrand recommends reading the controversy between Ellsworth and Martin in
"the Landholder", specifically Appendix A, CLXXXIX - CXCII, CXCIX for more
insight into Martin's arguments.]

Lansing moved to reword the seventh resolution so that it read:

> Resolved that the right of suffrage in the first branch of the national
> Legislature ought to be according to the rule established in the articles of
> confederation

Dayton seconded the motion but expressed great anxiety that the question might
not be voted on until tomorrow since Livingston was indisposed leaving New
Jersey unrepresented.

Williamson demonstrates that if the states are equally sovereign, and they
depart with equal portions of sovereignty, then they would remain equally
sovereign.  He could not see how the smaller states would suffer from this,
since the smaller states could throw their weight together to out-vote the
larger states.  He wished the delegates would keep in mind the introduction of
future states to the union from the western territories.  The new states would
be poor, less populous, and would not share the same economy as the old
states.  Therefore, they could join together with other small states to burden
the old states with unwise commercial and consumption policies.

Madison admitted that he would support any change to the resolutions that
would justly solve the difficulty of proportional representation.  However,
the present motion was neither just -- as Paterson and others in the
opposition had pointed out -- nor necessary for the protection of the small
states from the large.  The fallacy of the opposition lies in believing mere
treaties -- compacts between sovereign nations -- are the same as compacts by
which an authority is created which is paramount to the parties.  [NOTE:
Martin may have taken his arguments regarding treaties from *The Law of
Nations* by Vattel -- look into this.]  Aren't counties represented unequally
in the states because of different populations?  Because of different
importance?  Because the larger have more at stake than the smaller?  The same
will be true between the small and large states under a national legislature
elected by the people.

Why do the delegates of the smaller states fear some conspiracy by the larger?
Pennsylvania, Massachusetts, and Virginia are as different from one another in
economy, religion, and other circumstances as any other three states in the
union.  If the suspicion is only due to their size, experience suggests there
is no danger.  The journals of Congress do not show them voting alike on
issues.  There are no counties in any state which are similar by extent and
nothing else but that vote alike.  [Here he goes into many, *many* historical
examples for his point.]  The policies that the smaller states should promote
are those which reduce the power of the states to the condition of counties.

One last consideration:  If the result of this convention is a weak general
government, then the larger states, seeing their strength and size as
necessary to their security, will never agree to redraw their boundaries to
repartition the states equally.  A general government with sufficient energy
and permanency will remove the objection, and the smaller states may one day
see the equalization they wish for.

[Madison places the three preceding paragraphs into one single argument.
Yates and Paterson place them at different points throughout the debate.]

Wilson likens the argument for equal suffrage to the representation of the
Burroughs of England which have equal representation, and everyone agrees that
that is the most rotten part of the British constitution.

Sherman argues that the question is not what rights naturally belong to men,
but how they may be most equally and effectually guarded in society.  The rich
man enters society with more to lose than the poor man, but with his equal
vote he is equally safe.  If the rich man were to get a number of votes
proportional to his wealth over that of the poor man, the poor man would find
his station in society insecure.  This consideration was the motivation for
equal suffrage when the AoC was formed.

The vote on the motion was put off until tomorrow at the request of the New
York deputies.

Franklin rises and reflects upon how little progress they are making and how
many back and forth arguments have been made lately.  The assembly has spoken
of every historical example to aid in their situation, but perhaps there are
limits to human understanding.  When we were warring with Britain, we began
every meeting of Congress with a prayer.  Given the importance of their work,
he moved that they begin each morning of their convention with a prayer
officiated by the clergy in the town.  Sherman seconds the motion.

Hamilton worried that beginning their days with a prayer so late into the
convention would lead the public to criticize them and to think that it was a
failure of the convention that necessitated this change.

Franklin, Sherman, and others answered that the past omission of a duty could
not justify the continued omission of a duty and that not opening in prayer
would likely bring worse criticism.

Williamson stated that the true reason for the omission had been a lack of
funds to pay the clergy.

Randolph, in an effort to compromise, moved that a sermon be preached on the
Fourth of July at the request of the convention, and thenceforth the
convention could open each morning with a prayer.  Franklin seconded this
motion.

The committee adjourned without any vote being taken on the motion.

Franklin wrote afterwards, "The Convention, except three or four persons,
thought prayers unnecessary."(I, 452, footnote)

William Blount writes to one of his relatives that evening, "I can say things
are so much in embryo that I could give you no satisfactory account if I were
so much at liberty."(IV, 126)

# June 29, Friday

Johnson affirms that the states are political societies.  Are we to form a
general government for the people or the states?  Surely the latter.(I, 470)
The aristocratic interests have their means of defending themselves.  The
states likewise should have their means of defending themselves.  He suggests
that perhaps one branch of the legislature should be represented based on the
people and the other should represent the states.

Gorham pointed out that the states as they were currently confederated had
every right not to consolidate under these proposals.  However, the small
states should consider what will happen if the confederation dissolves.  The
large states will be able to protect themselves.  The small states should be
the most interested in forming a national government.  Delaware would be at
the mercy of Pennsylvania.  New Jersey does not and cannot have any foreign
trade and would be at the mercy of trade through New York for her consumption.
Many of the states have formed through the union of multiple colonies --
Massachusetts and Plymouth and Maine, Connecticut and New Haven.  These unions
have been beneficial, so too should be a union of the states.

Ellsworth was hopeful that some good government would be adopted.

Read had no objection to the system if it were purely national, but the plan
retained too much of the federal system.  The small states have little to fear
of dissolution.  Delaware enjoys tranquility and will continue to do so.  The
larger states want more power and a general government to allow them to have
it.  The only solution is to incorporate the states into one national
government.  Otherwise, the representatives from the larger states will
monopolize the power.  He praised Hamilton's plan and wished that it would
replace the current plan under debate.

Madison agreed that there was a place for the state governments but felt that
there was a gradation among governments from the smallest town to the most
sovereign empire.  The power of the states will be much reduced.  In fact in
some respects (e.g. indefinite powers of taxation) the general government will
be more powerful than the British Parliament were while they were colonies.
Giving the states an equal vote will produce an unequal vote among the
citizens of the union.  If we let each state depend on itself, they will soon
all fear foreign power and influence from neighboring states, and their lax
governments will be transformed into vigorous and high toned ones.  The
governments may become too powerful and threaten the internal liberty of all.
Each state would create a standing army for protection.  Incessant wars which
banished liberty from the old world would happen here, and executive
magistrates are always afforded more power in war time.  This will happen with
independent states or partial confederacies.

Hamilton asked if it was in their best interests to sacrifice individual
rights for the rights of artificial beings called "states".  Are the
inhabitants of the larger states to surrender their rights for the
preservation of artificial beings?(I, 472)  The smaller states feel that by
losing their equality, they will lose their liberty.  But representation is
not a question of liberty but of *power*.  Indeed Delaware will have one-tenth
the power of Pennsylvania, but will her citizens be any less free?  He
admitted that state representatives would have local attachments, but he
believed there might be ways of reducing that influence.  He wished to point
out one other argument against dissolution: Independent states would form
alliances with different foreign powers who would out of jealousy foment
quarrels and disturbances amongst them.  It was said that the proper end of
republican government was domestic tranquility, but no republic can provide
happiness at home without possessing sufficient stability and strength to make
itself respectable abroad.  We must form such a government now; we cannot
trust in future amendments to do it.  It is a miracle that we are here now
deliberating on this.  It would be madness to rely on future miracles.

Pierce considered how the members of Congress supported their local
advantages.  State interests must be sacrificed for the general good *but*
without destroying the states.  He represented a small state in this
convention, but he considered himself a citizen of the United States, whose
general interest he would always support.

Gerry denied the assertions of state sovereignty.  The states are not now nor
ever were independent.  Even when they were framing the AoC, they understood
the injustice of equal suffrage, but he had voted for it against his judgment
because of the immediate danger.  He criticized the delegates at the
convention because when they should have come here as a "band of brothers"
they acted more like "political negotiators"(I, 467)

Luther Martin countered that the independence and sovereignty of the states
were once well understood, though they may now seem strange and obscure.  He
read the passages from the AoC which used that language.  [Article II of the
AoC.]

The committee voted against the motion to have first branch elected based on
the method of the AoC, and the first clause of the seventh resolution passed
in the affirmative.

Ellsworth and Johnson moved to postpone the rest of the seventh resolution to
take up the eighth regarding the second branch of the legislature.

The vote for postponement passed in the affirmative.

Ellsworth then moved that the right of suffrage in the upper house be the same
as for Congress in the AoC.  Since the plan is half national and half federal,
the national interests are represented in the lower house, and the federal
interests can be represented in the upper house.  He hoped for such a
compromise because otherwise the convention would have been in vain.  While
Madison is right that the larger states share no interests now, they will like
individuals find a way of joining together and gaining from their advantage.
The small states must have some means of defending themselves.  The AoC was
founded on the equality of suffrage between the states.  Were they now to
ignore that precedent?  "Let not too much be attempted by which all may be
lost.  He was not in general a half-way man, yet he preferred doing half the
good he could rather than do nothing at all."(I, 469)

Baldwin wished that they had enumerated the powers of the legislature before
voting on suffrage.  He would vote against Ellsworth's motion, but he did not
like the current resolution in the plan either.  He preferred the right of
suffrage in the upper house be apportioned according to property as was done
in Massachusetts.  He agreed with others that it would be impossible for a
national government to take the place of the states in local affairs.

Madison stated that if he thought there was a danger to the smaller states, he
would give them a means to defend themselves.  But there is no danger.  The
problem in the general government was not larger states against smaller but
northern interests versus southern.  If the senate has equal suffrage, we will
see all the problems remain that we have seen in Congress.  "I would
compromise on this question, if I could do it on correct principles, but
otherwise not -- if the old fabric of the confederation must be the
ground-work of the new, we must fail."(I, 476)

The committee adjourned.

Washington dines at Robert Morris's house this evening.(IV, 130)  In 1831
Jared Sparks, writing a life of Gouverneur Morris, writes to Madison about the
following anecdote:  "On [G.  Morris's] return he called at the house of
Robert Morris, where he found Washington, who, with R. Morris, was much
dejected at what they deemed the deplorable state of things in the
convention."(III, 498)  There might be some truth to the encounter since G.
Morris returned to the convention the next Monday, July 2.

# June 30, Saturday

[Lansing notes that Hamilton left town this morning.(IV, 131)  Mason explains,
"Yates and Lansing never voted *in one single instance* with Hamilton who was
so much mortified at it that he went home."(III, 367)]

Brearly moved to have the president of the convention write a letter to the
governor of New Hampshire requesting the immediate attendance of their
delegates.  The difficulties of the subject and the differences of opinion
required all the assistance that the committee could obtain.  Paterson
seconded the motion.

Madison notes in his journal that "it was well understood that the object was
to add New Hampshire to the number of states opposed to the doctrine of
proportional representation, which it was presumed from her relative size she
must be adverse to."(I, 481)

Rutledge did not see the necessity.  New Hampshire knew the convention was
going on and could attend if they liked.  Should we suspend our business and
ask Rhode Island to send delegates as well?  Hopefully, all the present
difficulties would be worked out by the time they arrived anyway.

King mentioned that he had been in communication with New Hampshire as a
private correspondent.  From what he had gathered the delegates should arrive
shortly.  Personal circumstances had held them away.  Another letter would
have no effect.

Wilson wondered as to the propriety of sending such a message.  It could
spread alarm.  Also, since the convention was voluntary, he could see no
reason to solicit any particular state to attend.

Brearly's motion was voted against.

The committee then resumed the debate surrounding Ellsworth's motion for
allowing an equal vote to each state in the upper house.

Wilson pointed out that the states who voted against proportional
representation yesterday made up 22/90ths of the population.  The question now
is:  Shall a quarter of the US withdraw from the union, or shall
three-quarters renounce the inherent, unalienable, and indisputable rights of
men in favor of the artificial beings called 'states'?  Much has been made of
an imaginary combination of three states under proportional representation.
If we form the senate based on the motion, then one-third of the population
could overrule two-thirds on every question.  Are we forming a government for
men or imaginary beings called states?  Will the people be satisfied with
these metaphysical distinctions?  The rule of suffrage must be the same in
both houses.  When the smaller governs the greater, we call this aristocracy
or tyranny.  Bad governments are of two sorts:  that which does too little and
fails through weakness and that which does to much and destroys through
oppression.  Our confederation has failed through weakness, and we are here in
the convention to remedy it.  If we compose the senate as our opponents wish,
then we will leave our government fettered as before; only now, we will see
the good purposes of fair representation in the first house defeated by the
second.  He finished by reiterating that he thought the states were necessary
and valuable components of a good system.

Ellsworth countered that the motion was not an example of the minority ruling
the majority -- it was a necessary check given to the few to save them from
being destroyed by the many.  Had equal suffrage been kept in both houses,
Wilson might have a point.  British parliament -- which had been often praised
in this hall -- gave the House of Lords, who are so small a portion of the
population, veto power in order to protect their rights from the Commons.  No
confederation has ever existed without equal votes by all members.
Furthermore, the danger of a combination of the larger states is not
imaginary.  If some commercial treaty favored the ports of Boston,
Philadelphia, and somewhere in the Chesapeake, would they not join together to
support it?  A similar combination might occur in the appointments of great
officers.  He appealed to the obligations of the current federal pact, that
some regard should still be paid to the concept that each state, small or
large, held an equal right of suffrage.

Madison stated that "equality of representation was dictated by the necessity
of the times."(IV, 131)  He then accused Ellsworth of being contradictory.
First, Ellsworth accused the three larger states of being aristocratic in their
oppression of the small.  Then, he likened the protection of the smaller
states to the British House of Lords.  Ellsworth also erred in saying no
confederation was formed on proportional representation.  The King of Prussia
has nine votes in the German system.  More importantly, the Lycian confederacy
had members whose votes were in proportion to their importance, and
Montesquieu recommends them as the fittest model of that form of government.
Had the facts been as stated by Ellsworth, they would have only strengthened
the arguments against him -- the history and fate of ancient and modern
confederacies demonstrate some radical vice in their structures.  Ellsworth
considers equal suffrage in the senate as a bulwark, but there are many ways
in which the majority of states could injure the majority of people:

1. by instructing the wishes and interests of the majority
2. by extorting measures repugnant to the wishes and interests of the majority
3. by imposing measures adverse to the majority, as the senate will probably
   have some powers which the lower house does not

He agreed with Ellsworth that where different interests existed, they should be
given means of defense against one another.  However, the states were not
divided in their size but, mostly, in their climate and institution of
slavery.  The interests of the states were split between North and South, and
if any defenses be established between the states, it should be between those
two interests.  He admitted that he had thought about proposing a
representation based on freemen in the lower house and a representation
combining slaves in the upper house.  He declined to propose this due to (1)
an unwillingness to urge a diversity of interests when it will likely arise of
itself and (2) the inequality of powers that would be vested in the two houses
would destroy the equilibrium of interests.

Sherman responded to Madison's criticisms of the delinquency of the states.
Madison tried to insinuate that their delinquency was because Congress was
faulty.  However, the fault lies with the states.  Congress merely needs more
power to make their good measures effectual.

Davie felt proportional representation in the senate was impracticable.  There
would be ninety members at the outset, and more would be added by new states
entering the union.  So numerous a body cannot have the activity that the
committee wants in it.  Electing the senate through electors chosen by the
people would be too difficult.  Local interests will be present in the body
whether it is elected by the legislatures or the people.  However, he feared
to make the senate the representatives of the states, that this might bring us
the same troubles we currently experience with Congress.  Given these points,
he admitted that he could not support any proposal he had yet heard on the
suffrage and composition of the senate.  The nation was partly federal and
partly national.  He could not see why the government could not represent the
people in some respects and the states in others.

Wilson admitted that an exact proportion would result in too large a body of
the senate.  He wondered if the larger states might get fewer representatives
but guarantee the smaller states would get at least one.  He would be willing
to compromise on this position.

Franklin rose and summarized the current debate in the following terms:  "If a
proportional representation takes place, the small states contend that their
liberties will be in danger.  If an equality of votes is to be put in its
place, the large states say their money will be in danger."(I, 488)  He
proposed a compromise where each state would send an equal number of
representatives to the senate.  The senate in turn would get the sole power to
legislate and decide on matters related to the sovereignty of the states or
appointment of national officials.  This was the means by which a ship with
several owners made a decision on an expedition.  [The exact text of his
proposition can be found at I,507-8.]  He added that when he was a minister of
France during the late war, he would have been very happy if the US and France
had had an equal say in the disposition of money for the common defense.

King observed that the simple question was whether there should be equal
suffrage in the senate.  He was amazed that rights of every man in America
could be sacrificed to the phantom of state sovereignty.  He feared for his
country and thought this was the last chance at providing for its liberty and
happiness.  He would support any proposition rather than sit under a method of
suffrage that would be as short-lived as it was unjust.  He said he would be
willing to support some compromise similar to that proposed by Wilson, but he
would never support the current motion.

Dayton stood and said, "When assertion is given for proof, and terror
substituted for argument, [...] they would have no effect however eloquently
spoken."(I, 490)  He wished his opponents to show how the current evils they
experience were sown by the equality they now resist and how the seeds of
state dissolution are not sown in the proposed general government.  "He
considered the system on the table as a novelty, an amphibious monster; and
was persuaded that it would never be received by the people."(I, 490)

Luther Martin would never confederate if it could not be done on just
principles.(I, 490)  Wilson's amendment would admit to the same kind of
inequality.(I, 499)

Madison would support Wilson's suggestion if the senate was made independent
of the states.  The plan as currently composed makes the senate absolutely
dependent on the states.  He understood that he had spoken boldly against
Congress, but he said the would preserve the rights of the states as carefully
as the right of trial by jury.

Bedford stated that there was no perfect middle ground between consolidation
and confederation.  The first is out of the question.  They must continue as
equal sovereigns however imperfect.  We know that political societies possess
all the ambition and avarice of men.  We see this in the votes in this
convention.  The larger states (and states with potential like Georgia and
North Carolina) seek to aggrandize themselves at the expense of the small.
They no doubt believe they have right on their side, but they are blinded by
their own interests.  An inequality of votes will result in an inequality of
power.  "Give the opportunity, and ambition will not fail to abuse it."(I,
491)  You enlarge the power of the general government, and you say it will be
for the good of the whole.  Although the three great states form a majority of
the population, you say they will never hurt or injure the lesser states.  "*I
do not, gentlemen, trust you.*"(I, 500)  Whether the larger states combine or
compete, it will still lead to the ruin of the small.

We must make a government that the people will approve.  Will the smaller
states agree to their degradation.  The people support the enlargement of
Congress's powers namely, the ability to collect revenue and to coerce the
states when necessary.  The small states will not meet the large except under
the confederation.

The nationalists have said "with a dictatorial air" that this is the last
moment for a just government.  This will be true if this proposal goes before
the people.  The larger states will not let the confederation dissolve because
they know the small states "will find some foreign ally of more honor and good
faith, who will take them by the hand and do them justice."(I, 493)  He did
not mean to intimidate; it was a natural consequence.  The people expect an
enlargement of federal power -- why not make a government that they desire?

Ellsworth admitted that if they were under a national government, he would
support national security, but what he wanted was domestic happiness.  The
national government by its nature could not descend to the local interests on
which such happiness depended.  He depended on the state governments to defend
his natural rights.  He had no more satisfactory argument than that.

King wished to preserve the state governments for the same purpose that
Ellsworth had mentioned.  He thought that a clear jurisdiction between them
would provide all the security they needed.  Constitutions are to societies as
legislatures are to people.  The articles of union between England and
Scotland, for example, have secured Scotland's prosperity and happiness.  The
"Rights of States" might be preserved in the constitution.  He knew his
opponents would characterize this as a mere "paper security".  In reply he
pointed out that if they felt the fundamental articles of a compact would not
defend against physical power, there will be no security without the compact
either.

He could not sit, though, without addressing the language of Bedford.  I am
sorry that he would turn his hopes from their common country and propose
courting a foreign power.  He hoped that it was only an outburst of passion.
"Whatever may be my distress, I will never court a foreign power to assist in
relieving myself from it."(I, 502)

The committee adjourned.

Paterson had a number of notes ready for a speech he decided not to deliver(I,
505-7):
* Asserts that the states are sovereign beings under the AoC
* Opponents will say that the larger states contribute more, and therefore,
  representation ought to be proportional
  * A rich state and a poor state are in the same relation as a rich
    individual and a poor individual
* Wilson:  The minority will vote away the majority
  * Their rights are secured in the first branch
  * By your system the majority will vote away the liberties of the minority
* Madison:  Every violation of the AoC renders it null
  * The same power to rescind and to make -- it would be in the power of one
    party to abolish the compact
* King:  See the example of England and Scotland
  * That was a union or consolidation -- ours is a confederacy
  * The articles were agreed to because of the King's bribe

Dickinson likewise has notes for a speech.(IV, 134-9)  This is the gist:
* Are justice and compact to give way to partial convenience and pretended
  necessity?
* Is the current plan a foundation for empire?  "We shall only be pioneers for
  succeeding tyrants."(IV, 137)
* Redivide the states into equal portions, then we will have the equity and
  equality talked about.
* The eyes of the world are upon us.  Let our general principles be
  well-founded.

George Mason writes that evening that the convention is at a point where
fundamental principles must be decided.  If the committee can reach an
agreement, then they will probably sit until September.  If not, then the
convention will likely dissolve in the next week.(III, 50)

Mason also writes a "memorandum" some time around this date.(IV, 140)  It must
concern everyone that so many differences are to be found in such an august
body as this convention, appointed to so amend the constitution to preserve
the protection, safety, and happiness of the people.  Historical and
philosophical arguments have been made to support the justice of propriety of
single, equal suffrage of the states.  The conclusion has been made that the
people would refuse to adopt a government founded on a more equal
representation of the people rather than the states.  The argument rests on
the assumption that the states had always been sovereigns.  If this argument
is not grounded on facts or at best to be found only on facts from an outdated
paper, then no satisfactory conclusions can be drawn from them.(IV, 140)

George Washington writes that evening to David Stuart, a Virginia politician
and relative, about the convention.  He calls the refusal of Rhode Island to
send delegates "scandalous conduct".  He also mentions that New Hampshire
delegates have not reached the convention yet.  They have offered reasons, but
he is not sure if the excuses are legitimate or if they are intentionally
dragging their feet.  The primary causes of disagreement in the convention are
state interests and the ardent contention for state sovereignty -- the same
issues that have made the present government weak at home and disrespected
abroad.  He wishes that they would embrace a more enlarged and general scale
of politics.  He hopes that they will "form such a government as will bear the
scrutinizing eye of criticism and trust it to the good sense and patriotism of
the people to carry it into effect."(III, 52)

# July 2, Monday

[Madison reports that Gouverneur Morris returned to the convention this day
"having left the convention a few days after it commenced business."(I, 511,
footnote)  He was absent to attend the death of his mother.(III, 498)  He was
definitely present at the convention as late as May 25.  A second hand source
says he was present for Hamilton's speech on June 18, but this does not jive
with Madison's account.]

The committee opened the meeting by voting on Ellsworth's motion for equal
suffrage in the senate.  The vote was split -- 5 states for, 5 against, and
one (Georgia) divided.

Pinckney thought an equality of votes in the senate was inadmissible, but he
admitted that the worries about a combination of larger states to push the
smaller around was founded.  He believed that the way to prevent this was to
divide the states into classes and apportion an unrepresentative number of
votes that way.  Congress has failed to amend the federal system, and the only
thing that has prevented dissolution is this convention.(I, 510-1)  He
proposed the following resolutions(I, 520-1):

> Resolved that the second branch of the national legislature shall be elected
> in the following manner -- that the states be divided into ______ districts;
> the first to comprehend the states of ______ the second to comprehend the
> states of ______ the third to comprehend the states of ______ the fourth to
> comprehend the states of ______ and etc. -- that the members shall be
> elected by the said districts in the proportion following, in the first
> district...

> Resolved that the members of the second branch be elected for ______ years,
> and that immediately after the first election they be divided by lot into
> ______ classes; that the seats of the members of the first class shall be
> vacated at the expiration of the first year, the second the second year, and
> so on continually; to the end that the ______ part of the second branch, as
> nearly as possible may be annually chosen.

> Resolved that it shall be in the power of the national legislature for the
> convenience and advantage of the good people of the United States, to divide
> them into such further and other districts for the purposes aforesaid, as to
> the said legislature shall appear necessary.

C. C. Pinckney hoped his cousin's motion might be considered.  He wasn't
entirely in support of it but thought some compromise might be possible.  He
liked the one Franklin expressed.  He moved that a committee be made with a
member from each state to meet and report on some compromise.

Luther Martin had no objections to the motion, but said, "[N]o modifications
whatever could reconcile the smaller states to the least diminution of their
equal sovereignty."(I, 511)

Sherman admitted that they were at a "full stop", and a committee was the only
thing they could do.

G. Morris also supported the committee.  He spoke at length on the necessity
of a strong senate and checks and balances between the rich and poor.  The
purpose of the senate should be an aristocratic body to check the
changeability and excesses of the democratic branch.  The two bodies must
check one another by turning their vices against each other.  The aristocratic
body must have great personal property and pride.  "[P]ride is indeed the
great principle that actuates both the poor and the rich.  It is this
principle which in the former resists, in the latter abuses authority."(I,
512)  The senate must be as independent and stable as possible.  If senators
must resort to democratic choice, then democracy will eventually take over the
whole.  "The rich will strive to establish their dominion and enslave the
rest.  They always did.  They always will."(I, 512)  Thus, by setting the
aristocratic interest apart, the popular interest will be combined against it.
The senate must hold their offices for life.  They should also be allowed to
hold offices.  If the best and most virtuous men are prevented from holding
office, who will hold them?  "The wealthy will ever exist; and you can never
be safe unless you gratify them as a body, in the pursuit of honor and
profit."(I, 518)  Also against paying senators.  They should be independently
wealthy.

The executive should appoint the senators and fill vacancies.  This removes
the present differences on the ratio of suffrage.  Since the senators are
independent and for life, they may be taken just as easily from one place as
another.  The best way for the general government to resist the power of the
states is to make serving in the general government more appealing than
serving in the state governments.  The current plan is too dependent on the
states.  It is a mere treaty and not a government.  "A firm government alone
can protect our liberties.  [I fear] the influence of the rich."(I, 513)  The
influences of both bodies must be checked against one another.

Randolph favored the committee, but he did not expect much from it.  He
criticized Bedford's language on June 30.  If the smaller states worried about
the combination of the larger states, then the executive with revisionary
power could check it.  He even suggested that the states might retain an equal
vote for the executive.  The opposing bodies that G. Morris proposed could
never coexist.  Dissensions would arise as had been seen between the
legislatures within some of the states.  If the confederation dissolves,
neither the large or small states will survive.  He would pursue a scheme of
government that would prevent such a calamity.

Strong supported the committee and hoped they would report on the seventh
resolution as well.(I, 519)

Williamson hoped the smallness of the committee would allow for a compromise
which he thought was necessary to proceed.

Wilson objected to the committee because its method of voting would be one
which was opposed by one of the sides.  [Equal suffrage?  But that's been the
method of voting in the *entire convention*.]  Experience in Congress had also
shown the failure of committees consisting of members from each state.

Lansing did not oppose the committee but expected little from it.

Madison opposed the committee.  He had seen nothing but delays from committees
in Congress.  The report of the committee, being only and *opinion*, would
neither shorten the debate nor influence the decision of the whole.

Gerry favored the committee.  "Something must be done, or we shall not only
disappoint America, but the whole world."(I, 515)  We must make compromises --
the state constitutions would not have been formed otherwise.  Defects can be
amended by future conventions.

The committee voted to form a select committee out of a member from each
state.  The delegates, elected by ballot, were:

| State | Delegate |
|-------|----------|
| Massachusetts | Gerry |
| Connecticut | Ellsworth\* |
| New York | Yates |
| New Jersey | Paterson |
| Pennsylvania | Franklin |
| Delaware | Bedford |
| Maryland | L. Martin |
| Virginia | Mason |
| North Carolina | Davie |
| South Carolina | Rutledge |
| Georgia | Baldwin |

* Note:  Ellsworth would be absent for the committee meeting on July 3, and
  Sherman would actually attend in his place.(I, 526, footnote)

The committee adjourned until Thursday, July 5.

William Paterson writes to his wife, "It is impossible to say when the
convention will rise; much remains to be done, and the work is full of labor
and difficulty."(IV, 143)

[Blount leaves the convention to attend Congress after this day.  He returns
August 7.(III, 587)]

# July 3, Tuesday

[Yates -- who was appointed to the select committee -- is the only journal
entry for this day.]

The select committee met.  Gerry was appointed to be the chair.

According to Yates the committee rehashed many of the same arguments that had
been previously heard in the convention with respect to suffrage in the
national legislature.  Yates, having remained mostly silent in the convention
delivered his arguments for equal suffrage.  The speech was not recorded, but
the letter from Yates and Lansing to Clinton contains its principle arguments.
[III, Appendix A, CLXVII]

Franklin made some resolutions which were modified and then agreed to.(I, 523)
Luther Martin says the nationalists offered to allow equal representation in
the senate *if* the federalists would agree to the terms they wanted in the
house of representatives.(III, 188)  Gerry takes credit for these terms.  He
states that the resolution regarding the suffrage of the senate "never would
have been agreed to by the committee or by myself as a member without the
provision" relating to the origin of money bills described below.(III, 265)
Martin says of the compromise, "it was only consenting, after they had
struggled, to put *both their feet on our necks*, to take *one of them off*,
provided we would consent to let them *keep* the *other on*".(III, 188)

The following was their report(I, 523):

> The committee to whom was referred the eighth resolution, reported from the
> committee of the whole house, and so much of the seventh as had not been
> decided on, submit the following report:
>
> That the subsequent propositions be recommended to the convention, on
> condition that both shall be generally adopted.
>
> That in the first branch of the legislature, each of the states now in the
> union, be allowed one member for every 40,000 inhabitants, of the
> description reported in the seventh resolution of the committee of the whole
> house -- That each state, not containing that number, shall be allowed one
> member.
>
> That all bills for raising or apportioning money, and for fixing salaries of
> the officers of government of the United States, shall originate in the
> first branch of the legislature, and shall not be altered or amended by the
> second branch; and that no money shall be drawn from the public treasury,
> but in pursuance of appropriations to be originated in the first branch.
>
> That in the second branch of the legislature, *each state shall have an
> equal vote.*

Madison reports that Sherman had made a motion to the effect "that each state
should have an equal vote in the second branch; provided that no decision
therein should prevail unless the majority of states concurring should also
comprise a majority of inhabitants of the United States".  This motion was not
deliberated on or approved by the committee.  A similar provision had been
proposed when the AoC were being formed.(I, 526, footnote)

The select committee adjourned.  The convention took the next day, the Fourth
of July, off.

Alexander Hamilton traveling through New Jersey writes to Washington that the
people he has been speaking to are worried that the convention will be too
scared to go *far enough* in their proposals for government.  "They seem to be
convinced that a strong well mounted government will better suit the popular
palate than one of a different complexion [...] and that they must substitute
something not very remote from that which they have lately quitted."(III, 53)
He ends the letter by saying he expects to be absent ten or twelve days and
will return "if I have reason to believe that my attendance at Philadelphia
will not be a mere waste of time".(III, 54)

Spaight writes to James Iredell:  "The convention has made, as yet, but little
progress in the business they have met on; and it is a matter of uncertainty
when they will finish."(III, 54)

# July 5, Thursday

Gerry read the report from the select committee on Tuesday.

Gorham wished to hear how the select committee had arrived at the particular
conditions present in the resolutions.

Gerry stated that the members of the committee were of different opinions but
had agreed to the report in order that some ground might be made towards a
compromise.

Wilson thought the select committee had exceeded their powers.

Luther Martin proposed to take the motion on the whole report.

Wilson preferred to divide the report to vote on it.

Madison stated that the "concessions" on the part of the smaller states were
no real compromise.  The senators could get around the origination and
amendments of certain bills by handing them to members of the lower house to
introduce.  The convention is now at the point of conciliating the smaller
states by doing injustice to the majority of people in the US or displeasing
the smaller states by gratifying the majority of the people.  The choice would
be obvious.  He could not believe the smaller states would prefer to take a
chance on their own than to receive protection from a just government.  He
picks on the language of Bedford in particular again.  If the larger states
are willing to accept a just and judicious plan, he hoped the smaller states
would eventually do the same.

Butler did not believe the people of the US were so low as to adopt a plan so
unjust on the mere respect of the convention.  The second branch ought to be
apportioned according to the property of the states.

G. Morris objected to the form and matter of the report.  The whole thing
seems impracticable and wrong.  He considered himself a representative of not
only America but the whole human race -- "for the whole human race will be
affected by the proceedings of this convention."(I, 529)  The view of the
federalists is too narrow and short-sighted.  We did not come here to bargain
for particular states but for America as a whole.

Suppose that we leave here with the larger states agreeing to the plan, and
the smaller states declining it.  For a time, the separatists in the smaller
states will have power, but eventually, the ties of common interests and
habits will bring some party in those states to wish to unite.  This could
result in civil unrest.  How far foreign nations would be willing to involve
themselves no one knows.  The second branch of the report will cause appeals
to the states which will undermine the general government and destroy the
lower house.  If a bill passes in the senate but some state voted against it,
will that state abide by it?  State attachment and importance has been the
bane of this country.  We cannot get rid of it, but we can try to reduce it.
We must look past our little, particular spot and seek the true interest of
man.  Who is to say a generation or two from now where our descendants will
live?

Bedford rose to explain himself.  He did not imply that the small states
*wanted* to court the interests of foreign nations.  The articles are not
dissolved until done so by the larger states.  If the larger states dissolve
the confederation due to a breach of faith on their part, then foreign
governments having interests on this continent will make treaties with the
smaller states.  That is all.  No man can see what extremes the small states
might be driven to when oppressed by the large.  More worrying was the talk
from Morris that the sword should unite and from Gorham that Delaware should
be annexed to Pennsylvania and New Jersey divided between other states.  The
smaller states have conceded the suffrage of the house of representatives, but
they feel they ought to have some security somewhere -- that is the senate.
If they are not gratified by corresponding concessions, the small states will
never adopt the plan.  It is better that a defective plan be adopted than that
none should be recommended.  Defects can be corrected later.

Ellsworth was ready to adopt the report.  Some compromise was necessary, and
he had not heard a better one.

Williamson thought that the delegates should stop assuming the worst about
their opponents' words.  He would like to hear the report discussed but as is,
thought it contained the most objectionable resolutions he had yet heard.

Paterson understood that speaking freely would result in discussion getting
heated.  He complained of how Gorham and Morris had treated the smaller
states.

Gerry stated that although he had agreed to the report he had very material
objections to it.  The United States are not a single nation, nor do they
represent several nations.  No one should cling too closely to either view.
If we cannot compromise, then secession is inevitable.  Some at the convention
seem bent on it.  If we cannot reach an agreement, some foreign sword will do
the work for us.

Mason stated that the report of the select committee was not meant to be
resolutions to be adopted but the groundwork of further accommodations.  And
accommodations must be made.  "It could not be more inconvenient to any
gentleman to remain absent from his private affairs, than it was for him; but
he would bury his bones in this city rather than expose his country to the
consequences of a dissolution of the convention without anything being
done."(I, 533)

The first proposition of the report was taken up.

G. Morris thought that property ought to be taken into account as well as
population.  Life and liberty are considered to be worth more than property,
but an accurate view would show that property is the main object of society.
The savage state was preferable to the civilized with respect to life and
liberty and to all who had never had a taste of property.  If property is the
main object of society, it ought to be one measure of the influence due to
those who would be affected by government.  He also thought the method of
representation ought to be so devised to secure the original 13 states control
of the national government.  New states formed out of the western territories
would have different interests and would not be able to provide the same
revenue as the Atlantic states.

Rutledge agreed with everything Morris had just said.  He moved to postpone
the discussion of the first resolution to take up the following(I, 525 and IV,
150):

> That the suffrages of the several states be regulated and proportioned
> according to the sums to be paid towards the general revenue by the
> inhabitants of each state respectively -- That an apportionment of
> suffrages, according to the ratio afforesaid, shall be made and regulated at
> the end of ______ years from the first meeting of the legislature of the
> United States -- and so from time to time at the end of every ______ years
> thereafter -- but that for the present, and until the period first above
> mentioned ______ shall have one suffrage, etc.

Mason stated that if new states were admitted to the union, they should not,
for obvious reasons, be the subject of unfavorable discriminations.

Rutledge's motion was voted down.

The committee adjourned.

Nathan Dane, a Massachusetts Congressman, writes to Rufus King that he hopes
they produce something out of the convention because people understand that
the federal constitution requires amendments.(III, 54)

[Yates' journal entries end here.  Lansing reports that this was his and
Yates' last day in convention, but it seems New York is represented in votes
until July 10(I, 536), and Lansing continues to record his own journal of the
debates until at least July 9.]

Lansing and Yates wrote a letter to New York Governor George Clinton the
following January expressing why they left the convention when they did.  They
could not remain when they felt the convention exceeded their authority and
assented to political measures that they thought destructive to the happiness
of the people of the United States.  They assert they would have been against
*any* frame of government which consolidated the states under one government.
They believed consolidation was impossible due to:

1. a lack of authority to make the state government subservient to a national
   government
2. the conviction of the impracticality of a single, general government over
   the whole United States

They believed that the New York legislature never intended to grant them the
authority to abolish the sovereignty of the state.  They believed it was
possible to amend the confederation to remedy the current problems, that these
problems were due solely to a want of power on the part of Congress.(III,
244-7)

Madison, much later in life, says that as deputies from New York who were
against the convention from the start, they decided to leave when they
"foresaw that a primary aim of the convention would be to transfer from the
states to the common authority, the entire regulation of foreign
commerce."(III, 530)

# July 6, Friday

G. Morris moved to commit the number of representatives to "1 member for every
40,000 inhabitants" as was stated in the report.  Wilson seconded the motion.

Gorham wished not to directly fix the number of representatives for each
state.  Population sounded like the best option.  Kentucky and Maine are ready
to split from their respective states, and if they do, then the number of
representatives should be reduced.  Hopefully, all the states will eventually
be broken into divisions.  This will reduce their influence on the general
government.

Gerry did not think the large states ought to be cut up.  He thought
representation ought to be according to some combination of inhabitants and
wealth.

King wished the clause to be committed because he felt it had nothing to do
with the rest of the report.  The ratio of representation cannot be safely
fixed.  If fixed as stated, in 150 years the number of representatives would
be enormously excessive.  Property -- the primary object of society -- is a
better measure of ability and wealth than population.  Congress has already
divided the western territories into ten territories and has told them that
they may enter the union once they possess the same number of inhabitants as
the smallest state, Delaware.  It is possible that ten new votes might enter
the legislature with fewer additional inhabitants than Pennsylvania.

Butler believed some balance would be necessary between the old states and the
new.  Property is the only just measure of representation.  Property is "the
great object of government, the great cause of war, the great means of
carrying it on."(I, 542)

Pinckney thought that the measures of property and wealth -- values of land
and tax revenue -- were too unreliable and unstable.  Population was the only
just and practical rule.  He thought blacks should count equally with whites,
but would agree with the ratio set by Congress.

Davie thought wealth or property ought to be the measure in the senate, and
population ought to be the measure for the house of representatives.

The motion was committed.  The members of the committee were appointed by
ballot:  G. Morris, Gorham, Randolph, Rutledge, and King.

Wilson suggested that the committee might consider adopting a method similar
to that of Massachusetts.  It should give some advantage to the smaller states
without departing from proportional representation.

Wilson and Madison moved to postpone the rest of the resolution to take up the
question of suffrage in the senate.

The committee voted in the affirmative to postpone, and the suffrage of the
senate was taken into consideration.

Franklin said that he could not vote for this matter separately.  The whole
report should be voted on together.

Mason suggested referring the rest of the report to the committee just
appointed.

Randolph said that, as the committee consisted of members opposed to the
smaller states, its decisions could not be acceptable to them.

Martin and Jenifer moved to postpone the current vote until the committee
reported.

The committee voted to postpone in the affirmative.  The clause relating to
the originating of bills was then resumed.

G. Morris was against a restriction of these matters in either house.  If the
senators are not allowed to propose money plans, then the people will not be
able to compare the merits of their plans with the ones in the house of
representatives.

Wilson thought that the reverse of the report should be instituted.  The less
numerous body is better for deliberation; the more numerous is better for
decision.

Williamson thought that if the right was not given to both branches of the
legislature, then the senate ought to be given the power.  Being the branch
without the popular confidence would ensure the bills would be more closely
watched.

Mason pointed out that the lower house would be the direct representatives of
the people.  If the upper house was given the ability to give away the
people's money, they might soon forget where it came from.  They would become
an aristocracy.  He was concerned with the principles advanced by some of the
gentlemen.  [Regarding positives attitudes towards aristocracy?]  He preferred
proportional representation in both houses but was willing to compromise.

Wilson observed that if the senate was given the sole power to turn money
bills into law, then Mason would have a point.  But what difference does it
make which house of the legislature originates the bill if both must vote it
into law?

Gerry felt that the clause on money matters was not much of a concession on
the part of the small states.  If the senate does not originate money bills,
then it will reduce their weight and influence.

Pinckney thought it was pretty clear that the concessions were one-sided since
the privilege of originating money bills was of no account.

G. Morris believed that there could be no civilized society without an
aristocracy.  The endeavor is to prevent them from doing mischief.  The
current restriction deprives the senate of originating bills that they will be
more capable of crafting.  "It will be a dangerous source of disputes between
the two houses. [...] Every law directly or indirectly takes money out of the
pockets of the people."(I, 545)  The popular branch could extort the senate
with the power.

Franklin defended the reason for the resolution as stated.  It is important
that the people know who disposed of their money and how it had been disposed
of.  This is easier accomplished when those doing it are the direct
representatives of the people.  If there was a danger or a difficulty of a
veto in the senate (where the people were not proportionally represented),
then perhaps there should be no such veto power -- or if that will not do,
then no senate.  [Cheeky bastard :)]

"[Luther] Martin said that it was understood in committee that the difficulties
and disputes which had been apprehended, should be guarded against in the
detailing of the plan."(I, 546)

Wilson countered that "the difficulties and disputes will increase with the
attempts to define and obviate them."(I, 546)  He approved of Franklin's
principles that the people should know what is being done with their money but
thought they would be as knowledgeable and satisfied either way.

C. C. Pinckney was astonished that this could be called a concession.  Such a
restriction to money bills had been previously rejected 8 to 3, and the states
which now call it a concession were against it then as improper.

On the question of the origination of money bills, the committee voted 5 ayes,
3 noes, and 3 states divided.  The committee then voted on whether this result
counted as an affirmative since only 5 of 11 states had approved it.  The
committee voted that it was affirmative.  Madison notes, "In several
preceding instances like votes had sub silentio been entered as decided in the
affirmative."(I, 547)

The committee adjourned.

Abraham Baldwin writes the following to Joel Barlow, patriot and future US
diplomat:

> The conjectures of people on the great political subjects now before the
> convention are very various and not a little amusing.  So many forms of
> government I believe never were contrived before.  They are floating about
> here in all directions like spectators worlds some half-finished some a
> quarter the great part but just begun -- mere political tadpoles.(IV, 152)

# July 7, Saturday

William Rawle by letter informed the convention that the Library company of
Philadelphia granted the members of the convention use of their books for so
long as the convention was in session.(I, 548)  William Jackson, the secretary
of the convention, replied with thanks.(IV, 154-5)

The committee took up the question, "Shall the clause allowing each state one
vote in the second branch stand as part of the report?"(I, 549)

Gerry would rather agree to it than have no accommodation.  A flawed, if
workable, solution is better than a proper one that would not be accepted by
all the states.  He favored the measure as long as the first branch originated
money bills.  It would be best to suspend the question until the latest select
committee should make a report.(I, 550)  There are two or three thousand
influential politicians in the states who would be against the present plan
because it will annihilate the states.(I, 555)

Sherman thought an equal vote in the senate would give the general government
a necessary vigor.  The smaller states, having fewer people, are better able
to capture the real and fair sense of the people than the larger states.  It
is easier for improper men to reach office in the larger states.  The more
influence the larger states have, the weaker the general government will be.
By placing equal suffrage in the senate, we ensure that the majority of people
as well as states will be behind any measure.  If the majority of people are
for it, but the states are against it, then the general government will be
worse off than it is now.

Wilson thought allowing equal suffrage in the senate was an improper
compromise.  It was done more for the delegates at the convention than for
the constituents they were supposed to represent.  It is not just or right.

The committee voted in the affirmative that the clause would stand as part of
the report.  Madison writes, "several votes were given here in the affirmative
or were divided because another final question was to be taken on the whole
report."(I, 551)

Gerry wished to enumerate the powers of the general government before voting
on the representation in the senate.

Madison countered that it would be impossible to enumerate the powers before
knowing how the states would be represented in it.  If a just representation
is not upheld, then the new government will be every bit as impotent and
short-lived as the old.(I, 551)  A minority will control the majority either
through suffrage or by holding their assent to some essential matter.  If the
states have equal votes, a minority of people -- an aristocracy -- will
appoint government offices.  Because the small states are closer to the seat
of government, they will more easily conspire to assemble a quorum.(I, 554)

Paterson feared that given the representation in the first branch, the smaller
states needed an equal suffrage in the second branch to defend themselves.
Nothing short of that would do.  If we cannot agree on this, we should
dissolve and stop wasting our time.  He stated that he would vote *against*
the report because it yielded too much.

G. Morris would vote against the report because it made the senate another
impotent Congress.  Gerry had spoken of the plan being partly national and
partly federal, that one house of the legislature should protect individuals
and the other protect the states.  But what will protect the aggregate
interest of the whole?  "Among the many provisions which had been urged, he
had seen none for supporting the dignity and splendor of the American
Empire."(I, 552)  One of the nation's worst missteps of late was the sacrifice
of the general good for local interests.  What check will the senate provide?
Well, it will prevent the majority of people from injuring particular states.
But what if a particular, deserving state ought to be injured for the sake of
the majority of people?

The states have never been sovereign nations.  The small states, standing on
the brink of war and anarchy, extorted an equality of votes from the larger
states.  They now demand more rights than their fellow citizens of the larger
states.  But we are not now under the same necessity; we can consider what is
right rather than what is expedient.  We cannot put the interests of the
states before the happiness of the people.  The examples of Germany, the
Grecian states, and the Netherlands show the folly of this course of action.
It is of no consequence what states the senators are drawn from as long as
they act as an asylum against these evils.  He is against it drawing from the
states in equal portions but was willing to amend the plan so as to best
secure the people's liberty and happiness.

Sherman and Ellsworth moved to postpone the question to wait on the report
from the select committee.  The motion passed.

The committee adjourned.

Hugh Williamson writes to James Iredell:  "I think it more than likely that we
shall not leave this place before the middle of August.  The diverse and
almost opposite interests that are to be reconciled, occasion us to progress
very slowly."(III, 55)

# July 9, Monday

| State | New Delegates |
|-------|---------------|
| Maryland | Daniel Carroll |

The report of the select committee was read by G. Morris(I, 557-8):

> The committee to whom was referred the first clause of the first proposition
> reported from the grand committee beg leave to report.
>
> That in the first meeting of the legislature of the United States the first
> branch thereof consist of fifty-six members, of which number

| State | No. Members |
|:------|------------:|
| New Hampshire | 2 |
| Massachusetts | 7 |
| Rhode Island | 1 |
| Connecticut | 4 |
| New York | 5 |
| New Jersey | 3 |
| Pennsylvania | 8 |
| Delaware | 1 |
| Maryland | 4 |
| Virginia | 9 |
| North Carolina | 5 |
| South Carolina | 5 |
| Georgia | 2 |

> But as the present situation of the states may probably alter as well in
> point of wealth as in number of their inhabitants that the legislature be
> authorized from time to time to augment the number of representatives; and
> in case any of the states shall hereafter be divided, or any two or more
> states united, or any new state created within the limits of the United
> States the legislature shall possess authority to regulate the number of
> representatives in any of the foregoing cases upon the principles of their
> wealth and number of inhabitants.

Sherman was confused at how the numbers had been calculated.  They did not
seem to conform with any method that had been proposed so far.

Gorham answered that the number of blacks and whites with some regard to
wealth had been the general guide.  The legislature is free to make
alterations as propriety may require.  There were two objections to the means
of 1 member per 40,000 inhabitants:
1. This would make the house too numerous.
2. New western states, who may have different interests, might if admitted on
   that principle eventually out-vote the Atlantic states.

The report removes these objections:  The number will be small, and the
Atlantic states, having initial control, will maintain that control "by
dealing out the right of representation in safe proportions to the western
states."(I, 560)

Luther Martin asked whether the committee were guided by wealth or population
or both.

G. Morris and Rutledge moved to postpone the vote on the number of members to
instead take up the clause regarding the right of the legislature to alter the
numbers from time to time.  That right was agreed to without debate.

Sherman moved to refer the first part to a committee with members from each
state.  G. Morris seconded the motion.

Williamson thought it necessary to return to the rule of population.

G. Morris summarized Gorham's previous answer by saying that the report was
merely a rough estimate meant to bring the matter for consideration of the
convention.  "Wealth was not altogether disregarded by the committee."(I, 560)
Madison said of G. Morris later, "He wished to make the weight of wealth
balance that of numbers, which he pronounced to be the only effectual security
to each, against the encroachments of the other."(III, 499)

Read asked why Georgia was allowed two members when their population was just
below that of Delaware's.

G. Morris answered that the population growth there is so great that they will
likely be entitled to two members by the time the plan comes into effect.

Randolph worried that since the number would not change until the legislature
wished, those in power would never want to do so in order to maintain their
power.

Paterson thought the rough rule of wealth and population being used was too
vague.  Slaves have no freedom, no liberty, no ability to acquire property --
they *are* property.  Do the citizens of Virginia get a number of votes in
their state equal to the number of their slaves?  If slaves are not
represented in the states to which they belong, why should they represented in
the general government?  The principle of representation is an expedient
method by which an assembly is chosen by the people in place of an
inconvenient meeting of the people themselves.  If such a meeting were to
occur, would the slaves be allowed to vote?  No, therefore they should not be
represented.

Madison observed that if Paterson held his principle of representation
consistently, then the small states would have no right to equality of
suffrage.  They ought to vote in proportion to their citizens if the citizens
of all the states should meet.  He suggested that the first branch be
proportioned according to the number of free inhabitants, and the second
branch -- which had as one of its aims the protection of property -- should
be according to the whole number, including slaves.

Butler urged that wealth must be considered in representation.

King observed that since the South was more wealthy than the North, the former
would not unite with the latter unless some respect was paid to their
property.  Eleven of the thirteen states had decided that slaves ought to be
considered in taxation, and taxation and representation ought to go together.

On the question for submitting the first clause of the report to a select
committee with members from each state, it passed in the affirmative.  The
members of the select committee were:

| State | Delegate |
|-------|----------|
| Massachusetts | King |
| Connecticut | Sherman |
| New York | Yates |
| New Jersey | Brearly |
| Pennsylvania | G. Morris |
| Delaware | Read |
| Maryland | Carroll |
| Virginia | Madison |
| North Carolina | Williamson |
| South Carolina | Rutledge |
| Georgia | Houstoun |

The committee adjourned.

# July 10, Tuesday

King presented the report of the select committee from yesterday:

> That in the original formation of the legislature of the United States, the
> first branch thereof shall consist of sixty-five members, of which number

| State | No. Members |
|:------|------------:|
| New Hampshire | 3 |
| Massachusetts | 8 |
| Rhode Island | 1 |
| Connecticut | 5 |
| New York | 6 |
| New Jersey | 4 |
| Pennsylvania | 8 |
| Delaware | 1 |
| Maryland | 6 |
| Virginia | 10 |
| North Carolina | 5 |
| South Carolina | 5 |
| Georgia | 3 |

Rutledge moved to reduce New Hampshire to two delegates because she was a poor
state.  C. C. Pinckney seconded the motion.

King stated that New Hampshire numbered more than 120,000 people and had
fertile soil.  It would be likely that she would increase her numbers quickly.
The four eastern states (of 800,000) souls have one-third fewer members in the
house than the southern states (700,000 if you include slaves as
three-fifths).  It is important to unite the north and the south, but we ought
not to subject them to gross inequality.  The differences of interests are not
between the small and the large but between the east and south.  He was
willing to grant them some representatives in the interest of accommodation
but not a majority.

C. C. Pinckney stated that the southern states were more favored in the
previous report; although, he was glad to see members added to Georgia and
Virginia.  If they have a minority and the regulation of trade is relegated to
the general government, the southern states will be nothing more than
overseers of the north.  The southern states ought not to have a majority, but
they should be raised closer to an equality.

Williamson was not for reducing New Hampshire, but he did worry that the
northern majority endangered southern interests and had the means of
perpetuating it.

Dayton believed that they were drawing the boundary between northern and
southern interests wrong.  Pennsylvania was the dividing line having six
states on either side of her.

C. C. Pinckney insisted on the south having a due weight in the house given
her superior wealth.

G. Morris disliked the turn of the debate.  The states have many
representatives in the convention, but few are representatives of America.
The southern states have more than their fair share of representation.  Wealth
is a contributor to representation but not the only factor.  "If the southern
states are to supply the money, the Northern states are to spill their
blood."(I, 567)  The expected revenue from the southern states is greatly
exaggerated.  The representatives from New Hampshire should not be reduced.

Randolph did not believe New Hampshire was entitled to three members, but he
was against reducing her numbers.  The numbers ought to be set by census than
at the discretion of the legislature, and a super majority should be required
to pass certain motions, particularly commercial ones.(I, 567-8)  For example,
Edmund Randolph on this day had written up a list of accommodations meant to
act as a compromise for the small states.  He gave this list to Madison as a
record, but he did not bring them up until July 16.  \[See the notes for July
16 for a summary of the accommodations.\](III, 55-6)

The question for reducing the number of New Hampshire's representatives was
voted down, as was every other motion for reducing or increasing the numbers
for the representatives of any of the states.

Madison moved to double the number of all representatives.  A majority of a
quorum of 65 delegates would not be enough to represent all the people of the
United States, and they could not possess enough of the confidence of the
people.  Doubling the number would not be too great even with the addition of
new states.  The increased expense is worth the price.

Ellsworth thought the expense would indeed be too high.  If the body is too
large, debate and discussion would pass too slowly and would probably not
reach the conclusions it should.  The state legislatures are too large as it
is, and the general government doesn't need even that many since their debates
will only affect a few, great policies of national interest.

Sherman preferred 50 instead of 65.  Many would need to travel a great
distance.  They should also keep in mind the increase of representatives with
the addition of new states.

Gerry supported increasing the number of representatives.  A larger body is
less likely to be corrupted and more likely to have the confidence of the
people.  The worry of them growing too numerous can be assuaged by setting a
fixed number beyond which they should not grow.

Mason admitted that the objection over expense had weight not just for the
expense of the national government but also the expense to the people to
support them.  However, the question of expense was out-weighted by the danger
of having a body too small.  Thirty-eight would perhaps form a quorum of
sixty-five.  Twenty would be a majority of thirty-eight.  This is too few
delegates to make decisions on behalf of the people of the United States.
They could not bring enough knowledge of local interests nor acquire the
confidence of the people.  Even double the number may be too few.

Read supported Madison's motion.  With one representative only, both Rhode
Island and Delaware are an accident away from having no representation at all
in the body.  So small a number would lack the confidence of the people.  He
hoped the objectives of the general government would grow.  He agreed with
Gerry that a max number might be fixed to prevent an excessively large body.

Rutledge opposed the motion.  The state legislatures were too numerous as it
was.  If the size of the body is small enough, all the delegates of the states
should be expected to attend; therefore, there is no need to assume the
smallest quorum would ever be present.  Their constituents would urge their
attendance, and the body is not likely to sit more than six or eight weeks a
year.

The committee voted the motion to double the number of delegates down.  The
committee then voted in the affirmative to approve the numbers as they stood
in the current report.

Broom gave notice to the convention that he intended to claim an equal
suffrage in the second branch of the legislature for his state (Delaware).

Randolph moved that the ratio of representation ought to be fixed by regular
census.

G. Morris opposed a census.  He was against those sorts of "shackles" on the
legislature.  It would be inconvenient.  Furthermore, the western lands may
eventually outnumber the present Atlantic states.  It would be important for
the current states to maintain their majority in the legislature.  There was
an objection that without fixing the ratio to a census, the legislature would
never change the representation.  While this is possible, he thought it was
not probable.

The committee adjourned.

[Paterson and Brearly have papers where they try to work out the appropriate
number of delegates for each state based on the number of free inhabitants and
slaves.(I, 572-4)  Ditto Pinckney and Butler.(IV, 160-2)]

George Washington replied to Hamilton's letter on July 3 saying, "[The
convention] are now if possible in a worse train than ever [...]  In a word, I
*almost* despair of seeing a favorable issue to the proceedings of the
convention, and do therefore repent having had any agency in the business."
He goes on to characterize the federalists as "narrow minded politicians", and
he wishes that Hamilton would return.(III, 56-7)

# July 11, Wednesday

[New York is no longer represented in convention starting this date.]

The question regarding a census was taken up.

Sherman believed the legislature should not be shackled in this way.  We ought
to elect wise and good representatives and confide in them.

Mason was not against an initial ratio set by the convention but believed
that a fixed standard based on a census was necessary.  The northern states
presently had a right to greater representation in the general government, but
eventually, the southern states will probably outnumber them.  Those who have
power will do what they can to maintain it, and a minority in the north should
not be able to rule a majority in the south.  If the constitution does not
provide a census, then he will not support the plan in the convention or in
his state.  If we admit the western states into the union, they should be
given the same rights as the current states.  They will either refuse to unite
in the first place or will quickly secede.  It has been objected that they
will be poor when they enter and will not bring the same revenue as the other
states.  However, they may one day be more numerous and more wealthy than the
Atlantic states.  Population, while not precise, is a sufficient standard of
wealth.

Williamson moved to postpone Randolph's proposition in order to take up a
question regarding the census with a slightly different wording than
Randolph's.

Randolph believed Williamson's motion should be used in place of his own.  The
ratio of representation should be changed when justice requires it, not at the
whim of the legislature.  "What relates to suffrage is justly stated by the
celebrated Montesquieu, as a fundamental article in republican
governments."(I, 580)  New states should be admitted on equal terms.  The
census must be directed by the national legislature.  The states would be too
impartial to take it for themselves.

Butler and Pinckney moved that blacks be represented equally (not
three-fifths) with whites.

Gerry thought three-fifths was the most that could be admitted.

Gorham pointed out that this ratio was already fixed by Congress regarding
taxation.  The delegates from the slave states assert that blacks are inferior
to whites, but when it comes to representation, they are for some reason equal
to freemen.  He agreed that three-fifths was pretty near the just proportion.

Butler insisted that the labor of a slave in South Carolina was as good as
that of a freeman in Massachusetts.  If the government is meant to protect
property, then as property they ought to be valued equally with freemen.

Mason, although he was from a slave state, could not see the justice in the
motion.  Slaves produce valuable labor, and since they are in this way
valuable to the community at large, they ought to be included in the
representation.  However, they are not equal to freemen.  He added that the
southern states have a peculiar species of property over and above that of
other states.

Williamson reminded Gorham that the southern states contended for the
inferiority of blacks with respect to taxation, but the eastern states
contended for their equality in the same regard.  But he approved the
three-fifths ratio.

Butler's motion for the equality of slaves to freemen was voted down.

G. Morris said he had several objections to a census:
1. It fettered the legislature too much
2. It would exclude some states who do not have enough people for a single
   representative
3. It was inconsistent with the resolution passed on Saturday that the
   legislature had the power to adjust the representation from time to time

If slaves are wealth, then why is no other type of wealth considered?  All
these objections could be amended in time, but his primary objection is that
population is a poor measure of wealth.

King agreed with G. Morris's objections but would vote for the motion in the
interest of doing *something*.

Rutledge moved for the explicit admission of wealth to the method of
representation.  The western states will not initially provide as much to the
revenue of the nation as the Atlantic states and should not for that reason be
admitted representation based solely on population.

Sherman thought that population was the best measure of wealth and
representation.  If wealth was included in the calculation, it could only be
included as an estimate.  He had initially wished the method of amendment be
left to the sole discretion of the legislature, but the arguments by Randolph
and Mason had convinced him that a method of census should be fixed in the
constitution.

Read thought the legislature ought not to be too shackled.  It would turn the
constitution into a religious document -- embarrassing to those who had to
conform to it and more likely to produce a schism than harmony.

Mason objected Rutledge's motion as being too vague and impracticable.

Wilson had no objection to leaving the method of amendment in the sole hands
of the legislature but thought wealth was an impracticable rule.

Gorham worried that if the current convention was so ruled by local biases, he
could not trust that the national legislature would do better.  He had been
convinced by the arguments of others that the method of adjusting
representation needed to be fixed in the constitution.

G. Morris stated that the arguments of others and his own reflections had led
him to the opposite conclusion.  If they could not agree on a method that was
just for the present, how could they agree on a method that would be just for
all time.  Regarding the arguments by Mason, he was not convinced that the
western territories should be admitted under the same representation.  Their
delegates would not be as enlightened.  The urban areas are the proper school
for political talent.  The western people will ruin Atlantic interests if they
get power.  The people at the frontier are always against the best measures --
Pennsylvania is a case in point.

He also objected to allowing slaves into the measure of population.  The
people of Pennsylvania will revolt at the idea of being put on footing with
slaves.  Two objections had been made with respect to leaving the adjustment
of representation in the hands of the legislature:  (1) They would be
unwilling to revise at all, and (2) if forced to account for wealth, they
would be bound to a rule they could not execute.  If we cannot trust them in
the first instance, then we ought not to trust them with our liberty either --
let us have no government at all.  If we can trust them in the first instance,
then we can trust their judgment for the second as well.

Madison was surprised to hear such talk from a man who had strongly cautioned
against the depravity of men and the necessity of checking one vice with
another.  If the representatives were bound by the ties he had suggested, then
what is the purpose of a senate?  To reconcile his inconsistency with regard
to the western states, one might suppose he judged human character with a
compass.  The truth is all men having power ought to be distrusted to a
degree.  Morris mentioned Pennsylvania, but this was a case where the original
settlers did not admit a due share of representation to the new settlements.
He could not see any good argument against fixing the ration of representation
according to population.  He agreed that population was poor measure of
wealth, but in a free country, property and labor (the measures of wealth)
would tend to even out.  People will go where land is cheaper because there
labor is dearer.  "If it be true that the same quantity of produce raised on
the banks of the Ohio is of less value than on the Delaware, it is also true
that the same labor will raise twice or thrice the quantity in the former,
that it will raise in the latter situation."(I, 586)

Mason agreed with Morris that the interests of the people ought to be left to
the representatives of the people.  The objection is that without a fixed
method of ratio, the legislature will cease to be the representatives of the
people.  Once the western and southern states outnumber the northern, the
minority will maintain their power over the majority unless provided for by
the constitution.

Rutledge's motion for a consideration of wealth was voted against.  The motion
regarding a constitutional census was voted for.  The question of the
three-fifths ratio for slaves was then considered.

King thought that counting blacks at all along with whites would excite
opposition to the plan by states without slaves.  He was considering
opposition to the clause on that principle alone.  The southern states had
already received a greater allotment than they deserved in the report even
taking slaves into consideration.

Sherman stated that South Carolina had a correct proportion relative to New
York and New Hampshire.  Georgia had more, but their rapid growth seemed to
justify it.  It was not perfect, but he was satisfied with the present ratios.

Gorham supported population as the sole rule of representation.  In
Massachusetts people had been curious enough to compare estimated wealth with
population.  It had been found, even in Boston, an almost exact ratio between
wealth and population.

Wilson could not see the reason for counting blacks as three-fifths.  If they
are citizens, then why are they not considered equal to whites?  If they are
property, then why is no other property taken into consideration?  However, he
was willing to ignore these objections in the spirit of compromise.  He agreed
with Morris that Pennsylvania might oppose being put on footing with slaves,
but he differed with him in thinking population and wealth were not
correlated.  Comparing the city of Philadelphia with western settlements, one
would find a greater inequality in wealth in the city, but the ratios of
wealth to population would be basically the same.

G. Morris felt compelled by the situation to do injustice either to the
southern states or human nature, and he must do so to the former.  He could
never agree to so encourage the slave trade as to count them towards
representation, and he did not believe that the southern states would ever
confederate on terms which would deprive them of that trade.

On the question to agree on including three-fifths of slaves, it was voted
against.

On the question of taking a census in the first year of the new government, it
passed in the affirmative.  The census to occur every fifteen years was agreed
to without debate.

Madison moved to at the phrase "at least" after fifteen years in the event
that circumstances rendered a census on a particular year inconvenient.  The
committee voted against it.

The committee voted to add the clause "and the legislature shall alter or
augment the representation accordingly"(I, 588) without debate.

The committee voted to approve the amended report which was voted against.

The committee adjourned.

[John Dickinson has notes for a speech that was never delivered.  Based on the
content of those notes, the speech probably belongs somewhere near this day:]

Dickinson writes that representation by population is in general the best
rule, but our present circumstance renders it imperfect and unsafe.  New
states, though they may be populous, will be poor and will contribute little
to the national revenue.  In our experience, the members of counties who pay
little to the public treasury vote to give away the wealth of the
state.(157-8)

What is this principle which uses the number of slaves (in preference to other
property) to determine the representation of free men?  The continued slave
trade will increase the influence of the slave states over the others.  I
admit that measuring property or wealth is impracticable.  Why not base
representation on *contribution* in that case?  He suggests that the
representation of some state should not rise beyond a certain proportion of
its neighbors [but I do not understand his justification for this position].
The three-fifths ratio of slaves to representation will be ruinous to the
whole system.(I, 158-9)

Rufus King writes to Henry Knox on this day, "I wish it was in my power to
inform you that we had progressed a single step since you left us [...] If I
had returned to New York with you or with our very able and sagacious friend
Hamilton, I should have escaped much vexation, enjoyed much pleasure, and have
gratified the earnest wishes and desires of Mrs. King".(IV, 163)

# July 12, Thursday

G. Morris moved to add the following clause to the report:  "Provided always
that taxation ought to be proportioned according to representation."(I, 589)

Butler argued that representation should be according to population (blacks
included) but admitted the justice of Morris's position.

Mason also admitted the justice but worried that it could drive the
legislature to the plan of requisitions.

G. Morris admitted there were valid objections to his motion but thought they
were not applicable if the policy was limited to direct taxes.  The rule would
inapplicable to indirect taxes.  He was persuaded that imports and consumption
were nearly equal throughout the United States.

C. C. Pinckney supported the motion but believed that if the revision of the
census was left to the legislature's discretion, it would never be executed.
The rule must be fixed and the execution enforced in the constitution.  South
Carolina has exported £600,000 sterling this past year from the labor of her
blacks.  She will not be represented in proportion to this amount; therefore,
she ought not to be taxed on this amount.  He hoped a clause would be put in
the constitution preventing the legislature from taxing exports.

Wilson approved of the motion but could not see how it could be executed
unless it was restrained to direct taxes.

G. Morris inserted the word "direct", and the provision passed in the
affirmative.

Davie felt compelled to speak out.  The slaves of the southern states must be
counted in their representation by at least three-fifths.  North Carolina
would not confederate under any other terms.

Johnson, believing that wealth and population should be the rules of
representation, asserted that blacks should be included equally with whites
since population is the best measure of wealth.  He moved that a committee
would be organized to take the subject of representation into consideration.

G. Morris stated that he had come to form a compact for the good of America.
He would form this compact with any states who wanted to join, but since this
compact is voluntary, it is in vain to make demands from other states they
will never admit.  The people of Pennsylvania will never agree to including
blacks in the representation.  They should leave it in the hands of the
legislature to regulate the representation according to population and wealth.

C. C. Pinckney believed that the rule of wealth should be made explicit and
not left to the pleasure of the legislature.  The property of slaves should
not be exposed to danger under a government formed for the purpose of
protecting property.

Ellsworth moved to add the clause:

> and that the rule of contribution by direct taxation for the support of the
> government of the United States shall be the number of white inhabitants,
> and three-fifths of every other description in the several states, until
> some other rule that shall be more accurately ascertain the wealth of the
> several states can be devised and adopted by the legislature.(I, 594)

Butler seconded the motion and hoped it might be put to a committee.

Randolph was not satisfied with the wording.  The ingenuity of the legislature
will find a way to perpetuate the initial division of power.  He proposed the
following instead:

> that in order to ascertain the alterations in representation that may be
> required from time to time by changes in the relative circumstances of the
> states, a census shall be taken within two years from the first meeting of
> the general legislature of the US and once within the term of every ______
> years afterwards, of all the inhabitants in the manner and according to the
> ratio recommended by Congress in their resolution of the 18th day of April
> 1783 (rating the blacks at three-fifths of their number) and that the
> legislature of the US shall arrange the representation accordingly(I, 594)

He stated that the inclusion of slaves into the ratio of representation ought
to be explicitly protected.  He lamented that such a species of property
existed, but since it did exist, their holders would require this protection.
Since some in the convention wished to exclude slaves altogether, the
legislature ought not to be left with the option.

Ellsworth withdrew his motion and seconded Randolph's.

Wilson offered that perhaps the wording of the resolution ought to emphasize
that since slaves are entered into the rule of taxation, and representation is
be according to taxation, the slaves should be counted in the ratio.  This
would provoke less umbrage to the admission of slaves in representation.

King had two objections against tying the legislature down with explicit rules
to representation.  First, population is an uncertain estimate of the relative
wealth of the states.  Second, even if the convention could ascertain a just
ratio of representation today, it would not always remain so.  Justice should
be the basis of representation.  There was a necessity of placing some
confidence in every government, and giving the legislature the discretion to
modify the rule of representation does not exceed a reasonable confidence.

Pinckney moved that blacks be counted equally with whites.  They are as
productive and contribute as much to the resources of the country as free men
in the north.

C. C. Pinckney moves to replace "two" with "six" as to the number of years
within which the first census should be taken.  This motion passed in the
affirmative.

On the question of filling in the blank with twenty years, it was voted down,
but ten years passed.

Pinckney's motion that blacks count equally with whites was voted down.

The question on the census and the counting of three-fifths of slaves passed
in the affirmative.

The committee adjourned.

With respect to the principles of representation, Abraham Baldwin states in
1791 before the House of Representatives:

> It had not been found practicable to ground representation in the Federal
> Constitution upon any other principle than that of numbers; but extent of
> territory is unquestionably one of the natural principles on which it rests,
> and should if possible be regarded.(III, 364)

# July 13, Friday

It was moved to postpone the question on the clause of the first resolution
relating to the origin of money bills to take up the suffrage of the senate.

Gerry moved to amend the last clause agreed to by the committee to read "that
from the first meeting of the legislature of the US till a census shall be
taken all moneys to be raised for supplying the public treasury by direct
taxation, shall be assessed on the inhabitants of the several states according
to the number of their representatives respectively in the first branch"(I,
600-1) according to the general principle that taxation and representation
ought to go together.

Williamson worried that New Hampshire would complain.  They had allotted her a
liberal three representatives so that she would not feel disadvantaged by her
absence.  Since her delegates were still not present, she had no opportunity
to decide whether she would prefer the greater representation or the lesser
taxes.  Her representation ought to be reduced from two to three before taking
a vote on Gerry's motion.

Read did not approve.  He had wondered why some of the members of large states
had declined to take their full allotment of representatives.  He supposed now
that was because they did not want to pay their just proportion of taxes.  He
had no objection if the numbers were readjusted to take into account
representation and taxation.

G. Morris and Madison justified the choices they had made.  Morris said that
he thought eight representatives was a just portion for Pennsylvania, and he
could not in good conscience ask for more.  Madison stated that since he
thought the main difference in interests in the US was between the north and
south, he had fought to add two representatives to the south -- one to North
Carolina and one to South Carolina -- instead of one only to Virginia.
Madison supported the motion because it moderated the views of the opponents
and advocates of counting blacks.

Ellsworth hoped the proposition would be withdrawn.  It entered too much
detail, and the matter had already been sufficiently settled.  Since fractions
could not be made to representation, the rule would be unjust until the first
census could be made.

Wilson, on the other hand, hoped the proposition would stand.  The rule is as
reasonable and just before a census as after.  The rule is not to be adjusted
to the number of inhabitants but to the number of representatives.

Sherman opposed the motion.  He thought the legislature should be given the
liberty to decide the matter.

Mason was apprehensive of the motion.  He doubted whether the current rule
would be just as if would be after the actual census.

Ellsworth and Sherman moved to postpone the question which was voted against.

Gerry's motion was then voted against.

Gerry saw that the loss of the question had turned on the proposal of
assessing direct taxes on the inhabitants of the states which might restrain
the legislature to a poll tax.  He amended his old motion to say "that from
the first meeting of the legislature of the US until a census shall be taken,
all moneys for supplying the public treasury by direct taxation shall be
raised from the several states according to the number of their
representatives respectively in the first branch."(I, 603)

The amended motion passed in the affirmative.

Randolph moved to remove the reference to "wealth" from the resolutions on the
principle of representation and to replace it according to the number of
whites and three-fifths of blacks.

G. Morris opposed the motion because he thought it was still incoherent.
Either the blacks are inhabitants, and they should count equally with whites,
or they are property -- in which case "wealth" is the right word to use.  They
should not be counted as a fraction.  He thought the distinctions between
northern and southern interests were groundless, but he saw that the southern
delegates were intent on gaining a majority in the legislature.  The transfer
of such power from the maritime to the interior and landed interests will have
such a negative affect on commerce that he feel obligated now to vote for the
"vicious" principle of equal suffrage in the senate to protect the north
against it.

Either the distinction between the north and south is fictitious or real.  If
fictitious, then they should dismiss it and move on.  If real, then the north
and south should kindly separate.  "There can be no end of demands for
security if every particular interest is to be entitled to it."(I, 604)  Every
state and region of the US can claim it for their particular objects.  If the
southern states gain the power and join with the interior, they will
inevitably go to war with Spain over the Mississippi.  Having no property or
interest exposed to the sea, they will be little affected by such a war.  What
security do the eastern and northern states have against this danger?  It has
been said that Georgia and North and South Carolina will soon have the
majority of the population.  Everything is to be apprehended from them getting
power into their hands.

Butler said that the security the southern states are after is that no one
will take their slaves from them.  No one is saying those states will have
more people than the rest, only that they will have relatively more.  The
people and strength of the US is bearing southward and southwestward.

Wilson declared that all men everywhere have an equal right to representation.
If the interior becomes more populous than the coast, then the majority ought
to govern the minority.  Otherwise, they will separate as happened with
Britain and her colonies.  If population is not the right rule of
apportionment, then someone should point out a better one.  No one has.  He
could not agree that the protection of property was the primary aim of
government.  He thought the cultivation and improvement of the human mind was
the most noble aim.  With respect to the protection of human rights,
population is the best rule of representation, and property cannot vary much
from that.  He could not agree with allowing an equality of suffrage in the
senate on these grounds.

On the question to strike out "wealth" and make the changes moved by Randolph,
it passed in the affirmative.

The committee adjourned.

Jonathan Dayton writes to William Livingston, "I have the mortification to
inform your Excellency that, although we have been daily in convention, we
have not made the least progress in the business since you left us."(IV, 167)

Manasseh Cutler, a pastor in Philadelphia, recounts a tale about Ben Franklin
on this day:

> The Doctor showed me a curiosity he had just received, and with which he was
> much pleased.  It was a snake with two heads, preserved in a large vial.
> [...] The Doctor mentioned the situation of this snake, if it was traveling
> among bushes, and one head should choose to go one one side of the stem of a
> bush and the other head should prefer the other side, and that neither of
> the heads would consent to come back or give way to the other.  He was then
> going to mention a humorous matter that had that day taken place in
> convention, in consequence of his comparing the snake to America, for he
> seemed to forget that everything in convention was to be kept a profound
> secret; but the secrecy of convention matters was suggested to him, which
> stopped him, and deprived me of the story he was going to tell.(III, 59)

[I can only assume this analogy relates to the debates between the northern
and southern states.]
