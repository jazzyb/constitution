| Order | Author | Work | Notes |
|------:|--------|------|-------|
|  * | Farrand, Max | *The Records of the Federal Convention of 1787, Vol. I* | [farrand-1.md](notes/farrand-1.md)|
|  * | Farrand, Max | *The Records of the Federal Convention of 1787, Vol. II* | [farrand-2.md](notes/farrand-2.md)|
|  8 | Adams, John | [*Thoughts on Government*](http://oll.libertyfund.org/titles/adams-revolutionary-writings#lfAdams_label_096) | [adams-thoughts.md](notes/adams-thoughts.md) |
|  5 | Blackstone, William | *Commentaries on the Laws of England* | [blackstone-1.md](notes/blackstone-1.md) |
|  1 | Hobbs, Thomas | *Leviathan* | [hobbes-leviathan.md](notes/hobbes-leviathan.md) |
| 12 | Langguth, A. J. | *Patriots* | [langguth-patriots.md](notes/langguth-patriots.md) |
|  2 | Locke, John | *The Second Treatise of Government* | [locke-government.md](notes/locke-government.md) |
|  9 | Madison, James | [*Vices of the Political System of the United States*](http://teachingamericanhistory.org/library/document/vices-of-the-political-system/#doc-tabs-full) | [madison-vices.md](notes/madison-vices.md) |
| 11 | Miller, Nathan | *The Founding Finaglers* | [miller-finaglers.md](notes/miller-finaglers.md) |
|  3 | Montesquieu | *The Spirit of the Laws* | [montesquieu-spirit.md](notes/montesquieu-spirit.md) |
|  7 | Paine, Thomas | [*Common Sense*](http://www.ushistory.org/paine/commonsense/sense1.htm) | [paine-sense.md](notes/paine-sense.md) |
|  4 | Rousseau, Jean-Jacques | *The Social Contract* | [rousseau-contract.md](notes/rousseau-contract.md) |
| 13 | Van Cleve, George William | *We Have Not a Government* | [van-cleve-government.md](notes/van-cleve-government.md) |
| 10 | Washington, George | [*Notes on the Sentiments on the Government*](https://founders.archives.gov/documents/Washington/04-05-02-0154) | [washington-notes.md](notes/washington-notes.md) |
|  6 | Adams, Willi Paul | *The First American Constitutions* | [adams-constitutions.md](notes/adams-constitutions.md) |
