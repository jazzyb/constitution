[Thoughts on Government by John Adams](http://oll.libertyfund.org/titles/adams-revolutionary-writings)

Among the members of the Continental Congress, Adams was believed to be the
most knowledgeable about constitutional design.  They asked him how to frame
the constitutions of the new states.  Adams wrote *Thoughts on Government* as
a response to them as well as the proscriptions in Thomas Paine's *Common
Sense*.

* "the happiness of society is the end of government, as all divines and moral
  philosophers will agree that the happiness of the individual is the end of
  man. From this principle it will follow, that the form of government which
  communicates ease, comfort, security, or, in one word, happiness, to the
  greatest number of persons, and in the greatest degree, is the best"
* therefore, "virtue" is the best foundation for government; "fear" is the
  foundation of most governments
* list of political authors:   Sidney, Harrington, Locke, Milton, Nedham,
  Neville, Burnet, and Hoadly
* "Agree upon the number and qualifications of persons who shall have the
  benefit of choosing [representatives], or annex this privilege to the
  inhabitants of a certain extent of ground."
* the representative assembly should be "an exact portrait of the people at
  large"
* a people cannot be free in which all powers of government -- legislative,
  executive, or judicial -- are left in one body
  * a single assembly is liable to the same vices as a single individual
  * it will eventually vote itself perpetual as happened in Holland
  * an assembly is unfit to exercise executive power because it cannot
    accomidate the necessary executive properties of secrecy and dispatch
  * an assembly is too numerous, slow, and ignorant to wield judicial power
* if the legislature is a single house, then the executive and legislature
  will encroach upon one another until one has usurped all power
* the judicial could not mediate between the two because the legislature could
  always undermine it
* hence, this is why the executive ought to have veto power -- to prevent the
  encroachment of the legislature on the judicial
* the representatives of the people should appoint a "council" which would be
  an executive assembly with veto on the legislature
* the two bodies together choose the governor who also has veto power
* the governor ought to be elected annually to maintain his respect for the
  people and to teach humility, patience, and moderation
* other offices -- treasurer, attorney general, etc. -- should also be chosen
  annually by joint ballot of both houses
* "where annual elections end, there slavery begins"
* although, Adams mentions that the state legislatures might find that the
  terms should be enlarged to 3 years, 7 years, or life
* any 7 or 9 from the council may serve as a quorum to advise the governor
* governor should be commander-in-chief of the militia and armies
* governor and council should have the power of pardon
* judges and civil and military officers should be appointed by the governor
* ...unless you want to be more popular, in which case, they can be chosen by
  a joint ballot of both houses or one house with the approval of the other
* all officers should have commissions
* the terms of the judiciary ought to be for life to prevent them from being
  distracted by outside interests or making them dependent on any man
* all men should be trained with arms to be able to defend their country
  against sudden invasion
* smaller districts should be provided with public stocks of ammunition
* the government should spare no expense for public education
* the government should consider the introduction of sumptuary laws
  * see Montesquieu Book 7
* "A constitution founded on these principles introduces knowledge among the
  people, and inspires them with a conscious dignity becoming freemen; a
  general emulation takes place, which causes good humor, sociability, good
  manners, and good morals to be general. That elevation of sentiment inspired
  by such a government, makes the common people brave and enterprising. That
  ambition which is inspired by it makes them sober, industrious, and frugal.
  You will find among them some elegance, perhaps, but more solidity; a little
  pleasure, but a great deal of business; some politeness, but more civility."
* if there be a constitutional congress, it should contain a fair and adequate
  representation of the colonies, and its authority should be confined to:
  war, trade, interstate disputes, post-office, and the unappropriated
  western territories
