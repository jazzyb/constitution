# Commentaries on the Laws of England Book I:  Of the Rights of Persons

by William Blackstone

## Introduction

### Section 1: On the Study of the Law

* the study of english should be prefered over the study of the laws of rome or greece
* in britain the end of the law is "political or civil liberty"
* every man should know the laws to know the obligations society places him under
* gentlemen of liesure have no excuse not to know the law
* gentlemen have a duty to know the law; they are the ones who maintain order in society
* every professional is expected to enter into an apprentiship, "but every man of superior fortune thinks himself *born* a legislator"
* blames the passing of bad law on legislators who are ignorant of their own law
* the house of lords is the supreme court of appeal
* history of the teaching of common law in england
* lawyers should have a background in liberal studies; if they prioritize the mechanical practice of the law, then they are going about it backwards

### Section 2: Of the Nature of Laws in General

* laws are in general rules laid down by some superior being, e.g. the physical laws and God
* human law: "the precepts by which man...a creature endowed with both reason and freewill, is commsnded to make use of those faculties in the general regulation of his behavior"
* happiness can only be obtained by following heavenly justice
* that man should pursue his own happiness then is the foundation of natural law or ethics
* all human laws must derive from this natural law
* divine revelation is a part of natural law meant to overcome the weaknesses of our reason
* the inferior human law is created when equal individuals enter into society
* the law of nations is the law between societies
* "what reason has dictated to all men is called the law of nations"
* municiple law:
  * applies to everyone, not individuals
  * everyone is obligated to follow it - not advice or counsel
  * it is directed, not a contract that is only a promise
  * it covers civil obligations, not natural or revealed law
  * it must be publicly known
* the last invalidates any ex post facto law - the guilty party would have no way to know his action would become a crime
* there never existed the state of nature in which there was no society; the social contract had no formal origin; it *keeps* men together
* this implicit contract is that society should guard the rights of the individual, and the individual should obey the laws of society
* who shall rule?  the wise, good, and powerful
* the sovereign power is the one who makes the laws, the legislature
* good (virtuous): democracy
* wise: aristocracy
* powerful: monarchy
* Cicero: the best republic is that which is compounded from all three forms of government
* Tacitus: that such is a "visionary whim"
* law has 4 parts:
  * its declaration
  * the subjects it's directed towards
  * a means of righting the wronged party
  * a penalty
* how to interpret the law?
  * interogation of the legislature to explain particulars will not do
  * best method is to divine the intentions of the legislature when the rule was made through...
    * words
    * context
    * subject matter
    * effects/consequence
    * reason or spirit of the law

### Section 3: Of the Laws of England

* much of English law is derived from custom and memory of different peoples; like the language, the mixture makes the laws richer and more complete
* histories of which peoples English law derived from; no details
* many English laws - for example the style of courts and rules of inheritance - are not written down but are defined by custom, *common laws*
* decisions on common law are made by judges based on prior precedent set down in records
* judges should abide by precedent "to keep the scale of justice even and steady and not likely to waver with every new judge's opinion"
* if a judge finds a prior decision unjust or absurd, the prior sentence is considered *not law* instead of *bad law*
* a custom is good law if:
  * no one remembers the beginning of it; otherwise, an act of parliament would supercede it
  * it must have been continued without interuption
  * not subject to dispute, i.e. consented to
  * it must be reasonable - no good reason can be made against it
  * it must be explicit
  * " " compulsory
  * " " consistent with other customs
* "statutes" either declare the common law for good or remedy defects therein
* subsequent laws repeal their contrary predecessors in the case of negative statutes; however, if they can be done together they have concurrent jurisdicton
* if a statue which repeals a former is itself repealed, then the former is revived
* parliamentary acts which are unreasonable are void

### Section 4: Of the Countries Subject to the Laws of England

* "Thus were this brave people gradually conquered into the enjoyment of true liberty."
* how the laws of England came to rule over neighboring lands
* the common law of England has no bearing on the American colonies since the majority of them were founded on treaty or conquest
* they are under the control of parliament being subjects but not bound by the acts of parliament unless explicitly named
* colonial government
  * governor appointed by the king
  * courts of justice and appeal to the king in council
  * general assemblies = house of commons
  * council of state = upper house
* organization of English jurisdictions - not applicable to present research

## Chapter 1: Of the Absolute Rights of Individuals

* there are rights of persons and rights of things
* wrongs may be between individuals - civil injuries - or against general and public rights - crimes and misdemeanors
* the objects of English law fall within this fourfold division
* civil duties are included in rights
* corporations are artificial persons
* absolute rights belong to people as individuals - what they have in a state of nature
* relative rights belong to people as members of society
* private duties do not fall under the purview of the law, but public duties and absolute and relative rights do
* the primary end of the law is the maintanence and regulation of absolute rights; relative rights/duties are secondary
* his notions of society and nature align with Locke, etc.
* laws, whether consentual or not, which have a bad or indifferent (to common utility) end are destructive of liberty
* as soon as a slave steps on English soil he becomes a freeman
* tyranny better than anarchy - any government is better than none
* three primary rights
  1. right of personal security 
     * uninterupted enjoyment of body, health, life, and reputation
     * life and limb are so highly valued that killing is justified in selfdefense
     * a monk who enters a monestary is treated as if he is dead
     * any government who can kill indescriminantly is tyrannical
     * "No freeman shall in any way be ruined but by the lawful judgment of his peers or the law of the land."
  2. right of personal liberty
     * consists in the power of locomotion, i.e. a man may change his situation or go where he wants without restraint except due course of law
     * habeus corpus, see pp.91-2
     * there are times where the security of society requires a suspension of habeus corpus for a short, limited time
     * but the determination of suspension cannot be made by the executive - only the legislature
     * "exile...is a punishment unknown to the common law"
  3. right of private property
     * free use, enjoyment, and disposal of acquisitions without control except by law
     * even if taking someone's property is for the public good, only the legislature can make that determination
* protections for the maintanence of the primary rights
  1. constitution, powers, and privileges of Parliament
  2. limitations on the executive that he may not exceed the laws of the legislature
  3. application to the courts for a redress of injuries
  4. right to petition the king or parliament for a redress of injuries
  5. bearing arms for defense suitible to the person's condition
* any tyranny or opression must break one of these principles

## Chapter 2: Of the Parliament

* in tyranny the power to make and enforce the law rest in the same body
* historical precursors to 18th century parliament
* parliament may only be convened on the authority of the king
* without a single authority convening the assembly, if they could not agree on the time and place, how would anyone know which was the legitimate legislature
* by ancient statute the king convenes parliament ("frequently") at least once a year - officially once every three years at least
* three estates of parliament: lords temporal, lords spiritual, and the commons
* the executive should be a branch of the legislature but not the whole
* a union of the legislative and executive powers would be tyranny
* a total separation between the two would lead to the legislature usurping executive control
* the king's veto prevents encroachment
* "And herein indeed consists the true excellence of the English government, that all the parts of it form a mutual check upon each other."
* part of the mutual check between houses of parliament are their opposite interests
* lords spiritual are bishops
* lords temporal consists of all "peers" of the realm - dukes, barons, etc. - most are hereditary, except the scots who are elected
* ambition, rank, and honors is good in monarchies (tying personal interest to public good) though "dangerous or invidious" in republics
  * because it can be restrained by the king who grants the power
* the peers create a gradual scale of dignaty from peasant to prince "rising like a pyramid from a broad foundation"
* commons are the representatives of all men of property who have no seat among the lords
* 558 representatives in the house of commons
* though chosen from a particular district each represents the realm in general
* Edward Coke: the powers and jurisdiction of parliament are boundless
* Locke: supreme authority resting in the people, the people may remove or alter their government if it acts contrary to their trust
  * just in theory but in practice a disolution of government is a repeal of all good laws - any government is better than none
  * therefore, parliament is absolute and without control
  * footnote 8: Blackstone wanted to constrain radical interpretation of the Glorious Revolution
* because of its supremacy the members of parliament must not be incapable or improper to manage it
  * 21 years of age
  * oaths of alligiance
  * religious tests - cannot be catholic
  * natural born citizen - to prevent foreign influence
* if a matter arises concerning a house of parliament, only that house has the authority to adjudicate the matter
* parliament can make and unmake law and the judges cannot say otherwise
* the undefined jurisdiction of parliament is on purpose; if explicitly enumerated, the king could devise some new case to undermine parliament
* members of parliament have the following rights
  * freedom of speech
  * protection from arrest when on official business - does not apply to treason, felonies, or breach of the peace
* lords have the additional privilege of:
  * limited hunting in the king's forest if traveling to/from parliament
  * asking another peer to vote in his stead
  * writing their protest in the journals if a vote does not go their way
  * bills affecting the rights of peers may not be amended in the house of commons
* commons have the additional privilege of:
  * origin of money bills
    * theory: because only the people should have the right of taxing themselves
    * however, much property of the lords in practice is also taxed
    * real reason: because their authority is derived from the crown (rather than temporary election), the lords may be influenced by the crown to tax too much
    * lords cannot even amend, only reject
  * determining rules of suffrage and other election requirements
* qualifications of electors:
  * there are minimum property limits (a "freehold") because the poor would be too easily influenced
  * because a rich man may have property in different districts, he is entitled to cast one vote in each
  * 21 years of age
  * never found guilty of perjury
  * must have owned his freehold for a year before the election
  * only one voter per freehold
* qualifications of the elected:
  * property requirements for the same reason
  * native-born and at least 21
  * cannot be any of the 12 judges, clergy, or guilty of treason or felony
  * *ought* to be members of the district from which they are elected but in practice this is disregarded
  * not concerned with the administration of duties or taxes
  * no one with a pension under the crown
  * accepting an office under the crown voids the seat
* elections:
  * members are elected when parliament is summoned
  * undue influences (e.g. bribery) of electors are illegal
  * soldiers must stay at least 2 miles away
  * riots make elections void
* speaker of the house of commons may not give his opinions
* votes are public and pass with a majority
* private matters may be brought before the house
* parliamentary procedure looks very similar to rules of convention
* king must approve or veto bills
* king may end a session of parliament
* parliament is disolved on death of the king
* parliament must end after seven years (was three)

## Chapter 3: Of the King and His Title

* the hereditary succession of the monarch can be limited, changed, or eliminated by the supreme legislative authority of parliament (with the assent of the reigning monarch)
* monarchies can be hereditary or elective
* there is no such thing as "divine right" of king
* elective monarchy is the best suited to the rational principles of government
* but elective monarchy, like all elections, have a danger of influence, partiality, and artifice
* hereditary monarchy is more stable than elective [really???]
* modern Poland and Germany examples of danger of elective monarchies (see p.126 footnote 2)
* rules particular to English succession
* history of English succession beginning with Egbert im 800
* p.136 - the belief that Charles I was an elective prince and the chaos that followed "will be a standing argument in favor of hereditary monarchy to all future ages"
* however good elective monarchy looks in theory, it tends in practice to produce contention and anarchy

## Chapter 4: Of the King's Royal Family

* the queen is the "first and most considerable branch of the king's royal family"
* the queen has many rights - like buying property - that no other married woman can do
* she has separate courts, councils, and officers from the king
* under the law she is treated as a single woman because the king, burdened as he is with the responsibility of ruling a kingdom, should not have to also manage his wife's domestic affairs
* technically the queen is a subject but in terms of her personal security she is equal to the king, for example it is as much treason to plot her death as the king
* adultery is treason if the queen consort does it but not the king consort - a queen's affair causes doubt about heirs to the crown, not so for the king
* immediate family members who are not immediately in the line of succession have no other special rights other than having precedence before all peers and public officers

## Chapter 5: Of the Councils Belonging to the King
* peers cannot be arrested even when parliament is in session because they are always assumed to be assisting the king
* any peer may have an audience with the king
* privy council - personal advisors of the king - also may include commoners
* appointed on nomination of the king until he removes them
* seven articles of oath of office p.149
* their power is to investigate "all offices against the government"
* qualifications:
  * natural born subject of England
  * take oaths for security of government and church
* punishment for murder, assault, etc. against privy counsellor is death

## Chapter 6: Of the King's Duties
* "it being a maxim of the law that protection and subjection are reciprocal"
* the duty of the king is to govern his people by law
* quotes others who place the king under God and the law
* the laws of England belong to the people, and the king is bound to them by mutual consent
* the coronation oath is a contract between king and subjects

## Chapter 7: Of the King's Prerogative
* even though previous kings and queens held that their prerogative could not be questioned or discussed, that was never the true law of the land
* prerogatives: direct and incidental
  * direct: spring from the character of the office itself: declaring war and peace, etc.
  * incidental: always relate to something else and are merely exceptions in favor of the king: his debt shall be prefered before any of his subjects, etc.
* three kinds of direct prerogative: character (dignity), authority (power), and income (revenue)
* all of which are necessary to maintain the executive office
* checks and restrictions are necessary to prevent the prerogative from trampling on the liberties it is meant to protect
* the pomp of the royal majesty is necessary to distinguish the king's particular position from subjects - to allow him to better carry out his duties
* branches of the royal dignity
  1. soverneinty: subject to none but God
     * no court (even foreign ones) can have jurisdiction over him
     * no subject can oblige the king to fulfill a contract, but the king should voluntarily fulfill any lawful contract (or be persuaded to by his ministers)
     * the individual harm caused by a king breaking his promises is offset by the king having the sovereignty to protect the kingdom
     * ministers and counsellors may be punished if they give advice which causes the king to do wrong
     * to say that the king or parliament "can do no wrong" is to say that because there is no authority over them, there can be no redress; therefore it can't be called 'wrong'
     * however, if the king did something heinous and threatened the security of the realm, this would be considered an abdication of his authority and the throne
  2. perfection: the king can do or think no wrong; see above
     * if the king does something injurious, legally he must have been tricked into doing it - thus it is void
     * the king can't be late
     * he can never be considered a minor
  3. immortality: royals die, but the kingship never ceases
* the remaining powers of the king are placed in one person for "unanimity, strength, and dispatch" - to separate them or have them shared would make the office weaker
* "For civil liberty, rightly understood, consists  in  protecting  the rights  of  individuals  by  the  united  force  of society: society cannot be maintained, and of  course  can exert no protection, without  obedience to some sovereign power: and obedience is an empty name, if every individual has a right to decide how far he himself shall obey."
* the king is the representative of his people to foreign powers because a collective does not have the unanimity or strength to do so
* branches of royal power:
  1. the king has sole authority of sending and receiving ambassadors
     * ambassadors - representing their king who is not subject to any foreign law - are not subject to the private laws of the country they visit
     * they should be independent of any authority except those who sent them
     * this exemption does not extend to offenses against the laws of nature and reason, e.g. murder
     * Grotius: "the security of embassadors is  of  more  importance  than  the  punishment  of  a  particular  crime"
  2. to make treaties and alliances
     * such treaties must be made by the sovereign power
  3. to declare war and peace
     * as all individuals give up this prerogative when they enter into society, only the sovereign power may do this
     * Grotius: official declaration of war should procede conflict, to ensure that it is undertaken by the whole society and not private persons
     * declaring peace should be in the same hands that declare war
  4. issue letters of marque and reprisal of private foreign subjects acting without a declaration of war
     * this can be done by the ministers of the crown
  5. manage immigration
     * extends to the protection of foreign merchants even in times of war
* domestic prerogatives:
  1. veto power over the legislature
  2. commander-in-chief
     * sole power of raising and regulating fleets and armies
     * and erecting and manning forts
     * also extends to allowing subjects to leave the realm or export arms
  3. head of the judiciary
     * may erect courts of law
     * judges serve during good behavior
     * he is, since all offenses are against the king's peace or dignity, also the prosecutor
     * he derives his power to pardon from the same - the one offended should hold the power to forgive
     * but there is no right of pardon in parliamentary impeachments because in that case the commons are the prosecutors
     * a distinct judiciary is important
       * were it united with the legistature, a subject's liberty would be in the hands of arbitrary judges
       * were it united with the executive, (as happened with the star council) the law would be interpreted as what suited the king and his ministers best
     * prerogative to make proclamations - as long as they make no new law and only help to enforce the execution of existing laws
  4. confering (and disposing of) dignities and honors (offices)
     * he cannot create new offices with fees or add fees to old offices because this would produce a tax burden on the subject, and only parliament may tax
  5. arbiter of domestic commerce
     1. the establishment of markets (and their tolls)
     2. regulation of weights and measures - to produce uniformity in trade throughout the realm
     3. declare currency legal tender
  6. head of the church

## Chapter 8:  On the King's Revenue
* the king holds custody of the properties of the church of England
* he is entitled to its tithes although in practice they are held in a trust for the support of the clergy
* rents off lands
* profits from legal matters, fines, forfeitures, etc.
* shipwrecks are property of the king although this has been eased over time
* right to mines derived from his prerogative to mint coins
* WTF: "[waived goods], are goods stolen, and waived or thrown away by the thief in his flight, for fear of being apprehended. These are given to the king by the law, as a punishment upon the owner, for not himself pursuing the felon, and taking away his goods from him."
* "idiots" and "lunatics" are custody of the king
* 198 justification of taxes - some men engage in public concerns so that most can go about their private concerns; it only makes sense that some percentage of private gains go to support the security of those gains; but the taxes should not be outrageous
* 198 citizens give up a fraction of their property in order to secure the rest
* commons of parliament sets taxation (previously discussed on I. 181)
* taxes:
  * land (property) tax
  * malt tax
  * taxes on imports and exports
    * the customs are paid by the merchants who pass tge expense on to their customers
    * the benefit is that most citizens don't feel they are paying any tax
    * the downside is that it is easy to tax too highly and negatively affect trade
  * sales tax - the easiest to administer
  * tax on salt specifically
  * post office
    * history of the post office p.207
    * "for nothing but an exclusive right can support an office of this sort: many rival independent offices would only serve to ruin one another"
  * stamp duty - tax on paper
  * duty on homes, windows, and hearths
  * license of hackney coaches - taxes go to repairing the streets
  * duty on offices and pensions
* 209 begin discussion of national debt using "stocks" paying 5% interest
* the money exists only in idea in public faith
* *some* public debt is beneficial to trade for it increases the circulation of money
* *but* as is true in Britain, the debt may be so great that excesive taxes are needed to pay off the interest
* if the debt is owned by foreigners, they draw money out of the realm every year
* 211 "if the whole be owing to subjects only, it is then charging the active and industrious subject,  who  pays  his  share  of  the  taxes,  to  maintain the  indolent  and idle creditor who receives them"
* money is taken from the pockets of the industrious in the form of taxes and paid to the idle as interest on their debt

## Chapter 9: Of Subordinate Magistrates
* great officers of state include: lord treasurer, lord chamberlain, principle secretaries
  * not discussing any of the above
* will discuss: "sheriffs; coroners; justices of the peace; constables; surveyors of highways; and overseers of the poor"
* sheriff
  * acts as county executive and judge for cases of small claims
  * most sheriffs were democratically elected by the people of the shire
  * now appointed by judges and chosen by the king
  * one year terms - cannot serve again until 3 years after term
  * "For it would be highly unbecoming, that the executioners of justice should be also the judges; should impose, as well as levy, fines and amercements; should one day condemn a man to death, and personally execute him the next."
  * no sheriff's officer may practice as an attorney while they serve
  * officers: under-sheriff, bailiffs, gaolers
* coroner
  * has to do with "pleas of the crown" - principally judicial
  * chosen by the county freeholders
  * must be a knight or have sufficient lands for knighthood
  * ought to have sufficient property to uphold the dignity of his office and to be fined for any misdeeds while in office
  * used to be unable to take a salary or reward
  * king may remove him from office due to incapacity, insufficient property, "extortion, neglect, or misbehaviour"
  * sits in judgment of homicides
  * also handles who is to come in possession of shipwrecks
* justices of the peace
  * offices: keeper of the records of the county, lord chancellor, lord treasurer, lord high steward, lord marshall, lord high constable
  * may try felonies
  * appointed by the king
  * "contrary to these statutes, men of small substance had crept into the commission, whose poverty made them both covetous  and contemptible"
  * serve at the pleasure of the crown
  * the law is lenient toward their honest mistakes but severe toward any abuse
* constable
  * basically police
* surveyors of the highways
  * keep roads and bridges in repair
* overseers of the poor
  * collection of goods for those who can't work and employment of those who won't
* p. 233 "law of settlements"
* "surely they must be very deficient in sound policy, who suffer one half of a parish to continue idle, dissolute, and unemployed; and then form visionary schemes, and at length are amazed to find, that the industry of the other half is not able to maintain the whole"

## Chapter 10: Of the People, whether Aliens, Denizens, or Natives
* aliens and natural born
* subjects are bound to the king at birth - called natural allegiance
* natural allegiance cannot be discharged by the subject
* local allegiance is held by aliens to a domain and goes away as soon as they leave
* aliens may not purchase lands or estates for themselves
* alien subjects of enemy states have no rights in time of war except by the king's favor
* "so that all children, born out  of the king’s ligeance, whose  fathers were natural-born subjects, are now natural-born subjects themselves"
* "constitution of France differs from ours; for there, by their  *jus albinatus*, if a child be born of foreign parents, it is an alien"
* denizen - alien born but who has become an English subject - legally sits between the rights of subject and alien

## Chapter 11: Of the Clergy
* clergy do not have to serve on juries
* cannot serve in the offices of the previous chapter
* cannot sit in the Commons, farm, or trade
* the elections of bishops have a complicated history
* bishops can resign only to their superior; archbishops only to the king
* bishops council consists of a dean and chapter
* deans elected by the chapter
* parsons and vicars are beneath the dean
* all of the above have eclesiastical powers including holding court (bishop and dean)
* vicar is a secular office "to do divine service, to inform the people, and to keep hospitality"
* summary of rights of parson and vicar; more fully explained in vol. 2
* curate is the lowest ecclesiastical office

## Chapter 12: Of the Civil State
* the civil state are all men (nobles to peasants) who are neither clergy nor military
* the nobility (with the bishops) form the house of lords
* titles: duke, earl, baron, marquess, viscount
* duke
  * highest title after the royal family
* marquess is next
* earl after that [same as count???]
* then viscount
* barons
  * all nobility used to be barons in addition to their other titles
* who could sit in parliament used to be determined by holding land, e.g. a barony; now it is personal - "record of the writ of summons to them or their ancestors was admitted as a sufficient evidence of the tenure"
* nobility (also called peerage) can be granted as hereditary or non-hereditary
* "a nobleman shall be tried by his peers"
  * if the people judged them, they might not give them a fair trial due to envy
  * bishops in this case are not considered peers in this case
* peers cannot be arrested in civil cases
* peers swear "by their honor" rather than by oath in many cases *except as witnesses in trial*
* slander against peers is punished more severely than against commoners
* peers lose their nobility in death, degradation by the king, or act of parliament (which has happened only once)
* commoners are in law peers with one another in respect of their want of nobility
* first degree below the nobility is knight of the order of st. george
* then knight banneret, baronet, knights of the bath, knights bachelors, esquires, gentlemen, and finally yeoman
* everyone below them is a tradesman, artificer, or laborer

## Chapter 13: Of the Military and Maritime States
* "it is because he is a citizen, and would wish to continue so, that he makes himself for a while  a soldier. The laws therefore and constitution of these kingdoms know no such state as that of a perpetual standing soldier, bred  up to no other profession than that of war"
* officers of the military were long ago elected in the same manner as sheriffs
* the militias are under sole control of the king
* militias are chosen by lot every three years
* officered by the "principle landholders"
* they cannot be compelled to serve outside their county except in condition of invasion or rebellion
* an army formed in time of war is "only as temporary excrescences bred out of the distemper of the state, and not as any part of the permanent and perpetual laws of the kingdom"
* martial law is a necessity to be indulged in war but ought not to be allowed in peace time
* "the petition of right r  enacts, that no soldier shall be quartered on the subject without his own consent"
* "it was made one of the articles of the bill of rights, that the raising or keeping a standing army  within the kingdom in time of  peace, unless it be with consent  of parliament, is against law"
* England recently has kept a standing army in time of peace for fear of the other standing armies of Europe - their tenure is to be disbanded at the end of every year unless continued by parliament
  * "that any branch of the legislature may annually put an end to its legal existence, by refusing to concur in its continuance"
* the military should not be distinct from the people; its soldiers should:
  * be composed of natural subjects
  * enlisted for a short and limited time
  * live among the people - no separate camp, barracks, or "inland fortress"
  * citizens should be circulated among the soldiers
* laws governing troops shall be strict in war and relaxed in peace
* the king has an almost absolute legislative power over the military (unlike the navy, see below)
* draws on Montesquieu: "Hence have many free states, by departing from this rule, been endangered by the revolt of their slaves: while, in absolute and despotic governments where there no real liberty exists, and consequently no invidious comparisons can be formed, such incidents are extremely rare"
* free governments should observe two precautions:
  1. prevent the introduction of slavery
  2. if slavery already exists, do not arm the slaves
* the navy is "an army, from which, however strong and powerful, no danger can ever be apprehended to liberty"
* Blackstone owes the size and power of the English navy to navigation-acts, first framed in 1650, which required merchants to trade on English ships
* the power of the king is more limited over the navy than the army
  * the navy, being perpetual, needed parliament to pass permanent laws for their regulation; whereas the army is only temporary
a
## Chapter 14: Of Master and Servant
* slavery - an absolute, universal control over a servant - is contrary to reason and natural law
* argues against the "prisoner of war" justification as argued, e.g., by Locke
* the moment a slave touches English ground, he becomes free, and the law is obligated to protect his life and liberty
* if there is no term specified on the contract between a master and a domestic servant, then the law defaults to 1 year
* either the domestic servant or master must give a 3 month warning before termination
* apprentises are a special type of servant who serves a master with the expectation of education
* laborers serve only by the day or week, and there are laws to protect them from exploitation
* only those who have served as apprentises may ply their trade anywhere in England
  * opponents of the law claim this creates dangerous monopolies
  * supporters claim unskilled trade is just as detrimental to the public as monopolies
  * furthermore, it is good to employ youths, but no one undergo a seven year apprentiship if any unskilled person could engage in the practice
* called 'maintenance': "in general, it is an offence against public justice to encourage suits and animosities, by helping to bear the expense of them" - but masters may
* a master may be punished for the crime of a servant, "his negligence is a kind of implied consent to the robbery"
* if an apprentise while performing his work harms a customer's property through negligence, the master - *not* the servant - is answerable

## Chapter 15: Of Husband and Wife
* marrital crimes and customs (e.g., incest or divorce) are matters for the spiritual courts
* a marriage that has produced children through sex is indissoluable
* one of the ends of society is "to forbid a promiscuous intercourse"
* divorce allowed ~~in cases of "ill temper" or adultery~~ - it's more complicated than that
* husband and wife are considered a single individual under the law

## Chapter 16: Of Parent and Child
* "By begetting them therefore they have entered into a voluntary obligation, to endeavour, as far as in them lies, that the life which they have bestowed shall be supported and preserved."
* marriage is built on the natural obligation of the father to care for his children
* parents are obligated to bestow a proper education on their children
* anyone who educates their child in a catholic school must pay a fine and lose some of their rights
* parents have power over their children but "paternal power should consist in proper conduct, not in cruelty"
* the ceases when the child is 21
* disagrees with Montesquieu on the conditions under which a child does not have to care for their father; a child has such an obligation even for bad parents
* a bastard is a child *born* (not begotten) out of wedlock
* when in doubt "the presumption is in favor of legitimacy"
* bastards are not allowed to inherit being considered the sons of no one in some circles and sons of the people in others
* "A bastard may, lastly, be made legitimate, and capable of inheriting, by the transcendent power of an act of parliament, and not otherwise."

## Chapter 17: Of Guardian and Ward
* no heir shall be committed to the care of the person who is next in line to succeed
* with regards to committing crimes, 14 is considered an adult and subject to capital punishment; 7 or younger cannot; and between the two ages is the judgement of the court

## Chapter 18: Of Corporations
* it is in the public's interest that some individual rights be kept in "legal immortality" - artificial persons - corporations
* benefits of corporations
  * if property is bought by a mere voluntary body, it must be manually passed on after the death of the individuals
  * they may establish regulations for the whole that cannot be made in voluntary orgs
* romans invented the idea
* "corporations aggregate": many people (e.g. a university)
* "corporations sole": a single person and their succeeding heirs (e.g. the king)
  * "for the present incumbent, and his predecessor who lived seven centuries ago, are in law one and the same person"
* corporations are only formed by the king's consent - explicit or implied
* some corporations - e.g. city of London - are so old that the king's consent is implied
* explicit consent is done by act of parliament or charter
* corporations must have a name under which they will perform their business
* powers and rights
  * perpetuity
  * to act as a legal person under its corporate name
  * purchase and hold land
  * corporations are not bound by the private words or deeds of its individual members
  * to make by-laws to regulate the corporation
* aggregate corporations cannot commit felonies, act as a trustee, or be excomunicated
* agg corps must (with a few exceptions) have a head and cannot do business if the position is vacant
* no lands may be granted to a corp by will except for charitable uses
* on dissolution of corp, lands return to the one who sold or granted them to the corp
* corps may be dissolved by
  * act of parliament
  * death of all the members
  * voluntary surrender of franchises to the king
  * "By forfeiture of its charter, through negligence or abuse of its franchises"

