# The Social Contract

# Introduction
* not written by Rousseau but by the translator, Maurice Cranston
* Rousseau believed that the progress of society, technologically and
  politically, undermined men's souls.  Science, if it caused the abundance
  that the technological optimists hoped it would, would bring with it luxury
  which would undermine nations and men.
* Rousseau believed that the arts, in particular the theater, were morally
  harmful and that a healthy culture had no need of them.  This said by a man
  who penned a few popular ballets.
* The Swiss cantons of the time are the ideal form of democracy for Rousseau.
  Each town is a direct democracy where everyone has a voice in their own
  governance.  Rousseau sees the liberal progressive views of the
  enlightenment as a threat to such governance because they introduce the idea
  of "representation" which is hostile to the direct democracy he idolized.
  "Sovereignty" originates in the people, and it ought to stay there.
  Worth pointing out that Rousseau was born and raised in Switzerland until
  his late teens.
* Once men claim possession of property, covetousness and passions are
  inflamed.  Law and government is introduced to keep the order and maintain
  individual property.  The wealthy -- those with the most to lose -- are the
  driving force behind this development.
* In a way The Social Contract is an answer to Hobbes.  Rousseau agrees with
  Hobbes' theories about freedom vs. security, but he believes men can be free
  within political society and that it is only within civil society that men
  experience their truest freedom.
* Society *will* change men.  It can make them worse -- his criticism of many
  political governments -- or it can make them better -- help them become a
  social being in the fullest sense.
* Rousseau believes that the legislative process must be an act of direct
  democracy, but the administrative or executive function shouldn't be run by
  the whole body of citizens.  Rousseau thinks that leaving the executive
  function of government to everyone is too utopian.  When he says "democracy
  is for gods, not men", it is this latter sense that he means.
* Rousseau believes that the executive power will always encroach on the
  legislative.
* Rousseau's definition of "liberty" on p. 32.
* Rousseau's views versus Hobbes and Locke pp. 33 - 38
* Rousseau compared with Machiavelli w.r.t. religion p. 40

# Book I

## Ch. 1
* "Man was born free, and everywhere he is in chains."
* social order is a right that is the basis for all other rights

## Ch. 2
* the oldest and only natural society is the family
* the natural bond that unites the family only exists as long as the children
  need the protection of their parents -- afterwards, the only bond is choice
* the first natural law is to watch over one's own preservation
* starting at the age of reason, each man is his own master
* on Hobbes:  "These authors show us the human race divided into herds of
  cattle, each with a master who preserves it only in order to devour its
  members."

## Ch. 3
* there is no justification of rule by the "right of the strongest"
* how can one say that yielding to a superior force be a moral duty
  * it is merely necessity, at best prudence
* "All power comes from God, I agree; but so does every disease, and no one
  forbids us to summon a physician."
* obedience is owed only to legitimate powers

## Ch. 4
* legitimate authority is based on covenants
* to give up everything for nothing (slavery) is absurd
* for a whole people to do the same for a sovereign is madness
* no one can give up their children to slavery; no one can lose their liberty
  without their consent
* a right of slavery is sometimes justified by a right to kill the vanquished
  (I think Locke or Montesquieu might have spoken on this)
  * war does not regard personal relations
  * a state can only have as its enemy another state, not men
  * a prince has the right to kill defenders while they are armed, but once
    they lay down their arms, they are only private men, and no one has any
    longer the right to take their lives
  * war gives no more right to destruction than is necessary for victory
  * men have a right to kill only when they cannot enslave, so to derive
    enslavement from killing makes no sense
* the "right of slavery" is nonsensical; the words "right" and "slavery"
  cancel each other out

## Ch. 5
* there is a vast difference between master-slave relationships and
  ruler-subject relationships
* the law of majority-voting rests on a unanimous agreement to abide by such a
  system

## Ch. 6
* men leave the state of nature when they can no longer preserve themselves
  alone and must join together to pool their power
* the social pact:
  * each submits themselves to all
  * the submission is unconditional and no one has any personal rights to
    anything (???)
  * since everyone gives himself to all, no single person has claim to him; he
    retains all his rights and gains all the power of the rest
* this social pact was called a "city" in ancient times, now a "republic" or
  "body politic"
  * in its passive role it is called a "state"
  * in its active role, "sovereign"
  * when compared with others like it it is called a "power"
  * the individuals collectively are called a "people"
  * individually they are called "citizens" when the emphasis is on their
    sovereign power and "subjects" when they put themselves under the laws of
    the state

## Ch. 7
* each person in the social pact acts as a member of the sovereign body *and*
  as a subject in the state
* any public decision can impose an obligation on all the members
* the sovereign cannot engage in any pact with nullifies the original
  agreement
* since the sovereign is composed of the parts, it cannot have any interest
  contrary to any part of the body
* *but* each man has a private interest which may be different from his public
  interest as a citizen
* the injustice which would cause a man to seek all the rights of the citizen
  without performing the duties of a subject will ruin the body politic
* therefore it is implied in the social pact that anyone who refuses to obey
  the general will, shall be compelled to do so by the whole

## Ch. 8
* while a man surrenders some of the advantages of his state of nature, his
  gains in the social pact far out-weigh the losses
* joining with civil society creates "moral freedom":  "for to be governed by
  appetite alone is slavery, while obedience to a law one proscribes to
  oneself is freedom"

## Ch. 9
* public possession is more secure than private possession
* every man has a natural right to what he needs
* a person has "right of the first occupant" if...
  1. the land is not already inhabited
  2. the claimant occupies no more than is necessary for subsistence
  3. that the claimant actually works and cultivates the land
* the public land of the state derives from the private land of the subjects
* the social law replaces physical inequality with moral and legal equality

# Book II

## Ch. 1
* sovereignty cannot be alienated -- power can be delegated, but the will
  cannot
* silence of the people to a leader is the same as consent

## Ch. 2
* sovereignty is indivisible
* the duty of the sovereign is to make law
* declaring war, e.g., is not an act of sovereignty because it does not make
  law -- it is only an *application* of the law
* "The truth brings no man a fortune."

## Ch. 3
* the general will always tends towards the common good, but the action do not
  necessarily -- we always *want* what is advantageous even though we might
  not be able to discern it
* the general will is the balance (all pluses and minuses taken into account)
  of all the private wills
* if sectional groups form into political groups within the state, the result
  will tend away from the general will because the aggregation will represent
  only the combination of the group wills and not those of the people
* if one of these sectional groups takes a majority power, then the private
  will of that group will take the place of the general will
* the best condition is that such sectional groups do not form; however, if
  they do, the more there are, the better

## Ch. 4
* in making the social pact, the individual gives up only those rights,
  powers, and possessions that are the concern of the community, but *only the
  sovereign can determine what the community does and doesn't need*
* if the sovereign needs the service of a subject, that subject owes his
  service if the sovereign demands it
* *but* the sovereign must not impose any burden on its subjects which is not
  necessary
* these commitments are obligatory only because they are mutual
* we have the following conundrum:
  * a conflict arises that has not been settled by a prior general agreement
  * it is contentious because different interests are set against one another
* it is not at all clear who could be the arbiter to settle it
* it would also be absurd to rely on an express decision of the general will
  for such a decision could only be in the favor of one party and the other
  would consider it an alien and unjust will
* (Rousseau doesn't seem to clearly solve this circumstance.  I think he
  implies that because the sovereign deals with all equally, then we can trust
  the decision of the general will, but an answer is not clear to me.)

## Ch. 5
* if no one has the right to take their own life, how can the sovereign be
  granted that right -- i.e. death penalty?
* no one would say a man commits suicide if he sets out in a storm and dies
  when he knew the danger upon embarking
* any man may be asked to give his life to preserve the state
* thus, "it is in order to avoid becoming a victim of a murderer that one
  consents to die if one becomes a murderer oneself
* furthermore, by violating the law, the individual loses his membership in
  the society and becomes at war with it
  * (Bullshit.  First, Rousseau has already said that the state cannot be at
    war with individuals, only other states.  Second, if violating *any* law
    removes one's membership to the state, then the violation of *any* law can
    be punished with the harshest penalty.)
* no man should be put to death if he can be made to live without being a
  danger to society
* the ability to pardon should reside in the sovereign

## Ch. 6
* universal justice springs from reason, but it must be reciprocal; without
  some compulsion to obey, the law only harms the just who will obey it and
  helps the unlawful who will not
* an act of law is a rule which is as general as the will which makes it
* the laws applies to subjects in the abstract, not in particular
* the law may establish classes of citizens -- for example, a king and a
  hereditary monarchy -- but it cannot say who specifically that monarchy is
* any state ruled by law is called a "republic"
* any legitimate government is "republican"
* laws are the conditions on which civil society exists
* the public must be enlightened and taught how to judge rightly
  * this requires a "lawgiver"

## Ch. 7
* to discover the best rules best suited to a society would require a superior
  intelligence, a god
* (quoting Montesquieu:) "it is the leaders of the republic who shape the
  institutions but afterwards it is the institutions which shape the leaders
  of the republic"
* whoever sets out to create a society must set out to change human nature, to
  transform individuals into members of a whole
* the highest perfection of law-making is when each citizen can do nothing
  except through cooperation with others and the power of the whole is greater
  than the sum of the natural, individual powers
* the office of the lawgiver is not a part of the government nor of the
  sovereign:  "This office which gives the republic its constitution has no
  place in that constitution."
* the lawgiver may not be understood by the people at first because they have
  not yet been molded by the law into people who can appreciate the laws
* thus, the lawgiver may have to attribute his own wisdom to God in order to
  persuade the people

## Ch. 8
* the lawgiver does not start by laying down the best laws but by finding out
  what good laws the people can support
* "Nations, like men, are teachable only in their youth; with age they become
  incorrigible.  Once customs are established and prejudices rooted, reform is
  a dangerous and fruitless enterprise; a people cannot bear to see its evils
  touched, even if only to be eradicated; it is like a stupid, pusillanimous
  invalid who trembles at the sight of a physician."
* there is a time of maturity that nations require before they can be subject
  to law; during this time they do not need a lawgiver, they need a *master*

## Ch. 9
* there are limits to the size a state can be -- too large and it cannot be
  well-governed, too small and it cannot maintain itself
* government becomes more burdensome as it grows because the people must
  support the growing bureaucracy -- clerks rule the state
* the same laws and governments will not equally suit different peoples
* but misunderstanding arises if different people have different laws under
  the same governor
* the state can only be preserved by establishing an equilibrium of power with
  its neighbors

## Ch. 10
* there must be an appropriate ratio between the number of men and the amount
  of land which can sustain them
* the state must be instituted in a period of "peace and plenty"
* tyrants always choose troubled times to enact laws which the public would
  have never allowed if passions were cooler
* the people who are fit to receive laws are a people who are already united
  and can defend their freedom, but who do not already have law or some deep,
  old customs or superstitions

## Ch. 11
* the goal of all law ought to be *freedom* and *equality*
* "equality" does not mean that power and wealth should be equally
  distributed but that power should stop short of violence except that
  authorized by law and that wealth should never be so great to enable one to
  buy another nor poverty so great that one is forced to sell oneself to
  another
* just as circumstance works to destroy equality, legislation ought to work to
  preserve it
* of course, these general laws must be suited to the circumstance of the
  people
* the natural instincts of the people should be in harmony with the laws; if
  there is a great difference between them, the laws will be undermined and
  the state will eventually perish

## Ch. 12
* four kinds of law:
  1. political law:  the form of the government, its operation
  2. civil law:  relations between individuals in the society
     * these should be as limited as possible so that while indepenedent, each
       person is as dependent on the republic as possible
  3. criminal laws
  4. morals, customs, and a belief in the republic

# Book III

## Ch. 1
* like a man has both a mental will and physical ability, a government has a
  legislative power and an executive power
* legislative power belongs to the sovereignty of the people
* government is this executive power:  "An intermediary body established
  between the subjects and the sovereign for their mutual communication, a
  body charged with the execution of the laws and the maintenance of freedom,
  both civil and political."
* individuals in government are called "magistrates", "kings", or "governors"
* the whole is called "prince"
* "government" or "supreme administration" is the legitimate exercise of
  executive power
* the sovereign has authority over the government
* each part of the state needs to perform its duty
  * the governor governs
  * the sovereign legislates
  * the subjects obey
* no matter how many subjects there are, the law applies equally to each one;
  however, if the number of citizens increase, the share each one has in
  sovereignty decreases; "Whence it follows that the more the state is
  enlarged, the more freedom is diminished."
* the more power the executive needs to control, the more power the sovereign
  (legislature) needs to control the executive
* the prince should be nothing more than the general will concentrated through
  it

## Ch. 2
* the more numerous the magistrates, the weaker the government because the
  more force the government must exert over its own members
* the magistrate has three wills:
  1. their personal, private will
  2. their collective will with the rest of the magistrates together
  3. the general will of the sovereign which he is called to execute
* in a perfect system, #1 will be non-existent and #3 will be dominant
* the more people who are responsible for executing the law, the slower the
  business of executing the law becomes
* the more people in the state, the greater power the government must have
* therefore, the number of magistrates should be inverse to the population --
  the more people, the fewer magistrates and vice versa
* the art of the lawgiver is to find the right ratio of power to general will

## Ch. 3
* "democracy":  when each citizen is a magistrate or there are more
  magistrates than private citizens
* "aristocracy":  when there are fewer magistrates than private citizens
* "monarchy":  when there is only one magistrate from whom any others will
  derive their power
* there exist overlaps and mixed forms of all of the above
* democracies tend to suit small states and monarchies large ones

## Ch. 4
* it is not good for the one who makes the law should execute it
* there has never been and never can be a true democracy -- it goes against
  nature for the many to govern the few
* when the functions of government are divided between several groups (as they
  must be in democratic magistracy), those with the fewest members will
  acquire the greatest authority due to the greater facility dispatching
  business
* democracy requires:
  1. a small state such that each citizen knows the others
  2. simple manners and morals
  3. large measure of equality in social rank and wealth
  4. little to no luxury
* (p. 113 -- "celebrated author" == Montesquieu ???)
* democracy is the most difficult government to maintain

## Ch. 5
* three types of aristocracy:
  1. natural -- suited only to primitive peoples
  2. elective -- best
  3. hereditary -- worst
* the law should be careful to regulate the election of magistrates;
  otherwise, it will degrade into hereditary aristocracy
* foreigners will have more respect for venerable senators than an unknown and
  despised multitude
* there is a danger in aristocracy for the corporate will to diverge from the
  general and for part of the executive power to escape the control of the law
* equality of wealth is unlikely under this government
* aristocracy calls for fewer virtues, but some virtues are still necessary:
  moderation on the part of the aristocrats and contentment on the part of the
  poor
* administration should be given to those who can best give all their time to
  it; occasionally, someone not rich should be chosen for the office to remind
  that merit is preferred over riches

## Ch. 6
* monarchy has greater power, but that power is more easily corrupted against
  the sovereign
* the personal interest of the monarch is that the people are so weak as to
  never be able to resist them
* "Machiavelli's *Prince* is a handbook for republicans."
* monarchy is suited only to large states
* and yet it is difficult for a large state to be governed by a single man; a
  danger of this system is that the king will be forced to rule through too
  many deputies
* another danger:  it is rare for a good mind to rise to the level of monarch
* the interval between kings can be very dangerous when people vie for control
* this is why kings are more like to have hereditary succession rather than
  elected
* "When someone is brought up to command others, everything conspires to rob
  him of justice and reason."
* mastery of ruling is better learned by practice obeying rather than
  commanding
* monarchies result in instability

## Ch. 7
* no government is purely as simple as those just discussed; there is always
  some mixture of types
* when the ratio of prince to sovereign is greater than that of people to
  prince, the government must be divided to reduce power against the sovereign
* the same disadvantage can be prevented by establishing magistrates separate
  from the government between the prince and sovereign to balance the powers
  -- in this case government is not mixed but "tempered"

## Ch. 8
* agreement with Montesquieu:  not every people is suited to freedom
* the civil state can only survive if the people produce more than they
  consume
* the greater the distance between the people and the government the greater
  the taxes
* the greater the surplus of resources produced by the state, the more a
  monarch is needed whose luxury can consume the surplus -- it is better that
  the surplus be absorbed into the government than by private persons --
  otherwise, they risk revolution
* (lots of agreement and disagreement with Montesquieu w.r.t. the differences
  climate makes on government)

## Ch. 9
* the object of any political association is the protection and prosperity of
  its members
* the surest measure of this quality is the growth of their population --
  excluding immigration
* growth inclines towards the best government -- where the people diminish
  towards the worst

## Ch. 10
* the prince always tries to exert absolute power over the sovereign
* this destroys all political bodies
* government degenerates when (1) the government contracts or (2) the state
  dissolves
* contracts: moves from democracy to aristocracy or aristocracy to monarchy
* government never changes its form unless it becomes too weak to maintain the
  original form
* dissolution of the state takes place, first, when the prince usurps the
  power of the sovereign
  * in this case the social pact is broken and no subject is any more morally
    compelled to obey
* " " " " " ", second, when a single magistrate usurps the entire power of the
  government for himself
* the dissolution of the state and abuse of government is called "anarchy"
* three specific forms of anarchy:
  * democracy becomes "ochlocracy"
  * aristocracy becomes "oligarchy"
  * monarchy becomes "tyranny"
* a tyrant is one who executes power without having any right to it -- this
  term can be applied to good and bad princes
* a usurper of royal authority is called "tyrant"
* a usurper of the sovereign is called "despot" -- one who puts himself above
  the law
* a tyrant need not be a despot, but a despot is always a tyrant

## Ch. 11
* death of the body politic is inevitable
* the legislature is like the heart and the executive like the brain
* an imbecile can survive, but once the heart stops, the body dies
* a state continues through the power of the legislature, not the laws
  themselves for the laws will change with time
* laws grow in strength over time -- age makes them every day more revered
* if the laws grow weaker over time, this demonstrates that there is no more
  legislative power

## Ch. 12
* the people must assemble to maintain the sovereign power
  * (I think I interpreted this short chapter correctly.)

## Ch. 13
* they must assemble regularly to maintain the legal power
* it is always an evil to unite several towns into a single nation
* how are citizens to always be ready to assemble?
  * the territory should be evenly populated and everyone should have equal
    rights

## Ch. 14
* once a people is lawfully assembled, the executive power is suspended
* princes naturally oppose this power and hence the death of republics
* the next chapter is about an intermediate power which may be interposed
  between these two powers

## Ch. 15
* when public service ceases to be the concern of citizens and they'd rather
  pay with their purse than their person, the state has already begun to decay
* in a genuinely free state, citizens fulfill all obligations with their
  hands and none with their money
* "finance" is the word of a slave; it is unknown in republics
* taxation is more contrary to liberty than compulsory service
* the better the government, the less private business
* under bad government, no one wants to assemble because they do not believe
  the general will will be supported and they have too many domestic affairs
  to absorb their attention
* the people cannot have representatives -- they do not represent the general
  will and any laws they put in place are void
* for example, england is free only while electing their parliamentary
  representatives; once the representatives are elected the people are
  re-enslaved; "In the brief moments of its freedom, the English people makes
  such a use of that freedom that it deserves to lose it."

## Ch. 16
* the institution of government -- the separation of powers between the
  sovereign and the prince -- is not a contract
* a contract between the sovereign and a magistrate would be particular and
  thus could not be legitimate law
* being a contract, it would only be subject to natural law and thus contrary
  to a civil state
* there is only one contract -- that of the original association itself

## Ch. 17
* government is instituted in this way:
  1. sovereign establishes a government of a certain form
  2. the people name the magistrates who will enact the law

## Ch. 18
* the establishment of hereditary monarchy is simply a provisional form until
  the sovereign decides to arrange the government differently
* however, changes to the form of government are always dangerous
* the periodic assemblies are the means to prevent or postpone the usurpation
  of the sovereign power
* every assembly should open with the following motions:
  1. "Does it please the sovereign to maintain the present form of
     government?"
  2. "Does it please the people to leave the administration to those at
     present charged with it?"
* no law is above being revoked -- even the social pact itself
* any member may renounce his membership, recover his natural liberty, take
  his goods, and withdraw from the country
  * with the exception of doing so to evade a legal obligation

# Book IV

## Ch. 1
* the simpler the people and their form of government, the more peaceful,
  united, and equal they will be
* such a government needs few laws, and any new laws that are introduced are
  "felt" by the body as a whole and therefore no need to play politics
* when social ties slacken and personal interests exert an influence over the
  general will, then the state is corrupted; private interests are decreed in
  the guise of laws
* the general will cannot be destroyed, but other interests can shout over it

## Ch. 2
* the greater the harmony and unanimity in assemblies, the closer the
  sovereign follows the general will
* long debates, dissensions, and disturbances belie personal interests and the
  decline of the state
* however, unanimity reappears at the other extreme:  the people become
  apathetic and lose their will to deliberate; flattery or fear change voting
  into worship or cursing
* the majority vote binds the rest
* by agreeing to the social pact, the citizen obliges himself to obey all
  laws, even those he does not agree with because all laws represent the
  general will
* by definition:  majority opinion == general will
  * presupposes that the general will is present in the majority; if this is
    not the case, then all freedom has been lost
* to pass a law one doesn't necessarily need a strict majority
  * for decisions that are more important, one might want to fix some
    percentage of the vote that is required to pass the motion
  * but for decisions that are expedient, a single vote majority is sufficient

## Ch. 3
* on the elections of the prince and magistrates
* in a democracy elections should be chosen by lots because each citizen has
  an equal power in the government
* in an aristocracy election by choice -- voting -- is appropriate
* neither election has a place in monarchies where the prince appoints his own
  magistrates

## Ch. 4
* on the organization of government and voting in ancient rome
* the romans preferred the country to the city, and the greatest political
  power was drawn from among the country tribes
* (This chapter is similar to the sort you find in Montesquieu.  Good
  information but probably too specific to Roman history to be applicable to
  American political philosophy.)

## Ch. 5
* "tribunate":
  * keeps the balance of power between the executive and legislative
  * (sounds like they have veto power over both the legislature and executive ???)
* the tribunate becomes tyrannical when it either usurps the power of the
  executive or makes laws in place of the sovereign
* its power is weakened by the multiplication of its members
* the best method of keeping the tribunate in line is to appoint times in
  which its power is suspended

## Ch. 6
* there should exist some way of suspending the laws in times of immediate
  danger to the continuance of the state -- a "dictatorship"
* if the activity of the government is sufficient to counteract the danger,
  then one or two magistrates should be given this power
* if the law itself an obstacle to evading danger, then a single person should
  be given the right to temporarily suspend the laws and the sovereign
* as soon as the urgent need is over, the dictatorship becomes tyrannical or
  useless

## Ch. 7
* legislation gives birth to morals -- when legislation weakens, then morals
  degenerate
* a censorial office preserves the morals of the people with decrees
* general will is exercised with the law, public opinion/judgment is exercised
  by the censors

## Ch. 8
* the heads of the first states were not men but gods
* national divisions produced polytheism, polytheism produced religious and
  civil intolerance
* in these times the affairs of the state were bound with the affairs of the
  state religion
* Jesus established a religion which separated the two worlds
* the persecution of Christians was due to the distrust of this new sect,
  thinking that they were just biding their time in secret before overthrowing
  everyone else
* (Rousseau says:) Hobbes wished to recombine the head of state with the head
  of religion, but Christianity by its nature does not suit this combination
* types or ways of religion:
  * religion of the man:  a personal relationship with God
  * religion of the citizen:  the religion of the state
  * religion of the priest:  a religion which sets one at odds with two
    contradictory obligations
    * (I don't understand this last one.)
* the second equates obedience to the laws with obedience to god, love of
  country with divine love
  * however, this can make men superstitious and murderously intolerant
* the first lets believers look at all others regardless of nationality as
  brothers
  * however, this lacks the bonds to hold a society together; the followers
    are detached from ties to all worldy things including the state
* a society of Christians would be a perfect society in many ways -- they
  would obey the laws, rule justly, etc. -- but they could not be a society of
  men
* one hypocrite could take advantage of the whole system
* (I think Rousseau caricatures Christians in this system as too gullible and
  peace-loving.  Remember Matthew 10:16:  "Behold, I send you forth as sheep
  in the midst of wolves: be ye therefore wise as serpents, and harmless as
  doves.")
* the interest of the sovereign in a citizen's religion must only go to the
  point that the dogma maintains the social order -- beyond that point it is
  none of his business
* intolerance has no place in a civil religion

## Ch. 9
* (Rousseau expresses his desire to continue speaking on other political
  matters in other books, but this he never continued.)
