# Article I

- 1.2.4:
  > When vacancies happen in the Representation from any State, the Executive
  > Authority thereof shall issue Writs of Election to fill such Vacancies.

  * First mentioned by Committee of Detail (CoD) II-140, etc.
  * Agreed to without debate II-231 (August 9).
  * No mention of such a power in the Articles of Confederation (AoC).
  * Story: p. 148-150

- 1.2.5:
  > The House of Representatives shall chuse their Speaker and other Officers;
  > and shall have the sole Power of Impeachment.

  * First mentioned by CoD II-136, etc.
  * Agreed to without debate II-231 (August 9).
  * Story: p. 150-172

- 1.3.5:
  > The Senate shall chuse their other Officers, and also a President pro
  > tempore, in the absence of the Vice President, or when he shall exercise
  > the Office of President of the United States.

  * Choosing of officers first mentioned by CoD II-155, etc.
  * Agreed to without debate II-239 (August 9).
  * A latter committee added the bit about the president pro tempore between
    Aug. 31 and September 3. (II-498)
  * And that was agreed to without debate on Sept. 7 (II-538)
  * Story: p. 213-214

- 1.3.7:
  > Judgment in Cases of Impeachment shall not extend further than to removal
  > from Office, and disqualification to hold and enjoy any Office of honor,
  > Trust or Profit under the United States: but the Party convicted shall
  > nevertheless be liable and subject to Indictment, Trial, Judgment and
  > Punishment, according to Law.

  * First mentioned by CoD II-173, etc.
  * Agreed to without debate II-438 (August 28)
  * Move that impeached persons be suspended and debate II-612 (September 14)
  * Mason mentions on August 6 (IV-210) that "no mode of impeaching the judges
    is established"
  * Story: p. 250-255

- 1.8.5:
  > To coin Money, regulate the Value thereof, and of foreign Coin, and fix
  > the Standard of Weights and Measures;

  * First mentioned by CoD II-136, etc.
  * All parts of the clause agreed to without debate II-308 (August 16)
  * Story III: p. 16-21

- 1.8.8:
  > To promote the Progress of Science and useful Arts, by securing for
  > limited Times to Authors and Inventors the exclusive Right to their
  > respective Writings and Discoveries;

  * Madison and Pinckney moves to grant to Congress powers over copyrights and
    patents, respectively on August 18 (II-325)
  * These motions were combined by a committee into the clause above and
    accepted without debate on September 5 (II-509)
  * Story III: p. 48-51

- 1.8.13:
  > To provide and maintain a Navy;

  * CoD (II-158): Congress "shall have the exclusive power ... of equipping a
    navy"
  * CoD (II-168: "...to build and equip fleets"
  * changed to "to provide and maintain a navy" on August 18 and agreed to
    without debate (II-330)
  * Story III: p. 76-79
    * debated in the state conventions: "2 Elliot's Deb. 224, 319, 320"
    * (3 Elliot 428-30 ??? (and possibly 309 and 320))
    * (77-79 Randolph defends the ratification of the Constitution in part on
      Virginia's inability to maintain a fleet where the Union could)
    * defense of the provision: Federalist 11, 24, 41

- 1.8.14:
  > To make Rules for the Government and Regulation of the land and naval
  > Forces

  * only mention is II-330 (August 18)

- 1.8.15:
  > To provide for calling forth the Militia to execute the Laws of the Union,
  > suppress Insurrections and repel Invasions;

  * May 29 (I-21) Randolph introduces the Virginia resolutions; one of which
    is to empower Congress "to call forth the force of the Union against any
    member of the Union failing to fulfill its duty"
    * a similar resolution is also in the New Jersey Plan (I-245)
  * Committee of Detail presents the following wording on August 6: "To call
    forth the aid of the militia, in order to execute the laws of the Union,
    enforce treaties, suppress insurrections, and repel invasions" (II-182)
  * Morris amends the above without debate II-389-90
  * debates/justifications in the Virginia convention III-318-9
    * This references Elliot's 3, p. 378 and on; notably it leaves out Mason's
      argument against the resolution starting on the same page
  * Story III: 81-83
    * mentions of many reasons for rejecting the provision in Elliot's debates
    * Elliot 3, starting with Henry's comments on p. 410
    * [Q: Can Mason make Henry's arguments against the motion?]
    * Corbin:  "The honorable gentleman then urges an objection respecting the
      militia, who, he tells us, will be made the instruments of tyranny to
      deprive us of our liberty. Your militia, says he, will fight against
      you. Who are the militia? Are we not militia? Shall we fight against
      ourselves? No, sir; the idea is absurd." (Elliot's 3, p. 112)
    * Maclaine: "I would ask that gentleman who is so much afraid it will
      destroy our liberties, why he is not as much afraid of our state
      legislature; for they have much more power than we are now proposing to
      give this general government. They have an unlimited control over the
      purse and sword; yet no complaints are made. Why is he not as much
      afraid that our legislature will call out the militia to destroy our
      liberties? Will the militia be called out by the general government to
      enslave the people--to enslave their friends, their families,
      themselves? The idea of the militia being made use of, as an instrument
      to destroy our liberties, is almost too absurd to merit a refutation. It
      cannot be supposed that the representatives of our general government
      will be worse men than the members of our state government. Will we be
      such fools as to send our greatest rascals to the general government? We
      must be both fools as well as villains to do so." (Elliot's 4, p.63-4)
      * [Attribute this to Spaight since it is N.C.?]

- 1.8.18:
  > To make all Laws which shall be necessary and proper for carrying into
  > Execution the foregoing Powers, and all other Powers vested by this
  > Constitution in the Government of the United States, or in any Department
  > or Officer thereof.

  * first mentioned by CoD II-168
  * Madison and Pinckney move to amend with "and establish all offices", which
    is voted down (II-345); then the clause as a whole is approved (August 20)
  * Mason argues against in his notes II-640
  * Hamilton argues for it in Federalist XXXIII (III-239)
  * Story III, p. 109-126
    * Smith and Williams concerned about the power Elliot's 2, p. 330-4, etc.
      * p. 406 has their modified clause
    * Mason argues against Elliot's 3, p. 441, etc.
    * Nicholas argues for it and against a Bill of Rights on Elliot's 3, p. 245
    * Henry argues against it and for a Bill or Rights on Elliot's 3, p. 149
    * [Add arguments for and against a Bill of Rights (end of the Endorsement)
      to this section.]
    * Wilson Elliot's 2, p. 448, etc. argues for it, in particular an argument
      that freedom of the press is already implied by the Constitution
      * p. 468: "for, when it is said that Congress shall have power to make
	all laws which shall be necessary and proper, those words are limited
	and defined by the following, "for carrying into execution the
	foregoing powers." It is saying no more than that the powers we have
	already particularly given, shall be effectually carried into
	execution."

- 1.9.8:
  > No Title of Nobility shall be granted by the United States: And no Person
  > holding any Office of Profit or Trust under them, shall, without the
  > Consent of the Congress, accept of any present, Emolument, Office, or
  > Title, of any kind whatever, from any King, Prince or foreign state.

  * CoD II-169: "No state shall grant any title of nobility."
  * CoD wording passed without amendment or debate on August 23 (II-389)
  * Looks like the Committee of Style adds the second part of the clause
    (II-572)
  * Randolph explains the second part of the clause with an anecdote about
    Franklin III-327
    * ...and McHenry the first part III-150
  * Story III: p. 215-6

# Article II

- 2.1.4:
  > The Congress may determine the Time of chusing the Electors, and the Day
  > on which they shall give their Votes; which Day shall be the same
  > throughout the United States.

  * Brearly from the committee of eleven of 8/31 reports the following change
    to sect. 1 art. 10 of the CoD's draft:  "...The Legislature may determine
    the time of choosing and assembling the electors..." (Sept. 4 - II-494)
  * "but the election shall be on the same day throughout the U.S." added Sept. 6 (II-526)
  * Story III: p. 330-1
    * debated Elliot's 4, p. 104-6

- 2.1.5:
  > No person except a natural born Citizen, or a Citizen of the United
  > States, at the time of the Adoption of this Constitution, shall be
  > eligible to the Office of President; neither shall any Person be eligible
  > to that Office who shall not have attained to the Age of thirty-five
  > Years, and been fourteen Years a Resident within the United States.

  * Mason moves that the CoD develop "certain qualifications of landed
    property and citizenship" for officers of the national government (July
    26: p. II-121,134)
  * Gerry asks the CoD to "report proper qualifications for the President"
    (August 20: II-344)
  * Rutledge reports from committee that the President "be of the age of 35
    years, and a citizen of the U.S. and shall have been an inhabitant thereof
    for 21 years" (August 22: II-367)
  * Brearly reports from committee the qualifications as they are stated in
    the final clause
    * aggreed to w/o comment or debate on September 7 (II-536)
  * Story III: p. 332-4

- 2.1.6:
  > In Case of the Removal of the President from Office, or of his Death,
  > Resignation, or Inability to discharge the Powers and Duties of the said
  > Office, the same shall devolve on the Vice President, and the Congress may
  > by Law provide for the Case of Removal, Death, Resignation or Inability,
  > both of the President and Vice President, declaring what Officer shall
  > then act as President, and such Officer shall act accordingly, until the
  > Disability be removed, or a President shall be elected.

  * Hamilton first to suggest this?
    * "On the death, resignation, or removal of the [President] his
      authorities to be exercised by the President of the Senate till a
      successor be appointed." I-292
  * CoD: "The President of the Senate to succeed the Executive in case of
    vacancy." II-146
    * ditto: 172, 186
  * II-427: debate over whether the P. of the Senate should succeed the
    President, but the arguments assume Congress will elect
  * II-495: once the office of the Vice-President had been decided on (who
    would act in part as P. of the Senate), he became the logical successor
  * II-535: second part moved by Randolph + debate
  * minor edit II-599 fn. 23 and II-626
  * Story III, p. 334-7
    * E3-487-8:  Mason asks Madison to explain the clause
  * Article 1.3.4 debate is over the office of VP - should we link to that
    (footnote)?

- 2.1.8:
  > Before he enter on the Execution of his Office, he shall take the
  > following Oath or Affirmation: “I do solemnly swear (or affirm) that I
  > will faithfully execute the Office of President of the United States, and
  > will to the best of my Ability, preserve, protect and defend the
  > Constitution of the United States.”

  * p. 146 CoD: "shall swear fidelity to the Union... by taking an oath of
    office"
  * p. 185 initial oath as reported by CoD; up to "...Office of President of
    the United States [of America]"
  * p. 427 amended with "and will to the best of my judgment and power..."
  * p. 621 "judgment" changed to "ability"
  * Story III, p. 339

- 2.2.3:
  > The President shall have Power to fill up all Vacancies that may happen
  > during the Recess of the Senate, by granting Commissions which shall
  > expire at the End of their next Session.

  * Richard Spaight makes the motion on September 7 (II-540)
    * agreed to nem. con.
  * Story III, p. 409-12
    * seems to be a beef between Cato (5) and the Federalist (67) on the
      subject, but nothing in the state conventions

- 2.3:
  > He shall from time to time give to the Congress Information of the State
  > of the Union, and recommend to their Consideration such Measures as he
  > shall judge necessary and expedient; he may, on extraordinary Occasions,
  > convene both Houses, or either of them, and in Case of Disagreement
  > between them, with Respect to the Time of Adjournment, he may adjourn them
  > to such Time as he shall think proper; he shall receive Ambassadors and
  > other public Ministers; he shall take Care that the Laws be faithfully
  > executed, and shall Commission all the Officers of the United States.

  * let me try to break this down; the power consists of:
    1. state of the union
    2. convene Congress
    3. receive ambassadors
    4. faithfully execute the laws
    5. commission all officers of the U.S.
  * "with power to carry into effect the national laws" - II:23
    * approved nem. con.
  * first mention of state of the union - II:158
    * also "commission all officers"
    * also "convene the legislature"
  * CoD reports (8/6) #1, #2, #4, #5, #3 (II:185)
  * II:405 - minor amendment by Morris making the recommendations of the state
    of the union a duty and not a choice
  * "other public ministers" added to #3 - II:419
  * McHenry amends #2 to convene both or either house - II:553
  * Story III, p. 412-20

# Article IV

- 4.2.1:
  > The Citizens of each State shall be entitled to all Privileges and
  > Immunities of Citizens in the several States.

  * Mutual intercourse clause in the fourth Article of Confederation, II-135, story3-673
  * CoD introduces 8/6, II-187-8
  * only debate on the provision comes from Butler and Pinckney, who want it
    to explicitly protect property in slaves
    * Butler suggests a different article should be written to handle fugitive
      slaves, II-443
  * Story III, p. 673-5

- 4.2.2:
  > A Person charged in any State with Treason, Felony, or other Crime, who
  > shall flee from Justice, and be found in another State, shall on demand of
  > the executive Authority of the State from which he fled, be delivered up,
  > to be removed to the State having Jurisdiction of the Crime.

  * ditto 4.2.1 above
  * Story III, p. 675-6

# Article VI
- 6.2:
  > This Constitution, and the Laws of the United States which shall be made
  > in Pursuance thereof; and all Treaties made, or which shall be made, under
  > the Authority of the United States, shall be the supreme Law of the Land;
  > and the Judges in every State shall be bound thereby, any Thing in the
  > Constitution or Laws of any State to the contrary notwithstanding.

  * Move the congressional veto over state laws debate (currently p. 192-4) to
    here?
  * Paterson uses essentially the same wording above I-245
  * Lansing I-250 against veto (I don't think this is being used anywhere
    else)
  * Luther moves a variant of Paterson's wording, II:28-9, which is accepted
    nem con
  * Rutledge minor edit II:389
  * Mason objection II:639
  * Story III, p. 693-701
    * E4-265
    * E4-178
    * E3-266 (Mason)
