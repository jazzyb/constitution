Thesis:  The primary intent of the second amendment is to prevent the use of a
standing army.  The people are to be armed and trained to defend their liberty
and country so that a standing army need not be used.  The founders feared a
permanent military class of citizens would ultimately subvert (or be used to
subvert) the will of the people.

1. The [English Bill of Rights](https://www.legislation.gov.uk/aep/WillandMarSess2/1/2/introduction)
   declares among the rights and liberties of the people:
   * "That the raising or keeping a standing Army within the Kingdome in time
     of Peace unlesse it be with Consent of Parlyament is against Law."
   * "That the Subjects which are Protestants may have Arms for their Defence
     suitable to their Conditions and as allowed by Law."
   * Two actions by the previous king are given for the declaration of these
     rights:
     > Whereas the late King James the Second by the Assistance of diverse
     > evill Councellors Judges and Ministers imployed by him did endeavour to
     > subvert and extirpate the Protestant Religion and the Lawes and
     > Liberties of this Kingdome...
     >
     > By raising and keeping a Standing Army within this Kingdome in time of
     > Peace without Consent of Parlyament and Quartering Soldiers contrary to
     > Law.
     >
     > By causing severall good Subjects being Protestants to be disarmed at
     > the same time when Papists were both Armed and Imployed contrary to
     > Law.
2. Blackstone [Vol. I, p. 97] comments on this statute:
   > The fifth and last auxiliary right of the subject, that I shall at
   > present mention, is that of having arms for their defence, suitable to
   > their condition and degree, and such are allowed by law.  Which is also
   > declared by the same statute [above] and is indeed a public allowance,
   > under due restrictions, of the natural right of resistance and
   > self-preservation, when the sanctions of society and laws are found
   > insufficient to restrain the violence of oppression...
   >
   > And, lastly, to vindicate these rights, when actually violated or
   > attacked, the subjects of England are entitled, in the first place, to
   > the regular administration and free course of justice in the courts of
   > law; next to the right of petitioning the king and parliament for a
   > redress of grievances; and lastly to the right of having and using arms
   > for self-preservation and defence.
3. From the [Wikipedia article on the Glorious
   Revolution](https://en.wikipedia.org/wiki/Glorious_Revolution):  "The expansion of
   the military caused great concern, particularly in England and Scotland,
   where memories of the civil war left huge resistance to standing armies."
   * Reference: Childs, John (1987). The British Army of William III,
     1689-1702 (1990 ed.). Manchester University Press. ISBN 978-0719025525.
     p. 184
     * Note:  This is the third volume by Childs about the transformation of
       the British standing army in the late 17th century.  The other two
       volumes are "The Army of Charles II" and "The Army, James II, and the
       Glorious Revolution," respectively.
4. There are at least five state constitutions with a similar right prior to
   the U.S. Bill of Rights:
   * [Maryland](https://avalon.law.yale.edu/17th_century/ma02.asp):
     > XXV. That a well-regulated militia is the proper and natural defence of
     > a free government.
     >
     > XXVI. That standing armies are dangerous to liberty, and ought not to
     > be raised or kept up, without consent of the Legislature.
     >
     > XXVII. That in all cases, and at all times, the military ought to be
     > under strict subordination to and control of the civil power.
   * [Massachusetts](http://www.john-adams-heritage.com/text-of-the-massachusetts-constitution/):
     > Art. XVII. The people have a right to keep and to bear arms for the
     > common defence. And as, in time of peace, armies are dangerous to
     > liberty, they ought not to be maintained without the consent of the
     > legislature; and the military power shall always be held in an exact
     > subordination to the civil authority and be governed by it.
   * [North Carolina](https://avalon.law.yale.edu/18th_century/nc07.asp):
     > XVII. That the people have a right to bear arms, for the defence of the
     > State; and, as standing armies, in time of peace, are dangerous to
     > liberty, they ought not to be kept up; and that the military should be
     > kept under strict subordination to, and governed by, the civil power.
   * [Pennsylvania](https://www.law.gmu.edu/assets/files/academics/founders/Penn-Constitution.pdf):
     > XIII. That the people have a right to bear arms for the defence of
     > themselves and the state ; and as standing armies in the time of peace
     > are dangerous to liberty, they ought not to be kept up; And that the
     > military should be kept under strict subordination to, and governed by,
     > the civil power
   * [Virginia](https://constitution.org/1-Constitution/cons/early_state/virginia1776.html):
     > SEC. 13. That a well-regulated militia, composed of the body of the
     > people, trained to arms, is the proper, natural, and safe defence of a
     > free State; that standing armies, in time of peace, should be avoided,
     > as dangerous to liberty; and that in all cases the military should be
     > under strict subordination to, and governed by, the civil power.
5. Charles Pinckney (SC) on August 20, 1787 in the Constitutional Convention
   moves a number of proposals that look very much like the declarations of
   rights in many of the state constitutions.  Among the rights he mentions:
   * "No troops shall be kept up in time of peace but by consent of the
     legislature."
   * "The military shall always be subordinate to the civil power, and no
     grants of money shall be made by the legislature for supporting military
     land forces for more than one year at a time."
   * Farrand II, p. 341 (Pinckney mentions these motions, but not these above
     specifically, in Farrand III, p. 122.)
6. [Monday, August 17,
   1789](https://memory.loc.gov/cgi-bin/ampage?collId=llac&fileName=001/llac001.db&recNum=390)
   the House of Representatives debates the amendment proposed by Madison that
   would become the second amendment:
   * Original wording:
     > A well regulated militia, composed of the body of the people, being the
     > best security of a free state, the right of the people to keep and bear
     > arms shall not be infringed; but no person religiously scrupulous shall
     > be compelled to bear arms.
   * Gerry's interpretation of the amendment, p. 778:
     * "This declaration of rights, I take it, is intended to secure against
       the maladministration of the government."
     * "What, sir, is the use of a militia?  It is to prevent the use of a
       standing army, the bane of liberty."
     * "Whenever governments mean to invade the rights and liberties of the
       people, they always attempt to destroy the militia, as to make a
       standing army necessary."
     * (p. 780) "... objected to the first part of the clause, on account of
       the uncertainty with which it is expressed.  A well regulated militia
       being the best security of a free state, admitted an idea that a
       standing army was a secondary one."
   * Arguments against the amendment as worded focus on the last clause:
     * Gerry (778): That the government could disolve the militia by declaring
       who could or couldn't bear arms.
     * Jackson (779): Those who are scupulous should be able to pay for an
       equivalent.
     * Benson (779-80): The clause should be struck out and left to Congress.
   * Burke moves to add (but is lost):
     > A standing army of regular troops in time of peace is dangerous to
     > public liberty, and such shall not be raised or kept in time of peace
     > but from necessity, and for the security of the people, nor then
     > without the consent of two-thirds of the members present of both
     > Houses; and in all cases the military shall be subordinate to the
     > civilian authority.
7. Gouverneur Morris has this to say about the constitutional restrictions on
   the use of the militia in 1815 (Farrand III, p.420-1):
   > When, in framing the Constitution, we restricted so closely the power of
   > government over our fellow citizens of the militia, it was not because we
   > supposed there would ever be a Congress so mad as to attempt tyrannizing
   > over the people or militia, by the militia.  The danger we meant chiefly
   > to provide against was, the hazarding of the national safety by a
   > reliance on that expensive and inefficient force.
   * He goes on to defend the practice of keeping up a standing army.
8. Of course, see also the [Annotated Constitution
   article](https://constitution.congress.gov/browse/essay/amdt2-1/ALDE_00000408/)
   on the right.  (Has good references at the bottom.)
9. Worth checking out the book "The Second Amendment: A Biography" by Michael
   Waldman after our research.
10. Story's *Commentaries*, p. 746-747:  mostly a focus on the militia over
    standing armies
