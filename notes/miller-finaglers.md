* (p. 49) Robert Walpole (Prime Minister of England 1721 - 1742) had refused
  to tax the colonies under the belief that unrestricted trade would do more
  good for England than taxes would.  This was the precedent for Americans
  before the war.
* (p. 49) The English had just defeated the French and Indians and believed
  that the colonists should help foot the bill of their own defense.  The
  Americans felt differently since there was no more threats from the French
  or their Indian allies.  They did not want to help fund English troops on
  their soil and did not think they needed them.
* (p. 50) George Grenville started the taxes with the Acts of Trade and
  Navigation in order to make up the revenues.  This act and others at the
  time clamped down on smuggling -- a trade which had always been tacitly
  accepted by the crown and made men like John Hancock rich.
* (p. 53) Taxes were then imposed on top of the prohibition of smuggling.
* (p. 54) The corruption of British politics, particularly the buying of votes
  and influence, made many colonists at the time think that the crown was
  undermining their liberties.
* (p. 55) They feared that posts in the colonies (e.g. governorships) would be
  given to Englishmen looking to make a quick buck and would fleece the
  colonists.  Plus filling the posts with Englishmen would take the profession
  of graft out of the hands of Americans.
* The notes for Chapter III have some interesting titles to back up the
  previous bullets.
* (p. 56) Sam Adams ran Boston through the Caucus Club which, according to
  John Adams, decided who would win elections before the regular people had
  even voted.
* (p. 59) Land was the favorite object of speculation in the New World.
* (p. 62) "So bitter was the struggle [for land] between Connecticut and
  Pennsylvania that when the French and Indian War broke out, the two colonies
  seemed more likely to fight each other than the French."
* (p. 65) Washington may have taken land from volunteers who fought with him
  in the French and Indian War.
* (p. 67) Washington was notified on April 18, 1775 that the patents on land
  he acquired in the west were "null and void".  Then the revolution happened,
  and no one was willing to take them from the commander-in-chief of the
  American army.

> The true history of the American Revolution cannot be written.  A great many
> people in those days were not all what they seemed; nor what they are
> generally believed to have been.
>
> -- John Jay (p. 75)

* (p. 81) Robert Morris made his fortune selling goods to the continental army
  at inflated prices using unethical practices.  In 1781 he was made the
  Superintendent of Finance.  He argued and was allowed to continue running
  his private business during this time.
* (p. 94) Hamilton believed that the strength of the nation depended on the
  government appealing to the rich and well-born.
* (p. 94) Hamilton's desire for a strong central government could be due to
  the fact that he alone among the convention was born outside the continental
* (p. 96) More than the inadequacies of the Articles of Confederation, Shay's
  rebellion had a profound impact on the making of the Constitution.
* (p. 98) "Benjamin Franklin, who had lent the government $3,000 pounds and
  had accepted 6 percent promissory notes in return, observed in February 1788
  that 'such Certificates are low in Value at present, but we hope and believe
  they will mend when our new Constitutional Government is established.'"
  US on the island of Nevis.  He had no particular love for any state.
* (p. 118) When the Articles of Confederation went into effect, the states
  were supposed to cede their western lands to the national government, but
  most states delayed in order to profit off of selling the lands.
* (p. 122) Patrick Henry had been so opposed to a strong national government
  that he boycotted the constitutional convention.  However, when he earned a
  sizable return on Hamilton's certificates scheme, he became an ardent
  Federalist.
