# GENERAL NOTES
* Hobbes sought a scientific approach to politics
* At the time that Hobbes wrote, civil war created more instability within the
  state than threat of foreign war; Hobbes was driven to create a system of
  government that would reduce civil war

# Part I:  Of Man

## Introduction
* analogy of the political body as a human body

## Ch. 1:  Of Sense
* materialistic description -- nerves, etc. -- of how the person senses the
  external world
* external bodies impress upon, e.g., the eye and the ear; the (subjective?)
  sense of such bodies, e.g. their color or tone, is produced from within

## Ch. 2:  Of Imagination
* dreams are as to waking senses as the stars are to the sun -- obscured until
  the more powerful impression goes away
* memory is fancy or imagination of things we have seen and grows more obscure
  over time
* we imagine things we have seen before or we can compound what we have seen
  before -- like a man and a horse into a centaur
* when awake we consider the absurdity of dreams but when asleep we see no
  such absurdity about waking memories; therefore, we are right in realizing
  we are awake when not sleeping even though we think we are awake when we
  sleep
* external environment -- like temperature -- can affect our dreams and waking
  feelings
* understanding is imagination brought about by words or voluntary signs

## Ch. 3:  Of the Consequence or Trayne of Imaginations
* mental discourse is the train of thoughts or imagination; each thought is
  brought to mind by the next; they are not wholly unconnected to one another
  even though they may seem so
* two sorts of regulated trains of thought:
  * when imagining an effect, we seek the causes that produce it
  * when considering an action, we seek all the possible effects it might produce
    * unique to man
    * called foresight or prudence
* present is of nature, past of memory, future is only of the mind
* prudence allows man to learn and make predictions based on past experience
* man is incapable of conceiving of anything infinite
* all conceptions are based on things sensed

## Ch. 4:  Of Speech
* speech converts our thoughts into words
* allows particulars to be converted to universals (see: triangle theorems)
* without speech, no reckoning of numbers or magnitudes of forces or powers
* truth and falsehood are attributes of speech not the world
* small errors in definitions can multiply
* words can create ideas that don't exist -- "incorporeal body", "round
  square" -- in which case they are merely sounds which signify nothing at all
* understanding is conception caused by speech
* words carry also a signification of the disposition of the speaker -- what
  one calls "wisdom" another might call "fear"

## Ch. 5:  Of Reason and Science
* even the most practiced and diligent men make mistakes in reason
* the use of reason is to start at definitions and proceed from consequence to
  consequence to a conclusion
* words that don't exist -- "round squares" -- are the result of trying to
  reason without words
* Hobbes lists a number of reasons for coming to absurd conclusions
* science is right reason
* "they that trust only to the authority of books follow the blind blindly"

## Ch. 6:  Of the Interior Beginnings of Voluntary Motions; commonly called the Passions.  And the Speeches by which they are expressed.
* distinction between involuntary and voluntary motion
* voluntary motion begins with the imagination; the person must have an idea
  of what they want to do
* Hobbes builds up a large number of definitions of "passions": love,
  prudence, etc.  There is a implication that all of these passions are
  ultimately caused by one's environment
* afterwards discusses how different passions are spoken of
* the truest sign of one's passion is not in the words but in the actions

## Ch. 7:  Of the Ends, or Resolutions of Discourse
* no discourse can end in an *absolute* knowledge of fact; only that if
  certain conditions hold, then the conclusion is true
* "opinion" (also "judgment") is the conclusion of some discourse; although,
  he also seems to say that "opinion" is "science" that is not performed
  entirely correctly
* when an opinion is not due to proper discourse but is due to what someone
  else said, the opinion concerns less the facts than trust in the person who
  said it; this is called "belief" or "faith"

## Ch. 8:  Of the Vertues commonly called Intellectual; and their contrary Defects
* Hobbes names and defines a number of intellectual virtues similar to the
  section above concerning "passions":  wit, discretion, prudence, etc.
* contains many paragraphs concerning what causes "madness" and a bit about
  its relationship to prophecy

## Ch. 9:  Of the Severall Subjects of Knowledge
* a taxonomy of knowledge

## Ch. 10:  Of Power, Worth, Dignity, Honour, and Worthinesse
* "power" is the present means to obtain some future good; this can be
  singular abilities of a man or a result of the united strength of a faction
* "worth" is the price of a man and is dependent on the judgment of others
* "dignity" is the public worth of a man set by the state or community
* "honor" is to value someone highly
* "dishonor" " " " " lowly
* both honor and dishonor are culture dependent and not dependent on moral
  worth -- for example, piracy and highway robbery can be honorable in some
  parts of the world
* honor can be derived from any demonstration of power, even murder or rape
* (honorable and dishonorable here remind me of KJV Rom. 9:21)
* "worthiness" is a fitness or aptitude for a position or charge

## Ch. 11:  Of the difference of Manners
* "manners" are the qualities of how men live together in peace and harmony
* all men unto death are naturally inclined towards a restless pursuit of power
* this drive for power results in conflict
* some things dispose men to obey a common power:
  * desire for ease
  * fear of death/harm
  * desire of knowledge
  * desire of praise
* "For benefits oblige; and obligation is thraldome; and unrequitable
  obligation, perpetuall thraldome; which is to ones equall, hatefull."
* but benefits from a perceived superior lead to love
* Hobbes goes through many examples that lead men to either love or hate their
  sovereign; (reminds me of The Prince)
* men give different names to the same thing due to a difference in their
  passions
* "For I doubt not, but if it had been a thing contrary to any mans right of
  dominion, or to the interest of men that have dominion, *That the three
  Angles of a Triangle should be equall to two Angles of a Square*; that
  doctrine should have been, if not disputed, yet by the burning of all books
  of Geometry, suppressed, as farre as he whom it concerned was able."
* ignorance of remote and natural causes inclines men to believe things that
  are not true
* curiosity should lead one to find the cause of some thing, and the cause of
  that, and the cause of that, until they arrive at some uncaused (eternal)
  cause which is God
* ignorance of the natural causes of phenomena is the seed of religion and
  superstition

## Ch. 12:  Of Religion
* religion is unique to man due to his desire to know the causes of things
* (I like the Prometheus analogy.)
* men blame the things they cannot explain (such as fortune) on the
  motivations of invisible creatures
* intelligent men consider God a certainty based on the need for a "first
  mover"
* Hobbes notes a difference between men basically making things up into
  religions (Gentilism) and men who have been divinely inspired by God
  (Christianity)
* religion can be used to lend legitimacy to a ruler
* God made a kingdom on earth
* religions which do not practice what they preach will be changed or overthrown

## Ch. 13:  Of the Naturall Condition of Mankind, as concerning their Felicity, and Misery
* men in physical strength and intellect are mostly equal
* when men rely on their own strength to maintain their property or person, it
  will lead to war
* they need a common power "to keep them all in awe"
* civilization requires peace
* where there is no common power, there is no law or property; every man will
  simply take what he can get

## Ch. 14:  Of the first and second Naturall Lawes, and of Contracts
* "right of nature" is man's right to protect himself and his interests
* "liberty" is the absence of external impediments
* "law of nature" a rule (by reason) that a man may not do or lose his life or
  property
* law is obligation
* we have a law to seek peace and a right to defend ourselves
* a man may voluntarily renounce or transfer some rights in the pursuit of peace
* rights can only be alienable when the renunciation of them provides a good
  for the person
* a person cannot renounce his right to life
* "contract" is a mutual transfer of rights between men
* Hobbes discusses types of contracts:  covenants, free-gifts, etc.
* covenants cannot be made between men without a common power to enforce them
* covenants require mutual acceptance
* latter covenants do not supersede covenants made previously
* Hobbes discusses all the ways in which covenants are void or valid
* "A Covenant to accuse ones selfe, without assurance of pardon, is likewise
  invalide."
* the power which enforces the oaths can be a civil power or a spiritual one

## Ch. 15:  Of other Lawes of Nature
* "injustice" is failure to uphold one's covenant
* therefore, "justice" (and "injustice") require a common power
* this is where the concept of property begins
* discussion of how justice and injustice relate to men and their actions
* a gift implies that the giver wishes good will from the recipient
* every man should strive to accommodate himself to his society
* men should pardon when others repent
* revenge is forbidden
* no man by word or deed may declare contempt of hatred of another; to prevent
  war
* every man ought to acknowledge other men as his equal
* no man should reserve a right that they would deny another
* judges must be unbiased
* if a thing cannot be divided it should be enjoyed in common
* if it cannot be enjoyed in common then:
  * it should be given by lot, or
  * it should go to the first to possess it
* when there is a controversy of rights, an impartial judge should make
  determination
* no one can be his own judge
* the above precepts are referred to as "law", but Hobbes claims they are
  conclusions or theorems from man's desire for peace

## Ch. 16:  Of Persons, Authors, and things Personated
* "person" is he whose words or actions represent himself or another
* of "artificial persons" (organizations, etc.), the "actor" is the person who
  stands in for the A.P., and the "author" is the one responsible for the
  words and actions of the actor
* buildings, children, mad-men, etc. may be "personated"
* an A.P. may be represented by one man or many
* a multitude of representatives are "oftentimes mute and incapable of action"

# Part II:  Of Common-Wealth

## Ch. 17:  Of the Causes, Generation, and Definition of a Common-Wealth
* without a greater power to ensure security, men must rely on their own
  strength to secure themselves
* the force maintaining security for a state need only be big enough that
  enemies fear to attempt war
* men could never sustain an anarchistic utopia of peace without submission:
  * men are continually in competition with one another
  * many think themselves better governors than others and drive society to
    distraction and civil war
  * men lie and call good evil and evil good
  * it is not natural for men to join together; it requires something
    artificial -- a covenant -- which requires a power to sustain
* every citizen of a common-wealth is the "author"; the sovereign, whether an
  individual or an assembly, is the "actor"
* common-wealth by institution -- when men voluntarily submit to a sovereign
* common-wealth by acquisition -- when men are conquered

## Ch. 18:  Of the Rights of Soveraignes by Institution
* everyone who voted for the specific sovereign, as well as those who voted
  against, authorizes the actions and judgments of the sovereign as if they
  were their own
* once bound in covenant (with one another as well as to the sovereign), they
  cannot make a new, different covenant without the sovereign's permission
* there can be no breach of covenant on the part of the sovereign because the
  sovereign does not receive his power from the covenant but from the power by
  which he protects the common-wealth
* because all the citizens authorize the sovereign's actions, the sovereign
  cannot commit injury or injustice to any of them
* in the effort to prevent discord or civil war, the sovereign decides what
  speech may be permitted and which not
* sovereign sets the laws
* sovereign has the right of judicature
* " " " right of making peace and war
* sovereign chooses all government officials
* sovereign reserves the right of punishment and reward
* sovereign may reward "titles of honor" (earl, duke, etc.)
* the above rights are indivisible from the sovereign, but other rights that
  aren't useful for protecting the state (e.g.  coining money) can be divvied
  out

## Ch. 19:  Of the sverall kinds of Common-Wealth by Institution, and of Succession to the Soveraigne Power
* three kinds of common-wealth:
  * **monarchy**: rule by one
  * **democracy**: rule by all
  * **aristocracy**: rule by only part of the common-wealth
* all other forms of government are but versions of these three
* the sovereign is bound to choose his private interests over those of the
  public good; thus, the closer the private interests align with the public,
  the better the government; these interests are identical in monarchies
* monarchs have more efficient (and secret) access to council
* monarchs are better because the one cannot disagree with himself
* a con:  monarchs can more easily take a man's property, but the same can
  also be done by assemblies
* whatever inconvenience is caused by by the monarch (such as power passing to
  a child) is vastly out-weighed by the peace and stability it brings
* in a representative democracy (such as the U.S. today) the sovereign is not
  the president or congress but the people -- i.e. it is a democracy; the
  representatives are officials appointed by the multitude
* the same government can appear as a democracy or a monarchy to different
  groups -- take Judea under Roman rule:  the roman government was a
  democracy, but the government *appointed* a governor over Judea; so the rule
  was a monarchy to the Judeans
* there must be a clearly defined right of succession for both monarchs and
  assemblies; but this rule does not make any sense in a democracy
* in monarchies the right to choose a successor must always rest with the
  current sovereign and is not necessarily hereditary
* (Hobbes discusses the ideal hereditary succession and *somehow* comes up
  with the exact rules of succession in England at the time; how convenient!)

## Ch. 20:  Of Dominion Paternall, and Despoticall
* common-wealth by acquisition is when men authorize the actions of a
  sovereign under threat of injury
* acquisition is due to fear of the sovereign; institution is due to fear of
  other men
* all the rights and consequences of power under an instituted sovereign also
  hold under an acquired sovereign
* acquisition can be either of generation (paternal) or of conquest (despotic)
* parents hold dominion over their children via generation -- they begat the
  subjects
* mother holds ultimate dominion over the child because the father cannot be
  perfectly known
* if a person is taken captive in war, he has no natural obligation to the
  sovereign who captured him and may try to flee; but if he submits himself to
  the new sovereign in order to preserve his life, he now authorizes the
  actions of the new sovereign
* Hobbes spends a few pages giving examples of his theory using scripture.

## Ch. 21:  Of the Liberty of Subjects
* "A Free-Man is he, that in those things, which by his strength and wit he is
  able to do, is not hindred to doe what he has a will to."
* liberty is consistent with fear -- just because someone omits an offense for
  fear of punishment does not mean he did not have liberty
* Hobbes suggests some sort of determinism, but this is still consistent with
  liberty
* liberty allows men to pursue their own good insofar as they are able, but no
  subject has the liberty to exempt himself from laws which others must abide
* the sovereign is exempt from all of this
* the popular view at the time that subjects believed they could hold their
  sovereigns to account is, according to Hobbes, due to the influence of Greek
  and Roman writers who hated monarchies
* submission to authority consists in both liberty *and* obligation
* previously in Ch. 14, Hobbes listed ways in which a covenant can be made
  void (inalienable rights); every subject has these rights and they cannot be
  taken away; example:  right not to accuse oneself
* cowardice in battle is not injustice to the common-wealth, but treachery is
* no man has the right to do anything which reduces the sovereign's protection
* subjects have the right to sue their sovereign for (what we would now call)
  civil disagreements (e.g. debts)
* if a sovereign grants his subjects a right which removes the sovereign's
  ability to protect the common-wealth, then the grant is void
* the obligation of subjects lasts as long and no longer than the sovereign's
  power to protect them
* if one sovereign renders himself subject to a victor in war, the former's
  subjects immediately transfer their obligations to the victor

## Ch. 22:  Of Systemes Subject, Politicall, and Private
* distinguishes between various independent bodies within common-wealths such
  as "regular absolute" or "private unlawful" systems
* representatives are always limited by the power of the sovereign
* if a single representative act against the law, then his action is his own
  and does not reflect upon the body
* decrees of assemblies represent only the members of the assembly who voted
  *for* the motion
* debts owed by a single representative are his alone
* debts owed by assembly are only owed by those who agreed to go into debt
* if debt be to one of the assembly, then the debt may be paid from the
  common treasury
* one may protest limited representatives but never the sovereign
* provinces of a kingdom may be governed by representatives of the sovereign
  like governors or provincial assemblies
* limited assemblies may not pay their debt by passing the debt on to their
  provincial subjects
* best body politic for foreign trade is an assembly of all the merchants
  involved
* if a corporation owes a debt, every member is liable for the whole amount
* taxes on corporations should be divided among the members proportional to
  their stake in the adventure
* fines are only owed by those who voted for the motion
* the corporation may sue a member of their body, but they do not have the
  right to fine, imprison, or cause to forfeit any member; only the
  common-wealth may punish in that way
* deputies may represent every subject of a body politic for a limited time
* other body politics are lawful or unlawful based on the actions the group
  seeks to carry out
* leagues between the subjects of common-wealths, having no common sovereign
  power to keep the members in awe, are profitable while they can be made to
  last; however, such leagues within a common-wealth are unlawful because they
  are at best unnecessary (because they share a common sovereign)
* within a sovereign assembly, if some members conspire in secret against the
  rest, this is unlawful
* if there is no explicit law against it, there is nothing wrong for a private
  citizen to offer money to sovereign assembly members to vote in their favor
* it is unlawful for any subject to have their own private defense force
* a meeting of people of a reasonable number -- say for church or a show -- is
  lawful, but it becomes unlawful if it grows too large; it may be lawful for
  a thousand men to sign a petition, but if all of them show up to present
  their grievances it is unlawful because only one or two could have delivered
  the petition

## Ch. 23:  Of the Publique Ministers of Soveraign Power
* a public minister is one who has the authority to represent the sovereign in
  affairs of the common-wealth
* these ministers have a public persona when they are on public business but a
  private persona when dealing in their own business
* many types of ministers
* public pleas are criminal cases where the complainant is the sovereign or
  common-wealth
* counselors without other employment than to advise are not public ministers

## Ch. 24:  Of the Nutrition, and Procreation of a Common-wealth
* the "nutrition" of the common-wealth is its resources
* only a vast common-wealth may produce everything it need to be
  self-sufficient; thus trade or war is necessary to acquire the lacking
  commodities
* (Hobbes keeps using the word 'propriety', but based on the usage, I think
  the better word is 'property'.)
* property is an effect of common-wealth
* land is divided by the sovereign's discretion
* subjects may exclude all other subjects from their land except the
  sovereign, be it monarch or assembly
* public land tends towards the dissolution of government (not sure what he is
  say here)
* it is the right of the sovereign only to determine the places and trade
  involved in foreign traffic
* the sovereign should determine what contracts between subjects are valid
* money -- both gold and silver as well as that coined within the
  common-wealth -- is the blood of the common-wealth
* the price of "base money" -- that which is coined by the sovereign -- can
  have its worth enhanced or diminished
* common-wealths "procreate" by founding colonies where there is no one or war
  has killed all the natives; these colonies may be provinces or independent
  common-wealths if the original sovereign discharges his power over them

## Ch. 25:  Of Counsell
* 'command' is an imperative to do something for the benefit of the speaker
* 'counsel' is an imperative to do something for the benefit of the one spoken to
* man is obliged to do as he is commanded but not as he is counseled
* no one can be punished for what they say when asked for counsel
* "ignorance of the Law, is no good excuse"
* counsel with a "vehement desire to have it followed" is 'exhortation'
* exhortation is for the benefit of the counselor
* exhortation is appropriate for speaking to many but corrupt when counseling
  only one
* biblical examples of difference between command and counsel
* a good counselor should not have ends inconsistent with the one being
  counseled
* a good counselor should speak the truth as clearly and succinctly as
  possible
* a person is only a good counselor in the subjects of which he has experience
  and knowledge
* counselors should be heard independently rather than all together

## Ch. 26:  Of Civill Lawes
* civil law "is to every Subject, those Rules, which the Common-wealth hath
  Commanded him, by Word, Writing, or other sufficient Sign of the Will, to
  make use of, for the Distinction of Right, and Wrong; that is to say, of
  what is contrary, and what is not contrary to the Rule."
* Hobbes' deductions concerning the law:
  1. the sovereign is the sole legislator
  2. the sovereign is not subject to the civil laws
  3. the sovereign's consent (possibly by silence) makes something legal
  4. law of nature is equal to civil law; civil law is the natural law written
     down; the purpose of making laws is to restrain liberty
  5. if a new sovereign keeps the laws of an old sovereign, then the old laws
     now continue to be law by the authority of the new soveriegn; mere custom
     does not make the law
  6. yet another emphasis that law is defined by the authority of the
     sovereign
  7. the intention of the legislator (not the word) is the law
  8. the law is a command, and one only to those who can comprehend it --
     children, mad men, and beasts have no law
* laws of nature -- which can be determined from right reason -- need not be
  written down
* ministers of the sovereign, if not given instructions on what to do, must
  take reason (and agreement with the reason of their sovereign) as their
  guiding law
* only the sovereign or those he has appointed may interpret the laws;
  otherwise, an ignorant interpreter may become the legislator
* all laws require interpretation
* the interpreter of the law is the 'judge'
* previous judgments do not determine later judgments even in similar cases
* some discussion of how an innocent man cannot be punished for related crimes
  such as resisting arrest for a crime he has been found innocent of (p.  324-5)
* the law is not interpreted by moral philosophers or commentaries on the law
* attributes of a good judge:
  * right understanding of the law of nature
  * contempt of unnecessary riches
  * to leave emotion out of his judgment
  * patience of, attention to, and good memory of the case he hears
* seven kinds of civil laws of Justinian:
  1. edicts, constitutions, and epistles of the emperor
  2. decrees of the whole people of Rome (senate included)
  3. decrees of the common people (senate excluded)
  4. orders of the senate
  5. edicts of the praetors
  6. sentences and opinions of the lawyers with the authority to interpret the
     law
  7. unwritten customs (when not contrary to natural law)
* laws that are not natural laws are called 'positive' laws
* Hobbes discusses divisions of positive laws
* right is liberty; civil law is obligation

## Ch. 27:  Of Crimes, Excuses, and Extenuations
* imagining what it would be like to break a law is not a sin but the intent
  or actually making plans to do it is
* a crime is a sin that is executed -- e.g. planning to kill someone is a sin,
  but it is not a crime until the action is carried out
* where there is no sovereign power or law, there is no crime, but because the
  natural law is eternal, there is always sin
* ignorance of the *natural* law is no excuse because it is defined by reason
* (Interestingly, Hobbes' example of breaking the natural law is that of an
  Indian coming to Britain and converting people to Hinduism -- or the
  reverse.  Apparently, because that could cause strife within the kingdom.
  His reason should tell him that is wrong.)
* if a crime is not against the natural law but only civil, then ignorance may
  be an excuse
* ignorance of the penalty does not excuse
* penalties should be upheld consistently; if committing a crime is known to
  result in some lesser penalty, then anyone who commits that same crime has
  the excuse of precedent in not accepting a greater penalty
* people cannot be punished for committing crimes before the act was a crime
* via a defect in reason, men are liable to break the law in three ways:
  * believing that they will get away with it -- e.g. in the case of
    overthrowing the sovereign -- and in doing so will have been justified in
    the effort
  * misinterpretation of the laws by those who should know better, i.e. lawyers
  * not bothering to study the law and having too high an opinion of their
    natural wit leads men to infer incorrectly about what is legal and illegal
* the most frequent cause of crime is 'vainglory' -- over-rating one's own
  worth -- as if "worth" had anything to do with the law
* such men often have too high an opinion of their own wisdom and cause strife
  by thinking they know (or would know) how to rule better than the sovereign
* 'fear' is the least likely passion to lead to crime; for some men it may be
  the only thing that keeps them law-abiding
* there are cases where fear can lead to crime:  fear of how society will
  treat him if he does not revenge a wrong for example
* not all crimes are equal -- even the same crime can have extenuating
  circumstances
* if a man commit a crime to save himself from death, he is excused
* if the sovereign commands a man to commit a crime, then it is no crime
* different crime are of different degrees based on:
  * malignity of the cause
  * contagion of the example
  * mischief of the effect -- i.e. damage to much/many vs. few
  * concurrence of times, places, and persons -- e.g. murdering a parent is
    worse than another, robbing from the poor is worse than the rich
* crime committed because one believes oneself above the law for whatever
  reason is a greater crime than the same done hoping not to be discovered
* crime acted upon because of false teachers is not as bad
* crimes of passion are lesser than planned crimes
* examples of the various differences in effects of crimes
* private pleas are those that are brought by one subject against another
* public pleas are those that are brought by the sovereign/common-wealth

## Ch. 28:  Of Punishments, and Rewards
* 'punishment' is "an Evill inflicted by publique Authority, on him that hath
  done, or omitted that which is Judged by the same Authority to be a
  Transgression of the Law; to the end that the will of men may thereby the
  better be disposed to obedience."
* definition followed by justification of punishment
* from definition:
  1. private revenge is not punishment
  2. being neglected and unpreferred is not punishment
  3. evil inflicted without condemnation is not punishment but hostile act
  4. evil inflicted without sovereign authority is a hostile act
  5. the point of punishment should be to reduce further incidents of crime
  6. injury received in the committing of a crime is not punishment
  7. the harm from punishment must be greater than the benefit gained from the
     crime
  8. excessive punishment is hostility
  9. cannot punish crimes that were committed before the law existed
  10. a representative of the common-wealth cannot be punished
  11. harm inflicted in war -- either because the target was never a subject
      or because a subject is in rebellion -- is not punishment but hostility;
      hostility in war, though, is legitimate
* Hobbes defines the following types of punishment:
  * corporal, capital, pecuniary, ignominy, imprisonment, exile/banishment
* punishment of the innocent is against the law of nature
* inflicting harm on innocents in war in the defense of the common-wealth is
  not a transgression of the law of nature
* vengeance declared against rebels lawfully extends to the third and fourth
  generations because this offense is the renouncing of subjection
* rewards can be by contract -- salary for duty -- or gift
* if the sovereign gives to any out of fear, that is not a reward but a
  sacrifice

## Ch. 29:  Of those things that Weaken, or tend to the Dissolution of a Common-wealth
* sometimes common-wealths fail because the sovereign is content with less
  power than is needed to keep the peace
* another way they fail is private persons thinking they are the judges of
  good and evil actions
  * a related "repugnant doctrine" is that whatever a man do against his
    conscience is sin
  * another that faith or inspiration will guide them better than study and
    reason
* another is that the sovereign is subject to the civil laws
* another:  that men have an absolute right to their property that excludes
  the right of the sovereign
* another:  that sovereign power can be divided
* sometimes subjects when looking at foreign neighbors draw inspiration to
  alter the form of government already established
* they get these ideas from reading the books of the Greeks and Romans; they
  think it alright to kill their king as long as they first call him "tyrant"
* any government that divides its powers among different persons or factions
  is dividing the common-wealth into factions
* not as great a threat is the limitation or inability to raise money
* another minor threat:  that of too few private subjects having a monopoly on
  money
* another:  popular men, particularly popular war heroes
* another:  when a single town becomes powerful enough to challenge the
  common-wealth
* another:  enlarging a dominion until the conquered lands become a burden
* lastly, being conquered by a foreign force

## Ch. 30:  Of the Office of the Soveraign Representative
* the office of the sovereign is only for the safety of the people
* in that effort it is against the natural law for the sovereign to
  relinquish any of his rights
* he must also make clear to the people why he has these rights
* they should be taught the following:
  1. they should not desire to change their form of government
  2. they should not desire a different sovereign
  3. they should not (basically) take the sovereign's name in vain
  4. time should be set aside to teach the people these doctrines
  5. justice for their fellow subjects
* the instruction of the people depend upon the teaching of the youth in
  universities
* the law should hold all equal whether poor or rich
* taxes should be equal and that is due to the equal debt each man owes the
  common-wealth for his protection
* those who cannot take care of themselves should be supported by public --
  rather than private -- charity
* however, those who are able must work
* all laws are just; *good laws* are only those which are both necessary and
  clearly worded
* *necessary* -- do not limit liberty any more than is required to keep the
  peace -- like hedges which are not meant to stop travelers but to keep them
  on the right path
* *clear* -- in the causes and motivations behind the law; also the law is
  more easily understood with fewer words
* sovereign must make right application of punishment and reward
* monarchs must choose good counselors (as described in ch. 25)
* commanders should be loved by their army but not more so than the sovereign

## Ch. 31:  Of the Kingdome of God by Nature
* God declares his laws three ways:
  1. by natural reason
  2. by (personal) revelation
  3. through prophets
* the sovereign's right arises from his power; therefore, irresistible power
  results in absolute sovereignty
* affliction from God is not necessarily related to sin but to God's power
* God's power requires honor and worship from his subjects
* Hobbes enumerates and derives many attributes about God from reason that
  just so happen to result in the Christian concept of God
* Hobbes enumerates actions which give God honor
* (Hobbes seems to imply that the whole common-wealth must be of the same
  religion)

The last half of Hobbes' Leviathan (Parts III and IV) deal primarily with his
views of Anglican theology and how the spiritual world relates to his theories
of government.  They do not appear to have anything to do with any of the
issues the founding fathers wrestled with in 1787.  Therefore, I did not
bother to keep notes on the second half of the book.
