[Common Sense by Thomas Paine](http://www.ushistory.org/paine/commonsense/sense1.htm)

* "security being the true end and design of government, it unanswerably
  follows that whatever form thereof appears most likely to ensure it to us,
  with the least expense and greatest benefit, is preferable to all others"
* government is necessary where moral virtue fails
* origin and rise of government:  as society develops and it no longer becomes
  reasonable to act in a purely democratic mode, the elected are chosen to
  make governing simpler; elections become frequent so as to ensure the
  elected do not lose sight of their electors in society
* the constitution of England is so complex that it is difficult to discover
  where exactly the fault lies for errors
* there is something ridiculous in the supposed checks and balances of the
  three powers of the British government -- king, lords, and commons --
  e.g. commons can check the king by withholding supplies (which assumes the
  commons are wiser than the king), but the king can check the commons by
  rejecting their bills (which assumes the king is wiser than the commons) --
  an absurdity
* war is caused by the pride of kings; it is no wonder that Holland without a
  king has enjoyed more peace this century than all the monarchies of Europe
* the biblical ascension of David to the throne details the evils of monarchy
* it only takes a couple generations before the monarch can begin making
  outrageous claims to justify hereditary succession
* English history proves all the arguments for monarchy and hereditary
  succession false
* the error in the constitution of England is the monarchy which poisons the
  republic aspects of the government (the commons)
* America is disadvantaged because of her connection with England -- e.g. England
  goes to war in Europe and America's trade suffers even though America
  doesn't have a dog in the fight
* "nothing but independence, i.e. a continental form of government, can keep
  the peace of the continent and preserve it inviolate from civil wars"
* Paine offers his opinions on a frame of government:
  * "Let the assemblies be annual, with a President only. The representation
    more equal. Their business wholly domestic, and subject to the authority
    of a Continental Congress."
  * [Is the "assembly" the executive branch?]
  * Each colony should be divided into 6, 8, or 10 districts and each district
    to send enough delegates that each colony sends at least 30.
  * Congress would be made up of at least 390 members.
  * A colony should be chosen by lot; then by ballot the president should be
    chosen from the delegates of that colony.  At the next meeting chose from
    the remaining 12 colonies and so on until all 13 have had a rotation.
  * 3/5 of congress must approve to count as a majority
* Paine outlines the method for creating a constitution for the colonies which
  he calls the "Continental Charter":
  * Representatives should be chosen from the following places for each
    colony:  2 members of the continental congress, 2 members from each house
    of assembly, and 5 from the people at large.  "The members of Congress,
    Assemblies, or Conventions, by having had experience in national concerns,
    will be able and useful counsellors, and the whole, being impowered by the
    people, will have a truly legal authority."
  * The charter should fix the number and manner of choosing members of
    Congress, members of the [executive?] assembly, dates of sitting, and
    drawing the legal boundary of the jurisdiction between the two.
* In America:  "The law is king."
* we must form a government now with cool heads before some dictator takes
  over
* "No nation ought to be without a debt. A national debt is a national bond;
  and when it bears no interest, is in no case a grievance."
* America should build her own navy, and there is no reason, given her natural
  resources, that it should not be the greatest navy in the world
* we must split with England while we still have unoccupied land; the longer
  we wait the more likely the king will begin gifting it to some "worthless
  dependents"
* the best time for us to rebel is now in our continent's youth; once we
  become established, we will lose the imperative:  "Commerce diminishes the
  spirit both of patriotism and military defence. And history sufficiently
  informs us that the bravest achievements were always accomplished in the
  non-age of a nation [...] The more men have to lose, the less willing are
  they to venture. The rich are in general slaves to fear, and submit to
  courtly power with the trembling duplicity of a spaniel."
* our government must be founded on representation and election because
  "virtue is not hereditary"
