# The First American Constitutions

Willi Paul Adams

## Introduction
* 3 Jan 6, 1776 New Hampshire adopts first American constitution without approval of Britain
* 3 Virginia first with separate Bill of Rights
* 3-4 history of Massachusetts constitution is wrong- see Shays's Rebellion
* 4 Vermont not initially added to union because NY and NH both claimed the land and other states were more interested in peace
* 5 federalism was a natural byproduct of the self-organization and autonomy of towns, counties, etc.
* 7 logic in Declaration of Independence taken from English constitutional theory of 1688
* 7 judicial power in House of Lords
* 8 begin history of British constitutional thought
* 8-9 quotes Blackstone pp.154-5
* 11 preceding the Declaration of Independence Americans appealed to their fundamental agreement with the English constitution
* 11 origin of no taxation without representation: an Englishman is not obliged to pay taxes or laws except those passed by his elected representatives
* 14 "the happiness of the society is the first law of every government"  James Wilson 1774 or 5 - justified separate rule fro, Britain
* 16-7 colonists consider their charters constitutions granting self rule
* 20 Massachusetts separated legislative activity and the framing of a constitution
* 21 Paine and Adams opponents on issues of domestic politics
* 24 unique conflicts and compromises that American constitutionalism had to settle

## Chapter 1: Government by Congress and Committees, 1773-1776
* 25 social bonds remained intact throughout the war without strict forms of government; this allowed for a transitional period
* 28 "the organization of the american revolution was characterized by faith in the political importance of territorial representation combined with the awareness that specific tasks had to be delegated to ad hoc committees," etc.
* 28 towns, counties, etc. were important for building a system of government  [federalism writ small]
* 28 derived from the tradition of english civil war
* the majority of this chapter seems to be about the bottom-up political organization of the independent colonies
* 35 May 23, 1775 NY provincial congress establishes proportional representation of county representatives
* 38 how equal suffrage was established between the colonies in congress
* need to read John Adams's diary for continental congressional debates
* 41-2 colonies had a model of "real" government based on British rule: governor, governor's council, assembly, and provential judiciary
* 42 governors appoint judges
* 43 initial councils and committees prove unwieldy in the long term; calls for separation of powers into three branches
* 44 history of independent organization in western MA [add to Shays's Rebellion]
* 45 ditto NY
* 46 Paine argues against the present congresses and committees in *Common Sense*

## Chapter 2: The Role of the Continental Congress, 1775-1776
* 47 "none of the 13 colonies adopted a state constitution without an explicit previous recommendation from the continental congress to do so" [more details 54-7] - see claims by delegates that the states had never been independent
* 47-8 however there had benn reciprocal influence between congress and pre-constitution local colonial powers
* 53-4 the continental congress toyed with the idea of drafting a single constitution for all the colonies; John Adams against because he feared (due to the preferences of other delegates) that they would establish a unicameral legislature and executive committee instead of a single governor

## Chapter 3: "Choosing Deputies to Form a Government": The Making of the First State Constitutions
* 61 historically, monarchs had presented constititions to the people - the nation's problem was how to make the people the originators of the constitutions
* 62 initially, provincial congresses passed the constitution like any other bill, and the people elected the representatives knowing they were being given this special task
* 62-3 second stage: conventions were established, separate from the legislature, because the people did not trust the established power
* 63 "constituent power" - a body with the power to form a constitition
* 65 RI uses their charter, just removing references to the king
* 66 1775 - NH congress assigned a committee to design a new constitition; the congress became the lower house who elected the upper house ("council"); parts of the constitution were passed as bills
* 69 1776 - SC passed like any other law; the general assembly then filled all public offices; passed amendments months later
* 69-70 1778 - SC new constitution was passed like normal legislation, but president John Rutledge vetoed it, a power he held under the 76 constitution that the previous governor did not have; he pointed out that his oath of office required him to uphold the 76 constitution
* most states hesitated to create a new constitution believing that it would be seen as a declaration of independence
* 70 George Mason authored the VA declaration of rights
* 71 1776 - VA passed and implemented new constitution like any other legislation
* 72 1776 - NJ constitution passed like any other law
* 73 1776 - DE congress called a convention for the purpose
* 73 DE bits were copied from MD and PA
* 74 DE convention was first to dissolve itself after the work rather than immediately declare itself the new legislature
* 74 PA advocates for independence in the assembly were in the minority
* 76 PA voted for delegates to form a government
* 77 PA established declaration of rights and unicameral legislature
* 77 MD new congress elected specifically for the purpose of drafting a new constitution
* 79 NC an interim council of safety of 13 men took over from the provential congress; they called an election with the understanding that the representatives would draft a constitution
* 80 NC constitution passed like any other law
* 80 1777 - GA similar to NC
* 80 GA decided againsta declaration of rights
* 80 GA unicameral legislature modelled on PA
* 81 1779 GA opponents of the 1st constitution replaced it with one with bicameral legislature
* 81 1777 NY Gouverneur Morris proposed a convention, but supporters of independence disagreed because they felt the provential congress already had the power to form a new government
* 82 a congressional committee determined that a new congress be elected for this purpose; their reasoning was that the people need to explicitly make the decision
* 82 urban artisans call for popular ratification instead saying that the congress did not have the right to pass into law a plan it had framed
* 83 1777 NY congress passed the constitution with heavy revisions
* 87 MA waived property qualifications for voters for constitution
* 88 Adams defends the rejection of the 78 constitution on the grounds that there was no consensus in the suggestions for amendment
* 89 on the second try property qualifications were reinstated
* 89 John Adams gave the governor an absolute veto but the convention rejected it
* 90 1780 - MA towns voted per article with amendments, same result, disperate suggestions; the legislature simply declared that 2/3 of voters had accepted it, and it went into effect
* 90 VT territory had been granted 9+3 seats in the NY legislature
* 91 VT constitution almost word for word identical to PA
* 91-2 VT was successful, but there were failures: Westsylvania, Transylvania Co., State of Franklin (const. identical to NC)
* 94-5 MA in their first effort counted "no" and "yes if amended" identically - this lost the vote; on the second try they counted "yes" and "yes if amended" identically to pass it

## "Republic" and "Democracy" in Political Rhetoric
* 97 'democracy' and 'republicanism' as derogatory terms
* 98 'republican' to "give the demands of mass meetings priority over the decisions of regularly elected representatives"
* 99 colonial criticisms of monarchical government
* republicanism is linked with anarchy
* 101 Holland upheld as a model republic (also strict separation of church and state)
* 101 Paine first to definitively defend republicanism with *Common Sense*
* 103 Paine reserved 'republic' for communities that governed themselves and controlled the government
* 104 most used democracy synonomously with republic between 1774-1780
* 106 John Adams does not equate the two in 1787
* 108 oft quoted: "virtue is the principle of a republic"
* 109 "it cannot reasonably be imagined that the Almighty intended that the greater part of mankind should come into the world with saddles on their backs and bridles in their mouths, and a few ready booted and spurred to ride the rest to death"
* 109-10 foreshadow of distinction between repulic and democracy in the Federalist
* 110-11 Randolph, Gerry, others distinguish democracy and republicanism at federal convention
* 112 ditto Madison in Federalist 10 and 14
* 113 the reason for these distinctions was to warn of the dangers of unchecked majority rule

## Forms versus Principles of Government: Harnessing Enlightenment Ideas to Anglo-American Institutions
* 116 Adams and Henry are hesitant to make democratic principles the end-all-be-all of the government
* 117 distiction of *principles* - judged by ones feelings - and *forms* - judged by ones reason - of government
* 118 when the two conflicted, enlightenment ideals gave way to experience with British governance
* 122-4 common principles of republican government (Stoughton, MA 1779)

## Popular Sovereignty
* 133 VA bill of rights first to place sovereignty in the people
* 133 NJ preamble justified resistance against the king
* 133 DE "internal police":  the people's regulation of government; others copied the language
* 135 VA PA DE MA right of the people to resist and alter their government; and qualifications for when that right should be exercised
* 135 Adams notes that the right of an *individual* to resist is noticably absent
* 137 NJ DE first to (implicitly) introduce the idea that the constitution could be amended by explicitly forbidding the legislature from changing certain articles
* 137 PA first to so explicitly; if a majority of 'censors' voted for amendment, a convention had to be called in 2 years; the legislature was not allowed to alter it
* 137 MD legislature by 2/3 vote could draft amendments; next term of legislature by 2/3 vote could then pass them
* 138 SC legislature could pass amendments by simple majority
* 138 MA GA people could call convention
* 139-40 "recurrence to fundamental principles": the people must insist on a permanent role and be ready to intervene when their rights are threatened by their government
  * 140 based on Machiavelli's *Discourses*
* 142-3 bills of rights (of the states) were meant to be limitations on the sovereignty of the majority

## Liberty
* 150 skeptics doubted that personal liberty would be as secure in a republic as it was under limited monarchy
* 150 the English constitution was widely regarded to protect freedom better than any other
* 151 pro-republican opponents pointed out that the corruption of king/politicians in Britain had already eroded any practicable liberty
* 151 people must remain constantly vigilant to preserve freedom from political greed
* 152 explanation of liberty in natural right theory
* 152 difference between civil and political liberty
* 153-4 enumeration of rights in various state bills of rights
  * life (and self-defense)
  * liberty
  * property
  * pursuing and obtaining happiness and safety
  * trial by jury
  * suffrage
  * freedom of press
  * " " religion (privately)
* 155 the contradition of liberty prinicples with the practice of slavery was well acknowledged
* 156 right to rebel against authority *except* when that authority is law of the people's own making
* 157 before revolution liberty and proptery were treated equally
* 158-9 Gouvernour Morris contrasts political liberty and commerce (private property); unchecked liberty is a threat to property which becomes a threat to society

## Chapter 8: Equality
* 161 Ludlow qoute about artifical distinctions of wealth
* 164 the term "levelling" may refer as much to political freedom - i.e. universal suffrage - as property - wealth redistribution; see Levellers of 1600s
* 166 America characterized as having much less wealth inequality than Europe
* 169 notions of "equality" pre-1776 referred to equality of colonists with English; did not refer to social conditions or political rights
* 169-72 origin of "all men are created equal" in Declaration of Independence
  * only asserting that colonists are equal to English in their right to representation and self determination
* 172-4 there was reluctance to add this concept to state bills of rights; some objected to the domestic implication of "all men are born free and equal"
* 173 Hardwick, MA requested that it be explicit: "all men, whites and blacks, are born free and equal"
* 174 1776 quote: "the rich having been used to govern, seem to think it is their right; and the poorer commonality, having hitherto had little or no hand in government, seem to think it does not belong to them to have any"
* 175 Essex Result upholds the view that the educated men of leisure should rule
* 175-6 lower/middle class arguments against this principle
* 177 paraphrases Montesquieu that 'equality is the soul of a republic'
* 177 Benjamin Rush counters that the principle pressuposes a falsehood: "equal distribution of property, wisdom, and virtue among the inhabitants"
* 179 Continental Congress had tried and failed halt the importation of slaves Dec 1, 1774 and Apr 6, 1776
* 179 VA passed a law to prevent the import of slaves, but only to better control the slave market, not as a first step towards abolition
* 181-2 while MA elites refused to abolish slavery, commoners called for the equal recognition of whites, blacks, Indians
* 182 NOTE: this discussion in MA was not typical
* 183 slave states asserted that slaves were the property of society but not members
* 184-6 summary
* 186 Madison on the existing state bills of rights: "in some instances they do no more than state the perfect equality of mankind; this to be sure is an absolute truth, yet it is not absolutely necessary to be inserted at the head of a constitution"

## Chapter 9: Property
* 187 John Adams: "the balance of power in a society accompanies the balance of property in land"
* 187 PA Evening Post: "putting the rich and the poor on equal footing is giving the wealthy an amazing advantage"
* 188-9 colonist theories of property followed Locke's
* 190-1 the middle class pursued property as a guarantee of security and status; hence why property was so important in declarations of rights
* 192 all state constitutions clearly emphasized the freedom to acquire property
* 193 PA GA constitutions forbid entail and primogeniture in order to prevent aristocracy
* 194 there were usually property qualifications or at least taxes to vote
* 195 English suffrage limited to landholders having a freehold of forty shillings in annual value
* 315-331 table of property qualifications
* 195-6 other kinds of qualificationd
* 198 NH voters just needed to pay a tax
* 198 MA elites believed only the irresponsible would fail to meet the property qualifications for voters
* 199 opponents said that thousands of good men would be barred
* 200 MA average of 80% adult males could vote
* 201 CT 60-75% adult males could vote
* 201 CT popular calls for the end of property qualifications to vote
* 202 RI 79% adult males could vote, but minimum property qualifications were commonly ignored
* 203 NY 60% of adult males could vote for representatives, only 20% for senators and governors
* 203 NJ used any property in place of land
* 204 MD 36-55% white adult males could vote
* 204-5 VA most white adult males could vote for representatives, but Jefferson claimed too few tax payers and enlisted men still could not vote
* 205 NC 62-83% white adult males
* 205 SC more than half white adult males
* 205 GA 70% white adult males
* 206 John Adams believes that those without property - like women and children - will be too dependent on others to make up their own minds and will lead to corruption
* 207 arguments:
  1. owning property makes a man free
     * 207 ecomomic independence leads to intellectual independence
     * 208 Hamilton, Locke, Blackstone, and even Paine held that anyone dependent on others was unable to make their own choice
     * 209 opponents of this idea pointed out that anyone in the militia who bears arms to defend another's property should have the same rights as the property owner; also the poor are affected by the policies of the government, too
  2. real estate ties its owner to the well-being of the community
     * 210 a piece of land in the commonwealth would better secure a representative to the interests of the community than an oath of office
     * 210 Edmund Pendleton (for): "The persons who when they have produced burdens on the state, may move away an leave them to be born by others, I can by no means think should have the framing of laws."
     * 210-1 Jefferson (against): "Whoever intends to live in a country must wish that country well."
     * 211 others argued the opposite: that anyone with too much property would pursue their personal interests over the common good
  3. one must be subject to taxes in order to levy them
     * goes along with the "no taxation without representation" maxim
     * 212 opponents pointed out that this assumes that the poor could burden the rich with taxes, but that the rich could do no similar damage to the poor; yet history showed that the wealthy tended to reduce property taxes in favor of expensive poll taxes
  4. property and power together lead to political stability (from Harrington)
     * 212 most did not think this held in America; Paine believed that land was so cheap in America that labor and population had more impact on political stability
  5. policies involving property should be decided by the majority of property owners only
     * 213 derived from social contract theory which posited that property existed prior to the community; those entering the compact had a right to retain what they brought in
     * 214 critics emphasized the securing of liberty and happiness over that of property; a voice in the community's affairs became a natural right; denial of the vote to those who do not meet property qualifications is a violation of the social contract
     * 214 Jefferson suggested as a solution in the VA constitution draft that every landless man be given 50 acres
* 215 (footnote 103) according to Madison property was not a major consideration at the federal convention because no uniform rule would have been applicable to all the states; thus such qualifications were left to the states

## Chapter 10: The Common Good
* 216 tension between the individual and society - New England Puritain tradition
* 217 emphasis on policies needing to be good for the entire commonwealth, not just a part
* 218 government is an instrument subservient to the individual's interest, not the collective
* 218 common belief that decline of states follows the decline of virtue in citizens
* 219 party conflict could be seen as putting private interests before the public good
* 220 DE bill of rights established the common good as the reason for government power
* 220 good government inconsistent with private interests
* 221 state constitutions equated common good with the sum of private interests
* 222 analogies to the common good from finance
* 223 "mecantile interests" (as opposed to "landed interests") included activities indirectly related to commerce
* 223-4 debate over whether agriculture or commerce was more in the interests of the common good
* 224 Essex result stated that all interests when considered were really one; however, it also identified the public good largely with the interests of the propertied classes
* 225 PA justified the unicameral legislature with the idea of conflicting interests; the single house should consist of all interests
* 225 Hamilton's selfish view of human nature derived from Hume; see quote
* 226 all state constitutions were against parties, thinking it was the job of the constitutional form to reconcile conflicting interests
* 227 a fair system of representation was necessary to resolve conflicts and achieve the common good

## Chapter 11: Representation
* 228 since 1690s the state assemblies considered themselves equivalent to the House of Commons and followed rituals parliament as best they could
* 230-1 colonist view differed from English in that the former believed representatives and constituents shared common interests
* 232 representation and checks-and-balances were the foundations of state government
* 233 Adams: the legislature should be a miniature of the interests of society
* 233 Hamilton: a few of the most wise and good should rule - (Essex:) "men of education and fortune"
* 233 Essex: rational deliberation was only possible with <100; greater irrates the passions
* 234-9 gradual move from graduated representation (e.g. Boston with 4, all other towns with 1) to proportional representation
* 235-6 MA small towns fighting to maintain equal suffrage models what would occur in the federal convention
* 237 Essex suggested a compromise similar to senate/house compromise in convention
* 239-40 VA NY MA created artificial districts of equal land area from which to elect senators
* 242 no constitutions provided for the recall of representatives; most preferred short terms of office to keep representatives in check
* 242 SC reps had 2 year terms all other states use 1 year terms
* 243 MD longest senate term: 5 years
* 243 DE NY longest governor term: 3 years
* 244 most strict representative instructions were limited to votes for independence or new constitutions
* [Did the state letters to delegates include instructions?]
* 246 3 state constitutions guaranteed the right of instruction
* 247 (footnote 44) King makes a suggestion in MA convention that senators be instructed by the General Court
* 247 since 1688 parliament had been secret; the original reason was to keep out the king's spies; 1771 Commons gave up the privacy
* 247-9 state rules for publication of assemblies
* 249-50 rotation of office used to prevent accumulation of too much power and induce many men to try to devote themselves to the office
* 251 rotation also used to prevent political parties
* 328-331 state rules of rotation

## Chapter 12: The Separation of Powers
* 255 mixed model of government from Britain
* 256 checks and balances had been praised in the British constitution
* 256 congress objected to indefinite terms for governor counsellors
* 257 1773 Encyclopedia Britannica asserts that the mixed form of government is gothic in origin (see also footnote 9)
* 258 *Common Sense* answered a popular call for a simpler constructed government; the thought being that simple made it less likely to be corrupted and more easily fixable (see *CS*)
* 259 opponents characterized the attempts as idyllic and naive
* 261 arguments against bicameral legislatures
* 262 John Adams argument for balancing power between executive, legislative and judicial; which morphs into an argument against unicameral legislatures
* 263-4 arguments againt *unicameral* legislatures
* 265 early state constitutions did not treat judiciary as an independent branch of government
* 265-6 VA first to explicitly separate the three powers
* 266 NY council of revision of supreme court judges plus governor; also impeachment procedings
* 267 MA judges appointed by executive and remain in office during good behavior
* 267 footnote 44 early judicial review
* 268 early constitutions relied more on executive veto and periodic review and revision of the constitution to prevent the passing of unconstitutional law
* 268 "the principle of the separation of powers and the system of checks and balances obviously required that judges be appointed for life"; while this ensured independence from the legislature, it also ensured independence from the people
* 270 NY governor first to elect directly by the people
* 271 NY initiated state of the union
* 271 NY 2/3 overrule of veto

## Chapter 13: Federalism
* 274-5 federalism was not purposeful; it evolved from the domestic situation that the war imposed
* 276 even during the war a strong central government was proposed to replace the crown for fear that without it the states would devolve into civil war
* 276 "lack of information decided the issue" of suffrage in the first Continental Congress
* 277-8 Benjamin Franklin's draft of AoC
* 279-80 compare/contrast Dickinson's draft with Franklin's
* 280 ensuing debates moved the balance of power in the drafts to the states and away from Congress
* 282 three main interest groups at the federal level: slaveholders, large v. small states, and states with western territories
* 282 Franklin: the difference between sheep and slaves is that the former strengthen the economy and will never cause insurrections
* 283 Franklin: a confederation based upon such iniquitous principles as equal suffrage will never last long
* 283-4 arguments regarding equal v. proportional representation
* 284-5 fights over claims to western lands
* 287 "American Empire"
* 288 Washington's plea for a stronger central government
* 289 Willi Adams argues that the presidential system of the US draws more from state experiments than the British system

## Chapter 14: The State Constitutions' Analogies and Precedents for the United States Constitution
* 290 ditto last point above
* 292-3 senate analogies
* 293-4 presidential analogies
* 294 NY (then MA) credited with invention of modern president
* 295-6 distribution of senate seats by territory not a new idea
* 297 demsnds for constituent power of the people led to constitutional amendment process
* 297 MA convention for state constitution "resorted to self-deceit and fraud"
* 298 MA used as example against conditional ratification
* 298-9 various state methods of amendment process
* 299 eventual bill of rights did not include a single right which had not already been codified in some state declaration of rights

## Chapter 15: Testing the Republicanism versus Liberalism Hypothesis
* historical debates about the roles 1700s "republicanism", "democracy", and "liberalism" played in the federal convention
* 309 footnote 19 contrast of French and American revolutions
* 310 MA analogies to presidential veto snd executive appointment
* 311-2 property as a right in the practice of state constitutions
* 313 hypotheses why the 13 colonies rebelled and other British ones didn't