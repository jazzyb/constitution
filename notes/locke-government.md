# GENERAL NOTES
* notes start with Book II since Book I is just a negative criticism of
  Filmer's patriarchial theory of government which no one takes seriously
  anymore; II, §4 begins Locke's own political theory
* see ch. V of the introduction pp. 93-122

# Book II

## Chap. I
* briefly summarizes the theories of Filmer that he countered in Book I
* §1, 20-22: possible reference to Hobbes:  "...to think that all government
  in the world is the product only of force and violence, and that men live
  together by no other rules than that of beasts, where the strongest carries
  it..."
* §3 is Locke's definition of "political power": right of making law for
  preserving property and defending the commonwealth from foreign powers, all
  for the public good

## Chap. II:  Of the State of Nature
* in a state of nature all men are equal to one another -- no one has any
  power over another
* liberty is not license
  * man has not the right to destroy himself
  * no one has the right to invade the rights of others
* in the state of nature everyone has the right to punish others for
  transgressing the laws of nature -- but only to go so far as retribution or
  restraint -- not revenge
* transgression of the law of nature is to live by something other than reason
  and equality between men
* every man in a state of nature has the right to kill a murderer
* objection:  men in a state of nature will go easy on their friends and carry
  vengeance too far for others
  * "civil government" is a remedy for this
  * absolute monarchy is not -- for the monarch is only a man himself and is
    liable to the same weaknesses
* Q:  when were men ever in a state of nature?
  * the promises and compacts that men make with one another are bound by the
    laws of nature
* men remain in a state of nature until they consent to become a member of a
  body politic

## Chap. III:  Of the State of War
* a state of war is when a man determines to take power over another man's
  life
* man may kill someone who has made a declaration of war upon him
* attempts to enslave are declarations of war as well because as soon as
  someone has power over you, they have the ability to take your life
* §18 lawful to kill a thief -- because by taking from you by force you have
  no way to know they won't try to take everything away from you
* a state of war is over when the threat of force is gone
* after the threat the innocent party may rightfully take up a state of war
  upon their aggressor
* to avoid the state of war and to have an authority to judge between
  contenders is a reason for men to gather together in society

## Chap. IV:  Of Slavery
* man's natural liberty is to be free from the power of any man and be equals
  with each other
* freedom of men under government is to have common rules to live by, liberty
  to follow one's will where the law does not deny, and not to be under the
  arbitrary force of any other man
* man cannot consent to enslave himself
* one may forfeit one's life by some act which deserves death
* when one has another's life in such a way, he may delay in taking it and
  use him for service instead
* §23, 13-15 seems to justify suicide in the event that a slave's burden
  out-weighs his life
* slavery is the state of war continued between a lawful conqueror and captive
* if the two enter into a compact where they negotiate power by one and
  obedience of the other, then the slavery is suspended as long as the compact
  is in effect
* (see footnotes for this chapter, pp.284-5:  Locke believed that the black
  slaves in the Americas were lawful captives of a just war.)

## Chap. V:  Of Property
* in the state of nature all land, etc. belong to everyone in common
* every man has property of his own body and, by extension, his labor
* whatever a man removes from the state of nature and joined with his labor is
  also his property
* but man has only a right to take as much from the common state of nature as
  he can enjoy before it spoils
* as much land as a man is able to till, improve, etc. is his property
* land that is held in common by compact -- owned by the country or parish --
  can not become the property of one without consent of all
* labor improves the value of land, etc. by 10 or 100 times
* labor is the real value
* the things of "real" value support life but they may spoil
* gold, silver, diamonds do not spoil and by fancy or agreement are assigned
  intrinsic value
* thus came the use of money -- some thing with value that can be traded for
  things that spoil
* because of money men can produce more than they can use without wasting
  their property and can use the surplus to benefit others

## Chap. VI:  Of Paternal Power
* (primarily a criticism of Filmer)
* children are free from submission to their parents once they reach the age
  of reason
* §57, 17-18 "the end of law is not to abolish or restrain, but to preserve
  and enlarge freedom"
* the grown child may have obligations of honor and help to give to his
  parent, but that does not mean he must fulfill any of his parents' commands
* there is no connection between the honor a child bestows on his parents and
  the submission due to civil authority
* the reason men think so is that government started in small families where
  the father was the "prince" of the household; then households banded
  together and kept the form of governance; this has led to hereditary
  monarchies
* there is no more reason to believe that a prince has supreme authority over
  his subjects due to the father's authority in the home than to believe that
  princes are the only ones who can be priests since the father is the priest
  of his household

## Chap. VII:  Of Political or Civil Society
* man has a natural drive to join with others
* the first "society" was between man and wife, which over time extended to
  include children and servants/slaves
* explains that the reason husbands and wives are tied together longer than
  other animals is because the children are so much more dependent
* the husband is not an absolute monarch of his home; the wife has her own
  interests and influence
* civil society requires a common law for all men under it with judges with
  authority to decide disputes
* political society is when men give up their right to the laws of nature
  (judging the acts of men themselves and executing justice for themselves) to
  join together in civil society
* whenever any men are associated without a common judge to appeal to, they
  are still in a state of nature
* therefore absolute monarchies are not civil societies because if the monarch
  does injustice to anyone, that person has no one to appeal to
* §94, 32 "no man in civil society can be exempted from the laws of it"

## Chap. VIII:  Of the Beginning of Political Societies
* men can only join civil society by their own consent
* in civil society the majority have the right to "act and conclude the rest"
* if not the majority, then it must be the unanimous consent of all involved,
  but that would never happen due to different interests and therefore the
  majority is the best available option
* (§§96-7 references to Hobbes' leviathan)
* all civil societies began as men uniting in the way that Locke supposes they
  did
* early civil governments were ruled by the will of one man because they were
  based on the paterfamilias; when men were few enough and land was numerous,
  civil society was made up of single families
* §107, 25 & 40 "frame of government"
* biblical references:  the judges and first kings of Israel were basically
  only army generals; it was only over time that they became more concerned
  with domestic affairs
* governance passed from father to eldest son in these early family bands;
  hence, hereditary monarchy
* over time men found that poor rulers sometimes took over, and they sought to
  limit the power of the office to protect themselves from these poor rulers
* objection:  that men born under a government have no right to set up a new
  one
  * all governments have been set up independently of some other government
    either when (1) men colonize some other place, (2) kingdoms split up, or
    (3) mulitple communities band together to form a larger society
  * (in Locke's time) neither the citizenship of the parents nor the country
    of birth determined what society the child was a member of; he could not
    join any society until he reached the age of reason
  * (what would Locke think of the situation today in which children have no
    choice what society they are obligated to, and renunciation of citizenship
    is so difficult a process?)
* any land owned by a person who submits themselves to a government forfeits
  the land to be under the laws of the society as well
* submitting to the laws of a society and enjoying the privileges and
  protections of that society does not make one a member of that society
* nothing can make a man a member to society except a positive expression to
  be so

## Chap. IX:  Of the Ends of Political Society and Government
* man leaves the state of nature because, although he enjoys more freedom, his
  property (life, rights, and estate) are insecure if he is the sole defender
* while the law of nature is absolute, due to their lack of reason or bias men
  cannot be trusted to always obey it; therefore men must set up their own
  explicit law
* ...and need an impartial judge to appeal to
* ...and punishment needs to be carried out with superior force
* in his submission he gives up his rights that are necessary to give up in
  order to preserve both his property and his society
* ...and the power to punish those who break the law
* the legislative power can never extend further than to protect the common
  good
* ...nor can the execution of the laws and foreign war

## Chap. X:  Of the Forms of a Common-wealth
* types of government:  if the executive and legislative powers rest in...
  * perfect democracy:  ...the rule of the majority
  * oligarchy:  ...a few select men, their heirs and successors
  * monarchy:  ...one man...
    * hereditary monarchy:  ...and his heirs
    * elective monarchy:  ...for life and upon his death a new successor is
      chosen
  * ...a mixed form of any of the above
* the election of new representatives forms a new government
* commonwealth:  any independent community; *civitas* in latin

## Chap. XI:  Of the Extent of the Legislative Power
* the supreme power of the commonwealth is the legislature
* the foremost motivation of the legislature is the preservation of the
  society as far as it consists with the public good
* no one but the legislature that the people erected/gave their consent can
  make laws
* the power of the legislature is limited to the public good of society
* the law cannot be arbitrary; the law must be clearly stated and known
  authorized judges
* the supreme power cannot take from any man (including taxes) without his
  consent
* an exception to the above is in martial law where a commander may punish by
  death anyone who refuses to obey an order because this is necessary to
  preserve the commonwealth
* yet, even in that case, the commander has no right to take any other
  property from his soldiers
* the legislature may not transfer the right to make laws to anyone else
  without the consent of the people
* (§142 is a good summary of the chapter)

## Chap. XII:  Of the Legislative, Executive, and Federative Power of the Commonwealth
* a distinct executive branch who executes the laws must be separate from the
  legislative branch -- otherwise the people who make the laws might exempt
  themselves from following the laws
* there is a third power in commonwealths distinct from both the legislative
  and the executive which Locke calls the "federative"
* the federative power is that which makes deals between the commonwealth and
  the rest of mankind -- war and peace, transactions with others outside the
  commonwealth, etc. (-- "foreign policy" we might say today as opposed to
  "domestic policy")
* the federative is distinct from the executive since, dealing with foreign
  powers, it does not require standing laws and must rely more on the prudence
  of the one with the power
* even though federative and executive are distinct roles it is useful to give
  both to the same person or persons

## Chap. XIII:  Of the Subordination of the Powers of the Commonwealth
* even though Locke refers to the legislature as the "supreme authority", he
  places a "supreme power" in the people to remove or alter the legislature if
  they act contrary to the trust of the people
* the executive power without a share in the legislature is subordinate to the
  legislature
* the executive must needs be always in session but not the legislature
  because while there is always a need to execute the existing laws, there is
  not always a need to draft new laws
* the executive commonly has the power to assemble and disassemble the
  legislature, but he is still subordinate to their laws
* however, if the executive uses his power to hinder the meeting and acting of
  the legislature, then the executive is in a state of war with the people,
  and the people have the supreme power to reinstate their legislature
* on the one hand, constant, frequent meetings and long assemblies of the
  legislature could be a burden on the people; on the other hand, a delay in
  their convening or too short a session might hinder their ability to protect
  the common good; what can be done?
  * supposing the times of assemblies for the legislature are not laid out in
    the constitution, it is only natural that the power to convene and disolve
    the legislature be held by the executive
* without occasional electoral reform, the representation of the people in the
  legislature can become unequal
* (a note on "representation":  see footnote on p.373, Locke did not have in
  mind universal suffrage but voting rights only for those who own land)

## Chap. XIV:  Of Prerogative
* the executive has the prerogative to execute necessary actions on which the
  law is silent that by nature need to be enforced before the legislature can
  legalize them
* executive should have the power to mitigate the law -- to pardon
* the legislature has no way of foreseeing what may be needed in the future
* to legislate the prerogative is not to encroach on the prerogative
* prerogative comes with the people's trust that it is used wisely, and it
  tends to be the wisest princes who are given the greatest freedom of
  prerogative
* Q:  who shall judge when prerogative is used well?
  * A:  there can be no judge on earth to judge between the executive's
    prerogative and the legislature;  the people can only appeal to heaven

## Chap. XV:  Of Paternal, Political, and Despotical Power, considered together
* (this chapter is very repetitive of points previously made)
* paternal power is the rule of the parent over the child until he comes of
  the age of reason; it grants the parent no rights over the grown child's
  property
* political power is the power the man in a state of nature grants to society
  when he becomes a member to protect his property
* despotical power is absolute power one has over another's life and which
  puts him in a state of war with the aggressed

## Chap. XVI:  Of Conquest
* conquest is is not the formation of a new government any more than
  demolishing a house is the same as building a new one
* the conqueror, if he led an unjust war, can never have a right to the
  subjection and obedience of the conquered
* the victims of an unjust conqueror have no one to appeal to but heaven
* Q:  what does the conqueror of a *just* war get?
  * **no** power of those who conquered with him; his allies are as free as
    before
  * he has despotical power over the lives of those who voluntarily assisted
    in the war against him -- **but** not all their property
  * and no power over the lives or fortunes of those who did not engage in the
    war; this includes the children and wives of the men who did fight
  * he has a right to recoup his damages and losses from the war out of the
    property of the victims -- but no more than that
* even in a just war the conqueror has no right of dominion; the people who
  did not engage in war with him have a right to erect a new government
* §190, "every man is born with a double right":
  * right of freedom in his person
  * right to inherit his father's possessions
* from the first, man is free from obligation to a government even if he is
  born there
* from the second, the children retain a right to the land of their ancestors
* (the points about the children are odd considering Locke's approval of
  slavery in America as the result of a just war in Chap. IV)

## Chap. XVII:  Of Usurpation
* a usurper is a type of domestic conqueror who can never have right on his
  side
* the usurper is anyone who tries to take control of a government without
  being the legitimate heir defined by society or without having the consent
  of the people

## Chap. XVIII:  Of Tyranny
* tyranny is the exercise of power beyond one's right; he makes all give way
  to his own appetite
* all forms of governments, not just monarchies, are susceptible
* §204, "force is to be opposed to nothing but to unjust and unlawful force"
* there is no need for force when appeal to the law can be made:
  * if a highway robber tries to take my purse, I may kill him just for
    threatening to rob me
  * on the other hand, if I give money to someone to watch my possessions for
    a time; then when I return to recover them he refuses to give me my
    possessions and even draws a sword to prevent me from taking what is
    rightfully mine, I have no right to kill him because I have the time to
    retreat and appeal to the law to force him to return my possessions and/or
    money

## Chap. XIX:  Of the Dissolution of Government
* distinction between dissolution of *society* and dissolution of *government*
  * government may be dissolved without dissolving the society
  * whereas dissolution of society implies dissolution of government
* government is dissolved from within whenever the legislature is altered:
  * (the following assumes a government made up of an executive, hereditary
    legislature, and an elected legislature -- see §213)
  1. by the executive taking the role of the legislature
  2. by the executive preventing the legislature from assembling
  3. by the electors or elections being modified without the consent of the
     people
  4. by the delivery of the people under the subjection of a foreign power
* government is dissolved also whenever the executive neglects his duties
* when the government is dissolved by any of these means, the people are at
  liberty to form a new government
* governments are dissolved whenever the executive or legislature act contrary
  to the people's trust
  * when the legislature takes the executive's role
  * when the executive corrupts the legislatures *or* compels the electors to
    make certain choices
* §223, 9-10 possible Declaration of Independence (DoI) phrase, see p.414 n.
* an objection:  that no government can persist if the people are given the
  right to overthrow it whenever they are displeased
  * answer:  people do not so easily amend the status quo
  * (that seems less like an answer and more like a different problem)
  1. the people should rebel if they are burdened
  2. revolution doesn't happen as a mere "mismanagement" of affairs but by
     gross abuses of the authorities
  3. giving the people the power to form their own government is the best
     defense against rebellion for if the authorities are in opposition to the
     laws, then it is *they* who are in rebellion, not the people
* §225, 5-6 possible DoI phrase, see p.415 n.
* the authority is in rebellion if it acts in such a way to dissolve the
  government; it enters a state of war with the people
* §228, possible Hobbes reference (?) -- those who would say that civil war is
  worse than abusive authority are like the man who would say that being
  robbed is better than opposing the robber; what sort of world do they want
  to live in where only the oppressors are thriving?
* §230, 7-8 possible DoI phrase, see p.418 n.
* moreover, who is more often the cause of civil strife?  (1) the people
  overthrowing lawful rulers or (2) rulers exercising arbitrary power over
  their people
* (Locke singles out William Barclay's patriarchal arguments pp. 419-25.)
* only the people can have authority to judge whether their government has
  overstepped its bounds
* once an individual submits himself to society, he cannot leave society while
  that society lasts; that would destroy the community
* likewise, the power of the legislature (once established) cannot revert to
  the people while the government lasts
* however, if time limits have been set on the power of the legislature or
  else they have forfeited their power via miscarriages of their authority,
  the power reverts to the society
* at which point the people may put the old government in new hands or form a
  new government
