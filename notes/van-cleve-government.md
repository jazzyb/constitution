# We Have Not a Government

### Introduction

Historians are broadly divided between 19th century historians who thought the
Constitution was a necessary step to preserve the union and 20th century
historians who deny that there was a crisis.

The first:
* *The Critical Period of American History* by John Fiske
* *A History of the People of the United States from the Revolution to the
  Civil War* by John Bach McMaster
* *The Confederation and the Constitution* by Andrew C. McLaughlin
* *History of the Formation of the Constitution of the United States of
  America* by George Bancroft

The "moderns":
* *New Nation* by Merril Jensen
* *Forging of the Union* by Robert B. Morris
* *The Beginnings of National Politics:  An Interpretive History of the
  Contintental Congress* by Jack Rakove
* *The Creation of the American Republic* by Gordon S. Wood
* *E Pluribus Unum:  The Formation of the American Republic* by Forrest
  McDonald
* *Independence on Trial:  Foreign Affairs and the Making of the Constitution*
  by Frederick W. Marks III
* *The Antifederalists:  Critics of the Constitution* by Jackson T. Main
* *The Making of the Constitution* by Charles Warren
* *The American States During and After the American Revolution* by Allan
  Nevins
* *History of the United States: American Revolution, 1761-1789, vol. 3* by
  Edward Channing
* *An Economic Interpretation of the Constitution of the United States* by
  Charles A. Beard

See endnote 11 for even more suggestions and arguments for each view-point.

Also "'Experience Must be Our Only Guide':  History, Democratic Theory, and
the United States Constitution" in *Fame and the Founding Fathers* by Douglas
Adair -- demonstrates that the founding fathers drew their feeling that the
dissolution of the union would lead to civil war and despotism from the
lessons of history.

Compare and contrast Rufus King's evolving views (he was against the
convention to begin with but began to support it after Shay's Rebellion) with
those of Patrick Henry (who became an opponent of a stronger central
government).
(NOTE:  [The Founding Finaglers](notes/miller-finaglers.md) claim that Henry became an "ardent Federalist".)

## Part One:  The Search for National Identity

### Chapter 1:  War's Aftermath

Portrays Robert Morris as a treasurer who was forced to make promises of money
to others that Congress could not pay.  (The Founding Finaglers is not so kind
to Morris.)  This demonstrates the enormous economic crisis that America was
in after the war.(18-9)

Nevins concludes that the South suffered much more severe losses during the
war than the North and they occurred later in the war.(22)

Enormous inflation was caused by the printing of paper money during the war
that was not backed by specie.  Unfortunately, paper money was required by law
in many places to be accepted at face value ("legal tender").(22-3)

There was a general democratic (or "republican") spirit after the close of the
war:  women took on greater responsibility and demanded more rights, soldiers
demanded the vote, and non-wealthy, populist politicians took the Pennsylvania
legislature.(26-7)

Wartime constitutions tended to increase legislative power at the expense of
the governors who often did not have veto or appointment powers.(28)

Pennsylvania and Massachusetts provide competing views of republicanism at the
time.  Pennsylvania's constitution, written by Franklin, was populist -- the
single-house legislature was supreme.  Massachusetts's on the other hand,
written by John Adams, maintained property quotas for voting and holding
office.  The legislature was bicameral -- a house of representatives and a
senate -- and one had to be relatively wealthy to run for the senate.  The
senate was also partitioned based on property taxes so that Boston held more
power than the rural ares to the west.(29)

After the war, statesmen can be divided based on their vision for America.
The "federalists" saw the victory over Britain as a victory for the
independent republics in America.  They believed that all power was evil and
must therefore be minimized.  A national government (especially one with a
standing army) would lead to aristocracy and monarchy.  Nationalists on the
other hand believed that the decentralization of the confederation had made
the war longer and more expensive than it had needed to be.  The nationalists
did not see any threat to republican government from making the national
government more powerful.  In their mind the elected government
representatives were members of the people, too.  Therefore, they would not
make laws which burdened the common people when they would have to return to
their ranks shortly after their appointment.  For them a strong central
government was necessary to prevent domestic despotism and foreign
influence.(29-33)

In the Treaty of Peace with Britain, France and Spain both opposed the land,
navigation, and fishing rights that Britain granted to America.  They had
supported America to remove Britain from the continent, but they did not want
America to prosper at their expense.(35)

The treaty also failed to protect America's free trade with Britain.(36)

After the war America experienced significantly more imports than exports.
Exports were 24% lower than they were before the war.  This caused specie
(real money) to leave the continent at a higher rate, contributing to the
inability for people to have gold or silver.(37)

The new nation was going through a liquidity crisis (deflation) of property
and goods.  This inflated the paper money which could not buy anything.(37-38)

The economic conditions made many emigrate out of the populous areas in states
to the western lands in order to (1) avoid overbearing taxes and (2) find
cheap, fertile land.  The emigration worried the leaders because it removed a
tax revenue and placed the burden to make up that revenue on those who
remained.(39)

Debtors had very few laws (like bankruptcy) protecting them from creditors and
could be thrown in jail for failing to pay debts.(46)

Summary on p. 47.

### Chapter 2:  Americans and Post-War Debts

Congress required the states to pay their debts for the war but lacked the
power to compel them to do so -- New Jersey notably threatened not to pay any
debts unless Congress convinced New York to stop taxing imports into New
Jersey.  By 1786 the Confederation had become permanently insolvent and by
1787 had defaulted on major loans.  Congress could also do nothing to compel
citizens to honor pre-war debts to Britain as was required by the Treaty of
Peace.  When they refused, Britain refused to evacuate their western forts
preventing expansion into those areas by the US.(48-9)

Repaying public debts was seen as a moral imperative for governments just like
the expectation that an individual should repay his debts.(50)  Congress
emphasized to the states that their creditors, including the military, would
not support the country in the future if it could not be trusted to repay its
debts.(51)

There were disputes about which debts should be paid by the federal government
and which by the states, also how to go about paying those debts -- the
current requisition system, new tax powers, sell western lands, etc.(51-2)

Congress's only means of raising money during this period was by
"requisitioning" -- i.e. begging money from -- the states, but the states did
not pay these debts.  Contemporaries argued that the states *wouldn't* pay
them, but some historians argue that they *couldn't*.  As early as 1783,
lack of concern with "public faith" was predicted by many of the founding
fathers to be the weakness of democratic governments.(52-3)  As a result of
failure of the states to fund the federal government, Congress was forced to
engage in large-scale deficit financing.(54)  A Feb. 1786 report commissioned
by Congress concluded that there was not enough money coming in to even cover
the operating expenses of the republic much less the interests on its debts.
It stated that the Confederation could not last.(54)

See Madison's problems with the Confederation (#1 is failure to repay
requisitions):  [Vices of the Political System of the United
States](http://teachingamericanhistory.org/library/document/vices-of-the-political-system/).
This document was intended to be a list of problems that needed to be
addressed at the Constitutional Convention.

"Anarchy" is used to mean "where the laws lose their respect... where no
permanent security is given to the property and privileges of the citizens;
and no measures pursued, but such as suit the temporary interest and
convenience of the prevailing parties."(53-4) -- Charles Pinckney's speech to
the New Jersey Assembly on March 13, 1786.

In 1783 Congress agreed to pay "commutation" -- five years pay -- to officers
who had served in the continental army until the close of the war.  This was
less than was voted on in 1780 when Congress made a promise to pay officers
half-pay throughout their lives.  Even politicians who had voted against the
1780 law saw that commutation was necessary to earn the trust of the army.
Some New England states, Connecticut in particular, were against it.(57-61)

Debts were held disproportionately by the states.  The states north of
Maryland held more debt than the southern states, but the northern states held
more power in Congress to pass laws that reduced their requisition payments in
proportion.  As a result, Maryland, North Carolina, South Carolina, and
Georgia -- feeling that this was unfair -- made no requisition payments at
all.(63)

Two solutions to the requisition system were proposed:  (1) divide the
Confederation debt evenly among the states and let them pay it off at their
prerogative or (2) sell the western lands acquired from GB in the Treaty of
Peace.(64)

Three Mid-Atlantic states assumed responsibility for paying the Confederation
debt to its own citizens but not to nonresidents of the states.(64-5)

Rhode Island printed paper money that continued to decline in worth, but then
used this money to pay its debts.  By 1790, it declared that it was debt-free.
Of course, it had only paid ~20% of its real debt.  Most contemporaries
thought this was despicable.(67)  [I vaguely recall references to this either in
the journals or letters written during the convention.]  Van Cleve claims that
this would have occurred in many states, but others (Massachusetts) would have
paid their debt at close to 100 cents on the dollar.  This would have caused
bitterness on the part of, e.g., former soldiers who would be compensated far
differently depending only on which state they resided in.

The problem with selling the western lands is that they were so cheap.
Congress had only received $0.10 per acre in previous sells.  Indian tribes
also obstructed the work of surveyors.  Few leaders regarded this as a
reliable solution to the government's debts.(69)

The failure of the requisitions along with the failure of workable solutions
like the two proposed above convinced many leaders that Congress needed new,
compelling tax powers.

From the Treaty of Peace, Americans were required to repay debts owed to
Britain.  The majority of these debts (~84%) were held by Southern states.
Many states refused to honor this obligation of the treaty and instead passed
laws that denied British creditors the right to take American debtors to
court.  Congress could not compel the states to abide by the terms of the
treaty, and state legislatures were at the mercy of their constituents to pass
such laws.  Patrick Henry of Virginia became a champion of preventing British
creditors from getting what was owed them.  In response, the British refused
to evacuate the forts in the western lands.(69-72)

Summary on p. 72.

### Chapter 3:  Republic and Empire

States chose not to pay the requisitions not because they were unable but
because the payments were unpopular, and granting taxation powers to the
federal government failed because a central tax conflicted with the economic
and political interests of various states.(75)

AoC intentionally separated powers that had both been held by the colonial
legislatures:  Congress alone could determine how much money needed to be
raised and how it should be spent rested in Congress, but the power to raise
those taxes was the sole power of the states.(76)

Taxes were raised in all states after the war to pay the war debts.  These tax
increases were hugely unpopular everywhere, but Van Cleve reaches the
conclusion that the taxes placed on the people of the various states were not
overly burdensome.  While there would have been a minority of individuals
(mostly western farmers) who could not pay the taxes except by massive
liquidation, raising the taxes on the wealthy would have been sufficient to
cover the national debt -- though, such a "solution" would have been
politically impossible to implement.  States preferred to pay creditors who
were residents before paying the Confederation creditors (and not paying
British creditors at all) because state creditors held political sway.  Laws
were also passed -- by popular support -- in many states that made it more
easy to go delinquent on taxes.(76-84)
(NOTE:  What about "Debtors had very few laws (like bankruptcy) protecting them 
from creditors and could be thrown in jail for failing to pay debts." from Ch. 1
above?)

Feb. 1781 Congress tried to obtain an "impost" or power to tax imports.
Eleven states approved these powers until Rhode Island's legislature
unanimously rejected the proposal.(84-5)

Those against the import taxes argued:
* The sovereignty of the states -- including their sole power to tax -- was
  one of the revolutions central achievements.
* It gives the federal government a revenue independent of the people -- which
  would result in aristocracy or monarchy.(85-6)
* [Someone argued something along the lines of "the sword and the purse must
  not be held by the same hand" -- that Congress should not have both the
  power to declare war *and* collect taxes, but I can't find the quote now.]

Rhode Island specifically argued:
* The impost would necessitate more government officials and the expenses that
  would come along with that.
* It would burden commercial states (like Rhode Island) more than others.
* It would require a standing army to implement.
* (Privately, Rhode Island politicians worried that losing the revenue from
  trade would require them to raise other taxes which would cause their state
  to "dwindle into insignificance.")(86-7)

Supporters argued that Congress had a duty to pay its debts, and Confederate
taxation was the only way to insure they would be paid.  Congress could be
trusted with the added powers because the members of Congress were "the
people", too.(86)

Just as Congress sent a delegation to RI to persuade them to reconsider,
Virginia revoked their support of the impost, killing the plan.(87)

Throughout the 1780s, Confederate taxation powers were supported by the
majority of Congressmen on both sides of the isle -- nationalists and
federalists.(89)

Nationalist leaders, such as Madison, felt that if the federal government
could not relieve its debt, then the Union would dissolve under the strain,
leading to civil war or European imperial aggression.(89-90)

In April of 1783 Congress introduced a tax proposal that would grant impost
powers as well as a "supplemental" tax to be paid by the states.(89)  The
supplemental tax would be paid based on state population (slaves counting as
3/5 of a person).  Massachusetts felt that the supplemental tax -- which would
affect the southern states more -- would offset the burden of the impost tax
on the northern states.  Southern states (even with their disproportionate tax
burden) were more supportive of Confederate tax powers than the northern
states.(93)

Southern states (such as Virginia) worried about their defensibility.  If the
Union dissolved, then interstate conflict might lead to foreign intervention.
Madison and Washington in particular believed Confederate tax powers were
necessary to secure Virginia's defense.(94)  Gerry from Massachusetts, on the
other hand, believed that his state militia was sufficient to defend
Massachusetts.(96)

"There is a natural and necessary progression, from the extreme of anarchy to
the extreme of Tyranny;... arbitrary power is most easily established on the
ruins of Liberty abused to licentiousness." -- George Washington(95)

Federalist opponents worried that Confederate taxes would necessarily lead to
a standing army as had happened in Britain under the 17th-century Stuart
kings.(95)

Only eight states ultimately accepted the impost portion of the tax plan (one
short of making it law), and only five accepted the supplemental portion.  The
supplemental tax was extremely unpopular in Massachusetts and was one of the
catalysts of Shay's Rebellion.(96-7)  New York was the hold-out preventing the
passing of the impost.  Under the governorship of George Clinton, the state
political climate had moved away from any direct taxation which was unpopular
with western farmers.  They relied on their own import taxes for money and
were unwilling to give up the revenue.  (Connecticut and New Jersey residents
paid more than a third of New York's import taxes, and so were the largest
single funders of the government of New York.  They of course resented this,
but they and the federal government could do nothing to prevent it.)(96-9)

Summary starting on p. 99.

### Chapter 4:  Protecting American Commerce in an Imperial World

In the 1700s, even after the publication of *The Wealth of Nations*, it was
normal for European powers to close their and their colonies' borders to
foreign trade.  Willingness to permit foreign trade relied primarily on
military alliances.(104-5)  America after the war had difficulty negotiating
trade with European countries.  Britain (and Spain and France) went so far as
to try limiting American trade.(106)  The Earl of Sheffield stated that
Congress was so weak that Britain didn't need to fear retaliation for trade
limitations, and even if they did retaliate, Britain had enough power to
destroy the US carrying trade.(106-7) The trade limitation hit New England and
Massachusetts in particular the hardest.

By the end of 1783, pirates of the Barbary Coast had begun capturing ships,
enslaving sailors, and either ransoming or selling both.  This decimated the
remaining trading in New England.(107-108)  Massachusetts merchants were at
the forefront of demanding Congress the power to exercise retaliatory trade
powers.(108)  There was very little overlap between the trade interests of New
England and the South.  The trade powers that Massachusetts, e.g., desired
were viewed with indifference by southern states.(109-10)

Many [who?] were opposed to the growth of commerce itself believing either
that it allowed the spread of luxury which undermined republican virtue or
that merchants by their nature were greedy, unpatriotic monopolists.(110)

In light of trade difficulties, Congress proposed that the states grant them
trade powers in order to retaliate against the European powers.  "[I]n the
imperial world of the 1780s, international trade was regarded as a matter of
strategic national concern, not as private enterprise whose conduct should be
left to free markets."(111)

Congress suggested power to ban foreign nationals from trading with the US on
ships they owned (unless their government had made trade negotiations with the
US) and also banning foreign nationals from importing good from another
foreign country (unless the same).(111)  Contemporaries viewed the power as
too weak to work because they were only powers to prohibit, not to regulate --
the sole power of the states.  Even though Congress could approve trade
treaties, they had no power to force the states to abide by them.  Different
states had different trade goods and foreign markets -- why would one state
impact their own trade to protect the merchants of a different state?  It was
unlikely that all 13 states would approve the new powers.

Individual efforts by the states to control trade usually turned out to be
more costly to their American neighbors and did little to change British trade
policy.(113)  Neither treaties or policy changes at the state or national
level had any effect on the negative trade policies of France, Spain, or the
Barbary pirates.(114)

Congress in 1784 proposed "radical", perpetual trade powers over all foreign
and domestic commerce as well as the power to prohibit, regulate, or tax the
same.  (This is called the "Monroe proposal".)  The debate over commercial
powers was becoming more and more linked with debate over trade taxation.
Opposition to the new powers came primarily from the southern states.  Richard
Henry Lee argued:
1. It was dangerous to liberty and national security to concentrate this much
   power in Congress.
2. Different states had different interests.  The northern states would
   protect their shipping interests at the expense of preventing competition
   in the South.  (Southern states preferred to ship their exports on the
   cheapest ships regardless of whether they were foreign or domestic.)
3. Any attack on the Confederation's existing structure was dangerous and
   would weaken it.
Even the "eastern" states who would have benefited from the proposal hesitated
to hand-over that much power to the federal government.(115-6)

In response to the demands for increased commercial powers, Massachusetts
politicians passed laws banning all foreign countries from exporting goods
from Massachusetts.  They hoped that other states would patriotically follow
suit; however, because other states failed to follow their lead, foreign ships
docked at the ports of other states.  The laws were abandoned a year
later.(117)

In mid-1785 the Massachusetts legislature sought congressional approval for a
convention to reform the Confederation.  There was opposition to this proposal
from Elbridge Gerry, Rufus King, and Samuel Holten.  They argued that any new
commercial powers needed to be temporary, not permanent as was the case with
the Monroe proposal.  Second, they worried that changing the Confederation by
bypassing Congress would lead to aristocracy.  The new trade laws would give
rise to a standing army that would defend and be supported by the tax
revenue.  They worried that a convention would change the representation of
the states in Congress and that that would destroy the republic.(118-9)  After
hearing their opposition, the legislature repealed their call for a
convention.  Rufus King was presently content to let the states regulate their
trade, but he began to privately advocate for the formation of a separate
"sub-Confederation" for commerce to remedy the economic distress in the
northern states.(120)

Washington believed that the interests of the majority in the union -- the
northern trade policies -- should be submitted to by the minority -- the
southern states.(121)  Most Virginia politicians by contrast disagreed with
his views.

In Oct. 1785 Virginia attempted similar reforms as Massachusetts.  The
legislature supported granting new commercial powers to Congress including the
power to prohibit or tax cargo that came from countries with no treaty with
the US, a 5% import tax, and no duties on interstate commerce.  The proposal
was abandoned when opponents tried to put a time limit of 13 years on the
powers.  Madison, its biggest supporter, defended the abandonment:  "I think
it better to trust to further experience and even distress, for an adequate
remedy, than to try a temporary measure which may stand in the way of a
permanent one."(123)

John Tyler of Virginia, hearing of Massachusetts's similar proposal, asked
their congressmen to support a convention in 1786 in Annapolis to consider
trade reform.(123)  The three day meeting was attended by full delegates from
only three states and mere observers from two others -- Maryland didn't even
send delegates.  The agreement that was met there (which was an agreement all
the present delegates believed even before the convention) was that commercial
issues and reform could not be separated from broader reforms -- including
taxation -- to the Confederate government.  The Annapolis convention proposed
a broader convention take place in Philadelphia in 1787.  Given the poor
turn-out for the Annapolis convention, it is reasonable to assume that the
Annapolis proposal had little to do with getting 12 states to attend the
constitutional convention.  Van Cleve calls it a "coincidence".(124)

Theodore Sedgwick was so disappointed with the outcome of the Annapolis
convention that he, similar to King, proposed that the northern states split
from the southern and form their own Confederation.

Summary on p. 129.

## Part Two:  Western Expansion Strains

### Chapter 5:  "Astonishing" Emigrations and Western Conflicts

Massive migrations out of the eastern seaboard and into the western lands
granted by the Treaty of Peace filled the west with settlers.  These settlers
often fought bloody and costly wars with the Indian tribes of the region who
did not know or care that Britain had granted their lands to the Americans.
The US government was unable to protect the new settlers due to lack of funds
and the size of the waves of emigration.  The emigrants were mostly poor and
were looked down upon by most authorities.  Some settlers wanted to form their
own states while others just wanted the Confederation to leave them
alone.(134-7)

Sept. 1783 Congress had criminalized the settlement of Indian lands.  They
hoped to make treaties or otherwise buy the land from the Indians for
settlement, but the lands were settled regardless.  This made many leaders
worry that settlers would create a new Indian War that the Confederation would
not be able to fight.  The Confederation managed to gain claims to all the
western lands from the states (the last hold-out being Virginia), but they
were powerless to gain any advantage from their control nor were they able to
form any governments in the western territories until the constitutional
convention was already underway.(137-9)

In an ordinance of 1784, Congress divided up the northwestern territories into
ten potential new states.  They would start out as self-governing territories
under the authority of the Confederation, but they would eventually achieve
full statehood.  Jefferson added a provision that slavery be banned in the
territories after 1800, but the proposal lost narrowly.  The rest of the
ordinance never went into effect.  Many leaders voted against it because it
threatened to upset the present division of sectional powers.  They worried
that eventually the Confederation would be dictated by frontier agricultural
interests.(139-40)

Edmund Pendleton feared that taxes would continue to rise in the eastern
states as emigrants fled towards the interior.  Washington feared that
settlers would eventually secede from the Confederation and possibly ally
themselves with foreign powers.  This view was shared by Arthur St.
Clair.(148)  Most opponents of emigration admitted that there was no way to
limit settlement of the western lands, but they proposed various schemes that
they hoped would either provide revenue for the Confederation or ensure the
settlers would remain allied with the Confederation.  Washington's worries
were well-founded as the mid-1780s saw strong separationist movements in
Kentucky, Tennessee (the state of Franklin), and the Wyoming Valley of western
Pennsylvania.(140-3)

In April 1785, Congress sent Confederate troops into the Ohio Valley to burn
settlers' homes since they squatting on Indian lands, but the settlers just
rebuilt their homes when they were gone.  Congress did not have sufficient
power to limit emigration, avoid Indian war, or acquire revenue from the
territories.

Settlers continued to skirmish with Indians and encroach on their land.  The
Confederation stole the land of the Ohio River Valley from the Iroquois.(143)

In an April 1785 ordinance, Congress drew up laws for settlement of the
territories that they hoped would slow emigration (it didn't), but it made no
provision for governance of the territories because Congress was divided on
whether or not to allow slavery in the territories.  Rufus King, like
Jefferson before him, was against allowing slavery in the territories.  The
ordinance divided up the land into 640-acre parcels which favored land
speculators since poor settlers could not afford the land.(144-6)

Spain made a treaty with the Creeks in which they gifted them guns, balls, and
powder with the intent of using the Creeks to limit emigration from
Georgia.(149)  The British likewise made military alliances with the Iroquois.
They traded with them at the western forts which they refused to give up, and
provided the Iroquois with ammunition.(150)

The continued skirmishes with Indians, mainly in Kentucky, made many leaders
desire a standing army that could defend Confederate lands and settlers, but
Congress did not have the money to call new troops.  Most contemporaries seem
to place the blame squarely on the white settlers.  Governor Henry of Virginia
said that if the Confederation would not defend Virginia's settlers, then the
settlers would have no reason to remain loyal to the union.  Although, he also
admitted that war with the Indians was the settlers' fault.(153-5)

Henry's request that the Confederation do something about the bloodshed is
telling.  To defend its own settlers in Kentucky, the Virginia legislature
would have to approve the calling up of a militia *and* an increase in taxes
to pay for the militia.  Since the settlers that were being defended would not
be paying those taxes, Henry feared that the motion would be defeated.(156)
Virginians began longing for a stronger federal government which could protect
them from Indian attacks.(159)

Conclusion on p. 159.

### Chapter 6:  The Spanish Treaty Impasse and the Union's Collapse

Free navigation of the Mississippi river was vital to the continued settlement
of the western territories.  In fact many western settlers believed they had a
right to the river even before the 1783 Treaty of Paris which explicitly
granted them the right to its use.  Spain knew about the provision but
remained silent during its negotiation.(162)  In 1784 in an effort to limit US
influence in the region, the Spanish closed the river to American trade.
Spain asserted control not just over the Mississippi but also over southern
territories of the US at least as far as the Ohio river, and increasing
emigration into the region worried the Spanish government.(163)

Spain sent Don Diego de Gardoqui to negotiate a treaty with the US to cede
control of the Mississippi to Spain in exchange for commercial advantages for
the northern states.(163)  John Jay was appointed to be the diplomat to
negotiate with Gardoqui and was given instructions by Congress (unanimously
agreed to) to secure the reopening of the Mississippi.(164)  The advantages
for New England in the proposed treaty included selling fish and naval timber
to Spain and transport them on American ships.  There were no such advantages
for Southern states (for example, tobacco was explicitly excluded from the
treaty's provisions).(165)

Rufus King (MA), Charles Pettit (PA), and James Monroe (VA) worked with Jay on
the negotiation.  King believed that if the Mississippi were reopened that the
Confederation would never see benefits from western settlement.  All trade
would be confined to the Mississippi and no taxes or revenue would be gotten
from the settlers.  He believed the western territories would secede.(166)
King also recognized the economic benefits for New England and the
Mid-Atlantic states.  To reject the treaty would be to go to war with Spain, a
war that the US would lose and would end up in worse shape.(167)

Monroe, in contrast, believed that agreement of the treaty would push the
western settlers away from the Confederation:  "We separate those people I
mean all those westward of the mountains from the federal government and
perhaps throw them eventually into the hands of a foreign power."(167)
Madison sided with Monroe in condemning the proposed treaty.

George Washington believed that Congress should delay as long as possible in
addressing the treaty at all -- neither overtly relinquishing nor supporting
claims to the Mississippi.  He believed Congress should support efforts to
increase trade between western settlers and the eastern states.  By the time
the western states grew populous enough to *really* need the trade, Spain
would not be able to stop them.(168)

Washington probably overestimated the patience of the western settlers.  They
were already demanding war with Spain over the closing of the river and had
retaliated by attacking Spanish settlers in Vincennes.  Congress did not have
the authority or army required to prevent the settlers from starting a war
with Spain, and many congressmen, Jay included, believed the treaty issue had
to be resolved to avoid such a war.(169)

Thomas Jefferson believed trade on the Mississippi was necessary for the
expansion of the US.(170)  Henry Lee, Jr. supported the treaty but worried
that agreeing to the treaty would lend more support to opponents of expanded
federal powers.  Charles Pinckney (SC) responded to Jay's arguments in favor
of the treaty with counter-arguments that historians regard as "entirely
conclusive to the reader today"(170):

1. Spain was not a useful political or commercial partner, and the proposed
   treaty did not significantly expand current trade between the two nations.
2. If they were justified in allowing the closure of the Mississippi in order
   to limit western emigration, then Britain was justified in maintaining
   their western forts.
3. The "treaty" was primarily a means of dividing the Confederation.
4. If the treaty was ratified, then the South would never grant new powers to
   Congress.
5. Congress did not have the power to enforce the treaty even if it was agreed
   to.

King denigrated the interest in western expansion saying that they were
putting the "views of speculating land jobbers" before the interests of a
million people.  He argued that agreeing to the treaty was the only way to
prevent war with Spain.  He implied that Massachusetts would secede if it came
to that.(171)  Lee pointed out that if, on the other hand, the treaty was
agreed to, then the southern states would secede instead.  Either outcome
would lead to the dissolution of the Confederation.  Both King and Arthur St.
Clair (PA) voiced concerns about western emigration and hoped that a treaty
with Spain would weaken or halt it.(172)

Southern states secretly met with French diplomats to ask France to intercede
with Spain on their behalf.  France was an important ally of Spain at the
time, and the effort demonstrates how desperate they were to resolve the
issue.  France refused to intervene.(173)

In August 1786 Congress voted 7-5 that Jay's original instructions to reopen
the Mississippi be repealed.  The votes were divided along sectional lines.
Even though the AoC required at least 9 states to agree to any treaty, the
northern states insisted that only a simple majority was needed to repeal the
instructions.(173)

The divisions between the states on this issue led many prominent statesmen in
both the north and the south to consider forming separate confederations, at
least for commercial interests.(174-6)

The arguments on the treaty turned out to be so divisive that Congress was
unable to come to any new agreements on any other laws or reforms for the year
before the Constitutional Convention.(176)

(See a letter from a western settler on the treaty issue on p. 178)
Essentially, western settlers believed that agreeing to the treaty would
retard any economic development in the area.  They implied that they would go
to war with Spain themselves and secede from the Confederation if it came to
that.(178)  The issue even split Pennsylvania politics down the middle, but
both sides of the debate refused to adopt any resolution on the treaty so
close to the Constitution Convention indicating that no one wanted to divide
potential supporters of the convention over the treaty.(179)

The vote had also weakened support among the southern states for Congressional
reform.  Patrick Henry, whom Madison had admitted was once a supporter of
reforms, turned against the Confederation as a result of the vote.  He said he
"would rather part with the confederation than relinquish the navigation of
the Mississippi."  Henry would go on to decline his involvement as a
convention delegate.(180)  Madison was convinced that the treaty issue by
itself might prevent Virginia from ratifying a new constitution.(181)

Massachusetts declined to push the matter of the treaty since it was involved
in keeping down an armed rebellion (Shay's Rebellion) in late 1786.(181)

Gardoqui stated that he did not believe the Confederation could enter into any
treaty at all and anticipated the dissolution of the union.  The French
government instructed its diplomat to do nothing to support or encourage the
Spanish treaty and implied that dissolution would be in France's
interest.(182)

Congress asked Jay how the negotiations were going with Gardoqui, but the two
could not agree on satisfactory trade rights along the Mississippi.  Jay said
that he could see no way to appease all the states.  Either they agreed to a
treaty that some of the states did not support, or they waged a war that other
states would not support.  He could see no way out of the impasse.  Madison
tried to win some support from the southern states for the treaty by
suggesting to Congress to move the negotiations to Madrid and have Jefferson
take over for Jay.  Nathaniel Gorham (MA) argued that he wished to see the
Mississippi remain closed as it benefited the Atlantic states.  Madison then
tried to get Congress to reject the earlier vote as it had not been approved
by 9 states.  That motion, too, was rejected.(183)

Afterwards, Madison tried to convince others that the project was dead, that
no resolution one way or the other could be made on the issue.  He worried
that if anyone thought there was still a chance to resolve it, then it would
undermine the work of the Constitution Convention.(184)

By 1788 there was little remaining support in the north for the treaty.(184)

Summary on p. 185.

## Part Three:  Internal Divisions:  State Social Conflicts

### Chapter 7:  Economic Relief, Social Peace, and Republican Justice

There were two forms of economic relief that were experimented with by the
state governments during the period immediately after the revolution:  debt
relief and paper money.  Both experiments started during the war and had vocal
opposition.(191)  Both also touched on two fundamental political questions
during the 1780s:

1. "Should there be limits on the powers of republican majorities to provide
   economic relief if it harmed some people as well?"
2. "What, if anything, should be done about the significant interstate harms
   that many people thought were caused by some states' economic relief
   policies?"(192)

With respect to paper money, both Madison and Washington opposed paper money.
Madison believed that paper money was an unfortunate solution for the nation's
loss of specie due to trade.  The better solution would be to increase exports
instead.  Many other saw paper money as "legally sanctioned fraud" and
"inherently subject to political abuse."(193)  Washington believed only
debtors benefited from paper money and "the creditor and body politic
suffer."(193)

On the other end, Ben Franklin thought paper money issuance (particularly
for "land-bank" purposes -- see below) as successful ways to stabilize
economies and foster growth.  For Franklin, paper money depreciation was a
form of taxation -- as tax imposed largely on the wealthy and therefore
acceptable.

Currency depreciation acted as a form of wealth redistribution.  Washington,
Jefferson, and Henry Laurens (SC) had all lost significant amounts of money
during the war when their debts were repaid with deprecated paper money.

The main supporters of paper money argued that it was necessary to pay taxes
and debts in the absence of specie.  Using money instead of gold or silver
would help the economy by preferring domestic production over imports.  "And
paper money would allow taxes that largely benefited public creditors to be
more easily paid by non-creditors."(194)

Views on paper money correlated strongly with views on economic relief as a
whole.

(See 194-5 for summary of distinctions in how states handled economic relief.)
Massachusetts was one of the few states that passed no laws for economic
relief.  Their legislators opposed economic relief on principle.
(Interestingly, they were the only state in which saw armed rebellion.)

In South Carolina paper money was a "land-bank" law, meaning it was basically
a government loan to assist small farmers and other land-rich but cash-poor
planters.  SC extended debt repayments for several years.  SC did not make
paper money legal tender.  But SC did make property legal tender (via the Pine
Barren Act); creditors had to accept land at 3/4 of its appraised value which
was often significantly below its actual value.(196)  State merchants refused
to pay taxes because they could not get debtors to pay.  Because debtors
outnumbered creditors, republican majorities gained unlimited power over
public and private contracts -- characterized as tyranny of the majority.(197)

North Carolina's paper money had almost as bad a reputation as Rhode
Island's.(198)

Pennsylvania issued paper money, but their method of doing so was criticized
for helped creditors more than those who need it.(198)  The Pennsylvania laws
encouraged speculators to buy debt cheaply (such as the military certificates
of revolutionary soldiers) and make a profit on later government payouts.(199)
PA rejected making money legal tender.  They raised taxes and increased
collection in order to retire the money.  The money deprecated so much that it
stopped being a medium of exchange except for paying taxes so the state was
the biggest loser of the deal.(199)

PA had a heated debate over whether to revoke the state charter of the Bank of
North America -- a for-profit bank that many farmers resented because it would
not loan to them.  Critics accused the bank of fostering an aristocracy.  The
revocation of the charter and the post facto legal tender status of paper
money raised the question of whether republican governments should have the
power to modify or invalidate existing contracts.(200-1)

Thomas Paine broke ranks with his usual populist supporters in criticizing the
revocation of the charter and paper money -- both of which he believed were
unconstitutional.  He thought paper money and legal tender laws would only
harm honest laborers.  After Paine's criticisms, enemies of paper money
and friends of the Bank of North America won a narrow majority in the PA state
legislature.(202)

New York's laws and experience with economic relief were mostly similar to
PA's.(203)  Van Cleve claims that NY's paper money held its value remarkably
well.(204)

New Jersey saw its own divisive fight over paper money.  William Patterson
thought that republicanism required virtue (i.e. paying one's debts).  An
anonymous proponent of paper money policies stated that in a republic
government "the majority of the people bear rule, and it is up to them to
determine whether a proposition is unjust."(204)  In NJ, farmers were a large
majority of voters and wanted land-bank paper money policies.  NJ made paper
money legal tender.  They also adopted but quickly repealed property-tender
legislation.(205)

Rhode Island politics was completely dominated by economic relief proponents.
Per capita, RI issued twice as much paper money as any other state.  RI issued
heavy fines against anyone who did not accept paper money as legal tender.
Within a year the paper money had depreciated by 75%.  Merchants left the
state or closed their stores rather than accept the depreciated currency, and
riots broke out with severe damage to property.  Many debtors took advantage
of the law.  RI changed the law to prevent this but only for in-state
creditors.  RI also paid off its entire state debt using the paper money at a
huge loss to their creditors.  RI voters and legislators wanted to pay-off the
state debt without having to raise taxes.(207)

Other states waged war with RI by limiting the rights of RI creditors from
collecting their debts from their own inhabitants.(208)

Many statesmen -- nationalists and federalists alike -- opposed paper money,
but before the Philadelphia Convention there seems to have been no movement to
make it illegal at the federal level.(210)

In places other than Massachusetts there was minor armed riots, but they were
usually put down without violent confrontation.(211)  Van Cleve argues that
many who would have supported insurrection instead moved west; the western
territories provided a valuable "safety valve" for people upset with their
governments.

Summary on p. 212.

### Chapter 8:  Shay's Rebellion

Before Shay's Rebellion there were a number of Massachusetts leaders,
including Benjamin Lincoln, who were calling for a dissolution of the
Confederation so that the northern states could separate form the southern to
suit their own political desires.  Before the rebellion broke out Rufus King
(MA), John Jay (NY), and Washington (VA) all saw an immanent collapse of the
Confederation due to insolvency.(217)

Washington believed that a strong national government was required to enforce
laws for the public good and to collect taxes to fund that force.  Taxation
was his primary concern.(218)

Both nationalists and federalists saw the same issues with the Confederation,
but they approached them with different solutions.  The former believed a
stronger government was required to bring the different states in check.  The
latter believed that the Confederation should be dissolved and the states
should go their own way.(218)

The economic conditions in western Massachusetts were among the worst in the
union.  Farmers petitioned the government to enact reforms such as paper money
to help with paying debts and taxes.  They felt that they would soon lose
everything they had fought to keep in the revolution.(219-20)

Because of tax delinquencies the Massachusetts treasury was empty by the time
rebellion broke out.  The tax obligations were lower on the wealthier eastern
side of the state than they were in the west.(220)

The punishments for failure to pay debts were extremely high.  Court costs
even for defaults were high and losers had to pay the winners' attorneys'
fees.  The population called for changes to the tax and debt laws, but the
legislature turned a deaf ear, refusing to issue paper money or any economic
relief.(221, 223)  In response "Regulators" (as the insurgents were called)
blocked the state courts.(222-3)

Regulators saw themselves as continuing the tradition of the (unfinished)
revolution against oppressive tyranny.  Massachusetts leaders on the other
hand saw the insurgency as a repudiation of republicanism.  The people had
elected representatives to make political policies.  Now they were running an
end-around to try to force those representatives under threat of violence to
enact the laws they wanted.  It was extreme democratic mob-rule.  However,
even though many disagreed with the means of rebellion, they were often
sympathetic to the plight of the indebted Regulators.(224-5)

An emergency legislative session was called to address the problem.  They
suspended habeas corpus and provided some minor debt relief but declined to
accede to the primary demands of the Regulators such as paper money and
large-scale debt relief.(226)

Secretary of War, Henry Knox, convinced Congress to authorize Confederation
troops to protect the Springfield arsenal and combat the insurgency, but the
Confederation was insolvent and no one was willing to lend the government
money.(226)  The state government also lacked the money to assemble troops, so
time was taken to collect loans from wealthy merchants to fund the army.
After raising the army, the rebels were put down with ease.(227)

After Shay's Rebellion, the western counties received more representation in
the state legislature in the next election and enacted some of the demands --
adopting a tax-holiday and property-tender legislation.(229)

Massachusetts leaders had initially believed that the debtors had only
themselves to blame for their debt, buying imports they could not afford.
Many, including Abigail Adams, seem to think that luxury was undermining the
republican spirit of the people.(230)  Later, leaders like Rufus King thought
that it was overbearing debts and taxes which led to the rebellion.  Other
"archconservatives" such as Theodore Sedgwick thought the fault lied with
democracy and a desire by the lower classes to engage in wealth
redistribution.(231)

The rebellion caused a change in many of the leaders in Massachusetts to
consider increasing the powers of Congress after having seen how impotent
their own government's response had been.(231)  Although many laid the fault
on the weakness of the Confederation which had forced Massachusetts to fend
for itself.(234)

The rebellion made King question his political ideals.  He wrote:  "[M]y
Opinions have been established upon the belief that my country men were
virtuous, enlightened, and governed by a sense of Right & Wrong... there is
but too much reason to fear, that the Framers of our constitutions, & Laws,
have proceeded on principles that do not exist."(232)  Many federalists like
King joined the nationalists in calling for constitutional reform.

Edward Carrington (VA):  "Man is impatient of restraint; nor will he conform
to what is necessary to the good order of society, unless, he is perfect in
discernment and Virtue, or, the Government under which he lives, is efficient.
The Fathers of the American Fabric seem to have supposed the first of these
principles, peculiarly, our lot, and have chosen it for a foundation:  in the
progress of experiment, the fallacy is discovered, and the whole pile must
fall, if the latter cannot be supplied."(233)

Shay's Rebellion was largely the product of political and economic
circumstances local to Massachusetts due to its insistence on refusing debt
relief measures, especially paper money.(235)

Statesmen like Thomas Jefferson and Thomas Paine saw no real worry in Shay's
Rebellion.(238)  Many other statesmen -- Lee, Washington, and Madison -- may
have been unnecessarily worried by initial false reports of the size of the
insurrection and a fear that it would spread to other states.(239-40)  For
Washington in particular the rebellion was proof that he had been right all
along about the need for a stronger federal government.(241)

Summary on p. 242.

## Part Four:  Confederation Collapse and Its Consequences

### Chapter 9:  "The Truth Is, We Have Not a Government"

(Overview of the positions of states and individuals leading to the proposal
for a Constitutional Convention.  These have all been expanded at length in
the preceding chapters.)(245-52)

Virginia approved the Philadelphia Convention Nov. 1786.  The inability to pay
the nation's debts was the primary reason for the approval.  They hoped for
stronger, more extensive tax powers at the national level.  Importantly, it
also imposed no limits on its delegates as to the sorts of reforms that could
be made.  This signaled to other states that they were willing to
bargain.(252)

Even though opposition to the convention still existed in Virginia politics,
many who were otherwise reluctant were willing to see the experiment through
because public opinion had shifted to believe that neither Virginia nor
Congress had the power to protect western settlers from Indian attacks or
foreign influence.(253)  Many western settlers now believed that a stronger
government than Virginia or the Confederation was required to protect them
from Indians and force the British from their forts.(254)

Patrick Henry and his allies opposed the convention but did nothing to prevent
sending delegates.  They believed that if they attended the convention, then
they would be tied to supporting the final agreement, but if they stayed away
from it, they could oppose whatever final reforms were made.(255-6)

In Massachusetts, Rufus King and Nathan Dane initially opposed the convention
on the grounds that only Congress could propose amendments to the
Constitution.  But Shay's Rebellion convinced a number of statesmen that they
needed a stronger federal government to protect them from armed
insurgency.(256-7)  Even though King and Elbridge Gerry were not in full
support of the convention (their views had changed somewhat over the course of
Shay's Rebellion), King told Gerry that he should attend the convention
anyway.  They saw that it would create a new government, and they did not want
their side to be left out of the negotiations.(258)

The MA Senate -- at Samuel Adams's suggestion -- initially limited their
delegates to only consider issues related to trade and commerce.  They were
instructed not to interfere with the equal representation of the states in
voting in Congress.(258)  Several weeks later, the limits were removed from
the instructions (over Adams's protests).  This was likely due to Congress
approving the Philadelphia Convention on Feb. 21, 1787 -- an act which
demonstrated Congress's lack of confidence in itself or the present
Confederation.(259)

By late Feb. 1787, New York legislatures saw no reason to change the status
quo of the Confederation.  They had just voted down the Congressional impost
tax which allowed them to continue taxing imports and keeping their domestic
taxes low.(260)  Alexander Hamilton acted on a creative strategy to send
delegates to the convention -- on Fed. 17 he got the lower house of the New
York legislature to agree to send delegates *if* Congress agreed to support
the convention.  The motion was split along sectional lines -- senators from
the area around New York supported reforms, but rural representatives did
not.(261)  Hamilton then got Congress to approve the convention.

Afterwards, NY senators sought to tie the hands of their delegates by
requiring them to not make any changes to the AoC which were inconsistent with
the NY constitution which would have prevented any changes to the
Congressional tax powers, but this amendment was defeated by one vote.(263)

Rhode Island was the only state to ultimately boycott the entire convention.
The reason they publicly gave for doing so was their high regard for the
current Confederation; however, during the convention, they failed to enact
into law a tax which would have helped pay their requisitions to Congress.
They also expanded their program to forcibly retire state debts using their
deprecated paper money.(265)

Washington, even though he supported reforms, was at first reluctant to attend
the convention.  He worried that the convention would fail, and his reputation
would be tarnished by it, denying him the opportunity to lead when the
Confederation ultimately collapsed and Virginia was on its own.(266)

Even after agreeing to attend the convention, Washington told Madison that he
did not want to participate if the convention was not interested in pursuing
radical reforms.  He said he wished to "probe the defects of the Constitution
to the bottom."  These instructions gave Madison the courage to make his
radical proposals.  Washington wanted a continental empire capable of
enforcing taxation and protecting its territories.(268-9)

Madison worried that if they could not devise a better government at the
convention, then Congress would dissolve into a monarchy or separate
confederations.  (See "Observations on the Vices of the Political System of
the United States" for Madison's opinions on the current government.)(270)

Madison thought that there needed to be some provisions in the new
constitution to protect the minorities from the tyranny of the majority.  He
thought this extended not just to religious minorities but to the wealthy
elites (i.e. Shay's Rebellion).  He believed that the majority could be in the
wrong and could oppose the public good.  For example, he wrote to James Monroe
that even if all thirteen states had agreed to close the Mississippi in
support of the Spanish Treaty, "I shall never be convinced it is expedient,
because I cannot conceive it to be just."(273)

Madison saw majority tyranny as an inherent problem of republics and thought
it needed to be prevented at every level of government.  His solution in the
case of the Confederation was to essentially destroy the states by giving the
federal government veto power over every state law.  Van Cleve calls this
"tone-deaf" to Madison's opposition.(274-5)

Summary on p. 275.

### Conclusion:  The Birth of the American Empire

(This is a good section to re-read once you've gone through the Journals and
the Anti-/Federalist papers.  It gives a brief summary of how each debate in
the convention and afterward played out.)
