# The Law of Nations
by Emer de Vattel

## Preface
* 5 the natural law of nations is the application of the natural law of
  individuals to bodies of men, or *nations*
* 8 **but** we cannot simply the same natural law of individuals to nations
* 15 for example civic association -- the joining of men together into society
  -- does not seem as necessary to secure the liberties and security of
  nations as it does for men
* 15 yet:  "These societies have still, it is true, powerful motives for
  carrying on communication and commerce with each other; and it is even their
  duty to do it; **since no man can, without good reasons, refuse assistence
  to another man**." [emphasis mine]
* 15 independence is even necessary to a state in order to secure the best
  interest of her members
* 16 there is a distinction between the *necessary* and *voluntary* laws of
  nations
* 17 the necessary: a sacred law which all nations are bound to respect
* 17 the voluntary: a rule which the general welfare and safety oblige them to
  admit in their interactions
* 18 there is also the *arbitrary* law of nations: rules between nations which
  have been consented to (between/among the parties in question) through
  either treaty or custom
* [Might be worth reading Grotius one day.  Both Vattel and Blackstone have
  referenced him many times.]

## Preliminaries
* 67 nations are societies of men joined together for their mutual security
* 67 a society is a sort of moral "person" with rights and obligations
* 67 the "law of nations" is the rights and obligations between nations
  * 68 it is the law of nature applied to nations instead of men
* 68 rights are derived from duties and obligations
* 68 states exist in a "state of nature" towards one another
* 68 the laws of nations are not identical to individuals under the law of
  nature because societies are not identical to individuals
* 69 (footnote) "The great end of every being endowed with intellect and
  sentiment, is happiness."
* 70 the *necessary* law of nations is that which is derived from right reason
  and is immutable; it is how we distinguish lawful treaties from unlawful
  treaties and just from unjust customs or internal laws
* 71 nations may not infringe on another nation -- even when her customs are
  unlawful -- if it does not infringe on the formers' rights
  * 74 nations have the right of judging their own duty and no other nation
    has the right to compel them differently
* 72 "The general law of society is, that each individual should do for the
  others every thing which their necessities require, and which he can perform
  without neglecting the duty which he owes to himself".
* 73 the object of the society of nations is mutual assistance towards one
  another in so far as it does not interfere with her *first* duty to herself
* 75 rights and obligations:
  * "perfect right" allows one to compel another to fulfill their obligation
  * "imperfect right" provides for no such compulsion
  * "perfect obligation" gives others a right to compel our obligation
  * "imperfect obligation" only gives others the right to *ask* us to oblige
* 75 "Our obligation is always imperfect with respect to other people, while
  we possess the liberty of judging how we are to act: and we retain that
  liberty on all occasions where we ought to be free."
  * [I like that he focuses on obligations before rights, but it sounds like
    he is leaving himself enough loopholes to declare that he can do whatever
    he wants.]
* 75 small states are no less sovereign than large states; whatever is lawful
  for one nation is lawful for every other nation
* 76 every nation can maintain that she has justice on her side, and no other
  nation has the right to pronounce judgment against her; the party who is in
  the wrong is guilty of a crime against her own conscience
* 77 but the control of other nations does extend to violations on their
  rights and duties (e.g. ensuring domestic tranquility)
* 77 treaties are *particular* law and only bind the contracting parties
* 77 customs consecrated by long use are also particular in the same way
* 78 if a custom is useful and reasonable, then it becomes obligatory on the
  nations practicing it; they must expressly declare they will not observe it
  in the future in order to stop observing it
* 78 if a custom is unjust, every nation is bound to relinquish it
* 78 positive law of nations:
  * "voluntary" derives from presumed consent
  * "conventional" from express consent
  * "customary" from tacit consent
* 79 the necessary law is what a nation uses to determine what she must do to
  fulfill her duty
* 79 the voluntary law is what she use to determine what other states owe to
  her

## Book I:  Of Nations Considered in Themselves

### Chapter I:  Of Nations or Sovereign States
* 81 every society must have a public authority (called the "sovereignty") to
  act on its behalf -- and each member subjects himself to its authority
  wherein it is in the interest of the common good
* 82 definitions of democracy, monarchy, and "aristocratic republic" (with a
  senate)
* 83 talks about the union of weaker states with more powerful states and how
  they ought to maintain their sovereignty [NOTE: this probably came up during
  the debates (Martin perhaps?)]
* 84 the federal republic:  "their joint deliberations will not impair the
  sovereignty of each member, though they may, in certain respects, put some
  restraint on the exercise of it, in virtue of voluntary engagements."
  * examples:  ancient Greece, the Netherlands, and the "Helvetic body" [the
    union of Swiss cantons]

### Chapter II:  General Principles of the Duties of a Nation towards itself
* 85 "every moral being ought to live in a manner conformable to his nature"
* 86 to preserve and perfect his nature is a moral being (state's) first duty
* 86 perfection, in this case, is obtaining the end of civil society
* 86 the end of civil society is giving the public whatever they are in need
  of which promotes happiness -- e.g. property, a peaceful system of justice,
  and mutual defence against violence
* 87 every nation has a duty to preserve its political association
  * 87 unlike people, though, the duty is conditional rather than absolute --
    it may be dissolved by common consent
* 88 likewise the public body has a duty to preserve each of its members --
  people, towns, or provinces
* 89 all people must strive to perfect their society
* 90 "May luxury, that pest so fatal to the manly and patriotic virtues, that
  minister of corruption so dangerous to liberty..."
* 91 "a nation ought to know itself"

### Chapter III:  Of the Constitution of a State, and the Duties and Rights of the Nation in this respect
* 92 constitution is the order by which the society labors for its perfection
* 92 the importance of getting a constitution right from the outset
* 92 legal distinctions:
  * *fundamental laws* make up the constitution of the state
  * *civil laws* regulate the rights of citizens among themselves
  * *political laws* are made with the view of the public welfare
* 92 fundamental laws should be established "in a manner suitable to the genius
  of the people" -- they thus vary according to the disposition of the people
* 93 the laws of the nation (especially the fundamental) must be preserved
  with the utmost vigilance:  "Sudden revolutions strike the imagination of
  men [...] But we overlook the changes that insensibly happen by a long train
  of steps that are but slightly marked. It would be rendering nations an
  important service, to shew from history, how many states have thus entirely
  changed their nature, and lost their original constitution."
* 94 the people have a right to change their constitution with a simple
  majority
* 94-5 however, if the form of government is dissolved and a new one formed --
  e.g. from republic to monarchy -- the citizens who disagree are under no
  obligation to submit to the new government
* 95 the legislative power does not have a right to modify the fundamental
  laws
* 95 "the constitution of the state ought to possess stability"
* 96 no foreign power has the right to interfere in the domestic concerns of
  another nation

### Chapter IV:  Of the Sovereign, his Obligations, and his Rights
* 97 sovereignty is established only for the safety and advantage of society
* 100 all the rights and obligations that reside in the society to preserve
  and perfect itself reside also in the sovereign
* 100 sovereigns require all the knowledge possible about their nation and its
  people in order to govern properly
* 101 the sovereign is subject to the laws of his state; if they have
  legislative power, though they may legislate new laws, they do not have the
  right to change the fundamental laws of the society
* 102-3 however, they are above all civil penal laws; his function is too
  important to be distracted by affairs that do not involve the governance of
  the state
* 103 the prince must be treated as inviolable and almost sacred
* 104 but as soon as the sovereign breaks the laws of the constitution, they
  lose their authority, rights, and duties -- the people are no longer bound
  to obey them
* 108 if a prince's authority is absolute, he may still be opposed if he
  breaks the first principle of the nation -- the public safety -- but since
  any confrontation is expected to be violent, he should only be opposed when
  the nation can no longer bear his crimes
* 109 if the sovereign is limited, though, all legal, peaceful means should be
  used at the slightest sign that they are overstepping their bounds
* 109 in most cases the subjects of the crown should obey their sovereign
  without question, assuming that his commands are just
* 110 but no one ought to obey an order which goes against his conscience
* 111 and the people are allowed to resist violently if the sovereign
  threatens their lives
* 112 sovereigns should employ ministers to help administrate, but he must
  never give up his authority to them

### Chapter V:  Of States Elective, Successive or Hereditary, and of those called Patrimonial
* 113 an elected prince is considered a sovereign [compare to others,
  Montesquieu perhaps, who considered the people sovereign in such republics]
* 114 "the safety of the people is the supreme law"
* 115 if the nation fears the heir to the throne will damage the nation, they
  have a right to refuse him
* [patrimony is a form of government in which all power flows from the leader]
* 117 a prince may not divide his sovereignty among his heirs because it would
  break up the nation -- but this does not apply to, e.g., to colonies that
  would be granted independence by the mother country
* 118-9 if there is a question of successor, then the people have the right to
  decide their sovereign -- because once the sovereign comes into question,
  the power passes back to the people until the true sovereign can be
  determined
* 120 the nation has the right to set the conditions of succession and, by
  necessity, the rules of valid marriage and birth
* 123 when a sovereign is given the authority to choose his own successor, it
  is only a nomination -- the people have the final say

### Chapter VI:  Principal Objects of a Good Government; and first to provide for the Necessities of the Nation
* 126 the end of society is to secure property, justice, and safety
* 126 the first business of the nation is providing all the wants of the
  people
* 126 in order to meet the wants and needs of the people, there should be a
  sufficient number of able workmen in every profession
* 126-7 this should be done through wise regulations, not constraint
* 127 a worker who has been raised and educated by his country cannot lawfully
  leave it to work elsewhere
* 127 but this is a mild rule -- sometimes one leaves, learns in a foreign
  land, and then returns more able to meet their nation's needs than otherwise
* 127 the state ought to encourage labor and industry

### Chapter VII:  Of the Cultivation of the Soil
* 128 is the most important industry and requires the most encouragement from
  the state
* 129 citizens should honor the cultivator of the land, not look down on him
  because they represent a higher class
* 129-30 every nation has an *obligation* to cultivate their land because the
  hunter-gather mode of life cannot sustain the present size of the human race
* 130 public granaries should be kept during times of plenty in case of bad
  crops or famine -- and this should be prioritized over export

### Chapter VIII: Of Commerce
* 131 home and foreign trade
  * the former between individuals
  * the latter between nations
* 131 home trade raises the welfare of individuals
* [I think it is interesting that he thinks of domestic commerce as being
  between individuals, but foreign commerce happens at the level of the
  state.]
* 132 nations are obliged to encourage domestic trade because it raises the
  well-being of its citizens; foreign trade is a duty for the same reason
* 132 private property obliges one to sell goods one has no use of to one's
  neighbor at a fair price
  * [sell?  why not just give?]
* 133 just as every individual has the right to refuse goods, and no one can
  force another to buy his goods, so too does the state have every right to
  prohibit the import of foreign merchandise
* 135 nations and individuals are obliged to trade together for the common
  benefit of humanity, except when trade would be dangerous to the people, a
  nation may restrict such trade
* 136 if an individual, knowing their rights, does not defend one of their
  rights from encroachment, they are effectively relinquishing said right
* 136-7 trade may be cut off at any moment; the prior custom of trade -- even
  for a long time -- does not guarantee that trade must continue
* 137 likewise, if a nation has a commercial treaty with another, then the
  failure to exercise that right -- even for a long time -- does not
  invalidate the treaty
* 138 "monopoly in general is contrary to the rights of the citizens";
  however, there are exceptions, and a wise government would do well to set up
  *some* monopolies such as:
  * enterprises that require more resources than any single person has
  * enterprises that would become ruinous if not implemented with prudence
* 139 trade which increases specie in the nation should be encouraged; that
  which reduces specie should be discouraged -- called "balance of trade"
* 139 every state has a right to impose import duties

### Chapter IX:  Of the Care of the Public Ways of Communication, and the Right of Toll
* 139-40 public works and communication increase trade and diffuse wealth
  throughout the nation; they are a good form of welfare
* 140 the nation may justly oblige travelers and merchants to pay a toll for
  the maintenance of roads, bridges, canals, etc.
* 141 excessive tolls where no care is taken of the roads, etc. though is
  extortion
* 141 private property cannot deprive a man of passage

### Chapter X:  Of Money and Exchange
* 142 the state ought to have the exclusive right to coin money; if every
  individual could, fraud would become common, and specie would lose its value
* 142 the state ought to have money in store to provide for the common good
* 142 the debasement of coin is a debt the state owes the people; the state
  should rely on taxes and other contributions to raise funds instead
* 144 exchange (i.e. banking) should also be encouraged

### Chapter XI:  Second Object of Good Government -- to procure the true Happiness of the Nation
* 145 the previous chapters have talked about how the state may provide for
  the necessities of the people but not their happiness; one may be unhappy
  and still have everything they need
* 145 the sovereign should take pains to enlighten and educate the people
* 146 public education -- education should not be left to the parents alone
* 146 the whole nation should encourage the arts:  "if study does not always
  inspire the love of virtue, it is because it sometimes, and even too often,
  unhappily meets with an incorrigibly vicious heart"
* 147 ditto the sciences
* 147 philosophical discussion ought to be free from people taking words out
  of context to malign their authors
* 148 censorship should be entrusted only to those who are prudent and
  enlightened
* 148-9 while the state religion ought to be respected, it must not be allowed
  to censor criticism of superstition or philosophical discourse
* 150 sovereign should inspire love of virtue and abhorrence of vice; but
  virtue cannot be commanded
* 152 the state will be powerful when private virtues are elevated into civic
  virtues through a love of one's country
* 152-3 the love of one's country derives from the engagements he has entered
  into with society
* 153 our "country" is the land in which we were born -- it cannot be changed
  no matter where we move

### Chapter XII:  Of Piety and Religion
* 155-6 piety:  "a disposition of soul that leads us to direct all our actions
  towards the Deity, and to endeavour to please him in every thing we do"
* 156 everyone in the state from the common people to the leaders have an
  obligation to be pious
* 156 how can rulers be trusted if they do not fear God
* 156 blind, unenlightened piety, however, is more dangerous than atheism
* 157 the aspects of religious practice that are personal ought to be directed
  by the individual, but the aspects which are external and publicly
  established are under the purview of the state
* 157 freedom to practice one's religion is a natural and inviolable right
* 157 if all men are obligated to serve God, then the nation as a whole is as
  well by the establishment of a national religion
* 158 if the citizens cannot agree on which religion to follow, the state is
  left with the choice to either allow its citizens to follow their conscience
  or to divide the government
  * the latter, since it weakens both nations, should only be taken when there
    is more danger that the citizens will come to blows
* 158-9 "If the society will not allow me to do that to which I think myself
  bound by an indispensable obligation, it is obliged to allow me permission
  to depart."
* 159 the state religion should be supported but always with an eye towards
  how it may be improved
  * **but** it is the prerogative of the whole society -- not any single
    individual -- to make such changes; no person has the right to
    independently preach a new doctrine
  * if a new religion does spring up and is practiced by a great number, the
    authorities should consider it with the same regard it took when
    establishing a state religion the first time
* 160 if no religion has been established, the sovereign should encourage one,
  but he has no right to command the people to practice it because he received
  his power *before* the establishment of a religion
  * he does however have the right to discourage a religion he believes is
    dangerous to the people
* 161 the sovereign should protect and defend the established religion, but
  this duty only extends to acts which endanger the society or interfere with
  the public ceremonies; he must not punish people for crimes of the heart
* 162 but the best means of preventing disorder is the universal toleration of
  all religions which do not encourage vice or disturb the peace
* 163 the sovereign has a right to practice his own religion as long as he
  protects the state religion
* [there are a lot of pages after this that basically harp on the same points
  mentioned above]
* 170 a sovereign who does not have control over a state religion is only half
  a sovereign
* 171-2 when popes declared that every kingdom was subject to them both
  temporally and spiritually, they usurped the authority of the sovereigns of
  those nations
* 174-5 [Vattel believes] the vow of celibacy taken by catholic priests is to
  keep them separate from secular society and to consider only Rome their true
  country
* 176 [he goes so far as to condemn celibacy]
* 176 the clergy should be modest and humble and submit themselves to their
  king
* 180 the wealth horded by the church could more often be put to better use by
  the state
* [an enormous list of crimes the catholic church has committed against the
  sovereignty of princes]

### Chapter XIII:  Of Justice and Polity
* 185 the nation should ensure easy and speedy justice for its citizens
* 186 it is sometimes necessary that the law should deviate from the natural
  law in order to prevent abuse and fraud
* 186 penalties are necessary to overcome the failure of duty in the
  individual
* 186 "The legislator should assist the understanding of the judges, force
  their prejudices and inclinations, and subdue their will, by simple, fixed,
  and certain rules."
* 187 three branches of government:
  * legislature to make the laws
  * executive to give them force
  * "supreme tribunal" to judge disputes independent of the executive power
    * 188 "it would be very unbecoming, and highly improper for a prince, to
      take upon him to give judgment in his own cause"
* 187 executive power belongs in the sovereign [but what if the sovereign
  power is not invested in a single prince, e.g. Parliament or Congress?]
* 188 there should be supreme courts with final, appellate authority "for the
  greater safety"
* 190-1 the individual natural right of self-defense extends to the
  distributive justice of the state
* 191 punishments ought never to be extended beyond what is required to ensure
  the safety of the state and her citizens
* 192 punishment ought to be proportioned to the degree of injury to the
  public tranquility and safety of society
* 194-5 interesting view of duels in French society [Vattel is against them]

### Chapter XIV:  The third Object of a good Government -- to fortify itself against external Attacks
* 198 protection from an unjust enemy
* 198 strength of a state equal to its population, "military virtues", and
  wealth
* 198-9 the first object of the sovereign is to increase the number of
  citizens -- via marriage, allowing people to support their families, and
  encouraging immigration
* 200 valor is a military virtue
* 201 others:  ability in the commanders, discipline, frugality, strength,
  dexterity, stamina, and labor
* 201 the wealth of a nation consists of both the public and private wealth
* 202 the sovereign may need to regulate foreign trade to prevent the
  exportation of too much specie
* 202 national expenses should not exceed revenues

### Chapter XV:  Of the Glory of a Nation
* 203 an honorable reputation is the glory of a nation
* 204 qualities of the sovereign:  "justice, moderation, and greatness of
  soul"
* 204 a dignity of a country will be judged by the dignity of its sovereign
* 204 the reputation of his ministers affects the reputation of the sovereign
* 205 a nation will be judged by the most numerous vice or virtue of its
  citizens
* 206 attacking a nation's reputation/glory is equal to invasion by force, and
  the nation has a right to protect itself

### Chapter XVI:  Of the Protection sought by a Nation, and its voluntary Submission to a foreign Power
* 207 a weaker nation may seek a treaty of protection from a stronger country
  as long as she maintains the right to administer her government at her
  pleasure
* 207 there are other varying levels of submission whereby one nation may
  subsume some or all of the sovereignty of the weaker nation
* 208 any citizen who does not approve of the change is not under the
  authority of the new sovereign and has the right to move elsewhere
* 208 if the stronger is not able to protect the weaker -- either through bad
  faith or inability -- then the obligation is broken, and the weaker may
  reacquire all its rights

### Chapter XVII:  How a Nation may separate itself from the state of which it is a Member, or renounce its Allegiance to its Sovereign when it is not protected
* 211 the conditions of dissolution previously discussed do not hold in the
  case of the sovereign or stronger nation not having enough time to act --
  some delay may be expected
* 212 necessity or fortune may force a people's hand -- they are allowed to
  renounce their sovereign and submit to an invader in order to secure their
  lives

### Chapter XVIII:  Of the Establishment of a Nation in a Country
* 213 all men have a right to derive subsidence from the land; because the
  human population has grown so large, they can no longer hunt and gather in
  common and must cultivate crops; nations derive an exclusive right to a
  portion of the earth in order to cultivate crops for her people
* 213 *domain* = the land that a nation may dispose of as it thinks proper
* 213 *empire* = the nation's sovereign prerogatives
* 214 when individuals, previously possessing their own domain, join together
  in society, they establish the sovereignty over the country they inhabit
* 214 the first to possess uninhabited land creates an exclusive right to it
* 214-5 *actual* possession requires settlement and cultivation of a land; a
  nation which claims possession of land that it does not use goes against
  natural law
* 216 hunter/gather lifestyle does not count as a true possession of the land
  -- therefore the Indian nations did not *really* possess the New World
  before the European nations
* 217 colonies are to be viewed as a natural extension of the original state

### Chapter XIX:  Of our Native Country and several Things that relate to it
* 217 "natural-born citizens" are children born to citizens of a country in
  that country
* 218 every citizen (specifically fathers) reserves to his children the right
  to become citizens of his country -- i.e. the citizenship status of a child
  will be the same as their father
* 218 then to be born of a foreigner will not make the place of his birth "his
  country"
* 218 "inhabitants" - foreigners allowed to stay in the country
* 218 "perpetual inhabitants" - second class citizens united to a society but
  without all the advantages of a full citizen
* 218 "naturalization" is the process of making someone a citizen
* 218 children born to diplomats are to be considered citizens since their
  father is on the business of the state, he cannot be said to have quit his
  society
* 220 "vagrants" have no settlement and therefore no country
* 220 children have natural bonds to the society into which they were born,
  but when they have come of age, they may decide to leave it
* 221 "Every man has a right to quit his country, in order to settle in any
  other, when by that step he does not endanger the welfare of his country."
  But it is dishonorable to sever those ties without necessity.
* 221 abandonment of ones country in war to avoid defending it is a violation
  of the social contract
* 223 reasons a citizen may abandon their society:
  1. if the citizen cannot provide for himself in his own country
  2. the sovereign fails to fulfill its obligations towards the citizen
  3. if laws are enacted which cannot, by the social contract, oblige every
     citizen to obey -- e.g. if the nation permits only one religion to be
     pratciced, those who serve a different religion may leave
* 224 the right of emigration may be
  1. a natural right in any of the above cases
  2. secured to them by law
  3. voluntarily granted by the sovereign
  4. by treaty between states -- e.g. in which one state has promised full
     liberty to those emigrating from one country due to religious persecution
* 225 "supplicants" == refugees
* 225-8 definitions and differences between "exile" and "banishment"

### Chapter XX:  Of public, common, and private Property
* 228 differences between public and private property
* 229 the public revenue is to be spent at the discretion of the sovereign
* 230 the people may cede whatever common property they like to the sovereign;
  it is not a right he possesses by the nature of his office
* 230 members of a civil society are obligated to pay taxes in proportion to
  the advantages they reap due the safety of that society
* 230 "In England, the king lays the necessities of the state before the
  parliament; that body, composed of the representatives of the nation,
  deliberates, and, with the concurrence of the king, determines the sum to be
  raised, and the manner of raising it." [Sounds an awfully lot like the
  budget process of the US, even though that was only codified in 1921.]
* 231 (footnote) "Too great attention cannot be used in watching the
  imposition of taxes, which, once introduced, not only continue, but are so
  easily multiplied."
* 231 the public revenue should never be considered the sovereign's private
  property no matter how much authority he is given over it
* 232 the sovereign has a right to *eminent domain* as long as the people
  have not explicitly exempted him from it
* 233 eminent domain likened to throwing valuables off the side of a sinking
  vessel to save the ship
* 233 the sovereign may place limits on the use of common property in the
  interest of maintaining that property -- e.g. limits on hunting, limiting
  destructive fishing practices, etc.
* 234 the common property of a corporation may be shared equally among its
  members using a number of methods -- equal shares or all have equal access
  -- but no member should be disadvantaged in his use compared to the other
  members
* 235 with respect to common property that can be exhausted or can only be
  used one at a time (e.g. a well), the right of use belongs to the first
  person who acquired it; anyone else would have to wait their turn
* 235 expenses necessary for the preservation of public property should be
  born by all who share in its use equally
  * ergo toll-duties are lawful
* 236 the sovereign has rights over the use of corporate property as well
  since it is in the nation's best interest that the corporation not fall into
  "indigence"
* 236 individual rights over private property only extend so far that they do
  not interfere with a third party or the welfare of the state
  * e.g. the sovereign may (and should) prevent monopolies

### Chapter XXI:  Of the Alienation of the public Property, or the Domain, and that of a Part of the State
* 237-8 a nation may (but should not) alienate her property as she sees fit
  * the *sovereign*, it is important to note, does not necessarily have this
    right
* 240 but the people may grant the sovereign such an absolute right
* 240 no member -- town, etc. -- has a right to secede from the nation, but
  the nation may abandon them when the public safety requires it (e.g. in war)
* 241 such a state/town regains its sovereignty and liberty

### Chapter XXII:  Of Rivers, Streams, and Lakes
* what the title says [None of what is discussed in this chapter seems
  applicable (or, honestly, interesting).]

### Chapter XXIII:  Of the Sea
* 249 the sea cannot be possessed since no one can build outposts on it to
  defend
* 250 and no nation has a right to use their navy to prevent the use of the
  sea to others
* 251 everyone has a right to the sea and anyone prevented from exercising
  that right may lawfully defend that right by force
* 251 nations however may willingly give up such rights via treaties, but they
  cannot be lost for want of use
* 252 but if one nation claims an exclusive right to an area, and other
  nations obey that claim, then they have tacitly given up their right to it
* 253 a nation may protect her borders to some extent into the ocean
  * 254 "the dominion of the state over the neighboring sea extends as far as
    her safety renders it necessary and her power is able to assert it"
* 256 right to shipwrecks only applicable when the owners cannot be found
  [compare to Blackstone Vol I, Ch 8]

## Book II:  Of a Nation considered in its Relations to others

### Chapter I:  Of the Common Duties of a Nation towards others, or of the Offices of Humanity between Nations
* 260 "The better and more noble therefore the character with which a man is
  endowed, the more does he prefer the life of service to the life of
  pleasure." -- Cicero
* 261 as men have duties to other men *because* they are men, so do nations
  have duties to other nations -- "doing everything in our power for the
  preservation and happiness of others, as far as such conduct is reconcilable
  with our duties towards ourselves"
* 262 each state owes to other states exactly what it owes itself in so far as the
  latter is in need and the former does not neglect itself in fulfilling the
  duty
  * 263 this includes, e.g., relief aid and mutual defense of allies in war
* 264 nations should be especially ready to help perfect other nations by
  providing teachers of the sciences and law (if the latter is desirous of the
  help)
* 269 this is the ideal, but the imperfect state of men limits what can
  reasonably be achieved
  * 269 most nations only care to enrich themselves at the expense of others
    -- the good must not become the dupes of the wicked
* 270 a nation has a right to keep valuable information to itself, but if
  circumstances allow, it should not monopolize a universal good
* 271 the head of the nation cannot be as generous as a private person because
  he must keep the whole nation's interest in mind
* 272 if the nation intends to do perfect themselves and by accidental
  consequence harms another, then they have done no wrong because it was not
  their intent -- example:  in defending our country from attack, we do not
  have the goal of harming the attacker, only in defending ourselves

### Chapter II:  Of the Mutual Commerce between Nations
* 274 since no nation can supply everything it desires completely, trade
  benefits everyone
* 274 to this end every nation should do what it can to encourage and protect
  trade -- the maintenance of roads, protection of travelers, etc.
* 275 but a nation ought to discourage any trade which is harmful to it
* 276 to this end each nation may make commercial affairs in whatever way she
  thinks proper
* 277 treaties are to be treated as inviolable as contracts between
  individuals
* 279 consul:  "persons residing in the large trading cities, and especially
  the seaports, of foreign countries, with a commission to watch over the
  rights a privileges of their nation, and to decide disputes between the
  merchants there."
* 279 consuls should not be a public minister
* 279 though, he should be treated in many ways like a diplomat -- e.g.
  diplomatic immunity

### Chapter III:  Of the Dignity and Equality of Nations -- of Titles -- and other Marks of Honor
* 281 even though states are equals, small states should show a preference to
  larger states when appropriate because they represent more people
* 284 the title of the sovereign is to be chosen wisely -- too humble and the
  state will have no dignity -- too flamboyant and it may give the sovereign
  undue pride -- but an exalted title could encourage the sovereign to live up
  to its honor

### Chapter IV:  Of the Right to Security and the Effects of the Sovereignty and Independence of Nations
* 288 every nation has a "right to security" -- a right to defend herself from
  harm
* 289 a nation has the right, anticipating harm, to put down a dangerous
  government or people before they have committed outright violence
* 290 because each nation retains a right of sovereignty, it is not the
  prerogative of any other nation to pass judgment on the former for conduct
  which does not impact the latter
* 290 but a nation has the right to aid a people rebelling against an
  oppressive ruler
* 291 but it is against the law of nations to encourage a people to rebel who
  would not have otherwise
* 292 the sovereign has a right to decide on internal religious matters

### Chapter V:  Of the Observance of Justice between Nations
* 296 the execution of justice is even more sacred between nations than
  individuals because of the extent of their powers
* 297 rights of nations relating to justice:
  1. the right of just defense
  2. the right to obtain justice by force if it cannot be obtained otherwise
  3. the right of punishing injustice
* 297 if by her actions a nation shows that she regards no right as sacred and
  endangers humanity in general, other nations should form a confederacy to
  humble her

### Chapter VI:  Of the Concern a Nation may have in the Actions of her Citizens
* 298 a nation should punish those who injure one of her citizens
* 298 a nation should prevent her own citizens from harming those of a foreign
  nation
  * 299 and should aid in delivering a citizen up to the justice of a foreign
    power if they have harmed it
* 299 however, it would be unjust to impute to a nation or a sovereign the
  actions of a citizen or group of citizens acting independently
  * 301 unless the policies or maxims of the nation gave rise to the injustice

### Chapter VII:  Effects of the Domain between Nations
* the possessions of a nation are the domains she *justly* possesses as well as her rights understood by other nations generally
* the private property of citizens in some sense also belongs to her with respect to other nations
* if one nation has a right to any part of the property of another, she also has a right to the private property of the latter's citizens
* a sovereign ought not to interfere in the criminal proceedings of subjects in a foreign land unless the proceedings lack justice
* a nation has the right to leave land uncultivated and unused so long as doing so does not reduce other nations to a lack of resources
* nations have the right to determine the extent of use foreigners have of their unused land
* no nation may remove natives in order to colonize the land themselves
* boundaries must be clearly and unambiguously defined so that no one encroaches on another's lands in ignorance
* no one should enter a foreign state to capture a fugitive without the consent of said state
* the sovereign may limit who comes into their country either generally -- citizens of a particular country -- or particularly -- specific individuals

### Chapter VIII:  Rules with respect to Foreigners
* 312 any traveler through a foreign country tacitly consents to the law of
  the land he enters
  * 313 and punishments are the same for foreigners and citizens
* 313 defendants have a right to be tried before their own judge rather than a
  foreign one
* 314 as soon as a sovereign admits a foreigner into his country, he "engages
  to protect them as his own subjects"
* 314 the foreigner is bound to all duties that are not a quality of
  'citizen', e.g. to pay taxes on goods but not serve in the militia
* 315 he is free at all times to leave
* 315-6 the property of the traveler remains the property of his own country
  -- "immovable effects" not withstanding

### Chapter IX:  Of the Rights retained by all Nations after the Introduction of Domain and Property
* 319 "Nature imposes no obligations on men, without giving them the means of
  fulfilling them."
* 320 "right of necessity":  the right to fulfill one's necessary obligations
  even if the act would otherwise be unlawful
* 320 ex:  a nation has a right of necessity to -- if their people are
  starving -- demand that another nation with surpluses sell their goods at a
  fair value and to take them by force if they will not sell them
* 321 a nation may make use of foreign vessels in her country if she needs
  them, *but* she must pay just compensation for their use
* 321-2 [very odd justification for kidnapping women and making them into
  wives]
* 322 the right of property preferences the rights of a proprietor over all
  others; the other party must demonstrate their necessity to overcome it
  * "Thus a vessel driven by stress of weather has a right to enter, even by
    force, into a foreign port.  But if that vessel is infected with the
    plague, the owner of the port may fire upon it and beat it off, without
    any violation either of justice, or even of charity, which, in such a
    case, ought doubtless to begin at home."
* 323 a nation may refuse entrance to refugees for good reason, but refugees
  have a right to live *somewhere*; therefore, if no one will allow them
  entrance, they have a right to settle in the first land that can admit them
  without harming its own inhabitants
* 324 while I do not have a right to prevent you from partaking in a common
  good (say, drinking from a river), if you trample my crops to get to the
  river, I may ban you; in this instance the withholding of the common good is
  only accidental
* 324 "right of innocent use": one has the right to use another's property as
  long as it does not cause the latter loss or inconvenience
* 324 the owner is the judge of what use will cause him loss or inconvenience

### Chapter X:  How a Nation is to use her Right of Domain, in order to discharge her Duties towards other Nations, with respect to the Innocent Use of Things
* 326 a nation ought to allow innocent use -- even at a slight inconvenience
  -- if the advantage it gives to others is greater
* 327 nations ought to allow passage through their lands for the commerce of
  others if there is no loss of security to do so; and she is free to exact
  tolls
* 328 ditto for foreigners traveling through or settling in a country
* 329 a sovereign who fears a large settlement of foreigners may scatter them
  throughout his land so they do not pose a united threat
* 330 every nation has the duty to protect foreign travelers and provide
  lodging at fair price
* 331 "No nation is entitled to greater respect than the French: foreigners
  nowhere meet a reception more agreeable, or better calculated to prevent
  their regretting the immense sums they annually spend at Paris."

### Chapter XI:  Of Usucaption and Prescription among Nations
* 331 "usucaption": "the acquisition of domain founded on a long possession,
  uninterrupted and undisputed -- that is to say, an acquisition solely proved
  by this possession"
* 331-2 "prescription": exclusion of all pretensions to a right because the
  right has been so long unused that consent to the contrary must be presumed
* 332 [It would be interesting to compare Vattel's examples of usucaption
  beginning at the bottom of this page with Nozick's *Anarchy, State, and
  Utopia*.]
* 332-3 if a man neglects his property long enough, then another may lay claim
  to it, and if the latter holds it undisputed long enough, then the property
  is now his [but how long is "long enough"?]
* 335 if the original possessor is forced to abandon his property but
  proclaims loudly enough that he is still the proprietor, then there is no
  usucaption
* 337 both usucaption and prescription are a part of the voluntary law of
  nations
* 338 since the concept has so many exceptions and loopholes and different
  parties might not interpret their or their opponent's actions in the same
  light, neighboring nations should adopt explicit rules on this subject with
  one another

### Chapter XII:  Of Treaties of Alliance and other public Treaties
* 338 only agreements made between sovereigns acting on behalf of their
  countries -- as opposed to as private individuals -- are treaties
* 339 lesser powers below the sovereign may also make treaties as long as it
  is allowed by law or the sovereign
* 339 the sovereign may allow agents -- plenipotentiaries -- to hold the right
  on the nation's behalf
* 340 custom says that any treaties agreed to in the previous manner must be
  ratified by the sovereign power in order to come into effect
* 340 injury to one party cannot render treaties between nations invalid
* 341 the end of treaties should be equity -- if a sovereign learns after the
  agreement that the terms of a treat prove disadvantageous to an ally, it
  would be conformable to his duty to relax the terms in so far as he is not
  harmed or inconvenienced by it
* 341 no treaty is valid which is capable of destroying the state
* 341 treaties contrary to the law of nature are null and void
* 343 prior treaties take precedent over later treaties when they contradict
  one another
* 345 of a nation discovers that an act they are bound to in a treaty prevents
  them from fulfilling their duties to themselves -- e.g. providing food to an
  ally when one's citizens are starving -- the treaty tacitly admits such
  exceptions
* 348-9 equal vs unequal treaties
* 352 if a nation is forced to submit to a superior power and give up some of
  her sovereignty, she may lawfully renounce her former treaties if the
  superior power requires it
* 352 a sovereign should not force such submission of a weaker nation without
  just reasons -- just like individuals in a state of nature, it is better for
  nations to respect and uphold the dignity and liberty of one another
* 355 sovereigns must be careful not to word *real* alliances -- attached to
  the body of the state -- in such a way that they become only *personal*
  alliances -- one which expires when the sovereign does
* 356 treaties by republics are necessarily real; even if the form of the
  republic is changed, prior treaties will remain valid
* 356-7 treaties of monarchies, while they can usually be assumed to be real,
  can be more subtle
* 358-62 conditions under which treaties are to be upheld by successors
* 365 an ally remains the ally of a *state* no matter what changes have been
  made to its form of government since the start of the alliance

### Chapter XIII:  Of the Dissolution and Renewal of Treaties
* 366-7 there are very limited conditions under which a treaty can be
  understood to have been tacitly renewed [examples]
* 367 if an ally fails to fulfill their side of a treaty, the offended party
  has the right to either force them to fulfill it through arms or, easier,
  dissolve the treaty and consider his own promises to his ally canceled
* 368 if two nations have multiple independent treaties with one another, the
  violation of one does not necessarily cancel one's promises in the other;
  but the offended party may threaten to withdraw from the others if the
  offender does not abide
* 368-9 **however** this does not apply to independent articles within the
  same treaty -- they are all agreed to or all dissolved
* 369 Grotius:  "every article of a treaty carries with it a condition, by the
  non-performance of which, the treaty is wholly canceled" -- he continues
  that any intent to the contrary should be explicitly stated in the treaty
  itself
* 369 treaties dissolve when the nation which contracted it ceases to exist
* 370 if a nation conquers another with debts, the conquering nation now owes
  the debts -- "For a conqueror to refuse to pay the debts of a country he has
  subdued, would be robbing the creditors, with whom he is not at war."
* 371 if a nation submits herself to another for protection, she still retains
  all the duties of her treaties in so far as they are compatible with the new
  subordination
* 371 as treaties are made by the mutual engagement of multiple parties, they
  may be dissolved by mutual consent

### Chapter XIV:  Of other public Conventions -- of those that are made by subordinate Powers -- particularly of the Agreement called in Latin *Sponsio* -- and of Conventions of Sovereigns with private Persons
* 372 "conventions" == public compacts made between sovereigns
  * [I'm not terribly sure about the difference between these and treaties.]
* 374 "*sponsio*" == agreement relating to the affairs of state made by a
  public person who does not have the authority from the sovereign and exceeds
  the bounds of his commission
* 375 a private individual who makes a promise which depends on the will of
  another without being authorized to make such a promise, if the other
  disavows, should (1) accomplish the promise himself, (2) set things to the
  way they were before the promise, or (3) compensate the one to whom he
  promised
  * 376 a public person in the same situation is not under the same obligation
    because he cannot provide the same things he has promised on behalf of his
    sovereign
* 379 a sovereign ought not to take advantage of the "imprudent" by accepting
  an advantage from a party based on a treaty the sovereign has no intention
  of ratifying
  * the sovereign should set the thing right if this becomes the case

### Chapter XV:  Of the Faith of Treaties
* 387 the word the sovereign makes in agreeing to a treaty should be held as
  sacred and inviolable
* 388-90 examples of popes absolving sovereigns of their oaths to others
* 390 "A man of sense, a man of honor, does not think himself less bound by
  his word alone, by his faith once pledged, than if he had added the sanction
  of an oath."
* 391 the presence or absence of an oath does not affect the obligation of a
  treaty
* 393-4 he that words a treaty in such a way to cause a false interpretation
  of the treaty in the mind of the other breaks the faith of a treaty as if he
  had outright ignored his obligation

### Chapter XVI:  Of Securities given for the Observance of Treaties
* 396 sometimes a third party may be used to guarantee the observance of a
  treaty by acting against any violators with force
  * when several sovereigns enter into the same treaty with one another, they
    may all act as guarantees for its observance
  * the guarantee is not bound to give his assistance except when the
    aggrieved party is unable to obtain justice on their own
* 398 a sovereign may be allowed to govern a part of another's empire as a
  means of security in case the other breaks their end of the bargain
  * such a sovereign is bound to continue to govern the region in the same way
    the other had
  * the security is dissolved as soon as the treaty is fulfilled
* 400 human hostages may be used as securities, but even though the name is
  ominous, they often live at another court more as ministers than as
  prisoners
* 402 the party who sends the hostages, not their hosts, should provide for
  their support
* 404 if a prince is a hostage and ascends to the throne during this time, he
  should return to his throne and provide a new, suitable hostage

### Chapter XVII:  Of the Interpretation of Treaties
* general maxims of interpretation:
  1. 408 "It is not allowable to interpret what has no need of
     interpretation."
     * "However luminous each clause may be, however clear and precise the
       terms in which the deed is couched, all this will be of no avail if it
       be allowed to go in quest of extraneous arguments to prove that it is
       not to be understood in the sense which it naturally presents."
  2. 408 If a person could have expressed a clause clearly but did not, then
     that person cannot be trusted.
  3. 409 "Neither the one nor the other of the parties interested in the
     contract has a right to interpret the deed or treaty according to his own
     fancy."
  4. 409 "On every occasion when a person could and ought to have made known
     his intention, we assume for true against him what he has sufficiently
     declared."
  5. 410 "Every deed and every treaty must be interpreted by certain fixed
     rules calculated to determine its meaning as naturally understood by the
     parties concerned at the time when the dded was drawn up and accepted."
     * 409 in order to discover the true meaning of a clause, attention should
       principally be paid to the words of the *promising* party
* 411 general rule for all interpretation:  "whenever we meet with any
  obscurity in it, we are to consider what probably were the ideas of those
  who drew up the deed and to interpret it accordingly"
* 411-2 [examples of when to be lenient and when to be strict in
  interpretation]
* 412 "In the interpretation of treaties, compacts, and promises, we ought
  not to deviate from the common use of the language, unless we have very
  strong reasons for it."
  * 413 when an ancient deed is to be interpreted, we should use the common
    uses of the terms as they existed when it was written
* 414 "When we evidently see what is the sense that agrees with the intention
  of the contracting parties, it is not allowable to wrest their words to a
  contrary meaning."
* 416 ambiguity ought to be avoided
* 418 "Every interpretation which leads to an absurdity ought to be rejected."
* 419 Any interpretation "which would render a treaty null and inefficient
  cannot be admitted."
* 420 we should assume men's thoughts are consistent; if a man speaks
  ambiguously in one context but clearly on another, similar occasion, then we
  should interpret the ambiguity in light of similar, clearer statements
  elsewhere
* 421 "The interpretation ought to be made in such a manner, that all the
  parts may appear consonant to each other, that what follows may agree with
  what preceded, unless it evidently appear that by the subsequent clauses the
  parties intended to make some alteration in the preceding ones."
  * a maxim of Roman law:  "It is impertinent to give an opinion or make a
    response on any detail put forward without examining the whole law."
* 426 "Good-faith adheres to the intention; fraud insists on the terms when it
  thinks that they can furnish a cloak for its prevarications."
  * 427 such an act is as good (or bad) as a direct violation of the law
* 429 "How a change happening in the state of things may form an exception":
  * "If it be certain and manifest that the consideration of the present state
    of things was one of the reasons which occasioned the promise -- that the
    promise was made in consideration or in consequence of that state of
    things -- it depends on the preservation of things in the same state."
* 431 we should interpret by intentions and not strictly by words -- e.g. if a
  father appoints a guardian for his children, but it turns out the chosen
  guardian is a bum, then the judge would be right to choose a different
  guardian for the children, supposing that the father would have chosen
  someone different as well had he known better
* 431-2 if a law exists to prevent a possible threat, then it is no defense to
  point out that nothing threatening was intended by an action -- e.g. if a
  law exists that a person cannot walk down the street with a visible weapon,
  it is no defense that a person doing so is no threat; the purpose of the law
  is to prevent exciting such fears
* 432 [beginning here Vattel makes a distinction between 'favorable' and
  'odious' interpretations]
* 443 when two laws or treaties contradict one another:
  1. 443 "In all cases where what is barely permitted is found incompatible
     with what is positively prescribed, the latter claims a preference."
  2. 443 "the law or treaty which permits ought to give way to the law or
     treaty which forbids"
  3. 443 "the law or treaty which ordains gives way to the law or treaty which
     forbids"
  4. 445 if both prohibitions or permissions are of the same nature, then the
     older treaty or law takes precedence
  5. 445 "Of two laws or two conventions, we ought (all other circumstances
     being equal) to prefer the one which is less general and which approaches
     nearer to the point in question."
  6. 445 "What will not admit of delay is to be preferred to what may be done
     at another time."
  7. 446 "When two duties stand in competition, that one which is the more
     considerable, the more praiseworthy, and productive of the greater
     utilityis entitled to the preference."
  8. 447 "If we cannot acquit ourselves at the same time of two things
     promised to the same person, it rests with him to choose which of the two
     we are to perform."
  9. 447 "If a treaty that has been confirmed by an oath happens to clash with
     another treaty which has not been sworn to, all circumstances being in
     other respects equal, the preference is to be given to the former."
  10. 447 "all circumstances being in other respects equal, what is enjoined
      under a penalty claims a preference over that which is not enforced by
      one -- and what is enjoined under a greater penalty over that which is
      enforced by a lesser"

### Chapter XVIII:  Of the Mode of terminating Disputes between Nations
* 448-9 an individual may be free to forfeit a right or forget a wrong in
  certain contexts, but a sovereign has much less wiggle room:  his duty is to
  do that which provides the greatest advantage to the state; for the
  long-term good of the state, such things should often be defended
* 450 before jumping into all-out war try:
  1. amicable accommodation
  2. compromise
  3. mediation
  4. arbitration - like mediation, but the disputants are bound to the
     decision
     * 451 but there is no reason to submit to an unjust decision
* 455 "taking up the sword" is the last resort
* 458 retaliation -- punishing an evil for an evil -- is wrong when it affects
  anyone but the guilty party
  * as stated above, a nation must not punish beyond what her safety requires
* 459 one may punish without going to war -- e.g. removing her rights in your
  country
* 461 if a nation engages in reprisal against another by seizing her property,
  the reprisal may also target *individual citizens* of the state -- their
  property being a part of the state's aggregate property
* 462 reprisals may be taken against a nation not just for the actions of the
  sovereign but also for the actions of any subject of that sovereign
  * 464 the sovereign of such a subject should compel them to make it right
* 465 between private persons, reprisals have a limit:  we should abstain from
  reprisals if the only way to ensure it is death; "it is certainly worthy ...
  of every man of principle, rather to abandon his right than to kill the
  person who unjustly resists him"
  * 466 however, between sovereigns -- whose concern is for the whole nation
    -- the case is otherwise

## Book III:  Of War

### Chapter I:  Of War, its different Kinds, and the Right of making War
* 469 *war*:  "the state in which we prosecute our right by force" -- derives
  from the right of self-defense and preservation of rights
* 470 only the sovereign has the right to declare war
* 471 in a defensive struggle against an aggressor the only goal can be
  self-defense
* 471 offensive war may be waged for a number of reasons (none of which are
  necessarily just):
  * take something we believe is our right
  * punish for an injury
  * preemptive strike
  * etc.

### Chapter II:  Of the Instruments of War, -- the raising of Troops, &c. -- their Commanders, or the Subordinate Powers in War
* 472 the right of raising troops also rests in the sovereign
* 472 in an emergency a governor or mayor may raise troops for defense; in
  which case the right to do so is tacitly derived from that of the sovereign
* 473 even though both the right of war and raising of troops rest in the
  sovereign, a people may place the rights in different bodies of their
  government
  * Ex. England -- the king declares war, but only parliament can draft or
    maintain the army
* 473 every citizen has an obligation to defend their state
* 474 even though every person has the duty to defend their state, there may
  be natural reasons why a particular individual cannot:
  * they cannot handle arms
  * they cannot handle the fatigue of war
  * they are more useful to society elsewhere
* 476 standing armies should be paid (out of taxes) and housed (in barracks if
  it can be helped)
  * [Compare to Blackstone ch. 13]
* 476 the sovereign should provide for soldiers permanently injured in war
* 479-80 the sovereign should clearly define the rules and duties of enlisted
  men
* 481 "Every promise made by any of the subordinate powers, by any commander
  within his department, in conformity to the terms of his commission and to
  the authority which he naturally derives from his office and the functions
  intrusted to his care [...] is [...] made in the name and by the authority
  of the sovereign, and equally obligatory on him, as if he had himself
  personally made it."

### Chapter III:  Of the just Causes of War
* 482 war is always a last resort
* 483-4 the justification of war is to avenge or prevent injury
* 484 whatever "strikes" at a nation's "perfect rights" [see p.75 above] is an
  injury necessitating war
* 485-6 the motives for going to war are as important as its justification --
  a lawful cause could act as a mere pretext for a baser motive
* 486 *pretext*: "reasons which are, in themselves, true and well-founded,
  but, not being of sufficient importance for undertaking a war, are made use
  of only to cover ambitious views, or some other vicious motive"
* 489 there are situations when both parties in war have a sincere claim to
  being in the right; in which case both parties should be considered as being
  in the right until the contest determines a winner
  * however, do not confuse "winning a war" with "being in the right"; success
    in war requires strength, not justice
* 491  Q:  Seeing a neighbor growing in power and, due to the lesson of
  history, fearing that they may one day threaten our nation -- are we
  justified in striking first to subdue them before they become too powerful?
  * 492 "it is a sacred principle of the law of nations, that an increase of
    power cannot, alone and of itself, give any one a right to take up arms to
    oppose it"
  * 493-4 for war in such a case to be just, there must be some indication by
    the ambition of the sovereign (or otherwise) that the accumulation of
    power represents a clear and present danger
* 496 modern (1700s) Europe is so interconnected that the states, through a
  political equilibrium, relate to one another more like a republic than a
  state of nature
  * 497 this is due to confederacies forming to oppose the most powerful
    states to ensure they do not force themselves upon weaker ones
  * 498 other nations should take every justifiable recourse to preserve the
    political equilibrium
* 499 if a neighbor erects forts at our border and amasses troops, whether or
  not we preemptively defend ourselves is predicated on the sovereign's
  character:
  * if he has done this in the past prior to conflict, we are justified in
    attacking first
  * if he has not and has no known quarrel with us and tells us he means us no
    harm, we should take him at his word and make only those precautions which
    are necessary to ensure our security
* 500 on militias:  "This happy method of a free republic -- the custom of
  training up all her citizens to the art of war -- renders the state
  respectable abroad, and saves it from a very pernicious defect at home [the
  standing army]."

### Chapter IV:  Of the Declaration of War, -- and of War in due form
* 500 war, stated again, is only a last resort
* 502 *conditional declaration of war*:  if you don't fix *this*, then we will
  begin hostilities until you do
* 503 a clear and fixed declaration of war is useful for determining any
  reparations at the end of the conflict
* 507 a defensive war requires no declaration

### Chapter V:  Of the Enemy and of Things belonging to the Enemy
* 510 when a sovereign declares another sovereign their enemy, every subject of
  the former becomes an enemy of every subject of the latter
  * thus everything owned by every citizen of the enemy is to be considered
    "things belonging to the enemy"
* 510-11 if the subject of an enemy state owns "immovable" property in our
  state, then he stays the owner even if it is in our boundary
  * 511 "but the income may be sequestered in order to prevent its being
    remitted to the enemy's country"

### Chapter VI:  Of the Enemy's Allies -- of warlike Associations -- of Auxiliaries and Subsidies
* 512 alliances can be generally divided into defensive and offensive
* 513 it is lawful and commendable to succor and assist nations involved in a
  just war
* 515 you owe no assistance to a nation engaged in an unjust war even if they
  have an alliance with you
* 519 I do not have to treat as my enemy any country who assists my enemy
  without engaging in direct hostilities with me

### Chapter VII:  Of Neutrality -- and the Passage of Troops through a Neutral Country
* 524 a neutral nation gives *no* assistance to either side when under no
  obligation to give it
* 524-5 two occasions to take a stance of neutrality:
  1. when the right party in the conflict is ambiguous
  2. even if one party is clearly in the right, if it would endanger the
     nation to engage in the conflict
* 526 a nation wishing to remain neutral should treaty with both parties so
  that the conditions of her continued neutrality are clear
* 527 conscripting troops for an enemy -- as long as they are not provided for
  invasion -- does not break neutrality
  * [Do I understand what "levy" means in this context?]
  * 527-8 ditto trade or lending money -- as long as she is willing to
    continue the same for us
* 529 if one country by going to war with another indirectly injures my
  commerce with the latter, it is not a just reason for war because any harm
  to me is incidental
* 530-1 BUT it is lawful to confiscate any contraband destined for our enemy
  from a neutral party (a declaration of war should clearly state that we
  intend to do so)
  * 532 from the above derives the right to search neutral ships
* 533 if neutral property is found on an enemy's ship, it is to be restored to
  the owners
* 534 *innocent* passage of troops through a neutral country is to be allowed
  * 535 but the sovereign is of course free to refuse for any reason
  * 536 however, if the army is under threat of destruction if it cannot
    retreat through the neutral state, then they have a right to proceed, even
    at the sovereign's denial
* 539 unlawful to attack an enemy in a neutral country

### Chapter VIII:  Of the Rights of Nations in War -- and first of what we have right to do, and what we are allowed to do to the Enemy's Person in a just War
* 542 what we may be allowed to do in war is contextual and is different from
  war to war
* 543 we should always give quarter to enemy combatants who lay down arms
  * 544 the lone exception is when the enemy in question has broken the laws
    of war, e.g. refusing quarter himself -- in this case the execution is a
    punishment
* 546-7 no leader should be punished with death by his enemy for his bravery
  and resilience in defending his homeland
* 549 fugitives and deserters found among the enemy may be punished with death
  because they are not considered enemies but traitors
* 549 women, children, elderly, and sick are not to be harmed
  * 550 the same goes for clergy, academics, and anyone else who is not
    engaged in hostilities
* 552 prisoners may be confined or secured if there is any worry that they
  will rise against their captors
* 553 if there are too many captives to feed or manage safely, they should be
  released on "parole" (a promise that they will not resume hostilities for a
  set time)
* 556 it is lawful to condemn POWs to slavery only under the same conditions
  it would be to kill them
* 557 the state is bound to procure POWs when she has the means
* 558 one may assassinate with surprise -- e.g. sneaking into a war camp and
  murdering the general in his sleep
* 559 BUT one may not do so using a traitor or poison

### Chapter IX:  Of the Rights of War with regard to Things belonging to the Enemy
* 567 we have a right to deprive our enemy of his possessions
* 568 we may take much more than he owes us -- e.g. town, provinces, etc. --
  with the end goal to compel him to surrender
* 568 *booty*: "movable property" taken from the enemy
* 569 a general may ask "contributions" from the country-side in exchange for
  no pillaging
* 570 areas of land may be ravaged -- put to flame and sword -- only as a last
  resort to put a barrier between oneself and a savage enemy who intends to do
  similar to you
  * 571 even so, care should be taken to spare monuments and holy places
* 573 "All damage done to the enemy unnecessarily, every act of hostility
  which does not tend to procure victory and bring the war to a conclusion, is
  a licentiousness condemned by the law of nature."

### Chapter X:  Of Faith between Enemies -- of Stratagems, Artifices in War, Spies, and some other Practices
* 576 even in war against an unjust aggressor, there are still limits to our
  actions towards them based on the laws of nature
* 577 all promises made to an enemy in the course of a war are obligatory, but
  we are no longer bound to observe anything which the enemy is the first to
  break
* 578 we are obliged to utter the truth in treaties and conventions because to
  do otherwise is to undermine their foundations
* 579 but feints and surprises in war -- *strategems* -- are acceptable
  because we are under no obligation to behave honestly in those situations
* 580 example of lie that would not be acceptable in war:  pretending to be a
  boat in distress in order to lure the enemy to help
  * because one would not want to live in such a world where men could not
    trust one another in this way
* 582 it is lawful to employ either ones own subjects or mercenaries as spies,
  but no one is obliged to serve as a spy
* 582-3 it is *lawful* to seduce the enemy's own subjects as spies, but it is
  not noble to do so
* 584 but it is perfectly acceptable to accept the help of a traitor who acts
  of his own accord; however, if we can succeed without a traitor, then we
  should
* 585 if the enemy attempts to seduce a governor, it is lawful for him to
  deceive the enemy by pretending to acquiesce

### Chapter XI:  Of the Sovereign who wages an unjust War
* 586 "And should our weak voice, throughout the whole succession of ages,
  prevent even one single war, how gloriously would our studies and our labour
  be rewarded!"
* 587 the ruler who wages an unjust war can never make right the destruction
  he works against humanity
* 588 the soldiers and officers (the "instruments in the hands of their
  sovereign") cannot be held accountable for the war their ruler wages

### Chapter XII:  Of the Voluntary Law of Nations, as it regards the Effects of Regular Warfare, independently of the Justice of the Cause
* 589 every nation has the right to determine what is just or unjust for her
  own conduct
* 589 no other nation (in so far as they do not affect her) has a right to
  interfere with another nation's actions, because there is no ultimate judge
  between nations to make the final determination
* the voluntary law of nations as they relate to war:
  1. 591 "regular war, as to its affects, is to be accounted just on both sides"
  2. 591 "whatever is permitted to the one in virtue of the state of war, is
     also permitted to the other", but we can never abide "unbridled
     licentiousness"
  3. 592 the law of nations does not justify one who wages unjust war even if
     the outcome is that they win; they are still morally liable for their
     actions

### Chapter XIII:  Of Acquisitions by War and particularly of Conquests
* 593 in war we have a right to take property from our enemy and use it for
  our own ends; until I can acquire restitution from my enemy, I am allowed to
  take an equivalent from his possessions
* 594 BUT we must make a just estimation of what is due to us and take no more
  than is necessary
* 594 "The lawful end of punishment is future security."
* 594 But a nation does not have the right to judge that their enemy is taking
  more than is due to them; thus, all acquisition in warfare is valid
  according to the voluntary law of nations independent of its justice
* 594 property is vested in the enemy the moment it comes into his power
* 596 conscience requires that we restore to a third-party property that was
  taken by an enemy in an unjust war (even at our own expense); provided the
  theft was recent enough that we can be sure it is their property
* 596 a conquered town or province under the control of an enemy is still to
  be considered as belonging to the original sovereign until he renounces it
  because it may still return to his possession at the end of hostilities -- a
  third-party, e.g., would not be allowed to buy it unless it was voluntarily
  relinquished
* 598 a sovereign, having completely conquered her opponent, does not have a
  right to do with her enemy as she please -- she has only a right to just
  restitution
* 600 a conquered people should be treated with all the justice and rights
  that one's own citizens possess
* 603 just as we should return property from our enemy to the third-party to
  whom it belongs, so too should we free a people who have been unjustly
  oppressed by our enemy

### Chapter XIV:  Of the Right of Postliminium
* 603 *the right of postliminium*:  that persons and things taken by the enemy
  are restored to their former state, now coming again into the power of the
  nation to which they belong
* 606 movable goods are exempted from the right of postliminium unless they
  were taken so recently that there is no doubt who the rightful owner is
  * once some time has passed, there may be no end to debates over what
    belongs to whom -- thus we do not apply the right of postliminium
* 608 liberated peoples should always be returned to their former liberty;
  they are not the same thing as spoils of war
* 609 unless that nation is completely conquered, in which case they become
  the citizens of their new sovereign

### Chapter XV:  Of the Rights of private Persons in War
* 612 war can only be declared by the sovereign, and any hostilities engaged
  in (except self-defense) must be done at the order of the sovereign
* 613 even though in a state of war every subject of enemy nations are at war,
  it is good practice (and leads to less bloodshed) to have dedicated armies
  do the actual combat while the common citizen continues on his life
* 614 even so, there are times (like citizens taking up arms to remove an
  invader from their town) when citizens may act on a tacit order that their
  sovereign would approve of
* 615 rank-and-file soldiers should always obey their commanders' orders, even
  if they don't understand them or disagree
* 616 nor should they attack the enemy without express orders from their
  commanders; even a successful outcome should be punished for its
  recklessness
* 617 subjects should be repaid for damages inflicted to property in the
  course of erecting fortifications but not for damage in the ordinary course
  of war
* 617-8 an exception would be supporting a family who lost their breadwinner

### Chapter XVI:  Of various Conventions made during the Course of the War
* 619 a truce does not mean the end of a war
* 619 but a long truce can act in this way when enemies are weary of war but
  cannot agree to restitution
* 620 a long "general truce" can only be declared by the sovereign
* 620 short, partial (local) truces can be declared by the general in charge
  of the hostility because sometimes he cannot wait for an order from his
  sovereign
* 621 subjects may engage in combat before hearing about a truce; they are not
  to be held liable for not knowing about the truce; but the sovereign should
  make restitution because he is bound to fulfill the promise of the truce
* 622 subjects violating the truce do not invalidate the truce; they should be
  punished as criminals; if they are not punished, the sovereign becomes
  complacent in their actions, and *then* the truce is broken
* 624 a truce does not prevent a sovereign from doing what he would do during
  peace -- that is arming troops, marching them through his country, etc.
* 625 but a truce does limit parties from doing something they would not have
  been able to do during hostilities
  * Ex.: a partial truce is called on the siege of a town; the attackers
    cannot engage in any hostility; nor can the defenders use the truce to
    refortify their walls or erect new foritifications that they would not
    have been able to do were they still fighting
  * 626-7 Ex.: say a truce has been called for the burying of dead; one army
    uses the time to retreat; this invalidates the truce, and the other army
    may stop them; but it is perfectly justifiable for the former to retreat
    because it is up to the vigilence of the latter to prevent it
* 627 if the enemy retreats from a town during a truce, it is lawful for us to
  retake it, but it would not be lawful to try to retake the town if the enemy
  still occupies it *even if they leave it unguarded*
* 628 the right of postliminium does not come into effect in a truce
* 629 one does not need to redeclare war after a truce unless perhaps the
  truce has been particularly long (several years)
* 632 a prisoner of war released on parole, is obligated to obey that parole,
  and his own sovereign cannot compel him to disobey it

### Chapter XVII:  Of Safe-conducts and Passports -- with Questions on the Ransom of Prisoners of War
* 633 "A *safe-conduct* is given to those who otherwise could not safely pass
  through the places where he who grants it is master"
  * this is different from a passport which is a formality for someone who has
    the right of passage even without it
* [I don't think passports and safe-conducts are applicable to my research...]
* [also goes on to discuss ransom for prisoners of war]

### Chapter XVIII:  Of Civil War
* 642 rebellion, sedition, etc. are crimes against the state -- even when they
  are justified -- because they disturb the peace of the state
  * 642 if people are disturbed by their government, they should pursue
    peaceful, legal matters through the courts or directly to the sovereign
* 642 even if the disturbance is unjustified, the sovereign should show mercy
  to the rebels in direct proportion to their number because punishment grows
  in cruelty the more people it affects
* 643 [I like this phrase:] "when they have once drawn the sword, they must
  throw away the scabbard"
* 643 in ending a rebellion, the sovereign should seek to give the people
  satisfaction -- there are very few rebellions which are completely
  unjustified
* 645 *rebellion*: unjustified insurrection
* 645 *civil war*: when a rebellion rises in sufficient strength to become a
  real opposition to the sovereign
* 645 the two sides in a civil war should be viewed the same as we would two
  different nations at war -- and the same rules of war should be observed
  between them
* 648 foreign nations should observe the same rules towards the different
  parties in a civil war as they would any other war

## Book IV:  Of the Restoration of Peace; and of Embassies

### Chapter I:  Of Peace and the Obligation to cultivate it
* 652 both people and nations have an obligation to cultivate peace

### Chapter II:  Treaties of Peace
* 655 the same authority who has the power to make war should also have the
  same power to make peace
  * 656 however, the government/constitution may set limits to the terms of
    peace -- e.g. he may not relinquish control of a territory
    * 658 not applicable to lands captured by the enemy: the sovereign has the
      right to not try to take them back by force
  * 657 if a nation has not set limits on the powers of a sovereign to make
    peace, then that sovereign may relinquish control of a part of the state
    for the prupose of securing peace and saving the rest; the nation has
    given her tacit consent with her silence on the matter
* 656 nations -- like Sweden -- may refrain from putting the power of making
  war in a single sovereign and leave it to an assembly -- even if the king
  still retains the power to make peace
* 659-60 if the sovereign is captured as a prisoner of war, then another
  should become the sovereign while the former is in captivity and unable to
  fulfill his duties; thus, if the sovereign is a prisoner of war, he no
  longer has the power to declare peace
* 661 a sovereign cannot make peace without involving his allies
* 664 a peace treaty, by its very nature, necessarily implies amnesty towards
  the parties involved in the war
* 664 whatever is not otherwise specified in the treaty returns to its
  condition before the war
* 665 if a treaty says that all things should be restored to the state they
  were in before the war, this cannot be applied to movable property (i.e.
  *booty*)

### Chapter III:  Of the Execution of the Treaty of Peace
* 666 if an army without knowledge of the treaty engages in hostilities after
  the official end of the war, they should not be punished; but their
  sovereign has the responsibility of returning anyone and anything which was
  captured
* 666 peace treaties should be published widely and quickly
* 669 a peace treaty should be written to be as unambiguous as possible
* 670 in cases of ambiguity, the treaty should be interpreted in the most
  favorable way towards the "loser" of the war
* 670 names of countries ceded should be interpreted according to the usage
  prevailing at the time among intelligent men
* 670 the treaty only relates to the war which it terminates

### Chapter IV:  Of the Observance and Breach of the Treaty of Peace
* 672 fear or force alone can never justify breaching a treaty of peace
  * however, if a usurper forces a nation to accept an unjust treaty, then it
    is not peace but oppression, and the latter is at liberty to breach it
* 674-5 neither going to war over a new cause nor allying oneself with an
  enemy can be viewed as breaching a treaty
* 675-6 resuming hostilities for the same reason as a previous war was fought
  is to reignite that very war; in which case the old treaty is voided, and
  should the parties wish anything in that old treaty to be upheld in a new
  treaty, it should be explicitly stated in the new
* 676 justifiable self-defense is not a breach of treaty
* 677 a breach of the treaty against one nation is also a breach of the treaty
  against all their allies named in the treaty
  * 680-1 but if one nation breaches the treaty with another, the treaty is
    still in full effect between the latter and the allies of the former
    (provided the allies are not also involved in the breach)
* 677 doing anything contrary to engaging in "friendship" is a breach of the
  treaty; because every treaty of peace has as its implicit purpose for
  nations to live in peace with each other
  * [but if this is the case, how can a new war for new reasons between the two
    nations involved in a treaty *not* void every prior treaty between them; either
    the aggressor is unjust in their aggression -- in which case the aggressor
    has voided the treaty -- or the other party has done something which
    necessitates the aggression -- in which case the other has voided the
    treaty]
* 678-9 if a single article of a treaty of peace is broken, then the whole
  treaty is made void
  * the lone exception is if the treaty explicitly states something to the
    effect of "though any one of the articles of the treaty may happen to be
    violated, the others shall nevertheless subsist in full force"
* 679 if a penalty is given for breaking a particular article of the treaty,
  once an offending party has submitted to the penalty, the treaty is resumed
* 679-80 if a party to the treaty is unable to honor an article of the treaty,
  then the other party should work to accept some sort of possible equivalent
  -- when forced to choose, we should choose peace over justice
* 681 if a treaty is violated by one party, the other party may either declare
  the treaty null and void, or they may declare that it is still in force and
  insist the other party abide by it

### Chapter V:  Of the Right of Embassy, or the Right of sending and receiving public Ministers
* 682 *public ministers*: diplomats
* 683 every sovereign nations has a right to send and receive diplomats
* 684 this right extends even to cities/territories who are under the
  subjection of a larger power
* 685 a sovereign who prevents another nation from sending/receiving diplomats
  in time of peace offends the law of nations
  * 686 naturally, a sovereign has a right to detain diplomats between allied
    enemies in time of war
* 686 a sovereign cannot deny an audience with the diplomats of a peaceful
  nation -- the law of nations requires communication remain open between
  nations
  * 686 but it does not apply to indefinitely housing diplomats who have no
    immediate business with the sovereign/nation
* 687-8 sovereigns should allow diplomats from countries with whom they are at
  war, in order to keep open the possibility of peaceful negotiations
* 689 because no nation can judge the domestic affairs of another, it is
  within her right to accept the diplomats of a usurper; and it is no injury
  to the "rightful" sovereign if they do

### Chapter VI:  Of the several Orders of public Ministers -- of the representative Character -- and of the Honours due to Ministers
* 690 public officials / ambassadors have the same powers and prerogatives as
  their sovereign to the extent that their office grants
* 691-2 four different types of ambassadors: *embassadors*, *envoys*,
  *resident*, and *ministers*
* [this chapter repeats a number of things that have been discussed elsewhere
  above]
* 695 ambassadors are to be shown all respect due to a representative of the
  sovereign

### Chapter VII:  Of the Rights, Privileges, and Immunities of Embassadors and other Public Ministers
* 697 because ambassadors have a right to security, their nation has a right
  to erect an embassy to house them
  * "A right to the end inseparably involves a right to the necessary means."
* 698 a crime committed against an ambassador cannot be pardoned by the
  sovereign of the land in which it happened; it can only be pardoned by the
  sovereign whom it was against
* 699-700 ambassadors are protected by the law of nations; a consequence is
  that every nation has an interest in the murder of an ambassador (unlike
  the murder of a private citizen which only concerns the nations involved)
  because it risks disturbing the peace between nations
* 700-1 ambassadors may be arrested in war time, but they may not be harmed
* 702 in order to keep open communication between nations, there should always
  be a rank of messenger (*durmmer* or *trumpeter*) who has perfect security
  to pass messages between sovereigns even in time of war
  * 703 ...even during a civil war
* 704 only the sovereign, commander-in-chief, or a general may send such a
  messenger, and they may only be sent to the opposing commander-in-chief;
  diverging from this may result in the messenger suspected of being a spy
* 706 ambassadors must be independent of the sovereign authority -- *both
  civil (729-30) and criminal* -- when they are in residence abroad
  * 707 but ambassadors should still obey all laws where they reside when it
    does not interfer with their business -- "he is independent; but he has
    not a right to do whatever he pleases"
* 708-10 ambassadors, as they are on a mission of peace, should not use their
  position to seduce others to betray their country
* 711 how to treat an ambassador who has committed a (non-violent) crime:
  1. ordinary crimes: make a plea to your sovereign who will bring the issue
     up with the ambassador's sovereign
  2. crime against the sovereign himself: refuse to see him or expel him from
     the court
* 720 ambassadors should not be retaliated against for the actions of their
  sovereign
* 721-2 many examples of how ambassadors are universally treated throughout
  history and the world
* 726 a sovereign visiting foreign soil should be treated the same way as the
  ambassador
* 728 representatives of a legislative body in a republic should have the same
  protections and independence as ambassadors when they are engaged in public
  business

### Chapter VIII:  Of the Judge of Embassadors in Civil Cases
* 731 even though an ambassador is not, by default, subject to the civil
  jurisdiction of his country of residence, he may voluntarily subject himself
  to the civil law provided he has the permission of his sovereign
* 731 if someone commits a crime against an ambassador, the ambassador should
  not initiate a criminal prosecution by, e.g., going to the police himself;
  he should notify the sovereign who will take action
* 731-3 an ambassador, though working for a foreign prince, may be a subject
  of the country in which he resides; in which case, care must be taken to
  separate his public and private affairs; the public affairs have the same
  independence and immunity as other ambassadors, but his private affairs are
  under the same jurisdiction as every other subject
* 733-4 an ambassador's immunity extends to his personal property and any
  property which is necessary for his public role, but any property he
  acquires as a result of private business -- e.g. trade/commerce -- does not
  have such immunity
* 735 immovable property is always under the jurisdiction of the nation of
  residence even when owned by the ambassador
  * the only exception is his domicile which he needs to fulfill his public
    duties (737)
* 736 "If the necessity and importance of his functions set him above all
  prosecution in the foreign country where he resides, shall any man be
  allowed to molest him in the performance of his ministerial duties by
  summoning him to appear before the tribunals of his own country?"
  * [Am I right in interpreting Vattel here to mean that the ambassador is
    even immune from prosecution in his home country while fulfilling his
    public duties?]

### Chapter IX:  Of the Embassador's House and Domestics
* 738-9 an embassy or ambassador's home, because it has jurisdictional
  immunity, may be used as an asylum for people who commit misdemeanors; but a
  sovereign has every right to ignore such immunity when the criminal being
  offered asylum threatens the safety or peace of the state
* 740 jurisdictional immunity also extends to the vehicles an ambassador uses
  to carry out public business
  * 741 ditto his household and staff
* 743 an ambassador has authority over his staff
* 744 if a staff member commits a crime, the ambassador may fire him; in which
  case, he comes under the jurisdiction of the nation of residence
