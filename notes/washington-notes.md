# [Notes on the Sentiments on the Government of John Jay, Henry Knox, and James Madison](https://founders.archives.gov/documents/Washington/04-05-02-0154)

## Mr. Jay

* does not believe that giving further powers to congress is the answer
* wishes to separate executive and judicial duties from the legislative
* separate congress into upper (lifetime membership) and lower (annual
  elections) houses
* single "Governor General" as executive with a council of judicial officers
* much more power given to the national government -- "The *States* retaining
  only so much as may be necessary for domestic purposes"
* Jay questions the convention because "it ought to have originated with, &
  the proceedings be confirmed by the People -- the only source of Just
  authority."

## General Knox

* prefers one government to an association of governments
* lower house ("assembly") chosen every 1, 2, or 3 years
* upper house ("senate") chosen every 5, 6, or 7 years
* single executive called the "Governor General" chosen by the assembly and
  senate every 7 years
* single judicial appointed by the executive "during good behaviour"
* both the judicial and executive are impeachable by the assembly and triable
  by the senate
* laws passed by the national government should trump local governments and
  enforced by a standing army
* all national concerns to be executed by the GG without reference to local
  governments
* "This is considered as a government of the least possible powers to preserve
  that federated government -- To attempt to establish less will be to hazard
  the existence of republicanism, and to subject us to division of the
  European powers or to a despotism arising from high handed Commotions."

## Mr. Madison

* a change must be made in the principle of representation
* national government should have complete authority in the regulation of
  trade, taxing imports and exports, and naturalization
* veto power over state laws
* there should be a national judiciary
* militia should be placed under some authority which is entrusted with
  general protection and defense
* there should be a lower house of many members and an upper house of fewer
  (but he leaves blank the limits to their offices)
* veto power over state laws should rest in the legislature
* a single national executive
* an article should be added to the constitution expressly guaranteeing
  protection of the states from foreign and domestic threats
* manner and right of coercion over the state governments should be expressly
  declared
* ratification must come from the people and not just the authority of the
  state legislatures
